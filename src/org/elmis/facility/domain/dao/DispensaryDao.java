package org.elmis.facility.domain.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.model.Dispensary;
import org.elmis.facility.utils.ElmisAESencrpDecrp;

public class DispensaryDao {
	
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public void addDispensingPoint(Dispensary dispensary){
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			session.insert("Dispensary.insert_dispensary", dispensary);
			session.commit();
		}
		finally{
			session.close();
		}
	}
	public List<Dispensary> getDispensingPoints(int siteId){
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			System.out.println(siteId);
			return session.selectList("Dispensary.get_dispensaries", siteId);
		}
		finally{
			session.close();
		}
	}
	public static void main(String[] args) {
		Properties prop = new Properties(System.getProperties());
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("Programmproperties.properties");
			prop.load(fis);

			System.setProperties(prop);

			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
		System.setProperty("dbpassword", dbpassword);
		Dispensary dispensary = new Dispensary();
		dispensary.setDispensingPointName("ARV1");
		dispensary.setProgramArea("ARV");
		dispensary.setSiteId(12);
		new DispensaryDao().addDispensingPoint(dispensary);
		List<Dispensary> dispensaryList = new DispensaryDao().getDispensingPoints(77);
		for (Dispensary d : dispensaryList)
			System.out.println(d);
		
		
	}
}
