﻿ALTER TABLE elmis_patient
 
   ADD COLUMN dateofbirth date,
  ADD COLUMN firstname character varying(50),
  ADD COLUMN lastname character varying(50),
  ADD COLUMN sex character varying(10),
  ADD COLUMN location character varying(100),