package org.elmis.forms.admin.dispensingpoints;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.forms.stores.issuing.IssuingJD;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class NumberPadJD extends javax.swing.JDialog {

	//private static int numberOfItems = 0;
	private static String barcodeIn;

	private List<Character> charList = new LinkedList();

	private char c;

	private Products product;
	private ProductQty qty;
	private static int selectedProgramID;
	
	List<Programs> programs = new LinkedList();
	List<VW_Systemcalculatedproductsbalance> productsstockQtyList = new LinkedList();
	FacilityProductsDAO Facilityproductsmapper = null;

	/** Creates new form NumberPadJD 
	 * @wbp.parser.constructor*/
	public NumberPadJD(java.awt.Frame parent, boolean modal, Products product,int selectedprogid) {
		super(parent, modal);
		addWindowListener(new WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}

		
		});
		initComponents();

		this.product = product;
		qty = this.product.getProductQty();
		
		this.selectedProgramID = selectedprogid;

		this.barcodeIn = IssuingJD.barcodeScanned;

		if (!this.product.getPrimaryname().equals("")) {

			//if the name is too long cut it short for display purpose
			if (this.product.getPrimaryname().length() >= 40) {

				productNameJL.setText(this.product.getPrimaryname().substring(
						0, 40));
			} else {

				productNameJL.setText(this.product.getPrimaryname());
			}
		}
        
		//this.qtyInStockJL.setText(this.qty.getQty() + "");
		//	this.qtyJTF.setText(numberOfItems + "");

		//this.qtyJTF.setText(qty.getQty() + "");

		this.setSize(320, 500);
		this.setLocationRelativeTo(null);

		this.getRootPane().setDefaultButton(okJBtn);
		this.qtyJTF.setFocusable(false);
		this.subtractJBtn.setFocusable(false);
		this.deleteJBtn.setFocusable(false);
		this.jButton1.setFocusable(false);
		this.jButton2.setFocusable(false);
		this.jButton3.setFocusable(false);
		this.jButton4.setFocusable(false);
		this.jButton5.setFocusable(false);
		this.jButton6.setFocusable(false);
		this.jButton7.setFocusable(false);
		this.jButton8.setFocusable(false);
		this.jButton9.setFocusable(false);
		this.jButton10.setFocusable(false);
		//this.jButton1.setFocusable(false);

		this.setVisible(true);

	}
	
	
	
	/** Creates new form NumberPadJD */
	public NumberPadJD(java.awt.Frame parent, boolean modal, Products product) {
		super(parent, modal);
		addWindowListener(new WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}

		
		});
		initComponents();

		this.product = product;
		qty = this.product.getProductQty();
		
		
		this.barcodeIn = IssuingJD.barcodeScanned;

		if (!this.product.getPrimaryname().equals("")) {

			//if the name is too long cut it short for display purpose
			if (this.product.getPrimaryname().length() >= 40) {

				productNameJL.setText(this.product.getPrimaryname().substring(
						0, 40));
			} else {

				productNameJL.setText(this.product.getPrimaryname());
			}
		}
        
		//this.qtyInStockJL.setText(this.qty.getQty() + "");
		//	this.qtyJTF.setText(numberOfItems + "");

		//this.qtyJTF.setText(qty.getQty() + "");

		this.setSize(320, 500);
		this.setLocationRelativeTo(null);

		this.getRootPane().setDefaultButton(okJBtn);
		this.qtyJTF.setFocusable(false);
		this.subtractJBtn.setFocusable(false);
		this.deleteJBtn.setFocusable(false);
		this.jButton1.setFocusable(false);
		this.jButton2.setFocusable(false);
		this.jButton3.setFocusable(false);
		this.jButton4.setFocusable(false);
		this.jButton5.setFocusable(false);
		this.jButton6.setFocusable(false);
		this.jButton7.setFocusable(false);
		this.jButton8.setFocusable(false);
		this.jButton9.setFocusable(false);
		this.jButton10.setFocusable(false);
		//this.jButton1.setFocusable(false);

		this.setVisible(true);

	}
	
	//changes introduced by Moses Kausa to get the product qua	ntity in store before issuing.
	private void formWindowOpened(WindowEvent evt) {
		// TODO Auto-generated method stub
		if(!(this.product == null)){
			getProductQuantity(this.product.getCode());
		}
		
	}		
	private void getProductQuantity(String code){
		String programcode = null;
		Facilityproductsmapper = new FacilityProductsDAO();
		
		programs = Facilityproductsmapper.dogetselectedprogram(this.selectedProgramID);
		
		for (@SuppressWarnings("unused")
		Programs prog : programs) {
			
			programcode = prog.getCode();	
		System.out.println(programcode + "got the code 2014");
		productsstockQtyList = Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(programcode,code);
		
		
		for (@SuppressWarnings("unused")
		VW_Systemcalculatedproductsbalance p : productsstockQtyList) {
			if(p.getProductcode().equals(code)){
				//set the qty here now 
				if(!(p.getBalance() == 0)){
				this.qtyInStockJL.setText(p.getBalance() + "");
				}else if(p.getBalance() == 0){
					
					String zero = "0";
					this.qtyInStockJL.setText(zero + "");
				}
			}
			
			}
		   if(productsstockQtyList.size() == 0){
			String zero = "0";
		    this.qtyInStockJL.setText(zero + "");
			}
		
		}
		
	}
	//end of changes 
	//END -2014 Moses Kausa
	
	
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jPanel2 = new javax.swing.JPanel();
		jButton1 = new javax.swing.JButton();
		jButton2 = new javax.swing.JButton();
		jButton3 = new javax.swing.JButton();
		jButton4 = new javax.swing.JButton();
		jButton5 = new javax.swing.JButton();
		jButton6 = new javax.swing.JButton();
		jButton7 = new javax.swing.JButton();
		jButton8 = new javax.swing.JButton();
		jButton9 = new javax.swing.JButton();
		jButton10 = new javax.swing.JButton();
		okJBtn = new javax.swing.JButton();
		deleteJBtn = new javax.swing.JButton();
		subtractJBtn = new javax.swing.JButton();
		addJBtn = new javax.swing.JButton();
		qtyJTF = new javax.swing.JTextField();
		productNameJL = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		qtyInStockJL = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(153, 153, 153));
		jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(
				javax.swing.border.BevelBorder.RAISED));

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));
		jPanel2.setBorder(javax.swing.BorderFactory
				.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

		jButton1.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton1.setText("1");
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		jButton2.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton2.setText("2");
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton2ActionPerformed(evt);
			}
		});

		jButton3.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton3.setText("3");
		jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton3ActionPerformed(evt);
			}
		});

		jButton4.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton4.setText("4");
		jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton4ActionPerformed(evt);
			}
		});

		jButton5.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton5.setText("5");
		jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton5ActionPerformed(evt);
			}
		});

		jButton6.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton6.setText("6");
		jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton6ActionPerformed(evt);
			}
		});

		jButton7.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton7.setText("7");
		jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton7ActionPerformed(evt);
			}
		});

		jButton8.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton8.setText("8");
		jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton8ActionPerformed(evt);
			}
		});

		jButton9.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton9.setText("9");
		jButton9.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton9ActionPerformed(evt);
			}
		});

		jButton10.setFont(new java.awt.Font("Tahoma", 0, 24));
		jButton10.setText("0");
		jButton10.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton10ActionPerformed(evt);
			}
		});

		okJBtn.setBackground(new java.awt.Color(0, 153, 0));
		okJBtn.setFont(new java.awt.Font("Tahoma", 0, 24));
		okJBtn.setForeground(new java.awt.Color(153, 0, 0));
		okJBtn.setText("OK");
		okJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				okJBtnActionPerformed(evt);
			}
		});
		okJBtn.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent evt) {
				okJBtnFocusLost(evt);
			}
		});

		deleteJBtn.setFont(new java.awt.Font("Tahoma", 0, 18));
		deleteJBtn.setText("DEL");
		deleteJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				deleteJBtnActionPerformed(evt);
			}
		});

		subtractJBtn.setFont(new java.awt.Font("Tahoma", 0, 24));
		subtractJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/sort_descending.png"))); // NOI18N
		subtractJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				subtractJBtnActionPerformed(evt);
			}
		});

		addJBtn.setFont(new java.awt.Font("Tahoma", 0, 24));
		addJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/sort_ascending.png"))); // NOI18N
		addJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addJBtnActionPerformed(evt);
			}
		});
		addJBtn.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				addJBtnKeyTyped(evt);
			}
		});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout
				.setHorizontalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																jButton1,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																jButton4,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																jButton7,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																subtractJBtn,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																deleteJBtn,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																addJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																146,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addGroup(
																jPanel2Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel2Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jButton10,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								70,
																								Short.MAX_VALUE)
																						.addComponent(
																								jButton8,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								67,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton5,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								67,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton2,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								67,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel2Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								okJBtn,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton9,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton6,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton3,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
										.addContainerGap()));

		jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { deleteJBtn, jButton1, jButton10,
						jButton2, jButton3, jButton4, jButton5, jButton6,
						jButton7, jButton8, jButton9, okJBtn, subtractJBtn });

		jPanel2Layout
				.setVerticalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																addJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																subtractJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jButton3,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton2,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jButton6,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton5,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton4,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jButton9,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton8,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton7,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																okJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton10,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																deleteJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { deleteJBtn, jButton1, jButton10,
						jButton2, jButton3, jButton4, jButton5, jButton6,
						jButton7, jButton8, jButton9, okJBtn, subtractJBtn });

		qtyJTF.setFont(new java.awt.Font("Tahoma", 0, 14));
		qtyJTF.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent evt) {
				qtyJTFFocusLost(evt);
			}
		});
		qtyJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				qtyJTFKeyTyped(evt);
			}
		});

		productNameJL.setFont(new java.awt.Font("Tahoma", 1, 12));
		productNameJL.setForeground(new java.awt.Color(255, 255, 255));
		productNameJL.setText("Product name here");

		jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Quantity in Stock");

		qtyInStockJL.setFont(new java.awt.Font("Tahoma", 1, 12));
		qtyInStockJL.setForeground(new java.awt.Color(255, 255, 255));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																jPanel2,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																jPanel1Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING,
																				false)
																		.addGroup(
																				jPanel1Layout
																						.createSequentialGroup()
																						.addComponent(
																								jLabel2)
																						.addGap(
																								10,
																								10,
																								10)
																						.addComponent(
																								qtyInStockJL,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								153,
																								Short.MAX_VALUE))
																		.addComponent(
																				productNameJL,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				qtyJTF,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				245,
																				Short.MAX_VALUE)))
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												qtyJTF,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												46,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(productNameJL)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel2)
														.addComponent(
																qtyInStockJL,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																14,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jPanel2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(437, 437, 437)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 462,
						Short.MAX_VALUE).addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void addJBtnKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		c = evt.getKeyChar();

		charList.add(c);

	}

	private void okJBtnFocusLost(java.awt.event.FocusEvent evt) {
		// TODO add your handling code here:

		okJBtn.requestFocus();
	}

	private void addJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		int qtyInStock = Integer.parseInt(qtyInStockJL.getText());
		int requestedQty = 0;

		if (!qtyJTF.getText().equalsIgnoreCase("")) {

			requestedQty = Integer.parseInt(qtyJTF.getText()) + 1;

		}
		//javax.swing.JOptionPane.showMessageDialog(null, "test "+requestedQty);

		StringBuilder s = new StringBuilder(charList.size());
		for (char c : charList) {

			s.append(c);

		}

		if (!qtyJTF.getText().equals("")) {

			if (IssuingJD.barcodeScanned != null) {

				//if the same item is being scanned 
				if (s.toString().trim().equals(IssuingJD.barcodeScanned.trim())) {

					//qtyJTF.setText((Integer.parseInt(qtyJTF.getText()) + 1) + "");
					//	getStockLimit((Integer.parseInt(qtyJTF.getText()) + 1));

					if (requestedQty > qtyInStock) {

						javax.swing.JOptionPane.showMessageDialog(this,
								"Over limit ");
					} else if (requestedQty <= qtyInStock) {

						qtyJTF.setText((Integer.parseInt(qtyJTF.getText()) + 1)
								+ "");
						//qtyJTF.setText(qtyJTF.getText() + qtyToAdd);
					}
					charList.clear();

				} else {

					//if different item scanned update last scanned barcode

					IssuingJD.barcodeScanned = s.toString().trim();

					charList.clear();

					//add scanned items to ItemsJTable

					okJBtnActionPerformed(evt);

				}

			} else {

				//qtyJTF.setText((Integer.parseInt(qtyJTF.getText()) + 1) + "");

				if (requestedQty > qtyInStock) {

					javax.swing.JOptionPane.showMessageDialog(this,
							"Over limit ");
				} else if (requestedQty <= qtyInStock) {

					qtyJTF.setText((Integer.parseInt(qtyJTF.getText()) + 1)
							+ "");
					//qtyJTF.setText(qtyJTF.getText() + qtyToAdd);
				}

				//	getStockLimit((Integer.parseInt(qtyJTF.getText()) + 1));
				//	javax.swing.JOptionPane.showMessageDialog(null, "Here");

			}

		} else {

			//qtyJTF.setText(1 + "");
			getStockLimit(1);

		}

	}

	private void subtractJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		if (!qtyJTF.getText().equals("")
				&& Integer.parseInt(qtyJTF.getText()) > 0) {

			qtyJTF.setText((Integer.parseInt(qtyJTF.getText()) - 1) + "");

		}
	}

	private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButton18A7ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButton1n15ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButtoton13ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButleteJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void okJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		if (!qtyInStockJL.getText().equalsIgnoreCase("0")) {

			//ArrayList<String> itemList = new ArrayList<String>();

			//itemList.add(IssuingJD.selectedProduct.getPrimaryname());

			if (this.qtyJTF.getText().equalsIgnoreCase("")) {

				//itemList.add(1 + "");

			} else {
				//itemList.add(this.qtyJTF.getText());
			}
			
			//itemList.add(IssuingJD.selectedProduct.getId().toString());
			//itemList.add(IssuingJD.selectedProduct.getCode());
			
			 
			 @SuppressWarnings("unused")
			String[] itemList = {IssuingJD.selectedProduct.getPrimaryname(),
					 this.qtyJTF.getText(), 
					 IssuingJD.selectedProduct.getId().toString(),IssuingJD.selectedProduct.getCode()	                
				     
				
                 };
			 
			 IssuingJD.PopulateProgram_ChangeregimenTable(itemList);
			this.dispose();
			//IssuingJD.tableModel_issueJTable.insertRow(itemList);

			//javax.swing.JOptionPane.showMessageDialog(null, "OK");
			
				
	
			
			

		} else {

			javax.swing.JOptionPane.showMessageDialog(null, "Out of stock");
		}

	}

	private void qtyJTFFocusLost(java.awt.event.FocusEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.requestFocusInWindow();
	}

	private void deleteJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		if (!qtyJTF.getText().equals("")) {

			//qtyJTF.setText(qtyJTF.getText().substring( (qtyJTF.getText().length()-1),qtyJTF.getText().length()));

			qtyJTF.setText(qtyJTF.getText().substring(0,
					qtyJTF.getText().length() - 1));

			//qtyJTF.setText( (qtyJTF.getText().length()-1),qtyJTF.getText().substring(qtyJTF.getText().length()));

			//System.out.println(qtyJTF.getText().length());//, (qtyJTF.getText().length()-1)));b)
		}
	}

	private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "0");

		getStockLimit(0);

	}

	private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "9");
		getStockLimit(9);

	}

	private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "8");
		getStockLimit(8);
	}

	private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "7");

		getStockLimit(7);
	}

	private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "6");
		getStockLimit(6);
	}

	private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "5");

		getStockLimit(5);
	}

	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "4");

		getStockLimit(4);
	}

	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "3");

		getStockLimit(3);
	}

	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "2");

		getStockLimit(2);
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "1");

		getStockLimit(1);
	}

	private void qtyJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		final char c = evt.getKeyChar();
		if (!(Character.isDigit(c) || (c == KeyEvent.VK_PERIOD)
				|| (c == KeyEvent.VK_COMMA) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
			this.getToolkit().beep();
			evt.consume();
		}
	}

	private void getStockLimit(int qtyToAdd) {
     if(!(qtyInStockJL.getText().toString().equals(""))){
		Double qtyInStock = Double.parseDouble(qtyInStockJL.getText());
   
		Double requestedQty =  Double.parseDouble(qtyJTF.getText() + qtyToAdd);

		if (requestedQty > qtyInStock) {

			javax.swing.JOptionPane.showMessageDialog(this, "Over limit ");
		} else if (requestedQty <= qtyInStock) {

			//javax.swing.JOptionPane.showMessageDialog(this, "Qty requested "
			//		+ requestedQty + "\nStock " + qtyInStock + "\nQty is JTF "
			//		+ qtyJTF.getText());

			qtyJTF.setText(qtyJTF.getText() + qtyToAdd);
		}

		requestedQty = 0.0;
     }//return overLimit;

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				NumberPadJD dialog = new NumberPadJD(new javax.swing.JFrame(),
						true, new Products(),selectedProgramID);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton addJBtn;
	private javax.swing.JButton deleteJBtn;
	private javax.swing.JButton jButton1;
	private javax.swing.JButton jButton10;
	private javax.swing.JButton jButton2;
	private javax.swing.JButton jButton3;
	private javax.swing.JButton jButton4;
	private javax.swing.JButton jButton5;
	private javax.swing.JButton jButton6;
	private javax.swing.JButton jButton7;
	private javax.swing.JButton jButton8;
	private javax.swing.JButton jButton9;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JButton okJBtn;
	private javax.swing.JLabel productNameJL;
	private javax.swing.JLabel qtyInStockJL;
	private javax.swing.JTextField qtyJTF;
	private javax.swing.JButton subtractJBtn;
	// End of variables declaration//GEN-END:variables

}