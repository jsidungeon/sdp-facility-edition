/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.receiving;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.MutableComboBoxModel;

import org.elmis.facility.dao.FacilityPhysicalCountDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Store_Physical_Count;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;

import com.oribicom.tools.TableModel;

import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.GroupLayout;

import java.awt.Label;

import javax.swing.JLabel;

import java.awt.Color;
import java.awt.TextField;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import java.awt.event.KeyAdapter;

/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class ProductsPhysicalCountJD extends javax.swing.JDialog {
    		
	private int         col;
	private int         rowz;
	TableColumnAligner tablecolumnaligner;
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;
	private Integer Productid = 0; 
	@SuppressWarnings("rawtypes")
	private List<Character> charList = new LinkedList();
	private char letter;
	private String searchText = "";
	private String pname;
	private String pstrength;
	private String SearchResult = "";
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> searcharvproductsList = new LinkedList();
	List<VW_Systemcalculatedproductsbalance> productsPhysicalcountList = new LinkedList();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
    private Elmis_Stock_Control_Card 	savestockcontrolcard = null;
	private String mydateformat = "yyyy-MM-dd";// hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	private Date Startdate;
	private Date Enddate;
	public Boolean productsearchenabled = false;
	public Boolean calltracer = false;
	public Boolean saveresponse = false;
	private Integer  row1 ;
	private  Integer row2 ;
	PositiveIntegerCellEditor checkuserinputinteger = null;
	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
		"Product Id","Product Code",
			"Product Name",
			"Generic Strength ",
			"Physical Count" ,"Remarks"};
	
	private static final String[] columns_searchfacilityApprovedproducts = {
		"Product Id","Product Code",
		"Product Name",
		"Generic Strength ",
		"Physical Count","Remarks"};
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "", "" ,""};
	private static final Object[] defaultv_searchfacilityapprovedproducts = { "", "",
		"", "", "" ,""};
	private static final int rows_fproducts = 0;
	private static final int rows_searchfproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] {false, false, false, false, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 5) {

				return getValueAt(0,5).getClass();
			}
			
			if (columnIndex == 4) {

				
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static TableModel tableModel_searchfproducts = new TableModel(
			columns_searchfacilityApprovedproducts,
			defaultv_searchfacilityapprovedproducts, rows_searchfproducts) {

		boolean[] canEdit = new boolean[] {false, false, false, false, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				/*if (columnIndex == 2) {

					return getValueAt(0, 2).getClass();
				}*/

				if (columnIndex == 5) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,5).getClass();
					// }	
				}
				if (columnIndex == 4) {
                 
					
					  
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		/*public Class getColumnClass(int c) 
		{     
		for(int rowIndex = 0; rowIndex < data.size(); rowIndex++)
		{
		    Object[] row = data.get(rowIndex);
		    if (row[c] != null) {
		        return getValueAt(rowIndex, c).getClass();
		    }   
		}
		return String.class;
		}*/

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsbalanceList = new LinkedList();
	ArrayList<Store_Physical_Count> sccproductbalanceList = new ArrayList<Store_Physical_Count>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> productbalanceprogramsList = new LinkedList();
	//ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator sccproductbalanceiterator = productsbalanceList.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProductsbalance;
	private Products facilitysccproduct;
	private FacilityPhysicalCountDAO physicalcountupdater = null;
	FacilityProductsDAO Facilityproductsmapper = null;
	private Timestamp shipmentdate;
	private Store_Physical_Count storephysicalcount;
	private Store_Physical_Count savestorephysicalcount;
	//private Shipped_Line_Items shipped_line_items;
	//private Shipped_Line_Items saveShipped_Line_Items;
	//private VW_Systemcalculatedproductsbalance sccproductelmis_stock_control_card;
	public static Boolean cansave = false;
	public static Boolean Adjustmentboolean = true;
	private String Pcode = "";
	private String Prodcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private int rnrid;
	private int intQtyreceived = 0;
	public String facilityprogramcode = "";
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private String programname = "";
	public static final String  STOREPHYSICAlCOUNTCALL = "STOREPHYSICAlCOUNTCALL"; 
	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();

	private JComboBox balanceProgramList = new JComboBox();
	MutableComboBoxModel modelbalanceProgramslist = (MutableComboBoxModel) balanceProgramList
			.getModel();

	/** Creates new form receiveProducts */
	public ProductsPhysicalCountJD(java.awt.Frame parent,
			boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(1000, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.ShipmentdateJDate.setPreferredSize(new Dimension(150, 20));
		balanceProgramTypeJCB.setPreferredSize(new Dimension(150, 20));
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		facilityapprovedProductsJT = new javax.swing.JTable();
		CancelJBtn = new javax.swing.JButton();
		SaveJBtn = new javax.swing.JButton();
		jPanel2 = new javax.swing.JPanel();
		ShipmentdateJDate = new com.toedter.calendar.JDateChooser();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel2.setVisible(true);
		balanceProgramTypeJCB = new javax.swing.JComboBox();
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Product Physical Count");
		setBackground(new java.awt.Color(51, 51, 255));
		setFont(new java.awt.Font("Ebrima", 0, 20));
		setForeground(new java.awt.Color(204, 153, 0));
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		facilityapprovedProductsJT.setDefaultRenderer(Object.class, new CustomModel());
		//AdjustmentfacilityapprovedProductsJT.addMouseListener(new CustomListener());
		facilityapprovedProductsJT.addKeyListener(new CustomListener1());
		

		facilityapprovedProductsJT.setFont(new java.awt.Font("Ebrima", 0, 20));
		facilityapprovedProductsJT.setModel(tableModel_fproducts);
		facilityapprovedProductsJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						facilityapprovedProductsJTMouseClicked(evt);
					}
				});
		facilityapprovedProductsJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						facilityapprovedProductsJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(facilityapprovedProductsJT);

		CancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBtn.setText("Close");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save  ");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));

		ShipmentdateJDate.setDateFormatString(mydateformat);
		ShipmentdateJDate.setFont(new Font("Ebrima", Font.PLAIN, 18));
		ShipmentdateJDate
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ShipmentdateJDatePropertyChange(evt);
					}
				});

		jLabel1.setFont(new Font("Ebrima", Font.BOLD, 18));
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Physical count date");

		jLabel2.setFont(new Font("Ebrima", Font.BOLD, 18));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Program Area");

		balanceProgramTypeJCB.setFont(new java.awt.Font("Ebrima", 0, 18));
		balanceProgramTypeJCB.setModel(modelbalanceProgramslist);
		balanceProgramTypeJCB
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						balanceProgramTypeJCBActionPerformed(evt);
					}
				});
		balanceProgramTypeJCB
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						balanceProgramTypeJCBPropertyChange(evt);
					}
				});
		
		JLableSearch = new JLabel("Product Name Search");
		JLableSearch.setForeground(Color.WHITE);
		JLableSearch.setFont(new Font("Ebrima", Font.BOLD, 18));
		
		SearchproductsJTF = new JTextField();
		SearchproductsJTF.addKeyListener(new KeyAdapter() {
		
			public void keyTyped(java.awt.event.KeyEvent evt) {
				
				SearchproductsJTFKeyTyped(evt);
			}
		});
		SearchproductsJTF.setFont(new Font("Ebrima", Font.PLAIN, 18));
		SearchproductsJTF.setColumns(10);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(ShipmentdateJDate, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel1))
					.addGap(78)
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(balanceProgramTypeJCB, GroupLayout.PREFERRED_SIZE, 226, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel2))
					.addGap(41)
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(JLableSearch, GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
						.addComponent(SearchproductsJTF, GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE))
					.addGap(31))
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel2Layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(jLabel2, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
							.addComponent(JLableSearch))
						.addComponent(jLabel1))
					.addGap(18)
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(ShipmentdateJDate, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
						.addGroup(jPanel2Layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(balanceProgramTypeJCB, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
							.addComponent(SearchproductsJTF, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(45, Short.MAX_VALUE))
		);
		jPanel2.setLayout(jPanel2Layout);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(10)
							.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 884, Short.MAX_VALUE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addContainerGap(705, Short.MAX_VALUE)
							.addComponent(CancelJBtn)
							.addGap(18)
							.addComponent(SaveJBtn)))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(18)
					.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 311, GroupLayout.PREFERRED_SIZE)
					.addGap(29)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(CancelJBtn)
						.addComponent(SaveJBtn))
					.addContainerGap(76, Short.MAX_VALUE))
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}// </editor-fold>
	
	 public class CustomModel extends DefaultTableCellRenderer {

	        /**
	         * 
	         */
	        private static final long   serialVersionUID    = 1L;

	        @Override
	        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	            JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	            Color c = Color.WHITE;
	            if (isSelected && row == rowz & column == col)
	                c = Color.GREEN;
	            label.setBackground(c);
	            return label;
	        }
	    }
 
	
 public class CustomListener1 extends KeyAdapter  {
 	 public void keyTyped(KeyEvent e) {   
 		/* if(e.getKeyCode() == KeyEvent.VK_ENTER){
 			 rowz = AdjustmentfacilityapprovedProductsJT.getSelectedRow();
 	            col = AdjustmentfacilityapprovedProductsJT.getSelectedColumn();
 	            // Repaints JTable
 	            AdjustmentfacilityapprovedProductsJT.repaint();
           }*/
 		// if(e.getKeyCode() == KeyEvent.VK_TAB){
 			 rowz = facilityapprovedProductsJT.getSelectedRow();
 	         col = facilityapprovedProductsJT.getSelectedColumn();
 	            // Repaints JTable
 	           facilityapprovedProductsJT.repaint();
           //}
         // Select the current cell
         
     }
 }
	
	
	private void SearchproductsJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:
				productsearchenabled  = true ;
				System.out.println(this.arvsearchproductsList.size() + "list not null");

				letter = evt.getKeyChar();

				StringBuilder s = new StringBuilder(charList.size());

				if ((letter == KeyEvent.VK_BACK_SPACE)
						|| (letter == KeyEvent.VK_DELETE)) {

					// do nothing
					searchText = this.SearchproductsJTF.getText();
				    System.out.println("I detedcted a back space ah! did you call me ??");
			           //re populate table with original list 
			           this.callbackARVproductlist();
					if (charList.size() > 0) {

						charList.remove(charList.size() - 1);

					}

					if (charList.size() <= 0) {

						s = new StringBuilder(charList.size());
						charList.clear();
					}

					for (char c : charList) {

						s.append(c);

					}
				} else {

					charList.add(letter);

					for (char c : charList) {

						s.append(c);

					}

					//get ARV list 
					try {
						if (!(this.SearchproductsJTF.getText() == "")) {

							if (!(s.substring(0, 1).matches("[0-9]"))) {

								callfacility = new FacilitySetUpDAO();
								Facilityproductsmapper = new FacilityProductsDAO();

								//this.facilityprogramcode = "ARV";
								//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
								//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

								facility = callfacility.getFacility();

								facilitytype = callfacility
										.selectAllwithTypes(facility);
								typeCode = facilitytype.getCode();

								productsList = Facilityproductsmapper
										.dogetcurrentFacilityApprovedProducts(
												facilityprogramcode, typeCode);
								arvsearchproductsList = productsList;

								/*for(VW_Program_Facility_ApprovedProducts p: arvsearchproductsList ){
									
									if(p.getPrimaryname().contains(s)){
										this.arvproductsList.add(p);
									}
								}*/

								for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {
									if (p.getPrimaryname().toLowerCase().contains(s.toString().toLowerCase())) {
										this.arvproductsList.add(p);
									}
									
								}

								//	System.out.println("Delete " + s);
								//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

								this.searchPopulateProgram_ProductTable(
										arvproductsList, "STRING_FOUND");

							}
						}

					} catch (NullPointerException e) {
						e.getMessage();
					}

				}

				// searchText += Character.toString(letter);
				/*	charList.add(letter);

					for (char c : charList) {

						s.append(c);

					}

					try {
						//check if char is a digit and escape the loop.
						if(!(this.searchjTextField.getText() == "")){
						if(!(s.substring(0,1).matches("[0-9]"))){ 

						callfacility = new facilitysetupsessionsmappercalls();
						Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

						this.facilityprogramcode = "ARV";
						//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
						//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

						facility = callfacility.getFacility();

						facilitytype = callfacility.selectAllwithTypes(facility);
						typeCode = facilitytype.getCode();

						productsList = Facilityproductsmapper.dogetcurrentFacilityApprovedProducts(facilityprogramcode, typeCode);
							
						arvsearchproductsList = productsList;

						for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

							if (p.getPrimaryname().contains(s)) {
								this.arvproductsList.add(p);
							}
						}
						//System.out.println("Delete " + s);
						//arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						searchPopulateProgram_ProductTable(arvproductsList,
								"STRING_FOUND");
						
						}
						
					}
					} catch (NullPointerException e) {
						e.getMessage();
					}*/
	}
	


	
	private void callbackARVproductlist(){
		//get ARV list 
		try {
			if (!(this.SearchproductsJTF.getText() == "")) {

		
					callfacility = new FacilitySetUpDAO();
					Facilityproductsmapper = new FacilityProductsDAO();

					//this.facilityprogramcode = "ARV";
				    facility = callfacility.getFacility();

					facilitytype = callfacility
							.selectAllwithTypes(facility);
					typeCode = facilitytype.getCode();

					productsList = Facilityproductsmapper
							.dogetcurrentFacilityApprovedProducts(
									facilityprogramcode, typeCode);
					arvsearchproductsList = productsList;

					this.searchPopulateProgram_ProductTable(
							arvsearchproductsList, "STRING_FOUND");

				
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}

	private void balanceProgramTypeJCBPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
	}

	@SuppressWarnings("unchecked")
	private void searchPopulateProgram_ProductTable(List fapprovProducts,
			String mysearch) {

		try {

			this.SearchResult = mysearch;

			/*	callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProducts(facilityprogramcode);*/
			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : searcharvproductsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			tableModel_searchfproducts.clearTable(); 
			this.facilityapprovedProductsJT
					.setModel(tableModel_searchfproducts);


			fapprovedproductsIterator = fapprovProducts.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");

				defaultv_searchfacilityapprovedproducts[1] = facilitysccProducts
						.getCode();

				defaultv_searchfacilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname();
				defaultv_searchfacilityapprovedproducts[3] = facilitysccProducts
						.getStrength();
				/*defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getPacksize();*/
				//	defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
				/*	TableColumn column1 = facilityapprovedProductsJT
							.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());*/

				//defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_searchfacilityApprovedproducts.length; j++) {
					cols.add(defaultv_searchfacilityapprovedproducts[j]);
					
				}

				tableModel_searchfproducts.insertRow(cols);
				//tableModel_searchfproducts.fireTableDataChanged();
				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	public static Date toDate(java.sql.Timestamp timestamp) {
		long millisec = timestamp.getTime() + (timestamp.getNanos() / 1000000);
		return new Date(millisec);

	}

	private void balanceProgramTypeJCBActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		if (productdeliverdate != null) {

			//convert to date value 	
			Date convertdate = toDate(productdeliverdate);
			//date split up methods
			Calendar cal = Calendar.getInstance();
			cal.setTime(convertdate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			if (!(day == 1)) {
				day = 1;
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.DATE, day);
				cal.set(Calendar.YEAR, year);
				Startdate = cal.getTime();
			}
			Enddate = toDate(productdeliverdate);
		}

		JComboBox cb = (JComboBox) evt.getSource();
		programname = (String) cb.getSelectedItem();
		System.out.println(programname);
		this.facilityprogramcode = programname;
		
		callfacility = new FacilitySetUpDAO();
		Facilityproductsmapper = new FacilityProductsDAO();
		facility = callfacility.getFacility();
		//System.out.println(facility.getCode() + "We have the facility code");
		facilitytype = callfacility.selectAllwithTypes(facility);
		typeCode = facilitytype.getCode();
		
		String dbdriverStatus = System.getProperty("dbdriver");
		if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
			productsbalanceList = Facilityproductsmapper
					.dogetcurrentFacilityApprovedProducts(
							facilityprogramcode, typeCode);
		} else {
			productsbalanceList = Facilityproductsmapper
					.dogetcurrentFacilityApprovedProducts(
							facilityprogramcode, typeCode);
		}
		this.PopulateProgram_ProductTable(productsbalanceList);

	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.dispose();
	}

	private void ShipmentdateJDatePropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		try {
			
			 Calendar currentDate = Calendar.getInstance(); //Get the current date
			 SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss.S"); //format it as per your requirement
			 String dateNow = formatter.format(currentDate.getTime());
			 System.out.println("Now the date is :=>  " + dateNow);

			String mydate = this.ShipmentdateJDate.getDate().toString();
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			java.util.Date d;

			d = sdf.parse(oldDateString);

			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			if(d.after(currentDate.getTime())){
				System.out.println("Test date diff ");
				productdeliverdate = Convertdatestr(currentDate.getTime().toString());
				JOptionPane.showMessageDialog(this, "Receipt Date cannnot be in the future",
						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				this.ShipmentdateJDate.setDate(new Date());
					
			}
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			java.util.Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productdeliverdate;

	}

	
	private boolean checktableCellinput(String s){
	
			 try { 
			        Integer.parseInt(s); 
			    } catch(NumberFormatException e) { 
			        return false; 
			    }
			    // only got here if we didn't return false
			    return true;
	}
	private void facilityapprovedProductsJTPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setMinWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setMaxWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setWidth(0);
				
		try {
			if ("tableCellEditor".equals(evt.getPropertyName())) {
				checkuserinputinteger = new  PositiveIntegerCellEditor();
				//saveShipped_Line_Items = new Shipped_Line_Items();
				if (this.facilityapprovedProductsJT.isColumnSelected(3)) {
					
				}

				if (this.facilityapprovedProductsJT.isColumnSelected(4)) {
					if (facilityapprovedProductsJT.isEditing()) {
						//checkuserinputinteger.stopCellEditing();
					//render color of cell 
						//TableColumn col = facilityapprovedProductsJT.getColumnModel().getColumn(3);
						//DefaultTableModel model3 = (DefaultTableModel)facilityapprovedProductsJT.getModel();
						//col.setCellRenderer(new CustomRenderer());
						System.out.println("THIS CELL HAS STARTED EDITING");
						//get column index
						colindex = this.facilityapprovedProductsJT
								.getSelectedColumn();
						rowindex = this.facilityapprovedProductsJT
								.getSelectedRow();
						Prodcode = tableModel_fproducts.getValueAt(
								this.facilityapprovedProductsJT
										.getSelectedRow(),1).toString();
						this.row1 = facilityapprovedProductsJT.getSelectedRow();

					} else if (!facilityapprovedProductsJT.isEditing()) {
						this.row2 = facilityapprovedProductsJT.getSelectedRow();
						this.calltracer = false;
						String chars
						 = tableModel_fproducts.getValueAt(
									this.facilityapprovedProductsJT.getSelectedRow(), 4).toString();
		              		if(!(chars.isEmpty())){			
							if (!(chars.substring(0, 1).matches("[0-9]"))) {	
								
								if(!(checktableCellinput((chars.substring(0, 1))))){
									
							    	JOptionPane.showMessageDialog(null, String.format("Physical Count must be a positive number", chars.substring(0, 1)));	
							    	//facilityapprovedProductsJT.editCellAt(this.facilityapprovedProductsJT.getSelectedRow(), 3);
							    }
								
							}
		              		}
						
                      	if(calltracer == false){
                      	  if ((chars.substring(0, 1).matches("[0-9]"))) {	
						     createshipmentObj(Prodcode, rnrid, shipmentdate);
                      		}
                      	
						}
						 //call tracer = true;
						System.out.println("THIS CELL IS NOT EDITING");
						 
					}
				}
			}
		} catch (ParseException p) {
			p.getMessage();
		}
	}
	
	

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	private void facilityapprovedProductsJTMouseClicked(
			java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call create  shipment object 
  	    	
		
		 if ( productsearchenabled == true){
		        
	    	    		
	    		
	    		Pcode = tableModel_searchfproducts.getValueAt(
	    				this.facilityapprovedProductsJT
	    						.getSelectedRow(), 1).toString();
	    		
	    		Productid = (Integer
	    				.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 0)
	    						.toString()));
	    		
	    		pname = tableModel_searchfproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),2).toString();
	    		
	    		pstrength = tableModel_searchfproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),3).toString();
		
		 }else{
	    		
	    		Pcode = tableModel_fproducts.getValueAt(
	    				this.facilityapprovedProductsJT
	    						.getSelectedRow(), 1).toString();
	    		
	    		Productid = (Integer
	    				.parseInt(tableModel_fproducts.getValueAt(rowindex, 0)
	    						.toString()));
	    		
	    		pname = tableModel_fproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),2).toString();
	    		
	    		pstrength = tableModel_fproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),3).toString();
					
		 }

	}

	public void createshipmentObj(String apcode, int arnrid,
			Timestamp amodifieddate) throws ParseException {
		
		String dbdriverStatus = System.getProperty("dbdriver");
		if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
					productsPhysicalcountList = Facilityproductsmapper
					.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
		} else {
			productsPhysicalcountList = Facilityproductsmapper
					.dogetcurrentSystemcalculatedproductbalanceshsqldb(facilityprogramcode,apcode);
		
		}
		//created a shipment object storephysicalcount.
		if (productdeliverdate == null) {
			java.util.Date date = new java.util.Date();
			amodifieddate = new Timestamp(date.getTime());
		} else {
			amodifieddate = productdeliverdate;
		}
		storephysicalcount.setProductcode(apcode);
		storephysicalcount.setFacilitycode(this.facility.getCode());
		storephysicalcount.setCreateddate(amodifieddate);
	    storephysicalcount.setCreatedby(AppJFrame.userLoggedIn);
		storephysicalcount.setDispensaryname(System.getProperty("dp_name"));
		if ( productsearchenabled == true){
			try {
				storephysicalcount.setPhysicalcount(Double.parseDouble(tableModel_searchfproducts.getValueAt(rowindex,4)
								.toString()));
				String str = tableModel_fproducts.getValueAt(rowindex, 4)
						.toString();
				System.out.println("%%%%%% "+str);
				Double suppliedPhysicalCount = Double.parseDouble(str.toString());
								storephysicalcount.setPhysicalcount(suppliedPhysicalCount);
				
			//iterate through the array list and compare the quantity flag the differences
				for (@SuppressWarnings("unused")
				VW_Systemcalculatedproductsbalance p : productsPhysicalcountList) {
					if(p.getProductcode().equals(storephysicalcount.getProductcode())){
						
						
				         Double Physicalcountbal = storephysicalcount.getPhysicalcount();
								
								Physicalcountbal =	(Math
										.round(Physicalcountbal * 100.0) / 100.0);
						
						if( (p.getBalance().compareTo( Physicalcountbal) != 0 )){
					GetLastDayOfMonth(storephysicalcount,storephysicalcount.getCreateddate(),productsPhysicalcountList,storephysicalcount.getPhysicalcount());
						 System.out.println("Im am invisible");
						//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 3);
						 Double calculatedPhysicalCount = p.getBalance();
						 double diffmsgnum = (calculatedPhysicalCount - suppliedPhysicalCount);
							if(diffmsgnum < 0){
								diffmsgnum = -(diffmsgnum);
							}
							diffmsgnum =	(Math
							.round(diffmsgnum * 100.0) / 100.0); 
						 
						int ok = new arvmessageDialog()
						.showDialog(
								this,
								"Physical count does not match System Calculated balance\n" +
										"System calculated balance = "+  "\n" +
										calculatedPhysicalCount+ "\n"+" Computerized adjustment = "+ diffmsgnum,
								"Physical count error",
								"NO, \n Edit ",
								"Do Adjustment");
								
						if (ok == JOptionPane.YES_OPTION) {
							try {
							//Proceed to save the physical count 
							this.calltracer = true;
							saveresponse = true;
							//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 4);
							}catch(java.lang.ArrayIndexOutOfBoundsException e){
								e.getStackTrace();
							}
						}else if (ok == JOptionPane.NO_OPTION) {
						
								this.calltracer = true;
								saveresponse = false;
							//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 4);
								double diffnum = (calculatedPhysicalCount - suppliedPhysicalCount);
								if(diffnum < 0){
								diffnum = -(diffnum);
								}
								
								
								diffnum =	(Math
										.round(diffnum * 100.0) / 100.0); 
								new testMultipleAdjustmentsStore(javax.swing.JOptionPane.getFrameForComponent(this),
				    					true, Pcode, Productid, productdeliverdate,this.facilityprogramcode,pname,pstrength,diffnum,STOREPHYSICAlCOUNTCALL,"STOREPC");
							
						}else if (ok == JOptionPane.CANCEL_OPTION) {
							//set up adjustment form.
							
					
						}
						
						
						
					} 
					}
				}
				
				}catch(NumberFormatException e){
					e.getStackTrace();
				}
		}else{
		
		try {
			String str = tableModel_fproducts.getValueAt(rowindex, 4)
					.toString();
			System.out.println("%%%%%% "+str);
			Double suppliedPhysicalCount = Double.parseDouble(str.toString());
		storephysicalcount.setPhysicalcount(suppliedPhysicalCount);
		
		//iterate through the arraylist and compare the quantity flag the differences
		for (@SuppressWarnings("unused")
		VW_Systemcalculatedproductsbalance p : productsPhysicalcountList) {
			if(p.getProductcode().equals(storephysicalcount.getProductcode())){
				
         Double Physicalcountbal = storephysicalcount.getPhysicalcount();
				
				Physicalcountbal =	(Math
						.round(Physicalcountbal * 100.0) / 100.0);
				
				
				if( (p.getBalance().compareTo( Physicalcountbal) != 0 )){
				GetLastDayOfMonth(storephysicalcount,storephysicalcount.getCreateddate(),productsPhysicalcountList,storephysicalcount.getPhysicalcount());
				System.out.println("Im am invisible");
				double calculatedPhysicalCount = p.getBalance();
				
				 double diffmsgnum = (calculatedPhysicalCount - suppliedPhysicalCount);
					if(diffmsgnum < 0){
						diffmsgnum = -(diffmsgnum);
					}
				 
					diffmsgnum =	(Math
							.round(diffmsgnum * 100.0) / 100.0); 
				int ok = new arvmessageDialog()
				.showDialog(
						this,
						"Physical count does not match System Calculated balance\n" +
								"System calculated balance = "+  "\n" +
								calculatedPhysicalCount+ "\n"+" Computerized adjustment = "+ diffmsgnum,
						"Physical count error",
						"NO, \n Edit ",
						"Do Adjustment");
				
				if (ok == JOptionPane.YES_OPTION) {
					try {
						//Proceed to save the physical count 
				     calltracer = true;
					saveresponse = true;
					System.out.println("Got inside yeah!");
					//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 4);
					}catch(java.lang.ArrayIndexOutOfBoundsException e){
						e.getStackTrace();
					}
				}else if (ok == JOptionPane.NO_OPTION) {
					
						this.calltracer = true;
						saveresponse = false;
						
						double diffnum = (calculatedPhysicalCount - suppliedPhysicalCount);
						if(diffnum < 0){
						diffnum = -(diffnum);
						}
						
						diffnum =	(Math
								.round(diffnum * 100.0) / 100.0); 
						new testMultipleAdjustmentsStore(javax.swing.JOptionPane.getFrameForComponent(this),
		    					true, Pcode, Productid, productdeliverdate,this.facilityprogramcode,pname,pstrength,diffnum,STOREPHYSICAlCOUNTCALL,"STOREPC");
					//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(), 4);
					
				}
				
			}
			}
		}
		
		}catch(NumberFormatException e){
			e.getStackTrace();
		}catch(java.lang.NullPointerException n){
			n.getMessage();
		}
		}
		
		if ( productsearchenabled == true){
			try {
				/*storephysicalcount.setSystem_physicalcount(Integer
						.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 3)
								.toString()));*/
				}catch(NumberFormatException e){
					e.getStackTrace();
				}
			
		}else {
		try {
		/*storephysicalcount.setSystem_physicalcount(Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 3)
						.toString()));*/
		}catch(NumberFormatException e){
			e.getStackTrace();
		}
		}
		if ( productsearchenabled == true){
			storephysicalcount.setComments(tableModel_searchfproducts.getValueAt(
					rowindex, 5).toString());
		}else{
		storephysicalcount.setComments(tableModel_fproducts.getValueAt(
				rowindex, 5).toString());
		
		}
		//storephysicalcount.setCreatedby(AppJFrame.userLoggedIn);
		//storephysicalcount.setCreateddate(amodifieddate);
		//sccproductbalanceList.add(storephysicalcount);
		
	

	}

	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		String displaymessage = "";
		Boolean persist  = false;
		Boolean clickonce = false;
		//this.facilityapprovedProductsJT.editCellAt(facilityapprovedProductsJT.getSelectedRow(),4);
		//this.facilityapprovedProductsJT.firePropertyChange(facilityapprovedProductsJT.getColumnName(3), true, true);

		physicalcountupdater = new FacilityPhysicalCountDAO();
		
			if(!(this.productsearchenabled == true)){
		
			 		savestorephysicalcount = new Store_Physical_Count();
					//	System.out.println(elmisstockcontrolcardList.size());
			 		this.facilityapprovedProductsJT.editCellAt(-1,-1);
			 		int j = 0 ;
			 		for(int i = 0 ; i < this.facilityapprovedProductsJT.getRowCount();i++){
			 			
						Timestamp amodifieddate;
			 			if (productdeliverdate == null) {
		 					java.util.Date date = new java.util.Date();
		 					amodifieddate = new Timestamp(date.getTime());
		 				} else {
		 					amodifieddate = productdeliverdate;
		 				}
			 			if(!(this.facilityapprovedProductsJT.getValueAt(i,4).toString().equals(""))){
			 				System.out.println("HOW WBIG IS THE SELECTION IN JTABLE");
			 				persist = true;	
							//call me update methods
			 				
			 				savestorephysicalcount.setProductcode(tableModel_fproducts.getValueAt(
									i, 1).toString());
			 				savestorephysicalcount.setFacilitycode(this.facility.getCode());
			 				savestorephysicalcount.setCreateddate(amodifieddate);
			 				savestorephysicalcount.setCreatedby(AppJFrame.userLoggedIn);
			 				savestorephysicalcount.setComments(tableModel_fproducts.getValueAt(
			 						i, 5).toString());
			 				savestorephysicalcount.setPhysicalcount(Double.parseDouble(tableModel_fproducts.getValueAt(i,4)
											.toString()));
			 				
			 				savestorephysicalcount.setDispensaryname("stores");
			 				
			 				if (this.Adjustmentboolean == true){
			 					new checkstorePCdate(savestorephysicalcount.getProductcode());
			 								 					
			 				Facilityproductsmapper
							.Insertproductsphysicalcount(savestorephysicalcount);
			 				
			 				clickonce = true;
			 				/*JOptionPane.showMessageDialog(this, "Products saved to database",
									"Saving to Database", JOptionPane.INFORMATION_MESSAGE);*/
			 				//createsccobject(storephysicalcount);
			 				physicalcountupdater.callstockcontrolcardupdater(createsccobject(savestorephysicalcount));
			 				
			 				}else{
			 					JOptionPane.showMessageDialog(this, "Adjustments physical count is incorrect",
										"Adjustment total Error", JOptionPane.INFORMATION_MESSAGE);
			 					Adjustmentboolean = true;
			 				}
				 				
			 				
			 				
			 				
			 			}else{
			 				//print message qty required.
			 				
			 				do{
			 					if(j < 1){
				 				/*JOptionPane.showMessageDialog(this, "Product Quantity is Empty",
			 						"Save Error ", JOptionPane.INFORMATION_MESSAGE);*/
			 					}
				 				  j+=1;
				 				}while(j < 1);
			 				  
			 				
			 			}
			 			
			 		}
			
					if (clickonce == true){
					JOptionPane.showMessageDialog(this, "Products saved to database",
							"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
					}
			
			}else if (productsearchenabled == true){
				savestorephysicalcount = new Store_Physical_Count();
				//	System.out.println(elmisstockcontrolcardList.size());
				this.facilityapprovedProductsJT.editCellAt(-1,-1);
				int q  = 0;
		 		for(int i = 0 ; i < this.facilityapprovedProductsJT.getRowCount();i++){
		 			
					Timestamp amodifieddate;
		 			if (productdeliverdate == null) {
	 					java.util.Date date = new java.util.Date();
	 					amodifieddate = new Timestamp(date.getTime());
	 				} else {
	 					amodifieddate = productdeliverdate;
	 				}
		 			if(!(this.facilityapprovedProductsJT.getValueAt(i,4).toString().equals(""))){
		 				persist = true;
		 				System.out.println("HOW WBIG IS THE SELECTION IN JTABLE");
		 						 	
						//call me update methods
		 				
		 				savestorephysicalcount.setProductcode(tableModel_searchfproducts.getValueAt(
								i, 1).toString());
		 				savestorephysicalcount.setFacilitycode(this.facility.getCode());
		 				savestorephysicalcount.setCreateddate(amodifieddate);
		 				savestorephysicalcount.setCreatedby(AppJFrame.userLoggedIn);
		 				savestorephysicalcount.setComments(tableModel_searchfproducts.getValueAt(
		 						i, 5).toString());
		 				savestorephysicalcount.setPhysicalcount(Double.parseDouble(tableModel_searchfproducts.getValueAt(i,4)
										.toString()));
		 				savestorephysicalcount.setDispensaryname("stores");
		 				
		 				
		 				
		 				if (this.Adjustmentboolean == true){
		 					
		 					new checkstorePCdate(savestorephysicalcount.getProductcode());
			 				Facilityproductsmapper
							.Insertproductsphysicalcount(savestorephysicalcount);
			 				
			 				clickonce = true;
			 				/*JOptionPane.showMessageDialog(this, "Products saved to database",
									"Saving to Database", JOptionPane.INFORMATION_MESSAGE);*/
			 				//createsccobject(storephysicalcount);
			 				physicalcountupdater.callstockcontrolcardupdater(createsccobject(savestorephysicalcount));
			 				
			 				}else{
			 					JOptionPane.showMessageDialog(this, "Adjustments physical count is incorrect",
										"Adjustment total Error", JOptionPane.INFORMATION_MESSAGE);
			 					
			 					Adjustmentboolean = true;
			 				}
		 			}else{
		 				do{
		 					if(q < 1){
			 				/*JOptionPane.showMessageDialog(this, "Product Quantity is Empty",
		 						"Save Error ", JOptionPane.INFORMATION_MESSAGE);*/
		 					}
			 				  q+=1;
			 				}while(q < 1);
		 				  
		 			}
		 			
		 		}
		 		
		 		if(clickonce == true){
		 		JOptionPane.showMessageDialog(this, "Products saved to database",
						"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
		 		}
			}else{
				
			}
		
			this.facilityapprovedProductsJT.validate();
			callbackARVproductlist();
		
	}

	
	public void GetLastDayOfMonth(Store_Physical_Count sccphysicalcount,Date  mydate, @SuppressWarnings("rawtypes") List<VW_Systemcalculatedproductsbalance> myproductlist,Double double1) {
		
		//Date today = new Date();  
		physicalcountupdater = new FacilityPhysicalCountDAO();
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(mydate);  
        calendar.add(Calendar.MONTH, 1);  
        calendar.set(Calendar.DAY_OF_MONTH, 1);  
        calendar.add(Calendar.DATE, -1);  
        Date lastDayOfMonth = calendar.getTime();  
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
        System.out.println("Today            : " + sdf.format(mydate));  
        System.out.println("Last Day of Month: " + sdf.format(lastDayOfMonth));  
        int comparison  = lastDayOfMonth.compareTo(mydate);
       // if( comparison == 0){
        	//then update the stock control, card balance with the Physical count
        	System.out.println("PRINT OUT MY DATES");
			//check for date and time for updating stock control card products balance
			for (@SuppressWarnings("unused")
			VW_Systemcalculatedproductsbalance p : myproductlist) {
			
				//physicalcountupdater.callstockcontrolcardupdater(createsccobject(p));
				
			}
			
			
        //}
		
	}
	
	
	private void createsccobjectsamePC( Store_Physical_Count myphyscialcountproduct){
		
		savestockcontrolcard = new Elmis_Stock_Control_Card();
		//	System.out.println(elmisstockcontrolcardList.size());
			try{				
			savestockcontrolcard.setRefno("");
			savestockcontrolcard.setId(GenerateGUID());
			savestockcontrolcard.setProductcode(myphyscialcountproduct.getProductcode());
			savestockcontrolcard.setIssueto_receivedfrom("Physical Count");
			//savestockcontrolcard.setQty_received(sp.getQty_received());

			//savestockcontrolcard.setQty_isssued(sp.getQty_isssued());
			//savestockcontrolcard.setAdjustments(sp.getAdjustments());
			savestockcontrolcard.setBalance(myphyscialcountproduct.getPhysicalcount());
			//savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
			//savestockcontrolcard.setProductid(myphyscialcountproduct..getProductid());

			savestockcontrolcard.setCreatedby(myphyscialcountproduct.getCreatedby());
			savestockcontrolcard.setCreateddate(myphyscialcountproduct.getCreateddate());
			//savestockcontrolcard.setProgram_area(myphyscialcountproduct.);
		    savestockcontrolcard.setRemark(myphyscialcountproduct.getComments());
		    
		    physicalcountupdater.callstockcontrolcardupdater(savestockcontrolcard);
			}catch(NullPointerException e){
				e.getMessage();
			}
	}
	
	private Elmis_Stock_Control_Card createsccobject( Store_Physical_Count storephysicalcount2){
	
		savestockcontrolcard = new Elmis_Stock_Control_Card();
		//	System.out.println(elmisstockcontrolcardList.size());
			try{				
			savestockcontrolcard.setRefno("");
			savestockcontrolcard.setId(GenerateGUID());
			savestockcontrolcard.setProductcode(storephysicalcount2.getProductcode());
			savestockcontrolcard.setIssueto_receivedfrom("Physical Count");
			//savestockcontrolcard.setQty_received(sp.getQty_received());

			//savestockcontrolcard.setQty_isssued(sp.getQty_isssued());
			//savestockcontrolcard.setAdjustments(sp.getAdjustments());
			savestockcontrolcard.setBalance(storephysicalcount2.getPhysicalcount());
			//savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
			//savestockcontrolcard.setProductid(myphyscialcountproduct..getProductid());

			savestockcontrolcard.setCreatedby(storephysicalcount2.getCreatedby());
			savestockcontrolcard.setCreateddate(storephysicalcount2.getCreateddate());
			//savestockcontrolcard.setProgram_area(myphyscialcountproduct.);
		    savestockcontrolcard.setRemark(storephysicalcount2.getComments());
	       }catch(NullPointerException e){
				e.getMessage();
			}
		  return savestockcontrolcard;
			
	}
	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
				
		tablecolumnaligner = new TableColumnAligner();
		tablecolumnaligner.rightAlignColumn(this.facilityapprovedProductsJT,3);
		storephysicalcount = new Store_Physical_Count();
		this.facilityapprovedProductsJT.getTableHeader()
		.setFont(new Font("Ebrima", Font.PLAIN, 20));
		this.facilityapprovedProductsJT.setRowHeight(30);

		this.ShipmentdateJDate.setFocusable(isFocusable());
		Facilityproductsmapper = new FacilityProductsDAO();
		productbalanceprogramsList = Facilityproductsmapper.selectAllPrograms();
		for (Programs p : productbalanceprogramsList) {
			modelbalanceProgramslist.addElement(p.getCode());

		}

		//productsbalanceList = Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalances(valueA)

		//	PopulateProgram_ProductTable(productsbalanceList);
		rnrid = 4;
		this.ShipmentdateJDate.setDate(new Date());
		this.ShipmentdateJDate.setFocusable(isFocusable());
		shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");
	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fapprovProducts) {

		try {
			callfacility = new FacilitySetUpDAO();
			Facilityproductsmapper = new FacilityProductsDAO();

			facility = callfacility.getFacility();
			System.out
					.println(facility.getCode() + "We have the facility code");
			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();

			if (fapprovProducts == null) {
				callfacility = new FacilitySetUpDAO();
				Facilityproductsmapper = new FacilityProductsDAO();

				//this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				if (!(facilityprogramcode.equals(""))) {
					//convert timestamp to date 
					//Startdate;
					//	Enddate;
					String dbdriverStatus = System.getProperty("dbdriver");
					if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {

						productsbalanceList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
					} else {
						productsbalanceList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
					}
				}

			} else {
				productsbalanceList = fapprovProducts;
			}

			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsbalanceList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = productsbalanceList.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProductsbalance = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProductsbalance.getId();
				defaultv_facilityapprovedproducts[1] = facilitysccProductsbalance
						.getCode();

				defaultv_facilityapprovedproducts[2] = facilitysccProductsbalance
						.getPrimaryname();
				;
				defaultv_facilityapprovedproducts[3] = facilitysccProductsbalance
						.getStrength();
				/*defaultv_facilityapprovedproducts[3] = facilitysccProductsbalance
						.getBalance();*/

				/* defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
					TableColumn column1 = facilityapprovedProductsJT.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());
					
				defaultv_facilityapprovedproducts[4] = column1;*/

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				ProductsPhysicalCountJD dialog = new ProductsPhysicalCountJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton CancelJBtn;
	private javax.swing.JButton SaveJBtn;
	private com.toedter.calendar.JDateChooser ShipmentdateJDate;
	private javax.swing.JComboBox balanceProgramTypeJCB;
	private javax.swing.JTable facilityapprovedProductsJT;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private JLabel JLableSearch;
	private JTextField SearchproductsJTF;
}