package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.LabLineItem;

public class LabLineItemDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public LabLineItemDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public void addLineItem(List<LabLineItem> labLineItemList)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		try
		{
			for(LabLineItem l : labLineItemList)
			{
				session.insert("LabLineItem.addLabLineItem", l);
				session.commit();
			}
		}
		finally
		{
			session.close();
		}
	}
	public void getLineItem()
	{
		
	}
}
