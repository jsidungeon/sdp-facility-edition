/**
 * 
 */
package org.elmis.facility.tools;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;

/**
 * @author jbanda
 * 
 */
public class MessageDialog {

	/**
	 * 
	 */
	public MessageDialog() {
		// TODO Auto-generated constructor stub
		
		//UIManager UI=new UIManager();
		 //UI.put("OptionPane.background",new ColorUIResource(01,81,81));
		// UI.put("Panel.background",new ColorUIResource(01,81,81));
		
	}
	
	public int showDialog(Component classname, String message, String title,
			String jbutton1Text) {
		
		
		ImageIcon icon = new ImageIcon("src/elmis_images/hiv test result.png");
	  		
		
		Object[] options = {
				 jbutton1Text
						 };
		int ok = JOptionPane.showOptionDialog(classname,  message , title,
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				icon, options, options[0]);

	/*	Object[] options = {
				"<html><body bgcolor=\"#E6E6FA\"><b><br>" + jbutton1Text
						+ "<br><br><html>" };
		int ok = JOptionPane.showOptionDialog(classname, "<html><br><br>"
				+ message + "<br><br><br><html>", title,
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
				new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_images/hiv test result.png")), options, options[0]);
						
						*/

		return ok;
	}

	public int showDialog(Component classname, String message, String title,
			String jbutton1Text, String jbutton2Text) {
 
		ImageIcon icon = new ImageIcon("src/elmis_images/hiv test result.png");
		
		
		Object[] options = {
				 jbutton1Text
						,
				 jbutton2Text  };
		int ok = JOptionPane.showOptionDialog(classname,  message , title,
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,

				icon, options, options[1]);



		return ok;
	}

	public int showDialog(Component classname, String message, String title,
			String jbutton1Text, String jbutton2Text, String jbutton3Text) {

		ImageIcon icon = new ImageIcon("src/elmis_images/hiv test result.png");
		
		Object[] options = {
				 jbutton1Text
						,
				 jbutton2Text
						,
				 jbutton3Text  };

		int ok = JOptionPane.showOptionDialog(classname,  message , title,
				JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,

				icon, options, options[2]);

			


		return ok;
	}

}
