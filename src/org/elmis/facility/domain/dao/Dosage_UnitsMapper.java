package org.elmis.facility.domain.dao;

import org.elmis.facility.domain.model.Dosage_Units;
import org.elmis.facility.domain.model.Dosage_UnitsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Dosage_UnitsMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int countByExample(Dosage_UnitsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int deleteByExample(Dosage_UnitsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int insert(Dosage_Units record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int insertSelective(Dosage_Units record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    List<Dosage_Units> selectByExample(Dosage_UnitsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    Dosage_Units selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int updateByExampleSelective(@Param("record") Dosage_Units record, @Param("example") Dosage_UnitsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int updateByExample(@Param("record") Dosage_Units record, @Param("example") Dosage_UnitsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int updateByPrimaryKeySelective(Dosage_Units record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table dosage_units
     *
     * @mbggenerated Mon Oct 21 10:55:58 CAT 2013
     */
    int updateByPrimaryKey(Dosage_Units record);
}