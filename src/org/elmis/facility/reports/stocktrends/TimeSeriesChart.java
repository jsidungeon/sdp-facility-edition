package org.elmis.facility.reports.stocktrends;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Day;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleInsets;

public class TimeSeriesChart {

	/**
	 * the cost time series
	 */
	//public static TimeSeries s2;
	public static TimeSeries s1;
	public static int selectedProductId;
	
	// TimeSeriesCollection datasetColl;
	/**
	 * a method that calculates the costs
	 */
	// public static CalcCostSeries cSeries = new CalcCostSeries();

	public TimeSeriesChart() {

		//conn = NetworkMode.getConn();
		
	}

	//TODO if i need the date range
//	public JFreeChart createChart(final String chartTitleIn,java.util.Date startDate, java.util.Date endDate) {
		
		public JFreeChart createChart(final String chartTitleIn, final int selectedProductIdIn) {
			
			selectedProductId = selectedProductIdIn;

		// create the data set
		final XYDataset dataset = TimeSeriesChart.createDataset();

		//TODO if i need date range
		//final JFreeChart chart = ChartFactory.createTimeSeriesChart(chartTitleIn + " from "+ publicMethods.SDFDateFormatter2.format(startDate)+ " to "+ publicMethods.SDFDateFormatter2.format(endDate), // title
				
				final JFreeChart chart = ChartFactory.createTimeSeriesChart(chartTitleIn , // title
				"Date", // x-axis label
				"Consumption", // y-axis label
				dataset, // data
				true, // create legend?
				true, // generate tooltips?
				false // generate URLs?
				);

		chart.setBackgroundPaint(Color.white);

		final XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		final XYItemRenderer r = plot.getRenderer();
		if (r instanceof XYLineAndShapeRenderer) {
			final XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
			renderer.setBaseShapesVisible(true);
			renderer.setBaseShapesFilled(true);
		}
		final DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("MMM-yyyy"));

		return chart;
	}

	/**
	 * creat the time series for the sales
	 * 
	 * @param startDateIn
	 *            String
	 * @param endDateIn
	 *            String
	 * @return XYDataset
	 */
	
	//TODO if i need date range
	//public static XYDataset createDataset(java.util.Date startDateIn,java.util.Date endDateIn) {

		@SuppressWarnings("deprecation")
		public static XYDataset createDataset() {
		/**
		 * the sales time series
		 */

		 s1 = new TimeSeries("Current Year", Day.class);
		//final TimeSeries s2 = new TimeSeries("Last Year", Day.class);

	
		
		SqlSessionFactory factory = new MyBatisConnectionFactory().getSqlSessionFactory();

		SqlSession session = factory.openSession();
		StockControlCard	scc = new StockControlCard();
		List<StockControlCard> sccList = new ArrayList();
		try {

			//scc
			sccList = session.selectList("selectSccConsumptionTrend", selectedProductId);
			
			//("getProgramesSupported");
			
			//System.out.println(this.products.getPrimaryname() +" "+IssuingJD.selectedProductID);

			//this.populateProgramsTable(this.programsList);

		} finally {
			session.close();
		}
		
		
		
	 for(StockControlCard	stockControCard : sccList){
		 
		
		 
		 s1.add(new Day(stockControCard.getCreateddate()), stockControCard.getQty_isssued());
		 
		
		 
	 }

		/**
		 * the time series collection to send back to the chart
		 */
		final TimeSeriesCollection dataset = new TimeSeriesCollection();
		/**
		 * create the costs timeseries
		 */
		// TimeSeriesChart.s2 = TimeSeriesChart.cSeries.CalcCostSeries(
		// startDateIn, endDateIn);

		// dataset.addSeries(new AllExpsForChart().getS2(startDateIn,
		// endDateIn));
		dataset.addSeries(s1);
		//dataset.addSeries(s2);
		dataset.setDomainIsPointsInTime(true);

		return dataset;
	}
}
