package org.elmis.facility.domain.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.Reagent;

public class ReagentDao {

	private SqlSessionFactory sqlSessionFactory;

	public ReagentDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<Reagent> getReagents()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Reagent> list = session.selectList("Reagent.getAllReagents");
			return list;
		} finally {
			session.close();
		}
	}
	public void updateReagentStatus(Map<String, String> reagentsLinkedHashMap)
	{
		SqlSession session = sqlSessionFactory.openSession();
		try {

			for (Map.Entry<String, String> entry : reagentsLinkedHashMap.entrySet()) {
				System.out.println(entry.getKey()+" "+entry.getValue());
				Reagent reagent = new Reagent();
				reagent.setCommodityCode(entry.getKey());
				if (entry.getValue().equals("Y"))
					reagent.setCommodityStatus(true);
				else
					reagent.setCommodityStatus(false);

				session.update("Reagent.updateReagent",  reagent);
				session.commit();
			}
		}
		finally {
			session.close();
		}
	}
}
