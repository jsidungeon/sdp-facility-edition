package org.elmis.facility.network;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class NetworkMode {

	public static Connection c = null;

	public NetworkMode() {

	}

	public static Connection getConn() {

		try {

			try {

				Class.forName(System.getProperty("dbdriver"));
				c = DriverManager.getConnection(System.getProperty("dburl"),
						System.getProperty("dbuser"), System
								.getProperty("dbpassword"));

			} catch (ClassNotFoundException e) {

				e.printStackTrace();
			}

		} catch (SQLException e) {

			e.printStackTrace();

			return null;

		}

		return c;

	}

}
