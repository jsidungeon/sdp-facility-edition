package org.elmis.forms.hivtest;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.elmis.facility.utils.SdpStyleSheet;

public class ConfirmatoryTestSelectorJD extends JDialog implements ItemListener{

	private String results[] = {"Select a test result","R", "NR", "IN"};
	private JComboBox comboBox;
	private JButton btnSave;
	private String selectedTestResult;
	private boolean testSelected = false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConfirmatoryTestSelectorJD dialog = new ConfirmatoryTestSelectorJD(new JDialog(), true);
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public ConfirmatoryTestSelectorJD(javax.swing.JDialog parent, boolean modal) {
		super(parent, modal);
		setTitle("Confirmatory Test Results");
		setResizable(false);
		setBounds(100, 100, 391, 160);
		
		JPanel panel = new JPanel();
		SdpStyleSheet.configJPanelBackground(panel);
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Select Confirmatory Test Result");
		SdpStyleSheet.configOtherJLabel(lblNewLabel);
		lblNewLabel.setBounds(88, 11, 215, 14);
		panel.add(lblNewLabel);
		
		comboBox = new JComboBox(results);
		comboBox.addItemListener(this);
		comboBox.setBounds(88, 37, 152, 20);
		panel.add(comboBox);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				testSelected = false;
				dispose();
			}
		});
		btnCancel.setBounds(88, 87, 89, 23);
		panel.add(btnCancel);
		
		btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveSelectedConfirmatoryTestResult();
			}
		});
		btnSave.setEnabled(false);
		btnSave.setBounds(214, 87, 89, 23);
		panel.add(btnSave);
		setLocationRelativeTo(parent);

	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		int selectedTestIndex = comboBox.getSelectedIndex();
		if (selectedTestIndex != 0){
			
			btnSave.setEnabled(true);
		}
		else{
			btnSave.setEnabled(false);
		}
	}
	public boolean isTestResultSelected(){
		return testSelected;
	}
	public String getSelectedTestResult(){
		return selectedTestResult;
	}
	private void saveSelectedConfirmatoryTestResult(){
		selectedTestResult = ""+comboBox.getSelectedItem();
		testSelected = true;
		dispose();
	}
}
