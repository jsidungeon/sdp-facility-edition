DROP TABLE elmis_hiv_test_Purpose;
CREATE TABLE elmis_hiv_test_Purpose (id serial NOT NULL, purpose character varying(15) NOT NULL, description TEXT,
 CONSTRAINT elmis_hiv_test_Purpose_pkey PRIMARY KEY (id), CONSTRAINT elmis_hiv_test_Purpose_code_key UNIQUE (purpose));
INSERT INTO elmis_hiv_test_Purpose (purpose) VALUES ('TRAINING');
INSERT INTO elmis_hiv_test_Purpose (purpose) VALUES ('RESEARCH');