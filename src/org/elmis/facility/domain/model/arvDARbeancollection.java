package org.elmis.facility.domain.model;


import java.util.List;

import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
public class arvDARbeancollection {

	private List<Elmis_Dar_Transactions> arvfixeddarproductsList;
	
	public arvDARbeancollection(List<Elmis_Dar_Transactions> mylist){
		arvfixeddarproductsList = mylist;
	}

	public List<Elmis_Dar_Transactions> getArvfixeddarproductsList() {
		return arvfixeddarproductsList;
	}

	public void setArvfixeddarproductsList(List<Elmis_Dar_Transactions> arvfixeddarproductsList) {
		this.arvfixeddarproductsList = arvfixeddarproductsList;
	}
	
	
}
