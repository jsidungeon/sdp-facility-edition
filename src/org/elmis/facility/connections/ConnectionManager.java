package org.elmis.facility.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.ElmisProperties;
/**
 * 
 * @author Michael Mwebaze
 * Singleton class to manage connection pools
 */
public class ConnectionManager {

	private static ConnectionManager instance = null;
	
	private static final String USERNAME_HSQL = "dbuser";
	private static final String PASSWORD_HSQL = "dbpassword";
	private static String USERNAME_POSTGRESQL = System.getProperty("dbuser");
	private static String PASSWORD_POSTGRESQL = System.getProperty("dbpassword");
	private static final String CONN_STRING_HSQL = "jdbc:hsqldb:database/elmis_facility";
	//private static final String CONN_STRING_POSTGRES = "jdbc:postgresql://localhost:5432/elmis_facility";
	private static String CONN_STRING_POSTGRES = System.getProperty("dburl");
	private static final String CONN_STRING_MYSQ = "jdbc:mysql://localhost/ngamba";
	
	private DBType dbType = DBType.MYSQL;
	private Connection conn = null;
	
	private ConnectionManager()
	{
		
	}
	public static ConnectionManager getInstance()
	{
		if (instance == null)
		{
			instance = new ConnectionManager();
		}
		return instance;
	}
	public void testConnection(String domain, String username, String password, String database)
	{
		CONN_STRING_POSTGRES = "jdbc:postgresql://"+domain+":5432/"+database;
		PASSWORD_POSTGRESQL = password;
		
		USERNAME_POSTGRESQL = username;
	}
	/**
	 *set the type of database to connect too. At the moment supports HSQL & POSTGRES ONLY 
	 * @param dbType
	 */
	public void setDBType(DBType dbType)
	{
		this.dbType = dbType;
	}
	private boolean openConnection() throws SQLException, ClassNotFoundException
	{
		switch (dbType) {
		case HSQL:
			//Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(CONN_STRING_HSQL, USERNAME_HSQL, PASSWORD_HSQL);
			System.out.println("Connected to HSQL");
			return true;
			
		case POSTGRESQL:
			conn = DriverManager.getConnection(CONN_STRING_POSTGRES, USERNAME_POSTGRESQL, PASSWORD_POSTGRESQL);
			System.out.println("Connected to POSTGRES");
			return true;

		default:
			return false;
		}
	}
	
	public  Connection getConnection() throws SQLException, ClassNotFoundException
	{
		if (conn == null) {
			if (openConnection()) {
				return conn;
			}
			else
				return null;
		}
		return conn;
	}
	
	public void close()
	{
		try {
			conn.close();
			conn = null;
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
