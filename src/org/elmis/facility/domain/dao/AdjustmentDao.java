package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.model.Adjustment;

public class AdjustmentDao {
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public void saveHivAdjustments(List<Adjustment> adjustmentList)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		try
		{
			for (Adjustment a : adjustmentList)
				session.insert("Adjustment.insert_hiv_adjustments", a);
			session.commit();
		}
		finally{
			session.close();
		}
	}
	public void saveArvAdjustments(List<Adjustment> adjustmentList)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		try
		{
			for (Adjustment a : adjustmentList)
				session.insert("Adjustment.insert_arv_adjustments", a);
			session.commit();
		}
		finally{
			session.close();
		}
		//call adjustment file to complete addition or reduction of balances.
		
		
		
	}
	public Map<String, Integer>  getHivProductBalances(int siteId){
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Integer> map = new HashMap<>();
		try
		{
			List<Map<String, Object>> productBalances = session.selectList("HivTestResult.hiv_product_balances", siteId);
			for (Map<String, Object> p : productBalances){
				map.put(p.get("productcode").toString(), (Integer)p.get("product_balance"));
			}
			return map;
		}
		finally{
			session.close();
		}
	}
}
