/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reports.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author mmwebaze
 *
 */
public class ReportStorageManager {

	private String folderDir = System.getProperty("user.dir");
	private String reportFolder = "Report-Folder";

	public boolean isFolderAvailable()
	{
		Path folder = Paths.get(folderDir+"/"+reportFolder);

		if (Files.exists(folder,  LinkOption.NOFOLLOW_LINKS))
			return true;
		else
			return false;
	}
	public void createReportFolder()
	{
		Path pathToReportFolder = Paths.get(folderDir, reportFolder);
		try {
			Files.createDirectories(pathToReportFolder);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getReportFolder()
	{
		return folderDir+"/"+reportFolder;
	}
	
	public void emptyReportFolder() throws IOException
	{
		File directory = new File(getReportFolder());
		for (File file: directory.listFiles())
			file.delete();
	}
	public static void main(String[] g)
	{
		ReportStorageManager r = new ReportStorageManager();
		try {
			r.emptyReportFolder();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
