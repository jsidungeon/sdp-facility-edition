package org.elmis.facility.main.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Date;

import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.JLabel;

import org.elmis.facility.reporting.generator.ReportGenerator;


public class ProgressBarRangeJD extends JDialog implements Runnable{
	private int siteId;
	private Date date1;
	private Date date2;

	/**
	 * Create the dialog.
	 */
	public ProgressBarRangeJD(String message, Date date1, Date date2, int siteId) {
		setTitle("Please wait processing.....");
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		this.siteId = siteId;
		this.date1 = date1;
		this.date2 = date2;
		setBounds(100, 100, 424, 148);
		setLocationRelativeTo(null);
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		getContentPane().add(progressBar, BorderLayout.SOUTH);
		
		JLabel messageLable = new JLabel(message);
		getContentPane().add(messageLable, BorderLayout.CENTER);
		//new Thread(new ReportThread(this, siteId)).start();

	}

	@Override
	public void run() {
		ReportGenerator reportGenerator = new ReportGenerator();
		reportGenerator.generateHivDailyActivityRegisterRange(date1,date2,siteId);
		dispose();
		
	}

}
