package org.elmis.facility.reporting.model;

public class RandrArdDrug {

	private int rrId;
	public int getRrId() {
		return rrId;
	}
	public void setRrId(int rrId) {
		this.rrId = rrId;
	}
	private String regimenId;
	private String drugProduct;
	private String unit;
	private String beginningBal;
	private String totalReceived;
	private String qtyDispensed;
	private String lossAdjustment;
	private String phyCount;
	private String amc;
	private String maxQty;
	private String orderQty;
	
	public String getRegimenId() {
		return regimenId;
	}
	public void setRegimenId(String regimenId) {
		this.regimenId = regimenId;
	}

	public String getDrugProduct() {
		return drugProduct;
	}
	public void setDrugProduct(String drugProduct) {
		this.drugProduct = drugProduct;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getBeginningBal() {
		return beginningBal;
	}
	public void setBeginningBal(String beginningBal) {
		this.beginningBal = beginningBal;
	}
	public String getTotalReceived() {
		return totalReceived;
	}
	public void setTotalReceived(String totalReceived) {
		this.totalReceived = totalReceived;
	}
	public String getQtyDispensed() {
		return qtyDispensed;
	}
	public void setQtyDispensed(String qtyDispensed) {
		this.qtyDispensed = qtyDispensed;
	}
	public String getLossAdjustment() {
		return lossAdjustment;
	}
	public void setLossAdjustment(String lossAdjustment) {
		this.lossAdjustment = lossAdjustment;
	}
	public String getPhyCount() {
		return phyCount;
	}
	public void setPhyCount(String phyCount) {
		this.phyCount = phyCount;
	}
	public String getAmc() {
		return amc;
	}
	public void setAmc(String amc) {
		this.amc = amc;
	}
	public String getMaxQty() {
		return maxQty;
	}
	public void setMaxQty(String maxQty) {
		this.maxQty = maxQty;
	}
	public String getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(String orderQty) {
		this.orderQty = orderQty;
	}
}
