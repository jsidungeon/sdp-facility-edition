package org.elmis.facility.utils;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

public class SdpSingleApplicationInstance {
	
	public static boolean singleApplicationInstance() throws IOException{
		RandomAccessFile randomFile=new RandomAccessFile("./singleinstance.xml.","rw");
		FileChannel channel=randomFile.getChannel();
		if(channel.tryLock()==null) {
			return false;
		}
		return true;
	}

}
