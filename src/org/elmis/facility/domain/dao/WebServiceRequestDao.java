package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.model.WebServiceRequest;

public class WebServiceRequestDao {
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<WebServiceRequest> getWebServiceRequests(){
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("WebServiceRequest.get_requests");
		}
		finally{
			session.close();
		}
	}
}
