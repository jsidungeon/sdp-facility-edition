package org.elmis.forms.regimen;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.domain.dao.RegimenMapper;
import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.ElmisRegimenCategory;
import org.elmis.facility.domain.model.ElmisRegimenProduct;
import org.elmis.forms.regimen.dao.RegimenDao;
import org.elmis.forms.regimen.util.ComboBoxItem;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.border.EtchedBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DefineRegimen extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel pnlMain = new JPanel();
	private JTable tblProducts;
	private JTextField txtRegimenName;
	JComboBox<ComboBoxItem> cboRegimenCategories;
	final JCheckBox chckbxActive;
	private JScrollPane scrollPane;
	private JTable table;
	private DefineRegimen _this = this;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DefineRegimen dialog = new DefineRegimen(null, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void populateRegimensTable(){
		DefaultTableModel model = new DefaultTableModel(null, new String[] {
				"Name", "Line", "Active" }) {
			private static final long serialVersionUID = 1L;
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int c) {
				switch (c) {
				case 0:
					return Boolean.class;
				default:
					return String.class;
				}
			}
		};
	}

	@SuppressWarnings("serial")
	private void populateProductsTable() {
		DefaultTableModel model = new DefaultTableModel(null, new String[] {
				"", "Code", "Primary Name" , "productid"}) {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int c) {
				switch (c) {
				case 0:
					return Boolean.class;
				default:
					return String.class;
				}
			}
		};

		List<Map<String, Object>> prodList = fetchProductsForFacility();
		for (Map<String, Object> map : prodList) {
			model.addRow(new Object[] { false, map.get("code"),
					map.get("primaryname") , map.get("productid")});
		}
	/*	TableColumnModel m=tblProducts.getColumnModel();
		TableColumn col=m.getColumn(3);
		m.removeColumn(col);*/
		pnlMain.setLayout(null);


		JButton btnDefineRegimen = new JButton("Save Regimen");
		btnDefineRegimen.setBounds(29, 467, 163, 23);
		pnlMain.add(btnDefineRegimen);
		btnDefineRegimen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

	/*			int dialogButton = JOptionPane.YES_NO_OPTION;
				if (JOptionPane.showConfirmDialog (null, "?","Warning",dialogButton) == JOptionPane.YES_OPTION){
	
				}*/
				//------------
				ElmisRegimen regimen = new ElmisRegimen();
				regimen.setActive(chckbxActive.isSelected());
				regimen.setName(txtRegimenName.getText().trim());
				regimen.setCode("");
				regimen.setCategoryId(((ElmisRegimenCategory)(((ComboBoxItem)cboRegimenCategories.getSelectedItem()).getValue())).getId());
				RegimenDao dao = RegimenDao.getInstance();
				dao.insert(regimen);
				System.out.println("id = " + regimen.getId());
				
				for (int i = 0; i < tblProducts.getRowCount(); i++) {
					if (Boolean
							.valueOf(tblProducts.getValueAt(i, 0).toString())){
						System.out.println(tblProducts.getValueAt(i, 0) + ","
								+ tblProducts.getValueAt(i, 1) + ","
								+ tblProducts.getValueAt(i, 2) +","+ tblProducts.getValueAt(i, 3));
						ElmisRegimenProduct regProduct = new ElmisRegimenProduct();
						regProduct.setRegimenId(regimen.getId());
						regProduct.setProductId((Integer) tblProducts.getValueAt(i, 3));
						dao.insertRegimenProduct(regProduct);
					}
				}
				JOptionPane.showMessageDialog(_this, "Regimen Saved Successfully!");
			}
		});
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(29, 259, 484, 185);
		pnlMain.add(scrollPane);
		
				tblProducts = new JTable();
				scrollPane.setViewportView(tblProducts);
				tblProducts.setModel(model);
				
						
						tblProducts.getColumnModel().getColumn(1).setPreferredWidth(158);
						tblProducts.getColumnModel().getColumn(2).setPreferredWidth(193);
						hideColumn(tblProducts.getColumnModel().getColumn(3));
	}
	
	private void hideColumn(TableColumn column){
		column.setMinWidth(0);
		column.setMaxWidth(0);
		column.setPreferredWidth(0);
	}

	private void setUpCategoriesCombo() {
		RegimenDao dao = RegimenDao.getInstance();
		List<ElmisRegimenCategory> regCategories = dao.fetchRegimenCategories();
		for (ElmisRegimenCategory elmisRegimenCategory : regCategories) {
			cboRegimenCategories
					.addItem(new ComboBoxItem(elmisRegimenCategory));
		}

	}

	
	private List<Map<String, Object>> fetchProductsForFacility() {
		String resource = "SqlMapConfig.xml";
		SqlSessionFactory sqlMapper = null;
		Reader reader = null;

		try {
			reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader,
					System.getProperties());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SqlSession session = sqlMapper.openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		List<Map<String, Object>> prodList = null;
		try {

			prodList = mapper.selectProductsForFacility("ARV", "MSL");
			// System.out.println(prodList);

			for (Map<String, Object> map : prodList) {
				for (String key : map.keySet()) {
					/*
					 * Object value = map.get(key); System.out.println( key +
					 * " = " + value);
					 */
					//System.out.println(map.get("primaryname"));
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodList;
	}

	private Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prop;
	}

	/**
	 * Create the dialog.
	 */
	public DefineRegimen(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setTitle("Define Regimen");
		setResizable(false);
		setBounds(100, 100, 564, 546);
		getContentPane().setLayout(new BorderLayout());
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnlMain, BorderLayout.CENTER);
		{
			populateProductsTable();
		}
		
		JSeparator separator = new JSeparator();
		separator.setBounds(438, 60, 1, 2);
		pnlMain.add(separator);
		
		JPanel panel = new JPanel();
		panel.setBounds(29, 56, 484, 185);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlMain.add(panel);
		panel.setLayout(null);
		
				cboRegimenCategories = new JComboBox();
				cboRegimenCategories.setBounds(20, 101, 280, 20);
				setUpCategoriesCombo();
				panel.add(cboRegimenCategories);
				{
					JLabel lblNewLabel = new JLabel("Regimen Line");
					lblNewLabel.setBounds(20, 84, 132, 14);
					panel.add(lblNewLabel);
				}
				
						chckbxActive = new JCheckBox("Active");
						chckbxActive.setBounds(20, 147, 97, 23);
						panel.add(chckbxActive);
						
								txtRegimenName = new JTextField();
								txtRegimenName.setBounds(20, 42, 280, 20);
								panel.add(txtRegimenName);
								txtRegimenName.setColumns(10);
								
										JLabel lblRegimenName = new JLabel("Regimen Name");
										lblRegimenName.setBounds(20, 21, 132, 14);
										panel.add(lblRegimenName);
										
										table = new JTable();
										table.setBounds(404, 272, -365, 54);
										pnlMain.add(table);
										
										JButton btnClose = new JButton("Close");
										btnClose.addMouseListener(new MouseAdapter() {
											@Override
											public void mouseClicked(MouseEvent arg0) {
												_this.dispose();
											}
										});
										btnClose.setBounds(213, 467, 139, 23);
										pnlMain.add(btnClose);
						chckbxActive.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								System.out.println(chckbxActive.isSelected());
							}
						});
				cboRegimenCategories.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ComboBoxItem item = (ComboBoxItem) cboRegimenCategories
								.getSelectedItem();
						ElmisRegimenCategory category = (ElmisRegimenCategory) item
								.getValue();
						System.out.println(category.getId() + " , "
								+ category.getName());
					}
				});
				if(!this.isVisible())this.setVisible(true);
	}
}
