package org.elmis.forms.stores.receiving;

import java.awt.Color;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;

public class checkstorePCdate {
	
	FacilityProductsDAO Facilityproductsmapper = null;
	List<Elmis_Stock_Control_Card>storephyscalcountcheckList = new LinkedList();
	private String  prodCode ;
	public checkstorePCdate(String productcode){
		prodCode = productcode;
		
		createdateObjects();
	}
	
		
	
	public static Date toDate(java.sql.Timestamp timestamp) {
		long millisec = timestamp.getTime() + (timestamp.getNanos() / 1000000);
		return new Date(millisec);

	}
	
public void createdateObjects(){
	
	Facilityproductsmapper = new FacilityProductsDAO();
	Timestamp productdeliverdate = null;
	String oldDateString;
	String mydate = new java.util.Date().toString();
	String newDateString;
	final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
	// final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
	final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
	oldDateString = mydate;

	SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
	java.util.Date d;
	d = null;
	try {
		d = sdf.parse(oldDateString);
	} catch (ParseException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	sdf.applyPattern(NEW_FORMAT);
	newDateString = sdf.format(d);
	productdeliverdate = Timestamp.valueOf(newDateString);

	// end create time stamp

	Date Startdate = new Date();
	Date Enddate = new Date();
	if (productdeliverdate != null) {

		// convert to date value
		Date convertdate = toDate(productdeliverdate);
		// date split up methods
		Calendar cal = Calendar.getInstance();
		cal.setTime(convertdate);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		if (!(day == 1)) {
			day = 1;
			cal.set(Calendar.MONTH, month);
			cal.set(Calendar.DATE, day);
			cal.set(Calendar.YEAR, year);
			Startdate = cal.getTime();
		}
		Enddate = toDate(productdeliverdate);
	}
	
	storephyscalcountcheckList = Facilityproductsmapper.docheckifPCisthelasttransactionoftheMonth(Startdate, Enddate, prodCode);
	

	for( Elmis_Stock_Control_Card e  : storephyscalcountcheckList ){
		if(!(e.getIssueto_receivedfrom().equalsIgnoreCase("Physical Count"))){
			//update physical count count_carried out to false for that product we the last entry made
			Facilityproductsmapper.doupdatestorephysicalcountBoolean(e.getProductcode());
		}else{
			Facilityproductsmapper.doupdatestorephysicalcountBoolean(e.getProductcode());
		}
	}
	


}
}
