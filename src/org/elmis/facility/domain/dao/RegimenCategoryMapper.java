package org.elmis.facility.domain.dao;

import java.util.List;

import org.elmis.facility.domain.model.ElmisRegimenCategory;

public interface RegimenCategoryMapper {
	
	List<ElmisRegimenCategory> selectAllCategories();
}
