/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.receiving;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.MutableComboBoxModel;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.dao.FacilityStoreProductBalanceDAO;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.main.gui.ProgressBarJD;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.jdesktop.swingx.table.DatePickerCellEditor;

import com.oribicom.tools.TableModel;


/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class ReceiveProductsJD extends javax.swing.JDialog {
    
	private int         col;
	private int         rowz;
	
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;
	TableColumnAligner tablecolumnaligner; 
	private String mydateformat = "yyyy-MM-dd"; // hh:mm:ss";
	public Timestamp productdeliverdate;
	public Timestamp selecteddate;
	public String oldDateString;
	public String newDateString;
	public static String barcodeScanned;
	public static String barcodeproductinsert;
	public static VW_Program_Facility_ApprovedProducts selectedProduct;
	private ProgressBarJD showmyProgressbar ;
	private static Elmis_Stock_Control_Card    storeqtyproduct;
	private static Products searchProduct;
	private List<Elmis_Stock_Control_Card> qtyproducts = new LinkedList();
	public static String searchprogramcode =  null;
    Double ProdQty = null;
	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
			"Product Id", "Product Code", "Product Name", "Generic Strength",
			"Expiry Date","Quantity Received" , "Remarks" };
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "","", "", ""};
	private static final String[] columns_barcodefacilityApprovedproducts = {
		"Product Id", "Product Code", "Product Name", "Generic Strength",
		"Expiry Date","Quantity Received" , "Remarks" };
private static final Object[] defaultv_barcodefacilityapprovedproducts = { "", "",
		"", "","", "", "" };
	
	private static final int rows_fproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false,true, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 7) {

				return getValueAt(0, 7).getClass();
			}
			if (columnIndex == 5) {

				 
			}
			if (columnIndex == 4) {

				 
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/
		
		
		

	};
	
	public static TableModel tableModel_barcodefproducts = new TableModel(
			columns_barcodefacilityApprovedproducts,
			defaultv_barcodefacilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false,true, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 7) {

				return getValueAt(0, 7).getClass();
			}
			if (columnIndex == 5) {

				  
			}
			
			if (columnIndex == 4) {

				 
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};
	
	
	
	
	
	public static TableModel tableModel_searchfproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false,true, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				/*if (columnIndex == 2) {

					return getValueAt(0, 2).getClass();
				}*/

				if (columnIndex == 7) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,7).getClass();
					// }	
				}
				if (columnIndex == 5) {

					
				}
				if (columnIndex == 4) {

					 
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		/*public Class getColumnClass(int c) 
		{     
		for(int rowIndex = 0; rowIndex < data.size(); rowIndex++)
		{
		    Object[] row = data.get(rowIndex);
		    if (row[c] != null) {
		        return getValueAt(rowIndex, c).getClass();
		    }   
		}
		return String.class;
		}*/

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	
	
		
	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//List<Shipped_Line_Items>  shippedItemsList =  new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator elmistockcontrolcarditerator = elmisstockcontrolcardList
			.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	FacilityProductsDAO Facilityproductsmapper = null;
	FacilityStoreProductBalanceDAO facilityproductsbalancemapper = null;
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	public static Boolean cansave = false;
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer Productid = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private List<Character> charList = new LinkedList();
	private char letter;
	private String searchText = "";
	private String SearchResult = "";
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	//List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//private VW_Systemcalculatedproductsbalance facilitysccProducts;
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();
 private    TableColumn column1;
	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public ReceiveProductsJD(java.awt.Frame parent, boolean modal,String selectedprogramCode ) {
		super(parent, modal);
		setForeground(Color.WHITE);
		setFont(UIManager.getFont("ColorChooser.font"));
		initComponents();
		searchprogramcode = selectedprogramCode;
		this.setSize(1000, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		ShipmentdateJDate.setPreferredSize(new Dimension(150, 30));
		//ShipmentdateJDate.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{ShipmentdateJDate.getCalendarButton()}));
		SearchproductssccJTF.setPreferredSize(new Dimension(150, 30));
		refnojTextField.setPreferredSize(new Dimension(150, 30));
	  
	}

	

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		ShipmentdateJDate = new com.toedter.calendar.JDateChooser();
		jLabel2 = new javax.swing.JLabel();
		refnojTextField = new javax.swing.JTextField();
		refnojTextField.addFocusListener(new FocusAdapter() {
		
			public void focusLost(java.awt.event.FocusEvent evt) {
				refnojTextFieldfocusLost(evt);
				
				
			}
			
		});
		jScrollPane1 = new javax.swing.JScrollPane();
		facilityapprovedProductsJT = new javax.swing.JTable();
		CancelJBtn = new javax.swing.JButton();
		SaveJBtn = new javax.swing.JButton();
	
		SearchproductssccJTF = new javax.swing.JTextField();
		SearchproductssccJTF.addFocusListener(new FocusAdapter() {
		
			public void focusLost(java.awt.event.FocusEvent evt) {
				SearchproductssccJTFfocusLost(evt);
			}

			
		});
		jLabel4 = new javax.swing.JLabel();
		
		facilityapprovedProductsJT.setDefaultRenderer(Object.class, new CustomModel());
		//AdjustmentfacilityapprovedProductsJT.addMouseListener(new CustomListener());
		facilityapprovedProductsJT.addKeyListener(new CustomListener1());
		

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Products Receipts");
		
	  // this.getLayeredPane().getComponent(0) .setFont(new Font("Ebrima",Font.BOLD,30));
		
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		jLabel1.setFont(new Font("Ebrima", Font.BOLD, 18));
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Shipment date");

		ShipmentdateJDate.setDateFormatString(mydateformat);
		ShipmentdateJDate.setFont(new Font("Ebrima", Font.PLAIN, 20));
		ShipmentdateJDate
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ShipmentdateJDatePropertyChange(evt);
					}
				});

		jLabel2.setFont(new Font("Ebrima", Font.BOLD, 18));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Despatch Number");

		refnojTextField.setFont(new Font("Ebrima", Font.PLAIN, 18));

		facilityapprovedProductsJT.setFont(new java.awt.Font("Ebrima", 0, 20));
		facilityapprovedProductsJT.setModel(tableModel_fproducts);
		facilityapprovedProductsJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						facilityapprovedProductsJTMouseClicked(evt);
					}
				});
		facilityapprovedProductsJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						facilityapprovedProductsJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(facilityapprovedProductsJT);

		CancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBtn.setText("Close");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save  ");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		SearchproductssccJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				SearchproductssccJTFKeyTyped(evt);
			}
           
			public void keyReleased(java.awt.event.KeyEvent evt) {
				SearchproductssccJTFkeyReleased(evt);
			}
		});

		jLabel4.setFont(new Font("Ebrima", Font.BOLD, 18));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Product Name Search");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 820, Short.MAX_VALUE)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(ShipmentdateJDate, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel1))
							.addPreferredGap(ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(jLabel2)
								.addComponent(refnojTextField, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE))
							.addGap(33)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(jLabel4)
								.addComponent(SearchproductssccJTF, GroupLayout.PREFERRED_SIZE, 313, GroupLayout.PREFERRED_SIZE)))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(CancelJBtn)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(SaveJBtn)))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(34)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(jLabel4)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(SearchproductssccJTF, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
							.addComponent(jLabel2)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(refnojTextField, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
							.addComponent(jLabel1)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(ShipmentdateJDate, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addGap(32)
					.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(CancelJBtn)
						.addComponent(SaveJBtn))
					.addContainerGap(95, Short.MAX_VALUE))
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 550, Short.MAX_VALUE)
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents
	
	
	 public class CustomModel extends DefaultTableCellRenderer {

	        /**
	         * 
	         */
	        private static final long   serialVersionUID    = 1L;

	        @Override
	        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	            JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	            Color c = Color.WHITE;
	            if (isSelected && row == rowz & column == col)
	                c = Color.GREEN;
	            label.setBackground(c);
	            return label;
	        }
	    }

	
public class CustomListener1 extends KeyAdapter  {
	 public void keyTyped(KeyEvent e) {   
		/* if(e.getKeyCode() == KeyEvent.VK_ENTER){
			 rowz = AdjustmentfacilityapprovedProductsJT.getSelectedRow();
	            col = AdjustmentfacilityapprovedProductsJT.getSelectedColumn();
	            // Repaints JTable
	            AdjustmentfacilityapprovedProductsJT.repaint();
        }*/
		// if(e.getKeyCode() == KeyEvent.VK_TAB){
			 rowz = facilityapprovedProductsJT.getSelectedRow();
	         col = facilityapprovedProductsJT.getSelectedColumn();
	            // Repaints JTable
	           facilityapprovedProductsJT.repaint();
        //}
      // Select the current cell
      
  }
}
	
	
	
	

	private void SearchproductssccJTFfocusLost(FocusEvent evt) {
		// TODO Auto-generated method stub
	//refnojTextField.requestFocusInWindow();
	
	}	
	
	protected void refnojTextFieldfocusLost(FocusEvent evt) {
		// TODO Auto-generated method stub
		
		if(refnojTextField.getText().equals("")){
			
		/*	JOptionPane.showMessageDialog(this, "Dispatch Number is required!",
					"Saving Failed", JOptionPane.INFORMATION_MESSAGE);*/
		
		//refnojTextField.requestFocusInWindow();
			
		}
		this.facilityapprovedProductsJT.requestFocusInWindow();	
	}



	@SuppressWarnings("static-access")
	private void SearchproductssccJTFkeyReleased(java.awt.event.KeyEvent evt){
		// TODO add your handling code here:
               
				//last event is enter after barcode scan event
				if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

					/*javax.swing.JOptionPane.showMessageDialog(null,
							"Check if went inside HERE");*/
					/*    letter = evt.getKeyChar();

					StringBuilder s = new StringBuilder(charList.size());
					
					charList.add(letter);

					for (char c : charList) {

						s.append(c);

					
					}*/

					//if((s.substring(0,1).matches("[0-9]"))){ 
					//get the scanned barcode number
					this.barcodeScanned = SearchproductssccJTF.getText();

					/*selectedProduct = Facilityproductsmapper
							.barcodeselectProduct(barcodeScanned);*/
					//Get ARV facility Approved products only iterate through them to get scanned product
					storeqtyproduct = new Elmis_Stock_Control_Card();
					callfacility = new FacilitySetUpDAO();
					Facilityproductsmapper = new FacilityProductsDAO();
					facilityproductsbalancemapper = new FacilityStoreProductBalanceDAO();
					if(!(searchprogramcode == null)){
						this.facilityprogramcode = searchprogramcode;}
					
					//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
					//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

					facility = callfacility.getFacility();

					facilitytype = callfacility.selectAllwithTypes(facility);
					typeCode = facilitytype.getCode();

					productsList = Facilityproductsmapper
							.dogetcurrentFacilityApprovedProducts(facilityprogramcode,
									typeCode);
					arvsearchproductsList = productsList;

					for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

						//System.out.println(p.getCode());

					}

					for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {
						try {
							if (p.getCode().contains(barcodeScanned)) {
								selectedProduct = p;
                                System.out.println("found the product code"); 
								//javax.swing.JOptionPane.showMessageDialog(null,
										//selectedProduct.getManufacturerbarcode());
							}

						} catch (NullPointerException e) {
							e.getMessage();
						}
					}

                     //product found via barcode scanner
					
					if (!(this.selectedProduct.getCode().equals(""))) {

						//if qty in stock is zero , show message dialog 
						//qty = this.selectedProduct.getProductQty();

						qtyproducts = this.facilityproductsbalancemapper.dogetcurrentstoreproductbalances(selectedProduct.getCode());
								

						//javax.swing.JOptionPane.showMessageDialog(null, qty
								//+ "Product QUANTITY");

						//if 
						if (!(qtyproducts.isEmpty())){
							try{
                            for(Elmis_Stock_Control_Card e :qtyproducts ){
							//selectedProduct.setProductqty(qtyproducts);
                            	if(!(e.getPrimaryname().equals(null))){
                            		storeqtyproduct = e;
                            	//storeqtyproduct.setProductcode(e.getProductcode());
							// show the number pad to get quantity
							new receiveNumberPadJD(javax.swing.JOptionPane
									.getFrameForComponent(this), true,
									this.storeqtyproduct);
                            	}
                            }
							 }catch(java.lang.NullPointerException n){
	                            	n.getMessage();
	                            }
						} else {

							javax.swing.JOptionPane
									.showMessageDialog(
											this,
											"<html><body bgcolor=\"#E6E6FA\"><b><p><p><p><p><font size=\"4\" color=\"red\">"
													+ ""
													+ this.selectedProduct
															.getPrimaryname()
													+ " is currently stocked out. </font><p><p><p><p></b></body></html>");
							//receive new  product 
							new receiveNumberPadJD(javax.swing.JOptionPane
									.getFrameForComponent(this), true,
									this.selectedProduct);
						}

						SearchproductssccJTF.setText("");

						if (SearchproductssccJTF.getText().equals("")) {

							charList.clear();
						}
						SearchproductssccJTF.requestFocusInWindow();

					} else {

						javax.swing.JOptionPane.showMessageDialog(null,
								"Product not found" + "SCANNED BAR CODE!!");
					}

					SearchproductssccJTF.setText("");

				}

	}
	
	@SuppressWarnings("static-access")
	private void callbackAlphaproductlist(){
		//get ARV list 
		try {
			//if (!(this.SearchproductssccJTF.getText().toString().equals(""))) {
				
				System.out.println(this.SearchproductssccJTF.getText().toString() +"WHAT IS SEARCH??");

				if(this.barcodeproductinsert == "BARCODE_CALL"){
					this.tableModel_barcodefproducts.clearTable();
					
					//this.tableModel_barcodefproducts.fireTableDataChanged();
				}
			
					this.PopulateProgram_ProductTable(
							arvsearchproductsList);
			//this.refnojTextField.setText("");		
            //this.refnojTextField.requestFocusInWindow();
				
			//}

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}


	
	
	
	@SuppressWarnings("static-access")
	private void SearchproductssccJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
		
		// TODO add your handling code here:

		System.out.println(this.arvsearchproductsList.size() + "list not null");

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
			searchText = this.SearchproductssccJTF.getText();
			 System.out.println("I detedcted a back space ah! did you call me ??");
	           //re populate table with original list 
			 if(this.SearchproductssccJTF.getText().toString().length() == 0){
	           this.callbackAlphaproductlist();
			 }
			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}

			for (char c : charList) {

				s.append(c);

			}
		} else {

			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			//get ARV list 
			try {
				if (!(this.SearchproductssccJTF.getText() == "")) {

					if (!(s.substring(0, 1).matches("[0-9]"))) {

						callfacility = new FacilitySetUpDAO();
						Facilityproductsmapper = new FacilityProductsDAO();
                        
						if(!(searchprogramcode == null)){
						this.facilityprogramcode = searchprogramcode;}
						//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
						//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

						facility = callfacility.getFacility();

						facilitytype = callfacility
								.selectAllwithTypes(facility);
						typeCode = facilitytype.getCode();

						productsList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
						arvsearchproductsList = productsList;

						/*for(VW_Program_Facility_ApprovedProducts p: arvsearchproductsList ){
							
							if(p.getPrimaryname().contains(s)){
								this.arvproductsList.add(p);
							}
						}*/

						for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

							if (p.getPrimaryname().toLowerCase().contains(s.toString().toLowerCase())) {
								this.arvproductsList.add(p);
							}
						}

						//	System.out.println("Delete " + s);
						//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						this.searchPopulateProgram_ProductTable(
								arvproductsList, "STRING_FOUND");

					}
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}

		}

		// searchText += Character.toString(letter);
		/*	charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			try {
				//check if char is a digit and escape the loop.
				if(!(this.searchjTextField.getText() == "")){
				if(!(s.substring(0,1).matches("[0-9]"))){ 

				callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper.dogetcurrentFacilityApprovedProducts(facilityprogramcode, typeCode);
					
				arvsearchproductsList = productsList;

				for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

					if (p.getPrimaryname().contains(s)) {
						this.arvproductsList.add(p);
					}
				}
				//System.out.println("Delete " + s);
				//arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

				searchPopulateProgram_ProductTable(arvproductsList,
						"STRING_FOUND");
				
				}
				
			}
			} catch (NullPointerException e) {
				e.getMessage();
			}*/
	}
	
	@SuppressWarnings("unchecked")
	private void searchPopulateProgram_ProductTable(List fapprovProducts,
			String mysearch) {

		try {

			this.SearchResult = mysearch;

			/*	callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProducts(facilityprogramcode);*/
			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}

			this.facilityapprovedProductsJT
					.setModel(tableModel_searchfproducts);
			tableModel_searchfproducts.clearTable();

			fapprovedproductsIterator = fapprovProducts.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getProductid();

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength();
				
			 	TableColumn column1 = facilityapprovedProductsJT
						.getColumnModel().getColumn(4);
			//	column1.setCellRenderer(new JDateChooserRenderer());
				column1.setCellEditor(new DatePickerCellEditor());
				//Set the value of the entered product quantity 
				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}
				tableModel_searchfproducts.insertRow(cols);
				//tableModel_searchfproducts.fireTableDataChanged();
				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		int ok = new arvmessageDialog()
		.showDialog(
				this,
				"Confirm Close \n\n",
				"Form has un saved data",
				"YES",
				"NO");
		if (ok == JOptionPane.YES_OPTION) {
		this.dispose();
		}
		if (ok == JOptionPane.NO_OPTION) {
			//do nothing and save data on form
			}
	}

	private void ShipmentdateJDatePropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		try {
			
			 Calendar currentDate = Calendar.getInstance(); //Get the current date
			 SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MMM-dd hh:mm:ss.S"); //format it as per your requirement
			 String dateNow = formatter.format(currentDate.getTime());
			 System.out.println("Now the date is :=>  " + dateNow);

			String mydate = this.ShipmentdateJDate.getDate().toString();
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
				
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			if(d.after(currentDate.getTime())){
				System.out.println("Test date diff ");
				productdeliverdate = Convertdatestr(currentDate.getTime().toString());
				JOptionPane.showMessageDialog(this, "Receipt Date cannnot be in the future",
						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				this.ShipmentdateJDate.setDate(new Date());
			}
			System.out.println(newDateString);
			
			

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate + "Error source 2014");
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			selecteddate = Timestamp.valueOf(newDateString);
			System.out.println(selecteddate + "for Reveive EXPIRY");
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
		System.out.println(selecteddate + "same expiry date??");
		return selecteddate;
		

	}
	
	
	
	private boolean checktableCellinput(String s){
		
		 try { 
		        Integer.parseInt(s); 
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    // only got here if we didn't return false
		    return true;
}
	
	private String checkselectedDate(Calendar mdate){
	//	Date mydate = mdate;
		String rdate = null;
		try {
            
			Date testdate = new Date();
			Calendar currentDate = Calendar.getInstance(); //Get the current date
			 SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
			 String dateNow = formatter.format(currentDate.getTime());
			  rdate = formatter.format(currentDate.getTime()); 
			 System.out.println("Now the date is :=>  " + dateNow);
			
				SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy"); 
				
				
			//String sdate = formatter.format(mdate);
			
			//System.out.println(sdate);
			//this.registerdate = mydate;
			@SuppressWarnings("static-access")
			Calendar d = mdate.getInstance();
			if((d.after(currentDate.getTime()) || (d.equals(currentDate.getTime())))){
				
				JOptionPane.showMessageDialog(this, "Expiry  Date cannnot be in the past or today",

						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
			}
						} catch (NullPointerException e) {
			e.getMessage();
		}catch(IllegalArgumentException i){
			i.getLocalizedMessage();
		}
		
		return  rdate;
	

	}
	private void facilityapprovedProductsJTPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		facilityapprovedProductsJT.getColumnModel().getColumn(0).setMinWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setMaxWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setWidth(0);
		
		
		
		
		 Calendar currentDate = Calendar.getInstance(); //Get the current date
		 SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
		 String dateNow = formatter.format(currentDate.getTime());
		 System.out.println("Now the date is :=>  " + dateNow);
		
	
		/*facilityapprovedProductsJT.getColumnModel().getColumn(5).setMinWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(5).setMaxWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(5).setWidth(0);*/


		if ("tableCellEditor".equals(evt.getPropertyName())) {

			//saveShipped_Line_Items = new Shipped_Line_Items();
			if (this.facilityapprovedProductsJT.isColumnSelected(4)) {
				if (facilityapprovedProductsJT.isEditing()) {
					
				}else if(!(facilityapprovedProductsJT.isEditing())){
					try{ 
						
						
						SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd yyyy");
						
					
					    	String  exdatestr = facilityapprovedProductsJT.getValueAt(facilityapprovedProductsJT.getSelectedRow()
						    		  ,4).toString();
					    	 //facilityapprovedProductsJT.setValueAt(Convertdatestr(exdatestr), facilityapprovedProductsJT.getSelectedRow(), 5);
					    	//java.util.Date exdate = Date.parse(exdatestr);
					    	//call check date selected.
					    	//column1.setCellEditor(new DatePickerCellEditor());
					    	String mystr =  column1.getCellEditor().getCellEditorValue().toString();
					    	
					    	//String tmystr = formatter.format(column1.getCellEditor().getCellEditorValue().toString());
					    	
					    	System.out.println(mystr);
					    	//System.out.println(tmystr);
					    	//Calendar testdate;
							//testdate = (Calendar) column1.getCellEditor().getCellEditorValue();

  	
                         	//mystr =checkselectedDate(testdate);
  	                       facilityapprovedProductsJT.setValueAt(mystr, facilityapprovedProductsJT.getSelectedRow(),4);
					      
						}catch(java.lang.IllegalArgumentException A){
							A.getStackTrace();
						}catch(java.lang.NullPointerException j){
							j.getMessage();
						}
				}
	
			}
			if (this.facilityapprovedProductsJT.isColumnSelected(6)) {

			}
			if (this.facilityapprovedProductsJT.isColumnSelected(5)) {
				if (facilityapprovedProductsJT.isEditing()) {
					
			
					System.out.println("THIS CELL HAS STARTED EDITING");
					//get column index
					colindex = this.facilityapprovedProductsJT
							.getSelectedColumn();
					rowindex = this.facilityapprovedProductsJT.getSelectedRow();
					Pcode = tableModel_fproducts
							.getValueAt(
									this.facilityapprovedProductsJT
											.getSelectedRow(), 1).toString();
					
					/*ProdQty = (Integer
							.parseInt(tableModel_fproducts.getValueAt(rowindex, 6)
									.toString()));*/

                      System.out.println(ProdQty + "SAVE without tab");
					/*System.out.println(colindex);	
					System.out.println(rowindex);	
					System.out.println(Pcode);*/

				} else if (!facilityapprovedProductsJT.isEditing()) {
					
					String chars
					 = tableModel_fproducts.getValueAt(
								this.facilityapprovedProductsJT.getSelectedRow(), 5).toString();
	              		if(!(chars.isEmpty())){			
						if (!(chars.substring(0,1).matches("[0-9]"))) {	
							
							if(!(checktableCellinput((chars)))){
								
						    	JOptionPane.showMessageDialog(null, String.format("Physical Count must be a positive number", chars));	
						    	//facilityapprovedProductsJT.editCellAt(this.facilityapprovedProductsJT.getSelectedRow(),5);
						    }
							
						}
	              		}
					try {
						
						ProdQty = Double.parseDouble(tableModel_fproducts
								.getValueAt(this.facilityapprovedProductsJT
												.getSelectedRow(),5).toString());
						}catch (NumberFormatException n){
							n.getStackTrace();
						}
                     //this.facilityapprovedProductsJT.editCellAt(-1,-1);
                     //check if createob method has been called already skip if not call me 
					//if ((chars.substring(0,1).matches("[0-9]"))) {	
					createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);
					//}
					System.out.println("THIS CELL IS NOT EDITING");

				}
				// System.out.println(ProdQty + "SAVE without tab");
				//createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);
			}
			
			//System.out.println(ProdQty + "SAVE without tab");
			//check if create obj has been called an dcall 
			//createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);
		}
		
		System.out.println(ProdQty + "SAVE without tab");
		//createshipmentObj(Pcode, rnrid, shipmentdate,ProdQty);
	}

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	private void facilityapprovedProductsJTMouseClicked(
			java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call create  shipment object 

	}

	public void createshipmentObj(String apcode, int arnrid,
			Timestamp amodifieddate,Double productQty) {
		//System.out.println("Did object call me ???");
		if(!(this.barcodeproductinsert == "BARCODE_CALL")){
        //check if call to fill products was from search function
            if(this.SearchResult.equals("STRING_FOUND")){
            	
            	try {
            		double A = 0.0;
            		Double B = 0.0;
            		double C = 0.0;
            		if (productdeliverdate == null) {
            			java.util.Date date = new java.util.Date();
            			amodifieddate = new Timestamp(date.getTime());
            		} else {
            			amodifieddate = productdeliverdate;
            		}

            		System.out.println(amodifieddate);
            		System.out.println(A);
            		System.out.println(B);
            		System.out.println(C);

            		String prodremark = tableModel_searchfproducts.getValueAt(rowindex,6)
            				.toString();

            		if (prodremark != null) {
            			elmis_stock_control_card.setRemark(prodremark);
            		}

            		String prodexpiredate = tableModel_searchfproducts.getValueAt(rowindex, 4)
            				.toString();
            		System.out.println(prodexpiredate + "EXPIRY DATE ??");
            		if (prodexpiredate != null) {

            			Timestamp expiredate = Convertdatestr(prodexpiredate);
            			System.out.println(expiredate + "EXPIRY DATE NEW ??");
            			elmis_stock_control_card.setExpirydate(expiredate);
            		}
            		elmis_stock_control_card.setRefno(this.refnojTextField.getText()
            				.toString());
            		System.out.println(elmis_stock_control_card.getRefno());

            		elmis_stock_control_card.setProductcode(tableModel_searchfproducts.getValueAt(rowindex, 1).toString());
            		elmis_stock_control_card.setIssueto_receivedfrom(facilityproductsource);
            		
            	
            		elmis_stock_control_card.setQty_received(Double.parseDouble(tableModel_searchfproducts.getValueAt(rowindex,5)
            						.toString()));
            		elmis_stock_control_card.setProductid(Integer
            				.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 0)
            						.toString()));

            		elmis_stock_control_card.setQty_issued(0.0);

            		elmis_stock_control_card.setAdjustments(0.0);
            		
            		
            		elmis_stock_control_card.setId(this.GenerateGUID());

            		A = elmis_stock_control_card.getQty_received();
            		B = elmis_stock_control_card.getQty_issued();
            		C = elmis_stock_control_card.getAdjustments();

            		System.out.println(A);
            		System.out.println(B);
            		System.out.println(C);
            		//changes Mkausa - 2014
            		if(!(searchprogramcode == null)){
            		electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(searchprogramcode,apcode);
            		}
            		
            		if(electronicsccbal.size() > 0){
            		for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
            			if( b.getProductcode().equals(apcode)){
            								
            				@SuppressWarnings("unused")
            				double increase = b.getBalance() + elmis_stock_control_card.getQty_received();
            				elmis_stock_control_card.setBalance(increase);
            			}else{
            				elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
            			}
            		}
            		}else{
            			elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
            		}
            	
            		elmis_stock_control_card.setAdjustmenttype(this.adjustmentname);
            		System.out.println(elmis_stock_control_card.getBalance());
            		elmis_stock_control_card.setStoreroomadjustment(false);
            		elmis_stock_control_card.setProgram_area(facilityprogramcode);
            		//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));

            		elmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
            		elmis_stock_control_card.setCreateddate(amodifieddate);
            		elmis_stock_control_card.setDpid(AppJFrame.siteId);
            		//created linked list object 

            		
            		elmisstockcontrolcardList.add(elmis_stock_control_card);
            		System.out.println(elmisstockcontrolcardList.size() + "ARRAY SIZE???");
            		System.out.println("Array size here &&&");
            		elmis_stock_control_card = new Elmis_Stock_Control_Card();
            		
            		}catch (NumberFormatException n){
            			n.getStackTrace();
            		}catch (NullPointerException l){
            			 l.getMessage();
            		}catch(IndexOutOfBoundsException i){
            			i.getStackTrace();
            		}
            		//return saveShipped_Line_Items;	
            	
            }else{
            	
            	
        		//created a shippment object 
        		try {
        		double A = 0.0;
        		double B = 0.0;
        		double C = 0.0;
        		if (productdeliverdate == null) {
        			java.util.Date date = new java.util.Date();
        			amodifieddate = new Timestamp(date.getTime());
        		} else {
        			amodifieddate = productdeliverdate;
        		}

        		System.out.println(amodifieddate);
        		System.out.println(A);
        		System.out.println(B);
        		System.out.println(C);

        		String prodremark = tableModel_fproducts.getValueAt(rowindex,6)
        				.toString();

        		if (prodremark != null) {
        			elmis_stock_control_card.setRemark(prodremark);
        		}

        		String prodexpiredate = tableModel_fproducts.getValueAt(rowindex, 4)
        				.toString();
        		System.out.println(prodexpiredate + "EXPIRY DATE ??");
        		if (prodexpiredate != null) {

        			Timestamp expiredate = Convertdatestr(prodexpiredate);
        			System.out.println(expiredate + "EXPIRY DATE NEW ??");
        			elmis_stock_control_card.setExpirydate(expiredate);
        		}
        		elmis_stock_control_card.setRefno(this.refnojTextField.getText()
        				.toString());
        		System.out.println(elmis_stock_control_card.getRefno());

        		elmis_stock_control_card.setProductcode(apcode);
        		elmis_stock_control_card.setIssueto_receivedfrom(facilityproductsource);
        		
        	
        		elmis_stock_control_card.setQty_received(Double.parseDouble(tableModel_fproducts.getValueAt(rowindex,5)
        						.toString()));
        		elmis_stock_control_card.setProductid(Integer
        				.parseInt(tableModel_fproducts.getValueAt(rowindex, 0)
        						.toString()));

        		elmis_stock_control_card.setQty_issued(0.0);

        		elmis_stock_control_card.setAdjustments(0.0);
        		
        		
        		elmis_stock_control_card.setId(this.GenerateGUID());

        		A = elmis_stock_control_card.getQty_received();
        		B = elmis_stock_control_card.getQty_issued();
        		C = elmis_stock_control_card.getAdjustments();

        		System.out.println(A);
        		System.out.println(B);
        		System.out.println(C);
        		//changes Mkausa - 2014
        		if(!(searchprogramcode == null)){
        		electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(searchprogramcode,apcode);
        		}
        		
        		if(electronicsccbal.size() > 0){
        		for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
        			if( b.getProductcode().equals(apcode)){
        								
        				@SuppressWarnings("unused")
        				double increase = b.getBalance() + elmis_stock_control_card.getQty_received();
        				elmis_stock_control_card.setBalance(increase);
        			}else{
        				elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
        			}
        		}
        		}else{
        			elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
        		}
        	
        		elmis_stock_control_card.setAdjustmenttype(this.adjustmentname);
        		System.out.println(elmis_stock_control_card.getBalance());
        		elmis_stock_control_card.setStoreroomadjustment(false);
        		elmis_stock_control_card.setProgram_area(facilityprogramcode);
        		//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));

        		elmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
        		elmis_stock_control_card.setCreateddate(amodifieddate);
        		elmis_stock_control_card.setDpid(AppJFrame.siteId);
        		//created linked list object 

        		//elmisstockcontrolcardList.add(savestockcontrolcard);
        		elmisstockcontrolcardList.add(elmis_stock_control_card);
        		System.out.println(elmisstockcontrolcardList.size() + "ARRAY SIZE???");
        		System.out.println("Array size here &&&");
        		elmis_stock_control_card = new Elmis_Stock_Control_Card();
        		
        		}catch (NumberFormatException n){
        			n.getStackTrace();
        		}catch (NullPointerException l){
        			 l.getMessage();
        		}catch(IndexOutOfBoundsException i){
        			i.getStackTrace();
        		}
        		//return saveShipped_Line_Items;
                }
            }
        	
      
        if(this.barcodeproductinsert == "BARCODE_CALL"){
          //start of bar code create object.
        	try {
        		double A = 0.0;
        		double B = 0.0;
        		double C = 0.0;
        		if (productdeliverdate == null) {
        			java.util.Date date = new java.util.Date();
        			amodifieddate = new Timestamp(date.getTime());
        		} else {
        			amodifieddate = productdeliverdate;
        		}

        		System.out.println(amodifieddate);
        		System.out.println(A);
        		System.out.println(B);
        		System.out.println(C);
                
        		String scannerPcode = tableModel_barcodefproducts.getValueAt(arnrid,1)
        				.toString();
        		String prodremark = tableModel_barcodefproducts.getValueAt(arnrid,6)
        				.toString();

        		if (prodremark != null) {
      
      elmis_stock_control_card.setRemark(prodremark);
        		}

        		String prodexpiredate = tableModel_barcodefproducts.getValueAt(arnrid, 4)
        				.toString();
        		System.out.println(prodexpiredate + "EXPIRY DATE ??");
        		if (prodexpiredate != null) {

        			Timestamp expiredate = Convertdatestr(prodexpiredate);
        			System.out.println(expiredate + "EXPIRY DATE NEW ??");
        			elmis_stock_control_card.setExpirydate(expiredate);
        		}
        		elmis_stock_control_card.setRefno(this.refnojTextField.getText()
        				.toString());
        		System.out.println(elmis_stock_control_card.getRefno());

        		elmis_stock_control_card.setProductcode(scannerPcode);
        		elmis_stock_control_card.setIssueto_receivedfrom(facilityproductsource);
        		        	
        		elmis_stock_control_card.setQty_received(Double.parseDouble(tableModel_barcodefproducts.getValueAt(arnrid,5)
        						.toString()));
        		elmis_stock_control_card.setProductid(Integer
        				.parseInt(tableModel_barcodefproducts.getValueAt(arnrid, 0)
        						.toString()));

        		elmis_stock_control_card.setQty_issued(0.0);

        		elmis_stock_control_card.setAdjustments(0.0);
        		
        		
        		elmis_stock_control_card.setId(this.GenerateGUID());

        		A = elmis_stock_control_card.getQty_received();
        		B = elmis_stock_control_card.getQty_issued();
        		C = elmis_stock_control_card.getAdjustments();

        		System.out.println(A);
        		System.out.println(B);
        		System.out.println(C);
        		//changes Mkausa - 2014
        		if(!(searchprogramcode == null)){
        		electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(searchprogramcode,scannerPcode);
        		}
        		
        		if(electronicsccbal.size() > 0){
        		for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
        			if( b.getProductcode().equals(scannerPcode)){
        								
        				@SuppressWarnings("unused")
        				double increase = b.getBalance() + elmis_stock_control_card.getQty_received();
        				elmis_stock_control_card.setBalance(increase);
        			}else{
        				elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
        			}
        		}
        		}else{
        			elmis_stock_control_card.setBalance(elmis_stock_control_card.getQty_received());
        		}
        	
        		elmis_stock_control_card.setAdjustmenttype(this.adjustmentname);
        		System.out.println(elmis_stock_control_card.getBalance());
        		elmis_stock_control_card.setStoreroomadjustment(false);
        		elmis_stock_control_card.setProgram_area(facilityprogramcode);
        		//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));

        		elmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
        		elmis_stock_control_card.setCreateddate(amodifieddate);

        		//created linked list object 

        		//elmisstockcontrolcardList.add(savestockcontrolcard);
        		elmisstockcontrolcardList.add(elmis_stock_control_card);
        		System.out.println(elmisstockcontrolcardList.size() + "ARRAY SIZE???");
        		System.out.println("Array size here &&&");
        		elmis_stock_control_card = new Elmis_Stock_Control_Card();
        		elmis_stock_control_card.setDpid(AppJFrame.siteId);
        		}catch (NumberFormatException n){
        			n.getStackTrace();
        		}catch (NullPointerException l){
        			 l.getMessage();
        		}catch(IndexOutOfBoundsException i){
        			i.getStackTrace();
        		}	
        	
        }
        

		
	}

	private boolean isInteger(String s) {
	    try { 
	        Integer.parseInt(s); 
	        return true;
	    } catch(NumberFormatException e) { 
	        return false; 
	}
	}
	
	@SuppressWarnings("static-access")
	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		String displaymessage = "";
		String checkqty = "";
		//showmyProgressbar = new ProgressBarJD("Saving Received Items", null, AppJFrame.getDispensingId(facilityprogramcode));
		this.facilityapprovedProductsJT.editCellAt(-1,-1);
	
		checkqty = facilityapprovedProductsJT.getValueAt(rowindex,5).toString();
		 //this.facilityapprovedProductsJT.editCellAt(-1,-1);
			if(this.barcodeproductinsert == "BARCODE_CALL"){
				//this.createshipmentObj(facilityapprovedProductsJT.getValueAt(0, 1).toString(), rnrid, productdeliverdate, 11);
				//loop through the size of the table model when calling create object...
				for(int i = 0; i <=tableModel_barcodefproducts.getRowCount()-1 ; i++){
					System.out.println(tableModel_barcodefproducts.getValueAt(i, 1));
				this.createshipmentObj(tableModel_barcodefproducts.getValueAt(0, 1).toString(), i, productdeliverdate, 11.0);
				}
			}
			
		if(!(checkqty.equals("")))	{
			if(!(this.refnojTextField.getText().equals(""))){
				//check if is integer
		if(isInteger((this.refnojTextField.getText().toString())) == true){
		int ok = new arvmessageDialog()
		.showDialog(
				this,
				"Confirm Save \n\n",
				"Saving Quantity Received Information",
				"YES, To \n Save",
				"NO, To\n Edit the Quantity Entered ");
		
	/*int confirm = javax.swing.JOptionPane.showConfirmDialog(this,
				"Do  you want to save the Received Quantities to the database\n\n"
						+ "Yes - to Save\n\n"
						+ "No -to Edit the Quantity Entered\n\n"
						+ "Cancel - to exit Dialog",
				"Saving Quantity Received Information",
				JOptionPane.YES_NO_OPTION);*/


		if (ok == JOptionPane.YES_OPTION) {
			//Insert the object to database
			
			
			savestockcontrolcard = new Elmis_Stock_Control_Card();
			//	System.out.println(elmisstockcontrolcardList.size());
			for (@SuppressWarnings("unused")
			Elmis_Stock_Control_Card sp : elmisstockcontrolcardList) {
                
				if ((sp.getQty_received().compareTo(0.0) != 0)){
				if(!(sp.getRefno().equals(""))){				
				savestockcontrolcard.setRefno(sp.getRefno());
				}else{
					savestockcontrolcard.setRefno(this.refnojTextField.getText()
							.toString());
				}
				savestockcontrolcard.setProductcode(sp.getProductcode());
				savestockcontrolcard.setIssueto_receivedfrom(sp
						.getIssueto_receivedfrom());
				savestockcontrolcard.setQty_received(sp.getQty_received());

				savestockcontrolcard.setQty_issued(sp.getQty_issued());
				savestockcontrolcard.setAdjustments(sp.getAdjustments());
				savestockcontrolcard.setBalance(sp.getBalance());
				savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
				savestockcontrolcard.setProductid(sp.getProductid());

				savestockcontrolcard.setCreatedby(sp.getCreatedby());
				savestockcontrolcard.setCreateddate(sp.getCreateddate());
				savestockcontrolcard.setProgram_area(sp.getProgram_area());
				savestockcontrolcard.setStoreroomadjustment(sp
						.isStoreroomadjustment());
				savestockcontrolcard.setId(sp.getId());
				savestockcontrolcard.setDpid(sp.getDpid());
				
				try{
					if (sp.getRemark().equals("")) {
						
						if(this.SearchResult.equals("STRING_FOUND")){
							savestockcontrolcard.setRemark(this.tableModel_searchfproducts.getValueAt(rowindex,6)
									.toString());	
						}else{
						savestockcontrolcard.setRemark(this.tableModel_fproducts.getValueAt(rowindex,6)
								.toString());
						}
					}else{
						savestockcontrolcard.setRemark(sp.getRemark());
					}
					}catch(java.lang.NullPointerException n){
						n.getMessage();
					}
				
			
				if (sp.getExpirydate() != null) {

					savestockcontrolcard.setExpirydate(sp.getExpirydate());
				}
				
				if(sp.getProgram_area().toString().equals("ARV")){
					System.out.println("foo save  to elmis_prodQty");
					
					
				}
				Facilityproductsmapper
						.InsertstockcontrolcardProducts(savestockcontrolcard);
				
				}else {
					
										
						//savestockcontrolcard.setRefno(sp.getRefno());
						
						if(!(sp.getRefno().equals(""))){				
							savestockcontrolcard.setRefno(sp.getRefno());
							}else{
								savestockcontrolcard.setRefno(this.refnojTextField.getText()
										.toString());
							}
						
						
						savestockcontrolcard.setProductcode(sp.getProductcode());
						savestockcontrolcard.setIssueto_receivedfrom(sp
								.getIssueto_receivedfrom());
					  //savestockcontrolcard.setQty_received(sp.getQty_received());
						
						
						savestockcontrolcard.setQty_received(Double.parseDouble(tableModel_fproducts.getValueAt(rowindex, 5)
										.toString()));
						savestockcontrolcard.setQty_issued(sp.getQty_issued());
						savestockcontrolcard.setAdjustments(sp.getAdjustments());
						savestockcontrolcard.setBalance(sp.getBalance());
						savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
						savestockcontrolcard.setProductid(sp.getProductid());

						savestockcontrolcard.setCreatedby(sp.getCreatedby());
						savestockcontrolcard.setCreateddate(sp.getCreateddate());
						savestockcontrolcard.setProgram_area(sp.getProgram_area());
						savestockcontrolcard.setStoreroomadjustment(sp
								.isStoreroomadjustment());
						savestockcontrolcard.setId(sp.getId());
						savestockcontrolcard.setDpid(sp.getDpid());
						try{
							if (sp.getRemark().equals("")) {
								if(this.SearchResult.equals("STRING_FOUND")){
									savestockcontrolcard.setRemark(this.tableModel_searchfproducts.getValueAt(rowindex,6)
											.toString());	
								}else{
								savestockcontrolcard.setRemark(this.tableModel_fproducts.getValueAt(rowindex,6)
										.toString());
								}
							}else{
								savestockcontrolcard.setRemark(sp.getRemark());
							}
							}catch(java.lang.NullPointerException n){
								n.getMessage();
							}
						
						if (sp.getExpirydate() != null) {

							savestockcontrolcard.setExpirydate(sp.getExpirydate());
						}
						Facilityproductsmapper
								.InsertstockcontrolcardProducts(savestockcontrolcard);
					
					
				}

			}

			JOptionPane.showMessageDialog(this, "Products saved to database",
					"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
			// call repopulate
			
			this.callbackAlphaproductlist();
			
		} else if (ok == JOptionPane.NO_OPTION) {
			this.facilityapprovedProductsJT.validate();
			//this.dispose();
		} 
		
			}else{
				JOptionPane.showMessageDialog(this, "Dispatch Number can not be alphanumric!",
						"Saving Failed", JOptionPane.ERROR_MESSAGE);
			}
		    //end of check alphanumeric chars
			}else{
				//set message here .
				JOptionPane.showMessageDialog(this, "Dispatch Number is required!",
						"Saving Failed", JOptionPane.ERROR_MESSAGE);
				
			}
		}else{
		//
			JOptionPane.showMessageDialog(this, "Quantity Received  field is empty",
					"No Changes made", JOptionPane.INFORMATION_MESSAGE);
		}
		
		//this.tableModel_barcodefproducts.clearTable();
		//this.facilityapprovedProductsJT.setModel(tableModel_fproducts);
		//call original program list to repopulate 
		//this.callbackAlphaproductlist();

	}

	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new FacilityProductsDAO();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		this.facilityapprovedProductsJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 22));
		this.facilityapprovedProductsJT.setRowHeight(30);
		
		tablecolumnaligner.rightAlignColumn(this.facilityapprovedProductsJT,6);
		for (Losses_Adjustments_Types l : facilityAdjustmentList) {
			modelAdjustments.addElement(l.getName());

		}
		PopulateProgram_ProductTable(productsList);
		rnrid = 4;
		this.ShipmentdateJDate.setDate(new Date());
		this.ShipmentdateJDate.setFocusable(isFocusable());
		shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");
		this.ShipmentdateJDate.requestFocusInWindow();
		//refnojTextField.requestFocusInWindow();
		
		    column1 = facilityapprovedProductsJT.getColumnModel().getColumn(4);
		  //column1.setCellRenderer(new JDateChooserRenderer());
	
			//column1.setCellEditor(new DatePickerCellEditor());
		    
			column1.setCellEditor(new DatePickerCellEditor());
	}
	
	
	
	
	@SuppressWarnings( { "unused", "unchecked", "static-access" })
	public static void PopulateProgram_ChangeregimenTable(String[]  itemList) {
			
			try {
				System.out.println(Arrays.toString(itemList)); 
				
				facilityapprovedProductsJT
				.setModel(tableModel_barcodefproducts);
		
				//tableModel_barcodefproducts.clearTable();	
				
				
			for (int i = 0; i < itemList.length - 1; i++) {
				 
                    if(i == 0) {
					defaultv_barcodefacilityapprovedproducts[0] = itemList[i]
							.toString();
					defaultv_barcodefacilityapprovedproducts[1] = itemList[i + 1]
							.toString();

					defaultv_barcodefacilityapprovedproducts[2] = itemList[i + 2]
							.toString();
					defaultv_barcodefacilityapprovedproducts[3] = itemList[i + 3]
							.toString();
					TableColumn column1 = facilityapprovedProductsJT
							.getColumnModel().getColumn(4);
					//column1.setCellRenderer(new JDateChooserRenderer());
					
					
					column1.setCellEditor(new DatePickerCellEditor());
					
					defaultv_barcodefacilityapprovedproducts[5] = itemList[i + 4]
							.toString();
					defaultv_barcodefacilityapprovedproducts[6] = "barcode receipts";

					@SuppressWarnings("rawtypes")
					ArrayList cols = new ArrayList();

					//cols.add(regimenlist);
					for (int j = 0; j < columns_barcodefacilityApprovedproducts.length; j++) {
						cols.add(defaultv_barcodefacilityapprovedproducts[j]);

					}

						tableModel_barcodefproducts.insertRow(cols);
					//fapprovedproductsIterator.remove();
                    }
					 // find the position of array ;	
					System.out.println(i);	
					
				}
			  

			
			} catch (NullPointerException e) {
				//do something here 
				e.getMessage();
			}
			
			
			flushtableModel_barcodefproducts(itemList);
			SearchproductssccJTF.requestFocusInWindow();
		
	}

	private static void flushtableModel_barcodefproducts(String[]  itemList){
		
		for (int i = 0; i < itemList.length - 1; i++) {
			 
                if(i == 0) {
				defaultv_barcodefacilityapprovedproducts[0] = "";
						
				defaultv_barcodefacilityapprovedproducts[1] = "";
				defaultv_barcodefacilityapprovedproducts[2] = "";
				defaultv_barcodefacilityapprovedproducts[3] = "";
				
				
				defaultv_barcodefacilityapprovedproducts[5] = "";
				defaultv_barcodefacilityapprovedproducts[6] = "";

			
			

				
				//fapprovedproductsIterator.remove();
                }
				 // find the position of array ;	
				System.out.println(i);	
				
			}
	}
	
	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fapprovProducts) {
		checkbox = new JCheckBox();
		try {
			callfacility = new FacilitySetUpDAO();
			Facilityproductsmapper = new FacilityProductsDAO();

			this.facilityprogramcode = ProgramsJP.selectedprogramCode;
			this.facilityproductsource = ProductsSourceJP.selectedproductsource;
			//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

			facility = callfacility.getFacility();

			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();
			String dbdriverStatus = System.getProperty("dbdriver");

			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
				productsList = Facilityproductsmapper
						.dogetcurrentFacilityApprovedProducts(
								facilityprogramcode, typeCode);
			} else {
				productsList = Facilityproductsmapper
						.dogetcurrentFacilityApprovedProductshsqldb(
								facilityprogramcode, typeCode);
			}

			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			this.facilityapprovedProductsJT
			.setModel(tableModel_fproducts);
			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = productsList.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getProductid();
				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength();
				column1 = facilityapprovedProductsJT
						.getColumnModel().getColumn(4);
				
			
				column1.setCellEditor(new DatePickerCellEditor());
			
				//	defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
				/*TableColumn column1 = facilityapprovedProductsJT
						.getColumnModel().getColumn(4);
				column1.setCellRenderer(new JDateChooserRenderer());
				column1.setCellEditor(new DatePickerCellEditor());*/
				//defaultv_facilityapprovedproducts[5] = selecteddate;
					

				//				defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				Properties prop = new Properties(System.getProperties());
				FileInputStream fis = null;
				try {
					fis = new FileInputStream("Programmproperties.properties");
					prop.load(fis);

					System.setProperties(prop);

					fis.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
				String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
				System.setProperty("dbpassword", dbpassword);
				ReceiveProductsJD dialog = new ReceiveProductsJD(
						new javax.swing.JFrame(), true, searchprogramcode);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}
	private javax.swing.JButton CancelJBtn;
	private javax.swing.JButton SaveJBtn;
	private static javax.swing.JTextField SearchproductssccJTF;
	private com.toedter.calendar.JDateChooser ShipmentdateJDate;
	private static javax.swing.JTable facilityapprovedProductsJT;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextField refnojTextField;
	// End of variables declaration//GEN-END:variables

}