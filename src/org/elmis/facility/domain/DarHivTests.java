package org.elmis.facility.domain;

import java.util.Date;

/**
 * DarHivTests entity. @author MyEclipse Persistence Tools
 */

public class DarHivTests implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 5698998124002893410L;
	private String id;
	private Integer facilityId;
	private Integer testingPointId;
	private Integer clientId;
	private String purpose;
	private String result;
	private Date dateOfTest;
	private String screening;
	private String confirmatory;
	private String tiebreaker;
	private String finalTestResults;
	private String positiveResult;
	private String indeterminateResult;
	private String negativeResult;

	// Constructors

	/** default constructor */
	public DarHivTests() {
	}

	/** minimal constructor */
	public DarHivTests(String id) {
		this.id = id;
	}

	/** full constructor */
	public DarHivTests(String id, Integer facilityId, Integer testingPointId,
			Integer clientId, String purpose, String result, Date dateOfTest,
			String screening, String confirmatory, String tiebreaker,
			String finalTestResults, String positiveResult,
			String indeterminateResult, String negativeResult) {
		this.id = id;
		this.facilityId = facilityId;
		this.testingPointId = testingPointId;
		this.clientId = clientId;
		this.purpose = purpose;
		this.result = result;
		this.dateOfTest = dateOfTest;
		this.screening = screening;
		this.confirmatory = confirmatory;
		this.tiebreaker = tiebreaker;
		this.finalTestResults = finalTestResults;
		this.positiveResult = positiveResult;
		this.indeterminateResult = indeterminateResult;
		this.negativeResult = negativeResult;
	}

	public Integer getClientId() {
		return this.clientId;
	}

	public String getConfirmatory() {
		return this.confirmatory;
	}

	public Date getDateOfTest() {
		return this.dateOfTest;
	}

	public Integer getFacilityId() {
		return this.facilityId;
	}

	public String getFinalTestResults() {
		return this.finalTestResults;
	}

	public String getId() {

		return this.id;
	}

	public String getIndeterminateResult() {
		return this.indeterminateResult;
	}

	public String getNegativeResult() {
		return this.negativeResult;
	}

	public String getPositiveResult() {
		return this.positiveResult;
	}

	public String getPurpose() {
		return this.purpose;
	}

	public String getResult() {
		return this.result;
	}

	public String getScreening() {
		return this.screening;
	}

	public Integer getTestingPointId() {
		return this.testingPointId;
	}

	public String getTiebreaker() {
		return this.tiebreaker;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public void setConfirmatory(String confirmatory) {
		this.confirmatory = confirmatory;
	}

	public void setDateOfTest(Date dateOfTest) {
		this.dateOfTest = dateOfTest;
	}

	public void setFacilityId(Integer facilityId) {
		this.facilityId = facilityId;
	}

	public void setFinalTestResults(String finalTestResults) {
		this.finalTestResults = finalTestResults;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setIndeterminateResult(String indeterminateResult) {
		this.indeterminateResult = indeterminateResult;
	}

	public void setNegativeResult(String negativeResult) {
		this.negativeResult = negativeResult;
	}

	public void setPositiveResult(String positiveResult) {
		this.positiveResult = positiveResult;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public void setScreening(String screening) {
		this.screening = screening;
	}

	public void setTestingPointId(Integer testingPointId) {
		this.testingPointId = testingPointId;
	}

	public void setTiebreaker(String tiebreaker) {
		this.tiebreaker = tiebreaker;
	}

}