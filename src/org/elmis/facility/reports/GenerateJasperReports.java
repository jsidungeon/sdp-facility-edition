/**
 * 
 */
package org.elmis.facility.reports;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;

import java.awt.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.reports.GenerateJasperReports;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.TableModel;





/**
 * @author JBanda
 *
 */
public class GenerateJasperReports {

	

	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	private List<StockControlCard>  ascclist = new LinkedList();
	private GenerateJasperReports _this = this;
	/**
	 * 
	 */
	public GenerateJasperReports() {
		// TODO Auto-generated constructor stub
	}
	
public void createStockControlCard(int selectedProductID){
		
		SqlSessionFactory factory = new MyBatisConnectionFactory().getSqlSessionFactory();

		SqlSession session = factory.openSession();
		StockControlCard	scc = new StockControlCard();
		List<StockControlCard> ascclist  = new LinkedList();
		List <StockControlCard> sccList = new ArrayList();
		try {

			//scc
			sccList = session.selectList("selectBySCCByProductID",selectedProductID);//("getProgramesSupported");
			//ascclist = session.selectList("selectBySCCByProductID",selectedProductID);
			//System.out.println(this.products.getPrimaryname() +" "+IssuingJD.selectedProductID);
            for(StockControlCard s: sccList){
            System.out.println(s.getProductcode());
            System.out.println(s.getAdjustments());
            System.out.println(s.getBalance());
            System.out.println(s.getIssueto_receivedfrom());
            }
           
            
			//this.populateProgramsTable(this.programsList);

		} finally {
			session.close();
		}
		
		
		// TODO add your handling code here:

		
		
		

			

			

			
		

		// new Items_issue().createBeanCollection()
		try {
       
	
			

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport("Reports/stockcontrolcard.jasper",
					parameterMap, new JRBeanCollectionDataSource(sccList));

			/*new JasperViewer(print);
			JasperViewer.viewReport(print, false);*/
			
			JFrame frame2 = new JFrame("Report");
			frame2.getContentPane().setPreferredSize(
					new Dimension(1300, 650));
			frame2.getContentPane().add(new JRViewer(print));
			frame2.pack();
			frame2.setVisible(true);
			

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}
	
		

}


public void createStockControlCardbycode(String selectedProductcode,String primaryname,String packsize){
	
	SqlSessionFactory factory = new MyBatisConnectionFactory().getSqlSessionFactory();

	SqlSession session = factory.openSession();
	StockControlCard	scc = new StockControlCard();
	List<StockControlCard> ascclist  = new LinkedList();
	List <StockControlCard> sccList = new ArrayList();
	try {

		//scc
		sccList = session.selectList("selectBySCCByProductID",selectedProductcode);//("getProgramesSupported");
		//ascclist = session.selectList("selectBySCCByProductID",selectedProductID);
		//System.out.println(this.products.getPrimaryname() +" "+IssuingJD.selectedProductID);
        for(StockControlCard s: sccList){
        System.out.println(s.getProductcode());
        System.out.println(s.getAdjustments());
        System.out.println(s.getQty_isssued());
        System.out.println(s.getQty_received());
        System.out.println(s.getBalance());
        System.out.println(s.getIssueto_receivedfrom());
        }
       
        
		//this.populateProgramsTable(this.programsList);

	} finally {
		session.close();
	}
	
	
	// TODO add your handling code here:

	
		
	

	// new Items_issue().createBeanCollection()
	try {
   
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("primaryname", primaryname);
		parameters.put("packsize", packsize);
		parameters.put("facilityname", System.getProperty("facilityname"));
		parameters.put("productcode", selectedProductcode);
		parameters.put("district", System.getProperty("district"));
		AppJFrame.glassPane.activate(null);
		print = JasperFillManager.fillReport("Reports/stockcontrolcard.jasper",
				parameters, new JRBeanCollectionDataSource(sccList));
		JRViewer jrViewer =  new JRViewer(print);
		// SCCReportDialogue dialog = new SCCReportDialogue(new JRViewer(print), null, true);
		showReport(jrViewer);
        // dialog.setVisible(true);
		
		/*JFrame frame2 = new JFrame("Report");
		frame2.getContentPane().setPreferredSize(
				new Dimension(1300, 650));
		frame2.getContentPane().add(new JRViewer(print));
		frame2.pack();
		frame2.setVisible(true);*/
		

	} catch (JRException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
				.toString());
	}

	

}



private  void showReport ( JRViewer jv) {
	JDialog jf = new JDialog();

	jf.setTitle("Report");
	jf.setModal(true);
	jv.setPreferredSize(new Dimension(1300, 600));
	jf.getContentPane().add(jv);
	jf.validate();
	jf.setLocationRelativeTo(null);
	jf.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	jf.pack();
	jf.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - jf.getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - jf.getHeight()/2);
	jf.setVisible(true); 
	

	

}




}
