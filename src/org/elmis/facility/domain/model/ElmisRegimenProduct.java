package org.elmis.facility.domain.model;

public class ElmisRegimenProduct {
	private Integer id;
	private Integer productId;
	private Integer regimenId;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getProductId() {
		return productId;
	}
	
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	public Integer getRegimenId() {
		return regimenId;
	}
	
	public void setRegimenId(Integer regimenId) {
		this.regimenId = regimenId;
	}
	
}
