package org.elmis.facility.utils;

import java.io.IOException;
/**
 * SoftKeyboard.Java
 * @Purpose: Used to start up On-screen Keyboard. Functionality only working for Windows Platform.  
 * @author Michael Mwebaze
 * @version 1.0
 *
 */
public class SoftKeyboard {

	/**
	 * Launches the On-screen Keyboard (OSK)
	 */
	public static void startSoftKeyboard()
	{
		if (OSType.isWindows()) {
			String sysroot = System.getenv("SystemRoot");
			try {
				Runtime.getRuntime().exec("cmd /c "+sysroot+"/System32/osk.exe");
			} catch (IOException e) {

				e.printStackTrace();
			}
		} else if (OSType.isMac()) {
			System.out.println("This is Mac");
		} else if (OSType.isUnix()) {
			String[] cmd = {"onboard"};
			try
			{
				Runtime.getRuntime().exec(cmd);
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}
		} else if (OSType.isSolaris()) {
			System.out.println("This is Solaris");
		} else {
			System.out.println("Your OS is not support!!");
		}
	}
}
