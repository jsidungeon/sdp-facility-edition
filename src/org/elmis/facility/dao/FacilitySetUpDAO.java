package org.elmis.facility.dao;
import java.util.List;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.Elmis_Dar_TransactionsMapper;
import org.elmis.facility.domain.dao.facilitiesMapper;
import org.elmis.facility.domain.dao.usersMapper;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.facilities;
import org.elmis.facility.domain.model.Facility_Operators;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Geographic_Zones;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.network.MyBatisConnectionFactory;
public class FacilitySetUpDAO {
	
	private String stra ;
	private SqlSessionFactory sqlSessionFactory; 
		
	public FacilitySetUpDAO(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	

	@SuppressWarnings("unchecked")
	public Facility getFacility(){
		String facilityCode = System.getProperty("facilityCode");
		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			 Facility fac = session.selectOne("getFacility", facilityCode);
			 //String operator = fac.
			// System.out.println(fac.getCode().toString() + "FacilityCode Noted");
			return fac;
		} finally {
			session.close();
		}
	}
	
		
	public  List<facilities>
	getfacilitynameList () {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		facilitiesMapper mapper = session.getMapper(facilitiesMapper.class);
     List<facilities> listfacnames = mapper.selectfacilitynames();  
     return listfacnames;
		}finally{
			session.close();
		}
     }	
	
	
	
	@SuppressWarnings("unchecked")
	public List<users> selectAll(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			usersMapper mapper  = session.getMapper(usersMapper.class);
			List<users> list = mapper.getAllUsers();
			return list;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public Facility_Types selectAllwithTypes(Facility facility){

		SqlSession session = sqlSessionFactory.openSession();
		String facCode = (facility.getCode()).trim();
		try {
			Facility_Types facttypes = session.selectOne("getFacilityTypes",facCode);
			
			
			return facttypes;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public Facility_Operators selectAllwithOps(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Facility_Operators facOps = session.selectOne("getFacilityOps");
			
			
			return facOps;
		} finally {
			session.close();
		}
	}
	

	@SuppressWarnings("unchecked")
	public Geographic_Zones selectAllwithGeozones(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Geographic_Zones geozone = session.selectOne("getAllGeoZones");
			
			
			return geozone;
		} finally {
			session.close();
		}
	}	
	
	
	
	
	
}
