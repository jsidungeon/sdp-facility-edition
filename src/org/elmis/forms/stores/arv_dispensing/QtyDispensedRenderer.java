package org.elmis.forms.stores.arv_dispensing;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

class QtyDispensedRenderer extends DefaultTableCellRenderer {
	private static final long serialVersionUID = 6703872492730589499L;

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		Component cellComponent = super.getTableCellRendererComponent(table,
				value, isSelected, hasFocus, row, column);
		if (table.getValueAt(row, ARTNumberJP.TBL_IDX_EQUIV_BOTTLES) != null && table.getValueAt(row, ARTNumberJP.TBL_IDX_EQUIV_BOTTLES).toString().trim().length() > 0
				&& table.getValueAt(row, ARTNumberJP.TBL_IDX_BALANCE) != null && table.getValueAt(row, ARTNumberJP.TBL_IDX_BALANCE).toString().trim().length() > 0) {
			Double qty = Double.parseDouble((table.getValueAt(row, ARTNumberJP.TBL_IDX_EQUIV_BOTTLES)
					.toString().trim()));
			Double balance = Double.parseDouble((table.getValueAt(row, ARTNumberJP.TBL_IDX_BALANCE)
					.toString().trim()));
			if (qty > balance) {
				cellComponent.setBackground(Color.RED);
			} else {
				cellComponent.setBackground(Color.WHITE);
			}
		} else {
			cellComponent.setBackground(Color.WHITE);
		}
		return cellComponent;
	}
}