/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.receiving;

import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.MutableComboBoxModel;

import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;

import com.oribicom.tools.TableModel;

/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class arvdispensingproductAdjustmentsJD extends javax.swing.JDialog {

	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;

	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	private String adjustmentname = "";
	private String programname = "";
	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
			"Product Id", "Product Code", "Product name", "Generic Strength",
			"Remarks", "Quantity" };
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "", "", "" };
	private static final int rows_fproducts = 0;
	private JComboBox facilityProgList = new JComboBox();
	MutableComboBoxModel modelPrograms = (MutableComboBoxModel) facilityProgList
			.getModel();

	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 5) {
				return getValueAt(0, 5).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};
	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> adjustmensproductsList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	FacilityProductsDAO Facilityproductsmapper = null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private JCheckBox checkbox;
	public static Boolean cansave = false;
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilitytypeCode;

	ArrayList<Elmis_Stock_Control_Card> adjustmentstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();

	ListIterator elmistockcontrolcarditerator = adjustmentstockcontrolcardList
			.listIterator();

	private Elmis_Stock_Control_Card Adjustmentelmis_stock_control_card;
	private Elmis_Stock_Control_Card Adjustmentsavestockcontrolcard;
	public String facilityproductsource;

	/** Creates new form receiveProducts */
	public arvdispensingproductAdjustmentsJD(java.awt.Frame parent,
			boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(1000, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		ShipmentdateJDate.setPreferredSize(new Dimension (150,20));
		AdjustmentTypeJCB1.setPreferredSize(new Dimension (150,20));
		SearchproductssccJTF.setPreferredSize(new Dimension (150,20));
		ProgramTypeJCB.setPreferredSize(new Dimension (150,20));
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel3 = new javax.swing.JLabel();
		ShipmentdateJDate = new com.toedter.calendar.JDateChooser();
		jLabel1 = new javax.swing.JLabel();
		AdjustmentTypeJCB1 = new javax.swing.JComboBox();
		jLabel2 = new javax.swing.JLabel();
		ProgramTypeJCB = new javax.swing.JComboBox();
		jScrollPane1 = new javax.swing.JScrollPane();
		AdjustmentfacilityapprovedProductsJT = new javax.swing.JTable();
		CancelJBtn = new javax.swing.JButton();
		SaveJBtn = new javax.swing.JButton();
		SearchproductssccJTF = new javax.swing.JTextField();
		jLabel4 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Product Adjustments");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Adjustment date");

		ShipmentdateJDate.setDateFormatString(mydateformat);
		ShipmentdateJDate.setFont(new java.awt.Font("Tahoma", 0, 18));
		ShipmentdateJDate
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ShipmentdateJDatePropertyChange(evt);
					}
				});

		jLabel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Adjustment Type");

		AdjustmentTypeJCB1.setFont(new java.awt.Font("Tahoma", 0, 18));
		AdjustmentTypeJCB1.setModel(modelAdjustments);
		AdjustmentTypeJCB1
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						AdjustmentTypeJCB1ActionPerformed(evt);
					}
				});

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Program Area");

		ProgramTypeJCB.setFont(new java.awt.Font("Tahoma", 0, 18));
		ProgramTypeJCB.setModel(modelPrograms);
		ProgramTypeJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				ProgramTypeJCBActionPerformed(evt);
			}
		});
		ProgramTypeJCB
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ProgramTypeJCBPropertyChange(evt);
					}
				});

		AdjustmentfacilityapprovedProductsJT.setFont(new java.awt.Font(
				"Tahoma", 0, 18));
		AdjustmentfacilityapprovedProductsJT.setModel(tableModel_fproducts);
		AdjustmentfacilityapprovedProductsJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						AdjustmentfacilityapprovedProductsJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(AdjustmentfacilityapprovedProductsJT);

		CancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/navigate_left.png"))); // NOI18N
		CancelJBtn.setText("Cancel");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/navigate_right.png"))); // NOI18N
		SaveJBtn.setText("Save  ");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Search");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																jPanel1Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								jScrollPane1,
																								javax.swing.GroupLayout.Alignment.LEADING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								778,
																								Short.MAX_VALUE)
																						.addGroup(
																								javax.swing.GroupLayout.Alignment.LEADING,
																								jPanel1Layout
																										.createSequentialGroup()
																										.addGroup(
																												jPanel1Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.TRAILING)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				jLabel3)
																																		.addGap(
																																				8,
																																				8,
																																				8))
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				jLabel1)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
																										.addGroup(
																												jPanel1Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.LEADING)
																														.addComponent(
																																AdjustmentTypeJCB1,
																																0,
																																227,
																																Short.MAX_VALUE)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				ShipmentdateJDate,
																																				javax.swing.GroupLayout.PREFERRED_SIZE,
																																				197,
																																				javax.swing.GroupLayout.PREFERRED_SIZE)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
																										.addGroup(
																												jPanel1Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.LEADING)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addGap(
																																				45,
																																				45,
																																				45)
																																		.addComponent(
																																				jLabel4))
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																		.addComponent(
																																				jLabel2)))
																										.addPreferredGap(
																												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																										.addGroup(
																												jPanel1Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.TRAILING)
																														.addComponent(
																																SearchproductssccJTF,
																																javax.swing.GroupLayout.DEFAULT_SIZE,
																																361,
																																Short.MAX_VALUE)
																														.addComponent(
																																ProgramTypeJCB,
																																0,
																																361,
																																Short.MAX_VALUE))))
																		.addContainerGap())
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				CancelJBtn)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				SaveJBtn)
																		.addGap(
																				18,
																				18,
																				18)))));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																ShipmentdateJDate,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel3)
																						.addGroup(
																								jPanel1Layout
																										.createSequentialGroup()
																										.addGroup(
																												jPanel1Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.BASELINE)
																														.addComponent(
																																SearchproductssccJTF,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																javax.swing.GroupLayout.DEFAULT_SIZE,
																																javax.swing.GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																jLabel4))
																										.addPreferredGap(
																												javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel1)
																						.addComponent(
																								AdjustmentTypeJCB1,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel2,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								26,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								ProgramTypeJCB,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												208,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																CancelJBtn)
														.addComponent(SaveJBtn))
										.addContainerGap(67, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.PREFERRED_SIZE));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void ProgramTypeJCBActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		JComboBox cb = (JComboBox) evt.getSource();
		programname = (String) cb.getSelectedItem();
		System.out.println(programname);
		callfacility = new FacilitySetUpDAO();
		Facilityproductsmapper = new FacilityProductsDAO();
		facility = callfacility.getFacility();
		System.out.println(facility.getCode() + "We have the facility code");
		facilitytype = callfacility.selectAllwithTypes(facility);
		typeCode = facilitytype.getCode();

		this.facilityprogramcode = programname;
		adjustmensproductsList = Facilityproductsmapper
				.dogetcurrentFacilityApprovedProducts(facilityprogramcode,
						typeCode);
		this.PopulateProgram_ProductTable(adjustmensproductsList);

	}

	private void AdjustmentTypeJCB1ActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		JComboBox cb = (JComboBox) evt.getSource();
		adjustmentname = (String) cb.getSelectedItem();
		System.out.println(adjustmentname);

	}

	private void ProgramTypeJCBPropertyChange(java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.dispose();
	}

	private void ShipmentdateJDatePropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		try {

			String mydate = this.ShipmentdateJDate.getDate().toString();
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void AdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		AdjustmentfacilityapprovedProductsJT.getColumnModel().getColumn(0)
				.setMinWidth(0);
		AdjustmentfacilityapprovedProductsJT.getColumnModel().getColumn(0)
				.setMaxWidth(0);
		AdjustmentfacilityapprovedProductsJT.getColumnModel().getColumn(0)
				.setWidth(0);
		if ("tableCellEditor".equals(evt.getPropertyName())) {

			//saveShipped_Line_Items = new Shipped_Line_Items();
			if (this.AdjustmentfacilityapprovedProductsJT.isColumnSelected(4)) {

			}
			if (this.AdjustmentfacilityapprovedProductsJT.isColumnSelected(5)) {
				if (AdjustmentfacilityapprovedProductsJT.isEditing()) {
					System.out.println("THIS CELL HAS STARTED EDITING");
					//get column index
					colindex = this.AdjustmentfacilityapprovedProductsJT
							.getSelectedColumn();
					rowindex = this.AdjustmentfacilityapprovedProductsJT
							.getSelectedRow();
					Pcode = tableModel_fproducts.getValueAt(
							this.AdjustmentfacilityapprovedProductsJT
									.getSelectedRow(), 1).toString();

					/*System.out.println(colindex);	
					System.out.println(rowindex);	
					System.out.println(Pcode);*/

				} else if (!AdjustmentfacilityapprovedProductsJT.isEditing()) {

					createshipmentObj(Pcode, rnrid, shipmentdate);

					System.out.println("THIS CELL IS NOT EDITING");

				}
			}
		}

	}

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	public void createshipmentObj(String apcode, int arnrid,
			Timestamp amodifieddate) {
		//System.out.println("Did object call me ???");

		//created a shippment object 
		//created a shippment object 

		Double A = 0.0;
		Double B = 0.0;
		double C = 0;
		if (productdeliverdate == null) {
			java.util.Date date = new java.util.Date();
			amodifieddate = new Timestamp(date.getTime());
		} else {
			amodifieddate = productdeliverdate;
		}

		System.out.println(amodifieddate);
		System.out.println(A);
		System.out.println(B);
		System.out.println(C);

		String prodAdjustremark = tableModel_fproducts.getValueAt(rowindex, 4)
				.toString();

		if (prodAdjustremark != null) {
			Adjustmentelmis_stock_control_card.setRemark(prodAdjustremark);
		}
		Adjustmentelmis_stock_control_card.setProductcode(apcode);

		Adjustmentelmis_stock_control_card.setProductid(Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 0)
						.toString()));
		if (this.adjustmentname.equals("TRANSFER_OUT")) {
			Adjustmentelmis_stock_control_card.setQty_issued(Double.parseDouble(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setAdjustments(0);

		}
		this.adjustmentname.trim();
		if (this.adjustmentname.equals("DAMAGED")) {

			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}

		if (this.adjustmentname.equals("LOST")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}
		if (this.adjustmentname.equals("STOLEN")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}

		if (this.adjustmentname.equals("EXPIRED")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}

		if (this.adjustmentname.equals("PASSED_OPEN_VIAL_TIME_LIMIT")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}

		if (this.adjustmentname.equals("COLD_CHAIN_FAILURE")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}

		if (this.adjustmentname.equals("CLINIC_RETURN")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}
		if (this.adjustmentname.equals("FOUND")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}
		if (this.adjustmentname.equals("PURCHASE")) {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 5)
							.toString()));

			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
		}

		Adjustmentelmis_stock_control_card
				.setId(com.oribicom.tools.publicMethods.createGUID());//this.GenerateGUID());
		A = Adjustmentelmis_stock_control_card.getQty_received();
		B = Adjustmentelmis_stock_control_card.getQty_issued();
		C = Adjustmentelmis_stock_control_card.getAdjustments();

		if (!(this.adjustmentname.equals("PURCHASE"))) {
			C = -C;
		} else {
			C = +C;
		}

		System.out.println(A + "WHAT IS  THE NUMBER");
		System.out.println(B);
		System.out.println(C);
		Double num = Adjustmentelmis_stock_control_card.getBalance();
		if (num == null) {
			num = 0.0;
			Adjustmentelmis_stock_control_card.setBalance(num + (A + B + C));
		} else {
			Adjustmentelmis_stock_control_card.setBalance(num + (A + B + C));
		}
		Adjustmentelmis_stock_control_card
				.setAdjustmenttype(this.adjustmentname);
		System.out.println(Adjustmentelmis_stock_control_card.getBalance());
		//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));
		Adjustmentelmis_stock_control_card.setStoreroomadjustment(true);
		Adjustmentelmis_stock_control_card.setProgram_area(facilityprogramcode);
		Adjustmentelmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
		Adjustmentelmis_stock_control_card.setCreateddate(amodifieddate);

		//created linked list object 

		//adjustmentstockcontrolcardList.add(savestockcontrolcard);
		adjustmentstockcontrolcardList.add(Adjustmentelmis_stock_control_card);
		System.out.println(adjustmentstockcontrolcardList.size()
				+ "ARRAY SIZE???");
		System.out.println("Array size here &&&");
		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
		//return saveShipped_Line_Items;

	}

	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		String displaymessage = "";
		
		int ok = new arvmessageDialog()
		.showDialog(
				this,
				"Do  you want to save the Received Quantities to the database\n\n",
				"Saving Quantity Received Information",
				"YES, To \n Save",
				"NO, To\n Edit the Quantity Entered ");

		/*int confirm = javax.swing.JOptionPane.showConfirmDialog(this,
				"Do  you want to save the Received Quantities to the database\n\n"
						+ "Yes - to Save\n\n"
						+ "No -to Edit the Quantity Entered \n\n"
						+ "Cancel - to exit Dialog",
				"Saving Quantity Received Information",
				JOptionPane.YES_NO_OPTION);*/

		if (ok == JOptionPane.YES_OPTION) {
			//Insert the object to database
			Adjustmentsavestockcontrolcard = new Elmis_Stock_Control_Card();
			//	System.out.println(adjustmentstockcontrolcardList.size());
			for (@SuppressWarnings("unused")
			Elmis_Stock_Control_Card sp : adjustmentstockcontrolcardList) {

				Adjustmentsavestockcontrolcard.setProductcode(sp
						.getProductcode());
				//savestockcontrolcard.setIssueto_receivedfrom(sp.getIssueto_receivedfrom());
				Adjustmentsavestockcontrolcard.setQty_received(sp
						.getQty_received());

				Adjustmentsavestockcontrolcard.setQty_issued(sp
						.getQty_issued());
				Adjustmentsavestockcontrolcard.setAdjustments(sp
						.getAdjustments());
				Adjustmentsavestockcontrolcard.setBalance(sp.getBalance());
				Adjustmentsavestockcontrolcard.setAdjustmenttype(sp
						.getAdjustmenttype());
				Adjustmentsavestockcontrolcard.setProductid(sp.getProductid());

				Adjustmentsavestockcontrolcard.setCreatedby(sp.getCreatedby());
				Adjustmentsavestockcontrolcard.setCreateddate(sp
						.getCreateddate());
				Adjustmentsavestockcontrolcard.setProgram_area(sp
						.getProgram_area());
				Adjustmentsavestockcontrolcard.setStoreroomadjustment(sp
						.isStoreroomadjustment());
				Adjustmentsavestockcontrolcard.setId(sp.getId());
				if (sp.getRemark() != null) {
					Adjustmentsavestockcontrolcard.setRemark(sp.getRemark());
				}
				Facilityproductsmapper
						.InsertstockcontrolcardProducts(Adjustmentsavestockcontrolcard);

			}

			JOptionPane.showMessageDialog(this, "Products saved to database",
					"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
		} else if (ok == JOptionPane.NO_OPTION) {
			this.AdjustmentfacilityapprovedProductsJT.validate();
		}
	}

	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		this.ShipmentdateJDate.setFocusable(isFocusable());
		Facilityproductsmapper = new FacilityProductsDAO();
		facilityprogramsList = Facilityproductsmapper.selectAllPrograms();
		for (Programs p : facilityprogramsList) {
			modelPrograms.addElement(p.getCode());

		}

		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		for (Losses_Adjustments_Types l : facilityAdjustmentList) {
			modelAdjustments.addElement(l.getName());

		}

		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
		PopulateProgram_ProductTable(adjustmensproductsList);
		rnrid = 4;
		shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");

	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fapprovProducts) {
		checkbox = new JCheckBox();
		try {

			if (fapprovProducts == null) {
				callfacility = new FacilitySetUpDAO();
				Facilityproductsmapper = new FacilityProductsDAO();

				facility = callfacility.getFacility();
				System.out.println(facility.getCode()
						+ "We have the facility code");
				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				this.facilityprogramcode = ProgramsJP.selectedprogramCode;

				if (!(facilityprogramcode == null)) {
					//reset program code 
					adjustmensproductsList = Facilityproductsmapper
							.dogetcurrentFacilityApprovedProducts(
									facilityprogramcode, typeCode);

				}

			} else {
				adjustmensproductsList = fapprovProducts;
			}
			//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;
			System.out.println(facilityprogramcode + "Got it right ??");
			System.out.println(typeCode + "Got it again right ??");
			rnrid = 4;
			System.out.println(rnrid + "Got it again right ??");

			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : adjustmensproductsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = adjustmensproductsList.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getProductid().toString();

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength().toString();
				//Set the value of the entered product quantity 

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}
				System.out.print(cols.size()
						+ "What is the SIZE of Array to Table??");
				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				arvdispensingproductAdjustmentsJD dialog = new arvdispensingproductAdjustmentsJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JComboBox AdjustmentTypeJCB1;
	private javax.swing.JTable AdjustmentfacilityapprovedProductsJT;
	private javax.swing.JButton CancelJBtn;
	private javax.swing.JComboBox ProgramTypeJCB;
	private javax.swing.JButton SaveJBtn;
	private javax.swing.JTextField SearchproductssccJTF;
	private com.toedter.calendar.JDateChooser ShipmentdateJDate;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables

}