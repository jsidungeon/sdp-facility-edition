/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reports.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author mmwebaze
 *
 */
public class PropertyLoader {

	private static Properties prop = new Properties();
	static
	{
		try
		{
			prop.load(new FileInputStream("Config/report.properties"));
			//maxStockLevel = prop.getProperty("");
		}
		catch(IOException e)
		{
			System.out.println(e.getLocalizedMessage());
		}
	}
	
	/*public PropertyLoader()
	{
		try
		{
			prop.load(new FileInputStream("Config/report.properties"));
			//maxStockLevel = prop.getProperty("");
		}
		catch(IOException e)
		{
			System.out.println(e.getLocalizedMessage());
		}
	}*/
	public static int getStockLevelProp(int programCode)
	{
		if (programCode == 1)//ARV
			return Integer.parseInt(prop.getProperty("arv.stocklevel"));
		else if (programCode == 2)//HIV
			return Integer.parseInt(prop.getProperty("hiv.stocklavel"));
		else if (programCode == 3)//LAB
			return Integer.parseInt(prop.getProperty("lab.stocklevel"));
			else
				return Integer.parseInt(prop.getProperty("emlip.stocklevel"));				
	}
	
	public static double getEmergencyOrderPointProp()
	{
		return Double.parseDouble(prop.getProperty("facility.orderpoint"));
	}
	/**
	 * @return
	 */
	public static String getFacilityName() {
		
		return System.getProperty("facilityname");
	}
	public static String getProvince()
	{
		return System.getProperty("province");
	}
	public static String getDistrict()
	{
		return System.getProperty("district");
	}
	public static String getFacilityApprover()
	{
		return prop.getProperty("facility.approvername");
	}
	public static String getFacilityAgentCode()
	{
		return prop.getProperty("facility.agentcode");
	}
}
