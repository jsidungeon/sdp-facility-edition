/**
 * 
 */
package org.elmis.facility.main.gui;

import java.sql.SQLException;

import org.elmis.facility.domain.DarHivTests;

import com.oribicom.tools.NetworkMode;



/**
 * @author JBanda
 *
 */
public class SaveHivTestResult {
	
	DarHivTests darTestResults;

	/**
	 * 
	 */
	public SaveHivTestResult() {
		
		 darTestResults = new DarHivTests();
		
	}
	
	public boolean newHivTestResult(DarHivTests darTestResults) {
		
		 this.darTestResults =darTestResults ;
	

		String sql = "insert into dar_hiv_tests(id, facility_id, testing_point_Id,"+
			//" client_Id, purpose, " //date_of_test,"
				"purpose,"
			+ " screening,confirmatory, Negative_result, positive_result " +
				")" + "values( '"
				+ this.darTestResults.getId()
				+ "','"
				+ this.darTestResults.getFacilityId()
				+ "','"
				+ this.darTestResults.getTestingPointId()
				+ "','"
			//	+ this.darTestResults.getClientId()
			//	+ "','"
				+ this.darTestResults.getPurpose()
				+ "','"
				//+ this.darTestResults.getDateOfTest()
			//	+ "','"
				+this.darTestResults.getScreening()
				+ "','"	
				+this.darTestResults.getConfirmatory()
				+ "','"		
				+this.darTestResults.getNegativeResult()
				+ "','"				
				+this.darTestResults.getPositiveResult()
				+ "')";

		//System.out.println("task query is  " +sql);
		
		if (NetworkMode.c == null) {

			NetworkMode.getConn();
		}
		try {
			NetworkMode.c.createStatement().executeUpdate(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;

	}

}
