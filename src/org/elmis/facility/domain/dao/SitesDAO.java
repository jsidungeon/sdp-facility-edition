package org.elmis.facility.domain.dao;


import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.forms.users.Sites;


 

/**
 * Program DAO
 * 
 * @author Friday Simfukwe
 * http://jsi-zambia.com (English)
 
 */
public class SitesDAO {
	
	private SqlSessionFactory sqlSessionFactory; 
	
	public SitesDAO(){
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
			
		
	}
	

	/**
	 * Returns the list of all Users instances from the database.
	 * @return the list of all Users instances from the database.
	 */
	@SuppressWarnings("unchecked")
	public List<Sites> selectAll(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Sites> list = session.selectList("Sites.getAllSites");
			return list;
		} finally {
			session.close();
		}
	}
	
	
	/**
	 * Returns  a User instances from the database.
	 * @return  a User instances from the database with the matched password and username.
	 */
	
	
	@SuppressWarnings("unchecked")
	//public Users getByUser(@Param("password") String password,  @Param("username") String username){
     // Usae ibatis annotation here to get the logged in user from the database 
		/*HashMap map = new HashMap();
		map.put("password", pwd);
		map.put("username", username);*/
		public Sites getBySite( String sitename){	
		
		
		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			
			Sites usr = new Sites();
			usr.setname(sitename);
			Sites site = (Sites) session.selectOne("selectBySiteName", usr);
			return site;
		} finally {
			session.close();
		}
		
	}

	

	/**
	 * Returns a User instance from the database.
	 * @param id primary key value used for lookup.
	 * @return A user instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public users selectById(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			users user = (users) session.selectOne("Users.getById",id);
			return user;
		} finally {
			session.close();
		}
	}

	/**
	 * Updates an instance of User in the database.
	 * @param contact the instance to be updated.
	 */
	public void update(users user){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.update("Users.updateByPrimaryKey", user);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Insert an instance of User into the database.
	 * @param contact the instance to be persisted.
	 */
	public void insert(users user){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.insert("Users.insert", user);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Delete an instance of User from the database.
	 * @param id primary key value of the instance to be deleted.
	 */
	public void delete(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.delete("Users.deleteById", id);
			session.commit();
		} finally {
			session.close();
		}
	}
}
