package org.elmis.facility.reporting.model;

public class IndicatorProduct extends Product{


@Override
	public String toString() {
		return "IndicatorProduct [emergencyOrderPoint=" + emergencyOrderPoint
				+ ", balance=" + balance + ", getId()=" + getId()
				+ ", getProgramId()=" + getProgramId() + ", getProductCode()="
				+ getProductCode() + ", getPrimaryName()=" + getPrimaryName()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
public int getBalance() {
	return balance;
}
public void setBalance(int balance) {
	this.balance = balance;
}
private int emergencyOrderPoint;
public int getEmergencyOrderPoint() {
	return emergencyOrderPoint;
}

public void setEmergencyOrderPoint(int emergencyOrderPoint) {
	this.emergencyOrderPoint = emergencyOrderPoint;
}
private int balance;
}
