package org.elmis.facility.reporting.model;

public class Product {
 private int id;
 private int programId;
 private String productCode;
 private String primaryName;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getProgramId() {
	return programId;
}
public void setProgramId(int programId) {
	this.programId = programId;
}
public String getProductCode() {
	return productCode;
}
public void setProductCode(String productCode) {
	this.productCode = productCode;
}
public String getPrimaryName() {
	return primaryName;
}
public void setPrimaryName(String primaryName) {
	this.primaryName = primaryName;
}
}
