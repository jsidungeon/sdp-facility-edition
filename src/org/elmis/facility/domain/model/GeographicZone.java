package org.elmis.facility.domain.model;

public class GeographicZone {
	
	private int id;
	private String code;
	private String geographicZoneName;
	public String getGeographicZoneName() {
		return geographicZoneName;
	}
	public void setGeographicZoneName(String geographicZoneName) {
		this.geographicZoneName = geographicZoneName;
	}
	private int levelId;
	private int parent;
	private String levelName;
	public String getLevelName() {
		return levelName;
	}
	@Override
	public String toString() {
		return "GeographicZone [id=" + id + ", code=" + code
				+ ", geographicZoneName=" + geographicZoneName + ", levelId="
				+ levelId + ", parent=" + parent + ", levelName=" + levelName
				+ "]";
	}
	public void setLevelName(String levelName) {
		this.levelName = levelName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getLevelId() {
		return levelId;
	}
	public void setLevelId(int levelId) {
		this.levelId = levelId;
	}
	public int getParent() {
		return parent;
	}
	public void setParent(int parent) {
		this.parent = parent;
	}
}
