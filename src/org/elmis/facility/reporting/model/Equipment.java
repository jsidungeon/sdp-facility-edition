package org.elmis.facility.reporting.model;

public class Equipment {
	private int id;
	private int counter;
	private boolean isFunctional;
	private char testsHandled;
	public char getTestsHandled() {
		return testsHandled;
	}
	public void setTestsHandled(char testsHandled) {
		this.testsHandled = testsHandled;
	}
	@Override
	public String toString() {
		return "Equipment [id=" + id + ", counter=" + counter
				+ ", isFunctional=" + isFunctional + ", equipCode=" + equipCode
				+ ", equipAvailable=" + equipAvailable + ", name=" + name + "]";
	}
	public boolean isFunctional() {
		return isFunctional;
	}
	public void setFunctional(boolean isFunctional) {
		this.isFunctional = isFunctional;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String equipCode;
	private boolean equipAvailable;
	public String getEquipCode() {
		return equipCode;
	}
	public void setEquipCode(String equipCode) {
		this.equipCode = equipCode;
	}
	
	public boolean isEquipAvailable() {
		return equipAvailable;
	}
	public void setEquipAvailable(boolean equipAvailable) {
		this.equipAvailable = equipAvailable;
	}
	private String name;	
}
