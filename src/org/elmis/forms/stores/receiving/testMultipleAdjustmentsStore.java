package org.elmis.forms.stores.receiving;

/***************************************************
	 * STARTING CHILD FORM FOR ADJUSTMENTS 
	 ***************************************************/
/**
 *
 * @author  __MKausa__
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import javax.swing.ComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.text.JTextComponent;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Adjustment;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.forms.stores.arv_dispensing.arvdispensingProductsPhysicalCountJD;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.domain.model.facilities;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.elmis.forms.stores.arv_dispensing.arvmessageDialog;
import org.elmis.forms.stores.receiving.ProductsPhysicalCountJD;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import com.oribicom.tools.TableModel;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.JRadioButton;

import java.awt.event.MouseAdapter;

public class testMultipleAdjustmentsStore extends javax.swing.JDialog   {
	
	private int         col;
	private int        rowz; 
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	private Boolean Adjustmentadditive;
	public String typeCode;
	TableColumnAligner tablecolumnaligner; 
	private Double calAdjustmenttotal;
	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	Integer ProdQty = null;
	private Boolean Transfer_IN = false;
	private Boolean Transfer_OUT = false;
	private Double  showdouble  ;  
	public String paramAdjustment = null; 
	public String Adjustmentform = "";
	public String prodAdjustremark = "";
	private List<Character> charList = new LinkedList();
	private char letter;
	private String selectedfacility = "";
	private String searchText = "";
	List<VW_Systemcalculatedproductsbalance> productsPhysicalcountList = new LinkedList();
	//Facility Approved Products JTable *******************
	List<VW_Program_Facility_ApprovedProducts> adjustmensproductsList = new LinkedList();
	//Combo filter serach *********************************************************************************
    JComboBox comboBox;
    ComboBoxModel model;
    JTextComponent editor;
    boolean selecting=false;
    //*****************************************************************************************************
    List<facilities> facnameList =  new LinkedList(); 
    List<facilities> facilitiesnameList =  new LinkedList();
	private Elmis_Stock_Control_Card Adjustmentelmis_stock_control_card; 
	private Elmis_Stock_Control_Card Adjustmentsavestockcontrolcard;
	public Boolean  productsearchenabled = false;
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	//public static int total_programs = 0;
	//public static Map parameterMap_fproducts = new HashMap();
	private JComboBox facilityList = new JComboBox();
	MutableComboBoxModel modelfacilities = (MutableComboBoxModel) facilityList
			.getModel();
	private JComboBox facilitynamecomboList = new JComboBox();
	MutableComboBoxModel modelfacilitiesList = (MutableComboBoxModel) facilitynamecomboList
			.getModel();
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	ARVDispensingDAO aFacilityproductsmapper = null;
	private ListIterator<Losses_Adjustments_Types> registeradjustmentfapprovedproductsIterator = null;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//List<Shipped_Line_Items>  shippedItemsList =  new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator elmistockcontrolcarditerator = elmisstockcontrolcardList
			.listIterator();
	private Losses_Adjustments_Types registerAdjustmentsfacilitysccProducts;
	FacilityProductsDAO Facilityproductsmapper = null;
	FacilitySetUpDAO facilitiesmapper =  null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	//public static Boolean cansave = false;
	private String testAdjustcall;
	private String Pname = "";
	private String Pstrength = "";
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer Productid = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private String facilityfromadjustmentname = "";
	private String facilitytoadjustmentname = "";
	private String selectedfacilitytoname = "";
	private String selectedfacilityfromname = "";
	ArrayList<Elmis_Stock_Control_Card> adjustmentstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	//List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//private VW_Systemcalculatedproductsbalance facilitysccProducts;
	public  List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	private final String[] columns_RegisterAdjustmentsfacilityApprovedproducts = {
		"Adjustment Type", "Quantity", "Remarks", "Additive"};
	private static final int rows_RegisterAdjustmentsfproducts = 0;
    private final Object[] defaultv_RegisterAdjustmentsfacilityapprovedproducts = { "", "",
		"",""};

	public TableModel tableModel_RegisterAdjustments= new TableModel(
			columns_RegisterAdjustmentsfacilityApprovedproducts,
			defaultv_RegisterAdjustmentsfacilityapprovedproducts, rows_RegisterAdjustmentsfproducts) {

		boolean[] canEdit = new boolean[] { false, true, true};

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				
				if (columnIndex == 2) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,2).getClass();
					// }	
				}
				if (columnIndex == 1) {
					//if(getValueAt(0, 3) != null){
					//  return Integer.class;
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		

	};


	/*private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();*/

	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public testMultipleAdjustmentsStore(java.awt.Frame parent, boolean modal,String adjustpcode,int adjustproductid,Timestamp adjustmentdateandtime,String progname,String pname, String pstrenth,Double adjusttotal,String adjustmentcaller,String formcaller) {
		super(parent, modal);
		
		Adjustmentform = formcaller;
		this.testAdjustcall = adjustmentcaller;
		calAdjustmenttotal = adjusttotal;
		productdeliverdate = adjustmentdateandtime;
		Pcode = adjustpcode;
		Productid = adjustproductid;
		facilityprogramcode = progname;
		Pname = pname;
		Pstrength = pstrenth;
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(1001, 450);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//AdjustmentTypeJCB1.setPreferredSize(new Dimension(200, 30));

	}
	
	private void hideColumn(TableColumn column){
		column.setMinWidth(0);
		column.setMaxWidth(0);
		column.setPreferredWidth(0);
	}
	
	private void changeColumnWidth(TableColumn column , int w){
		column.setMinWidth(w);
		column.setMaxWidth(w);
		column.setPreferredWidth(w);
	}
	public testMultipleAdjustmentsStore(java.awt.Frame parent, boolean modal,String adjustpcode,int adjustproductid,Timestamp adjustmentdateandtime,String progname,String pname, String pstrenth,String formcaller) {
		super(parent, modal);
		
		Adjustmentform = formcaller;
		productdeliverdate = adjustmentdateandtime;
		Pcode = adjustpcode;
		Productid = adjustproductid;
		facilityprogramcode = progname;
		Pname = pname;
		Pstrength = pstrenth;
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(1001, 450);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//AdjustmentTypeJCB1.setPreferredSize(new Dimension(200, 30));

	}

	
	
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		SaveJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Store Adjustments ");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
			public void windowActivated(java.awt.event.WindowEvent evt) {
				
				formwindowActivated(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		try{
		RegisterAdjustmentJT.setDefaultRenderer(Object.class, new CustomModel());
		//AdjustmentfacilityapprovedProductsJT.addMouseListener(new CustomListener());
		RegisterAdjustmentJT.addKeyListener(new CustomListener1());
		}catch(java.lang.NullPointerException n){
			n.getMessage();
		}
		JButton btnCancel = new JButton();
		btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCancelMouseClicked(evt);
			}

			
		});
		btnCancel.setText("Cancel");
		btnCancel.setFont(new Font("Ebrima", Font.BOLD, 12));
		
		enterfacilityname = new JLabel("Transfer from facility");
		enterfacilityname.setIcon(new ImageIcon(testMultipleAdjustmentsStore.class.getResource("/elmis_images/eLMIS basic info small.png")));
		enterfacilityname.setFont(new Font("Ebrima", Font.BOLD, 13));
		enterfacilityname.setForeground(Color.WHITE);
		
	   transfertofacilityJCB = new JComboBox();
	   transfertofacilityJCB.addItemListener(new ItemListener() {
	   	public void itemStateChanged(ItemEvent evt) {
	   		checkSelectedFacilitytoName(evt);
	   	}

		
	   });
	   transfertofacilityJCB.addKeyListener(new KeyAdapter() {
	   	@Override
	   	public void keyTyped(KeyEvent evt) {
	   		transfertofacilityJCBkeyTyped(evt);	
	   	}

		
	   });
		transfertofacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transfertofacilityJCBActionPerformed(evt);
				
			}
		});
		transfertofacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transfertofacilityJCB.setModel(modelfacilities);
	    transferoutJL = new JLabel("Transfer to facility");
		transferoutJL.setIcon(new ImageIcon(testMultipleAdjustmentsStore.class.getResource("/elmis_images/eLMIS basic info small.png")));
		transferoutJL.setForeground(Color.WHITE);
		transferoutJL.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		 transferfromfacilityJCB = new JComboBox();
		 transferfromfacilityJCB.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent evt) {
		 		checkSelectedFacilityfromName(evt);
		 	}
		 });
		 transferfromfacilityJCB.addKeyListener(new KeyAdapter() {
		 	@Override
		 	public void keyTyped(KeyEvent evt) {
		 		transferfromfacilityJCBkeyTyped(evt);
		 	}

		
		 });
		 
		 
		 
		transferfromfacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transferfromfacilityJCBActionPerformed(evt);
			}
		});
		
	
		transferfromfacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transferfromfacilityJCB.setModel(modelfacilitiesList);
		
		JLabel lblNewLabel = new JLabel("Adjustments for : ");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		Adjustmentproductnamelbl = new JLabel("");
		Adjustmentproductnamelbl.setForeground(Color.WHITE);
		Adjustmentproductnamelbl.setFont(new Font("Ebrima", Font.BOLD, 14));
		
	    transferoutrdbtn = new JRadioButton("Transfer Out");
		transferoutrdbtn.addMouseListener(new MouseAdapter() {
		
			@Override
			public void mouseClicked(MouseEvent evt) {
				transferOutComboEnabled(evt);
			}
		});
		transferoutrdbtn.setBackground(new java.awt.Color(102, 102, 102));
		transferoutrdbtn.setForeground(Color.WHITE);
		transferoutrdbtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		
	    transferinrdbtn = new JRadioButton("Transfer In");
		transferinrdbtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				transferInComboEnabled(evt);
			}

			
		});
		transferinrdbtn.setBackground(new java.awt.Color(102, 102, 102));
		transferinrdbtn.setForeground(Color.WHITE);
		transferinrdbtn.setFont(new Font("Ebrima", Font.BOLD, 12));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(39)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(Adjustmentproductnamelbl, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
					.addGap(424))
				.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(transferoutrdbtn, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
							.addContainerGap())
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(SaveJBtn))
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
										.addGroup(jPanel1Layout.createSequentialGroup()
											.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
												.addComponent(transfertofacilityJCB, Alignment.LEADING, 0, 350, Short.MAX_VALUE)
												.addComponent(transferfromfacilityJCB, Alignment.LEADING, 0, 350, Short.MAX_VALUE)
												.addComponent(enterfacilityname, Alignment.LEADING)
												.addComponent(transferinrdbtn, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
											.addGap(46))
										.addGroup(jPanel1Layout.createSequentialGroup()
											.addComponent(transferoutJL)
											.addPreferredGap(ComponentPlacement.RELATED)))
									.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 532, GroupLayout.PREFERRED_SIZE)))
							.addGap(55))))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(Adjustmentproductnamelbl, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(31)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(SaveJBtn)
								.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(3)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(transferoutrdbtn)
								.addComponent(transferinrdbtn))
							.addGap(43)
							.addComponent(enterfacilityname)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transferfromfacilityJCB, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
							.addGap(33)
							.addComponent(transferoutJL)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transfertofacilityJCB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(209, Short.MAX_VALUE))
		);

	
		RegisterAdjustmentJT = new JTable();
		scrollPane.setViewportView(RegisterAdjustmentJT);
		RegisterAdjustmentJT.setShowVerticalLines(false);
		RegisterAdjustmentJT.setShowHorizontalLines(true);
		RegisterAdjustmentJT.setShowGrid(false);
		RegisterAdjustmentJT.setRowSelectionAllowed(true);
		RegisterAdjustmentJT.setModel(tableModel_RegisterAdjustments);
		hideColumn(RegisterAdjustmentJT.getColumnModel().getColumn(3));
		changeColumnWidth(RegisterAdjustmentJT.getColumnModel().getColumn(0), 300);
		RegisterAdjustmentJT.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		RegisterAdjustmentJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						RegisterAdjustmentJTMouseClicked(evt);
					}
				});
		RegisterAdjustmentJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						RegisterAdjustmentJTPropertyChange(evt);
					}

					
				});
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 995, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	
	
	
	 public class CustomModel extends DefaultTableCellRenderer {

	        private static final long   serialVersionUID    = 1L;

	        @Override
	        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	            JLabel label = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	            Color c = Color.WHITE;
	            if (isSelected && row == rowz & column == col)
	                c = Color.GREEN;
	            label.setBackground(c);
	            return label;
	        }
	    }

	
public class CustomListener1 extends KeyAdapter  {
	 public void keyTyped(KeyEvent e) {   
		/* if(e.getKeyCode() == KeyEvent.VK_ENTER){
			 rowz = AdjustmentfacilityapprovedProductsJT.getSelectedRow();
	            col = AdjustmentfacilityapprovedProductsJT.getSelectedColumn();
	            // Repaints JTable
	            AdjustmentfacilityapprovedProductsJT.repaint();
          }*/
		// if(e.getKeyCode() == KeyEvent.VK_TAB){
			 rowz = RegisterAdjustmentJT.getSelectedRow();
	         col = RegisterAdjustmentJT.getSelectedColumn();
	            // Repaints JTable
	         RegisterAdjustmentJT.repaint();
          //}
        // Select the current cell
        
    }
}
	

	@SuppressWarnings("deprecation")
	protected void transferOutComboEnabled(MouseEvent evt) {
		// TODO Auto-generated method stub
		this.Transfer_OUT = true;
		this.transfertofacilityJCB.setEnabled(true);
        this.transferoutJL.setEnabled(true);
        this.transferfromfacilityJCB.setEnabled(false);
        this.enterfacilityname.setEnabled(false);
       this.transferinrdbtn.setEnabled(false);
        
	}
	
	
	private void transferInComboEnabled(MouseEvent evt) {
		// TODO Auto-generated method stub
		    Transfer_IN = true;
			this.transferfromfacilityJCB.setEnabled(true);
	        this.enterfacilityname.setEnabled(true);
	        
			this.transfertofacilityJCB.setEnabled(false);
	        this.transferoutJL.setEnabled(false);
	        this.transferoutrdbtn.setEnabled(false);
			//this.createshipmentObj(Pcode, Productid, productdeliverdate);
		
	}

	private void checkSelectedFacilitytoName(ItemEvent evt) {
		// TODO Auto-generated method stub
		if (evt.getStateChange() == ItemEvent.SELECTED) {
	          Object item = evt.getItem();
	          // do something with object
	          selectedfacilitytoname = item.toString();
	       }
	}
   	
	
	protected void checkSelectedFacilityfromName(ItemEvent evt) {
		
		// TODO Auto-generated method stub
				if (evt.getStateChange() == ItemEvent.SELECTED) {
			          Object item = evt.getItem();
			          // do something with object
			          selectedfacilityfromname = item.toString();
			       }
				//selecteddispensaryname = (String) cb.getSelectedItem();
	}

	protected void transferfromfacilityJCBActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		
		JComboBox cb = (JComboBox) evt.getSource();
		facilityfromadjustmentname = (String) cb.getSelectedItem();
		System.out.println(facilityfromadjustmentname);
		
	}


	protected void transfertofacilityJCBActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox) evt.getSource();
		facilitytoadjustmentname = (String) cb.getSelectedItem();
		System.out.println(facilitytoadjustmentname);
	}


	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		return productdeliverdate;

	}
	public  void RegisterAdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		System.out.println("test pop up adjustments");		

	}

	
	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List registerAdjustmentfapprovProducts) {
		checkbox = new JCheckBox();
		
		tableModel_RegisterAdjustments.clearTable();

		registeradjustmentfapprovedproductsIterator = registerAdjustmentfapprovProducts.listIterator();

			while (registeradjustmentfapprovedproductsIterator.hasNext()) {

				registerAdjustmentsfacilitysccProducts = registeradjustmentfapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				String desciption = registerAdjustmentsfacilitysccProducts.getDescription();
				if((!desciption.contains("+") && registerAdjustmentsfacilitysccProducts.getAdditive().booleanValue())){
					desciption += " (+)"; 
				}
				
				if((!desciption.contains("-") && !registerAdjustmentsfacilitysccProducts.getAdditive().booleanValue())){
					desciption += " (-)"; 
				}
				defaultv_RegisterAdjustmentsfacilityapprovedproducts[0] = desciption;
			    
				defaultv_RegisterAdjustmentsfacilityapprovedproducts[3]	 = registerAdjustmentsfacilitysccProducts.getAdditive();	
				
				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_RegisterAdjustmentsfacilityApprovedproducts.length; j++) {
					cols.add(defaultv_RegisterAdjustmentsfacilityapprovedproducts[j]);

				}
				System.out.print(cols.size()
						+ "What is the SIZE of Array to Table??");
				tableModel_RegisterAdjustments.insertRow(cols);

				registeradjustmentfapprovedproductsIterator.remove();
			

	}
	
	}	

	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
 		
	    String dispensaryName = AppJFrame.getDispensingPointName(facilityprogramcode);
		int siteId = AppJFrame.getDispensingId(facilityprogramcode);
		int rowCount = RegisterAdjustmentJT.getRowCount();
		java.util.Date date = new java.util.Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		aFacilityproductsmapper = new ARVDispensingDAO();
		Double showdouble = 0.0;   
		List<Adjustment> adjustmentList = new ArrayList<>();
		Adjustmentsavestockcontrolcard = new Elmis_Stock_Control_Card();
		Timestamp amodifieddate;
		if (productdeliverdate == null) {
			java.util.Date create_date = new java.util.Date();
			amodifieddate = new Timestamp(create_date.getTime());
		} else {
			amodifieddate = productdeliverdate;
		}
		for (int i = 0; i < rowCount; i++)
		{
			try {
				
				Object adjustmentQtyObj = tableModel_RegisterAdjustments.getValueAt(i,1);
				Object adjustmentRmksObj = tableModel_RegisterAdjustments.getValueAt(i, 2);
				
				if (adjustmentQtyObj != null){
					
					//changes to validate for adjustment balance 
					Double positiveAdjustment = 0.0;
				
					positiveAdjustment = (Double.parseDouble
							(adjustmentQtyObj.toString()));
					if(!(positiveAdjustment.compareTo(0.0) == 0)){
								
						this.Adjustmentadditive = Boolean.parseBoolean(this.RegisterAdjustmentJT.getValueAt(rowindex,3).toString());
					}
					
					if (positiveAdjustment < 0)
						positiveAdjustment = positiveAdjustment * -1;
					//check adjustment value against dispensary value
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,Pcode);
					}
					
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							showdouble = b.getBalance();
							if(Adjustmentadditive){
								Adjustmentsavestockcontrolcard.setDpid(siteId);
								Adjustmentsavestockcontrolcard.setProductcode(Pcode);
								Adjustmentsavestockcontrolcard.setProductid(Productid);
								Adjustmentsavestockcontrolcard.setId(this.GenerateGUID());
								Adjustmentsavestockcontrolcard.setBalance(showdouble + positiveAdjustment);
								
								String adjustmentType = ""+RegisterAdjustmentJT.getValueAt(i, 0);
								Adjustmentsavestockcontrolcard.setAdjustmenttype(adjustmentType);
								if (Adjustmentadditive)
									Adjustmentsavestockcontrolcard.setAdjustments(positiveAdjustment);
								else
									Adjustmentsavestockcontrolcard.setAdjustments(positiveAdjustment * -1);
								if (Transfer_IN){
									Adjustmentsavestockcontrolcard.setIssueto_receivedfrom(selectedfacilityfromname);
									
								}
								else if (Transfer_OUT){
									Adjustmentsavestockcontrolcard.setIssueto_receivedfrom(selectedfacilitytoname);
									
								}
								else{
									Adjustmentsavestockcontrolcard.setIssueto_receivedfrom(adjustmentType);
									
								}
								if (adjustmentRmksObj != null)
									Adjustmentsavestockcontrolcard.setRemark(adjustmentRmksObj.toString());
								else
									Adjustmentsavestockcontrolcard.setRemark("");
												
								Adjustmentsavestockcontrolcard.setStoreroomadjustment(true);
								Adjustmentsavestockcontrolcard.setProgram_area(facilityprogramcode);
								Adjustmentsavestockcontrolcard.setCreatedby(AppJFrame.userLoggedIn);
								Adjustmentsavestockcontrolcard.setCreateddate(amodifieddate);
								adjustmentstockcontrolcardList.add(Adjustmentsavestockcontrolcard);
							}else{
							
							
					if( b.getBalance() >= positiveAdjustment){
					
					Adjustmentsavestockcontrolcard.setDpid(siteId);
					Adjustmentsavestockcontrolcard.setProductcode(Pcode);
					Adjustmentsavestockcontrolcard.setProductid(Productid);
					Adjustmentsavestockcontrolcard.setId(this.GenerateGUID());
					Adjustmentsavestockcontrolcard.setBalance(showdouble - positiveAdjustment);
					
					String adjustmentType = ""+RegisterAdjustmentJT.getValueAt(i, 0);
					Adjustmentsavestockcontrolcard.setAdjustmenttype(adjustmentType);
					if (Adjustmentadditive)
						Adjustmentsavestockcontrolcard.setAdjustments(positiveAdjustment);
					else
						Adjustmentsavestockcontrolcard.setAdjustments(positiveAdjustment * -1);
					if (Transfer_IN){
						Adjustmentsavestockcontrolcard.setIssueto_receivedfrom(selectedfacilityfromname);
						
					}
					else if (Transfer_OUT){
						Adjustmentsavestockcontrolcard.setIssueto_receivedfrom(selectedfacilitytoname);
						
					}
					else{
						Adjustmentsavestockcontrolcard.setIssueto_receivedfrom(adjustmentType);
						
					}
					if (adjustmentRmksObj != null)
						Adjustmentsavestockcontrolcard.setRemark(adjustmentRmksObj.toString());
					else
						Adjustmentsavestockcontrolcard.setRemark("");
									
					Adjustmentsavestockcontrolcard.setStoreroomadjustment(true);
					Adjustmentsavestockcontrolcard.setProgram_area(facilityprogramcode);
					Adjustmentsavestockcontrolcard.setCreatedby(AppJFrame.userLoggedIn);
					Adjustmentsavestockcontrolcard.setCreateddate(amodifieddate);
					adjustmentstockcontrolcardList.add(Adjustmentsavestockcontrolcard);
				}else{
					
					if(!(Adjustmentadditive)){
					JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than product \n" +" Store balance \n\n" +
							"Store balance = "+showdouble,
										"Adjustment Error", JOptionPane.ERROR_MESSAGE);
					
					}
				}				
				}
				}
					
		}					
				
			}catch(NumberFormatException e){
				e.getStackTrace();
			}

			
			
		}
		
		if(!(adjustmentstockcontrolcardList.isEmpty())){
			// Need to reduce the quantities of balance at dispensary - Moses Kausa 2014
			//new updateARVproductBalances(adjustmentList);
			
			Facilityproductsmapper
			.AdjustmentInsertstockcontrolcardProducts(adjustmentstockcontrolcardList);
			//new AdjustmentDao().saveArvAdjustments(adjustmentList);
			javax.swing.JOptionPane.showMessageDialog(this,Pname+" ("+Pcode+ ")\n adjustments have been saved.");
			dispose();
			
			}
				
		    if(!( this.transferoutrdbtn.isEnabled())){
			 this.transferoutrdbtn.setEnabled(true);
			 
			 }
			 if(!( this.transferinrdbtn.isEnabled())){
				 this.transferinrdbtn.setEnabled(true);
			 }
	
		
		
	}
	
	
	
	private void transfertofacilityJCBkeyTyped(KeyEvent evt) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox) evt.getSource();
		searchText = cb.getSelectedItem().toString();
		System.out.println(this.arvsearchproductsList.size() + "list not null");

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
	
			 System.out.println("I detedcted a back space ah! did you call me ??");
	           //re populate table with original list 
			 if(searchText.length() == 0){
	           this.callbackAllfacilitylist();
			 }
			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}

			for (char c : charList) {

				s.append(c);

			}
		} else {

			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			//get ARV list 
			try {
				if (!(searchText == "")) {

			

					facilitiesnameList = facilitiesmapper.getfacilitynameList();

						for (facilities p : facilitiesnameList) {

							if (p.getName().toLowerCase().contains(s.toString().toLowerCase())) {
								this.facnameList.add(p);
							}
						}

						//	System.out.println("Delete " + s);
						//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						this.searchPopulatetransfertofacilityJCB(
								facnameList);

				
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}

		}

		
		
		
	}
	
	private void transferfromfacilityJCBkeyTyped(KeyEvent evt) {
		// TODO Auto-generated method stub
		
		JComboBox cb = (JComboBox) evt.getSource();
		searchText = cb.getSelectedItem().toString();
		System.out.println(this.arvsearchproductsList.size() + "list not null");

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
	
			 System.out.println("I detedcted a back space ah! did you call me ??");
	           //re populate table with original list 
			 if(searchText.length() == 0){
	           this.callbackAllfacilitylist();
			 }
			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}

			for (char c : charList) {

				s.append(c);

			}
		} else {

			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			//get ARV list 
			try {
				if (!(searchText == "")) {

			

					facilitiesnameList = facilitiesmapper.getfacilitynameList();

						for (facilities p : facilitiesnameList) {

							if (p.getName().toLowerCase().contains(s.toString().toLowerCase())) {
								this.facnameList.add(p);
							}
						}

						//	System.out.println("Delete " + s);
						//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						this.searchPopulatetransferfromfacilityJCB(
								facnameList);

				
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}

		}

		
	}
	
	private void callbackAllfacilitylist(){
		//get ARV list 
		try {
			
			Facilityproductsmapper = new FacilityProductsDAO();
			elmis_stock_control_card = new Elmis_Stock_Control_Card();
			facilitiesmapper = new FacilitySetUpDAO();
			facilitiesnameList = facilitiesmapper.getfacilitynameList();
			
			for (facilities f : facilitiesnameList) {
				modelfacilities.addElement(f.getName());

			}
	        
			for (facilities f : facilitiesnameList) {
				this.modelfacilitiesList.addElement(f.getName());

			}
						

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}

private void searchPopulatetransfertofacilityJCB(List<facilities> fsearchnameas){
	for (facilities f : facilitiesnameList) {
		modelfacilities.addElement(f.getName());

	}
}
	
	
	private void searchPopulatetransferfromfacilityJCB(List<facilities> fsearchnameas){
		

		for (facilities f : fsearchnameas) {
			this.modelfacilitiesList.addElement(f.getName());

		}
		
	}
	private void btnCancelMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		testMultipleAdjustmentsStore.this.dispose();
	}
	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}
	
	protected void RegisterAdjustmentJTMouseClicked(
			MouseEvent evt) {
			System.out.println("test me now ");
			
		this.adjustmentname = this.RegisterAdjustmentJT.getValueAt(RegisterAdjustmentJT.getSelectedRow(),0).toString();
		
		System.out.println(adjustmentname);
		
		if(this.adjustmentname.equals("TRANSFER_IN")) {
		//this.transferfromfacilityJCB.setEnabled(true);
         //  this.enterfacilityname.setEnabled(true);
        
		//this.transfertofacilityJCB.setEnabled(false);
       // this.transferoutJL.setEnabled(false);
       
		//this.createshipmentObj(Pcode, Productid, productdeliverdate);
		}else if (this.adjustmentname.equals("TRANSFER_OUT")){
			//this.transfertofacilityJCB.setEnabled(true);
	      //  this.transferoutJL.setEnabled(true);
	       // this.transferfromfacilityJCB.setEnabled(false);
	       // this.enterfacilityname.setEnabled(false);
	        
	       
		}else{
			//this.transfertofacilityJCB.setEnabled(false);
			//this.transferfromfacilityJCB.setEnabled(false);;
	       // this.enterfacilityname.setEnabled(false);
	       // this.transferoutJL.setEnabled(false);
		}
		
	}
	
	
	private boolean checktableCellinput(String s){
		
		 try { 
			 
			 if (s.contains(".")){
				Double.parseDouble(s) ;
			 }else{
		        Integer.parseInt(s); 
			 }
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    // only got here if we didn't return false
		    return true;
}		
	
/*private boolean checktableCellinput(String s){
		
		 try { 
		        Integer.parseInt(s); 
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    // only got here if we didn't return false
		    return true;
}*/	
private void RegisterAdjustmentJTPropertyChange(
			PropertyChangeEvent evt) {
		// TODO Auto-generated method stub

if ("tableCellEditor".equals(evt.getPropertyName())) {       

	if (this.RegisterAdjustmentJT.isColumnSelected(1)) {
		System.out.println("IM READY TO POP FORM- 2014");
		int col = RegisterAdjustmentJT.getSelectedColumn();
		 if (RegisterAdjustmentJT.isEditing())
		    {
		    	//get the adjustment type , quantity entered , and  or remarks 
			
				colindex = this.RegisterAdjustmentJT
						.getSelectedColumn();
				rowindex = this.RegisterAdjustmentJT
						.getSelectedRow();
			
				
		    }else{
		    	
		    	 String num;
		    	   try{
				    num = (RegisterAdjustmentJT.getValueAt(RegisterAdjustmentJT.getSelectedRow(), 1).toString());
				    if(!(checktableCellinput(num))){
				    	JOptionPane.showMessageDialog(null, String.format("Adjustment Quantity must be a positive number",  num));	
				    	//RegisterAdjustmentJT.editCellAt(RegisterAdjustmentJT.getSelectedRow(), 1);
				    }
				    if((checktableCellinput(num))){   
				    	//check if 
				    	
		    	      createshipmentObj(Pcode, Productid, productdeliverdate);
				    }
		    	   }catch(java.lang.NullPointerException n){
		    		   n.getMessage();
		    	   }
	}					


}
	
		
		
}
	


	}



	public void createshipmentObj(String apcode, int prodid,
			Timestamp amodifieddate) {
		//System.out.println("Did object call me ???");
				//created a shipment object 
			/*	Double A = 0.0;
				Double B = 0.0;
				double C = 0.0;
				double negativeadjust = 0.0;
				Double positive = 0.0;
				showdouble = 0.0 ;
				if (productdeliverdate == null) {
					java.util.Date date = new java.util.Date();
					amodifieddate = new Timestamp(date.getTime());
				} else {
					amodifieddate = productdeliverdate;
				}
				
					

				System.out.println(amodifieddate);
				System.out.println(A);
				System.out.println(B);
				System.out.println(C);
		
				
			     prodAdjustremark = this.tableModel_RegisterAdjustments.getValueAt(rowindex,2)
						.toString();
				

				if (!(prodAdjustremark.equals(""))) {
					Adjustmentelmis_stock_control_card.setRemark(prodAdjustremark);
				}
				Adjustmentelmis_stock_control_card.setProductcode(apcode);

				Adjustmentelmis_stock_control_card.setProductid(prodid);
				
		        
				if (this.adjustmentname.equalsIgnoreCase("TRANSFER_OUT")) {
					
				
					try {
						
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
								.toString()));
				       if (negativeadjust < 0)
					    negativeadjust = negativeadjust * -1;
				        negativeadjust = - negativeadjust;
				    	Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
				        System.out.println(negativeadjust);
				        positive = negativeadjust * - 1;
						
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							showdouble = b.getBalance();
							if( b.getBalance() >= positive){
							if( b.getProductcode().equals(apcode)){
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							//negativeadjust = Adjustmentelmis_stock_control_card.getAdjustments();
								
							}
						
					
				
					
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					if(!(this.facilitytoadjustmentname.equals(""))){
						//check if combo value changed 
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.facilitytoadjustmentname);
					}
			
					
					
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
	
										
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
					"Store balance = "+showdouble,
								"Adjustment Error", JOptionPane.ERROR_MESSAGE);
						showdouble = 0.0;
						tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
				} 
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
				}
				
				
				if (this.adjustmentname.equalsIgnoreCase("RETURN (+)")) {
					
		
					try {
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						
						System.out.println(negativeadjust);
						positive = negativeadjust ;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					

					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Return (+)");
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
					

						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
                  
					
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
				}
				
				
				
				
				
				
				if (this.adjustmentname.equalsIgnoreCase("TRANSFER_IN")) {
					
		
					try {
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(positive);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}

					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					 if(!(selectedfacilityfromname.equals(""))){
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.selectedfacilityfromname);
					 }
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
					
	
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}

				}
			 
				this.adjustmentname.trim();
				if (this.adjustmentname.equalsIgnoreCase("DAMAGED")) {
				
	
					
		         try{
		        	 
		        	 
		        		//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
		        	 negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
		        	 if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
		        	       	 
						negativeadjust = - negativeadjust;
					 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							showdouble = b.getBalance();
							if( b.getBalance() >= positive ){
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive ;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
						
						
						
				
		        

					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Damaged");
				
					
					}else{
							JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
						"Store balance = "+showdouble,
									"Adjustment Error", JOptionPane.ERROR_MESSAGE);
							showdouble = 0.0;
							tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
						}
					}
					
			
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
				
				
					 }catch(NumberFormatException e){
							e.getStackTrace();
						}
				}
				
				
			
				
				if (this.adjustmentname.equalsIgnoreCase("RETURN (-)")) {
					
					
					
			         try{
			        	 negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
											.toString()));
			        	 if (negativeadjust < 0)
			        			negativeadjust = - (negativeadjust);
							
						 negativeadjust = - (negativeadjust);
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						negativeadjust = - (negativeadjust);
						positive = negativeadjust ;
						
											
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							showdouble = b.getBalance();
							if(b.getBalance() >= positive){
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive ;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}

						Adjustmentelmis_stock_control_card.setQty_received(0.0);
						Adjustmentelmis_stock_control_card.setQty_issued(0.0);
						Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Return (-)");
					
						
						}else{
							JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
									"Store balance = "+showdouble,
												"Adjustment Error", JOptionPane.ERROR_MESSAGE);
										showdouble = 0.0;
										tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
									}
						
						}
							aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
				
						 }catch(NumberFormatException e){
								e.getStackTrace();
							}
					}
					
	
				if (this.adjustmentname.equalsIgnoreCase("LOST")) {

					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}	
						
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							this.showdouble = b.getBalance();
							if(b.getBalance() >= positive){
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive ;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}	
										
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Lost");
			
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
					"Store balance = "+showdouble,
								"Adjustment Error", JOptionPane.ERROR_MESSAGE);
						showdouble = 0.0;
						tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
					
					
				}
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
		
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
				}
					 
				
				if (this.adjustmentname.equalsIgnoreCase("STOLEN")) {

					
					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
					
						
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							this.showdouble = b.getBalance();
							if(b.getBalance() >= positive){
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
						
				
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Stolen");
			
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
					"Store balance = "+showdouble,
								"Adjustment Error", JOptionPane.ERROR_MESSAGE);
						showdouble = 0.0;
						tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
						}
					
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
			
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					
				}
					 
				

				if (this.adjustmentname.equalsIgnoreCase("EXPIRED")) {

								
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = - (negativeadjust);
						
						 negativeadjust = - (negativeadjust);
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						negativeadjust = - (negativeadjust);
						positive = negativeadjust ;
						
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							this.showdouble = b.getBalance();
							if(b.getBalance() >= positive ){
							if( b.getProductcode().equals(apcode)){
													
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive ;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
						
							
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Expired");
					
				
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
						
					}
					
			}
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
					 
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					
					
					
				}
				
				
		if (this.adjustmentname.equalsIgnoreCase("COMPUTER_GENERATED (-)")) {
					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = - (negativeadjust);
						
						negativeadjust = - (negativeadjust);
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						negativeadjust = - (negativeadjust);
						positive = negativeadjust ;
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
					
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							this.showdouble = b.getBalance();
							if(b.getBalance() >=  positive){
							if( b.getProductcode().equals(apcode)){
											
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
						
					
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Computer Generated Adjustment (-)");
				
					
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
						
					}
					
				}
						
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
		
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}  
				}
				
	
				if (this.adjustmentname.equalsIgnoreCase("COMPUTER_GENERATED(+)")) {

					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0){
							negativeadjust = negativeadjust * -1;
						}
						
						System.out.println(negativeadjust);
						positive = negativeadjust ;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Computer Generated Adjusment");
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					
					try{
					
			
						
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}		
				

				if (this.adjustmentname.equalsIgnoreCase("PASSED_OPEN_VIAL_TIME_LIMIT")) {
					

					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					} 
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
		
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				

				if (this.adjustmentname.equalsIgnoreCase("COLD_CHAIN_FAILURE")) {
					

					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
												
							@SuppressWarnings("unused")
							double increase = b.getBalance() -positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
						try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
		
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				
				if (this.adjustmentname.equalsIgnoreCase("CLINIC_RETURN")) {
					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						
						
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						
						positive = negativeadjust ;
					}catch(NumberFormatException e){
						e.getStackTrace();
					}  
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Clinic Return");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
										
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
	
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (this.adjustmentname.equalsIgnoreCase("FOUND")) {

					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						
						
						System.out.println(negativeadjust);
						positive = negativeadjust;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Found");
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					
					try{
					
	
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (this.adjustmentname.equalsIgnoreCase("PURCHASED")) {
					
	
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						
					
						System.out.println(negativeadjust);
						positive = negativeadjust;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Purchased");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					try{
					
		
						
						}catch (java.lang.NullPointerException j){
							j.getMessage();
						}
			
	}
				Adjustmentelmis_stock_control_card
						.setId(this.GenerateGUID());//this.GenerateGUID());
				A = Adjustmentelmis_stock_control_card.getQty_received();
				B = Adjustmentelmis_stock_control_card.getQty_issued();
				C = Adjustmentelmis_stock_control_card.getAdjustments();

			  
				System.out.println(A + "WHAT IS  THE NUMBER");
				System.out.println(B);
				System.out.println(C);

				Adjustmentelmis_stock_control_card
						.setAdjustmenttype(this.adjustmentname);
				System.out.println(Adjustmentelmis_stock_control_card.getBalance());
				//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));
				Adjustmentelmis_stock_control_card.setStoreroomadjustment(true);
				Adjustmentelmis_stock_control_card.setProgram_area(facilityprogramcode);
				Adjustmentelmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
				Adjustmentelmis_stock_control_card.setCreateddate(amodifieddate);

				//created linked list object 

				//adjustmentstockcontrolcardList.add(savestockcontrolcard);
				adjustmentstockcontrolcardList.add(Adjustmentelmis_stock_control_card);
				System.out.println(adjustmentstockcontrolcardList.size()
						+ "ARRAY SIZE???");
				System.out.println("Array size here &&&");
				Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
				//return saveShipped_Line_Items; */		
 }
				

	private void formwindowActivated(java.awt.event.WindowEvent evt){
		this.transferfromfacilityJCB.setEnabled(false);
		this.transferfromfacilityJCB.setEnabled(false);
		this.transfertofacilityJCB.setEnabled(false);
       this.enterfacilityname.setEnabled(false);
       transferoutJL.setEnabled(false);
	}
	

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new FacilityProductsDAO();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilitiesmapper = new FacilitySetUpDAO();
		facilitiesnameList = facilitiesmapper.getfacilitynameList();
		
		for (facilities f : facilitiesnameList) {
			modelfacilities.addElement(f.getName());

		}
        
		for (facilities f : facilitiesnameList) {
			this.modelfacilitiesList.addElement(f.getName());

		}
	//	AutoCompleteDecorator.decorate(this.transferfromfacilityJCB);
	//	AutoCompleteDecorator.decorate(this.transfertofacilityJCB);

	
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		this.RegisterAdjustmentJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		RegisterAdjustmentJT.setFont(new java.awt.Font(
				"Ebrima", 0, 20));
		this.RegisterAdjustmentJT.setRowHeight(30);
		this.PopulateProgram_ProductTable(facilityAdjustmentList);
		
		

			if (!this.Pname.equals("")) {

				//if the name is too long cut it short for display purpose
				if (this.Pname.length() >= 40) {

					this.Adjustmentproductnamelbl.setText(Pname.substring(
							0, 40) + "\n ," + " " + Pstrength);
				} else {

					Adjustmentproductnamelbl.setText(Pname + " " + Pstrength);
				}
			}
			
	/*	Jpanelfacilityto.setVisible(false);
		this.transferfromfacilityJCB.setVisible(false);
		this.transfertofacilityJCB.setVisible(false);
        this.enterfacilityname.setVisible(false);
        transferoutJL.setVisible(false);*/
		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
		
	}
	
	
	
		/**
	 * @param args the command line arguments
	 */

	private javax.swing.JButton SaveJBtn;
	private javax.swing.JPanel jPanel1;
	private JTable RegisterAdjustmentJT;
	private JLabel enterfacilityname;
	private JLabel transferoutJL;
	private JLabel Adjustmentproductnamelbl;
	@SuppressWarnings("rawtypes")
	private static JComboBox transfertofacilityJCB;
	private JRadioButton transferinrdbtn;
	private JRadioButton transferoutrdbtn;
	@SuppressWarnings("rawtypes")
	private  JComboBox  transferfromfacilityJCB;
}
	
	/**********************************************************
	 * END CHILD FORM 
	 **********************************************************/
			 
