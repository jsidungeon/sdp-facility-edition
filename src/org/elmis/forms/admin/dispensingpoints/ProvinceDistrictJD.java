/*
 * AddDispensingPointJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.admin.dispensingpoints;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.DefaultListSelectionModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import org.elmis.facility.domain.dao.FacilityDao;
import org.elmis.facility.domain.dao.GeographicZoneDao;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.GeographicZone;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.ElmisProperties;
import org.elmis.facility.utils.SdpStyleSheet;

/**
 * 
 * @author __USER__
 */
@SuppressWarnings("serial")
public class ProvinceDistrictJD extends javax.swing.JDialog {
	private String[] provinceArray;
	//private String[] districtArray;
	private List<Facility> facilityList;
	private String province;
	private int selectedRow;
	private int facilityTypeId;
	private String district;
	private String facilityName;
	private String facilityCode;
	private List<GeographicZone> districtList;
	private JLabel facilityNameLabel;
	private Map<String, String> provinceMap = new HashMap<>();
	private Map<String, Integer> districtMap = new HashMap<>();
	private String[][] data = null;
	private String[] facilityColumnNames = { "<HTML><b>Code</b></html>","<HTML><b>Facilitly</b></html>"};
	String[] columns_districts = { "Districts" };
	private DefaultTableModel facilityTableModel = new DefaultTableModel(data,facilityColumnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {
			if (column == 2)
				return true;
			else if (column == 3)
				return true;
			else
				return false;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2)
				return Integer.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};
	
	
	/** Creates new form AddDispensingPointJD */
	public ProvinceDistrictJD(java.awt.Frame parent, boolean modal, JLabel facilityNameLabel) {
		super(parent, modal);
		this.facilityNameLabel = facilityNameLabel;
		GeographicZoneDao geoZoneDao = new GeographicZoneDao();
		List<GeographicZone> provinceList = geoZoneDao.getGeographicZones();
		provinceArray = new String[provinceList.size()+1];
		provinceArray[0] = "Select a province";
		for (int i = 1; i < provinceArray.length; i++){
			provinceArray[i] = provinceList.get(i -1).getGeographicZoneName();
			provinceMap.put(provinceList.get(i-1).getGeographicZoneName(), provinceList.get(i-1).getCode());
		}
		
		initComponents();
		
		SdpStyleSheet.configJDialogBackground(this);

		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel4 = new javax.swing.JLabel();
		provinceJCombobox = new javax.swing.JComboBox();
		jScrollPane1 = new javax.swing.JScrollPane();
		facilitiesJTable = new javax.swing.JTable();
		facilitiesJTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		facilitiesJTable.getTableHeader().setReorderingAllowed(false);
		cancelJBtn = new javax.swing.JButton();
		savePropertiesBtn = new javax.swing.JButton();
		savePropertiesBtn.setEnabled(false);

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Setup Facility");

		SdpStyleSheet.configJPanelBackground(jPanel1);
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Province:");

		provinceJCombobox.setModel(new javax.swing.DefaultComboBoxModel(provinceArray));
		districtComboBox = new JComboBox(/*new javax.swing.DefaultComboBoxModel(districtArray)*/);
		districtComboBox.setEnabled(false);
		provinceJCombobox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				provinceComboBox1ItemStateChanged(evt);
			}
		});
		districtComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				districtComboBox1ItemStateChanged(evt);
			}
		});
		
		facilitiesJTable.setSelectionModel(new SingleListSelectionModel());
		facilitiesJTable.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 1) {
					selectedRow = facilitiesJTable.getSelectedRow();
					facilityCode = ""+facilitiesJTable.getValueAt(selectedRow, 0);
					facilityName = ""+facilitiesJTable.getValueAt(selectedRow, 1);
					savePropertiesBtn.setEnabled(true);
				}
			}
		});
		SdpStyleSheet.configTable(facilitiesJTable);
		jScrollPane1.setViewportView(facilitiesJTable);
		
		JLabel lblNewLabel = new JLabel("District:");
		lblNewLabel.setFont(new Font("Ebrima", Font.BOLD, 12));
		lblNewLabel.setForeground(Color.WHITE);
		
		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 387, Short.MAX_VALUE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(74)
							.addComponent(jLabel4)
							.addGap(10)
							.addComponent(provinceJCombobox, 0, 261, Short.MAX_VALUE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(74)
							.addComponent(lblNewLabel)
							.addGap(18)
							.addComponent(districtComboBox, 0, 261, Short.MAX_VALUE)))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(40)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(jLabel4)
						.addComponent(provinceJCombobox, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addGap(28)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(districtComboBox, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
					.addGap(38)
					.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		jPanel1.setLayout(jPanel1Layout);

		cancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.setText("Close");
		cancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelJBtnActionPerformed(evt);
			}
		});

		savePropertiesBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		savePropertiesBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Add.png"))); // NOI18N
		savePropertiesBtn.setText("Save");
		savePropertiesBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						savePropertiesJBtnActionPerformed(evt);
					}
				});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap(72,
						Short.MAX_VALUE).addComponent(savePropertiesBtn)
						.addGap(18, 18, 18).addComponent(cancelJBtn).addGap(28,
								28, 28)).addComponent(jPanel1,
				javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								layout
										.createSequentialGroup()
										.addComponent(
												jPanel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																cancelJBtn)
														.addComponent(
																savePropertiesBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																31,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	@SuppressWarnings("unchecked")
	/*private void descJTFActionPerformed(java.awt.event.ActionEvent evt) {
		 JComboBox cb = (JComboBox) evt.getSource();
		String provincename = (String) cb.getSelectedItem();
		paramDistricts = provincename;
		
		ArrayList cols = new ArrayList();
		for (int j = 0; j < columns_districts.length; j++) {
			//cols.add(defaultv_districts[j]);

		}
		System.out.print(cols.size()
				+ "What is the SIZE of Array to Table??");
		//tableModel_districts.insertRow(cols);

		
		System.out.println(paramDistricts);
		
		
	}*/

	private void cancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {

		this.dispose();
	}

	private void savePropertiesJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		new FacilityDao().setSelectedFacility(facilityCode);
		facilityTypeId = facilityList.get(selectedRow).getTypeid();
		facilityNameLabel.setText("Facility Name : "+facilityName);
		ElmisProperties.saveFacilitySettings(province, district, facilityName, facilityCode,""+facilityTypeId, ""+facilityList.get(selectedRow).getId());
		javax.swing.JOptionPane.showMessageDialog(null,	"Province: "+province+" District : "+district+" & Facility : "+facilityName+" have been changed.");
		dispose();
	}
	private void provinceComboBox1ItemStateChanged(java.awt.event.ItemEvent evt)
	{
		 if (java.awt.event.ItemEvent.SELECTED == evt.getStateChange()) {

			String selectedProvince = evt.getItem().toString();
			int x = provinceJCombobox.getSelectedIndex();
			if (x != 0){
				province = selectedProvince;
				districtComboBox.setEnabled(true);
				districtComboBox.removeAllItems();
				GeographicZoneDao geoZoneDao = new GeographicZoneDao();
				districtList = geoZoneDao.getGeographicZones(provinceMap.get(selectedProvince));
				if(districtList.size() != 0)
					districtComboBox.addItem("Select district");
				for (GeographicZone g : districtList)
					districtComboBox.addItem(g.getGeographicZoneName());
			}
			else
			{
				districtComboBox.removeAllItems();
				districtComboBox.setEnabled(false);
			}
			
		}
	}
	private void districtComboBox1ItemStateChanged(java.awt.event.ItemEvent evt)
	{
		if (java.awt.event.ItemEvent.SELECTED == evt.getStateChange()) {
			String selectedDistrict = evt.getItem().toString();
			int x = districtComboBox.getSelectedIndex();
			if (x != 0){
				int geoZoneId = 0;
				if (facilitiesJTable.getRowCount() != 0)
					((DefaultTableModel)facilitiesJTable.getModel()).setRowCount(0);
				district = selectedDistrict;
				for (GeographicZone g : districtList)
				{
					if (g.getGeographicZoneName().equalsIgnoreCase(selectedDistrict))
						geoZoneId = g.getId();
				}
				facilityList = new FacilityDao().getFacilities(geoZoneId);
				Collections.sort(facilityList);
				facilitiesJTable.setModel(facilityTableModel);
				facilitiesJTable.getColumnModel().getColumn(0).setPreferredWidth(80);
				facilitiesJTable.getColumnModel().getColumn(1).setPreferredWidth(290);
				for (int i = 0; i < facilityList.size(); i++){
					facilityTableModel.addRow(new String[facilityList.size()]);
					facilitiesJTable.getModel().setValueAt(facilityList.get(i).getCode().trim(), i, 0);
					facilitiesJTable.getModel().setValueAt(facilityList.get(i).getName().trim(), i, 1);
				}
			}
			else
			{
				if (facilitiesJTable.getRowCount() != 0)
					((DefaultTableModel)facilitiesJTable.getModel()).setRowCount(0);
			}
		}
	}


	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton savePropertiesBtn;
	private javax.swing.JButton cancelJBtn;
	private javax.swing.JComboBox<String> provinceJCombobox;
	private JComboBox<String> districtComboBox;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable facilitiesJTable;
	
	public class SingleListSelectionModel extends DefaultListSelectionModel {

	    public SingleListSelectionModel () {
	        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    }

	    @Override
	    public void clearSelection() {
	    }

	    @Override
	    public void removeSelectionInterval(int index0, int index1) {
	    }

	}
}