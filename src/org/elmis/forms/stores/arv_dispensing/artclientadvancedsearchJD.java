/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.MutableComboBoxModel;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.forms.regimen.util.GenericUtil;
import org.elmis.forms.stores.arv_dispensing.arvmessageDialog;

import com.oribicom.tools.TableModel;

import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

import java.awt.Label;

import org.elmis.forms.stores.receiving.storeAdjustmentTypeJD;
import org.elmis.forms.stores.receiving.testMultipleAdjustments;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.MutableComboBoxModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.jdesktop.swingx.table.DatePickerCellEditor;


//import com.oribicom.tools.TableModel;
import com.toedter.calendar.JDateChooserCellEditor;

import java.awt.Font;

import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.GroupLayout;

import java.awt.Toolkit;

import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.Color;

import com.toedter.calendar.JDateChooser;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.event.MouseAdapter;
import java.beans.PropertyChangeListener;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class artclientadvancedsearchJD extends javax.swing.JDialog {
	private ARVDispensingDAO callarvdispenser;
	@SuppressWarnings("rawtypes")
	private List<Elmis_Patient> elmis_patients = new LinkedList(); 
	private String selectedsex;
	public Timestamp selecteddate;
	private Date registerdate;
	private Date registerdate2;
	public Timestamp selecteddate2;
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	private String pname;
	private String pstrength;
	public String typeCode;
	private List<Character> charList = new LinkedList();
	private char letter;
	private String searchText = "";
	private String SearchResult = "";
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	
	ARVDispensingDAO aFacilityproductsmapper = null;
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	private String adjustmentname = "";
	private String programname = "";
	public String digit = "";
	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityartClients = {
			"First Name", "Last Name", "Client Age", "Gender","Art Number","NRC Number" };
	
	
	private static final Object[] defaultv_facilityartClients = { "", "",
			"", "","",""};
	
	private static final int rows_fproducts = 0;

	private JComboBox facilityProgList = new JComboBox();
	MutableComboBoxModel modelPrograms = (MutableComboBoxModel) facilityProgList
			.getModel();

	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();
	public static TableModel tableModel_fartclients = new TableModel(
			columns_facilityartClients,
			defaultv_facilityartClients, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false,false,false};

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 6) {
				return getValueAt(0,6).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<Elmis_Patient> fartclientsIterator;
	@SuppressWarnings("unchecked")
	List<Elmis_Patient> fartfindclientsList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	private Elmis_Patient facilityARTclients;
	FacilityProductsDAO Facilityproductsmapper = null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private JCheckBox checkbox;
	public static Boolean cansave = false;
	public  String Pcode = "";
	public int colindex = 0;
	public  int rowindex = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilitytypeCode;
	public Boolean  productsearchenabled = false;
    public Boolean  calltracer = false; 
    int rowcheck = 0;
	int rowcheckinit = -1;
	ArrayList<Elmis_Stock_Control_Card> adjustmentstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();

	ListIterator elmistockcontrolcarditerator = adjustmentstockcontrolcardList
			.listIterator();

	private Elmis_Stock_Control_Card Adjustmentelmis_stock_control_card;
	private Elmis_Stock_Control_Card Adjustmentsavestockcontrolcard;
	public String facilityproductsource;
	private Integer productid = 0;

	  /** Creates new form receiveProducts */
	public artclientadvancedsearchJD(java.awt.Frame parent,
			boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(900, 450);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		tableclients = new JTable();
		Year1dateChooser = new com.toedter.calendar.JDateChooser();
		Year2dateChooser = new com.toedter.calendar.JDateChooser();
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Advanced Search");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		
		JLabel lblNewLabel = new JLabel("First Name");
		lblNewLabel.setFont(new Font("Ebrima", Font.PLAIN, 15));
		lblNewLabel.setForeground(Color.WHITE);
		
		firstnameJT = new JTextField();
		firstnameJT.setColumns(10);
		
		lblNewLabel_1 = new JLabel("Last Name");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Ebrima", Font.PLAIN, 15));
		
		lastnameJT = new JTextField();
		lastnameJT.setColumns(10);
		
		JLabel lblDateOfBirth = new JLabel("Date of Birth Between");
		lblDateOfBirth.setForeground(Color.WHITE);
		lblDateOfBirth.setFont(new Font("Ebrima", Font.PLAIN, 15));
		
		lblNewLabel_2 = new JLabel("and");
		lblNewLabel_2.setFont(new Font("Ebrima", Font.PLAIN, 15));
		lblNewLabel_2.setForeground(Color.WHITE);
		
		JComboBox sexJComboBox = new JComboBox();
		sexJComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				sexJComboBoxactionPerformed(evt);
			}

			
		});
		sexJComboBox.setModel(new DefaultComboBoxModel(new String[] {"Select Gender", "Male", "Female"}));
		sexJComboBox.setFont(new Font("Ebrima", Font.PLAIN, 13));
		
		JLabel lblSex = new JLabel("Gender");
		lblSex.setForeground(Color.WHITE);
		lblSex.setFont(new Font("Ebrima", Font.PLAIN, 15));
		
		JButton button = new JButton();
		button.addMouseListener(new MouseAdapter() {
		
			public void mouseClicked(MouseEvent evt) {
				buttonMouseClicked(evt);
			}

			
		});
		
		
		
		button.setText("Search");
		button.setFont(new Font("Ebrima", Font.BOLD, 12));
			
		
		//JDateChooser Year1dateChooser = new JDateChooser();
		Year1dateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener()  {
			public void propertyChange(java.beans.PropertyChangeEvent evt) {
				Year1dateChooserPropertyChange(evt);
			}

			
		});
		Year1dateChooser.getCalendarButton().setFont(new Font("Ebrima", Font.PLAIN, 13));
		Year1dateChooser.setPreferredSize(new Dimension(150, 20));
		Year1dateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		Year1dateChooser.setDateFormatString("yyyy-MM-dd");
		
		//JDateChooser Year2dateChooser = new JDateChooser();
		Year2dateChooser.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
			public void propertyChange(java.beans.PropertyChangeEvent evt) {
				Year2dateChooserPropertyChange(evt);
			}
		});
		Year2dateChooser.getCalendarButton().setFont(new Font("Ebrima", Font.PLAIN, 13));
		Year2dateChooser.setPreferredSize(new Dimension(150, 20));
		Year2dateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		Year2dateChooser.setDateFormatString("yyyy-MM-dd");
		
		facilityclientsscrollPane = new JScrollPane(tableclients);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(facilityclientsscrollPane, GroupLayout.DEFAULT_SIZE, 805, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addComponent(lblDateOfBirth)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(Year1dateChooser, GroupLayout.PREFERRED_SIZE, 138, GroupLayout.PREFERRED_SIZE)
									.addGap(12)
									.addComponent(lblNewLabel_2)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(Year2dateChooser, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(firstnameJT, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE)
										.addComponent(lastnameJT, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE))))
							.addPreferredGap(ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
							.addComponent(lblSex, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(button, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(sexJComboBox, 0, 148, Short.MAX_VALUE))
							.addGap(72))))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(27)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(firstnameJT, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(lastnameJT, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblNewLabel_1, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
							.addGap(13)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
									.addComponent(lblNewLabel_2)
									.addComponent(Year2dateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
									.addComponent(Year1dateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
									.addComponent(lblDateOfBirth, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(sexJComboBox, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblSex, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addComponent(button, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)))
					.addGap(33)
					.addComponent(facilityclientsscrollPane, GroupLayout.PREFERRED_SIZE, 159, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(163, Short.MAX_VALUE))
		);
		
		
		//scrollPane.setColumnHeaderView(tableclients);
		tableclients.setModel(tableModel_fartclients);
		jPanel1.setLayout(jPanel1Layout);
		tableclients
		.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				tableclientsMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 829, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 526, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(209, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	
	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate + "Error source 2014");
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			selecteddate = Timestamp.valueOf(newDateString);
			
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
		System.out.println(selecteddate + "same expiry date??");
		return selecteddate;
		

	}
	
	private Date convertstringtodate(String s){
		SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
		//String dateInString = "7-Jun-2013";
		Date date = null;
		try {
			date = formatter.parse(s);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(date);
		System.out.println(formatter.format(date));
		return date;
	}
	
	
	protected void Year2dateChooserPropertyChange(PropertyChangeEvent evt) {
		
		
		// TODO Auto-generated method stub
		try {
            
			 Calendar currentDate = Calendar.getInstance(); //Get the current date
			 SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
			 String dateNow = formatter.format(currentDate.getTime());
			 System.out.println("Now the date is :=>  " + dateNow);
			
						
			Date mydate = this.Year2dateChooser.getDate();
			String sdate = formatter.format(mydate);
			System.out.println(sdate);
			this.registerdate2 = mydate;
			System.out.println(registerdate2);
			Date d = mydate;
			if(d.after(currentDate.getTime())){
				System.out.println("Test date diff ");
				productdeliverdate = Convertdatestr(currentDate.getTime().toString());
				JOptionPane.showMessageDialog(this, "Registeration Date cannnot be in the future",
						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				this.Year1dateChooser.setDate(new Date());
			}
			

		} catch (NullPointerException e) {
			e.getMessage();
		}


		
		
	}
	private void sexJComboBoxactionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox) evt.getSource();
		selectedsex = (String) cb.getSelectedItem();
		System.out.println(selectedsex);
	}
	private void Year1dateChooserPropertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
		try {
            
			 Calendar currentDate = Calendar.getInstance(); //Get the current date
			 SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd"); //format it as per your requirement
			 String dateNow = formatter.format(currentDate.getTime());
			// System.out.println("Now the date is :=>  " + dateNow);
			
						
			Date mydate = this.Year1dateChooser.getDate();
			String sdate = formatter.format(mydate);
			System.out.println(sdate);
			this.registerdate = mydate;
			System.out.println(registerdate);
			Date d = mydate;
			if(d.after(currentDate.getTime())){
				System.out.println("Test date diff ");
				productdeliverdate = Convertdatestr(currentDate.getTime().toString());
				JOptionPane.showMessageDialog(this, "Registeration Date cannnot be in the future",
						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				this.Year1dateChooser.setDate(new Date());
			}
			

		} catch (NullPointerException e) {
			e.getMessage();
		}

	
}

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}
	private void clientsearchswitch(){
		
		
		
		
		
	}
	private void buttonMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		
		
		//elmis_patient = new Elmis_Patient();
		
		/*if(!(firstnameJT.getText().equals("")) && (!(lastnameJT.getText().equals("")))){

		String dbdriverStatus = System.getProperty("dbdriver");
		if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
			elmis_patients = callarvdispenser.dogetcurrentARVclientList(
					
					registerdate,registerdate2, firstnameJT.getText().toString(),
					lastnameJT.getText().toString(), selectedsex);
					
		} else {			
          elmis_patients = callarvdispenser.dogetcurrentARVclientListhsqldb(
					
					registerdate,registerdate2, firstnameJT.getText().toString(),
					lastnameJT.getText().toString(), selectedsex);
		}
		
			
		if (elmis_patients != null){
			//found a list of elmis_patients
			PopulateProgram_ProductTable(elmis_patients);
		}
		
		}if (!(firstnameJT.getText().equals("")) && (lastnameJT.getText().equals(""))){
			//search with an OR
			String dbdriverStatus = System.getProperty("dbdriver");
			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
				elmis_patients = callarvdispenser.dogetcurrentARVclientListFirstname(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
						
			} else {
				
	          elmis_patients = callarvdispenser.dogetcurrentARVclientListFirstname(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
			}
			
			
			
			if (elmis_patients != null){
				//found a list of elmis_patients
				PopulateProgram_ProductTable(elmis_patients);
			}
			
		}if ((firstnameJT.getText().equals("")) && (!(lastnameJT.getText().equals("")))){
			String dbdriverStatus = System.getProperty("dbdriver");
			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
				elmis_patients = callarvdispenser.dogetcurrentARVclientListLastname(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
						
			} else {
				
	          elmis_patients = callarvdispenser.dogetcurrentARVclientListLastname(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
			}
			
			
			
			if (elmis_patients != null){
				//found a list of elmis_patients
				PopulateProgram_ProductTable(elmis_patients);
			}
			
		}if(this.registerdate != null && this.registerdate2 != null){
			String dbdriverStatus = System.getProperty("dbdriver");
			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
				elmis_patients = callarvdispenser.dogetcurrentARVclientListbyDOB(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
						
			} else {
				
	          elmis_patients = callarvdispenser.dogetcurrentARVclientListbyDOB(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
			}
			
			
			
			if (elmis_patients != null){
				//found a list of elmis_patients
				PopulateProgram_ProductTable(elmis_patients);
			}
		}if(this.registerdate != null && this.registerdate2 != null){
			if (!(firstnameJT.getText().equals("")) && (!(lastnameJT.getText().equals("")))){
		
			String dbdriverStatus = System.getProperty("dbdriver");
			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
				elmis_patients = callarvdispenser.dogetcurrentARVclientListbyfirstLastDOB(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
						
			} else {
				
	          elmis_patients = callarvdispenser.dogetcurrentARVclientListbyfirstLastDOB(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
			}
			
						
			if (elmis_patients != null){
				//found a list of elmis_patients
				PopulateProgram_ProductTable(elmis_patients);
			}
			
		}
		}if(this.registerdate != null && this.registerdate2 != null) {
			if (!(firstnameJT.getText().equals("")) && (!(lastnameJT.getText().equals("")))){
				try{
					if(!(this.selectedsex.equals("Select Sex"))){
				
					String dbdriverStatus = System.getProperty("dbdriver");
					if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
						elmis_patients = callarvdispenser.dogetcurrentARVclientListbyall(
								
								registerdate,registerdate2, firstnameJT.getText().toString(),
								lastnameJT.getText().toString(), selectedsex);
								
					} else {
						
			          elmis_patients = callarvdispenser.dogetcurrentARVclientListbyfirstLastDOB(
								
								registerdate,registerdate2, firstnameJT.getText().toString(),
								lastnameJT.getText().toString(), selectedsex);
					}
					
								
					if (elmis_patients != null){
						//found a list of elmis_patients
						PopulateProgram_ProductTable(elmis_patients);
					}
					
				
			}
			}catch(java.lang.NullPointerException n){
				n.getMessage();
		}
		} 
		try{
			if(!(this.selectedsex.equals("Select Sex"))){

			String dbdriverStatus = System.getProperty("dbdriver");
			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
				elmis_patients = callarvdispenser.dogetcurrentARVclientListbysex(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
						
			} else {
				
	          elmis_patients = callarvdispenser.dogetcurrentARVclientListbysex(
						
						registerdate,registerdate2, firstnameJT.getText().toString(),
						lastnameJT.getText().toString(), selectedsex);
			}
			
						
			if (elmis_patients != null){
				//found a list of elmis_patients
				PopulateProgram_ProductTable(elmis_patients);
			}
			
		
	}
		}catch(java.lang.NullPointerException n){
		
		n.getMessage();
	}
	}*/
		
		String dbdriverStatus = System.getProperty("dbdriver");
		/*if (selectedsex.equals("Select Sex")){
			selectedsex= null;
		}*/
		
	//DEBUG FOR ADVANCED SEARCH	
		if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
			elmis_patients = callarvdispenser.dogetcurrentARVclientList(
					
					registerdate,registerdate2, "%"+firstnameJT.getText().toString().trim()+"%",
					"%"+lastnameJT.getText().toString().trim()+"%", (selectedsex != null)? ((!selectedsex.contains("ale") )?null:selectedsex):null);
					
		} else {			
          elmis_patients = callarvdispenser.dogetcurrentARVclientListhsqldb(
					
					registerdate,registerdate2, firstnameJT.getText().toString(),
					lastnameJT.getText().toString(), selectedsex);
		}
		if (elmis_patients != null){
			//found a list of elmis_patients
			PopulateProgram_ProductTable(elmis_patients);
		}
			
		
		//PopulateProgram_ProductTable_flush();
	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		
		/*tableclients.getColumnModel().getColumn(2)
		.setMinWidth(0);
		tableclients.getColumnModel().getColumn(2)
		.setMaxWidth(0);
		tableclients.getColumnModel().getColumn(2)	
		.setWidth(0);*/

		
		callarvdispenser = new ARVDispensingDAO();

		PopulateProgram_ProductTable_flush();

		this.tableclients.getTableHeader()
		.setFont(new Font("Ebrima", Font.PLAIN, 20));
		this.tableclients.setRowHeight(30);
		Facilityproductsmapper = new FacilityProductsDAO();
		facilityprogramsList = Facilityproductsmapper.selectAllPrograms();
		for (Programs p : facilityprogramsList) {
			modelPrograms.addElement(p.getCode());

		}

		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		for (Losses_Adjustments_Types l : facilityAdjustmentList) {
			modelAdjustments.addElement(l.getName());

		}
		
		

	

	}
	
	private void tableclientsMouseClicked(
			java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call create  shipment object 
  	    	
		    
	   try{ 	    		
	    		
		ARTNumberJP.Artcode =  this.tableModel_fartclients.getValueAt(
	    				this.tableclients
	    						.getSelectedRow(), 4).toString();
	    		
		ARTNumberJP.Artnrc  	    		
	    		= tableModel_fartclients.getValueAt(
	    				this.tableclients
						.getSelectedRow(),5).toString();
	    		
	   }catch(NullPointerException n){
		    n.getMessage();
	   }
		
		this.dispose();
	}

	
private void PopulateProgram_ProductTable_flush(){

	tableModel_fartclients.clearTable();
}
	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fartclients) {
		checkbox = new JCheckBox();
		try {
			System.out.println("11111");
			if (fartclients.isEmpty()) {
				aFacilityproductsmapper   = new ARVDispensingDAO();
				Facilityproductsmapper = new FacilityProductsDAO();
							
		    
				String dbdriverStatus = System.getProperty("dbdriver");
				
					//reset program code

					if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
						elmis_patients = callarvdispenser.dogetcurrentARVclientList(
								
								registerdate,registerdate2, firstnameJT.getText().toString(),
								lastnameJT.getText().toString(), selectedsex);
						System.out.println("22222");
							
					} else {
						elmis_patients = callarvdispenser.dogetcurrentARVclientListhsqldb(
								
								registerdate,registerdate2, firstnameJT.getText().toString(),
								lastnameJT.getText().toString(), selectedsex);
								
					}
				

			} else {
				fartfindclientsList = fartclients;
				System.out.println("333333");
			}
	
		
			 this.tableModel_fartclients.clearTable();
			 System.out.println("44444");

			 fartclientsIterator = fartfindclientsList.listIterator();
			 int controler = 0;
			while (fartclientsIterator.hasNext()) {
			System.out.println(" : "+controler);
				facilityARTclients = fartclientsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityartClients[0] = facilityARTclients.getFirstname();

				defaultv_facilityartClients[1] = facilityARTclients.getLastname();

			     defaultv_facilityartClients[2] = facilityARTclients.getAge();
				defaultv_facilityartClients[3] = facilityARTclients.getSex();
				defaultv_facilityartClients[4] = facilityARTclients.getArt_number();
				defaultv_facilityartClients[5] = facilityARTclients.getNrc_number();
				
						
				//Set the value of the entered product quantity 

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityartClients.length; j++) {
					cols.add(defaultv_facilityartClients[j]);

				}
				System.out.print(cols.size()
						+ "What is the SIZE of Array to Table??");
				tableModel_fartclients.insertRow(cols);

				fartclientsIterator.remove();
				controler++;
			}
		} catch (NullPointerException e) {
			System.out.println("Thrown out here....");
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				artclientadvancedsearchJD dialog = new artclientadvancedsearchJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}
	private javax.swing.JPanel jPanel1;
	private com.toedter.calendar.JDateChooser Year1dateChooser;
	private com.toedter.calendar.JDateChooser Year2dateChooser;
	private JTextField firstnameJT;
	private JLabel lblNewLabel_1;
	private JTextField lastnameJT;
	private JLabel lblNewLabel_2;
	private JScrollPane facilityclientsscrollPane;
	private JTable tableclients;
}