package org.elmis.facility.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.elmis.facility.utils.ElmisProperties;
/**
 * DatabaseSwitcher.java
 * Purpose: Used to switch from one database to another i.e POSTGRES to HSQL
 * @author Michael Mwebaze
 * @version 1.0
 */
//this is a change so i can push...
public class DatabaseSwitcher {

	/*private static final String USERNAME_HSQL = ElmisProperties.getHyperSqlProperties().getProperty("jdbc.username");
	private static final String PASSWORD_HSQL = ElmisProperties.getHyperSqlProperties().getProperty("jdbc.password");
	private static final String CONN_STRING_ELMIS_HSQL = ElmisProperties.getHyperSqlProperties().getProperty("urlhsql");*/
	private static final String USERNAME_POSTGRESQL = System.getProperty("dbuser");//postgres
	private static final String PASSWORD_POSTGRESQL = System.getProperty("dbpassword");//"p@ssw0rd";
/*	private static final String USERNAME_SQLITE = ElmisProperties.getSqliteProperties().getProperty("dbusersqlite");
	private static final String PASSWORD_SQLITE = ElmisProperties.getSqliteProperties().getProperty("dbpasswordsqlite");
	private static final String CONN_STRING_ELMIS_SQLITE = ElmisProperties.getElmisProperties().getProperty("dburlsqlite");*/
	private static final String CONN_STRING_ELMIS_POSTGRES = System.getProperty("dburl");//"jdbc:postgresql://elmis1:5432/postgres";

	/**
	 * 
	 * @return true if a connection to HSQL database is available otherwise it will return false
	 */
	/*public static boolean switchToHsql(){

		try (
				Connection connection = DriverManager.getConnection(CONN_STRING_ELMIS_HSQL, USERNAME_HSQL, PASSWORD_HSQL
						)){

			System.out.println("Connected to elmis_db on Embedded HSQLDB");
			return true;

		} catch (SQLException e) {
			System.err.println("ERROR HSQL "+e.getMessage());
		}
		return false;
	}*/
	/**
	 * 
	 * @return true if connection to POSTGRES database is available otherwise it will return false
	 */
	public static boolean switchToPostgres()
	{	
		try(
				Connection connection = DriverManager.getConnection(CONN_STRING_ELMIS_POSTGRES, USERNAME_POSTGRESQL, PASSWORD_POSTGRESQL
						)){

			System.out.println("Connected to database elmis_facility db on postgres");
			return true;
		}
		catch(SQLException e)
		{
			System.out.println("Error postgress: "+e.getMessage());
		}
		return false;
	}
	/*public static boolean switchToSqlite()
	{
		try(
				Connection connection = DriverManager.getConnection(CONN_STRING_ELMIS_SQLITE, USERNAME_SQLITE, PASSWORD_SQLITE
						)){

			System.out.println("Connected to database SDP SQLITE DATABASE");
			return true;
		}
		catch(SQLException e)
		{
			System.out.println("Error sqlite: "+e.getMessage());
		}
		return false;
	}*/

}
