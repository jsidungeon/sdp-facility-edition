/*
 * SystemSettingTab.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.system;

import org.elmis.facility.main.gui.NetworkSettingsJD;
import org.elmis.forms.users.UsersAdminTabJD;

/**
 *
 * @author  __USER__
 */
public class SystemSettingTab extends javax.swing.JDialog {

	/** Creates new form SystemSettingTab */
	public SystemSettingTab(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(660, 400);
		this.setLocationRelativeTo(null);

		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		companySettingsJL = new javax.swing.JLabel();
		usersAdminJL = new javax.swing.JLabel();
		databaseSettingsJL = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Admin");

		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		companySettingsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		companySettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/tasks48x55.png"))); // NOI18N
		companySettingsJL.setText(" ");
		companySettingsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Organisation Settings"));
		companySettingsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				companySettingsJLMouseClicked(evt);
			}
		});

		usersAdminJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/icon_Users_48.gif"))); // NOI18N
		usersAdminJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("User Admin"));
		usersAdminJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseClicked(evt);
			}
		});

		databaseSettingsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		databaseSettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/icon_ConnectorConfig.gif"))); // NOI18N
		databaseSettingsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Database Settings"));
		databaseSettingsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				databaseSettingsJLMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												companySettingsJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												126,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												usersAdminJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												146,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												databaseSettingsJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												137,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(165, Short.MAX_VALUE)));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { companySettingsJL,
						databaseSettingsJL, usersAdminJL });

		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																databaseSettingsJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																117,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																jPanel1Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.TRAILING,
																				false)
																		.addComponent(
																				companySettingsJL,
																				javax.swing.GroupLayout.Alignment.LEADING,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				117,
																				Short.MAX_VALUE)
																		.addComponent(
																				usersAdminJL,
																				javax.swing.GroupLayout.Alignment.LEADING,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				117,
																				Short.MAX_VALUE)))
										.addContainerGap(225, Short.MAX_VALUE)));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { companySettingsJL,
						databaseSettingsJL, usersAdminJL });

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap().addComponent(
						jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void databaseSettingsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		new NetworkSettingsJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);
	}

	private void companySettingsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//AppJFrame.glassPane.activate(null);

		new SystemSettingJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);

		//AppJFrame.glassPane.deactivate();
	}

	private void usersAdminJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:
		// TODO add your handling code here:

		//AppJFrame.glassPane.activate(null);

		//AppJFrame.glassPane.deactivate();

		new UsersAdminTabJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SystemSettingTab dialog = new SystemSettingTab(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel companySettingsJL;
	private javax.swing.JLabel databaseSettingsJL;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JLabel usersAdminJL;
	// End of variables declaration//GEN-END:variables

}