/*
 * RolesJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.Roles;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Rights;
import org.elmis.facility.domain.model.Roles;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.network.MyBatisConnectionFactory;

import com.oribicom.tools.TableModel;

/**
 *
 * @author  __USER__
 */
public class RolesJD extends javax.swing.JDialog {

	//Roles
	private static final String[] columns_roles = { "Role name", "Description" };
	private static final Object[] defaultv_roles = { "", "" };
	private static final int rows_roles = 0;
	public static TableModel tableModel_roles = new TableModel(columns_roles,
			defaultv_roles, rows_roles);
	private static Roles roles;
	private static ListIterator<Roles> rolesIterator;
	List<Roles> rolesList = new LinkedList();

	//Users 

	private static final String[] columns_users = { "user name", "Job Title" };
	private static final Object[] defaultv_users = { "", "" };
	private static final int rows_users = 0;
	public static TableModel tableModel_users = new TableModel(columns_users,
			defaultv_users, rows_users);
	private static users users;
	private static ListIterator<users> usersIterator;
	List<Roles> usersList = new LinkedList();

	//Rights 

	private static final String[] columns_rights = { "Name", "Description" };
	private static final Object[] defaultv_rights = { "", "" };
	private static final int rows_rights = 0;
	public static TableModel tableModel_rights = new TableModel(columns_rights,
			defaultv_rights, rows_rights);
	private static Rights rights;
	private static ListIterator<Rights> rightsIterator;
	List<Roles> rightsList = new LinkedList();

	/** Creates new form RolesJD */
	public RolesJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);

		initComponents();

		this.setSize(900, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		//get the roles

	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		rolesJtable = new javax.swing.JTable();
		addRoleJL = new javax.swing.JLabel();
		editRightsJL = new javax.swing.JLabel();
		deleteJL = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jScrollPane2 = new javax.swing.JScrollPane();
		permissionJtable = new javax.swing.JTable();
		jScrollPane3 = new javax.swing.JScrollPane();
		usersJtable = new javax.swing.JTable();
		viewRolesJL = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Manage Roles");

		rolesJtable.setModel(tableModel_roles);
		rolesJtable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				rolesJtableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(rolesJtable);

		addRoleJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/CreateDocuments.gif"))); // NOI18N
		addRoleJL.setText("Add new role");
		addRoleJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				addRoleJLMouseClicked(evt);
			}
		});

		editRightsJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/EditLayout.gif"))); // NOI18N
		editRightsJL.setText("Edit role rights");
		editRightsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				editRightsJLMouseClicked(evt);
			}
		});

		deleteJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/editdelete.png"))); // NOI18N
		deleteJL.setText("Delete role");
		deleteJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				deleteJLMouseClicked(evt);
			}
		});

		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/detailview.gif"))); // NOI18N
		jLabel4.setText("Right");

		permissionJtable.setModel(tableModel_rights);
		jScrollPane2.setViewportView(permissionJtable);

		usersJtable.setModel(tableModel_users);
		jScrollPane3.setViewportView(usersJtable);

		viewRolesJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/CompositeQuery16.png"))); // NOI18N
		viewRolesJL.setText("View Roles");
		viewRolesJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewRolesJLMouseClicked(evt);
			}
		});

		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/Users16.png"))); // NOI18N
		jLabel5.setText("Users");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				viewRolesJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				addRoleJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				editRightsJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				deleteJL))
														.addComponent(
																jScrollPane1,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																675,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																jPanel1Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel4)
																						.addComponent(
																								jScrollPane2,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								342,
																								Short.MAX_VALUE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addGroup(
																								jPanel1Layout
																										.createSequentialGroup()
																										.addComponent(
																												jLabel5)
																										.addGap(
																												282,
																												282,
																												282))
																						.addComponent(
																								jScrollPane3,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								327,
																								javax.swing.GroupLayout.PREFERRED_SIZE))))
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																viewRolesJL)
														.addComponent(addRoleJL)
														.addComponent(
																editRightsJL)
														.addComponent(deleteJL))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												168,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel4)
														.addComponent(jLabel5))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jScrollPane2,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																142,
																Short.MAX_VALUE)
														.addComponent(
																jScrollPane3,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																142,
																Short.MAX_VALUE))
										.addContainerGap()));

		jTabbedPane1.addTab("Roles", jPanel1);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE,
						700, Short.MAX_VALUE).addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE,
						407, Short.MAX_VALUE).addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void deleteJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			rightsList = session.selectList("selectRightByRole");

			populateRightsTable(rightsList);

		} finally {
			session.close();
		}

	}

	private void editRightsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			rightsList = session.selectList("selectByRights");

			/*	for (Roles roles : rolesList) {
			 System.out.println("Record = " + roles.getName() + " "
			 + roles.getDescription());

			 }*/
			populateRightsTable(rightsList);

		} finally {
			session.close();
		}

	}

	private void rolesJtableMouseClicked(java.awt.event.MouseEvent evt) {

		// TODO add your handling code here:

		//javax.swing.JOptionPane.showMessageDialog(this,	tableModel_roles.getValueAt(rolesJtable.getSelectedRow(), 0));

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			//usersList = session.selectList("selectByusers");	

			usersList = session.selectList("selectUsersByRole",tableModel_roles.getValueAt(rolesJtable.getSelectedRow(), 0));
			populateUsersTable(usersList);

			rightsList = session.selectList("selectRightByRole",
					tableModel_roles
							.getValueAt(rolesJtable.getSelectedRow(), 0));
			populateRightsTable(rightsList);

		} finally {
			session.close();
		}

	}

	private void viewRolesJLMouseClicked(java.awt.event.MouseEvent evt) {

		// TODO add your handling code here:

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			rolesList = session.selectList("selectByRoles");

			/*	for (Roles roles : rolesList) {
			 System.out.println("Record = " + roles.getName() + " "
			 + roles.getDescription());

			 }*/
			populateRolesTable(rolesList);

		} finally {
			session.close();
		}

	}

	private void addRoleJLMouseClicked(java.awt.event.MouseEvent evt) {

		new AddNewRoleJD(javax.swing.JOptionPane.getFrameForComponent(this),true);
	}

	public static void populateRolesTable(List dataList) {

		tableModel_roles.clearTable();
		rolesIterator = dataList.listIterator();

		while (rolesIterator.hasNext()) {

			roles = rolesIterator.next();
			defaultv_roles[0] = roles.getName();
			defaultv_roles[1] = roles.getDescription();
			

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_roles.length; j++) {
				cols.add(defaultv_roles[j]);

			}

			tableModel_roles.insertRow(cols);
			// jScrollPane1.repaint();
			rolesIterator.remove();
		}

	}

	public static void populateUsersTable(List dataList) {

		tableModel_users.clearTable();
		usersIterator = dataList.listIterator();

		while (usersIterator.hasNext()) {

			users = usersIterator.next();
			defaultv_users[0] = users.getFirstname() + " "
					+ users.getLastname();
			defaultv_users[1] = users.getJobtitle();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_users.length; j++) {
				cols.add(defaultv_users[j]);

			}

			tableModel_users.insertRow(cols);
			// jScrollPane1.repaint();
			usersIterator.remove();
		}

	}

	public static void populateRightsTable(List dataList) {

		tableModel_rights.clearTable();
		rightsIterator = dataList.listIterator();

		while (rightsIterator.hasNext()) {

			rights = rightsIterator.next();
			defaultv_rights[0] = rights.getName();
			defaultv_rights[1] = rights.getDescription();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_rights.length; j++) {
				cols.add(defaultv_rights[j]);

			}

			tableModel_rights.insertRow(cols);
			// jScrollPane1.repaint();
			rightsIterator.remove();
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				RolesJD dialog = new RolesJD(new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel addRoleJL;
	private javax.swing.JLabel deleteJL;
	private javax.swing.JLabel editRightsJL;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JTable permissionJtable;
	private javax.swing.JTable rolesJtable;
	private javax.swing.JTable usersJtable;
	private javax.swing.JLabel viewRolesJL;
	// End of variables declaration//GEN-END:variables

}