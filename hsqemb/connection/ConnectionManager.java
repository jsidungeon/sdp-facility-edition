package org.elmis.facility.database.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.elmis.facility.utils.ElmisProperties;
/**
 * 
 * @author Michael Mwebaze
 * Singleton class to manage connection pools
 */
public class ConnectionManager {

	private static ConnectionManager instance = null;
	
	private static final String USERNAME_HSQL = "dbuser";
	private static final String PASSWORD_HSQL = "dbpassword";
	private static final String USERNAME_POSTGRESQL = ElmisProperties.getElmisProperties().getProperty("dbuser");
	private static final String PASSWORD_POSTGRESQL = ElmisProperties.getElmisProperties().getProperty("dbpassword");
	private static final String CONN_STRING_HSQL = "jdbc:hsqldb:database/elmis_facility";
	//private static final String CONN_STRING_POSTGRES = "jdbc:postgresql://localhost:5432/elmis_facility";
	private static final String CONN_STRING_POSTGRES = ElmisProperties.getElmisProperties().getProperty("dburl");
	private static final String CONN_STRING_MYSQ = "jdbc:mysql://localhost/ngamba";
	
	private DBType dbType = DBType.MYSQL;
	private Connection conn = null;
	
	private ConnectionManager() {
		
	}
	
	public static ConnectionManager getInstance() {
		if (instance == null)
		{
			instance = new ConnectionManager();
		}
		return instance;
	}
	
	/**
	 *set the type of database to connect too. At the moment supports MYSQL ONLY 
	 * @param dbType
	 */
	public void setDBType(DBType dbType) {
		this.dbType = dbType;
	}
	private boolean openConnection() throws SQLException, ClassNotFoundException {
		switch (dbType) {
		case HSQL:
			//Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(CONN_STRING_HSQL, USERNAME_HSQL, PASSWORD_HSQL);
			System.out.println("Connected to HSQL");
			return true;
			
		case POSTGRESQL:
			conn = DriverManager.getConnection(CONN_STRING_POSTGRES, USERNAME_POSTGRESQL, PASSWORD_POSTGRESQL);
			System.out.println("Connected to POSTGRES");
			return true;

		default:
			return false;
		}
	}
	
	public  Connection getConnection() throws SQLException, ClassNotFoundException {
		if (conn == null) {
			if (openConnection()) {
				return conn;
			}
			else
				return null;
		}
		return conn;
	}
	
	public void close() {
		try {
			conn.close();
			conn = null;
		} catch (Exception e) {
			System.err.println(e);
		}
	}
}
