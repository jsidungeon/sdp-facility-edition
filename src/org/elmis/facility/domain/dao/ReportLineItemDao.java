/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.json.beans.LossAdjustments;
import org.elmis.facility.json.beans.Type;
import org.elmis.facility.reporting.model.ReportLineItem;

/**
 * @author MMwebaze
 *
 */
public class ReportLineItemDao {

	private SqlSessionFactory sqlSessionFactory;
	//SqlSession updateSession;// = sqlSessionFactory.openSession();
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<ReportLineItem> getLineItems(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("programCode") int programCode)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		map.put("programCode", programCode);

		SqlSession session = sqlSessionFactory.openSession();
		List<ReportLineItem> lineItemsList = null;

		try
		{
			lineItemsList =	session.selectList("ReportLineItem.submittedRnRLineItem", map);
			List<LossAdjustments> adjustments = new ArrayList<>();
			for (ReportLineItem r : lineItemsList)
			{
				map.put("productCode", r.getProductCode());
				List<HashMap<String, Object>> lostAdjustmentList = null;
				if (programCode == 2) {//HIV progam area
					map.put("programArea", "HIV");
					lostAdjustmentList = session.selectList("LossAdjustment.hiv_adjustment_type", map);
				}
				else if (programCode == 3){ //ARV program area
					map.put("programArea", "ARV");
					lostAdjustmentList = session.selectList("LossAdjustment.arv_adjustment_type", map);
				}
				else if (programCode == 1){//EM program areas
					map.put("programArea", "EM");
					lostAdjustmentList = session.selectList("LossAdjustment.EM_LAB_adjustment_type", map);
				}
				else{ //LAB program area
					map.put("programArea", "LAB");
					lostAdjustmentList = session.selectList("LossAdjustment.EM_LAB_adjustment_type", map);
				}
				for (HashMap<String, Object> hm : lostAdjustmentList)
				{
					LossAdjustments la = new LossAdjustments();
					la.setQuantity(Integer.parseInt(hm.get("adjustments").toString()));
					Type type = new Type();
					type.setName(hm.get("name").toString());
					la.setType(type);
					adjustments.add(la);
				}

				r.setLossesAndAdjustments(adjustments);
			}
			return lineItemsList;
		}
		finally
		{
			session.close();
		}
	}
}
