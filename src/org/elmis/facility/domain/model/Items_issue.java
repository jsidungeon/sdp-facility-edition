package org.elmis.facility.domain.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * @author JBanda
 *
 */
public class Items_issue {
	
	private int product_id;
	private String product_name;
	private Double qtyToIssue;
	private String product_code;
	private Date createddate;
	
	public static List createBeanCollection () {
		List list = new ArrayList ();
		
		Items_issue items = new Items_issue();
		
		items.setProduct_code("EM003");
		items.setProduct_id(12);
		items.setProduct_name("Amoxicibicarbonate");
		items.setQtyToIssue(200.0);
		items.setCreateddate(new Date());
		
		list.add(items);
		
		return list;
	}

	/**
	 * 
	 */
	public Items_issue() {
		// TODO Auto-generated constructor stub
	}

	public int getProduct_id() {
		return product_id;
	}

	public void setProduct_id(int productId) {
		product_id = productId;
	}

	public String getProduct_name() {
		return product_name;
	}

	public void setProduct_name(String productName) {
		product_name = productName;
	}

	public Double getQtyToIssue() {
		return qtyToIssue;
	}

	public void setQtyToIssue(Double qtyToIssue) {
		this.qtyToIssue = qtyToIssue;
	}

	public String getProduct_code() {
		return product_code;
	}

	public void setProduct_code(String productCode) {
		product_code = productCode;
	}

	public Date getCreateddate() {
		return createddate;
	}

	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

}
