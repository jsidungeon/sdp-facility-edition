/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.facility.dashboard_elmis;

import java.awt.Color;
import java.awt.Paint;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.elmis.facility.dashboard.util.IndicatorProductProducer;
import org.elmis.facility.domain.dao.IndicatorProductDao;
import org.elmis.facility.reporting.model.IndicatorProduct;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.util.Rotation;

/**
 *
 * @author Michael Mwebaze
 */
public class IndicatorProductPanel {
	private IndicatorProductProducer producer = new IndicatorProductProducer();
	
	public IndicatorProductPanel() {
		/*ExecutorService executorService = Executors.newSingleThreadExecutor();
		executorService.execute(producer);
		executorService.shutdown();*/
		//TimerTask tasknew = new TimerSchedulePeriod();
	      Timer timer = new Timer();
	      
	      // scheduling the task at interval
	      timer.schedule(producer,0, 5*6*10000);   
	}

	public ChartPanel createBarGraphs()
	{
		final CategoryItemRenderer renderer = new CustomRenderer(
	            new Paint[] {Color.YELLOW, Color.RED, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.CYAN,
	                Color.PINK, Color.BLUE}
	        );
		
		//List<IndicatorProduct> poducts = new IndicatorProductDao().getIndicatorProducts();
		List<IndicatorProduct> 	products = producer.getBlockingQueue().peek();
			System.out.println("Size "+products.size());
		
		DefaultCategoryDataset barDataset = new DefaultCategoryDataset();
		for (IndicatorProduct iProd : products)
		{
			barDataset.setValue(iProd.getBalance(), "", iProd.getProductCode());
		}

		JFreeChart chart = ChartFactory.createBarChart("Indicator Products", "Products", "Available Stock", barDataset, PlotOrientation.HORIZONTAL, false, true, false);
		chart.setBorderVisible(true);
		CategoryPlot plot = chart.getCategoryPlot();
		BarRenderer br = (BarRenderer) plot.getRenderer();
		br.setDrawBarOutline(false);
		//plot.setRangeGridlinePaint(Color.GRAY);
		br.setSeriesPaint(0, Color.GREEN);
		br.setMaximumBarWidth(.25);
		//plot.setRenderer(2, renderer);
		plot.setRenderer(renderer);
		return new ChartPanel(chart);
	}	
	public ChartPanel createPieChart()
	{
		//List<IndicatorProduct> products = new IndicatorProductDao().getIndicatorProducts();
		List<IndicatorProduct> 	products = producer.getBlockingQueue().peek();
		DefaultPieDataset pieDataset = new DefaultPieDataset();

		for (IndicatorProduct iProd : products)
		{
			pieDataset.setValue(iProd.getProductCode(), iProd.getBalance());
		}

		JFreeChart chart = ChartFactory.createPieChart3D("Indicator Products", pieDataset, true, true, true);
		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return new ChartPanel(chart);
	}
	class CustomRenderer extends BarRenderer
	{
		private Paint[] colors;
		public CustomRenderer(final Paint[] colors) {
			this.colors = colors;
		}
		public Paint getItemPaint(final int row, final int column) {
			if (column == 0)
				return colors[colors.length - 1];
			else
			return this.colors[column % this.colors.length];
		}
	}
}
