package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.RandrArdDrug;

public class RandrArvDao {
	private SqlSessionFactory sqlSessionFactory;

	public RandrArvDao(){
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	/**
	 * Returns the list of all Contact instances from the database.
	 * @return the list of all Contact instances from the database.
	 */
	@SuppressWarnings("unchecked")
	public List<RandrArdDrug> selectAll(){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<RandrArdDrug> list = session.selectList("RandrArdView.getAll");
			return list;
		} finally {
			session.close();
		}
	}

	/**
	 * Returns a Contact instance from the database.
	 * @param id primary key value used for lookup.
	 * @return A Contact instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public RandrArdDrug selectById(int id){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			RandrArdDrug randrArdDrug = (RandrArdDrug) session.selectOne("RandrArdView.getById",id);
			return randrArdDrug;
		} finally {
			session.close();
		}
	}

	/**
	 * Updates an instance of RegimenCategory in the database.
	 * @param contact the instance to be updated.
	 */
	public void update(RandrArdDrug randrArdDrug){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.update("RandrArdView.update", randrArdDrug);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Insert an instance of RegimenCategory into the database.
	 * @param contact the instance to be persisted.
	 */
	public void insert(RandrArdDrug randrArdDrug){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.insert("RandrArdDrug.insert", randrArdDrug);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Delete an instance of RegimenCategory from the database.
	 * @param id primary key value of the instance to be deleted.
	 */
	public void delete(int id){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.delete("RandrArdDrug.deleteById", id);
			session.commit();
		} finally {
			session.close();
		}
	}
}
