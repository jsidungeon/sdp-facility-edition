/*
 * ReportsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.Cursor;
import java.awt.Image;
import java.util.HashMap;
import java.util.Map;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;

import net.sf.jasperreports.engine.JasperPrint;

import org.elmis.facility.labs.EquipmentInformationJd;
import org.elmis.facility.labs.FacilityAvailableEquipmentJd;


/**
 * 
 * @author __USER__
 */
public class LabsJP extends javax.swing.JPanel {

	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	private final static Cursor busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final static Cursor defaultCursor = Cursor.getDefaultCursor();
	/** Creates new form ReportsJP */
	public LabsJP() {
		initComponents();
		
		equipInfoLabel.setVisible(true);
		equipInfoLabel1.setVisible(true);
		//weeklyRptJL.setVisible(false);
		//staffproductivityJL.setVisible(false);

		// productivityJBtn.setVisible(false);

		//equipInfoLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(arg0, arg1, arg2, arg3, arg4, arg5)));
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel3 = new javax.swing.JPanel();
		equipInfoLabel = new javax.swing.JLabel();
		jPanel4 = new javax.swing.JPanel();
		equipInfoLabel1 = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jPanel3.setBackground(new java.awt.Color(102, 102, 102));

		ImageIcon imageIcon = new ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS view r&r.png"));
		//Image image = imageIcon.getImage();
		//Image newimg = image.getScaledInstance(60, 60,  java.awt.Image.SCALE_SMOOTH);
		//imageIcon = new ImageIcon(newimg); 
		equipInfoLabel.setIcon(imageIcon); // NOI18N
		equipInfoLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Test Numbers",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		equipInfoLabel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				equipInfoLabelMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				equipInfoLabelMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				equipInfoLabelMouseExited(evt);
			}
		});

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
				jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout.setHorizontalGroup(jPanel3Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel3Layout.createSequentialGroup().addContainerGap()
						.addComponent(equipInfoLabel,
								javax.swing.GroupLayout.PREFERRED_SIZE, 128,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(85, Short.MAX_VALUE)));
		jPanel3Layout.setVerticalGroup(jPanel3Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel3Layout.createSequentialGroup().addComponent(
						equipInfoLabel).addContainerGap(69, Short.MAX_VALUE)));

		jPanel4.setBackground(new java.awt.Color(102, 102, 102));
		jPanel4.setBorder(null);
		jPanel4.setFont(new java.awt.Font("Ebrima", 1, 12));

		equipInfoLabel1.setIcon(imageIcon); // NOI18N
		equipInfoLabel1.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Equipment Available",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		equipInfoLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				equipAvailabilityMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				equipInfoLabel1MouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				equipInfoLabel1MouseExited(evt);
			}
		});

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout.setHorizontalGroup(jPanel4Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel4Layout.createSequentialGroup().addContainerGap()
						.addComponent(equipInfoLabel1,
								javax.swing.GroupLayout.PREFERRED_SIZE, 129,
								javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(22, Short.MAX_VALUE)));
		jPanel4Layout.setVerticalGroup(jPanel4Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel4Layout.createSequentialGroup().addComponent(
						equipInfoLabel1).addContainerGap(69, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel3, GroupLayout.PREFERRED_SIZE, 153, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(jPanel4, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(213, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(jPanel4, Alignment.LEADING, 0, 0, Short.MAX_VALUE)
						.addComponent(jPanel3, GroupLayout.DEFAULT_SIZE, 121, Short.MAX_VALUE))
					.addGap(154))
		);
		this.setLayout(layout);
	}// </editor-fold>

	private void equipAvailabilityMouseClicked(java.awt.event.MouseEvent evt) {
		try {
			LabsJP.this.setCursor(busyCursor);
			//AppJFrame.glassPane.activate(null);
			FacilityAvailableEquipmentJd machineConfig = new FacilityAvailableEquipmentJd(
					javax.swing.JOptionPane.getFrameForComponent(this), true);
			machineConfig.setVisible(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, ""+e.getLocalizedMessage(), "Error - Contact System Administrator", JOptionPane.ERROR_MESSAGE);
		} finally {
			//AppJFrame.glassPane.deactivate();
			LabsJP.this.setCursor(defaultCursor);
		}
	}
	
	private void equipInfoLabelMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		equipInfoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view r&r.png")));
	}

	private void equipInfoLabelMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		equipInfoLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS view r&r big.png")));
	}
	
	

	private void equipInfoLabelMouseClicked(java.awt.event.MouseEvent evt) {
		try {
			LabsJP.this.setCursor(busyCursor);
			//AppJFrame.glassPane.activate(null);
			EquipmentInformationJd equipInfo = new EquipmentInformationJd(
					javax.swing.JOptionPane.getFrameForComponent(this), true);
			equipInfo.setVisible(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, ""+e.getLocalizedMessage(), "Error - Contact System Administrator", JOptionPane.ERROR_MESSAGE);
		} finally {
			//AppJFrame.glassPane.deactivate();
			LabsJP.this.setCursor(defaultCursor);
		}
	}
	private void equipInfoLabel1MouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		equipInfoLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view r&r.png")));
	}

	private void equipInfoLabel1MouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		equipInfoLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS view r&r big.png")));
	}
	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel equipInfoLabel;
	private javax.swing.JLabel equipInfoLabel1;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
}