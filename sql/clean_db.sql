﻿
-- Michael
delete from elmis_hiv_site_balances;
delete from elmis_hiv_confirmatory_results ;
delete from elmis_hiv_dispensary_adjustments ;
delete from elmis_hiv_final_test_results ;
delete from elmis_hiv_screening_results ;
delete from elmis_hiv_test_physical_count;
delete from elmis_line_remarks ;
delete from elmis_randr_line_item ;
delete from elmis_submitted_randr ;
-- Moses
delete from elmis_product_qty;
delete from store_physical_count;
delete from elmis_stock_control_card;
delete from elmis_product_qty;
delete from elmis_patient;
-- Mesay 
delete from elmis_dar_transactions;
delete from elmis_ws_requests;
ALTER TABLE elmis_randr_line_item ADD column remarks text;

