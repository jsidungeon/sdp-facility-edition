/*
 * AddUserJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.users;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.UsersDAO;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.email.MailTrigger;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.utils.SdpStyleSheet;

import com.oribicom.tools.publicMethods;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * 
 * @author __USER__
 */
public class changeUserPasswordJD extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private users user;
	private static UsersDAO userDAO;
	MailTrigger userAdd = new MailTrigger();

	/** Creates new form AddUserJD */
	public changeUserPasswordJD(java.awt.Frame parent, boolean modal, users user) {
		super(parent, modal);

		initComponents();
		SdpStyleSheet.configJDialogBackground(this);
		this.user = user;
		userNameJL.setText(this.user.getFirstname() + " "
				+ this.user.getLastname());
		userNameJTF.setText(this.user.getUsername());

		this.setLocationRelativeTo(null);
		this.setSize(438, 286);
		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		passwordJPF = new javax.swing.JPasswordField();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jLabel7 = new javax.swing.JLabel();
		confirmPasswordJPF = new javax.swing.JPasswordField();
		jLabel8 = new javax.swing.JLabel();
		jLabel9 = new javax.swing.JLabel();
		userNameJTF = new javax.swing.JTextField();
		jLabel1 = new javax.swing.JLabel();
		userNameJL = new javax.swing.JLabel();
		CancelJBnt = new javax.swing.JButton();
		resetUserPasswordJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Reset user password");
		setBackground(new java.awt.Color(212, 208, 200));

		SdpStyleSheet.configJPanelBackground(jPanel1);
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		SdpStyleSheet.configOtherJLabel(jLabel5);
		jLabel5.setText("Login Details");

		SdpStyleSheet.configOtherJLabel(jLabel6);
		jLabel6.setText("User Name");

		SdpStyleSheet.configOtherJLabel(jLabel7);
		jLabel7.setText("Password");

		SdpStyleSheet.configOtherJLabel(jLabel8);
		jLabel8.setText("Confirm Password");

		userNameJTF.setEnabled(false);

		SdpStyleSheet.configOtherJLabel(jLabel1);
		jLabel1.setText("User :");

		userNameJL.setText("jLabel2");
		SdpStyleSheet.configOtherJLabel(userNameJL);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout.createSequentialGroup().addGap(
										10, 10, 10).addComponent(jLabel9,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										308,
										javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(159, Short.MAX_VALUE))
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel6)
																						.addComponent(
																								jLabel7)
																						.addComponent(
																								jLabel8)))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addGap(
																				10,
																				10,
																				10)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING,
																								false)
																						.addComponent(
																								jLabel1,
																								javax.swing.GroupLayout.Alignment.LEADING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								jLabel5,
																								javax.swing.GroupLayout.Alignment.LEADING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE))))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																userNameJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																218,
																Short.MAX_VALUE)
														.addComponent(
																userNameJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																218,
																Short.MAX_VALUE)
														.addComponent(
																passwordJPF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																218,
																Short.MAX_VALUE)
														.addComponent(
																confirmPasswordJPF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																218,
																Short.MAX_VALUE))
										.addGap(148, 148, 148)));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { jLabel1, jLabel5, jLabel6, jLabel7,
						jLabel8 });

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { confirmPasswordJPF, passwordJPF,
						userNameJTF });

		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addGap(5, 5, 5)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																userNameJL)
														.addComponent(jLabel1))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel5)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel6)
														.addComponent(
																userNameJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel7)
														.addComponent(
																passwordJPF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel8)
														.addComponent(
																confirmPasswordJPF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(234, 234, 234).addComponent(
												jLabel9)));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { jLabel1, jLabel5, jLabel6, jLabel7,
						jLabel8 });

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { confirmPasswordJPF, passwordJPF,
						userNameJTF });

		CancelJBnt.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBnt.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBnt.setText("Cancel");
		CancelJBnt.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBntActionPerformed(evt);
			}
		});

		resetUserPasswordJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		resetUserPasswordJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Reset password.png"))); // NOI18N
		resetUserPasswordJBtn.setText("Reset user password");
		resetUserPasswordJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						resetUserPasswordJBtnActionPerformed(evt);
					}
				});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 420, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(61, Short.MAX_VALUE))
						.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
							.addComponent(resetUserPasswordJBtn)
							.addGap(18)
							.addComponent(CancelJBnt)
							.addGap(81))))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 196, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(CancelJBnt, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
						.addComponent(resetUserPasswordJBtn, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(21, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void resetUserPasswordJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		String confirm = new String(confirmPasswordJPF.getPassword()).trim();

		String passwordConfirm = new String(passwordJPF.getPassword()).trim();
		String password = new String(passwordJPF.getPassword()).trim();

		// Convert user password

		try {

			password = publicMethods.hash(password);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(AppJFrame.desktop, e
					.getMessage().toString());
		}

		this.user.setPassword(password);

		if (passwordJPF.getText().equals("")) {

			javax.swing.JOptionPane.showMessageDialog(this,
					"Please enter a password");
		} else

		if (passwordConfirm.equals(confirm)) {

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();
			SqlSession session1 = factory.openSession();

			try {

				session.update("updateByUserPrimaryKeySelective", this.user);

				session.commit();

			} finally {

				session.close();
				javax.swing.JOptionPane.showMessageDialog(this,
						" User password changed");

			}

			this.dispose();

			//&}

			// Enter into data base

		} else {

			javax.swing.JOptionPane.showMessageDialog(null,
					" Passwords don't match");

		}

	}

	private void CancelJBntActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		int ok = javax.swing.JOptionPane.showConfirmDialog(this,
				"Are you sure you want to cancel", "", 2, 1);
		// int ok =
		// publicMethods.confirmDialog("Are you sure you want to cancel");
		if (ok == 0) {
			this.dispose();
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				changeUserPasswordJD dialog = new changeUserPasswordJD(
						new javax.swing.JFrame(), true, new users());
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton CancelJBnt;
	private javax.swing.JPasswordField confirmPasswordJPF;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JLabel jLabel8;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPasswordField passwordJPF;
	private javax.swing.JButton resetUserPasswordJBtn;
	private javax.swing.JLabel userNameJL;
	private javax.swing.JTextField userNameJTF;
	// End of variables declaration//GEN-END:variables

}