package org.elmis.facility.domain.model;

public class ElmisRegimen {
	private Integer id;
	private Integer categoryId; // change by regimen category entity
	private ElmisRegimenCategory category;
	private String code;
	private String name;
	private boolean active;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public ElmisRegimenCategory getCategory() {
		return category;
	}
	
	public void setCategory(ElmisRegimenCategory category) {
		this.category = category;
	}
	
}
