package org.elmis.facility.domain.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.LabTest;
import org.elmis.facility.reports.utils.CalendarUtil;

public class LabTestDao {
	private SqlSessionFactory sqlSessionFactory;
	public LabTestDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public boolean checkSubmission(@Param("machineCode")String machineCode, @Param("endDate") Date endDate)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("machineCode", machineCode);
		map.put("endDate", endDate);
		
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			int numberOfSubmissions = session.selectOne("LabTest.check_submission", map);
			if (numberOfSubmissions < 1)
				return false;
			else
				return true;
			
		}
		finally
		{
			session.close();
		}
	}
	public void deleteSubmission(@Param("machineCode")String machineCode, @Param("endDate") Date endDate)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("machineCode", machineCode);
		map.put("endDate", endDate);
		SqlSession session = sqlSessionFactory.openSession(false);

		try{
				session.delete("LabTest.delete_equipment_line_item", map);
				session.delete("LabTest.delete_reagent_line_item", map);
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public List<LabTest> getTests(@Param("machineCode") String machineCode)
	{
		Map<String, String> map = new HashMap<>();
		map.put("machineCode", machineCode);
		
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("LabTest.get_reagents", map);
		}
		finally
		{
			session.close();
		}
	}
	public List<LabTest> getReagents(@Param("machineCode") String machineCode)
	{
		Map<String, String> map = new HashMap<>();
		map.put("machineCode", machineCode);
		
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("LabTest.reagents_by_machine", map);
		}
		finally
		{
			session.close();
		}
	}
	public void submitEquipmentMonthlyData(LabTest testData)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("newStatus", testData.getIsEquipmentFunctioning());
		map.put("equipmentCode", testData.getMachineCode());
		
		SqlSession session = sqlSessionFactory.openSession(false);
		try
		{
			session.update("Equipment.change_functionality_status", map);
			session.insert("LabTest.insert_equipment_data", testData);
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public void submitReagentMonthlyData(LabTest testData)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		try
		{
			session.insert("LabTest.insert_reagent_data", testData);
			session.commit();
		}
		finally
		{
			session.close();
		}
		
	}
	public List<LabTest> getPsqlReagents()
	{
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("LabTest.reagents");
		}
		finally
		{
			session.close();
		}
	}
	public List<LabTest> getLabEquipmentInfoReport(@Param("dateTo") Date dateTo)
	{
		Map<String, Date> map = new HashMap<>();
		map.put("dateTo", dateTo);
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("LabTest.get_equipment_test_info", map);
		}
		finally
		{
			session.close();
		}
	}
	public Set<String> getOrderableReagents(@Param("toDate") Date toDate)
	{
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("toDate", toDate);
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			Set<String> set = new HashSet<>();
			List<LabTest> orderableList = session.selectList("LabTest.orderable_reagents", map);
			
			for (LabTest labTest: orderableList)
				set.add(labTest.getReagentCode());
			return set;
		}
		finally
		{
			session.close();
		}
	}
	public static void main(String[] args) {
		CalendarUtil cal = new CalendarUtil();
		Set<String> x = new LabTestDao().getOrderableReagents(cal.getSqlDate("2014-02-28"));
		
		for (String s: x)
		System.out.println(s);

	}

}
