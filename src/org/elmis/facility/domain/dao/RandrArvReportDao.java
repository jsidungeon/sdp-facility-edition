package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.RandrArdDrug;
import org.elmis.facility.reporting.model.RandrArdDrugReport;

public class RandrArvReportDao {
	
	private SqlSessionFactory sqlSessionFactory;

	public RandrArvReportDao(){
	
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	/**
	 * Returns the list of all Contact instances from the database.
	 * @return the list of all Contact instances from the database.
	 */
	@SuppressWarnings("unchecked")
	public List<RandrArdDrugReport> selectAll(){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<RandrArdDrugReport> list = session.selectList("RandrArdDrugReport.getAll");
			return list;
		} finally {
			session.close();
		}
	}
	
	public List<RandrArdDrug> checkSubmission(@Param("month") int month, @Param("year") int year, @Param("reportCode") int reportCode)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			Map<String, Integer> map = new HashMap<String, Integer>();
	        map.put("month", month);
	        map.put("year", year);
	        map.put("reportCode", reportCode);
	        List<RandrArdDrug> list = session.selectList("RandrArdDrugReport.checkReportSubmission", map);
			//RandrArdDrug randrArdDrug = (RandrArdDrug) session.selectOne("RandrArdDrugReport.checkReportSubmission",1);
			return list;
		}
		finally {
			session.close();
		}
		
	}
	
	public List<RandrArdDrug> getLabUsageData(@Param("labMachine") String labMachine, @Param("programCode") int programCode, @Param("month") int month, @Param("year") int year)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("labMachine", labMachine);
			map.put("programCode", programCode);
	        map.put("month", month);
	        map.put("year", year);
	        
			List<RandrArdDrug> randrArdDrug = session.selectList("RandrArdDrugReport.getLabMachine",map);
			return randrArdDrug;
		} finally {
			session.close();
		}
	}

	/**
	 * Returns a Contact instance from the database.
	 * @param id primary key value used for lookup.
	 * @return A Contact instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public RandrArdDrug selectById(int id){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			RandrArdDrug randrArdDrug = (RandrArdDrug) session.selectOne("RandrArdDrugReport.getById",id);
			return randrArdDrug;
		} finally {
			session.close();
		}
	}

	/**
	 * Updates an instance of RandrArdDrugReport in the database.
	 * @param contact the instance to be updated.
	 */
	public void update(RandrArdDrug randrArdDrug){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.update("RandrArdDrugReport.update", randrArdDrug);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Insert an instance of RandrArdDrugReport into the database.
	 * @param contact the instance to be persisted.
	 */
	public void insert(RandrArdDrugReport randrArdDrugReport){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.insert("RandrArdDrugReport.insert", randrArdDrugReport);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Delete an instance of RandrArdDrugReport from the database.
	 * @param id primary key value of the instance to be deleted.
	 */
	public void delete(int id){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.delete("RandrArdDrugReport.deleteById", id);
			session.commit();
		} finally {
			session.close();
		}
	}

}
