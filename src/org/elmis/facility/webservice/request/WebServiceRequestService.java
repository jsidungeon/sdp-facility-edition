package org.elmis.facility.webservice.request;

import java.util.UUID;

public class WebServiceRequestService {
	
	private static final String REQUEST_TYPE_RNR = "RNR";
	private static final String REQUEST_STATUS_PENDING = "PENDING";
	private static final String REQUEST_NAME_SUBMIT_RNR = "RNR";
	public void submitRnR(String rnrJson){
		WebServiceRequestDao dao = new WebServiceRequestDao();
		WebServiceRequest wsRequest = new WebServiceRequest(UUID.randomUUID().toString(), 
				REQUEST_NAME_SUBMIT_RNR , REQUEST_TYPE_RNR , REQUEST_STATUS_PENDING , rnrJson);
		dao.insertReqeuest(wsRequest);
	}

}
