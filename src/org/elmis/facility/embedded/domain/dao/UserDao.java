package org.elmis.facility.embedded.domain.dao;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.database.connection.MyBatisSqliteConnectionFactory;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.reports.utils.CalendarUtil;

import com.oribicom.tools.publicMethods;

public class UserDao {
	//private SqlSessionFactory sqliteSqlSessionFactory;
	private SqlSessionFactory psqlSessionFactory; 
	{
		//sqliteSqlSessionFactory = MyBatisSqliteConnectionFactory.getSqlSessionFactory();
		psqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public users getSqliteUser(users user)
	{
		return null;
	}
	/*public void updateSqliteTableUsers()
	{
		SqlSession sqliteSession = sqliteSqlSessionFactory.openSession(false);
		SqlSession psqlSession = psqlSessionFactory.openSession();
		try
		{
			int numberOfSqliteUsers = sqliteSession.selectOne("User.get_sqlite_users");
			System.out.println("Number of sqlite users : "+numberOfSqliteUsers);
			if (numberOfSqliteUsers == 0){
				List<users> psqlUsers = psqlSession.selectList("User.get_psql_users");
				System.out.println("Number of Psql users "+psqlUsers.size());
				for (users user: psqlUsers)
					sqliteSession.insert("User.insert_sqlite_user", user);
				sqliteSession.commit();
			}
			else{
				Map<String, Object> map = new HashMap<>();
				String timeStamp = sqliteSession.selectOne("User.get_modifications");
				Timestamp timestamp = new CalendarUtil().getSqlTimeStamp(timeStamp);
				System.out.println(": "+timestamp);
				map.put("ts", timestamp);
				List<users> modifiedPsqlUsers = psqlSession.selectList("User.get_changes", map);

				if (!modifiedPsqlUsers.isEmpty()){

					for (int i = 0; i< modifiedPsqlUsers.size(); i++){
						users user = modifiedPsqlUsers.get(i);
						String userName = user.getUsername();
						map.put("userName", userName);
						int x = sqliteSession.selectOne("User.get_sqlite_user", map);
						System.out.println(x+" : "+userName);
						if (x == 0)
						{
							sqliteSession.insert("User.insert_sqlite_user", user);
							sqliteSession.commit();
						}
						else{
							sqliteSession.delete("User.delete_sqlite_user", user);
							sqliteSession.insert("User.insert_sqlite_user", user);
							sqliteSession.commit();
						}
					}
				}
			}
		}

		finally
		{
			sqliteSession.close();
			psqlSession.close();
		}
	}
*/
	public boolean changeUserPassword(String userName, String oldPassword, String newPassword){
		SqlSession psqlSession = psqlSessionFactory.openSession();
		newPassword = publicMethods.hash(newPassword);
		oldPassword = publicMethods.hash(oldPassword);
		Map<String, String> map = new HashMap<String, String>();
		map.put("userName", userName);
		map.put("oldPassword", oldPassword);
		map.put("newPassword", newPassword);
		try
		{
			Integer userExists = psqlSession.selectOne("User.check_password", map);
			
			if (userExists == null)
				userExists = 0;
			if (userExists > 0){
				psqlSession.update("User.change_password",map);
				psqlSession.commit();
				return true;
			}
			else
				return false;
		}
		finally{
			psqlSession.close();
		}
	}
	/*public static void main(String[] args) {
		new UserDao().updateSqliteTableUsers();
	}*/
}
