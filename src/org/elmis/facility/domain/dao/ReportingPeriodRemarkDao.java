/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportingPeriodRemark;
import org.elmis.facility.reporting.model.StorePhysicalCount;

/**
 * @author MMwebaze
 *
 */
public class ReportingPeriodRemarkDao {

	private SqlSessionFactory sqlSessionFactory;
	
	public ReportingPeriodRemarkDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public void insertRemark(ReportingPeriodRemark remark)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("programCode", remark.getProgramCode());
		map.put("reportPeriodFrm", remark.getReportPeriodFrm());
		map.put("reportPeriodTo", remark.getReportPeriodTo());
		try {			
			session.delete("ReportingPeriodRemark.delRemark", map);
			session.insert("ReportingPeriodRemark.insertRemark",  remark);
			session.commit();
		}
		finally {
			session.close();
		}
	}
	public String getRemark(@Param("programCode") int programCode, @Param("reportPeriodFrm") Date reportPeriodFrm, @Param("reportPeriodTo") Date reportPeriodTo)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("programCode", programCode);
		map.put("reportPeriodFrm", reportPeriodFrm);
		map.put("reportPeriodTo", reportPeriodTo);

		try {
			List<ReportingPeriodRemark> list = session.selectList("ReportingPeriodRemark.getRemark", map);

			return list.get(0).getRemarks();
		}
		finally {
			session.close();
		}
	}
	
	public void deleteRemark(@Param("programCode") int programCode, @Param("reportPeriodFrm") Date reportPeriodFrm, @Param("reportPeriodTo") Date reportPeriodTo)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("programCode", programCode);
		map.put("reportPeriodFrm", reportPeriodFrm);
		map.put("reportPeriodTo", reportPeriodTo);
		
		try {
			
			session.delete("ReportingPeriodRemark.delRemark", map);
		}
		finally {
			session.close();
		}
	}
}
