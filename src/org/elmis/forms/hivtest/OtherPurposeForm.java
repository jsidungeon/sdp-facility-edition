package org.elmis.forms.hivtest;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import org.elmis.facility.domain.dao.HivTestPurposeDao;
import org.elmis.facility.domain.model.HivTestPurpose;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.SdpStyleSheet;

public class OtherPurposeForm extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private static String reason;
	private static boolean reasonNotGiven = false;
	private String[] reasonArray = {"Please Select a Reason"};
	private List<String> purposeList = new ArrayList<>();
	private JComboBox comboBox = new JComboBox();
	private int selectedIndex = 0;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Properties prop = new Properties(System.getProperties());
			FileInputStream fis = null;
			try {
				fis = new FileInputStream("Programmproperties.properties");
				prop.load(fis);

				System.setProperties(prop);

				fis.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
			String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
			System.setProperty("dbpassword", dbpassword);
			OtherPurposeForm dialog = new OtherPurposeForm(new javax.swing.JDialog(), true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
			if (OtherPurposeForm.isReasonGiven())
				System.out.println(">>>> "+OtherPurposeForm.getReason());
			else
				System.out.println("No reason has been given...");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public OtherPurposeForm(javax.swing.JDialog parent, boolean modal) {
		super(parent, modal);
		setTitle("Purpose of Test");
		setBounds(100, 100, 306, 241);
		getContentPane().setLayout(new BorderLayout());
		SdpStyleSheet.configJPanelBackground(contentPanel);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		purposeList.add("Please select a reason");
		for (HivTestPurpose p : new HivTestPurposeDao().getHivTestPurposes())
			purposeList.add(p.getPurpose());
		reasonArray = purposeList.toArray(new String[purposeList.size()]);

		
		comboBox.setModel(new javax.swing.DefaultComboBoxModel(reasonArray));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				purposeComboBoxActionEvent(evt);
			}
		});
		comboBox.setBounds(10, 11, 210, 38);
		contentPanel.add(comboBox);
		{
			JPanel buttonPane = new JPanel();
			SdpStyleSheet.configJPanelBackground(buttonPane);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				SdpStyleSheet.configCancelButton(this, cancelButton);
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if (selectedIndex == 0)
							dispose();
						else{
							int ok = JOptionPane.showConfirmDialog(OtherPurposeForm.this, "Are you sure you want to cancel ?", null, JOptionPane.YES_NO_OPTION);

							if (ok == JOptionPane.YES_OPTION){
								reasonNotGiven = false;
								OtherPurposeForm.this.dispose();
							}
						}
						//dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				getRootPane().setDefaultButton(cancelButton);
			}
			{
				JButton submitButton = new JButton("Submit");
				submitButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {

						if (selectedIndex == 0){
							JOptionPane.showMessageDialog(null, "Please submit a reason for carrying out these tests.", "Reason for Test Error!",
									JOptionPane.INFORMATION_MESSAGE);
							reasonNotGiven = false;
						}
						else{
							reasonNotGiven = true;
							dispose();
						}
					}
				});
				submitButton.setActionCommand("Submit");
				buttonPane.add(submitButton);
			}
		}
		setLocationRelativeTo(parent);
		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				
				if (selectedIndex == 0)
					OtherPurposeForm.this.dispose();
				else{
					int ok = JOptionPane.showConfirmDialog(OtherPurposeForm.this, "Are you sure you want to cancel ?", null, JOptionPane.YES_NO_OPTION);
					
					if (ok == JOptionPane.YES_OPTION){
						reasonNotGiven = false;
						OtherPurposeForm.this.dispose();
					}
				}

			}
		});
	}
	protected void purposeComboBoxActionEvent(ActionEvent evt) {
		selectedIndex = comboBox.getSelectedIndex();
		
		if (selectedIndex > 0){
			reason = comboBox.getSelectedItem().toString();
		}
	}

	public static String getReason(){
		return reason;
	}
	public static boolean isReasonGiven(){
		return reasonNotGiven;
	}
}
