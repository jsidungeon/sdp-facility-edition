package org.elmis.forms.stores.receiving;

/***************************************************
	 * STARTING CHILD FORM FOR ADJUSTMENTS 
	 ***************************************************/
/**
 *
 * @author  __MKausa__
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.swing.AbstractCellEditor;
import javax.swing.ComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;

import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.dao.AdjustmentDao;
import org.elmis.facility.domain.model.Adjustment;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.domain.model.facilities;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
//import com.oribicom.tools.TableModel;

public class HivAdjustmentJD extends javax.swing.JDialog   {
	

	private FacilitySetUpDAO callfacility;
	private Facility_Types facilitytype = null;
	private Facility facility = null;
	private int siteId;
	private Map<String, Integer> productBalanceMap;
	private String dispensaryName = System.getProperty("dp_name");
	private int remarkRowSelected;
	private Map<String, Boolean> adjustmentAdditiveMap = new HashMap<>();
	private String typeCode;
	private TableColumnAligner tablecolumnaligner; 
	private Timestamp productdeliverdate;
    private JComboBox comboBox;
    private ComboBoxModel model;
    private JTextComponent editor;
	private List<facilities> facilitiesnameList =  new LinkedList<>();
	private Elmis_Stock_Control_Card Adjustmentelmis_stock_control_card; 
	private JComboBox facilityList = new JComboBox();
	private MutableComboBoxModel modelfacilities = (MutableComboBoxModel) facilityList
			.getModel();
	private JComboBox facilitynamecomboList = new JComboBox();
	private MutableComboBoxModel modelfacilitiesList = (MutableComboBoxModel) facilitynamecomboList
			.getModel();
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList<>();
	@SuppressWarnings("unchecked")
	//private ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	private List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList<>();
	private FacilityProductsDAO Facilityproductsmapper = null;
	private FacilitySetUpDAO facilitiesmapper =  null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	//public static Boolean cansave = false;
	private String productName = "";
	private String Pstrength = "";
	private String pCode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer productId = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentName = "";
	private String facilityFromAdjustmentName = "";
	private String facilityToAdjustmentName = "";
	private final String[] adjustmentTableColumnNames = {
		"Adjustment Type", "Quantity", "Remarks"};
	private static final int rows_RegisterAdjustmentsfproducts = 0;
    private final Object[] defaultv_RegisterAdjustmentsfacilityapprovedproducts = { "", "",
		""};
    private String[][] data = null;
	private DefaultTableModel adjustmentTableModel= new DefaultTableModel(data, adjustmentTableColumnNames) {

		@Override
		public boolean isCellEditable(int row, int column) {
			if (column == 1)
				return true;
			else if (column == 2)
				return true;
			else
				return false;
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2)
				return String.class;
			if (columnIndex == 1)
				  return Integer.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};

	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public HivAdjustmentJD(java.awt.Frame parent, boolean modal,String adjustpcode,int adjustproductid,Timestamp adjustmentdateandtime,String progname,String productName, String pstrenth, int siteId) {
		super(parent, modal);
		this.siteId = siteId;
		productBalanceMap = new AdjustmentDao().getHivProductBalances(siteId);
		productdeliverdate = adjustmentdateandtime;
		pCode = adjustpcode;
		System.out.println(siteId+" Product code : "+pCode);
		productId = adjustproductid;
		facilityprogramcode = progname;
		this.productName = productName;
		Pstrength = pstrenth;
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(1001, 450);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		saveJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record HIV Dispensary Adjustments ");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
			public void windowActivated(java.awt.event.WindowEvent evt) {
				
				formwindowActivated(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		saveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		saveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		saveJBtn.setText("Save");
		saveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		saveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				saveJBtnMouseClicked(evt);
			}
		});
		saveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnCancel = new JButton();
		btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCancelMouseClicked(evt);
			}

			
		});
		btnCancel.setText("Cancel");
		btnCancel.setFont(new Font("Ebrima", Font.BOLD, 12));
		
		enterfacilityname = new JLabel("Transfer from facility");
		enterfacilityname.setIcon(new ImageIcon(testMultipleAdjustments.class.getResource("/elmis_images/eLMIS basic info small.png")));
		enterfacilityname.setFont(new Font("Ebrima", Font.BOLD, 13));
		enterfacilityname.setForeground(Color.WHITE);
		
	   transfertofacilityJCB = new JComboBox();
		transfertofacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transfertofacilityJCBActionPerformed(evt);
			}
		});
		transfertofacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transfertofacilityJCB.setModel(modelfacilities);
	    transferoutJL = new JLabel("Transfer to facility");
		transferoutJL.setIcon(new ImageIcon(testMultipleAdjustments.class.getResource("/elmis_images/eLMIS basic info small.png")));
		transferoutJL.setForeground(Color.WHITE);
		transferoutJL.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		 transferfromfacilityJCB = new JComboBox();
		transferfromfacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transferfromfacilityJCBActionPerformed(evt);
			}
		});
		transferfromfacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transferfromfacilityJCB.setModel(modelfacilitiesList);
		
		JLabel lblNewLabel = new JLabel("Adjustments for : ");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		Adjustmentproductnamelbl = new JLabel("");
		Adjustmentproductnamelbl.setForeground(Color.WHITE);
		Adjustmentproductnamelbl.setFont(new Font("Ebrima", Font.BOLD, 14));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(39)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(saveJBtn))
						.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
							.addGroup(jPanel1Layout.createSequentialGroup()
								.addComponent(lblNewLabel)
								.addGap(18)
								.addComponent(Adjustmentproductnamelbl, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE))
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 532, GroupLayout.PREFERRED_SIZE)))
					.addGap(32)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(transferfromfacilityJCB, 0, 339, Short.MAX_VALUE)
						.addComponent(transfertofacilityJCB, 0, 339, Short.MAX_VALUE)
						.addComponent(enterfacilityname)
						.addComponent(transferoutJL))
					.addGap(53))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(Adjustmentproductnamelbl, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
					.addGap(31)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(enterfacilityname)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transferfromfacilityJCB, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
							.addGap(25)
							.addComponent(transferoutJL)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transfertofacilityJCB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(saveJBtn)
								.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(209, Short.MAX_VALUE))
		);

	
		registerAdjustmentJT = new JTable();
		scrollPane.setViewportView(registerAdjustmentJT);
		registerAdjustmentJT.setShowVerticalLines(false);
		registerAdjustmentJT.setShowHorizontalLines(true);
		registerAdjustmentJT.setShowGrid(false);
		registerAdjustmentJT.setRowSelectionAllowed(true);
		registerAdjustmentJT.setModel(adjustmentTableModel);
		registerAdjustmentJT.getColumnModel().getColumn(2).setCellEditor(new RemarkTextAreaCellEditor());
		registerAdjustmentJT.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		registerAdjustmentJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						registerAdjustmentJTMouseClicked(evt);
					}
				});
		registerAdjustmentJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
					}
				});
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 995, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>

	protected void registerAdjustment() {
		//Adjustment adjustment = new Adjustment();
		System.out.println(">>>>> "+pCode);
		
	}


	protected void transferfromfacilityJCBActionPerformed(ActionEvent evt) {
		JComboBox cb = (JComboBox) evt.getSource();
		facilityFromAdjustmentName = (String) cb.getSelectedItem();
	}


	protected void transfertofacilityJCBActionPerformed(ActionEvent evt) {
		JComboBox cb = (JComboBox) evt.getSource();
		facilityToAdjustmentName = (String) cb.getSelectedItem();
	}

	public  void RegisterAdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		//System.out.println("test pop up adjustments");		

	}
	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void populateProgramProductTable(List<Losses_Adjustments_Types> registerAdjustmentfapprovProducts) {

		for (int i = 0; i < registerAdjustmentfapprovProducts.size(); i++)
		{
			String adjustmentTypeName = registerAdjustmentfapprovProducts.get(i).getName();
			Boolean additive = registerAdjustmentfapprovProducts.get(i).getAdditive();
			adjustmentTableModel.addRow(new String[registerAdjustmentfapprovProducts.size()]);
			registerAdjustmentJT.getModel().setValueAt(adjustmentTypeName, i, 0);
			adjustmentAdditiveMap.put(adjustmentTypeName, additive);
		}
	}	
	
	
	private void saveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		int sumNegativeAdjustments = 0;
		int rowCount = registerAdjustmentJT.getRowCount();
		java.util.Date date = new java.util.Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		List<Adjustment> adjustmentList = new ArrayList<>();
		for (int i = 0; i < rowCount; i++)
		{
			try {
				Object adjustmentQtyObj = adjustmentTableModel.getValueAt(i,1);
				Object adjustmentRmksObj = adjustmentTableModel.getValueAt(i, 2);
				
				if (adjustmentQtyObj != null){
					Adjustment adjustment = new Adjustment();
					adjustment.setSiteId(siteId);
					adjustment.setProductCode(pCode);
					adjustment.setDispensaryName(dispensaryName);
					Integer positiveAdjustment = (Integer
							.parseInt(adjustmentQtyObj.toString()));
					if (positiveAdjustment < 0)
						positiveAdjustment = positiveAdjustment * -1;
					String adjustmentType = ""+registerAdjustmentJT.getValueAt(i, 0);
					adjustment.setAdjustmentType(adjustmentType);
					if (adjustmentAdditiveMap.get(adjustmentType))
						adjustment.setAdjustmentQty(positiveAdjustment);
					else{
						int negativeAdjustment = positiveAdjustment * -1;
						adjustment.setAdjustmentQty(negativeAdjustment);
						sumNegativeAdjustments += negativeAdjustment; 
					}
					if (adjustmentType.equalsIgnoreCase("TRANSFER_IN")){
						adjustment.setTransferFrom(transferfromfacilityJCB.getSelectedItem().toString());
						adjustment.setTransferTo("");
					}
					else if (adjustmentType.equalsIgnoreCase("TRANSFER_OUT")){
						adjustment.setTransferTo(transfertofacilityJCB.getSelectedItem().toString());
						adjustment.setTransferFrom("");
					}
					else{
						adjustment.setTransferTo("");
						adjustment.setTransferFrom("");
					}
					if (adjustmentRmksObj != null)
						adjustment.setAdjustmentRemark(adjustmentRmksObj.toString());
					else
						adjustment.setAdjustmentRemark("");
					adjustment.setTimestamp(timestamp);
					adjustmentList.add(adjustment);
				}
				
			}catch(NumberFormatException e){
				e.getStackTrace();
			}

		}
		
		int totalSumOfAdjustments = 0;
		int productBalance = 0;
		if (productBalanceMap.get(pCode) != null)
			productBalance = productBalanceMap.get(pCode);
		for (int i = 0; i < adjustmentList.size(); i++){
			totalSumOfAdjustments += adjustmentList.get(i).getAdjustmentQty();
		}
		if (totalSumOfAdjustments < 0){
			int absolute = sumNegativeAdjustments * -1;
			if (absolute > productBalance){
				JOptionPane.showMessageDialog(this,
						"<html> <font color=red>"+pCode+"</font> Adjustments Quantity <font color=red>("+totalSumOfAdjustments+")</font> you have entered is invalid. <br>Please carry out the necessary adjustment first.</html>",	pCode+" Adjustment - Calculated Balance Anomaly",JOptionPane.WARNING_MESSAGE);
			}
			else{
				new AdjustmentDao().saveHivAdjustments(adjustmentList);
				javax.swing.JOptionPane.showMessageDialog(this,productName+" ("+pCode+ ")\n adjustments have been saved.");
				dispose();
			}
		}
		else{
			new AdjustmentDao().saveHivAdjustments(adjustmentList);
			javax.swing.JOptionPane.showMessageDialog(this,productName+" ("+pCode+ ")\n adjustments have been saved.");
			dispose();
		}
	}
	private void btnCancelMouseClicked(MouseEvent evt) {
		dispose();
	}
	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {

	}
	
	protected void registerAdjustmentJTMouseClicked(
			MouseEvent evt) {
		adjustmentName = registerAdjustmentJT.getValueAt(registerAdjustmentJT.getSelectedRow(),0).toString();

		if(this.adjustmentName.equals("TRANSFER_IN")) {
			this.transferfromfacilityJCB.setEnabled(true);
			this.enterfacilityname.setEnabled(true);

			this.transfertofacilityJCB.setEnabled(false);
			this.transferoutJL.setEnabled(false);
		}else if (this.adjustmentName.equals("TRANSFER_OUT")){
			this.transfertofacilityJCB.setEnabled(true);
			this.transferoutJL.setEnabled(true);
			this.transferfromfacilityJCB.setEnabled(false);
			this.enterfacilityname.setEnabled(false);
		}else{
			this.transfertofacilityJCB.setEnabled(false);
			this.transferfromfacilityJCB.setEnabled(false);;
			this.enterfacilityname.setEnabled(false);
			this.transferoutJL.setEnabled(false);
		}
	}			

	private void formwindowActivated(java.awt.event.WindowEvent evt){
		this.transferfromfacilityJCB.setEnabled(false);
		this.transferfromfacilityJCB.setEnabled(false);
		this.transfertofacilityJCB.setEnabled(false);
       this.enterfacilityname.setEnabled(false);
       transferoutJL.setEnabled(false);
	}
	

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new FacilityProductsDAO();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilitiesmapper = new FacilitySetUpDAO();
		facilitiesnameList = facilitiesmapper.getfacilitynameList();
		
		for (facilities f : facilitiesnameList) {
			modelfacilities.addElement(f.getName());

		}
        
		for (facilities f : facilitiesnameList) {
			this.modelfacilitiesList.addElement(f.getName());

		}
		AutoCompleteDecorator.decorate(this.transferfromfacilityJCB);
		AutoCompleteDecorator.decorate(this.transfertofacilityJCB);

	
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		this.registerAdjustmentJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		registerAdjustmentJT.setFont(new java.awt.Font(
				"Ebrima", 0, 20));
		this.registerAdjustmentJT.setRowHeight(30);
		populateProgramProductTable(facilityAdjustmentList);
		

			if (!this.productName.equals("")) {

				//if the name is too long cut it short for display purpose
				if (this.productName.length() >= 40) {

					this.Adjustmentproductnamelbl.setText(productName.substring(
							0, 40) + "\n ," + " " + Pstrength);
				} else {

					Adjustmentproductnamelbl.setText(productName + " " + Pstrength);
				}
			}
			
		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
	}
	
		/**
	 * @param args the command line arguments
	 */

	private javax.swing.JButton saveJBtn;
	private javax.swing.JPanel jPanel1;
	private JTable registerAdjustmentJT;
	private JLabel enterfacilityname;
	private JLabel transferoutJL;
	private JLabel Adjustmentproductnamelbl;
	@SuppressWarnings("rawtypes")
	private static JComboBox transfertofacilityJCB;
	@SuppressWarnings("rawtypes")
	private  JComboBox  transferfromfacilityJCB;
	class RemarksTextArea extends JTextArea{
		public RemarksTextArea()
		{
			addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent evt) {
					if(evt.getKeyCode() == KeyEvent.VK_ENTER)
					{
						registerAdjustmentJT.requestFocus();
						registerAdjustmentJT.changeSelection(remarkRowSelected + 1,2,false, false);
						if (registerAdjustmentJT.isEditing())
							registerAdjustmentJT.getCellEditor().stopCellEditing();
					}
				}
			});
		}
	}
	public class RemarkTextAreaCellEditor extends AbstractCellEditor implements TableCellEditor {

		private RemarksTextArea remarksTextArea;

		public RemarkTextAreaCellEditor () {
			remarksTextArea = new RemarksTextArea();
		}

		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {
			remarkRowSelected = row;
			remarksTextArea.setLineWrap(true);
			JScrollPane remarksScrollPane = new JScrollPane(remarksTextArea);
			remarksScrollPane
			.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			remarksTextArea.setText((String) value);
			return remarksScrollPane;
		}

		public Object getCellEditorValue() {
			String remarks = ((JTextArea)remarksTextArea).getText();
			return ((JTextArea)remarksTextArea).getText();
		}
	}
}
	
	/**********************************************************
	 * END CHILD FORM 
	 **********************************************************/
			 
