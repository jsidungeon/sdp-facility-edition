package org.elmis.facility.service.arv;

import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;

public interface IARVDispensing {
	public void saveDARTxN(Elmis_Dar_Transactions darTxn);
	
	public void updateARVBalace(Double qtyDeducted,String productCode,Integer dispensationUnitId);
	
	public Elmis_Patient getARVClientByART(String art);
	
	public Elmis_Patient getARVClientByNRC(String nrc);
	
	public void registerClient(Elmis_Patient client);
	
	public void updateARVClient(Elmis_Patient client);
}
