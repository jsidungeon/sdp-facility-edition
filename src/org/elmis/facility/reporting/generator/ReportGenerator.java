package org.elmis.facility.reporting.generator;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JRViewer;
import net.sf.jasperreports.view.JasperViewer;

import org.elmis.facility.domain.dao.ArvRequisitionDao;
import org.elmis.facility.domain.dao.EmlipRequisitionDao;
import org.elmis.facility.domain.dao.HivRequisitionDao;
import org.elmis.facility.domain.dao.HivTestResultDao;
import org.elmis.facility.domain.dao.LabRequisitionDao;
import org.elmis.facility.domain.dao.LabTestDao;
import org.elmis.facility.domain.model.HivTestResult;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reporting.DarJasperViewerJd;
import org.elmis.facility.reporting.JRViewJd;
import org.elmis.facility.reporting.model.LabTest;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reporting.model.ReportingPeriodRemark;
import org.elmis.facility.reporting.model.StorePhysicalCount;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.reports.utils.PropertyLoader;
import org.elmis.facility.reports.utils.ReportStorageManager;
/**
 * 
 * @author Michael Mwebaze
 *
 */
public class ReportGenerator {
	private CalendarUtil calenderUtil = new CalendarUtil();
	private Date prevDate;
	private Map<String, Object> parameters = new HashMap<>();
	private ReportStorageManager storageManager = new ReportStorageManager();
	//private String isEmergencyReport = "NO";

	public ReportGenerator()
	{
		prevDate = calenderUtil.getSqlDate(calenderUtil.getPreviousLastDayOfMonth(1));
		if (!storageManager.isFolderAvailable())
			storageManager.createReportFolder();
		parameters.put("district", PropertyLoader.getDistrict());
		parameters.put("province", PropertyLoader.getProvince());
		parameters.put("facility",PropertyLoader.getFacilityName());
	}
	/**
	 * 
	 * @param frmDate
	 * @param toDate
	 * @param normalOrder
	 * @param remarks
	 * @param jDialog
	 */
	public void generateHivReport(final String frmDate, final String toDate, final String normalOrder, final String remarks, final JDialog jDialog)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(2));
		parameters.put("emergencyOrderPoint", PropertyLoader.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		List<ReportRequisition> dispensedList = new HivRequisitionDao().getHivDispensaryIssues(dateFrm, dateTo, remarks, normalOrder);

		if (!dispensedList.isEmpty())
		{
			JasperDesign hivDesign = null;
			JasperReport hivRpt = null;
			JasperPrint hivPrnt = null;

			JRBeanCollectionDataSource hivDataSrc = new JRBeanCollectionDataSource(dispensedList);

			//String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R HIV.pdf";
			//String sourceFileName = "C:/Users/mmwebaze/Workspaces/MyEclipse Professional/sdp/Reports/ARV_MASTER_REPORT.jasper";
			String reportType = "HIV Report & Requisition for the period "+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate);
			try
			{
				//hivDesign = JRXmlLoader.load("./Reports/HIV_MASTER_REPORT.jrxml");
				//hivRpt = JasperCompileManager.compileReport(hivDesign);
				//hivPrnt  = JasperFillManager.fillReport(hivRpt, parameters, hivDataSrc);
				String compiledRpt = "./Reports/HIV_MASTER_REPORT.jasper";
				hivPrnt  = JasperFillManager.fillReport(compiledRpt, parameters, hivDataSrc);
				//printFileName = JasperFillManager.fillReportToFile(sourceFileName, parameters, arvDataSrc);
				JRViewer jv = new JRViewer(hivPrnt);
				new JRViewJd(null, true, 2, true, jv, reportType, hivPrnt, dateFrm, dateTo, normalOrder).setVisible(true);	
			}
			catch(JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING HIV Report & Requisition for the period<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("HIV DATA PROCESSING ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No HIV Drugs Related data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateArvDrugsReport(final String frmDate, final String toDate, final String normalOrder, final String remarks, final JDialog jDialog)
	{
		
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		reportRemark.setProgramCode(1);

		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(1));
		parameters.put("emergencyOrderPoint", PropertyLoader.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		List<ReportRequisition> dispensedList = new ArvRequisitionDao().getArvDispensaryIssues(dateFrm, dateTo, remarks, normalOrder); 
		if (!dispensedList.isEmpty())
		{
			JasperDesign arvDesign = null;
			JasperReport arvRpt = null;
			JasperPrint arvPrnt = null;

			JRBeanCollectionDataSource arvDataSrc = new JRBeanCollectionDataSource(dispensedList);

			//String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R ARV.pdf";
			String reportType = "ARV Report & Requisition for the period "+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate);
			//String sourceFileName = "C:/Users/mmwebaze/Workspaces/MyEclipse Professional/sdp/Reports/ARV_MASTER_REPORT.jasper";
			try
			{
				/*arvDesign = JRXmlLoader.load("./Reports/ARV_MASTER_REPORT.jrxml");
				arvRpt = JasperCompileManager.compileReport(arvDesign);
				arvPrnt  = JasperFillManager.fillReport(arvRpt, parameters, arvDataSrc);*/
				String arvSrcRpt = "./Reports/ARV_MASTER_REPORT.jasper";
				arvPrnt  = JasperFillManager.fillReport(arvSrcRpt, parameters, arvDataSrc);
				//printFileName = JasperFillManager.fillReportToFile(sourceFileName, parameters, arvDataSrc);
				JRViewer jv = new JRViewer(arvPrnt);
				new JRViewJd(null, true, 3, true, jv, reportType, arvPrnt,  dateFrm, dateTo, normalOrder).setVisible(true);	
			}
			catch(JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING ARV Report & Requisition for the period<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("ARV DATA PROCESSING ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No ARV Drugs Related data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateLabUsageReport(String frmDate, String toDate, String normalOrder, String remarks, JDialog jDialog)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		reportRemark.setProgramCode(1);

		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(3));
		parameters.put("emergencyOrderPoint", PropertyLoader.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		List<ReportRequisition> labIssuesList = new LabRequisitionDao().getLabIssues(dateFrm, dateTo, normalOrder, remarks);
		if (!labIssuesList.isEmpty())
		{
			JasperReport masterReport, equipReport;
			JasperPrint masterPrint;
			JasperDesign masterDesign, equipDesign;
			JRBeanCollectionDataSource masterDataSrc = new JRBeanCollectionDataSource(labIssuesList);
			//String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" LAB USAGE REPORT.pdf";
			String reportType = "LAB USAGE Report & Requisition for the period "+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate);
			try {
				//equipDesign = JRXmlLoader.load("./Reports/EQUIPMENT_INFO_REPORT.jrxml");
				//equipReport = JasperCompileManager.compileReport(equipDesign);

				//masterDesign = JRXmlLoader.load("./Reports/LAB_MASTER_REPORT.jrxml");
				String labSrcRpt = "./Reports/LAB_MASTER_REPORT.jasper";
				/*masterReport = JasperCompileManager.compileReport(masterDesign);
				masterPrint  = JasperFillManager.fillReport(masterReport, parameters, masterDataSrc);*/
				masterPrint  = JasperFillManager.fillReport(labSrcRpt, parameters, masterDataSrc);

				//JasperExportManager.exportReportToPdfFile(masterPrint,pdfName);
				JRViewer jv = new JRViewer(masterPrint);
				new JRViewJd(null, true, 4, true, jv, reportType, masterPrint, dateFrm, dateTo, normalOrder).setVisible(true);
			}
			catch (JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING LAB USAGE FOR THE PERIOD<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("---"+e.getMessage());
			}
			catch(Exception ex)
			{
				System.out.println(">>> "+ex.toString());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No LAB usage data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateEmlipReport(final String frmDate, final String toDate, final String normalOrder, final String remarks, final JDialog jDialog)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		ReportingPeriodRemark reportRemark = new ReportingPeriodRemark();
		reportRemark.setRemarks(remarks);
		reportRemark.setReportPeriodFrm(dateFrm);
		reportRemark.setReportPeriodTo(dateTo);
		reportRemark.setProgramCode(1);

		parameters.put("fromDate", frmDate);
		parameters.put("toDate", toDate);
		parameters.put("emergencyOrder", normalOrder);
		parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(4));
		parameters.put("emergencyOrderPoint", PropertyLoader.getEmergencyOrderPointProp() );
		parameters.put("remarks", remarks);

		List<ReportRequisition> emlipIssuesList = new EmlipRequisitionDao().getEmlipIssues(dateFrm, dateTo, normalOrder, remarks);

		JasperDesign emlipDesign = null;
		JasperReport emlipRpt = null;
		JasperPrint emlipPrnt = null;

		JRBeanCollectionDataSource emlipDataSrc = new JRBeanCollectionDataSource(emlipIssuesList);
		if (!emlipIssuesList.isEmpty())
		{
			//String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" R & R EMLIP.pdf";
			String reportType = "ESSENTIAL MEDICINES Report & Requisition for the period "+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate);
			try
			{
				//emlipDesign = JRXmlLoader.load("./Reports/EMLIP_REPORT.jrxml");
				//emlipRpt = JasperCompileManager.compileReport(emlipDesign);
				String srcRpt = "./Reports/EMLIP_REPORT.jasper";
				//emlipPrnt  = JasperFillManager.fillReport(emlipRpt, parameters, emlipDataSrc);
				emlipPrnt  = JasperFillManager.fillReport(srcRpt, parameters, emlipDataSrc);
				//JasperExportManager.exportReportToPdfFile(emlipPrnt,pdfName);
				JRViewer jv = new JRViewer(emlipPrnt);
				new JRViewJd(null, true, 1, true, jv, reportType, emlipPrnt, dateFrm, dateTo, normalOrder).setVisible(true);	
			}
			catch(JRException e)
			{
				JOptionPane.showMessageDialog(null,
						"<html><h2>ERROR GENERATING EMLIP Report & Requisition for the period<br> <font color=red> "
								+ frmDate
								+ " to "
								+ toDate
								+ " </font><br></h2></html>",	"ERROR",JOptionPane.ERROR_MESSAGE);
				System.out.println("EMLIP ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No EMLIP Data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generateLabEquipInfoReport(String frmDate, String toDate, JDialog jDialog)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		LabTestDao labTestDao = new LabTestDao();
		List<LabTest> listLabTestInfo = labTestDao.getLabEquipmentInfoReport(dateTo);//.getLabEquipmentInfoReport(/*dateFrm, dateTo*/);

		if (!listLabTestInfo.isEmpty()){
			JasperDesign labTestDesign = null;
			JasperReport labTestRpt = null;
			JasperPrint labTestPrnt = null;

			JRBeanCollectionDataSource labTestDataSrc = new JRBeanCollectionDataSource(listLabTestInfo);
			//String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" EQUIPMENT_INFO_REPORT.pdf";
			String reportType = "EQUIPMENT INFORMATION REPORT for the period "+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate);
			try
			{
				/*labTestDesign = JRXmlLoader.load("./Reports/LAB_INFO_REPORT.jrxml");
				labTestRpt = JasperCompileManager.compileReport(labTestDesign);
				labTestPrnt  = JasperFillManager.fillReport(labTestRpt, parameters, labTestDataSrc);*/
				String labEquipSrcRpt = "./Reports/LAB_INFO_REPORT.jasper";
				labTestPrnt  = JasperFillManager.fillReport(labEquipSrcRpt, parameters, labTestDataSrc);
				//JasperViewer.viewReport (labTestPrnt);
				//JasperExportManager.exportReportToPdfFile(labTestPrnt,pdfName);
				JRViewer jv = new JRViewer(labTestPrnt);
				new JRViewJd(null, true, 0, false, jv, reportType, labTestPrnt, dateFrm, dateTo, "").setVisible(true);	
			}
			catch(JRException e)
			{
				System.out.println("EQUIPMENT_INFO_REPORT ERROR:"+e.getMessage());
			}
		}
		else
			JOptionPane.showMessageDialog(jDialog, "<html>No Lab Equipment Data available to generate for the periold <font color=red>"+frmDate.toString()+"</font> to <font color=red>"+toDate.toString()+"</font></html>");
	}
	public void generatePhysicalCountReport(List<StorePhysicalCount> listPhyCount, String frmDate, String toDate)
	{
		Date dateFrm = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(frmDate));
		Date dateTo = calenderUtil.getSqlDate(calenderUtil.changeDateFormat(toDate));

		JasperDesign physCountRptDesign = null;
		JasperReport physCountRpt = null;
		JasperPrint physCountPrnt = null;

		JRBeanCollectionDataSource physCountDataSrc = new JRBeanCollectionDataSource(listPhyCount);
		//String pdfName = storageManager.getReportFolder()+"/"+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate)+" PHYSICAL_COUNT.pdf";
		String reportType = "PHYSICAL COUNT REPORT for the period "+calenderUtil.changeDateFormat(frmDate)+" to "+calenderUtil.changeDateFormat(toDate);
		Map<String, Object> map = new HashMap<>();
		map.put("frmDate", frmDate);
		map.put("toDate", toDate);
		try
		{
			physCountRptDesign = JRXmlLoader.load("./Reports/physical_count.jrxml");
			physCountRpt = JasperCompileManager.compileReport(physCountRptDesign);
			physCountPrnt  = JasperFillManager.fillReport(physCountRpt, map, physCountDataSrc);
			//JasperExportManager.exportReportToPdfFile(labTestPrnt,pdfName);
			JRViewer jv = new JRViewer(physCountPrnt);
			new JRViewJd(null, true, 0, false, jv, reportType, physCountPrnt, dateFrm, dateTo, "").setVisible(true);
		}
		catch(JRException e)
		{
			System.out.println("PHYSICAL COUNT REPORT ERROR:"+e.getMessage());
		}
	}
	public void generateSubmitedRandRs(List<ReportRequisition> randrList, String fromDate, String toDate, String programArea, String normalOrder, JDialog jDialog)
	{
		Date dateFrm = calenderUtil.getSqlDate(fromDate);
		Date dateTo = calenderUtil.getSqlDate(toDate);

		JasperDesign submitedRandrDesign = null;
		JasperReport submitedRandrRpt = null;
		JasperPrint submitedRandrPrnt = null;
		JRBeanCollectionDataSource submitedRandrDataSrc = new JRBeanCollectionDataSource(randrList);
		//Map<String, Object> map = new HashMap<>();
		parameters.put("fromDate", fromDate);
		parameters.put("toDate", toDate);		
		parameters.put("emergencyOrderPoint", PropertyLoader.getEmergencyOrderPointProp());
		parameters.put("emergencyOrder", normalOrder);
		String reportType = null;

		try
		{
			if (programArea.equals("ARV"))
			{
				parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(1));
				reportType = "ARV Report & Requisition for the period "+dateFrm+" to "+dateTo;
				String arv = "./Reports/ARV_MASTER_REPORT.jasper";
				//submitedRandrDesign = JRXmlLoader.load("./Reports/ARV_MASTER_REPORT.jrxml");
				//submitedRandrRpt = JasperCompileManager.compileReport(submitedRandrDesign);
				//submitedRandrPrnt  = JasperFillManager.fillReport(submitedRandrRpt, parameters, submitedRandrDataSrc);
				submitedRandrPrnt  = JasperFillManager.fillReport(arv, parameters, submitedRandrDataSrc);
			}
			else if (programArea.equals("HIV"))
			{
				parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(2));
				reportType = "HIV Report & Requisition for the period "+dateFrm+" to "+dateTo;
				String hiv = "./Reports/HIV_MASTER_REPORT.jasper";
				/*submitedRandrDesign = JRXmlLoader.load("./Reports/HIV_MASTER_REPORT.jrxml");
				submitedRandrRpt = JasperCompileManager.compileReport(submitedRandrDesign);
				submitedRandrPrnt  = JasperFillManager.fillReport(submitedRandrRpt, parameters, submitedRandrDataSrc);*/
				submitedRandrPrnt  = JasperFillManager.fillReport(hiv, parameters, submitedRandrDataSrc);
			}
			else if (programArea.equals("LAB"))
			{
				parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(3));
				reportType = "LAB USAGE Report & Requisition for the period "+dateFrm+" to "+dateTo;
				String lab = "./Reports/LAB_MASTER_REPORT.jasper";
				submitedRandrPrnt  = JasperFillManager.fillReport(lab, parameters, submitedRandrDataSrc);
			}
			else
			{
				parameters.put("maxStockLevel",  PropertyLoader.getStockLevelProp(4));
				reportType = "ESSENTIAL MEDICINES Report & Requisition for the period "+dateFrm+" to "+dateTo;
				String em = "./Reports/EMLIP_REPORT.jasper";
				submitedRandrPrnt  = JasperFillManager.fillReport(em, parameters, submitedRandrDataSrc);
			}
			JRViewer jv = new JRViewer(submitedRandrPrnt);
			new JRViewJd(null, true, 1, false, jv, reportType, submitedRandrPrnt, dateFrm, dateTo, "").setVisible(true);	
		}
		catch(JRException e)
		{
			System.out.println("ERROR GENERATING REPORT:"+e.getMessage());
			JOptionPane.showMessageDialog(jDialog, "ERROR GENERATING MESSAGE.");
		}
	}
	public void generateHivDailyActivityRegister(Date date, int siteId){
		HivTestResultDao hivTestResultsDao = new HivTestResultDao();
		Map<String, String> testProducts = hivTestResultsDao.getTestProducts();
		List<HivTestResult> hivTestResults = hivTestResultsDao.getDailyActivityRegisterTransaction(date, siteId);
		Map<String, Object> quantityIssued = hivTestResultsDao.getQuantityIssued(date, siteId);
		if (!hivTestResults.isEmpty()){
			//Map<String, Object> beginBalanceMap = hivTestResultsDao.getPhysicalCount(calU.getSqlDate(calU.getPreviousDate()), siteId);
			Map<String, Object> beginBalanceMap = hivTestResultsDao.getBeginBalance(date, siteId);
			Map<String, Object> physicalCountMap = hivTestResultsDao.getPhysicalCount(date, siteId);
			Map<String, Object> map = hivTestResultsDao.getAdjustments(date, siteId);
			int screenBalance = beginBalanceMap.get(beginBalanceMap.get("Screening")) == null? 0 : (Integer)beginBalanceMap.get(beginBalanceMap.get("Screening"));
			int confirmatoryBalance = beginBalanceMap.get(beginBalanceMap.get("Confirmatory")) == null? 0 : (Integer)beginBalanceMap.get(beginBalanceMap.get("Confirmatory"));
			Map<String, Object> parameterMap = new HashMap<>();
			parameterMap.put("screenDisplayName", testProducts.get("Screening"));
			parameterMap.put("confirmatoryDisplayName", testProducts.get("Confirmatory"));
			parameterMap.put("screenBalance", screenBalance);
			parameterMap.put("confirmatoryBalance", confirmatoryBalance);
			int screeningPhysicalCount =  physicalCountMap.get(physicalCountMap.get("Screening")) == null? 0 : (Integer)physicalCountMap.get(physicalCountMap.get("Screening"));
			int confirmatoryPhysicalCount = physicalCountMap.get(physicalCountMap.get("Confirmatory")) == null? 0 : (Integer)physicalCountMap.get(physicalCountMap.get("Confirmatory"));
			Long screeningAdjustment = (map.get("Screening") == null)? 0: (Long)(map.get("Screening"));
			Long confirmatoryAdjustment =  (map.get("Confirmatory")== null)? 0 : (Long)(map.get("Confirmatory"));
			parameterMap.put("screeningAdjustment", screeningAdjustment);
			parameterMap.put("confirmatoryAdjustment",confirmatoryAdjustment);
			parameterMap.put("screeningPhysicalCount", screeningPhysicalCount);
			parameterMap.put("confirmatoryPhysicalCount", confirmatoryPhysicalCount);
			parameterMap.put("siteName", AppJFrame.getDispensingPointName("HIV"));
			BigDecimal screeningIssues = (BigDecimal)quantityIssued.get("Screening");
			BigDecimal confirmatoryIssues = (BigDecimal)quantityIssued.get("Confirmatory");
			parameterMap.put("screeningQtyIssued", screeningIssues);
			parameterMap.put("confirmatoryQtyIssued", confirmatoryIssues);

			try{
				parameterMap.put("district", System.getProperty("district"));
				parameterMap
				.put("facilityname", System.getProperty("facilityname"));
				parameterMap.put("facilityid", System.getProperty("facilityId"));

				JasperPrint print = JasperFillManager.fillReport("./Reports/hiv_tests_dar.jasper", parameterMap,new JRBeanCollectionDataSource(hivTestResults));
				//JasperPrint print = JasperFillManager.fillReport(JasperCompileManager.compileReport(JRXmlLoader.load("./Reports/hiv_tests_dar.jrxml")), parameterMap,new JRBeanCollectionDataSource(hivTestResults));
				//new JasperViewer(print);
				//JasperViewer.viewReport(print, false);
				new DarJasperViewerJd(null, true, new JRViewer(print)).setVisible(true);
			}
			catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
						.toString());
			}
		}
		else
			JOptionPane.showConfirmDialog(null,
					"<html>No Tests have been carried out today <font color=red>"+date+" </font></html>",
					"Carryout Physical count ", JOptionPane.WARNING_MESSAGE);
	}
	
	public void generateHivDailyActivityRegisterRange(Date date1, Date date2, int siteId){
		HivTestResultDao hivTestResultsDao = new HivTestResultDao();
		Map<String, String> testProducts = hivTestResultsDao.getTestProducts();
		List<HivTestResult> hivTestResults = hivTestResultsDao.getDailyActivityRegisterTransactionRange(date1,date2, siteId);
		Map<String, Object> quantityIssued = hivTestResultsDao.getQuantityIssued(date1, siteId);
		if (!hivTestResults.isEmpty()){
			//Map<String, Object> beginBalanceMap = hivTestResultsDao.getPhysicalCount(calU.getSqlDate(calU.getPreviousDate()), siteId);
			Map<String, Object> beginBalanceMap = hivTestResultsDao.getBeginBalance(date1, siteId);
			Map<String, Object> physicalCountMap = hivTestResultsDao.getPhysicalCount(date1, siteId);
			Map<String, Object> map = hivTestResultsDao.getAdjustments(date1, siteId);
			int screenBalance = beginBalanceMap.get(beginBalanceMap.get("Screening")) == null? 0 : (Integer)beginBalanceMap.get(beginBalanceMap.get("Screening"));
			int confirmatoryBalance = beginBalanceMap.get(beginBalanceMap.get("Confirmatory")) == null? 0 : (Integer)beginBalanceMap.get(beginBalanceMap.get("Confirmatory"));
			Map<String, Object> parameterMap = new HashMap<>();
			parameterMap.put("screenDisplayName", testProducts.get("Screening"));
			parameterMap.put("confirmatoryDisplayName", testProducts.get("Confirmatory"));
			parameterMap.put("screenBalance", screenBalance);
			parameterMap.put("confirmatoryBalance", confirmatoryBalance);
			int screeningPhysicalCount =  physicalCountMap.get(physicalCountMap.get("Screening")) == null? 0 : (Integer)physicalCountMap.get(physicalCountMap.get("Screening"));
			int confirmatoryPhysicalCount = physicalCountMap.get(physicalCountMap.get("Confirmatory")) == null? 0 : (Integer)physicalCountMap.get(physicalCountMap.get("Confirmatory"));
			Long screeningAdjustment = (map.get("Screening") == null)? 0: (Long)(map.get("Screening"));
			Long confirmatoryAdjustment =  (map.get("Confirmatory")== null)? 0 : (Long)(map.get("Confirmatory"));
			parameterMap.put("screeningAdjustment", screeningAdjustment);
			parameterMap.put("confirmatoryAdjustment",confirmatoryAdjustment);
			parameterMap.put("screeningPhysicalCount", screeningPhysicalCount);
			parameterMap.put("confirmatoryPhysicalCount", confirmatoryPhysicalCount);
			parameterMap.put("siteName", AppJFrame.getDispensingPointName("HIV"));
			BigDecimal screeningIssues = (BigDecimal)quantityIssued.get("Screening");
			BigDecimal confirmatoryIssues = (BigDecimal)quantityIssued.get("Confirmatory");
			parameterMap.put("screeningQtyIssued", screeningIssues);
			parameterMap.put("confirmatoryQtyIssued", confirmatoryIssues);

			try{
				parameterMap.put("district", System.getProperty("district"));
				parameterMap
				.put("facilityname", System.getProperty("facilityname"));
				parameterMap.put("facilityid", System.getProperty("facilityId"));

				JasperPrint print = JasperFillManager.fillReport("./Reports/hiv_tests_dar_date_range.jasper", parameterMap,new JRBeanCollectionDataSource(hivTestResults));
				//JasperPrint print = JasperFillManager.fillReport(JasperCompileManager.compileReport(JRXmlLoader.load("./Reports/hiv_tests_dar.jrxml")), parameterMap,new JRBeanCollectionDataSource(hivTestResults));
				//new JasperViewer(print);
				//JasperViewer.viewReport(print, false);
				new DarJasperViewerJd(null, true, new JRViewer(print)).setVisible(true);
			}
			catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
						.toString());
			}
		}
		else
			JOptionPane.showConfirmDialog(null,
					"<html>No Tests have been carried out during this period <font color=red>"+date1+" </font> to <font color=red>"+date2+" </font></html>",
					"Carryout Physical count ", JOptionPane.WARNING_MESSAGE);
	}
}
