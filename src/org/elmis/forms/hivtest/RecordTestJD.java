/*
 * RecordTestJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest;

import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import org.elmis.facility.domain.dao.HivTestProductDao;
import org.elmis.facility.domain.dao.HivTestResultDao;
import org.elmis.facility.domain.model.HivTest;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.tools.MessageDialog;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.SdpStyleSheet;

import com.toedter.calendar.JDateChooser;

/**
 *
 * @author  Joe Banda
 * 
 */
public class RecordTestJD extends javax.swing.JDialog {

	private int selectTabIndex = 0;
	private boolean resetTestingPanel = true;
	private String otherPurpose = "";
	private String dispensaryPointName = AppJFrame.getDispensingPointName("HIV");
	private JLabel screeningLbl;
	private boolean isCounted = false;
	private boolean isIssued = true;
	private String testDateformat = "dd-MM-yyyy";
	private Date todayDate;
	private JDateChooser dateChooser;
	private String testPurpose;
	private Integer screeningProductqty;
	private Integer  confirmatoryProductqty;
	private String screeningProduct;
	private String screeningName;
	private String confirmatoryName;
	private String confirmatoryProduct;
	private int testId = 0;
	private boolean physicalCountCarriedOut = true;
	public   Map<String, Object>  products;
	@SuppressWarnings("rawtypes")
	public  TreeMap tm = new TreeMap();
	private Map<String, String> hivTestProducts = new HivTestProductDao().getHivTestProducts();
	//Map<String, Object>
	private int siteId;
	private String screeningResult = "";
	private HivTest hivTest;
	private String typeOfTest;
	private Object[][] purposeData = new Object[][] { { "VCT" }, { "PMTCT" },{ "Clinical Diagnosis" }, { "Quality Control" },{ "Other" } };
	private String[] emptyHeader = { "" };
	private DefaultTableModel purposeTableModel = new DefaultTableModel(purposeData,emptyHeader) {
		@Override
		public boolean isCellEditable(int row, int column) {
				return false;
		}

	};
	private Object[][] confirmatoryTestData = new Object[][] { { "Confirmatory" }};
	private Object[][] screeningtypeTestData = new Object[][] { { "Screening" }/*, { "Confirmatory" }*/};
	
	private DefaultTableModel typeTestTableModel = new DefaultTableModel(screeningtypeTestData,emptyHeader) {
		@Override
		public boolean isCellEditable(int row, int column) {
				return false;
		}

	};
	private Object[][] resultData = new Object[][] { { "Reactive (R)" }, { "Non-Reactive (NR)" },{ "Invalid (IN)" } };
	private DefaultTableModel resultTableModel = new DefaultTableModel(resultData,emptyHeader) {
		@Override
		public boolean isCellEditable(int row, int column) {
				return false;
		}

	};
	/** Creates new form RecordTestJD */
	public RecordTestJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		siteId = AppJFrame.getDispensingId("HIV");
		Boolean countCarriedOut = false;
		HivTestResultDao hivTestResultDao = new HivTestResultDao();
		//PhysicalCountChecker physicalCountChecker = new PhysicalCountChecker();
		//Map<String, Boolean> physicalCountStatusMap = physicalCountChecker.checkPhysicalCountBeforeDispensing(siteId); 
		SdpStyleSheet.configJDialogBackground(this);
		try{
		products = hivTestResultDao.getTestingProducts(siteId);
	 boolean val = products.isEmpty();
	if(val){
		isIssued = false;
		JOptionPane.showConfirmDialog(RecordTestJD.this,
				"<html>Please issue products to  <font color=red>"+dispensaryPointName+" </font> before you continue to carryout tests.</html>",
				"Issues Error to "+dispensaryPointName, JOptionPane.WARNING_MESSAGE);
	}else{
		if (!products.get("Screening").equals("Screening") ||  !products.get("Confirmatory").equals("Confirmatory")){
			
			/*isIssued = false;
			JOptionPane.showConfirmDialog(RecordTestJD.this,
					"<html>Please issue products to  <font color=red>"+dispensaryPointName+" </font> before you continue to carryout tests.</html>",
					"Issues Error to "+dispensaryPointName, JOptionPane.WARNING_MESSAGE);*/
		}
		else{
			String productUsedInScreening = hivTestProducts.get("Screening");
			String productUsedInConfirmatory = hivTestProducts.get("Confirmatory");
			String confirmatoryDisplayName = hivTestProducts.get("ConfirmatoryTest");
			String screeningDisplayName = hivTestProducts.get("ScreeningTest");
			//System.out.println("XXXX "+s);
			screeningProduct = productUsedInScreening;//products.get(hivTestProducts.get("Screening")).toString();
			confirmatoryProduct = productUsedInConfirmatory;//products.get("Confirmatory").toString();
			screeningName = products.get(screeningDisplayName).toString();
			confirmatoryName = products.get(confirmatoryDisplayName).toString();
			
			screeningProductqty = Integer.parseInt(products.get("Screeningqty").toString());
			confirmatoryProductqty = Integer.parseInt(products.get("Confirmatoryqty").toString());
			
			/*
			 * instatiate running total 
			 */
			products.put("screeningtotal",screeningProductqty);
			products.put("confirmatorytotal",confirmatoryProductqty);
			
			/*System.out.println("Beginingbalance = " +screeningProductqty);
			System.out.println("Beginingbalance =  " +confirmatoryProductqty);
			
			System.out.println("Count carried out : "+countCarriedOut);*/
			countCarriedOut = hivTestResultDao.checkPhysicalCount(siteId);
			
			
			/*if (physicalCountStatusMap.get(screeningProduct) != null){
				if (physicalCountStatusMap.get(screeningProduct) == false){
					physicalCountCarriedOut = false;
					JOptionPane.showConfirmDialog(RecordTestJD.this,
							"<html>Physical count of  <font color=red>"+screeningName+" </font>has not been carried out.</html>",
							"Physical Count "+screeningProduct, JOptionPane.WARNING_MESSAGE);
				}
			}
			if (physicalCountStatusMap.get(confirmatoryProduct) != null){
				if (physicalCountStatusMap.get(confirmatoryProduct) == false){
					physicalCountCarriedOut = false;
					JOptionPane.showConfirmDialog(RecordTestJD.this,
							"<html>Physical count of  <font color=red>"+confirmatoryName+" </font>has not been carried out.</html>",
							"Physical Count "+confirmatoryProduct, JOptionPane.WARNING_MESSAGE);
				}
			}*/
		}
	}

		}catch(NullPointerException n){
			n.getMessage();
		}
		/*if (calU.getSqlDate(calU.getTodayDate()).compareTo(new HivTestResultDao().getLastTractionDate(siteId)) > 0){
			Map<String, Object> map = new HivTestResultDao().getPhysicalCountStatus(siteId);
			if ((boolean)map.get(screeningProduct) == false){
				physicalCountCarriedOut = false;
				JOptionPane.showConfirmDialog(RecordTestJD.this,
						"<html>Physical count of  <font color=red>"+screeningProduct+" </font>has not been carried out.</html>",
						"Stock out "+screeningProduct, JOptionPane.WARNING_MESSAGE);
			}
			if ((boolean)map.get(confirmatoryProduct) == false){
				physicalCountCarriedOut = false;
				JOptionPane.showConfirmDialog(RecordTestJD.this,
						"<html>Physical count of  <font color=red>"+confirmatoryProduct+" </font>has not been carried out.</html>",
						"Stock out "+screeningProduct, JOptionPane.WARNING_MESSAGE);
			}
				
		}*/
		//this.siteId = siteId;

		hivTest = new HivTest();

		//UIManager UI=new UIManager();
		// UI.put("OptionPane.background",new ColorUIResource(255,0,0));
		// UI.put("Panel.background",new ColorUIResource(255,0,0));

		initComponents();
		
		if (isIssued){
			 screeningLbl.setText(screeningName+"("+(screeningProductqty)+")");
			 confirmatoryLbl.setText(confirmatoryName+"("+(confirmatoryProductqty)+")");
			
			//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
			//confirmatoryLbl.setText(confirmatoryName+"("+(int)products.get(confirmatoryProduct)+")");
		}
		//rend.setHorizontalAlignment(JLabel.LEFT);

		//jtableHeader.setDefaultRenderer(rend);

		//this.counsellingJtable.setOpaque(true);

		/*	this.counsellingJtable.getTableHeader().setDefaultRenderer(new DefaultTableRenderer(){
				  {
				    // you need to set it to opaque
				    setOpaque(true);
				  }

				@Override
				public Component getTableCellRendererComponent(final JTable table,
				  final Object value, final boolean isSelected, final boolean hasFocus,
				  final int row, final int column) {
				    // set the background
				    setBackground(yourDesiredColor);
				  }
				});
				
		 */

		if (System.getProperty("dp_name") != null) {

			this.setTitle(" " + AppJFrame.getDispensingPointName("HIV"));

		}

		///jTabbedPane1.remove(6);
		//jTabbedPane1.remove(5);
		//jTabbedPane1.remove(4);
		//jTabbedPane1.remove(3);
		jTabbedPane1.remove(2);
		jTabbedPane1.remove(1);

		//jTable2.setRowSelectionAllowed(true);

		//jTable2.setColumnSelectionAllowed(false);
		//jTable2.setRowSelectionInterval(0,2);
		//jTable2.repaint();

		this.setSize(632, 588);
		//setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//jPanel1.requestFocus();
		//
		//jTable1.repaint();

		//jTable2.getSelectionModel().getLeadSelectionIndex();
		//jTable2.requestFocus();
		//jTable2.setRowSelectionAllowed(true);
		//jTable2.setColumnSelectionAllowed(false);

		//jTable2.repaint();

		//jTable1.setSurrendersFocusOnKeystroke(true);
	}

	/** This method is called from within the constructor to
	 * initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is
	 * always regenerated by the Form Editor.
	 */
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {
		jLabel3 = new javax.swing.JLabel();
		jLabel3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				dispose();
			}
		});
		backJL = new javax.swing.JLabel();

		//setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle(" "+AppJFrame.getDispensingPointName("HIV"));

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		jLabel3.setText("Close");

		backJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		backJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Back.png"))); // NOI18N
		backJL.setText("Back");
		backJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				backJLMouseClicked(evt);
			}
		});
		
		JPanel panel = new JPanel();
		
		panel_1 = new JPanel();
		SdpStyleSheet.configJPanelBackground(panel);
		SdpStyleSheet.configJPanelBackground(panel_1);
		
		panel_2 = new JPanel();
		SdpStyleSheet.configJPanelBackground(panel_2);
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(backJL, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED, 347, Short.MAX_VALUE)
							.addComponent(jLabel3, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 492, GroupLayout.PREFERRED_SIZE)))
					.addGap(858))
				.addGroup(Alignment.LEADING, layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, layout.createSequentialGroup()
							.addGap(10)
							.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 492, GroupLayout.PREFERRED_SIZE))
						.addComponent(panel_1, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 502, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
					.addGap(5)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 365, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addGap(5)
							.addComponent(backJL, GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE))
						.addComponent(jLabel3, GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)))
		);
		panel_2.setLayout(null);
		
		JLabel lblTestDate = new JLabel("Test Date:");
		lblTestDate.setBounds(10, 11, 175, 14);
		panel_2.add(lblTestDate);
		SdpStyleSheet.configOtherJLabel(lblTestDate);
		dateChooser = new JDateChooser();
		dateChooser.setBounds(117, 11, 175, 32);
		panel_2.add(dateChooser);
		dateChooser.setDateFormatString(testDateformat);
		todayDate = new Date();
		//dateChooser.setDate(todayDate);
				jTabbedPane1 = new javax.swing.JTabbedPane();
				jTabbedPane1.setBackground(new java.awt.Color(102, 102, 102));
				jTabbedPane1.setBounds(10, 0, 499, 406);
				
				purposeJP = new javax.swing.JPanel();
				jScrollPane2 = new javax.swing.JScrollPane();
				purposeJTable = new javax.swing.JTable();
				testTypeJP = new javax.swing.JPanel();
				jScrollPane1 = new javax.swing.JScrollPane();
				testTypeJtable = new javax.swing.JTable();
				resultJP = new javax.swing.JPanel();
				jScrollPane3 = new javax.swing.JScrollPane();
				resultJtable = new javax.swing.JTable();
				
						jTabbedPane1.setBorder(new javax.swing.border.LineBorder(
								new java.awt.Color(255, 255, 255), 4, true));
						jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
							public void mouseClicked(java.awt.event.MouseEvent evt) {
								jTabbedPane1MouseClicked(evt);
							}
						});
						
								purposeJTable.setFont(new java.awt.Font("Tahoma", 0, 36));
								purposeJTable.setModel(purposeTableModel);
								purposeJTable.setCellSelectionEnabled(true);
								purposeJTable.setRowHeight(60);
								purposeJTable.addMouseListener(new java.awt.event.MouseAdapter() {
									public void mouseClicked(java.awt.event.MouseEvent evt) {
										if (dateChooser.getDate() != null){
											purposeJTableMouseClicked(evt);
										}
										else
											JOptionPane.showMessageDialog(null, "Please select a test date.", "Missing Date Error!",
													JOptionPane.INFORMATION_MESSAGE);
									}
								});
								purposeJTable.addKeyListener(new java.awt.event.KeyAdapter() {
									public void keyTyped(java.awt.event.KeyEvent evt) {
										purposeJTableKeyTyped(evt);
									}
								});
								jScrollPane2.setViewportView(purposeJTable);
								
										javax.swing.GroupLayout purposeJPLayout = new javax.swing.GroupLayout(
												purposeJP);
										purposeJPLayout.setHorizontalGroup(
											purposeJPLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 486, Short.MAX_VALUE)
										);
										purposeJPLayout.setVerticalGroup(
											purposeJPLayout.createParallelGroup(Alignment.LEADING)
												.addGroup(purposeJPLayout.createSequentialGroup()
													.addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 317, GroupLayout.PREFERRED_SIZE)
													.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
										);
										purposeJP.setLayout(purposeJPLayout);
										
												jTabbedPane1.addTab("Purpose", new javax.swing.ImageIcon(getClass()
														.getResource("/elmis_images/Purpose.png")), purposeJP); // NOI18N
												
														testTypeJtable.setFont(new java.awt.Font("Tahoma", 0, 36));
														testTypeJtable.setModel(typeTestTableModel);
														testTypeJtable.setEditingColumn(0);
														testTypeJtable.setEditingRow(0);
														testTypeJtable.setRowHeight(60);
														testTypeJtable.addMouseListener(new java.awt.event.MouseAdapter() {
															public void mouseClicked(java.awt.event.MouseEvent evt) {
																if (isIssued){
																	if (physicalCountCarriedOut){
																		//int balanceScreening = (int)products.get(screeningProduct);
																		//int balanceConfirmatory = (int) products.get(confirmatoryProduct);
																		
																		Integer balanceScreening = Integer.parseInt(products.get("Screening-HTK0002").toString());
																		Integer balanceConfirmatory = Integer.parseInt( products.get("Confirmatory-HTK0001").toString());
																		if (balanceScreening > 0 && balanceConfirmatory > 0){
																			testTypeJtableMouseClicked(evt);
																		}
																		else
																			JOptionPane.showConfirmDialog(RecordTestJD.this,
																					"<html>Your products for testing are stocked out.</html>",
																					"Stock out ", JOptionPane.WARNING_MESSAGE);
																	}
																	else{
																		JOptionPane.showConfirmDialog(RecordTestJD.this,
																				"<html>Please carry out physical count for your products for the last transaction day </html>",
																				"Carryout Physical count ", JOptionPane.WARNING_MESSAGE);
																		jTabbedPane1.remove(1);
																	}
																}
																else
																	JOptionPane.showConfirmDialog(RecordTestJD.this,
																			"<html>Please issue products to  <font color=red>"+dispensaryPointName+" </font> before you continue to carryout tests.</html>",
																			"Issues Error to "+dispensaryPointName, JOptionPane.WARNING_MESSAGE);
															}
														});
														jScrollPane1.setViewportView(testTypeJtable);
														
																javax.swing.GroupLayout testTypeJPLayout = new javax.swing.GroupLayout(
																		testTypeJP);
																testTypeJP.setLayout(testTypeJPLayout);
																testTypeJPLayout.setHorizontalGroup(testTypeJPLayout
																		.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
																		.addComponent(jScrollPane1,
																				javax.swing.GroupLayout.DEFAULT_SIZE, 486,
																				Short.MAX_VALUE));
																testTypeJPLayout.setVerticalGroup(testTypeJPLayout.createParallelGroup(
																		javax.swing.GroupLayout.Alignment.LEADING).addComponent(
																		jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 367,
																		Short.MAX_VALUE));
																
																		jTabbedPane1.addTab("Test Type", new javax.swing.ImageIcon(getClass()
																				.getResource("/elmis_images/test type.png")), testTypeJP); // NOI18N
																		
																				resultJP.setFont(new java.awt.Font("Tahoma", 0, 14));
																				
																						resultJtable.setFont(new java.awt.Font("Tahoma", 0, 36));
																						resultJtable.setModel(resultTableModel);
																						resultJtable.setRowHeight(60);
																						resultJtable.addMouseListener(new java.awt.event.MouseAdapter() {
																							public void mouseClicked(java.awt.event.MouseEvent evt) {

																								if (dateChooser.getDate().compareTo(todayDate) <= 0){
																									resultJtableMouseClicked(evt);
																								}
																								else{
																									new MessageDialog().showDialog(RecordTestJD.this,
																											"Tests cannot be carried out on dates in the future", "Test Date Error", "Close");
																								}
																							}
																						});
																						jScrollPane3.setViewportView(resultJtable);
																						
																								javax.swing.GroupLayout resultJPLayout = new javax.swing.GroupLayout(
																										resultJP);
																								resultJP.setLayout(resultJPLayout);
																								resultJPLayout.setHorizontalGroup(resultJPLayout.createParallelGroup(
																										javax.swing.GroupLayout.Alignment.LEADING).addComponent(
																										jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 486,
																										Short.MAX_VALUE));
																								resultJPLayout.setVerticalGroup(resultJPLayout.createParallelGroup(
																										javax.swing.GroupLayout.Alignment.LEADING).addComponent(
																										jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 367,
																										Short.MAX_VALUE));
																								
																										jTabbedPane1.addTab("Result", new javax.swing.ImageIcon(getClass()
																												.getResource("/elmis_images/Results icon.png")), resultJP);
		panel.setLayout(null);
		
		screeningLbl = new JLabel("");
		SdpStyleSheet.configOtherJLabel(screeningLbl);
		screeningLbl.setBounds(10, 11, 218, 32);
		panel.add(screeningLbl);
		
		confirmatoryLbl = new JLabel("");
		SdpStyleSheet.configOtherJLabel(confirmatoryLbl);
		confirmatoryLbl.setBounds(10, 47, 218, 32);
		panel.add(confirmatoryLbl);
		
		
		panel_1.setLayout(null);
		panel_1.add(jTabbedPane1);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void backJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

	/*	if (jTabbedPane1.getTabCount() > 1) {

			jTabbedPane1.remove(jTabbedPane1.getTabCount() - 1);

		}*/
		
		//remove tabs 
				int selectedtab = jTabbedPane1.getSelectedIndex() ;
				
				if( selectedtab == 1){
					
					backJL.setVisible(false);
				}
				if(selectedtab > 0 ){

				for (int j = jTabbedPane1.getTabCount(); j > selectedtab; j--) {

					jTabbedPane1.remove(j - 1);

				}
				}

	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//remove tabs 
		int selectedtab = jTabbedPane1.getSelectedIndex() + 1;

		for (int j = jTabbedPane1.getTabCount(); j > selectedtab; j--) {

			jTabbedPane1.remove(j - 1);

		}
	}

	/**
	 * @param evt
	 */
	private void resultJtableMouseClicked(java.awt.event.MouseEvent evt) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		
		java.sql.Date dateOfTest = new CalendarUtil().getSqlDate(dateFormat.format(dateChooser.getDate()));

		/***********************************************************/
		//this splits the selected result in the
		//resultJTable to take out the "()" for the selected result 
		//e.g.  Reactive(R) 
		String selectedResult = this.resultJtable.getModel().getValueAt(
				this.resultJtable.getSelectedRow(), 0).toString();
		String[] result = selectedResult.split("\\(");
		String testResult = result[1].toString().replaceAll("\\)", "");//returns ( R , NR ,or I)
		/***********************************************************/

		/*****************************************************/
		if (!testPurpose.equalsIgnoreCase("QC")){
			//if screening test is non-reactive	(option 1)	
			if (this.typeOfTest.equals("Screening")	&& testResult.equalsIgnoreCase("NR")) {

				int ok = new MessageDialog()
				.showDialog(
						this,
						"Screening test is Non-Reactive would you\nlike to Save the test now?",
						"Screening Test", "YES, Save \nScreening Test Result",
						"NO, Cancel\n Screening Test Non-Reactive Result");

				if (ok == 0) {
					
					//products.put("screeningtotal",screeningProductqty);
					//products.put("confirmatorytotal",confirmatoryProductqty);
					//products.get(screeningProduct);
					Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
					products.put("screeningtotal", screeningBalance);
					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					testId = hivTestResultDao.insertScreeningResult(testResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
					hivTestResultDao.insertFinalTestResult(testId, 'N', testPurpose, dateOfTest, siteId);
					new MessageDialog().showDialog(this, "Screening Test Non-Reactive Result has been saved.",
							"Saved", "Close and return ");
					jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					if (isIssued)
					screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
				}
				/****************************************************/
				//if screening test is reactive 
			} else if (this.typeOfTest.equals("Screening")	&& testResult.equalsIgnoreCase("R")) {
				screeningResult = "R";
				//prompt if you should do confirmatory rest

				/**************************************************/

				int ok = new MessageDialog()
				.showDialog(
						this,
						"Screening test is Reactive. \nDo you want to carry out Confirmatory Test to confirm result?",
						"Screening Test - Reactive", "YES, Do \nConfirmatroy Test",
						"NO, Save\nScreening Test Only");

				if (ok == 1) {
					//option 2 screening is reactive 
					//NO confirmatory test
					//int screeningBalanace = (int)products.get(screeningProduct)-1;
					Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
					products.put( "screeningtotal", screeningBalance);
					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					testId = hivTestResultDao.insertScreeningResult(testResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
					hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, dateOfTest, siteId);
					new MessageDialog().showDialog(this, "Screening Test Reactive Result has been saved but final result is Indeterminate.",
							"Saved", "Close and return ");
					jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					if(isIssued)
						//screeningLbl.setText(screeningName+" " +(this.screeningProductqty));
					    screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
				} else if (ok == 0) {
					//yes save confirmatory
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					((DefaultTableModel) this.testTypeJtable.getModel()).addRow(confirmatoryTestData[0]);
					jTabbedPane1.remove(jTabbedPane1.getTabCount() - 1);
					this.typeOfTest = "Confirmatory";
					//this.resultJtable.getModel().setValueAt(2,this.resultJtable.getSelectedRow(), 0);

				}

			}else if (this.typeOfTest.equals("Screening")	&& testResult.equalsIgnoreCase("IN")) {
				screeningResult = "ID";
				Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
				products.put( "screeningtotal", screeningBalance);
				HivTestResultDao hivTestResultDao = new HivTestResultDao();
				testId = hivTestResultDao.insertScreeningResult(testResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
				hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, dateOfTest, siteId);
				JOptionPane.showConfirmDialog(this, "Screening test is Invalid.", "Invalid test", JOptionPane.OK_OPTION);
				
				/*int ok = new MessageDialog()
				.showDialog(
						this,
						"Screening test is Indeterminate. \nDo you want to carry out Confirmatory Test to confirm result?",
						"Screening Test - Reactive", "YES, Do \nConfirmatroy Test",
						"NO, Save\nScreening Test Only");*/
				jTabbedPane1.setSelectedIndex(0);
				jTabbedPane1.remove(2);
				jTabbedPane1.remove(1);
				((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
				if(isIssued)
					//screeningLbl.setText(screeningName+"("+products.get(screeningProduct)+")");
					 screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
			}
			else if (this.typeOfTest.equals("Confirmatory")) {

				//option 3 if confirmatory is reactive(R)  
				if (testResult.equalsIgnoreCase("R")) {
				    //int screeningBalance = (int)products.get(screeningProduct)-1;
					Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
					//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
					
					Integer confirmatoryBalance = Integer.parseInt(products.get("confirmatorytotal").toString())-1;
					products.put("screeningtotal", screeningBalance);
					products.put( "confirmatorytotal", confirmatoryBalance);

					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					testId = hivTestResultDao.insertScreeningResult(screeningResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
					hivTestResultDao.insertConfirmatory(testResult, testPurpose, confirmatoryProduct, testId, siteId, confirmatoryBalance, dateOfTest, otherPurpose);
					hivTestResultDao.insertFinalTestResult(testId, 'P', testPurpose, dateOfTest,siteId);

					new MessageDialog().showDialog(this, "Confirmatory Test Reactive Result has been saved with final result Positive.",
							"Saved", "Close and return ");
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					((DefaultTableModel) this.testTypeJtable.getModel()).addRow(screeningtypeTestData[0]);
					//((DefaultTableModel) this.testTypeJtable.getModel()).addRow(typeTestData[1]);
					jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					if(isIssued){
						//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
						//confirmatoryLbl.setText(confirmatoryName+"("+(int)products.get(confirmatoryProduct)+")");
						 screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
						confirmatoryLbl.setText(confirmatoryName+"("+(confirmatoryBalance)+")");

					}
				}

				else if (testResult.equalsIgnoreCase("NR")) {
					//check if a screening test was done and was reactive 
					if (screeningResult.equalsIgnoreCase("R") ) {
						//if Confirmatory is NR and Screening was reactive
						//prompt if facility can do tiebreaker

						int ok = new MessageDialog()
						.showDialog(
								this,
								"Confirmatory test is Non-Reactive would you\n like to Save the test now?",
								"Confirmatory Test", "YES, Save\n Confirmatory Test", "Cancel\n Final test will be Indeterminate.");

						if (ok == 1) {
							//int screeningBalance = (int)products.get(screeningProduct)-1;
							Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
							products.put("screeningtotal", screeningBalance);
							HivTestResultDao hivTestResultDao = new HivTestResultDao();
							testId = hivTestResultDao.insertScreeningResult(testResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
							hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, dateOfTest, siteId);
							new MessageDialog().showDialog(this, "Screening Test Reactive Result has been saved but final result is Indeterminate.",
									"Saved", "Close ");
							jTabbedPane1.setSelectedIndex(0);
							jTabbedPane1.remove(2);
							jTabbedPane1.remove(1);
							((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
							if(isIssued)
								//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
								 screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
							
						} else if (ok == 0) {
							Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
							//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
							
							Integer confirmatoryBalance = Integer.parseInt(products.get("confirmatorytotal").toString())-1;
							products.put("screeningtotal", screeningBalance);
							products.put( "confirmatorytotal", confirmatoryBalance);
							HivTestResultDao hivTestResultDao = new HivTestResultDao();
							testId = hivTestResultDao.insertScreeningResult(screeningResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
							hivTestResultDao.insertConfirmatory(testResult, testPurpose, confirmatoryProduct, testId, siteId, confirmatoryBalance, dateOfTest, otherPurpose);
							hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, dateOfTest, siteId);

							new MessageDialog().showDialog(this, "Confirmatory Test Non-Reactive Result has been saved with final result of Indeterminate.",
									"Saved", "Close and return ");
							((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
							((DefaultTableModel) this.testTypeJtable.getModel()).addRow(screeningtypeTestData[0]);
							jTabbedPane1.setSelectedIndex(0);
							jTabbedPane1.remove(2);
							jTabbedPane1.remove(1);
							((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
							if(isIssued){
								//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
								//confirmatoryLbl.setText(confirmatoryName+"("+(int)products.get(confirmatoryProduct)+")");
								
								 screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
								 confirmatoryLbl.setText(confirmatoryName+"("+(confirmatoryBalance)+")");
								
							}
						}

					}

				}
				else if (testResult.equalsIgnoreCase("IN")){
					//int screeningBalance = (int)products.get(screeningProduct)-1;
					//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
					
					Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
					//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
					
					Integer confirmatoryBalance = Integer.parseInt(products.get("confirmatorytotal").toString())-1;
					products.put("screeningtotal", screeningBalance);
					products.put( "confirmatorytotal", confirmatoryBalance);

					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					testId = hivTestResultDao.insertScreeningResult(screeningResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
					hivTestResultDao.insertConfirmatory(testResult, testPurpose, confirmatoryProduct, testId, siteId, confirmatoryBalance, dateOfTest, otherPurpose);
					hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, dateOfTest,siteId);
					new MessageDialog().showDialog(this, "Confirmatory Test Indeterminate Result has been saved with final result of Indeterminate.",
							"Saved", "Close and return ");
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					((DefaultTableModel) this.testTypeJtable.getModel()).addRow(screeningtypeTestData[0]);
					//((DefaultTableModel) this.testTypeJtable.getModel()).addRow(typeTestData[1]);
					jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					if(isIssued){
						//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
						//confirmatoryLbl.setText(confirmatoryName+"("+(int)products.get(confirmatoryProduct)+")");
						    screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
							confirmatoryLbl.setText(confirmatoryName+"("+(confirmatoryBalance)+")");
						
						
						
					}
				}

			} 

		}
		else{//Quality Control Work Flow
			if (this.typeOfTest.equals("Screening")	&& testResult.equalsIgnoreCase("NR")) {
				/*int ok = new MessageDialog()
				.showDialog(
						this,
						"Screening test is Non-Reactive would you\nlike to Save the test now?",
						"Screening Test", "YES, Save \nScreening Test Result",
						"NO, Cancel\n Screening Test Non-Reactive Result");*/

				//if (ok == 0) {
				Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
				//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
								
				products.put("screeningtotal", screeningBalance);
				
					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					testId = hivTestResultDao.insertScreeningResult(testResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
					//hivTestResultDao.insertFinalTestResult(testId, 'N', testPurpose, sqlDate, siteId);
					new MessageDialog().showDialog(this, "Screening Test Non-Reactive Result has been saved.",
							"Saved", "Close and return ");
					jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					if(isIssued)
						//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
						 screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
					
				//}
				/****************************************************/
				//if screening test is reactive 
			} else if (this.typeOfTest.equals("Screening")	&& testResult.equalsIgnoreCase("R")) {
				screeningResult = "R";
				//prompt if you should do confirmatory rest

				/**************************************************/

				/*int ok = new MessageDialog()
				.showDialog(
						this,
						"Screening test is Reactive. \nDo you want to carry out Confirmatory Test to confirm result?",
						"Screening Test - Reactive", "YES, Do \nConfirmatroy Test",
						"NO, Save\nScreening Test Only");*/

				//if (ok == 1) {
					//option 2 screening is reactive 
					//NO confirmatory test
					//int screeningBalanace = (int)products.get(screeningProduct)-1;
					Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
					//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
									
					products.put("screeningtotal", screeningBalance);
					
					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					testId = hivTestResultDao.insertScreeningResult(testResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
					//hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, sqlDate, siteId);
					new MessageDialog().showDialog(this, "Screening Test Reactive Result has been saved, please carry out confirmatory test. ",
							"Saved", "Continue ");
					/*jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);*/
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					//((DefaultTableModel) this.testTypeJtable.getModel()).addRow(confirmatoryTestData[0]);
					jTabbedPane1.remove(jTabbedPane1.getTabCount() - 1);
					this.typeOfTest = "Confirmatory";
					if(isIssued)
						//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
						 screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
					
			//	} 
			/*else if (ok == 0) {
					//yes save confirmatory
					((DefaultTableModel) this.testTypeJtable.getModel())
					.removeRow(0);
					((DefaultTableModel) this.testTypeJtable.getModel()).addRow(confirmatoryTestData[0]);
					jTabbedPane1.remove(jTabbedPane1.getTabCount() - 1);
					this.typeOfTest = "Confirmatory";
					//this.resultJtable.getModel().setValueAt(2,this.resultJtable.getSelectedRow(), 0);

				}*/

			}  
			else if (this.typeOfTest.equals("Screening")	&& testResult.equalsIgnoreCase("IN")) {
				screeningResult = "ID";
				//int screeningBalanace = (int)products.get(screeningProduct)-1;
				Integer screeningBalance = Integer.parseInt(products.get("screeningtotal").toString())-1;
				//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
								
				products.put("screeningtotal", screeningBalance);
			
				HivTestResultDao hivTestResultDao = new HivTestResultDao();
				testId = hivTestResultDao.insertScreeningResult(testResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest, otherPurpose);
				//hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, dateOfTest, siteId);
				JOptionPane.showConfirmDialog(this, "Screening test is Invalid.", "Invalid test", JOptionPane.OK_OPTION);
				
				/*int ok = new MessageDialog()
				.showDialog(
						this,
						"Screening test is Indeterminate. \nDo you want to carry out Confirmatory Test to confirm result?",
						"Screening Test - Reactive", "YES, Do \nConfirmatroy Test",
						"NO, Save\nScreening Test Only");*/
				jTabbedPane1.setSelectedIndex(0);
				jTabbedPane1.remove(2);
				jTabbedPane1.remove(1);
				((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
				if(isIssued)
					//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
					 screeningLbl.setText(screeningName+"("+(screeningBalance)+")");
				
			}
			else if (this.typeOfTest.equals("Confirmatory")) {

				//option 3 if confirmatory is reactive(R)  
				if (testResult.equalsIgnoreCase("R")) {
					//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
					
					//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
					
					Integer confirmatoryBalance = Integer.parseInt(products.get("confirmatorytotal").toString())-1;
					products.put( "confirmatorytotal", confirmatoryBalance);
					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					//testId = hivTestResultDao.insertScreeningResult(screeningResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest);
					hivTestResultDao.insertConfirmatory(testResult, testPurpose, confirmatoryProduct, testId, siteId, confirmatoryBalance, dateOfTest, otherPurpose);
					//hivTestResultDao.insertFinalTestResult(testId, 'P', testPurpose, sqlDate,siteId);

					new MessageDialog().showDialog(this, "Confirmatory Test Reactive.",
							"Saved", "Close and return ");
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					//((DefaultTableModel) this.testTypeJtable.getModel()).addRow(typeTestData[0]);
					//((DefaultTableModel) this.testTypeJtable.getModel()).addRow(typeTestData[1]);
					jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					if(isIssued){
						    screeningLbl.setText(screeningName+"("+(Integer.parseInt(products.get("screeningtotal").toString()))+")");
							confirmatoryLbl.setText(confirmatoryName+"("+(confirmatoryBalance)+")");
					}
				}
				else if (testResult.equalsIgnoreCase("IN")){
				//	System.out.println("Ind...confirmatory...");
					//int screeningBalance = (int)products.get(screeningProduct)-1;
					//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
					
					Integer confirmatoryBalance = Integer.parseInt(products.get("confirmatorytotal").toString())-1;
					
					products.put( "confirmatorytotal", confirmatoryBalance);
					HivTestResultDao hivTestResultDao = new HivTestResultDao();
					//testId = hivTestResultDao.insertScreeningResult(screeningResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest);
					hivTestResultDao.insertConfirmatory(testResult, testPurpose, confirmatoryProduct, testId, siteId, confirmatoryBalance, dateOfTest, otherPurpose);
					//hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, dateOfTest,siteId);
					new MessageDialog().showDialog(this, "Confirmatory Test Invalid.",
							"Saved", "Close and return ");
					
					((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
					//((DefaultTableModel) this.testTypeJtable.getModel()).addRow(typeTestData[0]);
					//((DefaultTableModel) this.testTypeJtable.getModel()).addRow(typeTestData[1]);
					jTabbedPane1.setSelectedIndex(0);
					jTabbedPane1.remove(2);
					jTabbedPane1.remove(1);
					if(isIssued){
						//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
						//confirmatoryLbl.setText(confirmatoryName+"("+(int)products.get(confirmatoryProduct)+")");
						
						    screeningLbl.setText(screeningName+"("+(Integer.parseInt(products.get("screeningtotal").toString()))+")");
							confirmatoryLbl.setText(confirmatoryName+"("+(confirmatoryBalance)+")");
					
					}
				}
				else if (testResult.equalsIgnoreCase("NR")) {
					//check if a screening test was done and was reactive 
					//if (screeningResult.equalsIgnoreCase("R") ) {
						//if Confirmatory is NR and Screening was reactive
						//prompt if facility can do tiebreaker

						/*int ok = new MessageDialog()
						.showDialog(
								this,
								"Confirmatory test is Non-Reactive would you\n like to Save the test now?",
								"Confirmatory Test", "YES, Save\n Confirmatory Test", "Cancel\n Final test will be Indeterminate.");*/

						//if (ok == 1) {
							//int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
					
					        Integer confirmatoryBalance = Integer.parseInt(products.get("confirmatorytotal").toString())-1;
					        products.put( "confirmatorytotal", confirmatoryBalance);
							HivTestResultDao hivTestResultDao = new HivTestResultDao();
							hivTestResultDao.insertConfirmatory(testResult, testPurpose, confirmatoryProduct, testId, siteId, confirmatoryBalance, dateOfTest, otherPurpose);
							new MessageDialog().showDialog(this, "Confirmatory Test is Non-Reactive Result.","Saved", "Close ");
							jTabbedPane1.setSelectedIndex(1);
							jTabbedPane1.remove(2);
							jTabbedPane1.remove(1);
							((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
							if(isIssued){
								//screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
								//confirmatoryLbl.setText(confirmatoryName+"("+(int)products.get(confirmatoryProduct)+")");
								
								 screeningLbl.setText(screeningName+"("+(Integer.parseInt(products.get("screeningtotal").toString()))+")");
								 confirmatoryLbl.setText(confirmatoryName+"("+(confirmatoryBalance)+")");
								
							}
						//} 
				/*else if (ok == 0) {
							int screeningBalance = (int)products.get(screeningProduct)-1;
							int confirmatoryBalance = (int)products.get(confirmatoryProduct)-1;
							products.put( screeningProduct, screeningBalance);
							products.put( confirmatoryProduct, confirmatoryBalance);
							HivTestResultDao hivTestResultDao = new HivTestResultDao();
							testId = hivTestResultDao.insertScreeningResult(screeningResult, testPurpose, screeningProduct, siteId, screeningBalance, dateOfTest);
							hivTestResultDao.insertConfirmatory(testResult, testPurpose, confirmatoryProduct, testId, siteId, confirmatoryBalance, dateOfTest);
							//hivTestResultDao.insertFinalTestResult(testId, 'I', testPurpose, sqlDate, siteId);

							new MessageDialog().showDialog(this, "Confirmatory Test Non-Reactive Result has been saved with final result of Indeterminate.",
									"Saved", "Close and return ");
							((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
							((DefaultTableModel) this.testTypeJtable.getModel()).addRow(typeTestData[0]);
							jTabbedPane1.setSelectedIndex(1);
							jTabbedPane1.remove(2);
							if(isIssued){
								screeningLbl.setText(screeningName+"("+(int)products.get(screeningProduct)+")");
								confirmatoryLbl.setText(confirmatoryName+"("+(int)products.get(confirmatoryProduct)+")");
							}
						}*/

					//}

				} 

			} 
		}
	}

	private void testTypeJtableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		this.typeOfTest = this.testTypeJtable.getModel().getValueAt(
				this.testTypeJtable.getSelectedRow(), 0).toString();

		jTabbedPane1.addTab("Result", new javax.swing.ImageIcon(getClass()
				.getResource("/images/cakes3d.png")), resultJP);
		jTabbedPane1.setSelectedIndex(2);
	}

	private void purposeJTableMouseClicked(java.awt.event.MouseEvent evt) {
		testPurpose = purposeJTable.getModel().getValueAt(purposeJTable.getSelectedRow(), 0).toString();
		otherPurpose = "";
		if (testPurpose.equalsIgnoreCase("Quality Control")){
			resetTestingPanel = true;
			testPurpose = "QC";
		}
		else if (testPurpose.equalsIgnoreCase("Clinical Diagnosis")){
			testPurpose = "CD";
			resetTestingPanel = true;
			//otherPurpose = "";
		}
		else if (testPurpose.equalsIgnoreCase("Other")){
			testPurpose = "O";
			OtherPurposeForm otherPurposeForm = new OtherPurposeForm(this, true);
			otherPurposeForm.setVisible(true);
			if (OtherPurposeForm.isReasonGiven()== true){
				resetTestingPanel = true;
				otherPurpose = OtherPurposeForm.getReason();
			}
			else{
				resetTestingPanel = false;
				jTabbedPane1.setSelectedIndex(0);
				//jTabbedPane1.remove(1);
				//((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
			}
		}
		
		hivTest.setPurpose(purposeJTable.getModel().getValueAt(purposeJTable.getSelectedRow(), 0).toString());
		if (testPurpose.equalsIgnoreCase("QC")){
			//System.out.println("QC **** : "+testPurpose);
			if (testTypeJtable.getRowCount() != 0)
				((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
			((DefaultTableModel) this.testTypeJtable.getModel()).addRow(screeningtypeTestData[0]);
			((DefaultTableModel)this.testTypeJtable.getModel()).addRow(confirmatoryTestData[0]);
			jTabbedPane1.addTab("Type of Test", testTypeJP);
		}
		else{
			//System.out.println("Non QC **** : "+testPurpose);
			if (resetTestingPanel){
			if (testTypeJtable.getRowCount() != 0)
			((DefaultTableModel) this.testTypeJtable.getModel()).removeRow(0);
			((DefaultTableModel) this.testTypeJtable.getModel()).addRow(screeningtypeTestData[0]);
			jTabbedPane1.addTab("Type of Test", testTypeJP);
			}
		}
		if (resetTestingPanel)
		jTabbedPane1.setSelectedIndex(1);
		backJL.setVisible(true);
	}

	private void purposeJTableKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		final char c = evt.getKeyChar();

		if (c == KeyEvent.VK_ENTER) {

			selectTabIndex = jTabbedPane1.getSelectedIndex();

			if (selectTabIndex == 0) {

				jTabbedPane1.setSelectedIndex(1);

			} else if (selectTabIndex == 1) {

				jTabbedPane1.setSelectedIndex(2);

			}
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				Properties prop = new Properties(System.getProperties());
				FileInputStream fis = null;
				try {
					fis = new FileInputStream("Programmproperties.properties");
					prop.load(fis);

					System.setProperties(prop);

					fis.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
				String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
				System.setProperty("dbpassword", dbpassword);
				RecordTestJD dialog = new RecordTestJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}
	private javax.swing.JLabel backJL;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JPanel purposeJP;
	private javax.swing.JTable purposeJTable;
	private javax.swing.JPanel resultJP;
	private javax.swing.JTable resultJtable;
	private javax.swing.JPanel testTypeJP;
	private javax.swing.JTable testTypeJtable;
	private JLabel confirmatoryLbl;
	private JPanel panel_1;
	private JPanel panel_2;
}