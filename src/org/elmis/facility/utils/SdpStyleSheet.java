package org.elmis.facility.utils;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextArea;
/**
 * 
 * @author Michael Mwebaze
 * @version 1.0
 */
/**
 * 
 * @author Michael Mwebaze
 * @version 1.0
 */
public class SdpStyleSheet {
	
	private static Font sdpFont = new Font("Tahoma", Font.BOLD, 13);
	
	public static void configJTextAreaFont(JTextArea jTextArea)
	{
		//Font font = new Font("Verdana", Font.BOLD, 12);
		jTextArea.setFont(sdpFont);
	}

	public static void configTitleJLabel(JLabel titleJLabel)
	{
		//titleJLabel.setFont(new Font("Tahoma", Font.BOLD, 13));
		titleJLabel.setFont(sdpFont);
		titleJLabel.setForeground(new java.awt.Color(255, 255, 255));
	}
	public static void configOtherJLabel(JLabel otherJLabel)
	{
		otherJLabel.setFont(sdpFont);//setFont(new Font("Tahoma", Font.BOLD, 13));
		otherJLabel.setForeground(new java.awt.Color(255, 255, 255));
	}
	public static void configJPanelBackground(JPanel jPanel)
	{
		jPanel.setBackground(new java.awt.Color(102, 102, 102));
	}
	public static void configJDialogBackground(JDialog jDialog)
	{
		jDialog.getContentPane().setBackground(new java.awt.Color(102, 102, 102));
	}
	public static void configCancelButton(JDialog jDialog, JButton cancelButton)
	{
		cancelButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelButton.setIcon(new javax.swing.ImageIcon(jDialog.getClass().getResource("/elmis_images/Cancel.png")));
	}
	public static void configDeleteButton(JButton deleteButton)
	{
		
	}
	public static void configViewButton(JDialog jDialog, JButton viewButton)
	{
		viewButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		viewButton.setIcon(new javax.swing.ImageIcon(jDialog.getClass().getResource("/elmis_images/View report.png")));
	}
	public static void configSaveButton(JDialog jDialog,JButton saveButton)
	{
		saveButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		saveButton.setIcon(new javax.swing.ImageIcon(jDialog.getClass().getResource("/elmis_images/Save icon.png")));
	}
	public static void configTable(JTable jTable)
	{
		jTable.setFont(new java.awt.Font("Ebrima", 0, 14));
		jTable.setRowHeight(35);
		jTable.setBackground(Color.white);
	}
}
