package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.LaboratoryTestInformation;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.CalendarUtil;

public class LaboratoryTestInformationDao {

	private SqlSessionFactory sqlSessionFactory;
	//private CalendarUtil calenderUtil = new CalendarUtil();
	
	public LaboratoryTestInformationDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	
	public List<LaboratoryTestInformation> getLabEquipmentInfoReport(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate)
	{
		Map<String, Date> map = new HashMap<>();
		
		map.put("beginDate", beginDate);
		map.put("endDate", endDate);
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<LaboratoryTestInformation> list = session.selectList("LaboratoryTestInformation.getReport", map);
			return list;
		} finally {
			session.close();
		}
	}
	public void saveLabEquipmentInfoData(List<LaboratoryTestInformation> labTestInfoList, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate)
	{
		 SqlSession session = sqlSessionFactory.openSession();
		 
	        try {
	        	deleteCreatedLabReport(fromDate, toDate, session);
	        	for (LaboratoryTestInformation l : labTestInfoList)
	            session.insert("LaboratoryTestInformation.insert", l);
	            session.commit();
	        } finally {
	            session.close();
	        }
	}
	private void deleteCreatedLabReport(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, SqlSession session)
	{
		//SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap <String, Date>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);

		/*try {*/			
			session.delete("LaboratoryTestInformation.checkSubmission", map);
			//session.commit();
				
		/*} finally {
			session.close();
		}*/
	}
}
