package org.elmis.facility.domain.model;

import java.util.Date;

public class Geographic_Zones {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.id
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private Integer id;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.code
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private String code;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.name
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.levelid
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private Integer levelid;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.parent
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private Integer parent;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.modifiedby
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private Integer modifiedby;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.modifieddate
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private Date modifieddate;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.createdby
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private Integer createdby;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column geographic_zones.createddate
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    private Date createddate;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.id
     *
     * @return the value of geographic_zones.id
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public Integer getId() {
        return id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.id
     *
     * @param id the value for geographic_zones.id
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.code
     *
     * @return the value of geographic_zones.code
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public String getCode() {
        return code;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.code
     *
     * @param code the value for geographic_zones.code
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.name
     *
     * @return the value of geographic_zones.name
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.name
     *
     * @param name the value for geographic_zones.name
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.levelid
     *
     * @return the value of geographic_zones.levelid
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public Integer getLevelid() {
        return levelid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.levelid
     *
     * @param levelid the value for geographic_zones.levelid
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setLevelid(Integer levelid) {
        this.levelid = levelid;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.parent
     *
     * @return the value of geographic_zones.parent
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public Integer getParent() {
        return parent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.parent
     *
     * @param parent the value for geographic_zones.parent
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setParent(Integer parent) {
        this.parent = parent;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.modifiedby
     *
     * @return the value of geographic_zones.modifiedby
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public Integer getModifiedby() {
        return modifiedby;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.modifiedby
     *
     * @param modifiedby the value for geographic_zones.modifiedby
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setModifiedby(Integer modifiedby) {
        this.modifiedby = modifiedby;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.modifieddate
     *
     * @return the value of geographic_zones.modifieddate
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public Date getModifieddate() {
        return modifieddate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.modifieddate
     *
     * @param modifieddate the value for geographic_zones.modifieddate
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setModifieddate(Date modifieddate) {
        this.modifieddate = modifieddate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.createdby
     *
     * @return the value of geographic_zones.createdby
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public Integer getCreatedby() {
        return createdby;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.createdby
     *
     * @param createdby the value for geographic_zones.createdby
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setCreatedby(Integer createdby) {
        this.createdby = createdby;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column geographic_zones.createddate
     *
     * @return the value of geographic_zones.createddate
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public Date getCreateddate() {
        return createddate;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column geographic_zones.createddate
     *
     * @param createddate the value for geographic_zones.createddate
     *
     * @mbggenerated Wed Jul 17 09:21:19 CAT 2013
     */
    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }
}