/*
 * ProgramsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.reports.consumptiontrend;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.reports.stocktrends.TimeSeriesChart;
import org.elmis.facility.reports.stocktrends.TimeSeriesJD;
import org.jfree.chart.JFreeChart;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.TableModel;

/**
 * 
 * @author __USER__
 */
public class ProductsJP extends javax.swing.JPanel {

	// Create tableModel for Program Products for each facility
	private static final String[] columns_products = { "Product Code","Product name", "id" };
	private static final Object[] defaultv_products = { "","", "" };
	private static final int rows_products = 0;
	public static TableModel tableModel_products = new TableModel(
			columns_products, defaultv_products, rows_products);

	private static Products products;
	private static ListIterator<Products> productsIterator;

	private  List<Products> productsList = new LinkedList();
	private int selectedProgramID;
	
	
	private static Map parameterMap = new HashMap();
	private JasperPrint print;

	/** Creates new form ProgramsJP */
	public ProductsJP(int selectedProgramID) {
		
		this.selectedProgramID = selectedProgramID;
		initComponents();
		this.setFocusable(false);
		this.productsJTable.setFocusable(false);// remove from focus cycle
		
		
		this.productsJTable.getColumnModel().getColumn(0).setMinWidth(120);
		this.productsJTable.getColumnModel().getColumn(0).setMaxWidth(120);
		this.productsJTable.getColumnModel().getColumn(2).setMinWidth(0);
		this.productsJTable.getColumnModel().getColumn(2).setMaxWidth(0);

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			//if get all products is selected
			if(this.selectedProgramID == 0){
			this.productsList = session.selectList("getAll");
			
		}else{
			
			//if a program area is selected 
			this.productsList = session.selectList("selectProductByProgramID",
					this.selectedProgramID);
		}
			
			

			this.populateProductsTable(this.productsList);

		} finally {
			session.close();
		}
	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		productsJTable = new javax.swing.JTable();

		productsJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		productsJTable.setModel(tableModel_products);
		productsJTable.setRowHeight(60);
		productsJTable.setTableHeader(null);
		productsJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				productsJTableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(productsJTable);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE));
	}// </editor-fold>

	// GEN-END:initComponents

	private void productsJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
		

	int	selectedProductID = Integer
				.parseInt(this.tableModel_products.getValueAt(
						this.productsJTable.getSelectedRow(), 2).toString());
	
	String selectedProductName = this.tableModel_products.getValueAt(
			this.productsJTable.getSelectedRow(), 1).toString();
	
	JFreeChart chartIn = new TimeSeriesChart().createChart("Consumption Trend for "+ selectedProductName ,selectedProductID );
	
//	AppJFrame.glassPane.activate(null);

	new TimeSeriesJD(javax.swing.JOptionPane.getFrameForComponent(this),
			true, chartIn);
	
//	AppJFrame.glassPane.deactivate();

	
		

	}

	public static void populateProductsTable(List progProducts) {

		tableModel_products.clearTable();
		productsIterator = progProducts.listIterator();

		while (productsIterator.hasNext()) {

			products = productsIterator.next();
			
			defaultv_products[0] = products.getCode();
			defaultv_products[1] = products.getPrimaryname();

			defaultv_products[2] = products.getId();

			// defaultv_prgramproducts[5] = products.g

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_products.length; j++) {
				cols.add(defaultv_products[j]);

			}

			tableModel_products.insertRow(cols);

			productsIterator.remove();
		}
	}
	
	
	private void createStockControlCardX(int selectedProductID){
		
		SqlSessionFactory factory = new MyBatisConnectionFactory().getSqlSessionFactory();

		SqlSession session = factory.openSession();
		StockControlCard	scc = new StockControlCard();
		List sccList = new ArrayList();
		try {

			//scc
			sccList = session.selectList("selectBySCCByProductID",selectedProductID);//("getProgramesSupported");
			
			//System.out.println(this.products.getPrimaryname() +" "+IssuingJD.selectedProductID);

			//this.populateProgramsTable(this.programsList);

		} finally {
			session.close();
		}
		
		
		// TODO add your handling code here:

		
		
		

			

			

			
		

		// new Items_issue().createBeanCollection()
		try {

			
			
			

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport("Reports/stockcontrolcard.jasper",
					parameterMap, new JRBeanCollectionDataSource(sccList));

			new JasperViewer(print);
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}
	
		
		
		
	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable productsJTable;
	// End of variables declaration//GEN-END:variables

}