package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
//import org.elmis.facility.reporting.model.RandrArdDrug;
import org.elmis.facility.reporting.model.ReportRequisition;

/**
 * ReportRequisitionDao.java
 * Report Requisition DAO that calls the SQL statements necessary to
 * generate the required R & R
 * @author Michael Mwebaze
 * @version 1.0
 */
public class ReportRequisitionDao {

	private SqlSessionFactory sqlSessionFactory;

	/**
	 * Constructor that initializes the required session factory which can be used to 
	 * open and close connections to a database
	 */
	public ReportRequisitionDao(){

		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getSubmitedRandRs(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String programArea)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		try
		{
			if (programArea.equals("ARV"))
			{
				return session.selectList("ArvRequisition.submittedRandR", map);
			}
			else if (programArea.equals("HIV"))
			{
				return session.selectList("HivRequisition.submittedRandR", map);
			}
			else if (programArea.equals("LAB"))
			{
				return session.selectList("LabRequisition.submittedRandR", map);
			}
			else
			{
				return session.selectList("EmlipRequisition.submittedRandR", map);
			}
		}
		finally
		{
			session.close();
		}
	}
	public boolean isReportSubmitted(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("programCode") int programCode,  @Param("orderType")String orderType)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		map.put("programCode", programCode);
		map.put("orderType", orderType);

		try {
			Integer numberOfsubmittedRandRs = session.selectOne("ReportRequisition.check_submitted_randr", map);
			 if (numberOfsubmittedRandRs == null)
				 numberOfsubmittedRandRs = 0;

			if (numberOfsubmittedRandRs > 0)
				return true;
			else
				return false;
		}
		finally {
			session.close();
		}	
	}
	public void submitReportAndRequisition(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, @Param("programCode") int programCode, @Param("reportType")String reportType)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		map.put("programCode", programCode);
		map.put("reportType", reportType);

		try {
			session.update("ReportRequisition.change_randr_status", map);
			session.insert("ReportRequisition.save_submitted_randr", map);
			session.commit(); 
		}
		finally {
			session.close();
		}	
	}
	public List<ReportRequisition> searchSubmitedRandRs(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String programArea)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Date> map = new HashMap<>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		try
		{
			if (programArea.equals("ARV"))
			{
				return session.selectList("ArvRequisition.search_submitted_randr", map);
			}
			else if (programArea.equals("HIV"))
			{
				return session.selectList("HivRequisition.search_submitted_randr", map);
			}
			else if (programArea.equals("LAB"))
			{
				return session.selectList("LabRequisition.search_submitted_randr", map);
			}
			else
			{
				return session.selectList("EmlipRequisition.search_submitted_randr", map);
			}
		}
		finally
		{
			session.close();
		}
	}
}
