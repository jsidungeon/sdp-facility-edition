package org.elmis.forms.stores.receiving;

/***************************************************
	 * STARTING CHILD FORM FOR ADJUSTMENTS 
	 ***************************************************/
/**
 *
 * @author  __MKausa__
 */

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.swing.AbstractCellEditor;
import javax.swing.ComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.text.JTextComponent;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.dao.AdjustmentDao;
import org.elmis.facility.domain.model.Adjustment;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.domain.model.facilities;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import javax.swing.JRadioButton;

import java.awt.event.MouseAdapter;
//import com.oribicom.tools.TableModel;

public class ArvAdjustmentJD extends javax.swing.JDialog   {
	

	private FacilitySetUpDAO callfacility;
	private Facility_Types facilitytype = null;
	private Facility facility = null;
	private String dispensaryName = System.getProperty("dp_name");
	private int remarkRowSelected;
	private Map<String, Boolean> adjustmentAdditiveMap = new HashMap<>();
	private String typeCode;
	private TableColumnAligner tablecolumnaligner; 
	private Timestamp productdeliverdate;
    private JComboBox comboBox;
    private ComboBoxModel model;
    private JTextComponent editor;
    List<ProductQty> productcodesstockQtyList = new LinkedList();
    ARVDispensingDAO aFacilityproductsmapper = null;
	private List<facilities> facilitiesnameList =  new LinkedList<>();
	private Elmis_Stock_Control_Card Adjustmentelmis_stock_control_card; 
	private JComboBox facilityList = new JComboBox();
	private MutableComboBoxModel modelfacilities = (MutableComboBoxModel) facilityList
			.getModel();
	private JComboBox facilitynamecomboList = new JComboBox();
	private MutableComboBoxModel modelfacilitiesList = (MutableComboBoxModel) facilitynamecomboList
			.getModel();
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList<>();
	@SuppressWarnings("unchecked")
	//private ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	private List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList<>();
	private FacilityProductsDAO Facilityproductsmapper = null;
	private FacilitySetUpDAO facilitiesmapper =  null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	//public static Boolean cansave = false;
	private String productName = "";
	private String Pstrength = "";
	private String pCode = "";
	private int colindex = 0;
	private int rowindex = 0;
	public String Adjustmentform = "";
	private Integer productId = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentName = "";
	private String facilityFromAdjustmentName = "";
	private String facilityToAdjustmentName = "";
	private final String[] adjustmentTableColumnNames = {
		"Adjustment Type", "Quantity", "Remarks"};
	private static final int rows_RegisterAdjustmentsfproducts = 0;
    private final Object[] defaultv_RegisterAdjustmentsfacilityapprovedproducts = { "", "",
		""};
    private String[][] data = null;
	private DefaultTableModel adjustmentTableModel= new DefaultTableModel(data, adjustmentTableColumnNames) {

		@Override
		public boolean isCellEditable(int row, int column) {
			if (column == 1)
				return true;
			else if (column == 2)
				return true;
			else
				return false;
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2)
				return String.class;
			if (columnIndex == 1)
				  return Double.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};


	/*private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();*/

	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public ArvAdjustmentJD(java.awt.Frame parent, boolean modal,String adjustpcode,int adjustproductid,Timestamp adjustmentdateandtime,String progname,String productName, String pstrenth) {
		super(parent, modal);
		
		productdeliverdate = adjustmentdateandtime;
		pCode = adjustpcode;
		productId = adjustproductid;
		facilityprogramcode = progname;
		this.productName = productName;
		Pstrength = pstrenth;
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(1001, 450);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//AdjustmentTypeJCB1.setPreferredSize(new Dimension(200, 30));

	}

	
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		saveJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record ARV Dispensary Adjustments ");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
			public void windowActivated(java.awt.event.WindowEvent evt) {
				
				formwindowActivated(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		saveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		saveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		saveJBtn.setText("Save");
		saveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		saveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				saveJBtnMouseClicked(evt);
			}
		});
		saveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnCancel = new JButton();
		btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCancelMouseClicked(evt);
			}

			
		});
		btnCancel.setText("Cancel");
		btnCancel.setFont(new Font("Ebrima", Font.BOLD, 12));
		
		enterfacilityname = new JLabel("Transfer from facility");
		enterfacilityname.setIcon(new ImageIcon(testMultipleAdjustments.class.getResource("/elmis_images/eLMIS basic info small.png")));
		enterfacilityname.setFont(new Font("Ebrima", Font.BOLD, 13));
		enterfacilityname.setForeground(Color.WHITE);
		
	   transfertofacilityJCB = new JComboBox();
		transfertofacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transfertofacilityJCBActionPerformed(evt);
			}
		});
		transfertofacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transfertofacilityJCB.setModel(modelfacilities);
	    transferoutJL = new JLabel("Transfer to facility");
		transferoutJL.setIcon(new ImageIcon(testMultipleAdjustments.class.getResource("/elmis_images/eLMIS basic info small.png")));
		transferoutJL.setForeground(Color.WHITE);
		transferoutJL.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		 transferfromfacilityJCB = new JComboBox();
		transferfromfacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transferfromfacilityJCBActionPerformed(evt);
			}
		});
		transferfromfacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transferfromfacilityJCB.setModel(modelfacilitiesList);
		
		JLabel lblNewLabel = new JLabel("Adjustments for : ");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		Adjustmentproductnamelbl = new JLabel("");
		Adjustmentproductnamelbl.setForeground(Color.WHITE);
		Adjustmentproductnamelbl.setFont(new Font("Ebrima", Font.BOLD, 14));
		
		transferoutrdbtn = new JRadioButton("Transfer Out");
		transferoutrdbtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				transferOutComboEnabled(evt);
			}
		});
		transferoutrdbtn.setForeground(Color.WHITE);
		transferoutrdbtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		transferoutrdbtn.setBackground(new Color(102, 102, 102));
		
		transferinrdbtn = new JRadioButton("Transfer In");
		transferinrdbtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				transferInComboEnabled(evt);
			}
		});
		transferinrdbtn.setForeground(Color.WHITE);
		transferinrdbtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		transferinrdbtn.setBackground(new Color(102, 102, 102));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(39)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(Adjustmentproductnamelbl, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
					.addGap(424))
				.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(saveJBtn))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGap(23)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
								.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
									.addComponent(transferoutrdbtn, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
									.addGap(62)
									.addComponent(transferinrdbtn, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(enterfacilityname)
										.addComponent(transferoutJL)
										.addComponent(transferfromfacilityJCB, 0, 318, Short.MAX_VALUE)
										.addComponent(transfertofacilityJCB, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED, 86, Short.MAX_VALUE)))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 532, GroupLayout.PREFERRED_SIZE)))
					.addGap(36))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(Adjustmentproductnamelbl, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(transferoutrdbtn)
								.addComponent(transferinrdbtn))
							.addGap(18)
							.addComponent(enterfacilityname)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transferfromfacilityJCB, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
							.addGap(28)
							.addComponent(transferoutJL)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transfertofacilityJCB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
					.addGap(18)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(saveJBtn)
						.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(222, Short.MAX_VALUE))
		);

	
		registerAdjustmentJT = new JTable();
		scrollPane.setViewportView(registerAdjustmentJT);
		registerAdjustmentJT.setShowVerticalLines(false);
		registerAdjustmentJT.setShowHorizontalLines(true);
		registerAdjustmentJT.setShowGrid(false);
		registerAdjustmentJT.setRowSelectionAllowed(true);
		registerAdjustmentJT.setModel(adjustmentTableModel);
		registerAdjustmentJT.getColumnModel().getColumn(2).setCellEditor(new RemarkTextAreaCellEditor());
		registerAdjustmentJT.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		registerAdjustmentJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						registerAdjustmentJTMouseClicked(evt);
						//registerAdjustment();
						
					}
				});
		registerAdjustmentJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						//RegisterAdjustmentJTPropertyChange(evt);
					}

					
				});
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 995, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>

	protected void registerAdjustment() {
		//Adjustment adjustment = new Adjustment();
		System.out.println(">>>>> "+pCode);
		
	}


	protected void transferfromfacilityJCBActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		
		JComboBox cb = (JComboBox) evt.getSource();
		facilityFromAdjustmentName = (String) cb.getSelectedItem();
		//System.out.println(facilityFromAdjustmentName);
		
	}


	protected void transfertofacilityJCBActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox) evt.getSource();
		facilityToAdjustmentName = (String) cb.getSelectedItem();
		//System.out.println(facilityToAdjustmentName);
	}


	/*private Timestamp Convertdatestr(String mydate) {

		try {

			//System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			//System.out.println(newDateString);
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		return productdeliverdate;

	}*/
	public  void RegisterAdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		//System.out.println("test pop up adjustments");		

	}

	
	/*private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}*/

	private void transferInComboEnabled(MouseEvent evt) {
		// TODO Auto-generated method stub
		
			this.transferfromfacilityJCB.setEnabled(true);
	        this.enterfacilityname.setEnabled(true);
	        
			this.transfertofacilityJCB.setEnabled(false);
	        this.transferoutJL.setEnabled(false);
	       
			//this.createshipmentObj(Pcode, Productid, productdeliverdate);
		
	}

	
	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void populateProgramProductTable(List<Losses_Adjustments_Types> registerAdjustmentfapprovProducts) {

		for (int i = 0; i < registerAdjustmentfapprovProducts.size(); i++)
		{
			String adjustmentTypeName = registerAdjustmentfapprovProducts.get(i).getName();
			Boolean additive = registerAdjustmentfapprovProducts.get(i).getAdditive();
			adjustmentTableModel.addRow(new String[registerAdjustmentfapprovProducts.size()]);
			registerAdjustmentJT.getModel().setValueAt(adjustmentTypeName, i, 0);
			adjustmentAdditiveMap.put(adjustmentTypeName, additive);
		}
	}	
	
	protected void transferOutComboEnabled(MouseEvent evt) {
		// TODO Auto-generated method stub
		this.transfertofacilityJCB.setEnabled(true);
        this.transferoutJL.setEnabled(true);
        this.transferfromfacilityJCB.setEnabled(false);
        this.enterfacilityname.setEnabled(false);
	}


	
	private void saveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		int siteId = AppJFrame.getDispensingId("ARV");
		int rowCount = registerAdjustmentJT.getRowCount();
		java.util.Date date = new java.util.Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		aFacilityproductsmapper = new ARVDispensingDAO();
		Double showdouble = 0.0;   
		List<Adjustment> adjustmentList = new ArrayList<>();
		for (int i = 0; i < rowCount; i++)
		{
			try {
				
				
				Object adjustmentQtyObj = adjustmentTableModel.getValueAt(i,1);
				Object adjustmentRmksObj = adjustmentTableModel.getValueAt(i, 2);
				
				if (adjustmentQtyObj != null){
					
					//changes to validate for adjustment balance 
					Adjustment adjustment = new Adjustment();
					double positiveAdjustment = (Double.parseDouble
							(adjustmentQtyObj.toString()));
					if (positiveAdjustment < 0)
						positiveAdjustment = positiveAdjustment * -1;
					//check adjustment value against dispensary value
					productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(pCode,AppJFrame.getDispensingId("ARV"));
					for (@SuppressWarnings("unused")ProductQty p : productcodesstockQtyList) {
						showdouble  = p.getQty();
					if(p.getQty() >= positiveAdjustment ){
					adjustment.setSiteId(siteId);
					adjustment.setProductCode(pCode);
					adjustment.setDispensaryName(dispensaryName);
					
					String adjustmentType = ""+registerAdjustmentJT.getValueAt(i, 0);
					adjustment.setAdjustmentType(adjustmentType);
					if (adjustmentAdditiveMap.get(adjustmentType))
						adjustment.setAdjustmentQty(positiveAdjustment);
					else
						adjustment.setAdjustmentQty(positiveAdjustment * -1);
					if (adjustmentType.equalsIgnoreCase("TRANSFER_IN")){
						adjustment.setTransferFrom(transferfromfacilityJCB.getSelectedItem().toString());
						adjustment.setTransferTo("");
					}
					else if (adjustmentType.equalsIgnoreCase("TRANSFER_OUT")){
						adjustment.setTransferTo(transfertofacilityJCB.getSelectedItem().toString());
						adjustment.setTransferFrom("");
					}
					else{
						adjustment.setTransferTo("");
						adjustment.setTransferFrom("");
					}
					if (adjustmentRmksObj != null)
						adjustment.setAdjustmentRemark(adjustmentRmksObj.toString());
					else
						adjustment.setAdjustmentRemark("");
					adjustment.setTimestamp(timestamp);
					
					adjustmentList.add(adjustment);
				}else{
					JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than product \n" +" dispensary balance \n\n" +
							"dispensary balance = "+showdouble,
										"Adjustment Error", JOptionPane.ERROR_MESSAGE);
								
				}
				}
					
		}					
				
			}catch(NumberFormatException e){
				e.getStackTrace();
			}

		}
		
		if(!(adjustmentList.isEmpty())){
		// Need to reduce the quantities of balance at dispensary - Moses Kausa 2014
		new updateARVproductBalances(adjustmentList);
		
		new AdjustmentDao().saveArvAdjustments(adjustmentList);
		
		javax.swing.JOptionPane.showMessageDialog(this,productName+" ("+pCode+ ")\n adjustments have been saved.");
		dispose();
		}
	}
	private void btnCancelMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		dispose();
	}
	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}
	
	protected void registerAdjustmentJTMouseClicked(
			MouseEvent evt) {
		adjustmentName = registerAdjustmentJT.getValueAt(registerAdjustmentJT.getSelectedRow(),0).toString();

		if(this.adjustmentName.equals("TRANSFER_IN")) {
			//this.transferfromfacilityJCB.setEnabled(true);
		//	this.enterfacilityname.setEnabled(true);

			//this.transfertofacilityJCB.setEnabled(false);
			//this.transferoutJL.setEnabled(false);

			//this.createshipmentObj(Pcode, Productid, productdeliverdate);
		}else if (this.adjustmentName.equals("TRANSFER_OUT")){
			//this.transfertofacilityJCB.setEnabled(true);
			//this.transferoutJL.setEnabled(true);
			//this.transferfromfacilityJCB.setEnabled(false);
			//this.enterfacilityname.setEnabled(false);


		}else{
			//this.transfertofacilityJCB.setEnabled(false);
			//this.transferfromfacilityJCB.setEnabled(false);;
			//this.enterfacilityname.setEnabled(false);
			//this.transferoutJL.setEnabled(false);
		}

	}
	
/*private boolean checktableCellinput(String s){
		
		 try { 
		        Integer.parseInt(s); 
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    // only got here if we didn't return false
		    return true;
}*/	
/*private void RegisterAdjustmentJTPropertyChange(
			PropertyChangeEvent evt) {
		// TODO Auto-generated method stub

if ("tableCellEditor".equals(evt.getPropertyName())) {       

	if (this.registerAdjustmentJT.isColumnSelected(1)) {
		int col = registerAdjustmentJT.getSelectedColumn();
		 if (registerAdjustmentJT.isEditing())
		    {
		    	//get the adjustment type , quantity entered , and  or remarks 
			
				colindex = this.registerAdjustmentJT
						.getSelectedColumn();
				rowindex = this.registerAdjustmentJT
						.getSelectedRow();
			
				
		    }else{
		    	
		    	 String num;
				    num = (registerAdjustmentJT.getValueAt(registerAdjustmentJT.getSelectedRow(), 1).toString());
				    if(!(checktableCellinput(num))){
				    	JOptionPane.showMessageDialog(null, String.format("Adjustment Quantity must be a positive number",  num));	
				    	//RegisterAdjustmentJT.editCellAt(RegisterAdjustmentJT.getSelectedRow(), 1);
				    }
				    if((checktableCellinput(num))){   
		    	      createshipmentObj(pCode, productId, productdeliverdate);
				    }
	}					


}
	
		
		
}
	


	}*/



	/*public void createshipmentObj(String apcode, int prodid,Timestamp amodifieddate) {
				Integer A = 0;
				Integer B = 0;
				Integer C = 0;
				Integer negativeadjust = 0;
				Integer positive = 0;
				if (productdeliverdate == null) {
					java.util.Date date = new java.util.Date();
					amodifieddate = new Timestamp(date.getTime());
				} else {
					amodifieddate = productdeliverdate;
				}

				System.out.println(amodifieddate);
				System.out.println(A);
				System.out.println(B);
				System.out.println(C);
		
				
			     prodAdjustremark = this.adjustmentTableModel.getValueAt(rowindex,2)
						.toString();
				

				if (!(prodAdjustremark.equals(""))) {
					Adjustmentelmis_stock_control_card.setRemark(prodAdjustremark);
				}
				Adjustmentelmis_stock_control_card.setProductcode(apcode);

				Adjustmentelmis_stock_control_card.setProductid(prodid);
				
		        
				if (this.adjustmentName.equals("TRANSFER_OUT")) {
					
				
					try {
						
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * - 1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					if(!(this.facilityToAdjustmentName.equals(""))){
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.facilityToAdjustmentName);
					}
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
						
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						//negativeadjust = Adjustmentelmis_stock_control_card.getAdjustments();
							
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					

				}
				
				
				if (this.adjustmentName.equals("TRANSFER_IN")) {
					
		
					try {
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(positive);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}

					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.facilityFromAdjustmentName);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									//p.setQty(p.getQty()- positive );
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}

				}
			 
				this.adjustmentName.trim();
				if (this.adjustmentName.equals("DAMAGED")) {
				
	
					
		         try{
		        	 negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
		         }catch(NumberFormatException e){
						e.getStackTrace();
					}

					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				 

				if (this.adjustmentName.equals("LOST")) {

					
					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
					 
				
				if (this.adjustmentName.equals("STOLEN")) {

					
					
					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
					 
				

				if (this.adjustmentName.equals("EXPIRED")) {

								
					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					
					
					
				}
				

				if (this.adjustmentName.equals("PASSED_OPEN_VIAL_TIME_LIMIT")) {
					

					
					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					} 
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				

				if (this.adjustmentName.equals("COLD_CHAIN_FAILURE")) {
					

					
					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
												
							@SuppressWarnings("unused")
							int increase = b.getBalance() -positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
						try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				
				if (this.adjustmentName.equals("CLINIC_RETURN")) {
					
					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}  
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
										
							@SuppressWarnings("unused")
							int increase = b.getBalance() - positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
						aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (this.adjustmentName.equals("FOUND")) {

					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							int increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (this.adjustmentName.equals("PURCHASE")) {
					
	
					try{
						negativeadjust = (Integer
								.parseInt(adjustmentTableModel.getValueAt(rowindex,1)
										.toString()));
						
						negativeadjust = - negativeadjust;
						//System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_isssued(0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							int increase = b.getBalance() + positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode);
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j){
							j.getMessage();
						}
			
	}
				Adjustmentelmis_stock_control_card
						.setId(com.oribicom.tools.publicMethods.createGUID());//this.GenerateGUID());
				A = Adjustmentelmis_stock_control_card.getQty_received();
				B = Adjustmentelmis_stock_control_card.getQty_isssued();
				C = Adjustmentelmis_stock_control_card.getAdjustments();

			  
				System.out.println(A + "WHAT IS  THE NUMBER");
				System.out.println(B);
				System.out.println(C);

				Adjustmentelmis_stock_control_card
						.setAdjustmenttype(this.adjustmentName);
				//System.out.println(Adjustmentelmis_stock_control_card.getBalance());
				//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));
				Adjustmentelmis_stock_control_card.setStoreroomadjustment(true);
				Adjustmentelmis_stock_control_card.setProgram_area(facilityprogramcode);
				Adjustmentelmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
				Adjustmentelmis_stock_control_card.setCreateddate(amodifieddate);

				//created linked list object 

				//adjustmentstockcontrolcardList.add(savestockcontrolcard);
				adjustmentstockcontrolcardList.add(Adjustmentelmis_stock_control_card);
				System.out.println(adjustmentstockcontrolcardList.size()
						+ "ARRAY SIZE???");
				System.out.println("Array size here &&&");
				Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
				//return saveShipped_Line_Items; 		
 }*/
				

	private void formwindowActivated(java.awt.event.WindowEvent evt){
		this.transferfromfacilityJCB.setEnabled(false);
		this.transferfromfacilityJCB.setEnabled(false);
		this.transfertofacilityJCB.setEnabled(false);
       this.enterfacilityname.setEnabled(false);
       transferoutJL.setEnabled(false);
	}
	

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new FacilityProductsDAO();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilitiesmapper = new FacilitySetUpDAO();
		facilitiesnameList = facilitiesmapper.getfacilitynameList();
		
		for (facilities f : facilitiesnameList) {
			modelfacilities.addElement(f.getName());

		}
        
		for (facilities f : facilitiesnameList) {
			this.modelfacilitiesList.addElement(f.getName());

		}
		AutoCompleteDecorator.decorate(this.transferfromfacilityJCB);
		AutoCompleteDecorator.decorate(this.transfertofacilityJCB);

	
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		this.registerAdjustmentJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		registerAdjustmentJT.setFont(new java.awt.Font(
				"Ebrima", 0, 20));
		this.registerAdjustmentJT.setRowHeight(30);
		populateProgramProductTable(facilityAdjustmentList);
		

			if (!this.productName.equals("")) {

				//if the name is too long cut it short for display purpose
				if (this.productName.length() >= 40) {

					this.Adjustmentproductnamelbl.setText(productName.substring(
							0, 40) + "\n ," + " " + Pstrength);
				} else {

					Adjustmentproductnamelbl.setText(productName + " " + Pstrength);
				}
			}
			
	/*	Jpanelfacilityto.setVisible(false);
		this.transferfromfacilityJCB.setVisible(false);
		this.transfertofacilityJCB.setVisible(false);
        this.enterfacilityname.setVisible(false);
        transferoutJL.setVisible(false);*/
		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
		
	}
	
		/**
	 * @param args the command line arguments
	 */

	private javax.swing.JButton saveJBtn;
	private javax.swing.JPanel jPanel1;
	private JTable registerAdjustmentJT;
	private JLabel enterfacilityname;
	private JLabel transferoutJL;
	private JLabel Adjustmentproductnamelbl;
	@SuppressWarnings("rawtypes")
	private static JComboBox transfertofacilityJCB;
	@SuppressWarnings("rawtypes")
	private  JComboBox  transferfromfacilityJCB;
	private JRadioButton transferoutrdbtn;
	private JRadioButton transferinrdbtn;
	class RemarksTextArea extends JTextArea{
		public RemarksTextArea()
		{
			addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent evt) {
					if(evt.getKeyCode() == KeyEvent.VK_ENTER)
					{
						registerAdjustmentJT.requestFocus();
						registerAdjustmentJT.changeSelection(remarkRowSelected + 1,2,false, false);
						if (registerAdjustmentJT.isEditing())
							registerAdjustmentJT.getCellEditor().stopCellEditing();
					}
				}
			});
		}
	}
	public class RemarkTextAreaCellEditor extends AbstractCellEditor implements TableCellEditor {

		private RemarksTextArea remarksTextArea;

		public RemarkTextAreaCellEditor () {
			remarksTextArea = new RemarksTextArea();
		}

		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {
			remarkRowSelected = row;
			remarksTextArea.setLineWrap(true);
			JScrollPane remarksScrollPane = new JScrollPane(remarksTextArea);
			remarksScrollPane
			.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
			remarksTextArea.setText((String) value);
			return remarksScrollPane;
		}

		public Object getCellEditorValue() {
			String remarks = ((JTextArea)remarksTextArea).getText();
			return ((JTextArea)remarksTextArea).getText();
		}
	}
}
	
	/**********************************************************
	 * END CHILD FORM 
	 **********************************************************/
			 
