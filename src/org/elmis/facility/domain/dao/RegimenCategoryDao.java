package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.RegimenCategory;

public class RegimenCategoryDao {
	private SqlSessionFactory sqlSessionFactory;

	public RegimenCategoryDao(){
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	/**
	 * Returns the list of all Contact instances from the database.
	 * @return the list of all Contact instances from the database.
	 */
	@SuppressWarnings("unchecked")
	public List<RegimenCategory> selectAll(){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<RegimenCategory> list = session.selectList("RegimenCategory.getAll");
			return list;
		} finally {
			session.close();
		}
	}

	/**
	 * Returns a Contact instance from the database.
	 * @param id primary key value used for lookup.
	 * @return A Contact instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public RegimenCategory selectById(int id){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			RegimenCategory contact = (RegimenCategory) session.selectOne("RegimenCategory.getById",id);
			return contact;
		} finally {
			session.close();
		}
	}

	/**
	 * Updates an instance of RegimenCategory in the database.
	 * @param contact the instance to be updated.
	 */
	public void update(RegimenCategory regimenCategory){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.update("RegimenCategory.update", regimenCategory);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Insert an instance of RegimenCategory into the database.
	 * @param contact the instance to be persisted.
	 */
	public void insert(RegimenCategory regimenCategory){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.insert("RegimenCategory.insert", regimenCategory);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Delete an instance of RegimenCategory from the database.
	 * @param id primary key value of the instance to be deleted.
	 */
	public void delete(int id){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.delete("Contact.deleteById", id);
			session.commit();
		} finally {
			session.close();
		}
	}
}
