package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.AmcCalculator;
import org.elmis.facility.reports.utils.CalendarUtil;
/**
 * LabRequisitionDao.java
 * Lab Requisition DAO that calls the SQL statements necessary to
 * generate the required LAB R & R
 * @author Michael Mwebaze
 * @version 1.0
 */
public class LabRequisitionDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public LabRequisitionDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getLabIssues(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String normalOrder, String remarks)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		
		if (remarks.trim().length() != 0)
			map.put("remark", remarks);
		Set<String> nonOderableReagents = new LabTestDao().getOrderableReagents(toDate);
		
		if (normalOrder.equalsIgnoreCase("YES"))
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonthEmergency());
		else
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonth());
		List<ReportRequisition> labIssuesList = new ArrayList<>();
		
		try
		{
			deleteLabRandRData(toDate, session);
			List<ReportRequisition> issuedList = session.selectList("LabRequisition.labIssues", map);
			
			for (ReportRequisition r : issuedList)
			{
				r.setEmergencyOrder(normalOrder);
				map.put("productCode", r.getProductCode());
				Integer qtyReceived = session.selectOne("LabRequisition.lab_received", map);
				if (qtyReceived == null)
					r.setTotalReceived(0);
				else 
					r.setTotalReceived(qtyReceived);
				int amc = new AmcCalculator().amcCalculator(r.getProductCode(), r.getQtyDispensed());
				/*if (amc == 0)
				{
					amc = r.getQtyDispensed();
					r.setAmc(amc);
					int maxQty = amc * 3;
					r.setMaxQty(maxQty);
				}
				else
				{*/
					r.setAmc(amc);
					int maxQty = amc * 3;
					r.setMaxQty( maxQty);
				//}
				Integer phyCount = session.selectOne("LabRequisition.phyCount", map);
				Integer beginBal = session.selectOne("LabRequisition.beginBalance", map);
				Integer adjustments = session.selectOne("LabRequisition.adjustments", map);
				if (r.getReportPeriodFrm() == null)
					r.setReportPeriodFrm(fromDate);
				if (r.getReportPeriodTo() == null)
					r.setReportPeriodTo(toDate);
				
				
				if (phyCount == null){
					phyCount = 0;
					r.setPhyCount(phyCount);
					if (nonOderableReagents.contains(r.getProductCode()))
						r.setOrderQty(0);
					else
						r.setOrderQty(r.getMaxQty() - phyCount);
				}
				else{
					r.setPhyCount(phyCount);
					r.setOrderQty(r.getMaxQty() - phyCount);
				}
				if (beginBal == null){
					r.setBeginningBal(0);	
				}
				else{
					r.setBeginningBal(beginBal);
				}
				if (adjustments == null){
					r.setLossAdjustment(0);	
				}
				else{
					r.setLossAdjustment(adjustments);
				}
				if (r.getOrderQty() < 0)
					r.setOrderQty(0);
				r.setCurrentPrice(Math.round(r.getCurrentPrice() * r.getOrderQty()*100.0)/100.0);
				if (remarks.trim().length() != 0)
					r.setRemarks(remarks);
				session.insert("LabRequisition.insertLab",  r);
				labIssuesList.add(r);
			}
			/*if (remarks.trim().length() != 0)
				session.insert("LabRequisition.insertRemark", map);*/
			session.commit();
			return labIssuesList;
		}
		finally
		{
			session.close();
		}
	}
	private void deleteLabRandRData( Date toDate, SqlSession session)
	{
		session.selectList("LabRequisition.del_lab_data", toDate);
		session.delete("LabRequisition.deleteRemark", toDate);
		session.commit();
	}
}
