package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.ProductQtyExample;

public interface artdispenseProductQtyMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_product_qty
     *
     * @mbggenerated Mon Aug 26 11:43:09 CAT 2013
     */
    int countByExample(ProductQtyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_product_qty
     *
     * @mbggenerated Mon Aug 26 11:43:09 CAT 2013
     */
    int deleteByExample(ProductQtyExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_product_qty
     *
     * @mbggenerated Mon Aug 26 11:43:09 CAT 2013
     */
    int insert(ProductQty record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_product_qty
     *
     * @mbggenerated Mon Aug 26 11:43:09 CAT 2013
     */
    int insertSelective(ProductQty record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_product_qty
     *
     * @mbggenerated Mon Aug 26 11:43:09 CAT 2013
     */
    List<ProductQty> selectByExample(ProductQtyExample example);
    
    

    ProductQty getcurrentarvproductqty(@Param("prodcode") String prodcode,@Param("dpid") Integer dpid);
    @SuppressWarnings("rawtypes")
	List  getARVproductAdjustmentsPhysicalcountBalance(@Param("prodcode") String prodcode,@Param("dpid") Integer dpid);
    
    List<ProductQty> getcurrentListproductqty(@Param("prodcode") String prodcode,@Param("dpid")Integer dpid);
     
   int  updateproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Double prodqty,  @Param ("dpid") Integer dpid);
   
   int  updateproductqtybyPC(@Param ("prodcode") String prodcode,@Param ("prodqty") Double prodqty, @Param ("dpid")  Integer dpid);
   
   int  setupdatedispenaryproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Integer prodqty,@Param ("dispensary") String dispensary);
   
   int  updatedispenaryproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Double double1,@Param ("dispensary") String dispensary,@Param ("stockonhand_scc")double d);
   
   int updatedispenaryproductqtyless(@Param ("prodcode") String prodcode,@Param ("prodqty") Double positive,@Param ("dispensary") String dispensary,@Param ("stockonhand_scc")double d,
		   @Param ("dpid") Integer  dpid);
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_product_qty
     *
     * @mbggenerated Mon Aug 26 11:43:09 CAT 2013
     */
    int updateByExampleSelective(@Param("record") ProductQty record, @Param("example") ProductQtyExample example);
    
    int updateelmishivsitebalances(@Param("record") HashMap record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_product_qty
     *
     * @mbggenerated Mon Aug 26 11:43:09 CAT 2013
     */
    int updateByExample(@Param("record") ProductQty record, @Param("example") ProductQtyExample example);
    
    int updateProductQtyBalance(@Param("qtyDeducted") Double qtyDeducted,@Param("productCode") String productCode,@Param("dpid") Integer dispensationUnitId);
}