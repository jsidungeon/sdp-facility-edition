/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.Amc;
import org.elmis.facility.reports.utils.CalendarUtil;

/**
 * @author MMwebaze
 *
 */
public class AmcDao {
	private SqlSessionFactory sqlSessionFactory;
	private Date endRange;
	private CalendarUtil calendarUtil;

	public AmcDao()
	{
		calendarUtil = new CalendarUtil();
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	private boolean checkForRandR(SqlSession session, Map<String, Object> map)
	{
		/*Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		map.put("toDate", toDate);*/

		/*SqlSession session = sqlSessionFactory.openSession();
		try
		{*/
		//List<Amc> randr = session.selectList("Amc.checkforrandrifany", map);
		return session.selectList("Amc.checkforrandrifany", map).isEmpty();
		/*System.out.println(randr.isEmpty());
			for (Amc amc : randr)
				System.out.println(amc.getProductCode()+" --- "+amc.getQtyIssued());*/

		/*}
		finally
		{
			session.close();
		}*/
	}
	public int amc(@Param("productCode") String productCode)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		//map.put("fromDate", prevDate);
		int totalDispensed = 0;
		SqlSession session = sqlSessionFactory.openSession();

		try
		{

			//for (int x = 0; x < 2; x++)
			int x = 0;
			int y = 0;
			while(x < 12)
			{
				Date datum = calendarUtil.getSqlDate(calendarUtil.getPreviousLastDayOfMonth(x));
				map.put("prevDate", datum);
				if (checkForRandR(session, map) == false)
				{
					Integer dispensed = session.selectOne("Amc.amc", map);
					if (dispensed != null && dispensed != 0)
					{
						totalDispensed += dispensed;
						x++;

					}
					else if(dispensed == null || dispensed == 0)
					{
						totalDispensed += 0;
						y++;
						if (y == 12)
							break;
					}
					if (x == 2)
						break;
					//totalDispensed += dispensed;
				}
				else
					break;
			}
			return totalDispensed;
		}
		finally
		{
			session.close();
		}
	}
	public int calculateArvAmc(@Param("productCode") String productCode)
	{
		int x = 3;
		int control = 0;
		int sumOfIssues = 0; //Quantity dispensed 
		int amc = 0;
		endRange = calendarUtil.getSqlDate(calendarUtil.changeDateFormat(calendarUtil.getLastDayOfReportingMonth()));
		Date fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(x)); 

		Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		map.put("fromDate", fromRange);
		map.put("toDate", endRange);

		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Amc> list = session.selectList("amc.arv", map);

			while (list.size() < 3)
			{
				fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(++x));
				map.put("fromDate", fromRange);

				list = session.selectList("amc.arv", map);
				if (list.size() == 0 && control == 5){	

					break;
				}
				else if (list.size() == 1 && control == 5)
				{
					fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
					map.put("fromDate", fromRange);
					list = session.selectList("amc.arv", map);

					for (Amc c: list)
						sumOfIssues += c.getQtyIssued();

					return sumOfIssues;
				}
				else if (list.size() == 2 && control == 5)
				{
					fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
					map.put("fromDate", fromRange);
					list = session.selectList("amc.arv", map);

					for (Amc c: list)
						sumOfIssues = sumOfIssues + c.getQtyIssued();
					System.out.println("Sum issues 2 "+sumOfIssues);
					return sumOfIssues/2;
				}
				control++;
			}

			for (Amc c: list)
				sumOfIssues += c.getQtyIssued();

			amc = sumOfIssues/3;
			return amc;
		} finally {
			session.close();
		}
	}

	public int calculateEmlipAmc(@Param("productCode") String productCode)
	{
		int x = 3;
		int control = 0;
		int sumOfIssues = 0; //Quantity dispensed 
		int amc = 0;
		endRange = calendarUtil.getSqlDate(calendarUtil.changeDateFormat(calendarUtil.getLastDayOfReportingMonth()));
		Date fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(x)); 

		Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		map.put("fromDate", fromRange);
		map.put("toDate", endRange);

		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Amc> list = session.selectList("amc.emlip", map);

			while (list.size() < 3)
			{
				fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(++x));
				map.put("fromDate", fromRange);

				list = session.selectList("amc.emlip", map);
				if (list.size() == 0 && control == 5)
					break;
				else if (list.size() == 1 && control == 5)
				{
					fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
					map.put("fromDate", fromRange);
					list = session.selectList("amc.emlip", map);

					for (Amc c: list)
						sumOfIssues += c.getQtyIssued();

					return sumOfIssues;
				}
				else if (list.size() == 2 && control == 5)
				{
					fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
					map.put("fromDate", fromRange);
					list = session.selectList("amc.emlip", map);

					for (Amc c: list)
						sumOfIssues += c.getQtyIssued();

					return sumOfIssues/2;
				}
				control++;
			}

			for (Amc c: list)
				sumOfIssues += c.getQtyIssued();

			amc = sumOfIssues/3;
			return amc;
		} finally {
			session.close();
		}
	}
	public int calculateHivAmc(@Param("productCode") String productCode)
	{
		int x = 3;
		int control = 0;
		int sumOfIssues = 0; //Quantity dispensed 
		int amc = 0;
		endRange = calendarUtil.getSqlDate(calendarUtil.changeDateFormat(calendarUtil.getLastDayOfReportingMonth()));
		Date fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(x)); 

		Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		map.put("fromDate", fromRange);
		map.put("toDate", endRange);

		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Amc> list = session.selectList("amc.hiv", map);

			while (list.size() < 3)
			{
				fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(++x));
				map.put("fromDate", fromRange);

				list = session.selectList("amc.hiv", map);
				if (list.size() == 0)
					break;
				else if (list.size() == 1 && control == 5)
				{
					fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
					map.put("fromDate", fromRange);
					list = session.selectList("amc.hiv", map);

					for (Amc c: list)
						sumOfIssues += c.getQtyIssued();

					return sumOfIssues;
				}
				else if (list.size() == 2 && control == 5)
				{
					fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
					map.put("fromDate", fromRange);
					list = session.selectList("amc.hiv", map);

					for (Amc c: list)
						sumOfIssues += c.getQtyIssued();

					return sumOfIssues/2;
				}
				control++;
			}

			for (Amc c: list)
				sumOfIssues += c.getQtyIssued();

			amc = sumOfIssues/3;
			return amc;
		} finally {
			session.close();
		}
	}
	public int calculateLabAmc(@Param("productCode") String productCode)
	{
		int x = 3;
		int control = 0;
		int sumOfIssues = 0; //Quantity dispensed 
		int amc = 0;
		endRange = calendarUtil.getSqlDate(calendarUtil.changeDateFormat(calendarUtil.getLastDayOfReportingMonth()));
		Date fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(x)); 

		Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		map.put("fromDate", fromRange);
		map.put("toDate", endRange);

		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Amc> list = session.selectList("amc.lab", map);

			while (list.size() < 3)
			{
				fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(++x));
				map.put("fromDate", fromRange);

				list = session.selectList("amc.lab", map);
				if (list.size() == 0)
					break;
				else if (list.size() == 1 && control == 5)
				{System.out.println("AM HERE");
				fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
				map.put("fromDate", fromRange);
				list = session.selectList("amc.lab", map);

				for (Amc c: list){
					sumOfIssues += c.getQtyIssued();
					System.out.println("Issed "+c.getQtyIssued());
				}

				return sumOfIssues;
				}
				else if (list.size() == 2 && control == 5)
				{
					fromRange = calendarUtil.getSqlDate(calendarUtil.getPreviousMonth(3)); 	
					map.put("fromDate", fromRange);
					list = session.selectList("amc.lab", map);

					for (Amc c: list)
						sumOfIssues += c.getQtyIssued();

					return sumOfIssues/2;
				}
				control++;
			}

			for (Amc c: list)
				sumOfIssues += c.getQtyIssued();

			amc = sumOfIssues/3;
			return amc;
		} finally {
			session.close();
		}
	}
}
