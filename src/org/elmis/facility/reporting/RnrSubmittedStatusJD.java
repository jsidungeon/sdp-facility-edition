package org.elmis.facility.reporting;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.elmis.facility.domain.dao.WebServiceRequestDao;
import org.elmis.facility.domain.model.WebServiceRequest;
import org.elmis.facility.utils.SdpStyleSheet;

public class RnrSubmittedStatusJD extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private String[][] data = null;
	private String[] rnrSubmittedStatusColumns = { "<HTML><b>Program Area</b></html>",
			"<HTML><b>Submission Period</b></html>",
			"<html><b>Status</b></html>"};
	private DefaultTableModel rnrSubmittedStatusModel = new DefaultTableModel(data,rnrSubmittedStatusColumns) {
		@Override
		public boolean isCellEditable(int row, int column) {

			return false;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 4)
				return String.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			RnrSubmittedStatusJD dialog = new RnrSubmittedStatusJD();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public RnrSubmittedStatusJD() {
		setTitle("R&R Submission Status");
		setResizable(false);
		SdpStyleSheet.configJPanelBackground(contentPanel);
		WebServiceRequestDao wsRequestDao = new WebServiceRequestDao();
		List<WebServiceRequest> wsRequestList = wsRequestDao.getWebServiceRequests();
		setBounds(100, 100, 824, 486);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 86, 788, 318);
		contentPanel.add(scrollPane);
		
		table = new JTable();
		SdpStyleSheet.configTable(table);
		table.setModel(rnrSubmittedStatusModel);
		
		for (int i = 0; i < wsRequestList.size(); i++){
			rnrSubmittedStatusModel.addRow(new String[wsRequestList.size()]);
			table.getModel().setValueAt(wsRequestList.get(i).getProgramArea(), i, 0);
			table.getModel().setValueAt(wsRequestList.get(i).getReportingPeriod(), i, 1);
			table.getModel().setValueAt(wsRequestList.get(i).getRequestStatus(), i, 2);
		}
		table.setFillsViewportHeight(true);
		scrollPane.setViewportView(table);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		setLocationRelativeTo(null);
	}
}
