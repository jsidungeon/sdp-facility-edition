/**
 * 
 */
package org.elmis.facility.network;

/**
 * @author JBanda
 *
 */
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.utils.ElmisAESencrpDecrp;

public class MyBatisConnectionFactory {

	private static SqlSessionFactory sqlSessionFactory;

	static {

		try {

			String resource = "SqlMapConfig.xml";
			
			Reader reader = Resources.getResourceAsReader(resource);
			
			if (sqlSessionFactory == null) {
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, System.getProperties());//.build(reader);
			
			}
			
		}

		catch (FileNotFoundException fileNotFoundException) {

			fileNotFoundException.printStackTrace();

		}

		catch (IOException iOException) {

			iOException.printStackTrace();
			
			javax.swing.JOptionPane.showMessageDialog(null, "message");

		}
catch(Exception e){
	System.out.println(e.getLocalizedMessage());
}
	}

	public static SqlSessionFactory getSqlSessionFactory() {

		return sqlSessionFactory;

	}

}
