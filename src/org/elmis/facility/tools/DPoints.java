package org.elmis.facility.tools;

public class DPoints {
	private Integer id;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getSite_id() {
		return site_id;
	}
	public void setSite_id(Integer siteId) {
		site_id = siteId;
	}
	public String getDispensing_point_name() {
		return dispensing_point_name;
	}
	public void setDispensing_point_name(String dispensingPointName) {
		dispensing_point_name = dispensingPointName;
	}
	public String getProgram_area() {
		return program_area;
	}
	public void setProgram_area(String programArea) {
		program_area = programArea;
	}
	private Integer site_id;
	private String dispensing_point_name;
	private String program_area;
}
