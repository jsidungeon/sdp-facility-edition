/*
 * StartJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JDialog;

import org.elmis.forms.hivtest.RecordTestJD;
//import org.elmis.forms.hivtest.HivTestingJD;

/**
 * 
 * @author __USER__
 */
public class StartJP extends javax.swing.JPanel implements ActionListener {

	/** Creates new form StartJP */
	public StartJP(List<String> userRightsList) {
		initComponents();

		// organiserJL.setVisible(false);

		hivTestingJL.setVisible(false);
		dispenseJL.setVisible(false);

		for (String userRights : userRightsList) {

			//hiv Testing
			if (this.hivTestingJL.getName().equalsIgnoreCase(userRights.trim())) {
				hivTestingJL.setVisible(true);
			}
		}

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		hivTestingJL = new javax.swing.JLabel();
		dispenseJL = new javax.swing.JLabel();

		setBackground(new java.awt.Color(255, 255, 255));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		hivTestingJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		hivTestingJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/system_setup_55.png"))); // NOI18N
		hivTestingJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("HIV Testing"));
		hivTestingJL
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		hivTestingJL.setName("HIV_TESTING");
		hivTestingJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				hivTestingJLMouseClicked(evt);
			}
		});

		dispenseJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		dispenseJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/income_reports_55.png"))); // NOI18N
		dispenseJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Dispensing"));
		dispenseJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				dispenseJLMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						hivTestingJL, javax.swing.GroupLayout.PREFERRED_SIZE,
						102, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18,
						18, 18).addComponent(dispenseJL,
						javax.swing.GroupLayout.PREFERRED_SIZE, 96,
						javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(181, Short.MAX_VALUE)));

		layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { dispenseJL, hivTestingJL });

		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																dispenseJL,
																javax.swing.GroupLayout.Alignment.LEADING,
																0, 0,
																Short.MAX_VALUE)
														.addComponent(
																hivTestingJL,
																javax.swing.GroupLayout.Alignment.LEADING))
										.addContainerGap()));

		layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { dispenseJL, hivTestingJL });

	}// </editor-fold>
	//GEN-END:initComponents

	private void dispenseJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//AppJFrame.glassPane.activate(null);
		//new HivTestingJD(javax.swing.JOptionPane.getFrameForComponent(this),
		//		true);

		AppJFrame.glassPane.deactivate();
	}

	private void hivTestingJLMouseClicked(java.awt.event.MouseEvent evt) {
		System.out.println("****** &&& ");
		// javax.swing.JOptionPane.showMessageDialog(null, "message");
		// /MyBatisTest testApp = new MyBatisTest();
		AppJFrame.glassPane.activate(null);
		RecordTestJD recordTestJD = new RecordTestJD(javax.swing.JOptionPane.getFrameForComponent(this),true);
		recordTestJD.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		AppJFrame.glassPane.deactivate();

	}

	/********* Action Listener Coding Starts *************/
	// this action Listener for phone book
	//
	public void actionPerformed(ActionEvent evt) {

		// if (evt.getActionCommand().equalsIgnoreCase("a")) {

		// 888888888888888 use this for getting what letter was clicked and
		// getting the letter for the query
		// javax.swing.JOptionPane
		// .showMessageDialog(
		// null,
		// "Sorry this feature is not available for LegalManager Basic Edition \nUpgrade to activate this feature");

		// javax.swing.JOptionPane.showMessageDialog(null,
		// evt.getActionCommand()
		// .toString());
		// }
		AppJFrame.glassPane.activate(null);
		//new ContactAdminJD(javax.swing.JOptionPane.getFrameForComponent(this),
		//		true, evt.getActionCommand().toString());
		AppJFrame.glassPane.deactivate();

		// if (evt.getActionCommand().equalsIgnoreCase("b")) {
		// javax.swing.JOptionPane.showMessageDialog(null, "b");
		// }
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel dispenseJL;
	private javax.swing.JLabel hivTestingJL;
	// End of variables declaration//GEN-END:variables

}