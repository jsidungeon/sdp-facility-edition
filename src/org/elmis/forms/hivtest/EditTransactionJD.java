package org.elmis.forms.hivtest;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.elmis.facility.domain.dao.HivTestResultDao;
import org.elmis.facility.domain.model.HivTestResult;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.SdpStyleSheet;

public class EditTransactionJD extends JDialog {
	private JTextField soughtTestIdTxtfield;
	private JComboBox purposeCombobox;
	private JComboBox screeningCombobox;
	private JComboBox confirmatoryCombobox;
	private JButton btnSave;
	private HivTestResult hivTestResult;
	private JTextField testIdTxtfield;
	private JTextField finalResultTxtfield;
	private String purposes[] = {"VCT", "PMTCT", "CD", "QC", "Other"};
	private String results[] = {"R", "NR", "IN"};
	private int testId;
	private int screeningAdj = 0;
	private int confirmatoryAdj = 0;
	private int siteId; 
	private boolean deleteConfirmatoryTest = false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Properties prop = new Properties(System.getProperties());
					FileInputStream fis = null;
					try {
						fis = new FileInputStream("Programmproperties.properties");
						prop.load(fis);

						System.setProperties(prop);

						fis.close();

					} catch (IOException e) {
						e.printStackTrace();
					}
					String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
					System.setProperty("dbpassword", dbpassword);
					EditTransactionJD dialog = new EditTransactionJD(new javax.swing.JFrame(), true);
					dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
					dialog.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the dialog.
	 */
	public EditTransactionJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setResizable(false);
		setBounds(100, 100, 450, 300);
		
		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		SdpStyleSheet.configJPanelBackground(panel_1);
		panel_1.setBounds(0, 65, 434, 197);
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JButton btnClose = new JButton("Close");
		btnClose.setBounds(174, 163, 89, 23);
		panel_1.add(btnClose);
		
		btnSave = new JButton("Save Changes");
		btnSave.setEnabled(false);
		btnSave.setBounds(273, 163, 151, 23);
		panel_1.add(btnSave);
		
		JLabel lblFinalTestResult = new JLabel("Final Test Result:");
		SdpStyleSheet.configOtherJLabel(lblFinalTestResult);
		lblFinalTestResult.setBounds(10, 128, 153, 14);
		panel_1.add(lblFinalTestResult);
		
		JLabel lblConfirmatoryResult = new JLabel("Confirmatory Result:");
		SdpStyleSheet.configOtherJLabel(lblConfirmatoryResult);
		lblConfirmatoryResult.setBounds(10, 100, 153, 14);
		panel_1.add(lblConfirmatoryResult);
		
		JLabel lblScreeningResult = new JLabel("Screening Result");
		SdpStyleSheet.configOtherJLabel(lblScreeningResult);
		lblScreeningResult.setBounds(10, 72, 153, 14);
		panel_1.add(lblScreeningResult);
		
		JLabel lblPurpose = new JLabel("Purpose:");
		SdpStyleSheet.configOtherJLabel(lblPurpose);
		lblPurpose.setBounds(10, 44, 116, 14);
		panel_1.add(lblPurpose);
		
		JLabel lblTestId = new JLabel("Test ID:");
		SdpStyleSheet.configOtherJLabel(lblTestId);
		lblTestId.setBounds(10, 14, 75, 14);
		panel_1.add(lblTestId);
		
		testIdTxtfield = new JTextField();
		testIdTxtfield.setEditable(false);
		testIdTxtfield.setBounds(173, 14, 90, 20);
		panel_1.add(testIdTxtfield);
		testIdTxtfield.setColumns(10);
		
		purposeCombobox = new JComboBox(purposes);
		purposeCombobox.setEnabled(false);
		purposeCombobox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				//System.out.println((state == ItemEvent.SELECTED) ? "Selected" : "Deselected");
				if (state == ItemEvent.SELECTED){
					if ((""+purposeCombobox.getSelectedItem()).equalsIgnoreCase("QC"))
						finalResultTxtfield.setText("");
					else{
						String screeningResult = ""+screeningCombobox.getSelectedItem();
						String confirmatoryResult = ""+confirmatoryCombobox.getSelectedItem();
						if (screeningResult.equalsIgnoreCase("R") && confirmatoryResult.equalsIgnoreCase("R"))
							finalResultTxtfield.setText("P");
						else if (screeningResult.equalsIgnoreCase("NR"))
							finalResultTxtfield.setText("N");
						else /*if (screeningResult.equalsIgnoreCase("R") && confirmatoryResult.equalsIgnoreCase("NR"))*/
							finalResultTxtfield.setText("I");
					}
				}
			}
		});
		purposeCombobox.setEditable(true);
		purposeCombobox.setBounds(173, 44, 90, 20);
		panel_1.add(purposeCombobox);
		
		screeningCombobox = new JComboBox(results);
		screeningCombobox.setEnabled(false);
		screeningCombobox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				//System.out.println((state == ItemEvent.SELECTED) ? "Selected" : "Deselected");
				//System.out.println("Item: " + itemEvent.getItem());
				//System.out.println("Get selected item : "+purposeCombobox.getSelectedItem());
				if (state == ItemEvent.SELECTED){
					if ((""+purposeCombobox.getSelectedItem()).equalsIgnoreCase("QC"))
						finalResultTxtfield.setText("");
					else{
						String screeningResult = ""+screeningCombobox.getSelectedItem();
						String confirmatoryResult = ""+confirmatoryCombobox.getSelectedItem();
						if (screeningResult.equalsIgnoreCase("R") && confirmatoryResult.equalsIgnoreCase("R"))
							finalResultTxtfield.setText("P");
						else if (screeningResult.equalsIgnoreCase("NR")){
							finalResultTxtfield.setText("N");
							confirmatoryCombobox.setSelectedItem("");
							confirmatoryCombobox.setEnabled(false);
							confirmatoryAdj = 1;
							deleteConfirmatoryTest = true;
						}
						else if (screeningResult.equalsIgnoreCase("R") && hivTestResult.getConfirmatoryResult() == null){

							ConfirmatoryTestSelectorJD confirmTestSelector = new ConfirmatoryTestSelectorJD(EditTransactionJD.this, true);
							confirmTestSelector.setVisible(true);
							if (confirmTestSelector.isTestResultSelected())
								confirmatoryCombobox.setSelectedItem(confirmTestSelector.getSelectedTestResult());
							else {
								screeningCombobox.setSelectedItem("NR");
								confirmatoryCombobox.setSelectedItem(null);
							}
							confirmatoryCombobox.setEnabled(true);
						}
						else /*if (screeningResult.equalsIgnoreCase("R") && confirmatoryResult.equalsIgnoreCase("NR"))*/
							finalResultTxtfield.setText("I");
					}
				}
			}
		});
		screeningCombobox.setEditable(true);
		screeningCombobox.setBounds(173, 72, 64, 20);
		panel_1.add(screeningCombobox);
		
		confirmatoryCombobox = new JComboBox(results);
		confirmatoryCombobox.setEnabled(false);
		confirmatoryCombobox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent itemEvent) {
				int state = itemEvent.getStateChange();
				if (state == ItemEvent.SELECTED){
					if ((""+purposeCombobox.getSelectedItem()).equalsIgnoreCase("QC"))
						finalResultTxtfield.setText("");
					else{
						String screeningResult = ""+screeningCombobox.getSelectedItem();
						String confirmatoryResult = ""+confirmatoryCombobox.getSelectedItem();
						/*System.out.println("Comfirmatory listened...."+confirmatoryResult);
						System.out.println("Screening listened...."+screeningResult);*/
						if (screeningResult.equalsIgnoreCase("R") && confirmatoryResult.equalsIgnoreCase("R")){
							finalResultTxtfield.setText("P");
						}
						else if (screeningResult.equalsIgnoreCase("NR")){
							finalResultTxtfield.setText("N");
							//confirmatoryCombobox.setSelectedItem("");
							//confirmatoryCombobox.setEnabled(false);
							//confirmatoryAdj = 1;
						}
						else /*if (screeningResult.equalsIgnoreCase("R") && confirmatoryResult.equalsIgnoreCase("NR"))*/
							finalResultTxtfield.setText("I");
					}
				}
			}
		});
		confirmatoryCombobox.setEditable(true);
		confirmatoryCombobox.setBounds(173, 100, 64, 20);
		panel_1.add(confirmatoryCombobox);
		
		finalResultTxtfield = new JTextField();
		finalResultTxtfield.setText("P");
		finalResultTxtfield.setEditable(false);
		finalResultTxtfield.setBounds(173, 128, 44, 20);
		panel_1.add(finalResultTxtfield);
		finalResultTxtfield.setColumns(10);
		
		JPanel panel_2 = new JPanel();
		SdpStyleSheet.configJPanelBackground(panel_2);
		panel_2.setBounds(0, 0, 434, 63);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JLabel lblEnterSampleId = new JLabel("Enter Sample Id:");
		SdpStyleSheet.configOtherJLabel(lblEnterSampleId);
		lblEnterSampleId.setBounds(10, 11, 144, 14);
		panel_2.add(lblEnterSampleId);
		
		JButton btnSearchForSample = new JButton("Search for sample");
		btnSearchForSample.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
				testId = Integer.parseInt(soughtTestIdTxtfield.getText());
				searchForSample(testId, /*AppJFrame.getDispensingId("HIV")*/2);
				}
				catch(Exception ex){
					System.out.println(ex.getMessage());
				}
				
			}
		});
		btnSearchForSample.setBounds(250, 28, 174, 23);
		panel_2.add(btnSearchForSample);
		
		soughtTestIdTxtfield = new JTextField();
		soughtTestIdTxtfield.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					int testId = Integer.parseInt(soughtTestIdTxtfield.getText());
					searchForSample(testId, /*AppJFrame.getDispensingId("HIV")*/2);
				}
				catch(Exception ex){
					System.out.println(ex.getMessage());
				}
			}
		});
		soughtTestIdTxtfield.setBounds(10, 29, 144, 23);
		panel_2.add(soughtTestIdTxtfield);
		soughtTestIdTxtfield.setColumns(10);
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (purposeCombobox.getSelectedItem().toString().equalsIgnoreCase("QC")){
					//testResult.setFinalResult("");
				}
				else{
					HivTestResult testResult = new HivTestResult();
					testResult.setPurpose(purposeCombobox.getSelectedItem()+"");
					testResult.setSiteId(siteId);
					testResult.setTestId(hivTestResult.getTestId());
					testResult.setScreeningResult(""+screeningCombobox.getSelectedItem());
					if (screeningCombobox.getSelectedItem().toString().equalsIgnoreCase("NR"))
						testResult.setConfirmatoryResult("");
					else
						testResult.setConfirmatoryResult(confirmatoryCombobox.getSelectedItem()+"");
					testResult.setFinalResult(finalResultTxtfield.getText().charAt(0));
					new HivTestResultDao().updateTransaction(testResult, screeningAdj, confirmatoryAdj, deleteConfirmatoryTest);
					dispose();
				}
			}
		});
		btnClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		setLocationRelativeTo(parent);

	}

	private void searchForSample(int testId, int dispensaryId) {
		 HivTestResultDao hivTestDao = new HivTestResultDao();
		List<HivTestResult> hivTestResultList = hivTestDao.getHivTestResult(testId, dispensaryId);
		if (!hivTestResultList.isEmpty()){
			hivTestResult = hivTestResultList.get(0);
			siteId = hivTestResult.getSiteId();
			testId = hivTestResult.getTestId();
			purposeCombobox.setEnabled(true);
			screeningCombobox.setEnabled(true);
			confirmatoryCombobox.setEnabled(true);
			btnSave.setEnabled(true);
			testIdTxtfield.setEnabled(true);
			testIdTxtfield.setText(""+hivTestResult.getTestId());
			purposeCombobox.setSelectedItem(hivTestResult.getPurpose());
			screeningCombobox.setSelectedItem(hivTestResult.getScreeningResult());
			confirmatoryCombobox.setSelectedItem(hivTestResult.getConfirmatoryResult());
			System.out.println("***"+hivTestResult.getConfirmatoryResult());
			finalResultTxtfield.setText(hivTestResult.getFinalResult()+"");
		}
		else{
			JOptionPane.showMessageDialog(this, "No Test Sample with ID : "+testId+" available");
			disableForm();
		}
	}
	private void disableForm(){
		purposeCombobox.setEnabled(false);
		screeningCombobox.setEnabled(false);
		confirmatoryCombobox.setEnabled(false);
		testIdTxtfield.setEnabled(false);
		btnSave.setEnabled(false);
		testIdTxtfield.setText("");
		hivTestResult = null;
	}
}
