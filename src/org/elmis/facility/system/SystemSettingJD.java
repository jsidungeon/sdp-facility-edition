/*
 * SystemSettingJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.JComboBox;
import javax.swing.MutableComboBoxModel;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.dao.ProductsDAO;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Operators;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Geographic_Zones;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Program_products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Roles;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.utils.SdpStyleSheet;

import com.oribicom.tools.TableModel;

import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 *
 * @author  __USER__
 */
public class SystemSettingJD extends javax.swing.JDialog {

	//Programs JTable **************************************************
	private static final String[] columns_programs = { "Program name",
			"Active", "Start Date", "Program Code", "Row" };
	private static final Object[] defaultv_programs = { "", "", "", "", "" };
	private static final int rows_programs = 0;
	public static TableModel tableModel_programs = new TableModel(
			columns_programs, defaultv_programs, rows_programs);
	public static int total_programs = 0;
	public static Map parameterMap_programs = new HashMap();
	private static ListIterator<Programs> programsIterator;
	List<Roles> programsList = new LinkedList();
	private Programs programes;

	//*******************************************************************

	//Products JTable **************************************************
	private static final String[] columns_products = { "Program", "Active",
			"Start Date", "End Date" };
	private static final Object[] defaultv_products = { "", "", "", "", "", "",
			"" };
	private static final int rows_products = 0;
	public static TableModel tableModel_products = new TableModel(
			columns_programs, defaultv_programs, rows_programs);
	public static int total_products = 0;
	public static Map parameterMap_products = new HashMap();
	private static ListIterator<Products> productsIterator;
	List<Products> productsList = new LinkedList();
	private Products products;

	//*******************************************************************

	//Create tableModel for Program Products for each facility 
	private static final String[] columns_programproducts = { "Product name",
			"Product Code", "Dispensing Unit", "Pack Size",
			"Is a Controlled Substance" };
	private static final Object[] defaultv_prgramproducts = { "", "", "", "",
			"" };
	private static final int rows_programproducts = 0;
	public static TableModel tableModel_programproducts = new TableModel(
			columns_programproducts, defaultv_prgramproducts,
			rows_programproducts);

	private static Program_products program_products;
	private static ListIterator<Program_products> programproductiterator;
	//**************************************************************************

	//****************************************************************************
	// Create facility object
	FacilitySetUpDAO callfacility;
	Facility facility = null;
	Facility_Types facilitytype = null;
	Facility_Operators facilityop = null;
	Geographic_Zones facilitygeozone = null;
	public String selectedfacilitytypeCode;
	private JComboBox facilityOpsList = new JComboBox();
	MutableComboBoxModel model = (MutableComboBoxModel) facilityOpsList
			.getModel();

	private JComboBox facilityTypeList = new JComboBox();
	MutableComboBoxModel typemodel = (MutableComboBoxModel) facilityTypeList
			.getModel();

	private JComboBox facilitygeozoneList = new JComboBox();
	MutableComboBoxModel geozonemodel = (MutableComboBoxModel) facilitygeozoneList
			.getModel();

	//****************************************************************************
	private int selectTabIndex = 0;
	public static String selectedfacilitytypecode;

	/** Creates new form SystemSettingJD */
	public SystemSettingJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();

		backJBtn.setVisible(false);
		//this.setSize(810, 620);
		this.setLocationRelativeTo(null);

		this.setVisible(true);

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		buttonGroup1 = new javax.swing.ButtonGroup();
		SDP = new javax.swing.ButtonGroup();
		ActiveFacility = new javax.swing.ButtonGroup();
		Electricity = new javax.swing.ButtonGroup();
		InterNet = new javax.swing.ButtonGroup();
		SCC = new javax.swing.ButtonGroup();
		DAR = new javax.swing.ButtonGroup();
		DATA_Reportable = new javax.swing.ButtonGroup();
		SuppliesOthers = new javax.swing.ButtonGroup();
		jPanel5 = new javax.swing.JPanel();
		jTabbedPane1 = new javax.swing.JTabbedPane();
		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		jLabel14 = new javax.swing.JLabel();
		FacilitydescriptJT = new javax.swing.JTextField();
		jSeparator4 = new javax.swing.JSeparator();
		GolivedateJDate = new com.toedter.calendar.JDateChooser();
		GodowndateJDate = new com.toedter.calendar.JDateChooser();
		jLabel27 = new javax.swing.JLabel();
		jLabel28 = new javax.swing.JLabel();
		jLabel29 = new javax.swing.JLabel();
		jSeparator5 = new javax.swing.JSeparator();
		jLabel9 = new javax.swing.JLabel();
		jLabel10 = new javax.swing.JLabel();
		facilityCodeJTF = new javax.swing.JTextField();
		glnJT = new javax.swing.JTextField();
		jSeparator1 = new javax.swing.JSeparator();
		jLabel11 = new javax.swing.JLabel();
		jLabel12 = new javax.swing.JLabel();
		facilitynameJT = new javax.swing.JTextField();
		jLabel13 = new javax.swing.JLabel();
		jLabel30 = new javax.swing.JLabel();
		FacilityTypeJCB = new javax.swing.JComboBox();
		jSeparator6 = new javax.swing.JSeparator();
		jLabel31 = new javax.swing.JLabel();
		OperatedbyCB = new javax.swing.JComboBox();
		jSeparator7 = new javax.swing.JSeparator();
		jLabel32 = new javax.swing.JLabel();
		ServicedeliverypointYesJRB = new javax.swing.JRadioButton();
		ServicedeliverpointNoJRB = new javax.swing.JRadioButton();
		jLabel33 = new javax.swing.JLabel();
		ActivefacilityYesJRB = new javax.swing.JRadioButton();
		ActivefacilityNoJRB = new javax.swing.JRadioButton();
		jLabel34 = new javax.swing.JLabel();
		jPanel6 = new javax.swing.JPanel();
		jLabel17 = new javax.swing.JLabel();
		jLabel18 = new javax.swing.JLabel();
		AddressLine1JT = new javax.swing.JTextField();
		AddressLine2JT = new javax.swing.JTextField();
		jLabel15 = new javax.swing.JLabel();
		jLabel16 = new javax.swing.JLabel();
		PhoneJT = new javax.swing.JTextField();
		jLabel19 = new javax.swing.JLabel();
		FaxJT = new javax.swing.JTextField();
		jLabel20 = new javax.swing.JLabel();
		EmailAddressJT = new javax.swing.JTextField();
		jPanel2 = new javax.swing.JPanel();
		jLabel2 = new javax.swing.JLabel();
		viewProgramJL = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jScrollPane2 = new javax.swing.JScrollPane();
		programsJTable = new javax.swing.JTable();
		jLabel4 = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		programJTable = new javax.swing.JTable();
		jPanel3 = new javax.swing.JPanel();
		jLabel21 = new javax.swing.JLabel();
		jLabel7 = new javax.swing.JLabel();
		jLabel8 = new javax.swing.JLabel();
		jLabel22 = new javax.swing.JLabel();
		jLabel23 = new javax.swing.JLabel();
		geozoneJCB = new javax.swing.JComboBox();
		catchmentpopJT = new javax.swing.JTextField();
		jSeparator2 = new javax.swing.JSeparator();
		jLabel24 = new javax.swing.JLabel();
		latitudeJT = new javax.swing.JTextField();
		jLabel25 = new javax.swing.JLabel();
		longitudeJT = new javax.swing.JTextField();
		jSeparator3 = new javax.swing.JSeparator();
		jLabel26 = new javax.swing.JLabel();
		altitudeJT = new javax.swing.JTextField();
		jPanel4 = new javax.swing.JPanel();
		jLabel35 = new javax.swing.JLabel();
		jLabel36 = new javax.swing.JLabel();
		coldstoragegrosscapacityJRB = new javax.swing.JTextField();
		jSeparator8 = new javax.swing.JSeparator();
		jLabel37 = new javax.swing.JLabel();
		coldstoragenetcapacityJRB = new javax.swing.JTextField();
		jSeparator9 = new javax.swing.JSeparator();
		jLabel38 = new javax.swing.JLabel();
		electricityYesJRB = new javax.swing.JRadioButton();
		electricityNOJRB = new javax.swing.JRadioButton();
		jLabel39 = new javax.swing.JLabel();
		internetYesJRB = new javax.swing.JRadioButton();
		internetNoJRB = new javax.swing.JRadioButton();
		jSeparator10 = new javax.swing.JSeparator();
		jLabel40 = new javax.swing.JLabel();
		jLabel41 = new javax.swing.JLabel();
		electronicsccYesJRB = new javax.swing.JRadioButton();
		electronicsccNoJRB = new javax.swing.JRadioButton();
		electronicdarYesJRB = new javax.swing.JRadioButton();
		electronicdarNoJRB = new javax.swing.JRadioButton();
		jSeparator11 = new javax.swing.JSeparator();
		jLabel42 = new javax.swing.JLabel();
		jLabel43 = new javax.swing.JLabel();
		datareportableYesJRB = new javax.swing.JRadioButton();
		datareportableNoJRB = new javax.swing.JRadioButton();
		suppliesotherYesJRB = new javax.swing.JRadioButton();
		suppliesothersNoJRB = new javax.swing.JRadioButton();
		jSeparator12 = new javax.swing.JSeparator();
		jLabel44 = new javax.swing.JLabel();
		commentsJRB = new javax.swing.JTextField();
		backJBtn = new javax.swing.JButton();
		nextJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("View Facility Details");
		setLocationByPlatform(true);
		setResizable(false);
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowActivated(java.awt.event.WindowEvent evt) {
				formWindowActivated(evt);
			}

			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}

			public void windowClosing(java.awt.event.WindowEvent evt) {
				formWindowClosing(evt);
			}
		});

		jPanel5.setBackground(new java.awt.Color(102, 102, 102));

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});

		jPanel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		SdpStyleSheet.configJPanelBackground(jPanel1);
		SdpStyleSheet.configJPanelBackground(jPanel6);
		SdpStyleSheet.configJPanelBackground(jPanel2);
		SdpStyleSheet.configJPanelBackground(jPanel4);
		jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Facility long.png"))); // NOI18N
		jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		//jLabel3.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel3.setText("Facility Code");
		SdpStyleSheet.configOtherJLabel(jLabel3);
		jLabel14.setText("Facility Description");
		SdpStyleSheet.configOtherJLabel(jLabel14);

		FacilitydescriptJT.setEnabled(false);

		GolivedateJDate.setEnabled(false);

		GodowndateJDate.setEnabled(false);

		//jLabel27.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel27.setText("Go live date");
		SdpStyleSheet.configOtherJLabel(jLabel27);

		jLabel28.setForeground(new java.awt.Color(255, 0, 0));
		jLabel28.setText("*");

		//jLabel29.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel29.setText("Go down date");
		SdpStyleSheet.configOtherJLabel(jLabel29);

		jLabel9.setForeground(new java.awt.Color(255, 0, 0));
		jLabel9.setText("*");

		//jLabel10.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel10.setText("GLN");
		SdpStyleSheet.configOtherJLabel(jLabel10);

		facilityCodeJTF.setEnabled(false);

		glnJT.setEnabled(false);

		//jLabel11.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel11.setText("Facility name");
		SdpStyleSheet.configOtherJLabel(jLabel11);

		jLabel12.setForeground(new java.awt.Color(255, 0, 0));
		jLabel12.setText("*");

		facilitynameJT.setEnabled(false);

		//jLabel13.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel13.setText("Facility type");
		SdpStyleSheet.configOtherJLabel(jLabel13);

		jLabel30.setForeground(new java.awt.Color(255, 0, 0));
		jLabel30.setText("*");

		FacilityTypeJCB.setModel(model);

		//jLabel31.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel31.setText("Operated by");
		SdpStyleSheet.configOtherJLabel(jLabel31);

		OperatedbyCB.setModel(typemodel);

		//jLabel32.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel32.setText("Service delivery point?");
		SdpStyleSheet.configOtherJLabel(jLabel32);

		SDP.add(ServicedeliverypointYesJRB);
		ServicedeliverypointYesJRB.setFont(new java.awt.Font("Ebrima", 1, 12));
		ServicedeliverypointYesJRB.setText("Yes");

		SDP.add(ServicedeliverpointNoJRB);
		ServicedeliverpointNoJRB.setFont(new java.awt.Font("Ebrima", 1, 12));
		ServicedeliverpointNoJRB.setText("No");

		//jLabel33.setFont(new java.awt.Font("Ebrima", 0, 12));
		jLabel33.setText("Active facility?");
		SdpStyleSheet.configOtherJLabel(jLabel33);

		ActiveFacility.add(ActivefacilityYesJRB);
		ActivefacilityYesJRB.setFont(new java.awt.Font("Ebrima", 1, 12));
		ActivefacilityYesJRB.setText("Yes");

		ActiveFacility.add(ActivefacilityNoJRB);
		ActivefacilityNoJRB.setFont(new java.awt.Font("Ebrima", 1, 12));
		ActivefacilityNoJRB.setText("No");

		jLabel34.setForeground(new java.awt.Color(255, 0, 0));
		jLabel34.setText("*");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												94,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(39, 39, 39)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				ServicedeliverypointYesJRB)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				ServicedeliverpointNoJRB)
																		.addGap(
																				558,
																				558,
																				558))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jSeparator4,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				628,
																				Short.MAX_VALUE)
																		.addContainerGap(
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel14)
																		.addContainerGap(
																				518,
																				Short.MAX_VALUE))
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																jPanel1Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								jSeparator1,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								526,
																								Short.MAX_VALUE)
																						.addComponent(
																								FacilitydescriptJT,
																								javax.swing.GroupLayout.Alignment.LEADING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								526,
																								Short.MAX_VALUE)
																						.addGroup(
																								jPanel1Layout
																										.createSequentialGroup()
																										.addGroup(
																												jPanel1Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.LEADING)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				jLabel3)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																		.addComponent(
																																				jLabel9))
																														.addComponent(
																																facilityCodeJTF,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																226,
																																javax.swing.GroupLayout.PREFERRED_SIZE)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				jLabel11)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																		.addComponent(
																																				jLabel12))
																														.addComponent(
																																facilitynameJT,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																226,
																																javax.swing.GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																jLabel31)
																														.addComponent(
																																OperatedbyCB,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																225,
																																javax.swing.GroupLayout.PREFERRED_SIZE)
																														.addComponent(
																																jLabel32)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				jLabel27)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																		.addComponent(
																																				jLabel28))
																														.addComponent(
																																GolivedateJDate,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																222,
																																javax.swing.GroupLayout.PREFERRED_SIZE))
																										.addPreferredGap(
																												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																												26,
																												Short.MAX_VALUE)
																										.addGroup(
																												jPanel1Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.LEADING)
																														.addComponent(
																																jLabel10)
																														.addComponent(
																																glnJT,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																250,
																																javax.swing.GroupLayout.PREFERRED_SIZE)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				jLabel13)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																																		.addComponent(
																																				jLabel30))
																														.addComponent(
																																FacilityTypeJCB,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																240,
																																javax.swing.GroupLayout.PREFERRED_SIZE)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				jLabel33)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																		.addComponent(
																																				jLabel34))
																														.addComponent(
																																jLabel29)
																														.addComponent(
																																GodowndateJDate,
																																javax.swing.GroupLayout.PREFERRED_SIZE,
																																207,
																																javax.swing.GroupLayout.PREFERRED_SIZE)
																														.addGroup(
																																jPanel1Layout
																																		.createSequentialGroup()
																																		.addComponent(
																																				ActivefacilityYesJRB)
																																		.addPreferredGap(
																																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																																		.addComponent(
																																				ActivefacilityNoJRB))))
																						.addComponent(
																								jSeparator6,
																								javax.swing.GroupLayout.Alignment.LEADING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								526,
																								Short.MAX_VALUE)
																						.addComponent(
																								jSeparator7,
																								javax.swing.GroupLayout.Alignment.LEADING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								526,
																								Short.MAX_VALUE)
																						.addComponent(
																								jSeparator5,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								526,
																								Short.MAX_VALUE))
																		.addGap(
																				122,
																				122,
																				122)))));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { FacilityTypeJCB, GodowndateJDate,
						GolivedateJDate, OperatedbyCB, facilityCodeJTF,
						facilitynameJT, glnJT });

		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel1,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				540,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel3)
																						.addComponent(
																								jLabel9,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								14,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel10))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								facilityCodeJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								glnJT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(
																				15,
																				15,
																				15)
																		.addComponent(
																				jSeparator1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				13,
																				13,
																				13)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel11)
																						.addComponent(
																								jLabel13)
																						.addComponent(
																								jLabel12)
																						.addComponent(
																								jLabel30))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								facilitynameJT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								FacilityTypeJCB,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				jSeparator6,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				10,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				jLabel31)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				OperatedbyCB,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				jSeparator7,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				10,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel32)
																						.addComponent(
																								jLabel33)
																						.addComponent(
																								jLabel34))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																				21,
																				Short.MAX_VALUE)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								ServicedeliverypointYesJRB)
																						.addComponent(
																								ServicedeliverpointNoJRB)
																						.addComponent(
																								ActivefacilityYesJRB)
																						.addComponent(
																								ActivefacilityNoJRB))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				jSeparator5,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				10,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addGroup(
																								jPanel1Layout
																										.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.BASELINE)
																										.addComponent(
																												jLabel27)
																										.addComponent(
																												jLabel28))
																						.addComponent(
																								jLabel29))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								GodowndateJDate,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								GolivedateJDate,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				jSeparator4,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				11,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																				87,
																				Short.MAX_VALUE)
																		.addComponent(
																				jLabel14)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				FacilitydescriptJT,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				39,
																				39,
																				39)))));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { FacilityTypeJCB, GodowndateJDate,
						GolivedateJDate, OperatedbyCB, facilityCodeJTF,
						facilitynameJT, glnJT });

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { jLabel14, jLabel3 });

		jTabbedPane1.addTab("Basic Information", new javax.swing.ImageIcon(
				getClass().getResource(
						"/elmis_images/eLMIS basic info small.png")), jPanel1); // NOI18N

		jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS basic info icon.png"))); // NOI18N

		//jLabel18.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel18.setText("Address Line 1");
		SdpStyleSheet.configOtherJLabel(jLabel18);

		AddressLine1JT.setEnabled(false);

		AddressLine2JT.setEnabled(false);

		//jLabel15.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel15.setText("Address Line 2");
		SdpStyleSheet.configOtherJLabel(jLabel15);

		//jLabel16.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel16.setText("Phone");
		SdpStyleSheet.configOtherJLabel(jLabel16);

		PhoneJT.setEnabled(false);

		//jLabel19.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel19.setText("Fax");
		SdpStyleSheet.configOtherJLabel(jLabel19);

		FaxJT.setEnabled(false);

		//jLabel20.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel20.setText("Email Address");
		SdpStyleSheet.configOtherJLabel(jLabel20);

		EmailAddressJT.setEnabled(false);

		javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(
				jPanel6);
		jPanel6.setLayout(jPanel6Layout);
		jPanel6Layout
				.setHorizontalGroup(jPanel6Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel6Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel17,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												91,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(40, 40, 40)
										.addGroup(
												jPanel6Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel6Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel6Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel18,
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								jLabel15,
																								javax.swing.GroupLayout.Alignment.TRAILING))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel6Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								AddressLine1JT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								295,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								AddressLine2JT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)))
														.addGroup(
																jPanel6Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel16)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				PhoneJT,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				295,
																				Short.MAX_VALUE))
														.addGroup(
																jPanel6Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel19)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				FaxJT,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				295,
																				Short.MAX_VALUE))
														.addGroup(
																jPanel6Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel20)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				EmailAddressJT,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				295,
																				Short.MAX_VALUE)))
										.addContainerGap()));

		jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { jLabel15, jLabel16, jLabel18,
						jLabel19, jLabel20 });

		jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { AddressLine1JT, AddressLine2JT,
						EmailAddressJT, FaxJT, PhoneJT });

		jPanel6Layout
				.setVerticalGroup(jPanel6Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel6Layout
										.createSequentialGroup()
										.addGroup(
												jPanel6Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel6Layout
																		.createSequentialGroup()
																		.addGap(
																				32,
																				32,
																				32)
																		.addGroup(
																				jPanel6Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel18)
																						.addComponent(
																								AddressLine1JT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								22,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(
																				18,
																				18,
																				18)
																		.addGroup(
																				jPanel6Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								AddressLine2JT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel15))
																		.addGap(
																				18,
																				18,
																				18)
																		.addGroup(
																				jPanel6Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel16,
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								PhoneJT,
																								javax.swing.GroupLayout.Alignment.TRAILING,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(
																				18,
																				18,
																				18)
																		.addGroup(
																				jPanel6Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel19,
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								FaxJT,
																								javax.swing.GroupLayout.Alignment.TRAILING,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(
																				18,
																				18,
																				18)
																		.addGroup(
																				jPanel6Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel20,
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								EmailAddressJT,
																								javax.swing.GroupLayout.Alignment.TRAILING,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)))
														.addGroup(
																jPanel6Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jLabel17,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				540,
																				Short.MAX_VALUE)))
										.addContainerGap()));

		jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { jLabel15, jLabel16, jLabel18,
						jLabel19, jLabel20 });

		jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { AddressLine1JT, AddressLine2JT,
						EmailAddressJT, FaxJT, PhoneJT });

		jTabbedPane1
				.addTab("Contact Information", new javax.swing.ImageIcon(
						getClass().getResource(
								"/elmis_images/eLMIS contact info small.png")),
						jPanel6); // NOI18N

		jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS programme support.png"))); // NOI18N
		jLabel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		//viewProgramJL.setFont(new java.awt.Font("Gulim", 1, 11));
		viewProgramJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS View details.png"))); // NOI18N
		viewProgramJL.setText("View Program");
		SdpStyleSheet.configOtherJLabel(viewProgramJL);
		viewProgramJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewProgramJLMouseClicked(evt);
			}
		});

		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/CreateHolidays.gif"))); // NOI18N
		jLabel5.setText("Activate Program");
		jLabel5.setEnabled(false);
		SdpStyleSheet.configOtherJLabel(jLabel5);

		jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/editdelete.png"))); // NOI18N
		jLabel6.setText("Remove Program");
		jLabel6.setEnabled(false);
		SdpStyleSheet.configOtherJLabel(jLabel6);

		programsJTable.setModel(tableModel_programs);
		SdpStyleSheet.configTable(programsJTable);
		programsJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				programsJTableMouseClicked(evt);
			}
		});
		jScrollPane2.setViewportView(programsJTable);

		//jLabel4.setFont(new java.awt.Font("Gulim", 1, 11));
		jLabel4.setText("Products");
		SdpStyleSheet.configOtherJLabel(jLabel4);

		programJTable.setModel(tableModel_programproducts);
		SdpStyleSheet.configTable(programJTable);
		jScrollPane1.setViewportView(programJTable);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout
				.setHorizontalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												83,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(18, 18, 18)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel2Layout
																		.createSequentialGroup()
																		.addComponent(
																				viewProgramJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				jLabel5)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				jLabel6))
														.addComponent(
																jScrollPane2,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																638,
																Short.MAX_VALUE)
														.addComponent(jLabel4)
														.addComponent(
																jScrollPane1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																626,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap()));
		jPanel2Layout
				.setVerticalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel2,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																540,
																Short.MAX_VALUE)
														.addGroup(
																jPanel2Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel2Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								viewProgramJL)
																						.addComponent(
																								jLabel5)
																						.addComponent(
																								jLabel6))
																		.addGap(
																				11,
																				11,
																				11)
																		.addComponent(
																				jScrollPane2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				149,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				jLabel4)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				jScrollPane1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				111,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap()));

		jTabbedPane1
				.addTab("Programs Supported", new javax.swing.ImageIcon(
						getClass().getResource(
								"/elmis_images/eLMIS program support sm.png")),
						jPanel2); // NOI18N

		jLabel21.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS geographic location Iicon.png"))); // NOI18N

		//jLabel7.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel7.setText("Geographic Zone");
		SdpStyleSheet.configOtherJLabel(jLabel7);

		jLabel8.setForeground(new java.awt.Color(255, 51, 51));
		jLabel8.setText("*");

		//jLabel22.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel22.setText("Catchment population");
		SdpStyleSheet.configOtherJLabel(jLabel22);

		jLabel23.setForeground(new java.awt.Color(255, 0, 0));
		jLabel23.setText("*");

		geozoneJCB.setModel(geozonemodel);

		catchmentpopJT.setEnabled(false);

		jSeparator2.setForeground(new java.awt.Color(255, 255, 255));
		jSeparator2.setFont(new java.awt.Font("Tahoma", 0, 8));

		//jLabel24.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel24.setText("Latitude");
		SdpStyleSheet.configOtherJLabel(jLabel24);

		latitudeJT.setEnabled(false);

		//jLabel25.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel25.setText("Longitude");
		SdpStyleSheet.configOtherJLabel(jLabel25);

		longitudeJT.setEnabled(false);

		jSeparator3.setForeground(new java.awt.Color(255, 255, 255));

		//jLabel26.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel26.setText("Altitude");
		SdpStyleSheet.configOtherJLabel(jLabel26);

		altitudeJT.setEnabled(false);
		SdpStyleSheet.configJPanelBackground(jPanel3);
		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
				jPanel3);
		jPanel3.setLayout(jPanel3Layout);
		jPanel3Layout
				.setHorizontalGroup(jPanel3Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel3Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel21,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												91,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(41, 41, 41)
										.addGroup(
												jPanel3Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel3Layout
																		.createSequentialGroup()
																		.addComponent(
																				altitudeJT,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				236,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addContainerGap())
														.addGroup(
																jPanel3Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																		.addGroup(
																				jPanel3Layout
																						.createSequentialGroup()
																						.addComponent(
																								jLabel26)
																						.addContainerGap())
																		.addGroup(
																				javax.swing.GroupLayout.Alignment.TRAILING,
																				jPanel3Layout
																						.createSequentialGroup()
																						.addGroup(
																								jPanel3Layout
																										.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.TRAILING)
																										.addComponent(
																												jSeparator3,
																												javax.swing.GroupLayout.Alignment.LEADING,
																												javax.swing.GroupLayout.DEFAULT_SIZE,
																												588,
																												Short.MAX_VALUE)
																										.addGroup(
																												javax.swing.GroupLayout.Alignment.LEADING,
																												jPanel3Layout
																														.createSequentialGroup()
																														.addGroup(
																																jPanel3Layout
																																		.createParallelGroup(
																																				javax.swing.GroupLayout.Alignment.LEADING)
																																		.addComponent(
																																				jLabel24)
																																		.addComponent(
																																				latitudeJT,
																																				javax.swing.GroupLayout.PREFERRED_SIZE,
																																				236,
																																				javax.swing.GroupLayout.PREFERRED_SIZE))
																														.addGap(
																																34,
																																34,
																																34)
																														.addGroup(
																																jPanel3Layout
																																		.createParallelGroup(
																																				javax.swing.GroupLayout.Alignment.LEADING)
																																		.addComponent(
																																				jLabel25)
																																		.addComponent(
																																				longitudeJT,
																																				javax.swing.GroupLayout.PREFERRED_SIZE,
																																				277,
																																				javax.swing.GroupLayout.PREFERRED_SIZE)))
																										.addGroup(
																												javax.swing.GroupLayout.Alignment.LEADING,
																												jPanel3Layout
																														.createParallelGroup(
																																javax.swing.GroupLayout.Alignment.TRAILING,
																																false)
																														.addComponent(
																																jSeparator2,
																																javax.swing.GroupLayout.Alignment.LEADING)
																														.addGroup(
																																javax.swing.GroupLayout.Alignment.LEADING,
																																jPanel3Layout
																																		.createSequentialGroup()
																																		.addGroup(
																																				jPanel3Layout
																																						.createParallelGroup(
																																								javax.swing.GroupLayout.Alignment.LEADING)
																																						.addGroup(
																																								jPanel3Layout
																																										.createSequentialGroup()
																																										.addComponent(
																																												jLabel7)
																																										.addPreferredGap(
																																												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																										.addComponent(
																																												jLabel8))
																																						.addComponent(
																																								geozoneJCB,
																																								javax.swing.GroupLayout.PREFERRED_SIZE,
																																								238,
																																								javax.swing.GroupLayout.PREFERRED_SIZE))
																																		.addGap(
																																				27,
																																				27,
																																				27)
																																		.addGroup(
																																				jPanel3Layout
																																						.createParallelGroup(
																																								javax.swing.GroupLayout.Alignment.LEADING)
																																						.addGroup(
																																								jPanel3Layout
																																										.createSequentialGroup()
																																										.addComponent(
																																												jLabel22)
																																										.addPreferredGap(
																																												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																																										.addComponent(
																																												jLabel23))
																																						.addComponent(
																																								catchmentpopJT,
																																								javax.swing.GroupLayout.PREFERRED_SIZE,
																																								272,
																																								javax.swing.GroupLayout.PREFERRED_SIZE)))))
																						.addGap(
																								53,
																								53,
																								53))))));

		jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { altitudeJT, catchmentpopJT,
						geozoneJCB, latitudeJT, longitudeJT });

		jPanel3Layout
				.setVerticalGroup(jPanel3Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel3Layout
										.createSequentialGroup()
										.addGroup(
												jPanel3Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel3Layout
																		.createSequentialGroup()
																		.addGap(
																				40,
																				40,
																				40)
																		.addGroup(
																				jPanel3Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel7)
																						.addComponent(
																								jLabel8)
																						.addComponent(
																								jLabel22)
																						.addComponent(
																								jLabel23))
																		.addGap(
																				18,
																				18,
																				18)
																		.addGroup(
																				jPanel3Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								geozoneJCB,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								catchmentpopJT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				jSeparator2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				10,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addGroup(
																				jPanel3Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel24)
																						.addComponent(
																								jLabel25))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addGroup(
																				jPanel3Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								latitudeJT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								longitudeJT,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				jSeparator3,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				10,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				jLabel26)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				altitudeJT,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																jPanel3Layout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				jLabel21,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				540,
																				Short.MAX_VALUE)))
										.addContainerGap()));

		jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { altitudeJT, catchmentpopJT,
						geozoneJCB, latitudeJT, longitudeJT });

		jTabbedPane1.addTab("Geographic Information",
				new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_images/eLMIS geographic sm.png")), jPanel3); // NOI18N

		jLabel35.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS Other info icon.png"))); // NOI18N

		//jLabel36.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel36.setText("Cold storage gross capacity");
		SdpStyleSheet.configOtherJLabel(jLabel36);

		coldstoragegrosscapacityJRB.setEnabled(false);

		//jLabel37.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel37.setText("Cold storage net capacity");
		SdpStyleSheet.configOtherJLabel(jLabel37);

		coldstoragenetcapacityJRB.setEnabled(false);

		//jLabel38.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel38.setText("Electricity available at facility?");
		SdpStyleSheet.configOtherJLabel(jLabel38);

		Electricity.add(electricityYesJRB);
		electricityYesJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		electricityYesJRB.setText("Yes");

		Electricity.add(electricityNOJRB);
		electricityNOJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		electricityNOJRB.setText("No");

		//jLabel39.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel39.setText("Facility has internet connectivity");
		SdpStyleSheet.configOtherJLabel(jLabel39);

		InterNet.add(internetYesJRB);
		internetYesJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		internetYesJRB.setText("Yes");

		InterNet.add(internetNoJRB);
		internetNoJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		internetNoJRB.setText("No");

		//jLabel40.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel40.setText("Facility has electronic SCC?");
		SdpStyleSheet.configOtherJLabel(jLabel40);

		//jLabel41.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel41.setText("Facility has electronic DAR?");
		SdpStyleSheet.configOtherJLabel(jLabel41);

		SCC.add(electronicsccYesJRB);
		electronicsccYesJRB.setFont(new java.awt.Font("Tahoma", 1, 11));
		electronicsccYesJRB.setText("Yes");

		SCC.add(electronicsccNoJRB);
		electronicsccNoJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		electronicsccNoJRB.setText("No");

		DAR.add(electronicdarYesJRB);
		electronicdarYesJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		electronicdarYesJRB.setText("Yes");

		DAR.add(electronicdarNoJRB);
		electronicdarNoJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		electronicdarNoJRB.setText("No");

		//jLabel42.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel42.setText("Data reportable for the facility?");
		SdpStyleSheet.configOtherJLabel(jLabel42);

		//jLabel43.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel43.setText("Facility supplies others?");
		SdpStyleSheet.configOtherJLabel(jLabel43);

		DATA_Reportable.add(datareportableYesJRB);
		datareportableYesJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		datareportableYesJRB.setText("Yes ");

		DATA_Reportable.add(datareportableNoJRB);
		datareportableNoJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		datareportableNoJRB.setText("No");

		SuppliesOthers.add(suppliesotherYesJRB);
		suppliesotherYesJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		suppliesotherYesJRB.setText("Yes");

		SuppliesOthers.add(suppliesothersNoJRB);
		suppliesothersNoJRB.setFont(new java.awt.Font("Gulim", 1, 11));
		suppliesothersNoJRB.setText("No");

		//jLabel44.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel44.setText("Comments");
		SdpStyleSheet.configOtherJLabel(jLabel44);

		commentsJRB.setEnabled(false);

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4Layout.setHorizontalGroup(
			jPanel4Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jLabel35, GroupLayout.PREFERRED_SIZE, 91, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel4Layout.createSequentialGroup()
							.addGap(18)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(jSeparator12, Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(jLabel36)
								.addComponent(coldstoragegrosscapacityJRB, GroupLayout.PREFERRED_SIZE, 351, GroupLayout.PREFERRED_SIZE)
								.addComponent(jSeparator8, GroupLayout.DEFAULT_SIZE, 576, Short.MAX_VALUE)
								.addComponent(jLabel37)
								.addComponent(coldstoragenetcapacityJRB, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE)
								.addComponent(jSeparator9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel4Layout.createSequentialGroup()
									.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
										.addComponent(jLabel38)
										.addGroup(jPanel4Layout.createSequentialGroup()
											.addComponent(electricityYesJRB)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(electricityNOJRB)))
									.addGap(94)
									.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
										.addGroup(jPanel4Layout.createSequentialGroup()
											.addComponent(internetYesJRB)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(internetNoJRB))
										.addComponent(jLabel39)))
								.addComponent(jSeparator10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel4Layout.createSequentialGroup()
									.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
										.addComponent(jLabel40)
										.addGroup(jPanel4Layout.createSequentialGroup()
											.addComponent(electronicsccYesJRB)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(electronicsccNoJRB)))
									.addGap(109)
									.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
										.addGroup(jPanel4Layout.createSequentialGroup()
											.addComponent(electronicdarYesJRB)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(electronicdarNoJRB))
										.addComponent(jLabel41)))
								.addComponent(jSeparator11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addGroup(jPanel4Layout.createSequentialGroup()
									.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
										.addComponent(jLabel42)
										.addGroup(jPanel4Layout.createSequentialGroup()
											.addComponent(datareportableYesJRB)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(datareportableNoJRB)))
									.addGap(88)
									.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
										.addGroup(jPanel4Layout.createSequentialGroup()
											.addComponent(suppliesotherYesJRB)
											.addPreferredGap(ComponentPlacement.UNRELATED)
											.addComponent(suppliesothersNoJRB))
										.addComponent(jLabel43)))))
						.addGroup(jPanel4Layout.createSequentialGroup()
							.addGap(26)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(commentsJRB, GroupLayout.DEFAULT_SIZE, 569, Short.MAX_VALUE)
								.addComponent(jLabel44))))
					.addContainerGap(49, Short.MAX_VALUE))
		);
		jPanel4Layout.setVerticalGroup(
			jPanel4Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel4Layout.createSequentialGroup()
					.addGroup(jPanel4Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel4Layout.createSequentialGroup()
							.addGap(22)
							.addComponent(jLabel36)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(coldstoragegrosscapacityJRB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(jSeparator8, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(jLabel37)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(coldstoragenetcapacityJRB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(jSeparator9, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jLabel38)
								.addComponent(jLabel39))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(electricityYesJRB)
								.addComponent(internetYesJRB)
								.addComponent(internetNoJRB)
								.addComponent(electricityNOJRB))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(jSeparator10, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jLabel40)
								.addComponent(jLabel41))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(electronicsccYesJRB)
								.addComponent(electronicsccNoJRB)
								.addComponent(electronicdarYesJRB)
								.addComponent(electronicdarNoJRB))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(jSeparator11, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(jLabel42)
								.addComponent(jLabel43))
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addGroup(jPanel4Layout.createParallelGroup(Alignment.BASELINE)
								.addComponent(datareportableYesJRB)
								.addComponent(datareportableNoJRB)
								.addComponent(suppliesotherYesJRB)
								.addComponent(suppliesothersNoJRB))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(jSeparator12, GroupLayout.PREFERRED_SIZE, 10, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(jLabel44)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(commentsJRB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel4Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jLabel35, GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)))
					.addContainerGap())
		);
		jPanel4.setLayout(jPanel4Layout);

		jTabbedPane1.addTab("Other Information",
				new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_images/eLMIS other info sm .png")), jPanel4); // NOI18N

		backJBtn.setFont(new java.awt.Font("Gulim", 1, 11));
		backJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/navigate_left.png"))); // NOI18N
		backJBtn.setText("Back");
		backJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				backJBtnActionPerformed(evt);
			}
		});

		nextJBtn.setFont(new java.awt.Font("Gulim", 1, 11));
		nextJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/navigate_right.png"))); // NOI18N
		nextJBtn.setText("Next  ");
		nextJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		nextJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				nextJBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(
				jPanel5);
		jPanel5.setLayout(jPanel5Layout);
		jPanel5Layout
				.setHorizontalGroup(jPanel5Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel5Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel5Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																jPanel5Layout
																		.createSequentialGroup()
																		.addComponent(
																				backJBtn)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				nextJBtn))
														.addComponent(
																jTabbedPane1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																772,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { backJBtn, nextJBtn });

		jPanel5Layout
				.setVerticalGroup(jPanel5Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel5Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jTabbedPane1,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												600, Short.MAX_VALUE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel5Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(nextJBtn)
														.addComponent(backJBtn))
										.addContainerGap()));

		jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { backJBtn, nextJBtn });

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap().addComponent(
						jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE,
						javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:

		getFacilityObject();

	}

	private void formWindowActivated(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		//make call to the db for facility details

	}

	private void programsJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		try {
			ProductsDAO Productdao = new ProductsDAO();

			String code = (String) (tableModel_programs.getValueAt(
					this.programsJTable.getSelectedRow(), 4).toString());

			code.trim();
			int i = Integer.parseInt(code);
			System.out.println(i + "PROGRAM ID IS OK");
			this.productsList = Productdao.getAllProductList(i);

			for (@SuppressWarnings("unused")
			Products p : productsList) {
				System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getDispensingunit().toString());
				System.out.println(p.getPacksize().toString());
				System.out.println(p.getControlledsubstance().toString());
				//System.out.println(p.getpgetControlledsubstance().toString());
			}
			this.PopulateProgram_ProductTable(this.productsList);

		} finally {
			//session.close();
		}

	}

	/************************************************
	 * Start facility set up
	 * 
	 * @author mkausa
	 *
	 ************************************************/
	@SuppressWarnings("unused")
	private void getFacilityObject() {

		try {
			callfacility = new FacilitySetUpDAO();
			facility = new Facility();
			facilitytype = new Facility_Types();
			facilityop = new Facility_Operators();
			facilitygeozone = new Geographic_Zones();
			facility = callfacility.getFacility();
			facilityop = callfacility.selectAllwithOps();
			facilitytype = callfacility.selectAllwithTypes(facility);
			facilitygeozone = callfacility.selectAllwithGeozones();
			System.out.println(facility.getCode());
			System.out.println(facility.getAddress1());
			System.out.println(facility.getGolivedate());
			System.out.println(facility.getSuppliesothers());
			//System.out.println(facilitygeozone.getName());
			//System.out.println(facilityop.getCode());

			populateContactInformation(facility);
			populateBasicInformation(facility);
			populateGeographiczoneInformation(facility);
			populatefacilityOtherInformation(facility);

		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	private void populateContactInformation(Facility facility) {
		// fill the contact details textboxes

		try {

			this.AddressLine1JT.setText(facility.getAddress1());
			this.AddressLine2JT.setText(facility.getAddress2());
			this.PhoneJT.setText(facility.getMainphone());
			this.FaxJT.setText(facility.getFax());
			this.EmailAddressJT.setText("");
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}
	}

	private void populateBasicInformation(Facility facility) {
		try {
			this.facilityCodeJTF.setText(facility.getCode());
			this.facilitynameJT.setText(facility.getName());
			this.glnJT.setText(facility.getGln());
			this.GodowndateJDate.setDate(facility.getGodowndate());
			this.GolivedateJDate.setDate(facility.getGolivedate());
			//Facilitytype 

			System.out.println(facilitytype.getCode());
			model.addElement(facilitytype.getCode());
			selectedfacilitytypecode = facilitytype.getCode();
			//facility operator
			typemodel.addElement(facilityop.getCode());
			//Radio buttons 
			if (facility.getSdp() == true) {

				ServicedeliverypointYesJRB.setSelected(true);
			} else {
				ServicedeliverpointNoJRB.setSelected(true);
			}
			if (facility.getActive() == true) {
				ActivefacilityYesJRB.setSelected(true);
			} else {
				ActivefacilityNoJRB.setSelected(true);
			}

			this.FacilitydescriptJT.setText(facility.getDescription());
		} catch (NullPointerException e) {
			// Do something
			e.getMessage();
		}

	}

	private void populateGeographiczoneInformation(Facility facility) {
		try {
			this.latitudeJT.setText(facility.getLatitude().toString());
			this.longitudeJT.setText(facility.getLongitude().toString());
			this.altitudeJT.setText(facility.getAltitude().toString());
			this.catchmentpopJT.setText(facility.getCatchmentpopulation()
					.toString());

			geozonemodel.addElement(facilitygeozone.getName());
		} catch (NullPointerException e) {
			e.getMessage();
		}
	}

	@SuppressWarnings("unused")
	private void populatefacilityOtherInformation(Facility facility) {
		try {

			this.coldstoragegrosscapacityJRB.setText(facility
					.getColdstoragegrosscapacity().toString());
			this.coldstoragenetcapacityJRB.setText(facility
					.getColdstoragenetcapacity().toString());
			this.commentsJRB.setText(facility.getComment());

			//Radio buttons 
			if (facility.getHaselectricity() == true) {

				this.electricityYesJRB.setSelected(true);
			} else {
				this.electricityNOJRB.setSelected(true);
			}
			if (facility.getOnline() == true) {
				this.internetYesJRB.setSelected(true);

			} else {
				this.internetNoJRB.setSelected(true);
			}

			if (facility.getHaselectronicscc() == true) {
				this.electronicsccYesJRB.setSelected(true);

			} else {
				this.electronicsccNoJRB.setSelected(true);
			}

			if (facility.getHaselectronicdar() == true) {
				this.electronicdarYesJRB.setSelected(true);

			} else {
				this.electronicdarNoJRB.setSelected(true);
			}

			if (facility.getSuppliesothers() == true) {
				this.suppliesotherYesJRB.setSelected(true);

			} else {
				this.suppliesothersNoJRB.setSelected(true);
			}

			if (facility.getDatareportable() == true) {
				this.datareportableYesJRB.setSelected(true);

			} else {
				this.datareportableNoJRB.setSelected(true);
			}
		} catch (NullPointerException e) {
			e.getMessage();
		}

	}

	//End Setting up facility details 

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List progProducts) {

		tableModel_programproducts.clearTable();
		productsIterator = progProducts.listIterator();

		while (productsIterator.hasNext()) {

			products = productsIterator.next();
			defaultv_prgramproducts[0] = products.getPrimaryname().toString();

			defaultv_prgramproducts[1] = products.getCode().toString();
			defaultv_prgramproducts[2] = products.getDispensingunit();
			defaultv_prgramproducts[3] = products.getPacksize().toString();
			defaultv_prgramproducts[4] = products.getControlledsubstance()
					.toString();
			//defaultv_prgramproducts[5] = products.g

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_programproducts.length; j++) {
				cols.add(defaultv_prgramproducts[j]);

			}

			tableModel_programproducts.insertRow(cols);

			productsIterator.remove();
		}
	}

	private void viewProgramJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();
		try {
			Integer facid = 0;
			facid = Integer.parseInt(System.getProperty("facilityid"));
			System.out.println(facid);
			this.programsList = session.selectList("getProgramesSupported",
					facid);

			this.populateProgramsTable(this.programsList);

		} finally {
			session.close();
		}

	}

	private void formWindowClosing(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		//System.setProperty("clientType", "null");
		//new NetworkProperties().writeToPropertiesFile();
		//System.exit(0);
	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		selectTabIndex = jTabbedPane1.getSelectedIndex();
		if (selectTabIndex == 1) {

			//jTabbedPane1.setSelectedIndex(0);
			backJBtn.setVisible(true);
			nextJBtn.setText("Next");
		} else if (selectTabIndex == 2) {

			backJBtn.setVisible(true);
			//jTabbedPane1.setSelectedIndex(1);
			nextJBtn.setText("Finish");
		} else if (selectTabIndex == 0) {

			//jTabbedPane1.setSelectedIndex(1);
			backJBtn.setVisible(false);
			nextJBtn.setText("Next");
		}

	}

	private void backJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		selectTabIndex = jTabbedPane1.getSelectedIndex();

		if (selectTabIndex == 1) {

			jTabbedPane1.setSelectedIndex(0);
			backJBtn.setVisible(false);
		} else if (selectTabIndex == 2) {

			jTabbedPane1.setSelectedIndex(1);
			nextJBtn.setText("Next");
		} else if (selectTabIndex == 3) {

			jTabbedPane1.setSelectedIndex(2);
			nextJBtn.setText("Next");
		} else if (selectTabIndex == 4) {

			jTabbedPane1.setSelectedIndex(3);
			nextJBtn.setText("Next");
		}
	}

	private void nextJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		selectTabIndex = jTabbedPane1.getSelectedIndex();
		if (selectTabIndex == 0) {

			jTabbedPane1.setSelectedIndex(1);
			backJBtn.setVisible(true);
		} else if (selectTabIndex == 1) {

			jTabbedPane1.setSelectedIndex(2);

		} else if (selectTabIndex == 2) {

			jTabbedPane1.setSelectedIndex(3);

		} else if (selectTabIndex == 3) {

			jTabbedPane1.setSelectedIndex(4);
			nextJBtn.setText("Finish");

		} else if (selectTabIndex == 4) {

			jTabbedPane1.setSelectedIndex(4);

		}

	}

	//populate programs

	private void populateProgramsTable(List dataList) {

		this.tableModel_programs.clearTable();
		this.programsIterator = dataList.listIterator();

		while (this.programsIterator.hasNext()) {

			programes = this.programsIterator.next();
			defaultv_programs[0] = this.programes.getName();
			defaultv_programs[1] = this.programes.getActive().toString();
			defaultv_programs[2] = this.programes.getCreateddate();
			defaultv_programs[3] = this.programes.getCode();
			defaultv_programs[4] = this.programes.getId();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_programs.length; j++) {
				cols.add(defaultv_programs[j]);

			}

			tableModel_programs.insertRow(cols);

			this.programsIterator.remove();
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SystemSettingJD dialog = new SystemSettingJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.ButtonGroup ActiveFacility;
	private javax.swing.JRadioButton ActivefacilityNoJRB;
	private javax.swing.JRadioButton ActivefacilityYesJRB;
	private javax.swing.JTextField AddressLine1JT;
	private javax.swing.JTextField AddressLine2JT;
	private javax.swing.ButtonGroup DAR;
	private javax.swing.ButtonGroup DATA_Reportable;
	private javax.swing.ButtonGroup Electricity;
	private javax.swing.JTextField EmailAddressJT;
	private javax.swing.JComboBox FacilityTypeJCB;
	private javax.swing.JTextField FacilitydescriptJT;
	private javax.swing.JTextField FaxJT;
	private com.toedter.calendar.JDateChooser GodowndateJDate;
	private com.toedter.calendar.JDateChooser GolivedateJDate;
	private javax.swing.ButtonGroup InterNet;
	private javax.swing.JComboBox OperatedbyCB;
	private javax.swing.JTextField PhoneJT;
	private javax.swing.ButtonGroup SCC;
	private javax.swing.ButtonGroup SDP;
	private javax.swing.JRadioButton ServicedeliverpointNoJRB;
	private javax.swing.JRadioButton ServicedeliverypointYesJRB;
	private javax.swing.ButtonGroup SuppliesOthers;
	private javax.swing.JTextField altitudeJT;
	private javax.swing.JButton backJBtn;
	private javax.swing.ButtonGroup buttonGroup1;
	private javax.swing.JTextField catchmentpopJT;
	private javax.swing.JTextField coldstoragegrosscapacityJRB;
	private javax.swing.JTextField coldstoragenetcapacityJRB;
	private javax.swing.JTextField commentsJRB;
	private javax.swing.JRadioButton datareportableNoJRB;
	private javax.swing.JRadioButton datareportableYesJRB;
	private javax.swing.JRadioButton electricityNOJRB;
	private javax.swing.JRadioButton electricityYesJRB;
	private javax.swing.JRadioButton electronicdarNoJRB;
	private javax.swing.JRadioButton electronicdarYesJRB;
	private javax.swing.JRadioButton electronicsccNoJRB;
	private javax.swing.JRadioButton electronicsccYesJRB;
	private javax.swing.JTextField facilityCodeJTF;
	private javax.swing.JTextField facilitynameJT;
	private javax.swing.JComboBox geozoneJCB;
	private javax.swing.JTextField glnJT;
	private javax.swing.JRadioButton internetNoJRB;
	private javax.swing.JRadioButton internetYesJRB;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel11;
	private javax.swing.JLabel jLabel12;
	private javax.swing.JLabel jLabel13;
	private javax.swing.JLabel jLabel14;
	private javax.swing.JLabel jLabel15;
	private javax.swing.JLabel jLabel16;
	private javax.swing.JLabel jLabel17;
	private javax.swing.JLabel jLabel18;
	private javax.swing.JLabel jLabel19;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel20;
	private javax.swing.JLabel jLabel21;
	private javax.swing.JLabel jLabel22;
	private javax.swing.JLabel jLabel23;
	private javax.swing.JLabel jLabel24;
	private javax.swing.JLabel jLabel25;
	private javax.swing.JLabel jLabel26;
	private javax.swing.JLabel jLabel27;
	private javax.swing.JLabel jLabel28;
	private javax.swing.JLabel jLabel29;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel30;
	private javax.swing.JLabel jLabel31;
	private javax.swing.JLabel jLabel32;
	private javax.swing.JLabel jLabel33;
	private javax.swing.JLabel jLabel34;
	private javax.swing.JLabel jLabel35;
	private javax.swing.JLabel jLabel36;
	private javax.swing.JLabel jLabel37;
	private javax.swing.JLabel jLabel38;
	private javax.swing.JLabel jLabel39;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel40;
	private javax.swing.JLabel jLabel41;
	private javax.swing.JLabel jLabel42;
	private javax.swing.JLabel jLabel43;
	private javax.swing.JLabel jLabel44;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JLabel jLabel8;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JPanel jPanel5;
	private javax.swing.JPanel jPanel6;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JSeparator jSeparator10;
	private javax.swing.JSeparator jSeparator11;
	private javax.swing.JSeparator jSeparator12;
	private javax.swing.JSeparator jSeparator2;
	private javax.swing.JSeparator jSeparator3;
	private javax.swing.JSeparator jSeparator4;
	private javax.swing.JSeparator jSeparator5;
	private javax.swing.JSeparator jSeparator6;
	private javax.swing.JSeparator jSeparator7;
	private javax.swing.JSeparator jSeparator8;
	private javax.swing.JSeparator jSeparator9;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JTextField latitudeJT;
	private javax.swing.JTextField longitudeJT;
	private javax.swing.JButton nextJBtn;
	private javax.swing.JTable programJTable;
	private javax.swing.JTable programsJTable;
	private javax.swing.JRadioButton suppliesotherYesJRB;
	private javax.swing.JRadioButton suppliesothersNoJRB;
	private javax.swing.JLabel viewProgramJL;
	// End of variables declaration//GEN-END:variables

}