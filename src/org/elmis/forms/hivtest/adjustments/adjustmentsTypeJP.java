/*
 * ProgramsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest.adjustments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.TableModel;

/**
 * 
 * @author __USER__
 */
public class adjustmentsTypeJP extends javax.swing.JPanel {

	// Create tableModel for Program Products for each facility
	private static final String[] columns_adjustType = { "Adjustment Type" };
	private static final Object[] defaultv_adjustType = { "", "" };
	private static final int rows_adjustType = 0;
	public static TableModel tableModel_adjustType = new TableModel(
			columns_adjustType, defaultv_adjustType, rows_adjustType);

	private Losses_Adjustments_Types adjustments;
	private ListIterator<Losses_Adjustments_Types> adjustmentsIterator;
	private List<Losses_Adjustments_Types> adjustmentsList = new LinkedList();

	//private  List<Products> productsList = new LinkedList();
	private int selectedProgramID;

	private static Map parameterMap = new HashMap();
	private JasperPrint print;

	/** Creates new form ProgramsJP */
	public adjustmentsTypeJP(int selectedProgramID) {

		this.selectedProgramID = selectedProgramID;
		initComponents();
		this.setFocusable(false);
		this.adjustTypeJTable.setFocusable(false);// remove from focus cycle

		//this.adjustTypeJTable.getColumnModel().getColumn(0).setMinWidth(120);
		//this.adjustTypeJTable.getColumnModel().getColumn(0).setMaxWidth(120);

		//this.productsJTable.getColumnModel().getColumn(2).setMinWidth(120);
		//this.productsJTable.getColumnModel().getColumn(2).setMaxWidth(120);

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			//if get all products is selected
			//	if(this.selectedProgramID == 0){
			//	this.productsList = session.selectList("getAll");

			//	}else{

			//if a program area is selected 
			//this.productsList = session.selectList("selectProductByProgramID",
			//	this.selectedProgramID);

			//selectHIVTestProducts

			this.adjustmentsList = session.selectList("selectAllAdjustments");
			//	}

			//	this.populateProductsTable(this.productsList);
			this.populateProductsTable(adjustmentsList);

		} finally {
			session.close();
		}
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		adjustTypeJTable = new javax.swing.JTable();

		adjustTypeJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		adjustTypeJTable.setModel(tableModel_adjustType);
		adjustTypeJTable.setRowHeight(60);
		adjustTypeJTable.setTableHeader(null);
		adjustTypeJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				adjustTypeJTableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(adjustTypeJTable);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE));
	}// </editor-fold>
	//GEN-END:initComponents

	private void adjustTypeJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
AdjustmentSelectJD.jTabbedPane1.addTab("QTY", new QtyJP("Selected"));
		
		//AdjustmentSelectJD.jTabbedPane1.addTab("Sex", new javax.swing.ImageIcon(getClass()
			//	.getResource("/images/CUSTOMER.PNG")), sexJP);
		AdjustmentSelectJD.jTabbedPane1.setSelectedIndex(2);
	}

	public void populateProductsTable(List progProducts) {

		tableModel_adjustType.clearTable();
		adjustmentsIterator = progProducts.listIterator();

		while (adjustmentsIterator.hasNext()) {

			adjustments = adjustmentsIterator.next();

			defaultv_adjustType[0] = adjustments.getName();// products.getCode();
			//defaultv_adjustType[1] = hivTestProducts.getDar_display_name();//products.getPrimaryname();

			//defaultv_products[3] = hivTestProducts.getId();//products.getId();

			// defaultv_prgramproducts[5] = products.g

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_adjustType.length; j++) {
				cols.add(defaultv_adjustType[j]);

			}

			tableModel_adjustType.insertRow(cols);

			adjustmentsIterator.remove();
		}
	}

	private void createStockControlCardX(int selectedProductID) {

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();
		StockControlCard scc = new StockControlCard();
		List sccList = new ArrayList();
		try {

			//scc
			sccList = session.selectList("selectBySCCByProductID",
					selectedProductID);//("getProgramesSupported");

			//System.out.println(this.products.getPrimaryname() +" "+IssuingJD.selectedProductID);

			//this.populateProgramsTable(this.programsList);

		} finally {
			session.close();
		}

		// TODO add your handling code here:

		// new Items_issue().createBeanCollection()
		try {

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport(
					"Reports/stockcontrolcard.jasper", parameterMap,
					new JRBeanCollectionDataSource(sccList));

			new JasperViewer(print);
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}

	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JTable adjustTypeJTable;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables

}