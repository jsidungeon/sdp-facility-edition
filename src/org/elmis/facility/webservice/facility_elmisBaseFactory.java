package org.elmis.facility.webservice;

import java.util.ArrayList;
import java.util.List;

import org.elmis.facility.utils.HttpClient;
import org.elmis.facility.utils.ResponseEntity;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.google.gson.Gson;


public class facility_elmisBaseFactory {

    // TODO: remove the hardcoded default values.

    //public static String BaseURL = "http://192.168.73.194:9091";
    //public static String BaseURL = "http://uat.tz.elmis-dev.org";

   //public static String BaseURL = "http://localhost:9091";


    public static String Vendor = "openLmis";

  // public static String Authorization  = "e2e2d6ae-1c8f-47ff-af8c-ce32ad89a8bb"; //TZ  token
   public static String Authorization  = "df6fc313-144a-4f35-8ea9-364285a9bd3f";


   public static String BaseURL = "http://uat.zm.elmis-dev.org";
   

    public static String UserName = "Admin123";

    public static Long FacilityID = 1L;

    @SuppressWarnings("unchecked")
	public static Object loadJSONLookup(String service, Class<?> type) throws Exception{

        HttpClient client = new HttpClient();
        client.createContext();

        ResponseEntity response = client.SendJSON("{}", BaseURL + "/rest-api/lookup/" + service, HttpClient.POST, Vendor , Authorization );
        try{
            JSONObject array = new JSONObject(response.getResponse());
            JSONArray innerArray = array.getJSONArray(service);
            Gson gson = new Gson();

            List list = new ArrayList<String>() ;
          for(int i = 0; i < innerArray.length(); i++){
                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(),type)) ;
            }
            return list;
        }

        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            throw new Exception ("Authentication Error");
        }

    }


    public static Object uploadJSON(String service, String result , String json , Class<?> type) throws Exception{
    	System.out.println(json);
        HttpClient client = new HttpClient();
        System.out.println(">>>");
        client.createContext();
        System.out.println("--->");
        ResponseEntity response = client.SendJSON(json, BaseURL + "/rest-api/" + service, HttpClient.POST, Vendor , Authorization );
        System.out.println("TSTSTS");
        JSONObject array = new JSONObject(response.getResponse());
        System.out.println("KLUYTR");

        String errorObject = null;
        try{
            errorObject = array.getString("error");
        }catch(Exception ex){
        	
        }
        if(errorObject != null){
            throw new Exception(errorObject);
        }
        try{
            JSONObject resultJson = array.getJSONObject(result);
            Gson gson = new Gson();
            return gson.fromJson(resultJson.toString(),type);
        }
        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            // JSON was not being parsed,
            // this indicates that the authentication has failed.
            throw new Exception("Authentication Error");
        }

    }


}
