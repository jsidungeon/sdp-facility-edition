package org.elmis.facility.domain.model;

import java.sql.Date;

public class Transaction {
	private int id;
	private Date transactionDate;
	private String productCode;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	@Override
	public String toString() {
		return "Transaction [id=" + id + ", transactionDate=" + transactionDate
				+ ", productCode=" + productCode + "]";
	}
	
}
