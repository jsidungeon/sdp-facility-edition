package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.IndicatorProduct;
import org.elmis.facility.reporting.model.Product;

public class IndicatorProductDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public IndicatorProductDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public int deleteIndicatorProduct(@Param("productCode")String productCode)
	{
		Map<String, String> map = new HashMap<>();
		map.put("productCode", productCode);
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			int rowsAffected = session.insert("IndicatorProduct.deleteIndicatorProduct", map);
			session.commit();
			return rowsAffected;
		}
		finally
		{
			session.close();
		}
	}
	public void addIndicatorProduct(Product p)
	{
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			session.insert("IndicatorProduct.addIndicatorProduct", p);
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public synchronized List<IndicatorProduct> getIndicatorProducts()
	{
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("IndicatorProduct.getProducts");
		}
		finally
		{
			session.close();
		}
	}
	public void updateEmergencyOrderPoint(String productCode, int emergencyOrderPoint)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		map.put("emergencyOrderPoint", emergencyOrderPoint);
		
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			session.update("IndicatorProduct.updateEOP", map);
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public static void main(String...a)
	{
		List<IndicatorProduct> p = new IndicatorProductDao().getIndicatorProducts();
		
		for (IndicatorProduct iProd : p)
		{
			System.out.println(iProd.getProductCode()+ " WITH BALANCE "+iProd.getBalance());
		}
	}
}
