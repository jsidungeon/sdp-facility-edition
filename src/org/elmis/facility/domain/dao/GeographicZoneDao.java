package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.model.GeographicZone;

public class GeographicZoneDao {
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<GeographicZone> getGeographicZones()
	{
		SqlSession session = sqlSessionFactory.openSession();
		
		try
		{
			return session.selectList("GeographicZone.getProvince_GeographicZones");
		}
		finally
		{
			session.close();
		}
	}
	public List<GeographicZone> getGeographicZones(String provinceCode)
	{
		Map<String, String> map = new HashMap<>();
		map.put("provinceCode", provinceCode);
		map.put("str", provinceCode+"%");
		SqlSession session = sqlSessionFactory.openSession();

		try
		{
			return session.selectList("GeographicZone.getDistrict_GeographicZones", map);
		}
		finally
		{
			session.close();
		}
	}
	public static void main(String[] args) {
		List<GeographicZone> l = new GeographicZoneDao().getGeographicZones();
		
		for (GeographicZone g : l)
			System.out.println(l.toString());
		System.out.println(l.size());
	}

}
