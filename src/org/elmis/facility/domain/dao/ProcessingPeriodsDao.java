package org.elmis.facility.domain.dao;

import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.domain.model.Processing_Periods;
import org.elmis.forms.regimen.util.GenericUtil;

public class ProcessingPeriodsDao {
	private SqlSessionFactory getSqlMapper(){
		String resource = "SqlMapConfig.xml";
		Reader reader = null;

		SqlSessionFactory sqlMapper = null;
		try {
			reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader,
					GenericUtil.getProperties());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sqlMapper;
	}
	
	
public List<Processing_Periods> selectPeriodByRange(Date startDate, Date endDate){
	List<Processing_Periods> periods = null;
	SqlSession session = getSqlMapper().openSession();
	Processing_PeriodsMapper mapper = session.getMapper(Processing_PeriodsMapper.class);
	
	try {
		periods = mapper.selectByRange(startDate , endDate);

	} catch (Exception ex) {
		ex.printStackTrace();
	} finally {
		session.close();
	}
	return periods;
}
	
	
	public static void main(String args[]){
		
	}
		


}
