/*
 * this class
 * 
 * */



package org.elmis.facility.network;

import org.elmis.facility.connections.DatabaseSwitcher;
import org.hsqldb.Server;

public class EmbeddedServer {

	int port = 5000;

	String db_name = "elmis_facility";	 
	private String location = "database/elmis_facility";		
	Server server;


	public EmbeddedServer(){


	}

	public  void startup(){

		//set the System properties for hsqlbd
		System.setProperty("dburl", "jdbc:hsqldb:hsql://localhost:5000/elmis_facility");
		System.setProperty("dbdriver","org.hsqldb.jdbcDriver");

		System.setProperty("dbhost","localhost");

		System.setProperty("dbport","5000");
		System.setProperty("dbname","elmis_facility");
		System.setProperty("dbuser","sa");
		System.setProperty("dbpassword","");

		this.server = new Server();
		this.server.setDatabaseName(0, this.db_name);

		this.server.setDatabasePath(0, "file:" + this.location + 
				";runtime.gc_interval=10000;hsqldb.default_table_type=cached;");
		this.server.setLogWriter(null);
		this.server.setErrWriter(null);
		this.server.setPort(this.port);
		this.server.setRestartOnShutdown(true);
		this.server.start();

	}
	public void switchToEmbedded()
	{
		/*if (DatabaseSwitcher.switchToSqlite())
			System.out.println("Now embedded...");*/
	}
}
