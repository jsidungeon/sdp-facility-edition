package org.elmis.facility.dao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.Dispensary_Store_Physical_CountMapper;
import org.elmis.facility.domain.dao.Elmis_Dar_TransactionsMapper;
import org.elmis.facility.domain.dao.Elmis_PatientMapper;
import org.elmis.facility.domain.dao.PatientDao;
import org.elmis.facility.domain.dao.SatelliteRandR_Mapper;
import org.elmis.facility.domain.dao.artdispenseProductQtyMapper;
import org.elmis.facility.domain.dao.arvdispensarystockstatusMapper;
import org.elmis.facility.domain.model.Dispensary_Store_Physical_Count;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Operators;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Geographic_Zones;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.SatelliteRandR;
import org.elmis.facility.domain.model.arvdispensarystockstatus;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.reports.utils.CalendarUtil;
public class ARVDispensingDAO {
	
	private String stra ;
	private SqlSessionFactory sqlSessionFactory; 
		
	public ARVDispensingDAO(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	

	@SuppressWarnings("unchecked")
	public Facility getFacility(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			 Facility fac = session.selectOne("getFacility");
			 //String operator = fac.
			// System.out.println(fac.getCode().toString() + "FacilityCode Noted");
			return fac;
		} finally {
			session.close();
		}
	}

	
	@SuppressWarnings("unchecked")
		
	public Facility_Types selectAllwithTypes(Facility facility){

		SqlSession session = sqlSessionFactory.openSession();
		String facCode = (facility.getCode()).trim();
		try {
			Facility_Types facttypes = session.selectOne("getFacilityTypes",facCode);
			
			
			return facttypes;
		} finally {
			session.close();
		}
	}

	
	
	
	@SuppressWarnings("unchecked")
	public Facility_Operators selectAllwithOps(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Facility_Operators facOps = session.selectOne("getFacilityOps");
			
			
			return facOps;
		} finally {
			session.close();
		}
	}
	public  ProductQty
	doselectqtyarvdispense (String pcode, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
     ProductQty prodqtyobj = mapper.getcurrentarvproductqty(pcode,dpid);  
     return prodqtyobj;
       }finally{
			
			session.close();
		}
     }	
	
	
	public  List<ProductQty>doselectProductQtydispense (String pcode,Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
     List<ProductQty> prodqtyobj = mapper.getcurrentListproductqty(pcode,dpid);  
     return prodqtyobj;
      }finally{
			
			session.close();
		}
     }	
	
	
	public  
	void doupdatearvdispenseqty (String prodcode,Double double1, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			System.out.println(prodcode + "qty params" + double1);
	 
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updateproductqty(prodcode,double1,dpid); 
        // mapper.updateproductqty(prodcode,1); 
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			session.close();
		}
         
       
     }	
	
	public  
	void doupdatearvdispenseqtybyPC (String prodcode,Double double1, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			System.out.println(prodcode + "qty params" + double1);
	 
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updateproductqtybyPC(prodcode,double1,dpid); 
        // mapper.updateproductqty(prodcode,1); 
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			session.close();
		}
         
       
     }	
	
	
	public void doupdatearvdispenseqtyless (String prodcode,Double positive,String location, double d, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			System.out.println(prodcode + "qty params" + positive);
		
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updatedispenaryproductqtyless(prodcode,positive,location,d,dpid); 
        // mapper.updateproductqty(prodcode,1); 
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			session.close();
		}
         
       
     }	
	
	
	
	
		
	//
	public  List<Elmis_Dar_Transactions>
	doselectsumDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumARVdar(startdate,enddate);  
     return listsumdar;
		} finally {
			session.close();
		}
     }	
	
	
	public  List<Elmis_Dar_Transactions>
	doselectfinalsumDARarv (java.util.Date startdate,java.util.Date enddate, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectfinalsumARVproducts(startdate,enddate,dpid);  
     return listsumdar;
		} finally {
			session.close();
		}
     }	
	
	
	/*public List<Double>
	doselectfinalsumDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Double> listsumdar = mapper.selectfinalsumARVproducts(startdate,enddate);  
     return listsumdar;
		} finally {
			session.close();
		}
     }	*/
	
	
	
	
	public  List<Elmis_Dar_Transactions>
	doselectsumsingledoseDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumsingledoseARVdar(startdate,enddate);  
     return listsumdar;
     
		} finally {
			session.close();
		}
     }	
	public  List<Elmis_Dar_Transactions>
	doselectsumliquidpowderDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumliquidpowderdoseARVdar(startdate,enddate);  
     return listsumdar;
		} finally {
			session.close();
		}
     }	
	public  List<Elmis_Dar_Transactions>
	doselectsumCotimoxazoleDARarv (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listsumdar = mapper.selectsumcotrimoxazoledoseARVdar(startdate,enddate);  
     return listsumdar;
		} finally {
			session.close();
		}
     }	
	
	public  List<Elmis_Dar_Transactions>
	doselectpatientarvDispensationparameter (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listpatientarvdar = mapper.selectsumcotrimoxazoledoseARVdar(startdate,enddate);  
     return listpatientarvdar;
		} finally {
			session.close();
		}
     
     }	
	
	
	public  List<Elmis_Dar_Transactions>
	doselectpatientarvDispensation (String myART) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listpatientarvdar = mapper.selectARVpatientdartransactions(myART);  
     return listpatientarvdar;
		} finally {
			session.close();
		}
     }	
		
	
	public  Elmis_Patient dogetcurrentARVclientbyART (String art) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     Elmis_Patient client = mapper.selectByARTnumber(art);  
     return client;
		} finally {
			session.close();
		}
     
     }
	
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientList (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		List<Elmis_Patient> clientlist = null;
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
        clientlist = mapper.selectARTclientsAdvancedsearch(dob1,dob2,fname,lname,sex);  
		} finally {
			session.close();
		}
		return clientlist;
     }
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientListLastname (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     List<Elmis_Patient> clientlist = mapper.selectARTclientsAdvancedsearchLastname(dob1,dob2,fname,lname,sex);  
     return clientlist;
		} finally {
			session.close();
		}
     
     }
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientListbyDOB (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     List<Elmis_Patient> clientlist = mapper.selectARTclientsAdvancedsearchbyDOB(dob1,dob2,fname,lname,sex);  
     return clientlist;
		} finally {
			session.close();
		}
     
     }
	
	
	
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientListFirstname (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     List<Elmis_Patient> clientlist = mapper.selectARTclientsAdvancedsearchFirstname(dob1,dob2,fname,lname,sex);  
     return clientlist;
		} finally {
			session.close();
		}
     
     }
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientListbyfirstLastDOB (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     List<Elmis_Patient> clientlist = mapper.selectARTclientsAdvancedsearchFirstname(dob1,dob2,fname,lname,sex);  
     return clientlist;
		} finally {
			session.close();
		}
     
     }
	
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientListbysex (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     List<Elmis_Patient> clientlist = mapper.selectARTclientsAdvancedsearchSex(dob1,dob2,fname,lname,sex);  
     return clientlist;
		} finally {
			session.close();
		}
     
     }
	
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientListbyall (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     List<Elmis_Patient> clientlist = mapper.selectARTclientsAdvancedsearchall(dob1,dob2,fname,lname,sex);  
     return clientlist;
		} finally {
			session.close();
		}
     
     }
	
	
	
	
	public  List<Elmis_Patient>
	dogetcurrentARVclientListhsqldb (java.util.Date dob1,java.util.Date dob2, String fname,String lname,String sex) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
     List<Elmis_Patient> clientlist = mapper.selectARTclientsAdvancedsearchhsqldb(dob1,dob2,fname,lname,sex);  
     return clientlist;
		} finally {
			session.close();
		}
     
     }
	
	
	
	public  Elmis_Patient
	dogetcurrentARVclientbyNRC (String nrc) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
        Elmis_Patient client = mapper.selectByNRCnumber(nrc);  
         return client;
		} finally {
			session.close();
		}
     }
	
	/*public Elmis_Patient  selectARTnumber(String art){

        SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Elmis_Patient patientdetail = session.selectOne("selectByARTnumber",art);
						
			return patientdetail;
		} finally {
			session.close();
		}
	}*/
	
	public  void  insertdispensedProducts(Elmis_Dar_Transactions stock){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Elmis_Dar_TransactionsMapper.insert",stock);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	public  void  Registerpatient(Elmis_Patient patient){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Elmis_PatientMapper.insert",patient);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	public  List<SatelliteRandR>
	dogetcurrentListSatelliteRandR (java.util.Date datefrom,java.util.Date dateto) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
			SatelliteRandR_Mapper mapper = session.getMapper(SatelliteRandR_Mapper.class);
            List<SatelliteRandR> satelliteRandRdataitems = mapper.getcurrentListSatelliteRandR(datefrom,dateto);  
     return satelliteRandRdataitems;
		} finally {
			session.close();
		}
     
     }
	
	public  List<arvdispensarystockstatus>
	dogetcurrentProductStatus (String productcode, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
			arvdispensarystockstatusMapper mapper = session.getMapper(arvdispensarystockstatusMapper.class);
            List<arvdispensarystockstatus> arvproductstatusitems = mapper.getARVproductstatus(productcode,dpid);  
     return arvproductstatusitems;
		} finally {
			session.close();
		}
     
     }
	
	
	public  void  Recordproducttransactionstatus(arvdispensarystockstatus arvprod){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.arvdispensarystockstatusMapper.insert",arvprod);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	

	
	public  void  RecordSatelliteRandR(SatelliteRandR randr){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.SatelliteRandR_Mapper.insert",randr);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	public  void
	doupdateARVClientdetails (String nrc,java.util.Date dob, String fname,String lname,String sex,String artNumber) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		new PatientDao().updatePatient(artNumber, nrc, new CalendarUtil().getSqlDate(dateFormat.format(dob)), fname, lname, sex);
		/*SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
        mapper.updateByARTnumber(nrc,dob,fname,lname,sex,artnumber);  
        session.commit();
		}finally{
	          session.close();
			}*/
   
     }
	
	
	
	
	public  void  UpdateRegisteredpatient(Elmis_Patient patient){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Elmis_PatientMapper.updateByARTnumber",patient);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
  
	public  List<Elmis_Dar_Transactions>
	dogetcurrentfinalARVdar (java.util.Date startdate,java.util.Date enddate,Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectfinalARVdar(startdate,enddate,dpid);  
     return listdar;
		}finally{
	          session.close();
			}
   
     }
	

	public  List<Elmis_Dar_Transactions>
	dogetcurrentARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectARVdar(startdate,enddate);  
     return listdar;
		}finally{
	          session.close();
			}
   
     }
	
	
	
	public  List<Elmis_Dar_Transactions>
	dosingledoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
        List<Elmis_Dar_Transactions> listdar = mapper.selectARVdarsingledose(startdate,enddate);  
          return listdar;
		}finally{
	          session.close();
			}
     
     }
	
	
	public  List<Elmis_Dar_Transactions>
	doliquidpowderdoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectARVdarliquidpowderdose(startdate,enddate);  
     return listdar;
		}finally{
	          session.close();
			}
     
     }
	
	public  List<Elmis_Dar_Transactions>
	docotrimoxazoledoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try {
		
		Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listdar = mapper.selectARVdarcotrimoxazoledose(startdate,enddate);  
     return listdar;
		}finally{
	          session.close();
			}
     }
	
	
	public  List<Elmis_Dar_Transactions>
	dogetcurrentAlldoseARVdar (java.util.Date startdate,java.util.Date enddate) {
		 SqlSession session = sqlSessionFactory.openSession();
		try {
		
		  Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
          List<Elmis_Dar_Transactions> listdar = mapper.selectAllARVdosedarproducts(startdate,enddate);  
          return listdar;
          
		}finally{
          session.close();
		}
     }
		
	/**
	 * Returns the list of all Geo Zone name from the database.
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Geographic_Zones selectAllwithGeozones(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Geographic_Zones geozone = session.selectOne("getAllGeoZones");
			
			
			return geozone;
		} finally {
			session.close();
		}
	}	
	
	public void updateBalance(Double qtyDeducted,String productCode,Integer dispensationUnitId){
		SqlSession session = sqlSessionFactory.openSession();
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class); 
		
		try {
			mapper.updateProductQtyBalance(qtyDeducted, productCode, dispensationUnitId);

		} finally {
			session.commit();
			session.close();
		}
		
	}
	
	
	
	@SuppressWarnings("rawtypes")
	public  List  SelectARVdispensaryproductsAdjustPhysicalcountbalances(HashMap map){
		
	       SqlSession session = sqlSessionFactory.openSession();
	     	List  arvdispList;
	       String 	pcode = map.get("prodcode").toString();
	       Integer dpid = Integer.parseInt( map.get("dpid").toString())	;
	     	
			try {
				
			 artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
			 arvdispList = mapper.getARVproductAdjustmentsPhysicalcountBalance(pcode,dpid);  
		     return arvdispList;
		      }finally{
					
					session.close();
				}
				
			
			
			
		}
	

	public  List<Elmis_Dar_Transactions>
	docheckifOtherlasttransactionoftheMonth(java.util.Date startdate,java.util.Date enddate,String valueA, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			Elmis_Dar_TransactionsMapper mapper = session.getMapper(Elmis_Dar_TransactionsMapper.class);
     List<Elmis_Dar_Transactions> listbal = mapper.checkifOtherlasttransactionoftheMonth(startdate,enddate,valueA,dpid);  
     return listbal;
		}finally{
			session.close();
		}
     }
	
	
	
	
	
	public  
	void doupdateARVstorephysicalcountBoolean(String  pcode, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			
	
			Dispensary_Store_Physical_CountMapper mapper = session.getMapper(Dispensary_Store_Physical_CountMapper.class);
        mapper.updatearvstorephysicalcountBoolean(pcode,dpid); 
        session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			session.close();
			
		}
         
       
     }	
	
	
	
	public  List<Dispensary_Store_Physical_Count>
	docheckifPCdateOtherlasttransactionoftheMonth(java.util.Date startdate,java.util.Date enddate,String valueA, Integer dpid) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			Dispensary_Store_Physical_CountMapper mapper = session.getMapper(Dispensary_Store_Physical_CountMapper.class);
     List<Dispensary_Store_Physical_Count> listbal = mapper.selectMaxPCdate(startdate,enddate,valueA,dpid);  
     return listbal;
		}finally{
			session.close();
		}
     }
	
	
}
