package org.elmis.facility.labs;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

import org.elmis.facility.domain.dao.EquipmentDao;
import org.elmis.facility.domain.dao.LabTestDao;
import org.elmis.facility.reporting.generator.ReportGenerator;
import org.elmis.facility.reporting.model.Equipment;
import org.elmis.facility.reporting.model.LabTest;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.utils.SdpStyleSheet;

public class EquipmentInformationJd extends JDialog implements ListSelectionListener {

	private final JPanel contentPanel = new JPanel();
	private final JButton saveButton;
	private int previousRow;
	private int remarkRowSelected;
	private boolean save = false;
	private int machineRowSelected; //Row selected from equipment table
	private int reagentRowSelected; //Row selected from reagent table
	private Map<String, Integer> reagentTableData = new HashMap<>();
	private Map<String, String> reagentTableRemarks = new HashMap<>();
	private Map<String, Character> equipmentMap = new HashMap<>();
	private boolean confirmStatus = false;
	private final static Cursor busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final static Cursor defaultCursor = Cursor.getDefaultCursor();
	private JCheckBox chckbxEmergency = new JCheckBox("<html><font color=white>Non-Emergency Order</font><html>");
	private boolean isNonEmergency = false; // this variable determines whether a
	// non-emergency report or emergency
	// report will be submitted
	private CalendarUtil cal = new CalendarUtil();
	private String frmDate; // reporting period begin date
	private String toDate; // reporting period end date
	private JTable machineTable;
	private String[][] data = null;
	private String[] machineColumnNames = { "<HTML><b>Equipment<br>Code</b></html>",
			"<HTML><b>Machine Name</b></html>",
			"<html><b>Equipment<br>Functioning (Y/N)</b></html>",
	"<html><b>Days<br>Out</b></html>" };
	private String[] reagentColumnNames = { "<HTML><b>Reagent<br>Code</b></html>",
			"<HTML><b>Test</b></html>",
			"<html><b>Test<br>Count</b></html>","<html><b>Remarks</b></html>" };
	private DefaultTableModel machineTableModel = new DefaultTableModel(data,machineColumnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {

			return false;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 4)
				return String.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};
	private DefaultTableModel reagentTableModel = new DefaultTableModel(data,reagentColumnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {
			if (column == 2)
				return true;
			else if (column == 3)
				return true;
			else
				return false;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2)
				return Integer.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};
	private JTextField frmTextField;
	private JTextField toTextField;
	private JTable reagentsTable;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			EquipmentInformationJd dialog = new EquipmentInformationJd(new javax.swing.JFrame(), true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public EquipmentInformationJd(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setTitle("Monthly Test Numbers & Equipment Information");
		setIconImage(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/appicon.png")).getImage());
		setBounds(100, 100, 1128, 560);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		SdpStyleSheet.configJDialogBackground(this);
		//contentPanel.setBackground(new java.awt.Color(102, 102, 102));
		chckbxEmergency.setBackground(new java.awt.Color(102, 102, 102));
		frmDate = cal.getFirstDayOfReportingMonth();
		toDate = cal.getLastDayOfReportingMonth();
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel panel = new JPanel();
			SdpStyleSheet.configJPanelBackground(panel);
			//panel.setBackground(new java.awt.Color(102, 102, 102));
			panel.setBounds(0, 0, 1152, 474);
			contentPanel.add(panel);
			panel.setLayout(null);

			JLabel titleLbl = new JLabel(
					"Monthly Test Numbers and Equipment Information");
			SdpStyleSheet.configTitleJLabel(titleLbl);
			/*titleLbl.setFont(new Font("Tahoma", Font.BOLD, 13));
			titleLbl.setForeground(new java.awt.Color(255, 255, 255));*/
			titleLbl.setBounds(10, 11, 363, 14);
			panel.add(titleLbl);

			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 97, 503, 373);
			panel.add(scrollPane);
			List<Equipment> equipmentList = new EquipmentDao().getFacilityApprovedEquipment();
			{
				machineTable = new JTable()
				{
					public String getToolTipText(MouseEvent e) {
						return "<html><b><font color=red size=4>Double click to change equipment status</font></b><html>";
					}
				};
				//machineTable.setFont(new java.awt.Font("Ebrima", 0, 14));
				SdpStyleSheet.configTable(machineTable);
				machineTable.setModel(machineTableModel);

				for (int i = 0; i < equipmentList.size(); i++) {
					String equipCode = equipmentList.get(i).getEquipCode().trim();
					equipmentMap.put(equipCode, equipmentList.get(i).getTestsHandled());
					machineTableModel.addRow(new String[equipmentList.size()]);
					machineTable.getModel().setValueAt(equipCode
							, i, 0);
					machineTable.getModel().setValueAt(
							equipmentList.get(i).getName().trim(), i, 1);
					if (equipmentList.get(i).isFunctional())
						machineTable.getModel().setValueAt("YES", i, 2);
					else
						machineTable.getModel().setValueAt("NO", i, 2);
				}
				machineTable.setFillsViewportHeight(true);
				machineTable.setCellSelectionEnabled(true);
				machineTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				machineTable.getTableHeader().setReorderingAllowed(false);
				machineTable.getTableHeader().setPreferredSize(new Dimension(30, 65));
				machineTable.getColumnModel().getColumn(0).setPreferredWidth(80);
				machineTable.getColumnModel().getColumn(1).setPreferredWidth(280);
				machineTable.getColumnModel().getColumn(2).setPreferredWidth(70);
				machineTable.getColumnModel().getColumn(3).setPreferredWidth(55);
				machineTable.setRowHeight(35);
				scrollPane.setViewportView(machineTable);
				chckbxEmergency.setFont(new Font("Ebrima", Font.PLAIN, 12));

				chckbxEmergency
				.addItemListener(new java.awt.event.ItemListener() {
					public void itemStateChanged(
							java.awt.event.ItemEvent evt) {
						chckbxEmergencyCheckBoxItemStateChanged(evt);
					}
				});
				chckbxEmergency.setBounds(901, 47, 171, 23);
				panel.add(chckbxEmergency);

				frmTextField = new JTextField();
				frmTextField.setEditable(false);
				frmTextField.setBounds(63, 48, 86, 20);
				panel.add(frmTextField);
				frmTextField.setColumns(10);

				toTextField = new JTextField();
				toTextField.setEditable(false);
				toTextField.setBounds(186, 48, 86, 20);
				panel.add(toTextField);
				toTextField.setColumns(10);
				frmTextField.setText(frmDate);
				toTextField.setText(toDate);
				JLabel lblFrom = new JLabel("From:");
				SdpStyleSheet.configOtherJLabel(lblFrom);
				/*lblFrom.setFont(new Font("Tahoma", Font.BOLD, 13));
				lblFrom.setForeground(new java.awt.Color(255, 255, 255));*/
				lblFrom.setBounds(10, 51, 46, 14);
				panel.add(lblFrom);

				JLabel lblTo = new JLabel("To:");
				lblTo.setBounds(159, 51, 30, 14);
				SdpStyleSheet.configOtherJLabel(lblTo);
				/*lblTo.setFont(new Font("Tahoma", Font.BOLD, 13));
				lblTo.setForeground(new java.awt.Color(255, 255, 255));*/
				panel.add(lblTo);

				JScrollPane scrollPane_1 = new JScrollPane();
				scrollPane_1.setBounds(535, 97, 565, 373);
				panel.add(scrollPane_1);

				reagentsTable = new JTable();
				SdpStyleSheet.configTable(reagentsTable);
				//reagentsTable.setFont(new java.awt.Font("Ebrima", 0, 14));
				reagentsTable.setModel(reagentTableModel);
				reagentsTable.getColumnModel().getColumn(2).setCellRenderer(new ErrorCellRenderer());
				reagentsTable.getColumnModel().getColumn(3).setCellEditor(new RemarkTextAreaCellEditor());
				reagentsTable.setFillsViewportHeight(true);
				reagentsTable.setCellSelectionEnabled(true);
				reagentsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				reagentsTable.getTableHeader().setReorderingAllowed(false);
				reagentsTable.getTableHeader().setPreferredSize(new Dimension(30, 65));
				reagentsTable.getColumnModel().getColumn(0).setPreferredWidth(65);
				reagentsTable.getColumnModel().getColumn(1).setPreferredWidth(290);
				reagentsTable.getColumnModel().getColumn(2).setPreferredWidth(58);
				//reagentsTable.getColumnModel().getColumn(3).setPreferredWidth(55);
				reagentsTable.getColumnModel().getColumn(3).setPreferredWidth(125);
				//reagentsTable.setRowHeight(55);
				scrollPane_1.setViewportView(reagentsTable);
				{
					JPanel buttonPane = new JPanel();
					buttonPane.setBackground(new java.awt.Color(102, 102, 102));
					buttonPane.setBounds(0, 473, 1112, 54);
					contentPanel.add(buttonPane);
					{
						JButton cancelButton = new JButton("Cancel");
						SdpStyleSheet.configCancelButton(this, cancelButton);
						/*cancelButton.setFont(new java.awt.Font("Ebrima", 1, 12));
						cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Cancel.png")));*/
						cancelButton.setBounds(844, 11, 119, 36);
						cancelButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								dispose();
							}
						});
						{
							JButton viewButton = new JButton("View Report");
							SdpStyleSheet.configViewButton(this, viewButton);
							/*viewButton.setFont(new java.awt.Font("Ebrima", 1, 12));
							viewButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/View report.png")));*/
							viewButton.setBounds(537, 11, 156, 36);
							viewButton.addActionListener(new ActionListener() {
								public void actionPerformed(ActionEvent arg0) {
									try {
										EquipmentInformationJd.this.setCursor(busyCursor);
										new ReportGenerator().generateLabEquipInfoReport(
												frmDate, toDate,
												EquipmentInformationJd.this);
									} finally {
										EquipmentInformationJd.this
										.setCursor(defaultCursor);
									}
								}
							});
							buttonPane.setLayout(null);
							viewButton.setActionCommand("viewReport");
							buttonPane.add(viewButton);
							getRootPane().setDefaultButton(viewButton);
						}
						cancelButton.setActionCommand("cancelButton");
						buttonPane.add(cancelButton);
					}

					saveButton = new JButton("Save Test");
					SdpStyleSheet.configSaveButton(this, saveButton);
					/*saveButton.setFont(new java.awt.Font("Ebrima", 1, 12));
					saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Save icon.png")));*/
					saveButton.setEnabled(false);
					saveButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							int selectedMachineCodeRow = machineTable.getSelectedRow();
							String selectedMachineCode = ""+machineTable.getValueAt(selectedMachineCodeRow, 0);
							LabTestDao labTestDao = new LabTestDao();
							labTestDao.deleteSubmission(selectedMachineCode, cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
							saveDataEquipmentFunctional(selectedMachineCode, labTestDao);
							save = false;
							reagentTableData.clear();
							reagentTableRemarks.clear();
							saveButton.setEnabled(false);
							//machineTable.requestFocus();
							//machineTable.changeSelection(selectedMachineCodeRow + 1,1,false, false);
						}
					});
					saveButton.setActionCommand("savebutton");
					saveButton.setBounds(973, 11, 128, 36);
					buttonPane.add(saveButton);
				}

				reagentsTable.getModel().addTableModelListener(
						new TableModelListener() {

							@Override
							public void tableChanged(TableModelEvent arg0) {

							}
						});


				machineTable.getSelectionModel().addListSelectionListener(this);
				machineTable.getModel().addTableModelListener(new TableModelListener() {

					@Override
					public void tableChanged(TableModelEvent arg0) {


					}
				});

				machineTable.addMouseListener(new MouseAdapter()
				{
					public void mouseClicked(MouseEvent e) {

						if (e.getClickCount() == 2) {
							String equipmentName = machineTable.getValueAt(machineRowSelected, 1).toString();
							String equipmentCode = machineTable.getValueAt(machineRowSelected, 0).toString();
							ReportFaultJd reportFaultJd = new ReportFaultJd(
									EquipmentInformationJd.this, true, equipmentCode, equipmentName,
									confirmStatus, toDate,  machineTable.getValueAt(machineRowSelected, 2).toString());
							reportFaultJd.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
							reportFaultJd.setVisible(true);

							if (reportFaultJd.getConfirmStatus()) {
								int daysOut = reportFaultJd.getNumberOfDaysOut();
								String functionalityStatus = reportFaultJd.getFunctionalityStatus();
								machineTable.setValueAt(daysOut,machineRowSelected, 3);
								machineTable.setValueAt(functionalityStatus,	machineRowSelected, 2);
								if (functionalityStatus.equalsIgnoreCase("NO"))
								{
									if (reagentsTable.getRowCount() != 0)
										clearTable(reagentsTable);
								}
								int ack = JOptionPane.showConfirmDialog(EquipmentInformationJd.this, "Do you want to save "+equipmentName+" data?", "Save "+equipmentCode+" Data", JOptionPane.YES_NO_OPTION);
								if (ack == JOptionPane.YES_OPTION)
								{
									boolean submissionStatus = new LabTestDao().checkSubmission(equipmentCode, cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
									if (submissionStatus)
									{
										int ackSubmission = JOptionPane.showConfirmDialog(EquipmentInformationJd.this, "Reporting on "+equipmentName+" was already captured. Do you want to over-write it?", "Over write "+equipmentCode+" Data", JOptionPane.YES_NO_OPTION);
										if (ackSubmission == JOptionPane.YES_OPTION)
										{
											new LabTestDao().deleteSubmission(equipmentCode, cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
											saveDataEquipmentNotFunctional(equipmentCode, daysOut);
											machineTable.setValueAt(functionalityStatus,	machineRowSelected, 2);
										}
									}
									else{
										saveDataEquipmentNotFunctional(equipmentCode, daysOut);
									}
								}
								else
								{
									machineTable.setValueAt(reportFaultJd.getFunctionalityStatus(),machineRowSelected, 2);
									machineTable.setValueAt("",machineRowSelected, 3);
									if (reportFaultJd.getFunctionalityStatus().equalsIgnoreCase("YES"))
										populateReagentTable(equipmentCode);
								}
							}
							else
							{
								machineTable.setValueAt(reportFaultJd.getFunctionalityStatus(),machineRowSelected, 2);
								machineTable.setValueAt("",machineRowSelected, 3);
								if (reportFaultJd.getFunctionalityStatus().equalsIgnoreCase("YES"))
									populateReagentTable(equipmentCode);

							}
						}
					}
				});
			}
		}
		setLocationRelativeTo(parent);
	}

	protected void chckbxEmergencyCheckBoxItemStateChanged(ItemEvent evt) {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			frmTextField.setText("");
			frmTextField.setText(cal.getFirstDayOfCurrentMonth());
			toTextField.setText("");
			toTextField.setText(cal.getCurrentDate());
			isNonEmergency = true;
			chckbxEmergency
			.setText("<html><b><font color=red>Emergency order</font><b></html>");
		} else {
			frmTextField.setText("");
			frmTextField.setText(cal.getFirstDayOfReportingMonth());
			toTextField.setText(cal.getLastDayOfReportingMonth());
			isNonEmergency = false;
			chckbxEmergency.setText("<html><font color=white>Non-Emergency Order</font><html>");
		}

	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) // mouse button not released yet
			return;
		previousRow = machineRowSelected;
		machineRowSelected = machineTable.getSelectedRow();
		String previousMachineCode = machineTable.getValueAt(previousRow, 0).toString();
		if (save)
		{
			LabTestDao labTestDao = new LabTestDao();
			int ack = JOptionPane.showConfirmDialog(EquipmentInformationJd.this, "Do you want to save "+machineTable.getValueAt(previousRow, 1)+" data?", "Save "+previousMachineCode+" Data", JOptionPane.YES_NO_OPTION);
			if (ack == JOptionPane.YES_OPTION)
			{
				boolean submissionStatus = new LabTestDao().checkSubmission(previousMachineCode, cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
				if (submissionStatus)
				{
					int ackSubmission = JOptionPane.showConfirmDialog(EquipmentInformationJd.this, "Reporting on "+machineTable.getValueAt(previousRow, 1)+" was already captured. Do you want to over-write it?", "Over write "+previousMachineCode+" Data", JOptionPane.YES_NO_OPTION);
					if (ackSubmission == JOptionPane.YES_OPTION)
					{
						new LabTestDao().deleteSubmission(previousMachineCode, cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
						saveDataEquipmentFunctional(previousMachineCode, labTestDao);
					}
				}
				else{
					saveDataEquipmentFunctional(previousMachineCode, labTestDao);
					save = false;
					reagentTableData.clear();
					reagentTableRemarks.clear();
				}
			}
			else
				save = false;
		}
		
		if (machineRowSelected < 0) // true when clearSelection
			return;
		int col = machineTable.getSelectedColumn();

		if (col < 0) // true when clearSelection
			return;
		String machineCode = (String) machineTable.getValueAt(machineRowSelected, 0);
		String machineStatus = (String) machineTable.getValueAt(machineRowSelected, 2);
		if (reagentsTable.getRowCount() != 0)
			clearTable(reagentsTable);
		if (machineStatus.equalsIgnoreCase("Yes")) {
			populateReagentTable(machineCode);
		}
	}
	private void clearTable(JTable table)
	{
		((DefaultTableModel)table.getModel()).setRowCount(0);
	}
	private void populateReagentTable(String machineCode)
	{
		List<LabTest> labTestlist = new LabTestDao().getTests(machineCode);
		if (equipmentMap.get(machineCode) == 'M')
		{
			for (int i = 0; i < labTestlist.size(); i++) {
				reagentTableModel.addRow(new String[labTestlist.size()]);
				reagentsTable.getModel().setValueAt(
						labTestlist.get(i).getReagentCode().trim(), i, 0);
				reagentsTable.getModel().setValueAt(
						labTestlist.get(i).reagentName.trim(), i, 1);
			}
		}
		else{
			LabTest testData = new LabTest();
			testData.setMachineCode(machineCode);
			testData.setIsEquipmentFunctioning(true);
			testData.setIsEmergency(isNonEmergency);
			testData.setBeginDate(cal.getSqlDate(cal.changeDateFormat(frmTextField.getText())));
			testData.setEndDate(cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
			EquipmentTestcountJd equipTestCount = new EquipmentTestcountJd(this, true, testData);
			
			equipTestCount.setVisible(true);
			LabTestDao labTestDao = new LabTestDao();
			for (LabTest l : labTestlist){
				testData.setReagentCode(l.getReagentCode());
				testData.setTestCount(0);
				testData.setRemarks("");
				labTestDao.submitReagentMonthlyData(testData);
			}
				
		}
	}
	
	class ErrorCellRenderer extends DefaultTableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
					row, column);
			if (column == 2) {
				if (value instanceof Integer) {
					component.setBackground(Color.WHITE);
					String reagentCode = table.getValueAt(row, 0).toString();
					reagentTableData.put(reagentCode, (Integer) value);
					saveButton.setEnabled(true);
					save = true;
				}
			}

			return component;
		}
	}
	class SubmittedCellRenderer extends DefaultTableCellRenderer {
		@Override
		public Component getTableCellRendererComponent(JTable table, Object value,
				boolean isSelected, boolean hasFocus, int row, int column) {
			Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus,
					row, column);
			//System.out.println("Setting background color to green...");
			component.setBackground(Color.GREEN);

			return component;
		}
	}
	public class RemarkTextAreaCellEditor extends AbstractCellEditor implements TableCellEditor {

		private RemarksTextArea remarksTextArea;
		private String reagentCode;

		public RemarkTextAreaCellEditor () {
			remarksTextArea = new RemarksTextArea();
		}

		public Component getTableCellEditorComponent(JTable table,
				Object value, boolean isSelected, int row, int col) {
			remarkRowSelected = row;
			remarksTextArea.setLineWrap(true);
			JScrollPane remarksScrollPane = new JScrollPane(remarksTextArea);
			remarksScrollPane
			.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

			remarksTextArea.setText((String) value);
			reagentCode = table.getValueAt(row, 0).toString();
			return remarksScrollPane;
		}

		public Object getCellEditorValue() {
			String reagentRemark = ((JTextArea)remarksTextArea).getText();
			reagentTableRemarks.put(reagentCode, reagentRemark);
			return ((JTextArea)remarksTextArea).getText();
		}
	}
	class RemarksTextArea extends JTextArea{
		public RemarksTextArea()
		{
			addKeyListener(new KeyAdapter() {
				@Override
				public void keyPressed(KeyEvent evt) {
					if(evt.getKeyCode() == KeyEvent.VK_ENTER)
					{
						reagentsTable.requestFocus();
						reagentsTable.changeSelection(remarkRowSelected + 1,2,false, false);
						if (reagentsTable.isEditing())
							reagentsTable.getCellEditor().stopCellEditing();
					}
				}
			});
		}
	}
	private void saveDataEquipmentFunctional(String previousMachineCode, LabTestDao labTestDao)
	{
		LabTest testData = new LabTest();
		testData.setMachineCode(previousMachineCode);
		testData.setIsEquipmentFunctioning(true);
		testData.setIsEmergency(isNonEmergency);
		testData.setBeginDate(cal.getSqlDate(cal.changeDateFormat(frmTextField.getText())));
		testData.setEndDate(cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
		labTestDao.submitEquipmentMonthlyData(testData);
		for (Map.Entry<String, Integer> entry : reagentTableData.entrySet()){
			testData.setReagentCode(entry.getKey());
			testData.setTestCount(entry.getValue());
			testData.setRemarks(reagentTableRemarks.get(entry.getKey()));
			labTestDao.submitReagentMonthlyData(testData);
		}
	}
	private void saveDataEquipmentNotFunctional(String machineCode, int daysOut)
	{
		LabTest testData = new LabTest();
		testData.setMachineCode(machineCode);
		testData.setNumberDaysOut(daysOut);
		testData.setIsEquipmentFunctioning(false);
		testData.setIsEmergency(isNonEmergency);
		testData.setBeginDate(cal.getSqlDate(cal.changeDateFormat(frmTextField.getText())));
		testData.setEndDate(cal.getSqlDate(cal.changeDateFormat(toTextField.getText())));
		new LabTestDao().submitEquipmentMonthlyData(testData);
	}
}
