/*
 * AddUserJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.users;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.UsersDAO;
import org.elmis.facility.domain.model.Role_assignments;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.email.MailTrigger;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.tools.PopulateJCBox;
import org.elmis.facility.utils.SdpStyleSheet;

import com.oribicom.tools.publicMethods;

/**
 * 
 * @author __USER__
 */
public class AddSiteJD extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private users user;
	private static UsersDAO userDAO;
	MailTrigger userAdd = new MailTrigger();

	/** Creates new form AddUserJD */
	public AddSiteJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);

		initComponents();
		SdpStyleSheet.configJDialogBackground(this);
		this.setLocationRelativeTo(null);
		this.setSize(440, 455);
		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel2 = new javax.swing.JLabel();
		firstNameJTF = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		lastNameJTF = new javax.swing.JTextField();
		jobRoleJC = new javax.swing.JComboBox();
		jSeparator1 = new javax.swing.JSeparator();
		passwordJPF = new javax.swing.JPasswordField();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jLabel7 = new javax.swing.JLabel();
		confirmPasswordJPF = new javax.swing.JPasswordField();
		jLabel8 = new javax.swing.JLabel();
		jLabel9 = new javax.swing.JLabel();
		userNameJTF = new javax.swing.JTextField();
		jLabel10 = new javax.swing.JLabel();
		emailAddressJTF = new javax.swing.JTextField();
		jLabel1 = new javax.swing.JLabel();
		cellPhoneJTF = new javax.swing.JTextField();
		CancelJBnt = new javax.swing.JButton();
		AddUserJB = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Add New Dispensing Site");

		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		SdpStyleSheet.configJPanelBackground(jPanel1);

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("First Name");

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Last Name");

		jobRoleJC.setModel(new PopulateJCBox().FillRolesJCBox());

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Job Role");

		jLabel5.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel5.setForeground(new java.awt.Color(255, 255, 255));
		jLabel5.setText("Login Details");

		jLabel6.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel6.setForeground(new java.awt.Color(255, 255, 255));
		jLabel6.setText("User Name");

		jLabel7.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel7.setForeground(new java.awt.Color(255, 255, 255));
		jLabel7.setText("Password");

		jLabel8.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel8.setForeground(new java.awt.Color(255, 255, 255));
		jLabel8.setText("Confirm Password");

		jLabel10.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel10.setForeground(new java.awt.Color(255, 255, 255));
		jLabel10.setText("Email Address");

		jLabel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Cell Phone");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout.createSequentialGroup().addGap(
										10, 10, 10).addComponent(jLabel9,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										308,
										javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap())
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel2,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																100,
																Short.MAX_VALUE)
														.addComponent(jLabel3)
														.addComponent(jLabel4)
														.addComponent(jLabel10)
														.addComponent(jLabel1)
														.addComponent(
																jLabel6,
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(jLabel7)
														.addComponent(jLabel8))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																lastNameJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																203,
																Short.MAX_VALUE)
														.addComponent(
																jobRoleJC, 0,
																203,
																Short.MAX_VALUE)
														.addComponent(
																emailAddressJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																203,
																Short.MAX_VALUE)
														.addComponent(
																cellPhoneJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																203,
																Short.MAX_VALUE)
														.addComponent(
																userNameJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																203,
																Short.MAX_VALUE)
														.addComponent(
																passwordJPF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																203,
																Short.MAX_VALUE)
														.addComponent(
																confirmPasswordJPF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																203,
																Short.MAX_VALUE)
														.addComponent(
																firstNameJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																203,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(140, 140, 140))
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jSeparator1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												321,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(126, Short.MAX_VALUE))
						.addGroup(
								jPanel1Layout.createSequentialGroup()
										.addContainerGap()
										.addComponent(jLabel5).addContainerGap(
												347, Short.MAX_VALUE)));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { jLabel1, jLabel10, jLabel2, jLabel3,
						jLabel4, jLabel5, jLabel6, jLabel7, jLabel8 });

		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel2,
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																firstNameJTF,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(12, 12, 12)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel3,
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																lastNameJTF,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(24, 24, 24)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel4,
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																jobRoleJC,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel10,
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																emailAddressJTF,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel1,
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																cellPhoneJTF,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(18, 18, 18)
										.addComponent(
												jSeparator1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												13,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(jLabel5)
										.addGap(23, 23, 23)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																userNameJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(jLabel6))
										.addGap(12, 12, 12)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																passwordJPF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(jLabel7))
										.addGap(12, 12, 12)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																confirmPasswordJPF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(jLabel8))
										.addGap(42, 42, 42)
										.addComponent(jLabel9)
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jPanel1Layout
				.linkSize(javax.swing.SwingConstants.VERTICAL,
						new java.awt.Component[] { jLabel10, jLabel2, jLabel3,
								jLabel4 });

		CancelJBnt.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBnt.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBnt.setText("Cancel");
		CancelJBnt.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBntActionPerformed(evt);
			}
		});

		AddUserJB.setFont(new java.awt.Font("Ebrima", 1, 12));
		AddUserJB.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Reset password.png"))); // NOI18N
		AddUserJB.setText("Add User");
		AddUserJB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				AddUserJBActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jPanel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												418,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								layout
										.createSequentialGroup()
										.addContainerGap(120, Short.MAX_VALUE)
										.addComponent(AddUserJB)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(CancelJBnt).addGap(108,
												108, 108)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jPanel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												356,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																CancelJBnt)
														.addComponent(AddUserJB))
										.addContainerGap(12, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void CancelJBntActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		int ok = javax.swing.JOptionPane.showConfirmDialog(this,
				"Are you sure you want to cancel", "", 2, 1);
		// int ok =
		// publicMethods.confirmDialog("Are you sure you want to cancel");
		if (ok == 0) {
			this.dispose();
		}
	}

	private void AddUserJBActionPerformed(java.awt.event.ActionEvent evt) {

		user = new users();

		String confirm = new String(confirmPasswordJPF.getPassword()).trim();

		String passwordConfirm = new String(passwordJPF.getPassword()).trim();
		String password = new String(passwordJPF.getPassword()).trim();

		// Convert user password

		try {
			//	MessageDigest md = MessageDigest.getInstance("MD5");
			//	password = publicMethods.getHexString(md
			//			.digest(password.getBytes()));
			password = publicMethods.hash(password);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(AppJFrame.desktop, e
					.getMessage().toString());
		}

		user.setPassword(password);

		if (firstNameJTF.getText().equals("")
				|| lastNameJTF.getText().equals("")
				|| userNameJTF.getText().equals("")) {

			javax.swing.JOptionPane.showMessageDialog(this,
					"First Name , Last name and User Name can not be blank");
		}

		else

		if (jobRoleJC.getSelectedItem().toString().equalsIgnoreCase(
				"Select Job Role")) {

			javax.swing.JOptionPane.showMessageDialog(this,
					"Please Select Job Role");
		} else if (passwordJPF.getText().equals("")) {

			javax.swing.JOptionPane.showMessageDialog(this,
					"Please enter a password");
		} else

		if (passwordConfirm.equals(confirm)) {

			// Search to see if user name already taken

			//	String userNameSQL = "select user_name from users where user_name = '"
			//			+ userNameJTF.getText().trim() + "' and deleted = 'false'";

			// check if username already exists

			//	new PopulateJCBox().FillRolesJCBox();

			/*&	if ( ) {

					String userName = rs.getString("user_name");
					javax.swing.JOptionPane
							.showMessageDialog(
									this,
									"User name "
											+ userName
											+ " is already in use \nplease enter a different user name ");
					// System.out.println();

				}*/

			//&else {

			//get the id of the selected role
			//int selectedRole = jobRoleJC.getSelectedIndex();
			//int selectedRoleId = new PopulateJCBox().roleIdList[selectedRole];

			// add new user

			//user.setId(publicMethods.createGUID());

			user.setFirstname(firstNameJTF.getText());
			user.setLastname(lastNameJTF.getText());
			user.setUsername(userNameJTF.getText());
			user.setEmail(emailAddressJTF.getText());
			user.setCellphone(cellPhoneJTF.getText());
			user.setJobtitle(jobRoleJC.getSelectedItem().toString().trim());
			user.setActive(true);

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();
			SqlSession session1 = factory.openSession();
			
			
			
			//check if your name already taken 			
			try{
				
				List<users> userNameAlreadyTaken = session.selectList("selectByUserName",user.getUsername());
				
								
				//if userName is not already take 
				if(userNameAlreadyTaken.size()== 0){
					
					try {
						
						
						

						session.insert("insertSelectiveUser", user);

						session.commit();

						//refresh table 
						List<users> usersList = session.selectList("getAllUsers");

						UsersAdminTabJD.populateUserAdminTable(usersList);

						//get the id of the newly created user
						userDAO = new UsersDAO();
						users newlyCreatedUser = userDAO.getByUser(password, this.user
								.getUsername());

						//get the id of the selected role
						int selectedRole = jobRoleJC.getSelectedIndex();
						int selectedRoleId = new PopulateJCBox().roleIdList[selectedRole];

						//create role_assignment object
						Role_assignments roleAssignment = new Role_assignments();
						roleAssignment.setRoleid(selectedRoleId);
						roleAssignment.setUserid(newlyCreatedUser.getId());

						//persist to database
						session1.insert("insertSelectiveRole_assignments",
								roleAssignment);

						session1.commit();

					} finally {

						javax.swing.JOptionPane.showMessageDialog(this,
								" New user added");

						session.close();

					}

					this.dispose();
					
					
					
				}//else if userName is already taken 
				else
				{
					
					javax.swing.JOptionPane.showMessageDialog(this,"username : " + user.getUsername() +" is already taken, please select another user name" );
					
				}
				
				
				
			}finally{
				
				
				
			}

		
		

		} else {

			javax.swing.JOptionPane.showMessageDialog(null,
					" Passwords don't match");

		}

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				AddSiteJD dialog = new AddSiteJD(new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton AddUserJB;
	private javax.swing.JButton CancelJBnt;
	private javax.swing.JTextField cellPhoneJTF;
	private javax.swing.JPasswordField confirmPasswordJPF;
	private javax.swing.JTextField emailAddressJTF;
	private javax.swing.JTextField firstNameJTF;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JLabel jLabel8;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JComboBox jobRoleJC;
	private javax.swing.JTextField lastNameJTF;
	private javax.swing.JPasswordField passwordJPF;
	private javax.swing.JTextField userNameJTF;
	// End of variables declaration//GEN-END:variables

}