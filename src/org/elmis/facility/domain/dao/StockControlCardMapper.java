package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.domain.model.StockControlCardExample;

public interface StockControlCardMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int countByExample(StockControlCardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int deleteByExample(StockControlCardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int insert(StockControlCard record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int insertSelective(StockControlCard record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    List<StockControlCard> selectByExample(StockControlCardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    StockControlCard selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int updateByExampleSelective(@Param("record") StockControlCard record, @Param("example") StockControlCardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int updateByExample(@Param("record") StockControlCard record, @Param("example") StockControlCardExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int updateByPrimaryKeySelective(StockControlCard record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_stock_control_card
     *
     * @mbggenerated Mon Aug 12 11:57:47 CAT 2013
     */
    int updateByPrimaryKey(StockControlCard record);
}