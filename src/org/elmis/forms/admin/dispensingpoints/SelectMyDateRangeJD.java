

package org.elmis.forms.admin.dispensingpoints;

import java.text.SimpleDateFormat;

import org.elmis.facility.domain.model.HivTestDar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import org.elmis.facility.domain.model.HivTest;
import org.elmis.facility.domain.model.HivTestDar;
import org.elmis.facility.domain.model.HivTestLossesAdustments;
import org.elmis.facility.domain.model.HivTestPhysicalCount;
import org.elmis.facility.domain.model.HivTestProducts;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.main.gui.ProgressBarJD;
import org.elmis.facility.main.gui.ProgressBarRangeJD;
import org.elmis.facility.reporting.generator.ReportGenerator;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.tools.MessageDialog;
import org.elmis.facility.utils.SdpStyleSheet;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;
//import java.sql.Date;
//import java.sql.Date;
//import com.oribicom.tools.TableModel;
//import com.sun.jmx.snmp.Timestamp;

/**
 * 
 * @author __USER__
 */
@SuppressWarnings("serial")
public class SelectMyDateRangeJD extends javax.swing.JDialog {
	
	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	private static java.util.Date startDate;
	private static java.util.Date endDate;
	private java.util.Date d;
	private Date todayDate;
	private String newDateString;
	private String mydateformat = "dd-MM-yyyy";
	String[] columns_districts = { "Districts" };
	public String paramDistricts = null;
	List<My_Districts> districtsList = new LinkedList();
	private List<HivTest> hivTestList;
	private List<HivTest> hivTestListPrevDay;
	private List<HivTestDar> hivTestDarList;// = new LinkedList();
	private List<HivTestProducts> HivTestProductsList;
	private List<HivTestPhysicalCount> physicalCountList;
	private List<HivTestLossesAdustments> hivtestLossesAdjustmentList;
	private List<StockControlCard> sccList;
	
	private HivTestDar dar;

	/** Creates new form AddDispensingPointJD */
	public SelectMyDateRangeJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();

		//this.setSize(450, 370);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		SdpStyleSheet.configJPanelBackground(jPanel1);
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		dateChooser1 = new com.toedter.calendar.JDateChooser();
		dateChooser2 = new com.toedter.calendar.JDateChooser();
		cancelJBtn = new javax.swing.JButton();
		addDispensingPointJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Run HIV DAR for Selected Date");

		//jPanel1.setBackground(new java.awt.Color(1, 129, 130));
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jLabel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		SdpStyleSheet.configOtherJLabel(jLabel1);
		//jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Select Date1 Here:");
		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		SdpStyleSheet.configOtherJLabel(jLabel2);
		//jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Select Date2 Here:");
		
		todayDate = new Date();
		dateChooser1.setDateFormatString(mydateformat);
		dateChooser1.setDate(todayDate);
		dateChooser1.setFont(new java.awt.Font("Ebrima", 0, 20));
		dateChooser1.setPreferredSize(new java.awt.Dimension(150, 20));
		dateChooser1
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ShipmentdateJDatePropertyChange(evt);
					}
				});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																dateChooser1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																353,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(jLabel2)
														.addComponent(
																dateChooser2,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																353,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(jLabel1))
										.addContainerGap(25, Short.MAX_VALUE)));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(jLabel1)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												dateChooser1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												32,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addComponent(jLabel2)
										.addComponent(
												dateChooser2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												32,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(51, Short.MAX_VALUE)));

		cancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.setText("Cancel");
		cancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelJBtnActionPerformed(evt);
			}
		});

		addDispensingPointJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		addDispensingPointJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Add.png"))); // NOI18N
		addDispensingPointJBtn.setText("Run The Report");
		addDispensingPointJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						addDispensingPointJBtnActionPerformed(evt);
					}
				});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap(108,
						Short.MAX_VALUE).addComponent(addDispensingPointJBtn)
						.addGap(18, 18, 18).addComponent(cancelJBtn).addGap(28,
								28, 28)).addComponent(jPanel1,
				javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								layout
										.createSequentialGroup()
										.addComponent(
												jPanel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED,
												25, Short.MAX_VALUE)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																cancelJBtn)
														.addComponent(
																addDispensingPointJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																31,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void ShipmentdateJDatePropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		try {

			String mydate = this.dateChooser1.getDate().toString();
			//System.out.println(mydate);
			
			String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			String oldDateString = mydate;
			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			newDateString=newDateString.substring(0,10);
			//java.util.Date date = ShipmentdateJDate.getDate();
			
			//String dateStr = this.ShipmentdateJDate.getDate().toString();  
			//SimpleDateFormat formatter = new SimpleDateFormat("yyyy-mm-dd");  
			System.out.println(newDateString);
			//d=(Date)newDateString;
			//System.out.println(newDateString);
			

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void cancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
       System.out.println("TEST");
		this.dispose();
	}

	private void addDispensingPointJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		int siteId = AppJFrame.getDispensingId("HIV");
		if (dateChooser1.getDate().compareTo(todayDate) <= 0){
			CalendarUtil cal = new CalendarUtil();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.sql.Date datum1 = cal.getSqlDate(sdf.format(dateChooser1.getDate()));
			java.sql.Date datum2 = cal.getSqlDate(sdf.format(dateChooser2.getDate()));
			new Thread(new ProgressBarRangeJD("Generating HIV Daily Activity Register Report",datum1,datum2, siteId)).start();;
			//new ReportGenerator().generateHivDailyActivityRegister(datum, siteId);
			/*new MessageDialog().showDialog(this,
					"Achived DARs Functionality not available in this version", "Functionality ", "Close");*/
		}
		else{
			new MessageDialog().showDialog(this,
					"Achived DARs not available for dates in the future", "Achive Date Error", "Close");
			dateChooser1.setDate(todayDate);
		}
		
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SelectMyDateJD dialog = new SelectMyDateJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private com.toedter.calendar.JDateChooser dateChooser1;
	private com.toedter.calendar.JDateChooser dateChooser2;
	private javax.swing.JButton addDispensingPointJBtn;
	private javax.swing.JButton cancelJBtn;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JPanel jPanel1;
	// End of variables declaration//GEN-END:variables

}