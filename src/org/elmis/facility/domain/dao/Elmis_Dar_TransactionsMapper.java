package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Dar_TransactionsExample;

public interface Elmis_Dar_TransactionsMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int countByExample(Elmis_Dar_TransactionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int deleteByExample(Elmis_Dar_TransactionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int insert(Elmis_Dar_Transactions record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int insertSelective(Elmis_Dar_Transactions record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    List<Elmis_Dar_Transactions> selectByExample(Elmis_Dar_TransactionsExample example);
    List<Elmis_Dar_Transactions>selectfinalARVdar(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate, @Param("dpid") Integer dpid);
    List<Elmis_Dar_Transactions>selectARVdar(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectsumARVdar(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectsumsingledoseARVdar(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectsumliquidpowderdoseARVdar(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectsumcotrimoxazoledoseARVdar(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectfinalsumARVproducts(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate,@Param("dpid") Integer dpid);
    //List<Double>selectfinalsumARVproducts(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectARVdarsingledose(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectARVdarliquidpowderdose(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectARVdarcotrimoxazoledose(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectAllARVdosedarproducts(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
    List<Elmis_Dar_Transactions>selectARVpatientdartransactions(@Param("artnumber") java.lang.String  artnumber);
    
   
   List<Elmis_Dar_Transactions>checkifOtherlasttransactionoftheMonth(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate,@Param("prodcode") String prodcode,@Param("dpid") Integer dpid);
    
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    Elmis_Dar_Transactions selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int updateByExampleSelective(@Param("record") Elmis_Dar_Transactions record, @Param("example") Elmis_Dar_TransactionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int updateByExample(@Param("record") Elmis_Dar_Transactions record, @Param("example") Elmis_Dar_TransactionsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int updateByPrimaryKeySelective(Elmis_Dar_Transactions record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table elmis_dar_transactions
     *
     * @mbggenerated Tue Aug 27 14:29:31 CAT 2013
     */
    int updateByPrimaryKey(Elmis_Dar_Transactions record);
}