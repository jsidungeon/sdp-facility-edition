package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.JOptionPane;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.AmcCalculator;
import org.elmis.facility.reports.utils.CalendarUtil;

public class ArvRequisitionDao {

	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getArvDispensaryIssues(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String remarks,String normalOrder)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		
		if (remarks.trim().length() != 0)
			map.put("remark", remarks);
		
		if (normalOrder.equalsIgnoreCase("YES"))
		{
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonthEmergency());
			map.put("prevFrmDate", new CalendarUtil().getFirstDayOfPreviousMonth());
			//System.out.println("Emergency");
		}
		else
		{
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonth());
			map.put("prevFrmDate", new CalendarUtil().getFirstDayOfPreviousMonth());
			//System.out.println("Non Emergency");
		}
		try
		{
			deleteArvRandRData(toDate, session);
			List<ReportRequisition> issuesList = new ArrayList<>();
			List<ReportRequisition> dispensedList = session.selectList("ArvRequisition.dispensed", map);
			for (ReportRequisition r: dispensedList)
			{
				r.setEmergencyOrder(normalOrder);
				String productCode = r.getProductCode();
				map.put("productCode", productCode);
				
				int amc = new AmcCalculator().amcCalculator(r.getProductCode(), r.getQtyDispensed());
				/*if (amc == 0)
				{
					amc = r.getQtyDispensed();
					r.setAmc(amc);
					int maxQty = amc * 3;
					r.setMaxQty(maxQty);
				}
				else
				{*/
					r.setAmc(amc);
					int maxQty = amc * 3;
					//System.out.println("Max Qty "+maxQty);
					r.setMaxQty( maxQty);
				//}
				
				Integer qtyReceived = session.selectOne("ArvRequisition.qtyReceived", map);
				System.out.println("AA");
				//System.out.println("qty received:"+qtyReceived);
				Integer phyCount = session.selectOne("ArvRequisition.phyCount", map);
				System.out.println("BBB");
				//System.out.println("Phy Count:"+phyCount);
				Integer beginBal = session.selectOne("ArvRequisition.beginBalance", map);

				//System.out.println("Begin bal:"+beginBal);
				Integer storeAdjustments = session.selectOne("ArvRequisition.store_adjustments", map);
				System.out.println("DDD");
				//System.out.println("Store Adjustments "+storeAdjustments);
				//Integer dispensaryAdjustments = session.selectOne("Adjustment.dispensary_adjustments", map);
				if (r.getReportPeriodFrm() == null)
					r.setReportPeriodFrm(fromDate);
				if (r.getReportPeriodTo() == null)
					r.setReportPeriodTo(toDate);
				if (qtyReceived == null)
					r.setTotalReceived(0);	
				else
					r.setTotalReceived(qtyReceived);
				
				if (phyCount == null)
				{
					phyCount = 0;
					r.setPhyCount(phyCount);
					r.setOrderQty(r.getMaxQty() - phyCount);
				}
				else
				{
					r.setPhyCount(phyCount);
					r.setOrderQty(r.getMaxQty() - phyCount);
				}
					
				if (beginBal == null)
					r.setBeginningBal(0);	
				else
					r.setBeginningBal(beginBal);
				/*if (dispensaryAdjustments == null)
					dispensaryAdjustments = 0;*/
					
				if (storeAdjustments == null)
					r.setLossAdjustment(0);	
				else
					r.setLossAdjustment(storeAdjustments /*+ dispensaryAdjustments*/);
				
				if (r.getOrderQty() < 0)
					r.setOrderQty(0);
				r.setCurrentPrice(Math.round(r.getCurrentPrice() * r.getOrderQty()*100.0)/100.0);
				if (remarks.trim().length() != 0)
					r.setRemarks(remarks);
				System.out.println("FFF");
				issuesList.add(r);
				System.out.println("GGGG");
				session.insert("ArvRequisition.insertArvrandr",  r);
				System.out.println("JJJ");
			}
			
			/*if (remarks.trim().length() != 0)
				session.insert("ArvRequisition.insertRemark", map);*/

			session.commit();
			return issuesList;
		}
		finally
		{
			session.close();
		}
	}
	private void deleteArvRandRData( Date toDate, SqlSession session)
	{
		session.selectList("ArvRequisition.delarvdata", toDate);
		session.delete("ArvRequisition.deleteRemark", toDate);
		session.commit();
	}
}
