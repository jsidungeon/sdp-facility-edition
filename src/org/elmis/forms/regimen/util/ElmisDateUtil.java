package org.elmis.forms.regimen.util;

import java.util.Calendar;
import java.util.Date;

public class ElmisDateUtil {
	
	public static Date addDays(Date date, Integer days){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		Date newDate = cal.getTime();
		return newDate;
	}
}
