package org.elmis.facility.domain.model;

import java.sql.Date;

public class HivTestResult {

	private int testId;
	private char finalResult;
	private String positiveResult;
	private String negativeResult;
	private String indeterminateResult;
	private int siteId;
	
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public String getPositiveResult() {
		return positiveResult;
	}
	public void setPositiveResult(String positiveResult) {
		this.positiveResult = positiveResult;
	}
	public String getNegativeResult() {
		return negativeResult;
	}
	public void setNegativeResult(String negativeResult) {
		this.negativeResult = negativeResult;
	}
	public String getIndeterminateResult() {
		return indeterminateResult;
	}
	public void setIndeterminateResult(String indeterminateResult) {
		this.indeterminateResult = indeterminateResult;
	}
	private String screeningResult;
	private String confirmatoryResult;
	private String screeningProduct;
	private String confirmatoryProduct;
	private Date testDate;
	private String purpose;
	public int getTestId() {
		return testId;
	}
	public void setTestId(int testId) {
		this.testId = testId;
	}
	public char getFinalResult() {
		return finalResult;
	}
	public void setFinalResult(char finalResult) {
		if (finalResult == 'N')
			negativeResult = "N";
		else if (finalResult == 'P')
			positiveResult = "P";
		else
			indeterminateResult = "I";
		this.finalResult = finalResult;
	}
	
	public String getScreeningResult() {
		return screeningResult;
	}
	public void setScreeningResult(String screeningResult) {
		this.screeningResult = screeningResult;
	}
	public String getConfirmatoryResult() {
		return confirmatoryResult;
	}
	public void setConfirmatoryResult(String confirmatoryResult) {
		this.confirmatoryResult = confirmatoryResult;
	}
	public String getScreeningProduct() {
		return screeningProduct;
	}
	public void setScreeningProduct(String screeningProduct) {
		this.screeningProduct = screeningProduct;
	}
	public String getConfirmatoryProduct() {
		return confirmatoryProduct;
	}
	public void setConfirmatoryProduct(String confirmatoryProduct) {
		this.confirmatoryProduct = confirmatoryProduct;
	}
	public Date getTestDate() {
		return testDate;
	}
	public void setTestDate(Date testDate) {
		this.testDate = testDate;
	}
	public String getPurpose() {
		return purpose;
	}
	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}
	@Override
	public String toString() {
		return "HivTestResult [testId=" + testId + ", finalResult="
				+ finalResult + ", positiveResult=" + positiveResult
				+ ", negativeResult=" + negativeResult
				+ ", indeterminateResult=" + indeterminateResult + ", siteId="
				+ siteId + ", screeningResult=" + screeningResult
				+ ", confirmatoryResult=" + confirmatoryResult
				+ ", screeningProduct=" + screeningProduct
				+ ", confirmatoryProduct=" + confirmatoryProduct
				+ ", testDate=" + testDate + ", purpose=" + purpose + "]";
	}
}
