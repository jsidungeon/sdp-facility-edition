package org.elmis.facility.ws.client;

import org.elmis.forms.regimen.util.GenericUtil;

public class ElmisConstants {
	public static final String LOCAL_ELMIS_SERVER_URL = GenericUtil.getProperties().getProperty("eLMISLocalServerBaseURL");;
}
