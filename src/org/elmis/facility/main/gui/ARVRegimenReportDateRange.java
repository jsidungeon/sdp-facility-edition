package org.elmis.facility.main.gui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

import org.elmis.facility.reports.regimen.RegimenReportData;

import com.toedter.calendar.JDateChooser;

public class ARVRegimenReportDateRange extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private final ARVRegimenReportDateRange _this = this;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ARVRegimenReportDateRange dialog = new ARVRegimenReportDateRange(null, false);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */

	public ARVRegimenReportDateRange(Frame parent, boolean modal) {
		super(parent, true);
		setResizable(false);
		setTitle("Regimen Report Date Range");
		setBounds(100, 100, 526, 295);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblStartDate = new JLabel("Start Date:");
		lblStartDate.setBounds(55, 58, 116, 14);
		contentPanel.add(lblStartDate);
		
		JLabel lblEndDate = new JLabel("End Date:");
		lblEndDate.setBounds(55, 121, 74, 14);
		contentPanel.add(lblEndDate);
		
		final JDateChooser dateChooserStart = new JDateChooser();
		dateChooserStart.setDateFormatString("dd-MM-yyyy");
		dateChooserStart.setBounds(161, 58, 230, 28);
		contentPanel.add(dateChooserStart);
		
		final JDateChooser dateChooserEnd = new JDateChooser();
		dateChooserEnd.setDateFormatString("dd-MM-yyyy");
		dateChooserEnd.setBounds(161, 107, 230, 28);
		contentPanel.add(dateChooserEnd);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_this.dispose();
			}
		});
		btnCancel.setBounds(347, 184, 89, 23);
		contentPanel.add(btnCancel);
		
		JButton btnViewReport = new JButton("View Report");
		btnViewReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				showReport( dateChooserStart.getDate() , dateChooserEnd.getDate());
				_this.dispose();
			}
		});
		btnViewReport.setBounds(206, 184, 131, 23);
		contentPanel.add(btnViewReport);
	}

	protected void showReport(Date to, Date from) {
		// TODO Auto-generated method stub
		  
        String mainReport = "./Reports/RegimenReport.jasper";
        String subReport = "./Reports/RegimenReport_subreport1.jasper";
//        String outFileName = "/home/mesay/Documents/Projects/JasperReports/test.pdf";  
        HashMap<String, Object> parameters = new HashMap<String, Object>(); 
        RegimenReportData reportData = new RegimenReportData();
        reportData.populateRegimenReport(to , from);
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(
				reportData.getRegimenLines());
//		parameters.put("SUB_DATA_SOURCE", reportData.getRegimenLines().get(0).getRegimenReportBeans());
		parameters.put("SUB_REPORT_PATH",subReport);
	//	parameters.put("SUBREPORT_DIR", "/home/mesay/Documents/Projects/JasperReports/ReportJars/");
        try  
        {  
            JasperPrint print = JasperFillManager.fillReport(mainReport,parameters,ds );  
//            JRExporter exporter =new net.sf.jasperreports.engine.export.JRPdfExporter();  
//            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,outFileName);  
	//            exporter.setParameter(JRExporterParameter.JASPER_PRINT,print);  
	//            exporter.exportReport();  
//	            System.out.println("Created file: " + outFileName);
            
          /*  JFrame frame = new JFrame("Regimen Report");
            frame.getContentPane().add(new JRViewer(print));
            frame.pack();
            frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);*/
//            frame.setVisible(true);
            RegimenReportDialogue dialog = new RegimenReportDialogue(new JRViewer(print), javax.swing.JOptionPane.getFrameForComponent(_this), true);
            dialog.setVisible(true);
            }  
        catch (JRException e)  
        {  
            e.printStackTrace();  
            System.exit(1);  
        }  
        catch (Exception e)  
        {  
        e.printStackTrace();  
        } 
    }
		
}
