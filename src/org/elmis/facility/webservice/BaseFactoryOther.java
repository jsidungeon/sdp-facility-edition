package org.elmis.facility.webservice;

import java.util.ArrayList;
import java.util.List;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.google.gson.Gson;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/1/13
 * Time: 5:11 PM
 */
public class BaseFactoryOther {

    // TODO: remove the hardcoded default values.
    public static String BaseURL = "http://localhost:9091";

    public static String Authorization  = "Admin123";

    public static String UserName = "wbomett";

    public static Long FacilityID = 1L;

    private static void parseHostAndPort(){
      // this assumes that the base URL contains the protocol and all
      HttpClient.PROTOCOL = BaseURL.substring(0, BaseURL.indexOf(':') );

      // set the port
      if(BaseURL.lastIndexOf(':') != BaseURL.indexOf(':')){
         String port = BaseURL.substring(BaseURL.lastIndexOf(':'));
         HttpClient.PORT = Integer.parseInt(port);
      }else{
        // default to http port 80
        HttpClient.PORT = 80;
      }

      String host = BaseURL.substring(BaseURL.lastIndexOf("//") + 2);
      if(host.indexOf(':') > 0){
        host = host.substring(0, host.indexOf(':') - 1);
      }

      HttpClient.HOST = host;

    }

    public static Object loadJSONLookup(String service, Class<?> type) throws Exception{
        parseHostAndPort();

        HttpClient client = new HttpClient();
        client.createContext();

        ResponseEntity response = client.SendJSON("{}", BaseURL + "/rest-api/lookup/" + service, HttpClient.POST, UserName , Authorization );
       
        try{
            JSONObject array = new JSONObject(response.getResponse());
            JSONArray innerArray = array.getJSONArray(service);
            Gson gson = new Gson();

            List list = new ArrayList<>() ;
            for(int i = 0; i < innerArray.length(); i++){
                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(),type)) ;
            }
            return list;
        }

        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            throw new Exception ("Authentication Error");
        }

    }


    public static Object uploadJSON(String service, String result , String json , Class<?> type) throws Exception{

        HttpClient client = new HttpClient();
        client.createContext();

        ResponseEntity response = client.SendJSON(json, BaseURL + "/rest-api/" + service, HttpClient.POST, UserName , Authorization );
        //SendJSON(String json, String url, String commMethod, String username, String password)
        if (response != null)
        	System.out.println(response.getResponse());
        JSONObject array = new JSONObject(response.getResponse());

        String errorObject = null;
        try{
            errorObject = array.getString("error");
        }catch(Exception ex){

        }
        if(errorObject != null){
            throw new Exception(errorObject);
        }
        try{
            JSONObject resultJson = array.getJSONObject(result);
            Gson gson = new Gson();
            return gson.fromJson(resultJson.toString(),type);
        }
        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            // JSON was not being parsed,
            // this indicates that the authentication has failed.
            throw new Exception("Authentication Error");
        }

    }


}
