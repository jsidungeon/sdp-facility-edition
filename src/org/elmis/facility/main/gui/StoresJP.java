/*
 * ReportsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperPrint;

import org.elmis.forms.admin.dispensingpoints.SelectDispensingPointJD;
import org.elmis.forms.stores.receiving.ProductsPhysicalCountJD;
import org.elmis.forms.stores.receiving.ReceivingJD;
import org.elmis.forms.stores.receiving.productAdjustmentsJD;
import org.elmis.forms.stores.scc.SelectProductsJD;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * 
 * @author __USER__
 */
public class StoresJP extends javax.swing.JPanel {

	private static Map parameterMap = new HashMap();
	private JasperPrint print;

	/** Creates new form ReportsJP */
	public StoresJP() {
		initComponents();
		//weeklyRptJL.setVisible(false);
		//staffproductivityJL.setVisible(false);

		// productivityJBtn.setVisible(false);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		issueItemJL = new javax.swing.JLabel();
		receiveItemJL = new javax.swing.JLabel();
		viewSCCJL = new javax.swing.JLabel();
		ReceiveitemJL = new javax.swing.JLabel();
		RecordPhysicalCountJL = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		issueItemJL.setBackground(new java.awt.Color(102, 102, 102));
		issueItemJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		issueItemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS issued items icon.png"))); // NOI18N
		issueItemJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Issue Items",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		issueItemJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				issueItemJLMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				issueItemJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				issueItemJLMouseExited(evt);
			}
		});

		receiveItemJL.setBackground(new java.awt.Color(102, 102, 102));
		receiveItemJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		receiveItemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS items recieved icon.png"))); // NOI18N
		receiveItemJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Receive Items",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		receiveItemJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				receiveItemJLMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				receiveItemJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				receiveItemJLMouseExited(evt);
			}
		});

		viewSCCJL.setBackground(new java.awt.Color(102, 102, 102));
		viewSCCJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewSCCJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS stock card control icon.png"))); // NOI18N
		viewSCCJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Stock Control Card",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewSCCJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewSCCJLMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				viewSCCJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				viewSCCJLMouseExited(evt);
			}
		});

		ReceiveitemJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		ReceiveitemJL.setIcon(new ImageIcon(StoresJP.class.getResource("/elmis_images/eLMIS losses&adjust.png"))); // NOI18N
		ReceiveitemJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Adjustments",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		ReceiveitemJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ReceiveitemJLMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				ReceiveitemJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				ReceiveitemJLMouseExited(evt);
			}
			
		});

		RecordPhysicalCountJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		RecordPhysicalCountJL.setIcon(new ImageIcon(StoresJP.class.getResource("/elmis_images/eLMIS Physical count.png"))); // NOI18N
		RecordPhysicalCountJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Physical Count",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		RecordPhysicalCountJL
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						RecordPhysicalCountJLMouseClicked(evt);
					}
					public void mouseEntered(java.awt.event.MouseEvent evt) {
						RecordPhysicalCountJLMouseEntered(evt);
					}

					public void mouseExited(java.awt.event.MouseEvent evt) {
						RecordPhysicalCountJLMouseExited(evt);
					}
				});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(issueItemJL, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(receiveItemJL, GroupLayout.PREFERRED_SIZE, 159, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(viewSCCJL, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(ReceiveitemJL, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(RecordPhysicalCountJL, GroupLayout.PREFERRED_SIZE, 163, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(211, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(RecordPhysicalCountJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(ReceiveitemJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(viewSCCJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(receiveItemJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(issueItemJL, Alignment.LEADING, GroupLayout.PREFERRED_SIZE,150,150))
					.addContainerGap(179, Short.MAX_VALUE))
		);
		this.setLayout(layout);
	}// </editor-fold>
	//GEN-END:initComponents

	private void RecordPhysicalCountJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling
		AppJFrame.glassPane.activate(null);
		new ProductsPhysicalCountJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}

	private void ReceiveitemJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		AppJFrame.glassPane.activate(null);
		new productAdjustmentsJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}
	private void receiveItemJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		receiveItemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_mouseover_images/eLMIS items issued icon big.png")));
	}

	private void receiveItemJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		receiveItemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_images/eLMIS items recieved icon.png")));
	}
	private void issueItemJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		issueItemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_mouseover_images/eLMIS issued items icon big.png")));
	}

	private void issueItemJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		issueItemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_images/eLMIS issued items icon.png")));
	}
	private void viewSCCJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		viewSCCJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_mouseover_images/eLMIS stock card control big.png")));
	}

	private void viewSCCJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		viewSCCJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_images/eLMIS stock card control icon.png")));
	}
	private void ReceiveitemJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		ReceiveitemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_images/eLMIS adjustments icon big.png")));
	}

	private void ReceiveitemJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		ReceiveitemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_images/eLMIS losses&adjust.png")));
	}
	private void RecordPhysicalCountJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		RecordPhysicalCountJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_mouseover_images/eLMIS physical count big.png")));
	}

	private void RecordPhysicalCountJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		RecordPhysicalCountJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
		"/elmis_images/eLMIS Physical count.png")));
	}
	
	
	
	
	
	
	
	
	
	
	
	private void viewSCCJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		AppJFrame.glassPane.activate(null);
		new SelectProductsJD(
				javax.swing.JOptionPane.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}

	private void receiveItemJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code

		// TODO add your handling code here:
		AppJFrame.glassPane.activate(null);

		new ReceivingJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);

		//new ReceivingJD(javax.swing.JOptionPane.getFrameForComponent(this),
		//	true);

		AppJFrame.glassPane.deactivate();
	}

	private void issueItemJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		AppJFrame.glassPane.activate(null);
		//new IssuingJD(javax.swing.JOptionPane.getFrameForComponent(this), true);

		new SelectDispensingPointJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel ReceiveitemJL;
	private javax.swing.JLabel RecordPhysicalCountJL;
	private javax.swing.JLabel issueItemJL;
	private javax.swing.JLabel receiveItemJL;
	private javax.swing.JLabel viewSCCJL;
	// End of variables declaration//GEN-END:variables

}