package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Program_products;
import org.elmis.facility.network.MyBatisConnectionFactory;


/**
 * Program_Supported DAO
 * 
 * @author Moses Kausa
 * http://jsi-zambia.com (English)
 
 */

public class Program_ProductsDAO {
    
private SqlSessionFactory sqlSessionFactory; 
	
	public Program_ProductsDAO(){
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
			
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Program_products> selectAll(String prodcode){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Program_products> list = session.selectList("Program_Products.getAllPrograms_Product",prodcode);
			return list;
		} finally {
			session.close();
		}
	}
	
	
	/**
	 * Returns  a Program_Products  instances from the database.
	 * @return  a Program_Products  instances from the database with the matched Program name.
	 */
		
	@SuppressWarnings("unchecked")
	
		public List<Program_products> getByProgram_Product(int pId) {	
	
		SqlSession session = sqlSessionFactory.openSession();
		
		try {
							
	 List<Program_products> rs =  session.selectList("Program_Products.getAllPrograms_Product",pId);
			return rs;
		} finally {
			session.close();
		}
		
	}


	/**
	 * Returns a Program_Products instance from the database.
	 * @param id primary key value used for lookup.
	 * @return A Program_Products instance with a primary key value equals to pk. null if there is no matching row.
	 */
	public Program_products selectById(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Program_products progs_prod = (Program_products) session.selectOne("Program_Products.getById",id);
			return progs_prod;
		} finally {
			session.close();
		}
	}

	/**
	 * Updates an instance of Program_Products in the database.
	 * @param Program_Products the instance to be updated.
	 */
	public void update(Program_products prog_product){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.update("Program_Products.updateByPrimaryKey", prog_product );
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Insert an instance of Program_Products into the database.
	 * @param Program_Products the instance to be persisted.
	 */
	public void insert(Program_products prog_product){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.insert("Program_Products.insert", prog_product);
			session.commit();
		} finally {
			session.close();
		}
	}

	/**
	 * Delete an instance of Program_Products from the database.
	 * @param id primary key value of the instance to be deleted.
	 */
	public void delete(int id){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			session.delete("Program_Products.deleteById", id);
			session.commit();
		} finally {
			session.close();
		}
	}
}
