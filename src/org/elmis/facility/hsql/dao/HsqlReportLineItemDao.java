package org.elmis.facility.hsql.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.database.connection.MyBatisHSQLConnectionFactory;
import org.elmis.facility.reporting.model.ReportLineItem;
import org.elmis.facility.reporting.model.ReportRequisition;

public class HsqlReportLineItemDao {

	private SqlSessionFactory sqlSessionFactory;
	
	{
		sqlSessionFactory = MyBatisHSQLConnectionFactory.getSqlSessionFactory();
	}
	
	public List<ReportRequisition> getRequisitionLineItems()
	{
		SqlSession session = sqlSessionFactory.openSession();
		
		try
		{
			return session.selectList("HsqlRequisition.requisitions");
		}
		finally
		{
			session.close();
		}
	}
	public void saveRequisitionLineItems(List<ReportLineItem> reportLineItem)
	{
		SqlSession session = sqlSessionFactory.openSession();
		
		try
		{
			for (ReportLineItem r : reportLineItem)
			{
				session.insert("HsqlRequisition.syncrequisitions", r);
				session.commit();
			}
			
		}
		finally
		{
			session.close();
		}
	}
}
