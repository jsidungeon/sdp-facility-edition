/*
 * ArvDispensePanelJd.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.reports.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author  __USER__
 */
public class ArvDispensePanelJd extends javax.swing.JDialog implements
		ActionListener {

	/** Creates new form ArvDispensePanelJd */
	public ArvDispensePanelJd(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		jTable1.setAutoResizeMode(jTable1.AUTO_RESIZE_OFF);
		jTable1.getColumnModel().getColumn(0).setPreferredWidth(450);
		jTable1.getColumnModel().getColumn(1).setPreferredWidth(50);
		inputTextField.setForeground(Color.RED);
		inputTextField.setFont(new Font("Verdana", Font.BOLD, 30));
		oneButton.addActionListener(this);
		twoButton.addActionListener(this);
		threeButton.addActionListener(this);
		fourButton.addActionListener(this);
		fiveButton.addActionListener(this);
		sixButton.addActionListener(this);
		sevenButton.addActionListener(this);
		eightButton.addActionListener(this);
		nineButton.addActionListener(this);
		zeroButton.addActionListener(this);
		enterButton.addActionListener(this);
		backSpaceButton.addActionListener(this);
		clearButton.addActionListener(this);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		inputTextField = new javax.swing.JTextField();
		jScrollPane1 = new javax.swing.JScrollPane();
		jScrollPane2 = new javax.swing.JScrollPane();
		jTable1 = new javax.swing.JTable();
		oneButton = new javax.swing.JButton();
		twoButton = new javax.swing.JButton();
		fourButton = new javax.swing.JButton();
		fiveButton = new javax.swing.JButton();
		eightButton = new javax.swing.JButton();
		sevenButton = new javax.swing.JButton();
		zeroButton = new javax.swing.JButton();
		backSpaceButton = new javax.swing.JButton();
		nineButton = new javax.swing.JButton();
		sixButton = new javax.swing.JButton();
		threeButton = new javax.swing.JButton();
		enterButton = new javax.swing.JButton();
		clearButton = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		jTable1.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] { { null, null }, { null, null },
						{ null, null }, { null, null } }, new String[] {
						"Prescription", "Quantity" }));
		jTable1.setRowHeight(35);
		jScrollPane2.setViewportView(jTable1);

		jScrollPane1.setViewportView(jScrollPane2);

		oneButton.setText("1");

		twoButton.setText("2");

		fourButton.setText("4");

		fiveButton.setText("5");

		eightButton.setText("8");

		sevenButton.setText("7");

		zeroButton.setText("0");

		backSpaceButton.setText("BACKSPACE");

		nineButton.setText("9");

		sixButton.setText("6");

		threeButton.setText("3");

		enterButton.setText("ENTER");

		clearButton.setText("CLEAR");

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												false)
												.addComponent(inputTextField)
												.addComponent(
														jScrollPane1,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														518, Short.MAX_VALUE))
								.addGap(169, 169, 169)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING,
												false)
												.addComponent(
														zeroButton,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(
														sevenButton,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(
														oneButton,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														77, Short.MAX_VALUE)
												.addComponent(
														fourButton,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														javax.swing.GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE))
								.addPreferredGap(
										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addComponent(
																						fiveButton,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						77,
																						Short.MAX_VALUE)
																				.addComponent(
																						eightButton,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						77,
																						Short.MAX_VALUE)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addPreferredGap(
																										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																								.addComponent(
																										twoButton,
																										javax.swing.GroupLayout.DEFAULT_SIZE,
																										77,
																										Short.MAX_VALUE)))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING,
																				false)
																				.addComponent(
																						nineButton,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addGroup(
																						layout.createSequentialGroup()
																								.addGroup(
																										layout.createParallelGroup(
																												javax.swing.GroupLayout.Alignment.TRAILING,
																												false)
																												.addComponent(
																														sixButton,
																														javax.swing.GroupLayout.Alignment.LEADING,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														Short.MAX_VALUE)
																												.addComponent(
																														threeButton,
																														javax.swing.GroupLayout.Alignment.LEADING,
																														javax.swing.GroupLayout.DEFAULT_SIZE,
																														72,
																														Short.MAX_VALUE))
																								.addPreferredGap(
																										javax.swing.LayoutStyle.ComponentPlacement.RELATED))))
												.addComponent(
														backSpaceButton,
														javax.swing.GroupLayout.PREFERRED_SIZE,
														156,
														javax.swing.GroupLayout.PREFERRED_SIZE))
								.addGap(18, 18, 18)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addComponent(enterButton)
												.addComponent(clearButton))
								.addGap(22, 22, 22)));
		layout.setVerticalGroup(layout
				.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(38, 38, 38)
								.addGroup(
										layout.createParallelGroup(
												javax.swing.GroupLayout.Alignment.LEADING)
												.addGroup(
														layout.createSequentialGroup()
																.addGap(1, 1, 1)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						twoButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						oneButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.TRAILING)
																				.addComponent(
																						fiveButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						fourButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						sevenButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						eightButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																				.addComponent(
																						zeroButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						backSpaceButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE)))
												.addGroup(
														layout.createSequentialGroup()
																.addComponent(
																		inputTextField,
																		javax.swing.GroupLayout.PREFERRED_SIZE,
																		91,
																		javax.swing.GroupLayout.PREFERRED_SIZE)
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																.addComponent(
																		jScrollPane1,
																		javax.swing.GroupLayout.DEFAULT_SIZE,
																		381,
																		Short.MAX_VALUE))
												.addGroup(
														layout.createSequentialGroup()
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.TRAILING,
																				false)
																				.addComponent(
																						enterButton,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						javax.swing.GroupLayout.DEFAULT_SIZE,
																						Short.MAX_VALUE)
																				.addGroup(
																						javax.swing.GroupLayout.Alignment.LEADING,
																						layout.createSequentialGroup()
																								.addComponent(
																										threeButton,
																										javax.swing.GroupLayout.PREFERRED_SIZE,
																										60,
																										javax.swing.GroupLayout.PREFERRED_SIZE)
																								.addPreferredGap(
																										javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																								.addComponent(
																										sixButton,
																										javax.swing.GroupLayout.PREFERRED_SIZE,
																										60,
																										javax.swing.GroupLayout.PREFERRED_SIZE)))
																.addPreferredGap(
																		javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																.addGroup(
																		layout.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																				.addComponent(
																						nineButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						60,
																						javax.swing.GroupLayout.PREFERRED_SIZE)
																				.addComponent(
																						clearButton,
																						javax.swing.GroupLayout.PREFERRED_SIZE,
																						127,
																						javax.swing.GroupLayout.PREFERRED_SIZE))
																.addGap(218,
																		218,
																		218)))
								.addContainerGap()));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				ArvDispensePanelJd dialog = new ArvDispensePanelJd(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton backSpaceButton;
	private javax.swing.JButton clearButton;
	private javax.swing.JButton eightButton;
	private javax.swing.JButton enterButton;
	private javax.swing.JButton fiveButton;
	private javax.swing.JButton fourButton;
	private javax.swing.JTextField inputTextField;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JTable jTable1;
	private javax.swing.JButton nineButton;
	private javax.swing.JButton oneButton;
	private javax.swing.JButton sevenButton;
	private javax.swing.JButton sixButton;
	private javax.swing.JButton threeButton;
	private javax.swing.JButton twoButton;
	private javax.swing.JButton zeroButton;
	// End of variables declaration//GEN-END:variables
	private StringBuilder inputString = new StringBuilder();

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == oneButton)
			inputTextField.setText(inputString.append(1).toString());
		else if (e.getSource() == twoButton)
			inputTextField.setText(inputString.append(2).toString());
		else if (e.getSource() == threeButton)
			inputTextField.setText(inputString.append(3).toString());
		else if (e.getSource() == fourButton)
			inputTextField.setText(inputString.append(4).toString());
		else if (e.getSource() == fiveButton)
			inputTextField.setText(inputString.append(5).toString());
		else if (e.getSource() == sixButton)
			inputTextField.setText(inputString.append(6).toString());
		else if (e.getSource() == sevenButton)
			inputTextField.setText(inputString.append(7).toString());
		else if (e.getSource() == eightButton)
			inputTextField.setText(inputString.append(8).toString());
		else if (e.getSource() == nineButton)
			inputTextField.setText(inputString.append(9).toString());
		else if (e.getSource() == zeroButton)
			inputTextField.setText(inputString.append(0).toString());
		else if (e.getSource() == backSpaceButton) {
			inputTextField.setText(inputString.deleteCharAt(
					inputString.length() - 1).toString());
		} else if (e.getSource() == enterButton) {
			//inputTextField.setText(inputString.append(2).toString());
		} else if (e.getSource() == clearButton) {
			inputString.setLength(0);
			inputTextField.setText(inputString.toString());
		} else if (e.getSource() == fourButton)
			inputTextField.setText(inputString.append(4).toString());

	}
}