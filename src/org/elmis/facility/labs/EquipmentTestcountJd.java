package org.elmis.facility.labs;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.elmis.facility.domain.dao.LabTestDao;
import org.elmis.facility.reporting.model.LabTest;

public class EquipmentTestcountJd extends JDialog implements ActionListener{

	private final JPanel contentPanel = new JPanel();
	private String equipmentCode;
	private JTextField testCountTxtField;
	private LabTest testData;
	private JLabel errorLbl;

	/**
	 * Create the dialog.
	 */
	public EquipmentTestcountJd(JDialog parent, boolean modal, LabTest testData) {
		super(parent, modal);
		setTitle("Test count for "+testData.getMachineCode());
		this.equipmentCode = testData.getMachineCode();
		this.testData = testData;
		setResizable(false);
		setIconImage(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/appicon.png")).getImage());
		setBounds(100, 100, 450, 150);
		getContentPane().setBackground(new java.awt.Color(102, 102, 102));
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 434, 75);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setBackground(new java.awt.Color(102, 102, 102));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		{
			JLabel testCountLbl = new JLabel("Test count:");
			testCountLbl.setFont(new Font("Tahoma", Font.BOLD, 13));
			testCountLbl.setForeground(new java.awt.Color(255, 255, 255));
			testCountLbl.setBounds(10, 21, 85, 31);
			contentPanel.add(testCountLbl);
		}
		
		testCountTxtField = new JTextField();
		testCountTxtField.addActionListener(this);
		testCountTxtField.setFont(new Font("Tahoma", Font.BOLD, 13));
		testCountTxtField.setBounds(114, 27, 86, 37);
		contentPanel.add(testCountTxtField);
		testCountTxtField.setColumns(10);
		{
			errorLbl = new JLabel("Numerical input only");
			errorLbl.setForeground(Color.RED);
			errorLbl.setFont(new Font("Tahoma", Font.BOLD, 13));
			errorLbl.setBounds(226, 38, 159, 14);
			errorLbl.setVisible(false);
			contentPanel.add(errorLbl);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new java.awt.Color(102, 102, 102));
			buttonPane.setBounds(0, 75, 434, 37);
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setFont(new java.awt.Font("Ebrima", 1, 12));
				cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Cancel.png")));
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
				getRootPane().setDefaultButton(cancelButton);
			}
			{
				JButton saveButton = new JButton("Save");
				saveButton.addActionListener(this);
				saveButton.setFont(new java.awt.Font("Ebrima", 1, 12));
				saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Save icon.png")));
				saveButton.setActionCommand("Save");
				buttonPane.add(saveButton);
			}
		}
		setLocationRelativeTo(parent);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		try{
			Integer testCount = Integer.parseInt(testCountTxtField.getText());
			testData.setMachineTestCount(testCount);

			int ackSubmission = JOptionPane.showConfirmDialog(this, "Do you want to save Test Count "+testCount+" for equipment "+equipmentCode+"?", "Save Test Count for "+equipmentCode, JOptionPane.YES_NO_OPTION);
			if (ackSubmission == JOptionPane.YES_OPTION)
			{
				boolean submissionStatus = new LabTestDao().checkSubmission(equipmentCode, testData.getEndDate());
				if (submissionStatus)
				{
					int ack = JOptionPane.showConfirmDialog(this, "Reporting on "+equipmentCode+" was already captured. Do you want to over-write it?", "Over write "+equipmentCode+" Data", JOptionPane.YES_NO_OPTION);
					if (ack == JOptionPane.YES_OPTION)
					{
						LabTestDao labTestDao = new LabTestDao();
						labTestDao.deleteSubmission(equipmentCode, testData.getEndDate());
						labTestDao.submitEquipmentMonthlyData(testData);
						dispose();
					}
				}
				else
				{
					new LabTestDao().submitEquipmentMonthlyData(testData);
					dispose();
				}
			}
			else
				dispose();
		}
		catch(NumberFormatException e){
			errorLbl.setVisible(true);
		}
		
	}
}
