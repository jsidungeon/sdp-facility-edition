package org.elmis.facility.domain.dao;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.ElmisRegimenCategory;
import org.elmis.facility.domain.model.ElmisRegimenProduct;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.reports.regimen.RegimenReportBean;

public interface RegimenMapper {
	List<Map<String , Object>> selectPatientRegimenProducts(@Param("art")String artNo,@Param("dpid")Integer dpid);
	
	List<ElmisRegimen> selectAllRegimens();
	
	List<RegimenReportBean> selectRegimenReportBeans(@Param("startDate")Date startDate,@Param("endDate")Date endDate);
	
	List<ElmisRegimen> selectRegimensByCategory(Integer categoyId);
	
	List<Map<String, Object>>  selectProductsForFacility(@Param("programCode") String programCode ,@Param("faciltyTypeCode") String facilityTypeCode);
	
	int insert(ElmisRegimen elmisRegimen);
	
	int insertRegimenProduct(ElmisRegimenProduct elmisRegimenProduct);
	
	List<Map<String, Object>>  selectRegimenProducts(@Param("regimenId") Integer regimenId);

	int updateRegimen(ElmisRegimen elmisRegimen);
	
	int deleteRegimenProduct(@Param("productId") Integer productId, @Param("regimenId") Integer regimenId);
	
	int deleteRegimenProductDosages(@Param("productId") Integer productId, @Param("regimenId") Integer regimenId);
	
	int insertRegimenCategory(ElmisRegimenCategory category);
	
	int updateRegimenCategory(ElmisRegimenCategory category);

	int deleteRegimenCategory(ElmisRegimenCategory category);
	
	Integer selectRegimenProductIdByRegimenAndProduct(@Param("productId") Integer productId , @Param("regimenId") Integer regimenId);

	int deleteRegimen(ElmisRegimen regimen);

	ElmisRegimen selectRegimenByART(@Param("art") String art);
	
	Facility_Types selectFacilityTypeByFacilityCode(@Param("code")String code);
}
