package org.elmis.facility.webservice;

import java.util.ArrayList;

import org.elmis.facility.domain.model.Dosage_Units;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Approved_Products;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Processing_Periods;
import org.elmis.facility.domain.model.Product_categories;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Program_products;
import org.elmis.facility.domain.model.Programs;



public class WebServiceClientImpl extends BaseFactoryOther implements WebServiceClient{

	
    public static void main(String args[]){
    	ArrayList<Products> products = null;
    	ArrayList<Programs> programs = null;
    	@SuppressWarnings("unused")
		ArrayList<Product_categories> product_categories = null;
    	@SuppressWarnings("unused")
		ArrayList<Losses_Adjustments_Types> losses_adjustments_types = null;
    	@SuppressWarnings("unused")
		ArrayList<Dosage_Units> dosage_units = null;
    	@SuppressWarnings("unused")
		ArrayList<Facility> facility = null;
    	@SuppressWarnings("unused")
		ArrayList<Program_products> program_products = null;
    	@SuppressWarnings("unused")
		ArrayList<Facility_Types> facility_types = null;
    	@SuppressWarnings("unused")
		ArrayList<Processing_Periods> processing_periods = null;
    	
    	
    	try {
    		products =  new WebServiceClientImpl().fetchProducts();
    		programs =  new WebServiceClientImpl().fetchPrograms();
    		for (Products product : products) {
    			
				System.out.println(product.getPrimaryname().toString());
				System.out.println(product.getCode().toString());
				System.out.println(product.getCategoryid().toString());
				System.out.println(product.getPacksize().toString());
			}
    		
            for (Programs program : programs) {
    			System.out.println(program.getCode() + "test");
				System.out.println(program.getName());
				System.out.println(program.getActive());
			}
    		
    		
    	} catch (Exception ex){
    		ex.getMessage();
    	} finally   	{
    		System.out.println("~||~");	
    	}
    }
    
  @SuppressWarnings("unchecked")
public  ArrayList<Dosage_Units> fetchDosageUnits() throws Exception{
        return (ArrayList<Dosage_Units>) loadJSONLookup("dosage-units", Dosage_Units.class);
    }
    
    @SuppressWarnings("unchecked")
	public ArrayList<Facility> fatchFacilities() throws Exception{
        return (ArrayList<Facility>) loadJSONLookup("facilities", Facility.class);
    }

    

    @SuppressWarnings({ "unchecked", "unchecked", "unchecked", "unchecked" })
	public ArrayList<Products> fetchProducts() throws Exception{
        return (ArrayList<Products>) loadJSONLookup("products", Products.class);
    }
    
    @SuppressWarnings("unchecked")
	public  ArrayList<Programs> fetchPrograms() throws Exception{
        return (ArrayList<Programs>) loadJSONLookup("programs", Programs.class);
    }

    
    @SuppressWarnings("unchecked")
	public  ArrayList<Product_categories> fetchProductCategories() throws Exception{
        return (ArrayList<Product_categories>) loadJSONLookup("product-categories", Product_categories.class);
    }
    
    @SuppressWarnings("unchecked")
	public  ArrayList<Facility_Approved_Products> fetchFacilityApprovedProducts() throws Exception{
        return (ArrayList<Facility_Approved_Products>) loadJSONLookup("product-categories", Facility_Approved_Products.class);
    }
    
    @SuppressWarnings("unchecked")
	public ArrayList<Program_products> fetchProgramproducts() throws Exception{
        return (ArrayList<Program_products>) loadJSONLookup("product-categories", Program_products.class);
    }
    
    @SuppressWarnings("unchecked")
	public  ArrayList<Facility_Types> fetchFacilityTypes() throws Exception{
        return (ArrayList<Facility_Types>) loadJSONLookup("product-categories", Facility_Types.class);
    }
    
   @SuppressWarnings("unchecked")
public  ArrayList<Processing_Periods> fetchProcessingPeriods() throws Exception{
        return (ArrayList<Processing_Periods>) loadJSONLookup("processing-periods", Processing_Periods.class);
    }

    @SuppressWarnings("unchecked")
	public  ArrayList<Losses_Adjustments_Types> fetchLossesAndAdjustmentTypes() throws Exception{
        return (ArrayList<Losses_Adjustments_Types>) loadJSONLookup("losses-adjustments-types", Losses_Adjustments_Types.class);
    }
    

}
