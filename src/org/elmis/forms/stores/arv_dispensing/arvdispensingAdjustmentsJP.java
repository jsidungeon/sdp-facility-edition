/*
 * AdjusmentsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;

import com.oribicom.tools.TableModel;

;
/**
 *
 * @author  __USER__
 * 
 */




public class arvdispensingAdjustmentsJP extends javax.swing.JPanel {
    
	
	
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;

	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;

	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
			"Product Code", "Product name", "Stock on hand",
			"Generic Strength", "Quantity Received" };
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "", "" };
	private static final int rows_fproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 3) {
				return getValueAt(0, 3).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};
	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	
	@SuppressWarnings("unchecked")

	//List<Shipped_Line_Items>  shippedItemsList =  new LinkedList();
	//ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	//ListIterator shipedItemsiterator = shippedItemsList.listIterator();

	FacilityProductsDAO Facilityproductsmapper = null;
	private int rnrid;
	private Timestamp shipmentdate;

	
	public static Boolean cansave = false;
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilitytypeCode;
	
	
	
	
	
	
	
	
	
	public static String selectedprogramCode = null;

	/** Creates new form ProgramsJP */
	public arvdispensingAdjustmentsJP() {
		initComponents();
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		AdjustmentsProductsJT = new javax.swing.JTable();
		AdjustmentsJDate = new com.toedter.calendar.JDateChooser();
		CancelJBtn = new javax.swing.JButton();
		SaveJBtn = new javax.swing.JButton();

		AdjustmentsProductsJT.setFont(new java.awt.Font("Bell MT", 0, 16));
		
		
		jScrollPane1.setViewportView(AdjustmentsProductsJT);

		AdjustmentsJDate.setDateFormatString(mydateformat);
		AdjustmentsJDate.setFont(new java.awt.Font("Bell MT", 0, 16));
	

		CancelJBtn.setFont(new java.awt.Font("Bell MT", 0, 16));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/navigate_left.png"))); // NOI18N
		CancelJBtn.setText("Cancel");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		SaveJBtn.setFont(new java.awt.Font("Bell MT", 0, 16));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/navigate_right.png"))); // NOI18N
		SaveJBtn.setText("Save  ");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jScrollPane1,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																415,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																layout
																		.createSequentialGroup()
																		.addComponent(
																				AdjustmentsJDate,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				207,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				CancelJBtn)
																		.addGap(
																				12,
																				12,
																				12)
																		.addComponent(
																				SaveJBtn)))
										.addContainerGap()));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								layout
										.createSequentialGroup()
										.addContainerGap(132, Short.MAX_VALUE)
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												202,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(24, 24, 24)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.BASELINE)
																		.addComponent(
																				CancelJBtn)
																		.addComponent(
																				SaveJBtn))
														.addComponent(
																AdjustmentsJDate,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap()));
	}// </editor-fold>
	//GEN-END:initComponents

	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		String displaymessage = "";

		//Insert the object to database

	/*	System.out.println(shippedItemsList.size());
		for (@SuppressWarnings("unused")
		Shipped_Line_Items sp : shippedItemsList) {

			saveShipped_Line_Items.setProductcode(sp.getProductcode());
			saveShipped_Line_Items.setRnrid(sp.getRnrid());
			saveShipped_Line_Items.setQuantityshipped(sp.getQuantityshipped());
			saveShipped_Line_Items.setModifieddate(sp.getModifieddate());

			int confirm = javax.swing.JOptionPane.showConfirmDialog(this,
					"Do  you want to save the Received Quantities to the database\n\n"
							+ "Yes - to Save\n\n"
							+ "No -to Edit the Quantity Entered"
							+ "Cancel - to exit Dialog",
					"Saving Quantity Received Information",
					JOptionPane.YES_NO_OPTION);

			if (confirm == JOptionPane.YES_OPTION) {
				Facilityproductsmapper
						.InsertshipmentProducts(saveShipped_Line_Items);
				JOptionPane.showMessageDialog(this,
						"Products saved to database", "Saving to Database",
						JOptionPane.INFORMATION_MESSAGE);
			} else if (confirm == JOptionPane.NO_OPTION) {
				this.AdjustmentsProductsJT.validate();
			}

		}*/
	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
	}

	void AdjustmentsJDatePropertyChange(java.beans.PropertyChangeEvent evt) throws ParseException {
		// TODO add your handling code here:
		try {

			String mydate = this.AdjustmentsJDate.getDate().toString();
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			java.util.Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}
	
	private void facilityapprovedProductsJTPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		if ("tableCellEditor".equals(evt.getPropertyName())) {

			//saveShipped_Line_Items = new Shipped_Line_Items();
			if (AdjustmentsProductsJT.isEditing()) {
				System.out.println("THIS CELL HAS STARTED EDITING");
				//get column index
				colindex = this.AdjustmentsProductsJT.getSelectedColumn();
				rowindex = this.AdjustmentsProductsJT.getSelectedRow();
				Pcode = tableModel_fproducts.getValueAt(
						this.AdjustmentsProductsJT.getSelectedRow(), 0)
						.toString();
				//System.out.println(colindex);	
				//System.out.println(rowindex);	
				//System.out.println(Pcode);

			} else {
				//createshipmentObj(Pcode, rnrid, productdeliverdate);
				System.out.println("THIS CELL IS NOT EDITING");

			}
		}
	}

	private void facilityapprovedProductsJTMouseClicked(
			java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call create  shipment object
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private com.toedter.calendar.JDateChooser AdjustmentsJDate;
	private javax.swing.JTable AdjustmentsProductsJT;
	private javax.swing.JButton CancelJBtn;
	private javax.swing.JButton SaveJBtn;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables

}