package org.elmis.facility.domain.dao;

import org.elmis.facility.webservice.request.WebServiceRequest;

public interface WebServiceRequestMapper {
	
	Integer insertRequest(WebServiceRequest wsRequest);

}
