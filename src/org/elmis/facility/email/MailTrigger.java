package org.elmis.facility.email;

//mail trigger class, responsible for sending emails
//must try different variations, method overloading, switch
//to see most efficient method

import java.util.List;

import org.elmis.facility.domain.model.users;
import org.elmis.facility.main.gui.AppJFrame;

public class MailTrigger {
	private Thread sendEmailThread;
	public String action = "";
	public void sendMail(users user, String matterTitle, int feenoteID, String currencySymbol,
			Double grandTotal, String userName) {
		
		//for fee note
		action = "New Fee note ";
		String msg = "Dear staff \n\n"
			+

			"\n\n*****************************************************\n\n"
			+

			"New Fee Note has been issued to :\n\n"
			+ "Client Name : "
			+ user.getFirstname().trim()
			+ "\n\n"
			+ "File Name : "
			+ matterTitle
			+ "\n\n"
			+ "Fee Note Number : "
			+ feenoteID
			+ "\n\n"
			+ "Amount : "
			+ currencySymbol
			+ " "
			+ grandTotal
			+ "\n\n"
			+ "Issued by : "
			+ userName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";

	// ************************send email notification to list
	// ********************************/
	List<String> mailingList = new EmailNotify()
			.getMailingList("select * from email_notify where feenote = 'yes'");

	sendEmailThread = new MailingListSender(msg, action,
			mailingList);
	sendEmailThread.start();
	

	// *****************************end send email notification
	// *******************************/
	}
	public void sendMail(String clientName, String paymentType, String cheque, 
		String currency, double amount, String details, String userName) {
		
		//new payment recieved
		action = "A New Payment";
		String msg = "Dear staff \n\n"
			+

			"\n\n*****************************************************\n\n" 
			+
			
			action + " has been recieved from :\n\n"
			
			+ "Client Name : " 
			+ clientName 
			+ "\n\n" 
			+ "Payment Type : " 
			+ paymentType 
			+ "\n\n" 
			+ "Cheque Number : " 
			+ cheque 
			+ "\n\n" 
			+ "Currency : " 
			+ currency 
			+ "\n\n" 
			+ "Amount : " 
			+ amount 
			+ "\n\n" 
			+ "Purpose : "
			+ details 
			+ "\n\n" 
			+ "Issued by : "
			+ userName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";
			
		List<String> mailingList = new EmailNotify()
		.getMailingList("select * from email_notify where payment = 'yes'");

		sendEmailThread = new MailingListSender(msg, action,
		mailingList);
		sendEmailThread.start();
		
	}
	 public void sendMail(String fileName, String clientName, String caseTitle, String userName) {
		 //new file added
		 action = "A file has been closed from your LegalManger file list";
		 String msg = "Dear staff \n\n"
			 +
			 action + ":\n\n"
			+ "File Name : "
			+ fileName
			+ "\n\n"
			+ "Client Name : "
			+ clientName
			+ "\n\n"
			+ "File Name : "
			+ caseTitle
			+ "\n\n"
			+ "Closed by : "
			+ userName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";
		 
		 List<String> mailingList = new EmailNotify()
		 .getMailingList("select * from legalserver.email_notify where file_closed = 'yes'");

		 sendEmailThread = new MailingListSender(msg, action, mailingList);
		 sendEmailThread.start();
	 }
	 public void sendMail(String userName, String firstName, String lastName, String jobTitle, String loggedInName) {
		 //new staff added
		 action  = "A new user has been added to your LegalManager user list ";
		 String msg = "Dear staff \n\n"
			 +
			 action + "\n\n"
			+ "UserName : "
			+ userName
			+ "\n\n"
			+ "First Name : "
			+ firstName
			+ "\n\n"
			+ "Last Name : "
			+ lastName
			+ "\n\n"
			+ "Job Title : "
			+ jobTitle
			+ "\n\n"
			+ "Added by : "
			+ loggedInName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";
		 
		 List<String> mailingList = new EmailNotify()
		 .getMailingList("select * from email_notify where new_staff = 'yes'");
		 
		 //debugging to show who's getting the email notifications
//		 System.out.println("the list is"); 
//		 for (int i = 0; i< mailingList.size(); i++) {
//			 System.out.println(mailingList.get(i));
//		 }

		 sendEmailThread = new MailingListSender(msg, action, mailingList);
		 sendEmailThread.start();
	 }
	 public void sendMail(String staffMember, String adminName) {
		//staff member deleted
		 action  = "A user has been deleted from your LegalManager user list";
		 String msg = "Dear staff \n\n"
			 +
			 action + "\n\n"
			+ "StaffMember : "
			+ staffMember
			+ "\n\n"
			+ "Deleted by : "
			+ adminName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";
		 
		 List<String> mailingList = new EmailNotify()
		 .getMailingList("select * from email_notify where staff_deleted = 'yes'");
		 
		 sendEmailThread = new MailingListSender(msg, action, mailingList);
		 sendEmailThread.start();
	 }
	 
	 public void sendMail(String staffMember, String updatedRole,  String adminName) {
			//staff member access level changed 
			 action  = "The following user has had their LegalManager access level changed ";
			 String msg = "Dear staff \n\n"
				 +
				 action + "\n\n"
				+ "StaffMember : "
				+ staffMember
				+ "\n\n"
				+ "Updated To Role Of : "
				+ updatedRole
				+ "\n\n"
				+ "Updated by : "
				+ adminName
				+ "\n\n"
				+ "\n\n*****************************************************\n\n"
				+ "LegalManager.";
			 
			 List<String> mailingList = new EmailNotify()
			 .getMailingList("select * from email_notify where staff_access_level = 'yes'");
			 
			 sendEmailThread = new MailingListSender(msg, action, mailingList);
			 sendEmailThread.start();
		 }
	 
	
	 public void sendMail(String invoiceCustomer, String fileNumber, String matterTitle, String currencySymbol,
			  int invoiceId, Double grandTotal) {
		//when invoice is issued
		 action  = "A new Invoice has been issued  ";
		 String msg = "Dear staff \n\n"
			 +
			 action + "\n\n"
			+ "Issued To : "
			+ invoiceCustomer
			+ "\n\n"
			+ "File Number : "
			+ fileNumber
			+ "\n\n"
			+ "Invoice Number : "
			+ invoiceId
			+ "\n\n"
			+ "MatterTitle : "
			+ matterTitle
			+ "\n\n"
			+ "GrandTotal : "
			+ currencySymbol + " " + grandTotal
			+ "\n\n"
			+ "Issued By: "
			+ AppJFrame.userName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";
		
		 List<String> mailingList = new EmailNotify()
		 .getMailingList("select * from email_notify where invoice = 'yes'");
		 
		 sendEmailThread = new MailingListSender(msg, action, mailingList);
		 sendEmailThread.start();
	 }
	 public void sendMail(String clientDeleted) {
		 //for when client is deleted
		 action  = "A Client has been deleted from your LegalManager Client List ";
		 String msg = "Dear staff \n\n"
			 +
			 action + "\n\n"
			+ "Client Name : "
			+ clientDeleted
			+ "\n\n"
			+ "Deleted By: "
			+ AppJFrame.userName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";
		
		 List<String> mailingList = new EmailNotify()
		 .getMailingList("select * from email_notify where client_deleted = 'yes'");
		 
		 sendEmailThread = new MailingListSender(msg, action, mailingList);
		 sendEmailThread.start();
		 
		 
	 }
	 public void sendsMail(String clientName, String fileMatter, String handledBy) {
		 //file added
		 action = "A file has been deleted from your LegalManger file list";
		 String msg = "Dear staff \n\n"
			 +
			 action + ":\n\n"
			+ "Client Name : "
			+ clientName
			+ "\n\n"
			+ "File Matter : "
			+ fileMatter
			+ "\n\n"
			+ "Handled By : "
			+ handledBy
			+ "\n\n"
			+ "Deleted By : "
			+ AppJFrame.userName
			+ "\n\n"
			+ "\n\n*****************************************************\n\n"
			+ "LegalManager.";
		 
		 List<String> mailingList = new EmailNotify()
		 .getMailingList("select * from legalserver.email_notify where file_deleted = 'yes'");

		 sendEmailThread = new MailingListSender(msg, action, mailingList);
		 sendEmailThread.start();
	 }
}
