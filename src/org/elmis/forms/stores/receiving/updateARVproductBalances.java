package org.elmis.forms.stores.receiving;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.domain.model.Adjustment;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;

public class updateARVproductBalances {
	
		
	FacilityProductsDAO Facilityproductsmapper = null;
	ARVDispensingDAO aFacilityproductsmapper = null;
	@SuppressWarnings({ "unchecked", "rawtypes" })
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	List<Adjustment> adjustmentList = new ArrayList<>();
	private String apcode = "";
	
	public updateARVproductBalances(List<Adjustment> adjustList){
		
		
       Facilityproductsmapper =  new FacilityProductsDAO();
		
		aFacilityproductsmapper =  new  ARVDispensingDAO();
		adjustmentList = adjustList;
		
		for( Adjustment ad: adjustmentList ){
			createshipmentObj(ad);
		}
		
		
		
	}
	public void createshipmentObj(Adjustment d) {
		//System.out.println("Did object call me ???");
				//created a shipment object 
	
		double positive = d.getAdjustmentQty();
		if(positive < 0 ){
			positive = - (positive);
		}
		
		apcode = d.getProductCode();
				if(!(adjustmentList.isEmpty())){
		        
				//if (d.getAdjustmentType().equalsIgnoreCase("TRANSFER_OUT")) {
					
					if (!(d.getAdditive())) {
			
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(d.getProductCode(),AppJFrame.getDispensingId("ARV"));
						
					
						if(productcodesstockQtyList.size() > 0){
							
							for (@SuppressWarnings("unused") ProductQty p : productcodesstockQtyList) {
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					

				}
				
				
				if (d.getAdditive()) {
					
						
					try{
						//Facilityproductsmapper =  new 	facilityproductssetupsessionmapperscalls();   
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
								
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
											
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}

				}
			 
			
		/*		if (d.getAdjustmentType().equalsIgnoreCase("DAMAGED")) {
				
	
					
		        
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));;
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
		
				
				

				if (d.getAdjustmentType().equalsIgnoreCase("COMPUTER_GENERATED (-)")) {

					
					
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				
			
				

				if (d.getAdjustmentType().equalsIgnoreCase("RETURN (-)")) {

					
					
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				
				
				
				

				if (d.getAdjustmentType().equalsIgnoreCase("LOST")) {

					
					
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
					 
				
				if (d.getAdjustmentType().equalsIgnoreCase("STOLEN")) {

					
					
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
					 
				

				if (d.getAdjustmentType().equalsIgnoreCase("EXPIRED")) {

					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					
					
					
				}
				

				if (d.getAdjustmentType().equalsIgnoreCase("PASSED_OPEN_VIAL_TIME_LIMIT")) {
					

					
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				

				if (d.getAdjustmentType().equalsIgnoreCase("COLD_CHAIN_FAILURE")) {
					

					
					
						try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				
				if (d.getAdjustmentType().equalsIgnoreCase("CLINIC_RETURN")) {
					
				
					
					try{
						//aFacilityproductsmapper = new facilityarvdispensingsessionsmappercalls();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() - positive);
									aFacilityproductsmapper.doupdatearvdispenseqtyless(apcode,positive,p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId("ARV"));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
			  
				
				
				if (d.getAdjustmentType().equalsIgnoreCase("RETURN (+)")) {

					
					
					try{
						//Facilityproductsmapper =  new 	facilityproductssetupsessionmapperscalls();   
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				
				
				if (d.getAdjustmentType().equalsIgnoreCase("COMPUTER_GENERATED(+)")) {

					
					
					try{
						//Facilityproductsmapper =  new 	facilityproductssetupsessionmapperscalls();   
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				
				
				
				
				
				if (d.getAdjustmentType().equalsIgnoreCase("FOUND")) {

					
					
					try{
						//Facilityproductsmapper =  new 	facilityproductssetupsessionmapperscalls();   
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (d.getAdjustmentType().equalsIgnoreCase("PURCHASE")) {
					
	
					
					try{
						//Facilityproductsmapper =  new 	facilityproductssetupsessionmapperscalls();   
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId("ARV"));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j){
							j.getMessage();
						}
			
	}*/
				
				
				}		
 }
				

}
