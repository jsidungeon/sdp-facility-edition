/*
 * ReportsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JPanel;

import net.sf.jasperreports.engine.JasperPrint;

import org.elmis.facility.reporting.ReportAndRequisition;
import org.elmis.facility.reporting.SatelliteReportandRequisition;
import org.elmis.facility.reporting.SatelliteReportandRequisitionJD;
import org.elmis.facility.search.reports.RandRSearchJd;
import org.elmis.forms.reports.consumptiontrend.SelectProductsJD;

/**
 * 
 * @author __USER__
 */
public class ReportsJP extends javax.swing.JPanel {

	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	private static java.util.Date startDate;
	private static java.util.Date endDate;
	private final static Cursor busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final static Cursor defaultCursor = Cursor.getDefaultCursor();
	private JPanel jPanel4;
	private ReportsJP _this = this;

	/** Creates new form ReportsJP */
	public ReportsJP() {
		initComponents();

		jLabel4.setVisible(true);
		jLabel5.setVisible(false);

		stockTrendsJL2.setVisible(true);
		arvpatientregimenJL.setVisible(true);
		SatelliteRandRJL.setVisible(true);

		//weeklyRptJL.setVisible(false);
		//staffproductivityJL.setVisible(false);

		// productivityJBtn.setVisible(false);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		stockTrendsJL = new javax.swing.JLabel();
		SatelliteRandRJL = new javax.swing.JLabel();
		stockTrendsJL2 = new javax.swing.JLabel();
		jLabel4 = new javax.swing.JLabel();
		jLabel4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				try {
					//AppJFrame.glassPane.activate(null);
					ReportsJP.this.setCursor(busyCursor);
					RandRSearchJd dialog = new RandRSearchJd(null, true);
					dialog.pack();
					dialog.setVisible(true);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(ReportsJP.this, "" + e.getLocalizedMessage(),
							"Error - Contact System Administrator",
							JOptionPane.ERROR_MESSAGE);
				} finally {
					ReportsJP.this.setCursor(defaultCursor);
					//AppJFrame.glassPane.deactivate();
				}
			}
		});
		jPanel4 = new javax.swing.JPanel();

		arvpatientregimenJL = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		stockTrendsJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		stockTrendsJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		stockTrendsJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS stock trends icon.png"))); // NOI18N
		stockTrendsJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Stock Trends",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		stockTrendsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				stockTrendsJLMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				stockTrendsJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				stockTrendsJLMouseExited(evt);
			}
		});

		SatelliteRandRJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		SatelliteRandRJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS stock out report icon.png"))); // NOI18N
		SatelliteRandRJL.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Satellite R & R",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		SatelliteRandRJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SatelliteRandRJLMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				jLabel2MouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				jLabel2MouseExited(evt);
			}
		});

		stockTrendsJL2
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		stockTrendsJL2.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS view r&r.png"))); // NOI18N
		stockTrendsJL2.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "View R & R",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		stockTrendsJL2.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				createRandrDoubleMouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				stockTrendsJL2MouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				stockTrendsJL2MouseExited(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS archived r&r.png"))); // NOI18N
		jLabel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Archived R & R",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jLabel4MouseClicked(evt);
			}
			public void mouseEntered(java.awt.event.MouseEvent evt) {
				jLabel4MouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				jLabel4MouseExited(evt);
			}
		});


		arvpatientregimenJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		arvpatientregimenJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		arvpatientregimenJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS arv regiment.png"))); // NOI18N
		arvpatientregimenJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "ARV Regimen",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		arvpatientregimenJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				arvpatientregimenJLMouseClicked(evt);
			}
		});

		jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS archived r&r.png"))); // NOI18N
		jLabel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Archived R & R",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						stockTrendsJL, javax.swing.GroupLayout.PREFERRED_SIZE,
						138, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18,
						18, 18).addComponent(SatelliteRandRJL,
						javax.swing.GroupLayout.PREFERRED_SIZE, 134,
						javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18,
						18).addComponent(stockTrendsJL2,
						javax.swing.GroupLayout.PREFERRED_SIZE, 133,
						javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18,
						18).addComponent(jLabel4,
						javax.swing.GroupLayout.PREFERRED_SIZE, 142,
						javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18,
						18).addComponent(jLabel5,
						javax.swing.GroupLayout.PREFERRED_SIZE, 122,
						javax.swing.GroupLayout.PREFERRED_SIZE).addGap(18, 18,
						18).addComponent(arvpatientregimenJL,
						javax.swing.GroupLayout.PREFERRED_SIZE, 125,
						javax.swing.GroupLayout.PREFERRED_SIZE)
						.addContainerGap(51, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				javax.swing.GroupLayout.Alignment.TRAILING,
				layout.createSequentialGroup().addContainerGap().addGroup(
						layout.createParallelGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								false).addComponent(stockTrendsJL,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addComponent(
								arvpatientregimenJL,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addComponent(jLabel5,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addComponent(jLabel4,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addComponent(SatelliteRandRJL,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								
								Short.MAX_VALUE).addComponent(stockTrendsJL2,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE, 134,
								Short.MAX_VALUE)).addGap(73, 73, 73)));
	}// </editor-fold>
	//GEN-END:initComponents

	public static void infoBox(String infoMessage, String location) {
		JOptionPane.showMessageDialog(null, infoMessage, "Information: "
				+ location, JOptionPane.INFORMATION_MESSAGE);
	}

	@SuppressWarnings("unused")
	private void arvpatientregimenJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//infoBox("Under Constructiion", "Report under construction");
		ARVRegimenReportDateRange arvRegimenReportDateRange = new ARVRegimenReportDateRange( javax.swing.JOptionPane.getFrameForComponent(_this) , true);
		arvRegimenReportDateRange.setVisible(true);
	}

	private void createRandrDoubleMouseClicked(java.awt.event.MouseEvent evt) {
		try {
			//AppJFrame.glassPane.activate(null);
			ReportsJP.this.setCursor(busyCursor);
			ReportAndRequisition dialog = new ReportAndRequisition(
					javax.swing.JOptionPane.getFrameForComponent(this), true,
					"username");
			dialog.pack();
			dialog.setVisible(true);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "" + e.getLocalizedMessage(),
					"Error - Contact System Administrator",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			ReportsJP.this.setCursor(defaultCursor);
			//AppJFrame.glassPane.deactivate();
		}
	}

	private void stockTrendsJL1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
	}

	private void stockTrendsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//	new DateIntervalJD(javax.swing.JOptionPane.getFrameForComponent(this),
		//		true);
		//startDate = new DateRange().getStartDate();
		//	endDate = new DateRange().getEndDate();

		/*	JFreeChart chartIn = new TimeSeriesChart().createChart("Consumption Trend for Acyclovir, tablet 400mg");
			
			AppJFrame.glassPane.activate(null);

			new TimeSeriesJD(javax.swing.JOptionPane.getFrameForComponent(this),
					true, chartIn);
			
			AppJFrame.glassPane.deactivate();*/
		AppJFrame.glassPane.activate(null);
		new SelectProductsJD(
				javax.swing.JOptionPane.getFrameForComponent(this), true);
		AppJFrame.glassPane.deactivate();

	}

	private void stockTrendsJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		stockTrendsJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS stock trends icon.png")));
	}

	private void stockTrendsJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		stockTrendsJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS stock trends icon big.png")));
	}

	private void stockTrendsJL2MouseClicked(java.awt.event.MouseEvent evt) {

	}

	private void stockTrendsJL2MouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		stockTrendsJL2.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS view r&r.png")));
	}

	private void stockTrendsJL2MouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		stockTrendsJL2
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMIS view r&r big.png")));
	}

	private void SatelliteRandRJLMouseClicked(java.awt.event.MouseEvent evt) {
		AppJFrame.glassPane.activate(null);
		new SatelliteReportandRequisitionJD(javax.swing.JOptionPane.getFrameForComponent(this), true);
		AppJFrame.glassPane.deactivate();

	}

	private void jLabel2MouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		SatelliteRandRJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS stock out report icon.png")));
	}

	private void jLabel2MouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		SatelliteRandRJL
				.setIcon(new javax.swing.ImageIcon(
						getClass()
								.getResource(
										"/elmis_mouseover_images/eLMIS stock out report icon big.png")));
	}

	private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {

	}

	private void jLabel4MouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS archived r&r.png")));
	}

	private void jLabel4MouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS archived r&r big.png")));
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel arvpatientregimenJL;
	private javax.swing.JLabel SatelliteRandRJL;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel stockTrendsJL;
	private javax.swing.JLabel stockTrendsJL2;
	// End of variables declaration//GEN-END:variables

}