/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.json.beans;

import java.util.List;

import org.elmis.facility.reporting.model.ReportLineItem;

/**
 * @author MMwebaze
 *
 */
public class ReportBean {
	
	
	// change two:
	// the shape of the report bean has been slightly changed
	// the headings as they are here do not work 
	// though we will have to revise this in the future, please use the following headers instead
	/*private int facilityId;
	private int programId;
	private int periodId;
	private int userId;
	private boolean emergency;
	
	
	
	public boolean isEmergency() {
		return emergency;
	}
	public void setEmergency(boolean emergency) {
		this.emergency = emergency;
	}
	
	//private Vendor vendor;
	
	public int getFacilityId() {
		return facilityId;
	}
	public void setFacilityId(int facilityId) {
		this.facilityId = facilityId;
	}
	public int getProgramId() {
		return programId;
	}
	public void setProgramId(int programId) {
		this.programId = programId;
	}
	public int getPeriodId() {
		return periodId;
	}
	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	/*public Vendor getVendor() {
		return vendor;
	}
	public void setVendor(Vendor vendor) {
		this.vendor = vendor;
	}*/
	
	//NOTE: AgentName is equivalent of Facility Code eg (9090A6);
	private String agentCode;
	private String programCode;
	private Integer periodId;
	private String approverName;
	private Boolean emergency;
	public Boolean getEmergency() {
		return emergency;
	}

	public void setEmergency(Boolean emergency) {
		this.emergency = emergency;
	}

	private List<ReportLineItem> products;
	
	public String getAgentCode(){
		return agentCode;
	}
	
	public void setAgentCode(String code){
		agentCode = code;
	}
	
	
	public String getProgramCode(){
		return programCode;
	}
	
	public void setProgramCode(String code){
		programCode = code;
	}
	
	public String getApproverName(){
		return approverName;
	}
	
	public void setApproverName(String name){
		approverName = name;
	}
	
	public List<ReportLineItem> getProducts() {
		return products;
	}
	public void setProducts(List<ReportLineItem> products) {
		this.products = products;
	}
	
	public Integer getPeriodId() {
		return periodId;
	}
	
	public void setPeriodId(Integer periodId) {
		this.periodId = periodId;
	}

	public boolean reportIsValid(){
		if(this.emergency == null){
			return false;
		}
		if(this.emergency == false && this.periodId == null){
			return false;
		}
		if(this.agentCode == null || this.programCode == null || this.approverName == null 
				|| this.products == null){
			return false;
		}
						
			
		for (ReportLineItem productReportItem : this.products) {
			if(!lineItemIsValid(productReportItem)){
				System.out.println(productReportItem.getProductCode() + "line has errors");
				return false;
			}
		}
		return true;
	}
	
	private boolean lineItemIsValid(ReportLineItem item){
		if(item.getReasonForRequestedQuantity() == null || 
				item.getReasonForRequestedQuantity().trim().length() < 1){
			return false;
		} 
		/* 
		   (quantityDispensed == (beginningBalance + quantityReceived + totalLossesAndAdjustments - stockInHand));
             }
            boolean valid = quantityDispensed >= 0 && stockInHand >= 0 && validQuantityDispensed;

		 * */
		return true;
	}
}
