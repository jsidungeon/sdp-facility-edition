package org.elmis.facility.connections;

public enum DBType {

	MYSQL, HSQL, ORACLE, MSQL, POSTGRESQL, SQLITE;
}
