/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reports.utils;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.dao.ReportRequisitionDao;
import org.elmis.facility.reporting.model.Amc;
import org.elmis.facility.reporting.model.ReportRequisition;


/**
 * AmcCalculator.java
 * Purpose: Used to calculate the AMC of a product
 * @author Michael Mwebaze
 * @version 1.0
 */
public class AmcCalculator {
	
	private int maxStockLevel;
	//private CalendarUtil calendarUtil;
	private PropertyLoader prop = new PropertyLoader();
	private SqlSessionFactory sqlSessionFactory;
	
	//public AmcCalculator()
	{	
		//calendarUtil = new CalendarUtil();
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	/**
	 * 
	 * @param programCode program area code e.g 
	 * @param endRange end date of the period in which products were issued
	 * @param productCode product code whose amc is to be calculated
	 * @return a list of product quantities that were issued up to 
	 * endRange 
	 */

	public int calculateAmc(@Param("productCode")String productCode)
	{
		int amc = 0;
		Map<String, String> map = new HashMap<>();
		map.put("productCode", productCode);
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			List<Integer> pastConsumptionList = new ArrayList<>();
			List<Amc> list =  session.selectList("Amc.amc", map);
			for (Amc c : list)
				pastConsumptionList.add(c.getQtyIssued());
			if (pastConsumptionList.isEmpty())
				return amc;
			else
			{
				int x = 0;
				int control = 0;
				while(x < pastConsumptionList.size())
				{
					int consumption = pastConsumptionList.get(x);
					if ( consumption != 0)
					{
						if (control < 3)
						{
							amc = amc + consumption;
							control++;
						}
						if (control == 3)
							break;
					}
					x++;
				}
				return amc/control;
			}
		}
		finally
		{
			session.close();
		}
	}
	public int amcCalculator(String productCode, int currentConsumption)
	{
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("productCode", productCode);
		Integer monthlyConsumption = null;
		Integer consumptionData = 0;
		int control = 0;
		try{
			CalendarUtil calU = new CalendarUtil();
			for (int i = 0; i < 2; i++)
			{
				Date date = calU.getSqlDate(calU.getPreviousLastDayOfMonth(i));
				map.put("date", date);
				monthlyConsumption = session.selectOne("Amc.amc_by_date", map);
				if (monthlyConsumption != null){
					consumptionData += monthlyConsumption;
					control++;
					System.out.println(control+" productCode :"+productCode+" Consumption data : "+consumptionData);
				}
				else
					monthlyConsumption = 0;
			}
			return (consumptionData + currentConsumption)/(control+1);
		}
		finally{
			session.close();
		}
	}
}
