package org.elmis.facility.reporting.model;

import java.sql.Date;

public class LaboratoryTestInformation {

	private String testName;
	private int machineTestCount;
	private int monthTotal;
	private String equipFunction;
	private Date beginDate;
	
	public Date getBeginDate() {
		return beginDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	private Date endDate;
	public Date getEndDate() {
		return endDate;
	}
	private int id;
	private int testId;
	
	public int getTestId() {
		return testId;
	}
	public void setTestId(int testId) {
		this.testId = testId;
	}
	public String getTestName() {
		return testName;
	}
	public void setTestName(String testName) {
		this.testName = testName;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMachineTestCount() {
		return machineTestCount;
	}
	public void setMachineTestCount(int machineTestCount) {
		this.machineTestCount = machineTestCount;
	}
	public int getMonthTotal() {
		return monthTotal;
	}
	public void setMonthTotal(int monthTotal) {
		this.monthTotal = monthTotal;
	}
	public String getEquipFunction() {
		return equipFunction;
	}
	public void setEquipFunction(String equipFunction) {
		this.equipFunction = equipFunction;
	}
}
