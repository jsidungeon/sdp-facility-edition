/*
 * StartJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.HivTestResultDao;
import org.elmis.facility.domain.model.HivTest;
import org.elmis.facility.domain.model.HivTestDar;
import org.elmis.facility.domain.model.HivTestLossesAdustments;
import org.elmis.facility.domain.model.HivTestPhysicalCount;
import org.elmis.facility.domain.model.HivTestProducts;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.forms.admin.dispensingpoints.SelectMyDateJD;
import org.elmis.forms.admin.dispensingpoints.SelectMyDateRangeJD;
import org.elmis.forms.hivtest.EditTransactionJD;
import org.elmis.forms.hivtest.RecordTestJD;
import org.elmis.forms.hivtest.physicalcount.SelectProductToCountJD;
import org.elmis.forms.stores.arv_dispensing.hivdispensingproductAdjustmentsJD;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.publicMethods;

/**
 * 
 * @author __USER__
 */
public class HivTestJP extends javax.swing.JPanel implements ActionListener {

	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	private static java.util.Date startDate;
	private static java.util.Date endDate;
	private int siteId = AppJFrame.getDispensingId("HIV");
	private List<HivTest> hivTestList;// = new LinkedList();
	private List<HivTestDar> hivTestDarList;// = new LinkedList();
	private List<HivTestProducts> HivTestProductsList;
	private List<HivTestLossesAdustments> hivtestLossesAdjustmentList;
	private List<HivTestPhysicalCount> physicalCountList;
	@SuppressWarnings("unused")
	private List<HivTestPhysicalCount> physicalCountperdayList;
	private List<HivTestPhysicalCount> beginingBalanceList;
	private List<HivTestPhysicalCount> beginingBalanceperdayList;
	@SuppressWarnings("unused")
	private List<HivTestLossesAdustments> lossesAdjustmentList;
	private List<StockControlCard> sccList;
	private HivTestDar dar;

	/** Creates new form StartJP */
	public HivTestJP(List<String> userRightsList) {
		initComponents();

		// organiserJL.setVisible(false);

		hivTestingJL.setVisible(false);
		//physicalCountJL.setVisible(false);
		editTransactionLbl.setVisible(false);
		viewDARJL1.setVisible(false);
		viewDARJL2.setVisible(false);
		jLabel1.setVisible(false);
		jLabel3.setVisible(false);
		jPanel1.setVisible(false);
		jPanel2.setVisible(false);
		physicalCountJL.setVisible(false);
		

		for (String userRights : userRightsList) {

			// hiv Testing
			if (this.hivTestingJL.getName().equalsIgnoreCase(userRights.trim())) {
				hivTestingJL.setVisible(true);
			}
		}

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		physicalCountJL = new javax.swing.JLabel();
		editTransactionLbl = new javax.swing.JLabel();
		jPanel1 = new javax.swing.JPanel();
		viewDARJL1 = new javax.swing.JLabel();
		viewDARJL2 = new javax.swing.JLabel();
		jPanel2 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		viewDARJL = new javax.swing.JLabel();
		losses_adjustmentsJL = new javax.swing.JLabel();
		hivTestingJL = new javax.swing.JLabel();
		viewPrevDARSJL = new javax.swing.JLabel();
		viewPrevDARSJL1 = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		physicalCountJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		physicalCountJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS Physical count.png"))); // NOI18N
		physicalCountJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Physical Count",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		physicalCountJL
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		physicalCountJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				physicalCountJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				physicalCountJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				physicalCountJLMouseExited(evt);
			}
		});

		editTransactionLbl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		editTransactionLbl.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS adjustments icon.png"))); // NOI18N
		editTransactionLbl.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Edit Transaction",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		editTransactionLbl.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				editTransactionJLMouseClicked(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Daily Activity Register (DAR)",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		jPanel1.setFont(new java.awt.Font("Ebrima", 1, 12));

		viewDARJL1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewDARJL1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS select date.png"))); // NOI18N
		viewDARJL1.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Select Date",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewDARJL1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewDARJL1MouseClicked(evt);
			}
		});

		viewDARJL2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewDARJL2.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS date set icon.png"))); // NOI18N
		viewDARJL2.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Date Range",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewDARJL2.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewDARJL2MouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addGap(131, 131, 131)
										.addComponent(
												viewDARJL1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												111,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												viewDARJL2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												111,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel1Layout.createSequentialGroup().addGroup(
						jPanel1Layout.createParallelGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								false).addComponent(viewDARJL2,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addComponent(viewDARJL1,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)).addContainerGap(47,
						Short.MAX_VALUE)));

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));
		jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Charts",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS usage by purpose.png"))); // NOI18N
		jLabel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Usage by purpose",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS RvsNR.png"))); // NOI18N
		jLabel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"R vs. NR",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout
				.setHorizontalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												113,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jLabel3,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												113,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(144, Short.MAX_VALUE)));
		jPanel2Layout.setVerticalGroup(jPanel2Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				jPanel2Layout.createSequentialGroup().addGroup(
						jPanel2Layout.createParallelGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								false).addComponent(jLabel3,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE).addComponent(jLabel1,
								javax.swing.GroupLayout.Alignment.LEADING,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								javax.swing.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)).addContainerGap(47,
						Short.MAX_VALUE)));

		viewDARJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewDARJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS dar icon.png"))); // NOI18N
		viewDARJL.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"DAR", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewDARJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				System.out.println("HIVTestJP "+siteId);
				CalendarUtil calU = new CalendarUtil();
				new Thread(new ProgressBarJD("Generating HIV Daily Activity Register Report",calU.getSqlDate(calU.getTodayDate()), siteId)).start();
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				viewDARJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				viewDARJLMouseExited(evt);
			}
		});

		losses_adjustmentsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		losses_adjustmentsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS losses&adjust.png"))); // NOI18N
		losses_adjustmentsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Adjustments",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		losses_adjustmentsJL
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						losses_adjustmentsJLMouseClicked(evt);
					}

					public void mouseEntered(java.awt.event.MouseEvent evt) {
						losses_adjustmentsJLMouseEntered(evt);
					}

					public void mouseExited(java.awt.event.MouseEvent evt) {
						losses_adjustmentsJLMouseExited(evt);
					}
				});

		hivTestingJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		hivTestingJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS HIV testing.png"))); // NOI18N
		hivTestingJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "HIV Testing",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		hivTestingJL
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		hivTestingJL.setName("HIV_TESTING");
		hivTestingJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				hivTestingJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				hivTestingJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				hivTestingJLMouseExited(evt);
			}
		});

		viewPrevDARSJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewPrevDARSJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS usage by purpose.png"))); // NOI18N
		viewPrevDARSJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "View Archived DARs",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewPrevDARSJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewPrevDARSJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				viewPrevDARSJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				viewPrevDARSJLMouseExited(evt);
			}
		});

		viewPrevDARSJL1
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewPrevDARSJL1.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS usage by purpose.png"))); // NOI18N
		viewPrevDARSJL1.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Physical Count",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewPrevDARSJL1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewPrevDARSJL1MouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				viewPrevDARSJL1MouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				viewPrevDARSJL1MouseExited(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addComponent(
																				jPanel1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				jPanel2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addComponent(
																				physicalCountJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				128,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				hivTestingJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				128,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				2,
																				2,
																				2)
																		.addComponent(
																				editTransactionLbl,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				128,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				viewDARJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				128,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				losses_adjustmentsJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				137,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				viewPrevDARSJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				137,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				viewPrevDARSJL1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				144,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap(821, Short.MAX_VALUE)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																viewPrevDARSJL1)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addGroup(
																				layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								physicalCountJL)
																						.addComponent(
																								hivTestingJL)
																						.addComponent(
																								editTransactionLbl)
																						.addComponent(
																								viewDARJL)
																						.addComponent(
																								losses_adjustmentsJL)
																						.addComponent(
																								viewPrevDARSJL)
																						.addComponent(
																										viewPrevDARSJL1))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING,
																								false)
																						.addComponent(
																								jPanel2,
																								javax.swing.GroupLayout.Alignment.TRAILING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE)
																						.addComponent(
																								jPanel1,
																								javax.swing.GroupLayout.Alignment.TRAILING,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								Short.MAX_VALUE))))
										.addGap(44, 44, 44)));
	}// </editor-fold>
	//GEN-END:initComponents

	private void editTransactionJLMouseClicked(MouseEvent evt) {
		try{
			AppJFrame.glassPane.activate(null);
			
			new EditTransactionJD(javax.swing.JOptionPane.getFrameForComponent(this), true).setVisible(true);
		}
		finally{
			AppJFrame.glassPane.deactivate();
		}
	}

	private void viewPrevDARSJL1MouseExited(java.awt.event.MouseEvent evt) {
		viewPrevDARSJL1.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS usage by purpose.png")));
	}

	private void viewPrevDARSJL1MouseEntered(java.awt.event.MouseEvent evt) {
		viewPrevDARSJL1.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_mouseover_images/eLMIS usage by purpose big.png")));
	}

	private void viewPrevDARSJLMouseExited(java.awt.event.MouseEvent evt) {
		viewPrevDARSJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS usage by purpose.png")));
	}

	private void viewPrevDARSJLMouseEntered(java.awt.event.MouseEvent evt) {
		viewPrevDARSJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_mouseover_images/eLMIS usage by purpose big.png")));
	}

	private void viewPrevDARSJLMouseClicked(java.awt.event.MouseEvent evt) {
		AppJFrame.glassPane.activate(null);

		new SelectMyDateJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);
		AppJFrame.glassPane.deactivate();
	}

	private void viewPrevDARSJL1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("***&****");
		try{
			AppJFrame.glassPane.activate(null);
			// new
			// PhysicalCountJD(javax.swing.JOptionPane.getFrameForComponent(this),
			// true);

			new SelectProductToCountJD(javax.swing.JOptionPane
					.getFrameForComponent(this), true, "HIV Tests Physical Count","", siteId);
		}finally{
			AppJFrame.glassPane.deactivate();
		}
	}

	private void losses_adjustmentsJLMouseExited(java.awt.event.MouseEvent evt) {
		losses_adjustmentsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS losses&adjust.png")));
	}

	private void losses_adjustmentsJLMouseEntered(java.awt.event.MouseEvent evt) {
		losses_adjustmentsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS adjustments icon big.png")));
	}

	private void viewDARJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		viewDARJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS dar big.png")));
	}

	private void viewDARJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		viewDARJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS dar icon.png")));
	}

	private void physicalCountJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		physicalCountJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS physical count.png")));
	}

	private void physicalCountJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		physicalCountJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMIS physical count big.png")));
	}

	private void hivTestingJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		hivTestingJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS HIV testing.png")));
	}

	private void hivTestingJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		hivTestingJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS HIV testing big.png")));
	}

	private void losses_adjustmentsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
/*
		AppJFrame.glassPane.activate(null);

		new AdjustmentTableJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
*/
		// TODO add your handling code here:
		// TODO add your handling code here:
		try {
			AppJFrame.glassPane.activate(null);
			new hivdispensingproductAdjustmentsJD(
					javax.swing.JOptionPane.getFrameForComponent(this), true, siteId);
		} finally {
			AppJFrame.glassPane.deactivate();
		}
		
	}

	private void viewDARJL2MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
	}

	private void viewDARJL1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
	}

	private void physicalCountJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		AppJFrame.glassPane.activate(null);
		// new
		// PhysicalCountJD(javax.swing.JOptionPane.getFrameForComponent(this),
		// true);

		new SelectProductToCountJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true, "HIV Tests Physical Count","",siteId);

		AppJFrame.glassPane.deactivate();
	}

	@SuppressWarnings("unchecked")
	private void viewDARJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO View Dar Function

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		hivTestList = new LinkedList();

		//use this function of working from embedded database

		try {

			if (System.getProperty("dbhost").equalsIgnoreCase("localhost")) {
				/*
				this.hivtestLossesAdjustmentList = session.selectList("selectHivTestLossesAdjustments");
				String today = publicMethods.DateFormatYYYYmmdd
						.format(publicMethods.toDay());

				this.hivTestList = session
						.selectList("selectHivTestsFromHSQLDB");
				
				this.HivTestProductsList = session
						.selectList("selectHIVTestProducts");

				this.physicalCountList = session
						.selectList("selectHivTestPhysicalCountHSQLDB");
				
				this.hivtestLossesAdjustmentList = session.selectList("selectHivTestLossesAdjustments");
				this.physicalCountperdayList = session.selectList("selectHivTestPhysicalcountperday");
				this.beginingBalanceperdayList = session.selectList("selectHivTestbeginingBalanceperday");
				 */
				this.hivTestList = session.selectList("selectHivTests");

				this.HivTestProductsList = session
						.selectList("selectHIVTestProducts");

				this.physicalCountList = session
						.selectList("selectHivTestPhysicalCount");

				this.hivtestLossesAdjustmentList = session
						.selectList("selectHivTestLossesAdjustments");

				this.physicalCountperdayList = session
						.selectList("selectHivTestPhysicalcountperday");

				this.beginingBalanceperdayList = session
						.selectList("selectHivTestbeginingBalanceperday");

			} else {

				this.hivTestList = session.selectList("selectHivTests");

				this.HivTestProductsList = session
						.selectList("selectHIVTestProducts");

				this.physicalCountList = session
						.selectList("selectHivTestPhysicalCount");

				this.hivtestLossesAdjustmentList = session
						.selectList("selectHivTestLossesAdjustments");

				this.physicalCountperdayList = session
						.selectList("selectHivTestPhysicalcountperday");

				this.beginingBalanceperdayList = session
						.selectList("selectHivTestbeginingBalanceperday");
			}

		} finally {
			session.close();
		}

		try {

			//populate physical count
			for (HivTestPhysicalCount physicalCount : physicalCountList) {

				System.out.println(physicalCount.getProduct_code() + "  "
						+ physicalCount.getQty_count());
				//}

				//populate products list
				for (HivTestProducts products : this.HivTestProductsList) {

					for (HivTestLossesAdustments l : hivtestLossesAdjustmentList) {
						if (l.getProduct_name().equalsIgnoreCase("Determine")) {
							parameterMap
									.put("screening_Adjustment", l.getQty());
						}
						if (l.getProduct_name().equalsIgnoreCase("Unigold")) {
							parameterMap.put("confirmatory_Adjustment", l
									.getQty());
						}
						if (l.getProduct_name().equalsIgnoreCase("SD Bioline")) {
							parameterMap.put("tiebreaker_Adjustment", l
									.getQty());
						}

					}

					for (HivTestPhysicalCount b : beginingBalanceperdayList) {
						if (b.getProduct_code().equalsIgnoreCase("HTK0006")) {
							parameterMap.put("begin_bal_screening", b
									.getQty_count());
						}
						if (b.getProduct_code().equalsIgnoreCase("HTK0001")) {
							parameterMap.put("begin_bal_confirmatory", b
									.getQty_count());
						}
						if (b.getProduct_code().equalsIgnoreCase("HTK0005")) {
							parameterMap.put("begin_bal_tiebreaker", b
									.getQty_count());
						}

					}
					// updated by Moses kausa. 2014

					// product physical count
					for (HivTestPhysicalCount p : physicalCountperdayList) {
						if (p.getProduct_code().equalsIgnoreCase("HTK0006")) {
							parameterMap.put("screening_physicalcount", p
									.getQty_count());
						}
						if (p.getProduct_code().equalsIgnoreCase("HTK0001")) {
							parameterMap.put("confirmatory_physicalcount", p
									.getQty_count());
						}
						if (p.getProduct_code().equalsIgnoreCase("HTK0005")) {
							parameterMap.put("tiebreaker_physicalcount", p
									.getQty_count());
						}

					}

					if (products.getType_of_test()
							.equalsIgnoreCase("Screening")) {
						parameterMap.put("screening_product", products
								.getDar_display_name());

						//for mybatis query in StockControlCardMapper.xml
						parameterMap.put("screening_product_para", products
								.getProduct_code());

					} else if (products.getType_of_test().equalsIgnoreCase(
							"Confirmatory")) {
						parameterMap.put("confirmatory_product", products
								.getDar_display_name());

						//for mybatis query in StockControlCardMapper.xml
						parameterMap.put("confirmatory_product_para", products
								.getProduct_code());

					} else if (products.getType_of_test().equalsIgnoreCase(
							"Tiebreaker")) {
						parameterMap.put("tiebreaker_product", products
								.getDar_display_name());

						//for mybatis query in StockControlCardMapper.xml
						parameterMap.put("tiebreaker_product_para", products
								.getProduct_code());

					}

					//for mybatis query in StockControlCardMapper.xml
					parameterMap.put("createddate_para", publicMethods.toDay());

					//pass to jasper reports for beginning balance parameter  

					/*if (products.getProduct_code().equalsIgnoreCase(
							physicalCount.getProduct_code())
							&& products.getType_of_test().equalsIgnoreCase(
									"Screening")) {

						parameterMap.put("begin_bal_screening", physicalCount
								.getQty_count());
						System.out.println(physicalCount.getProduct_code()
								+ "  " + physicalCount.getQty_count());

					} else if (products.getProduct_code().equalsIgnoreCase(
							physicalCount.getProduct_code())
							&& products.getType_of_test().equalsIgnoreCase(
									"Confirmatory")) {

						parameterMap.put("begin_bal_confirmatory",
								physicalCount.getQty_count());
						System.out.println(physicalCount.getProduct_code()
								+ "  " + physicalCount.getQty_count());

					} else if (products.getProduct_code().equalsIgnoreCase(
							physicalCount.getProduct_code())
							&& products.getType_of_test().equalsIgnoreCase(
									"Tiebreaker")) {

						parameterMap.put("begin_bal_tiebreaker", physicalCount
								.getQty_count());
						System.out.println(physicalCount.getProduct_code()
								+ "  " + physicalCount.getQty_count());

					}*/

				}

			}

			//pass a number parameters to query 
			factory = new MyBatisConnectionFactory().getSqlSessionFactory();

			session = factory.openSession();

			try {
				this.sccList = session.selectList(
						"selectHivTestsProductIssued", parameterMap);
			} finally {

				session.close();
			}

			//for (HivTestProducts products : this.HivTestProductsList) {
			for (StockControlCard scc : this.sccList) {

				if (scc.getProductcode().equalsIgnoreCase("HTK0006")) {
					if (scc.getQty_isssued() != 0) {
						parameterMap.put("quantity_received_screening", scc
								.getQty_isssued());
					} else {
						parameterMap.put("quantity_received_screening", 0);
					}

				} else if (scc.getProductcode().equalsIgnoreCase("HTK0001")) {
					if (scc.getQty_isssued() != 0) {
						parameterMap.put("quantity_received_confirmatory", scc
								.getQty_isssued());
					} else {
						parameterMap.put("quantity_received_confirmatory", 0);
					}

				} else if (scc.getProductcode().equalsIgnoreCase("HTK0005")) {
					if (scc.getQty_isssued() != 0) {
						parameterMap.put("quantity_received_tiebreaker", scc
								.getQty_isssued());
					} else {
						parameterMap.put("quantity_received_tiebreaker", 0);
					}

				}

				//	System.out.println("QTY " + scc.getQty_isssued()
				//	+ "  Code " + scc.getProductcode());

			}
			//}

			///parameterMap = new HashMap();

			parameterMap.put("district", System.getProperty("district"));
			parameterMap
					.put("facilityname", System.getProperty("facilityname"));
			//parameterMap.put("confirmatory_product", "Unigold");
			//parameterMap.put("tiebreaker_product", "Determine");

			parameterMap.put("location", System.getProperty("dp_name"));

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport(
					"Reports/hiv_tests_dar.jasper", parameterMap,
					new JRBeanCollectionDataSource(this.hivTestList));

			// JasperExportManager.exportReportToPdfFile(print,"reports//test.pdf");
			// new RenderPdf().renderPdf("/reports//test.pdf");
			new JasperViewer(print);
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(this, e.getMessage()
					.toString());
		}

		AppJFrame.glassPane.deactivate();

	}

	private void hivTestingJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		if (!System.getProperty("dp_name").toString().trim().equals("")
				|| !System.getProperty("dp_name").isEmpty()) {
			try{
				AppJFrame.glassPane.activate(null);
				
				new RecordTestJD(
						javax.swing.JOptionPane.getFrameForComponent(this), true);
			}
			finally{
				AppJFrame.glassPane.deactivate();
			}

		} else {
			// set the location
			try{
				AppJFrame.glassPane.activate(null);
				HivTestResultDao hivTestResultDao = new HivTestResultDao();
				
				new RecordTestJD(
						javax.swing.JOptionPane.getFrameForComponent(this), true);
			}
			finally{
				AppJFrame.glassPane.deactivate();
			}
		}

	}

	/********* Action Listener Coding Starts *************/
	// this action Listener for phone book
	//
	public void actionPerformed(ActionEvent evt) {

		// if (evt.getActionCommand().equalsIgnoreCase("a")) {

		// 888888888888888 use this for getting what letter was clicked and
		// getting the letter for the query
		// javax.swing.JOptionPane
		// .showMessageDialog(
		// null,
		// "Sorry this feature is not available for LegalManager Basic Edition \nUpgrade to activate this feature");

		// javax.swing.JOptionPane.showMessageDialog(null,
		// evt.getActionCommand()
		// .toString());
		// }
		AppJFrame.glassPane.activate(null);
		// new
		// ContactAdminJD(javax.swing.JOptionPane.getFrameForComponent(this),
		// true, evt.getActionCommand().toString());
		AppJFrame.glassPane.deactivate();

		// if (evt.getActionCommand().equalsIgnoreCase("b")) {
		// javax.swing.JOptionPane.showMessageDialog(null, "b");
		// }
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel hivTestingJL;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel editTransactionLbl;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JLabel losses_adjustmentsJL;
	private javax.swing.JLabel physicalCountJL;
	private javax.swing.JLabel viewDARJL;
	private javax.swing.JLabel viewDARJL1;
	private javax.swing.JLabel viewDARJL2;
	private javax.swing.JLabel viewPrevDARSJL;
	private javax.swing.JLabel viewPrevDARSJL1;
	// End of variables declaration//GEN-END:variables

}