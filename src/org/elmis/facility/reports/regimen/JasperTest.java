package org.elmis.facility.reports.regimen;
import java.util.Date;
import java.util.HashMap;

import javax.swing.JFrame;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;
public class JasperTest  
{  
  
    public static void main(String[] args)  
    {  
        String mainReport = "/home/mesay/Documents/Projects/JasperReports/ReportJars/RegimenReport.jasper";
        String subReport = "/home/mesay/Documents/Projects/JasperReports/ReportJars/RegimenReport_subreport1.jasper";
//        String outFileName = "/home/mesay/Documents/Projects/JasperReports/test.pdf";  
        HashMap<String, Object> parameters = new HashMap<String, Object>(); 
        RegimenReportData reportData = new RegimenReportData();
        reportData.populateRegimenReport(new Date(), new Date());
		JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(
				reportData.getRegimenLines());
//		parameters.put("SUB_DATA_SOURCE", reportData.getRegimenLines().get(0).getRegimenReportBeans());
		parameters.put("SUB_REPORT_PATH",subReport);
	//	parameters.put("SUBREPORT_DIR", "/home/mesay/Documents/Projects/JasperReports/ReportJars/");
        try  
        {  
            JasperPrint print = JasperFillManager.fillReport(mainReport,parameters,ds );  
//            JRExporter exporter =new net.sf.jasperreports.engine.export.JRPdfExporter();  
//            exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,outFileName);  
	//            exporter.setParameter(JRExporterParameter.JASPER_PRINT,print);  
	//            exporter.exportReport();  
//	            System.out.println("Created file: " + outFileName);  
            JFrame frame = new JFrame("Regimen Report");
            frame.getContentPane().add(new JRViewer(print));
            frame.pack();
            frame.setExtendedState(java.awt.Frame.MAXIMIZED_BOTH);
            frame.setVisible(true);
        }  
        catch (JRException e)  
        {  
            e.printStackTrace();  
            System.exit(1);  
        }  
        catch (Exception e)  
        {  
        e.printStackTrace();  
        System.exit(1);  
        } 
    }
}