/*
 * NumberPadJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Products;
import org.elmis.forms.stores.issuing.IssuingJD;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import java.awt.Button;

/**
 *
 * @author  __USER__
 */
//End of variables declaration//GEN-END:variables

	/***************************************************
	 * //start of child class
	 * ***********************************************/

public class arvNumberPadJD extends javax.swing.JDialog {

	//private static int numberOfItems = 0;
	//private static String barcodeIn;

	private List<Character> charList = new LinkedList();
	private char c;
	private String ARTnumberstr = "";
	private Products product;
	private String Numberpadcaller;
	//	private ProductQty qty;
	public static String fillregister = null;
	public static String fillregisternrc;
	public String fillsubstr;
	public String filldistsubstr;
	public String fillcitizensubsstr;
	public ARTNumberJP artnumberjp;
	public Boolean usekeyboard = false;

	/** Creates new form NumberPadJD */
	/** Creates new form NumberPadJD */
	public arvNumberPadJD(java.awt.Frame parent, boolean modal, String src) {
		super(parent, modal);
		initComponents();

		Numberpadcaller = src;
		//	this.qtyJTF.setText(numberOfItems + "");

		//this.qtyJTF.setText(qty.getQty() + "");

		this.setSize(320,490);
		this.setLocationRelativeTo(null);

		this.getRootPane().setDefaultButton(okJBtn);
		this.qtyJTF.setFocusable(false);
		/*this.subtractJBtn.setFocusable(false);
		this.deleteJBtn.setFocusable(false);
		this.jButton1.setFocusable(false);
		this.jButton2.setFocusable(false);
		this.jButton3.setFocusable(false);
		this.jButton4.setFocusable(false);
		this.jButton5.setFocusable(false);
		this.jButton6.setFocusable(false);
		this.jButton7.setFocusable(false);
		this.jButton8.setFocusable(false);
		this.jButton9.setFocusable(false);
		this.jButton10.setFocusable(false);*/
		//this.jButton1.setFocusable(false);

		this.setVisible(true);

	}

	/**
	 * @wbp.parser.constructor
	 */
	public arvNumberPadJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();
		//this.barcodeIn = IssuingJD.barcodeScanned;

		/*	if (!this.product.getPrimaryname().equals("")) {

				//if the name is too long cut it short for display purpose
				if (this.product.getPrimaryname().length() >= 40) {

					productNameJL.setText(this.product.getPrimaryname().substring(
							0, 40));
				} else {

					productNameJL.setText(this.product.getPrimaryname());
				}
			}

			this.qtyInStockJL.setText(this.qty.getQty() + "");*/
		//	this.qtyJTF.setText(numberOfItems + "");

		//this.qtyJTF.setText(qty.getQty() + "");
		this.setSize(332, 647);
		this.setLocationRelativeTo(null);

		this.getRootPane().setDefaultButton(okJBtn);
		this.qtyJTF.setFocusable(false);
		/*this.subtractJBtn.setFocusable(false);
		this.deleteJBtn.setFocusable(false);
		this.jButton1.setFocusable(false);
		this.jButton2.setFocusable(false);
		this.jButton3.setFocusable(false);
		this.jButton4.setFocusable(false);
		this.jButton5.setFocusable(false);
		this.jButton6.setFocusable(false);
		this.jButton7.setFocusable(false);
		this.jButton8.setFocusable(false);
		this.jButton9.setFocusable(false);
		this.jButton10.setFocusable(false);*/
		//this.jButton1.setFocusable(false);

		this.setVisible(true);

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jPanel2 = new javax.swing.JPanel();
		jButton1 = new javax.swing.JButton();
		jButton2 = new javax.swing.JButton();
		jButton3 = new javax.swing.JButton();
		jButton4 = new javax.swing.JButton();
		jButton5 = new javax.swing.JButton();
		jButton6 = new javax.swing.JButton();
		jButton7 = new javax.swing.JButton();
		jButton8 = new javax.swing.JButton();
		jButton9 = new javax.swing.JButton();
		jButton10 = new javax.swing.JButton();
		okJBtn = new javax.swing.JButton();
		deleteJBtn = new javax.swing.JButton();
		subtractJBtn = new javax.swing.JButton();
		addJBtn = new javax.swing.JButton();
		qtyJTF = new javax.swing.JTextField();
	    btnusekeyboard =  new javax.swing.JButton();
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(153, 153, 153));
		jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(
				javax.swing.border.BevelBorder.RAISED));

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));
		jPanel2.setBorder(javax.swing.BorderFactory
				.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

		jButton1.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton1.setText("1");
		jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton1MouseClicked(evt);
			}
		});
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		jButton2.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton2.setText("2");
		jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton2MouseClicked(evt);
			}
		});
		jButton2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton2ActionPerformed(evt);
			}
		});

		jButton3.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton3.setText("3");
		jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton3MouseClicked(evt);
			}
		});
		jButton3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton3ActionPerformed(evt);
			}
		});

		jButton4.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton4.setText("4");
		jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton4MouseClicked(evt);
			}
		});
		jButton4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton4ActionPerformed(evt);
			}
		});

		jButton5.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton5.setText("5");
		jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton5MouseClicked(evt);
			}
		});
		jButton5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton5ActionPerformed(evt);
			}
		});

		jButton6.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton6.setText("6");
		jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton6MouseClicked(evt);
			}
		});
		jButton6.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton6ActionPerformed(evt);
			}
		});

		jButton7.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton7.setText("7");
		jButton7.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton7MouseClicked(evt);
			}
		});
		jButton7.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton7ActionPerformed(evt);
			}
		});

		jButton8.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton8.setText("8");
		jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton8MouseClicked(evt);
			}
		});
		jButton8.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton8ActionPerformed(evt);
			}
		});

		jButton9.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton9.setText("9");
		jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton9MouseClicked(evt);
			}
		});
		jButton9.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton9ActionPerformed(evt);
			}
		});

		jButton10.setFont(new java.awt.Font("Ebrima", 0, 24));
		jButton10.setText("0");
		jButton10.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton10MouseClicked(evt);
			}
		});
		jButton10.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton10ActionPerformed(evt);
			}
		});

		okJBtn.setBackground(new java.awt.Color(0, 153, 0));
		okJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
		okJBtn.setForeground(new java.awt.Color(153, 0, 0));
		okJBtn.setText("OK");
		okJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				okJBtnActionPerformed(evt);
			}
		});
		okJBtn.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusLost(java.awt.event.FocusEvent evt) {
				okJBtnFocusLost(evt);
			}
		});
		deleteJBtn.setFont(new java.awt.Font("Ebrima", 0, 18));
		deleteJBtn.setText("DEL");
		deleteJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				deleteJBtnActionPerformed(evt);
			}
		});

		subtractJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
		subtractJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/sort_descending.png"))); // NOI18N
		subtractJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				subtractJBtnActionPerformed(evt);
			}
		});
		//JButton btnusekeyboard = new JButton("use keyboard");
		btnusekeyboard.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent arg0) {
				//set form variable
				
			}
		});
		btnusekeyboard.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				btnusekeyboardActionPerformed(evt);
			}
		});
		

		addJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
		addJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/sort_ascending.png"))); // NOI18N
		addJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				addJBtnActionPerformed(evt);
			}
		});
		addJBtn.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				addJBtnKeyTyped(evt);
			}
		});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2.setLayout(jPanel2Layout);
		jPanel2Layout
				.setHorizontalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(
																jButton1,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																jButton4,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																jButton7,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																subtractJBtn,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE)
														.addComponent(
																deleteJBtn,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																70,
																Short.MAX_VALUE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																addJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																146,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addGroup(
																jPanel2Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel2Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jButton10,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								70,
																								Short.MAX_VALUE)
																						.addComponent(
																								jButton8,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								67,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton5,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								67,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton2,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								67,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel2Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								okJBtn,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton9,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton6,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jButton3,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								70,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
										.addContainerGap()));

		jPanel2Layout
				.linkSize(javax.swing.SwingConstants.HORIZONTAL,
						new java.awt.Component[] { deleteJBtn, jButton1,
								jButton10, jButton2, jButton3, jButton4,
								jButton5, jButton6, jButton7, jButton8,
								jButton9, okJBtn, subtractJBtn });

		jPanel2Layout
				.setVerticalGroup(jPanel2Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel2Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																addJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																subtractJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jButton3,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton2,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jButton6,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton5,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton4,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jButton9,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton8,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton7,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel2Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																okJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																55,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jButton10,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																58,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																deleteJBtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																59,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jPanel2Layout
				.linkSize(javax.swing.SwingConstants.VERTICAL,
						new java.awt.Component[] { deleteJBtn, jButton1,
								jButton10, jButton2, jButton3, jButton4,
								jButton5, jButton6, jButton7, jButton8,
								jButton9, okJBtn, subtractJBtn });

		qtyJTF.setFont(new java.awt.Font("Ebrima", 0, 14));
		qtyJTF.addFocusListener(new java.awt.event.FocusAdapter() {

			public void focusLost(java.awt.event.FocusEvent evt) {
				qtyJTFFocusLost(evt);
			}
		});
		qtyJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				qtyJTFKeyTyped(evt);
			}
		});
		
		usekeyboardJbtn = new JButton("Use keyboard");
		usekeyboardJbtn.setIcon(new ImageIcon(arvNumberPadJD.class.getResource("/elmis_images/Repeat.png")));
		usekeyboardJbtn.addMouseListener(new MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					usekeyboardJbtnMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(jPanel2, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
						.addComponent(qtyJTF, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE)
						.addComponent(usekeyboardJbtn, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(qtyJTF, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(usekeyboardJbtn, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
					.addGap(384))
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(27, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 574, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(301, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>

	private void usekeyboardJbtnMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		System.out.println("close the form now");
		usekeyboard = true;
		System.out.println(usekeyboard + "~~~");
		this.dispose();
	}

	//GEN-END:initComponents

	private void jButton10MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton10.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}
	private void jButton9MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton9.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton8MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton8.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton7MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton7.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton6.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton5MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton5.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton4.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton3.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton2.getText();
		this.qtyJTF.setText(this.ARTnumberstr);
	}

	private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("One button clicked ??");
		this.ARTnumberstr = this.ARTnumberstr + "" + jButton1.getText();
		this.qtyJTF.setText(this.ARTnumberstr);

	}

	private void addJBtnKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		c = evt.getKeyChar();

		charList.add(c);

	}

	private void okJBtnFocusLost(java.awt.event.FocusEvent evt) {
		// TODO add your handling code here:
		okJBtn.requestFocus();
	}

	private void addJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void subtractJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		if (!qtyJTF.getText().equals("")) {

			qtyJTF.setText(qtyJTF.getText().substring(0,
					qtyJTF.getText().length() - 1));

		}
	}
	
	private void btnusekeyboardActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		
		usekeyboard = true;
		//System.out.println("close the child form");
		//System.out.println("Test Close form");
		//arvNumberPadJD.this.dispose();
     
	}

	private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButton18A7ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButton1n15ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButtoton13ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void jButleteJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void okJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//check source of call
    System.out.println("Check OK button !");
		if (this.Numberpadcaller.equals("ARTNUMBER")) {

			fillregister = this.ARTnumberstr;

		} else if (this.Numberpadcaller.equals("NRCNUMBER")) {
			@SuppressWarnings("unused")
			String split = "/";
			this.fillregister = this.ARTnumberstr;
			fillsubstr = this.fillregister.substring(0, 6);
			filldistsubstr = this.fillregister.substring(6, 8);
			fillcitizensubsstr = this.fillregister.substring(8, 9);
			this.fillregisternrc = fillsubstr + split + filldistsubstr
					+ split + fillcitizensubsstr;

		}
		//arvNumberPadJD.this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		arvNumberPadJD.this.dispose();

	}

	public String getData() {

		return qtyJTF.getText();

	}

	private void qtyJTFFocusLost(java.awt.event.FocusEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.requestFocusInWindow();
	}

	private void deleteJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		if (!qtyJTF.getText().equals("")) {

			qtyJTF.setText("");
			this.ARTnumberstr = "";

		}
	}

	private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "0");

		getStockLimit(0);

	}

	private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "9");
		getStockLimit(9);

	}

	private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "8");
		getStockLimit(8);
	}

	private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "7");

		getStockLimit(7);
	}

	private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "6");
		getStockLimit(6);
	}

	private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "5");
		getStockLimit(5);
	}

	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "4");
		getStockLimit(4);
	}

	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "3");
		getStockLimit(3);
	}

	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "2");
		getStockLimit(2);
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

		// TODO add your handling code here:
		//qtyJTF.setText(qtyJTF.getText() + "1");

		//getStockLimit(1);

		this.qtyJTF.setFocusable(true);

	}

	private void qtyJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		final char c = evt.getKeyChar();
		if (!(Character.isDigit(c) || (c == KeyEvent.VK_PERIOD)
				|| (c == KeyEvent.VK_COMMA)
				|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
			this.getToolkit().beep();
			evt.consume();
		}
	}

	private void getStockLimit(int qtyToAdd) {

		//int qtyInStock = Integer.parseInt(qtyInStockJL.getText());
		//int requestedQty = Integer.parseInt(qtyJTF.getText() + qtyToAdd);

		/*if (requestedQty > qtyInStock) {

			javax.swing.JOptionPane.showMessageDialog(this, "Over limit ");
		} else if (requestedQty <= qtyInStock) {

			//javax.swing.JOptionPane.showMessageDialog(this, "Qty requested "
			//		+ requestedQty + "\nStock " + qtyInStock + "\nQty is JTF "
			//		+ qtyJTF.getText());

			qtyJTF.setText(qtyJTF.getText() + qtyToAdd);
		}*/

		//requestedQty = 0;
		//return overLimit;

	
	}
	/**
	 * @param args the command line arguments
	 */
	/*	public static void main(String args[]) {
			java.awt.EventQueue.invokeLater(new Runnable() {
				public void run() {
					arvNumberPadJD dialog = new arvNumberPadJD(
							new javax.swing.JFrame(), true);
					dialog.addWindowListener(new java.awt.event.WindowAdapter() {
						public void windowClosing(java.awt.event.WindowEvent e) {
							System.exit(0);
						}
					});
					dialog.setVisible(true);
				}
			});
		}*/

	

		//GEN-BEGIN:variables
		// Variables declaration - do not modify
		private javax.swing.JButton addJBtn;
		private javax.swing.JButton deleteJBtn;
		private javax.swing.JButton jButton1;
		private javax.swing.JButton jButton10;
		private javax.swing.JButton jButton2;
		private javax.swing.JButton jButton3;
		private javax.swing.JButton jButton4;
		private javax.swing.JButton jButton5;
		private javax.swing.JButton jButton6;
		private javax.swing.JButton jButton7;
		private javax.swing.JButton jButton8;
		private javax.swing.JButton jButton9;
		private javax.swing.JPanel jPanel1;
		private javax.swing.JPanel jPanel2;
		private javax.swing.JButton okJBtn;
		private javax.swing.JTextField qtyJTF;
		private javax.swing.JButton subtractJBtn;
		private javax.swing.JButton btnusekeyboard;
		private JButton usekeyboardJbtn;
		// End of variables declaration//GEN-END:variables

	}