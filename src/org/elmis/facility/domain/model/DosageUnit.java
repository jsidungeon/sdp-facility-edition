package org.elmis.facility.domain.model;

public class DosageUnit {
	private Integer id;
	private Integer prductRegimenId;
	private String code;
	private String name;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getPrductRegimenId() {
		return prductRegimenId;
	}
	public void setPrductRegimenId(Integer prductRegimenId) {
		this.prductRegimenId = prductRegimenId;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
