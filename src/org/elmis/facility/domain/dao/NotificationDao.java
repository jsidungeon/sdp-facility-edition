package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.Notification;

public class NotificationDao {
	
	private SqlSessionFactory sqlSessionFactory;
	
	//public NotificationDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<Notification> getAllNotifications()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			return session.selectList("Notification.getAllMessages");
		} finally {
			session.close();
		}
	}
	
	public synchronized List<Notification> getNewNotifications()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Notification> list = session.selectList("Notification.getNewMessages");
			return list;
		} finally {
			session.close();
		}
	}
	
	public void deleteNotification(int id)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.delete("Notification.deleteNotification", id);
			session.commit();
		} finally {
			session.close();
		}
	}
	public void addNotification(Notification n)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.insert("Notification.insertNotification", n);
			session.commit();
		} finally {
			session.close();
		}
	}
}
