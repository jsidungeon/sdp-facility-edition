/*
 * ProgramsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.JDialog;
import javax.swing.SwingUtilities;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.reports.GenerateJasperReports;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.TableModel;

/**
 * 
 * @author __USER__
 */
public class ProductsJP extends javax.swing.JPanel {

	// Create tableModel for Program Products for each facility
	private static final String[] columns_products = { "Product Code",
			"Product name", "id", "packsize","Strength" };
	private static final Object[] defaultv_products = { "", "", "", "" ,""};
	private static final int rows_products = 0;
	public static TableModel tableModel_products = new TableModel(
			columns_products, defaultv_products, rows_products);

	private static VW_Program_Facility_ApprovedProducts products;
	private static ListIterator<VW_Program_Facility_ApprovedProducts> productsIterator;

	private List<Products> productsList = new LinkedList();
	private int selectedProgramID;
	private JasperPrint print;
	private static Map parameterMap = new HashMap();

    private String PackSize = "";
    FacilitySetUpDAO callfacility;
    FacilityProductsDAO Facilityproductsmapper = null;
    ARVDispensingDAO arvproductcaller = null;
    Facility facility = null;
    Facility_Types facilitytype = null;
    public String facilityprogramcode;
    public String typeCode;
    List<VW_Program_Facility_ApprovedProducts> adjustmensproductsList = new LinkedList();
    private String 	 PrimaryName = "";
    private String selectedProductCode = "";
	/** Creates new form ProgramsJP */
	@SuppressWarnings("static-access")
	public ProductsJP() {

		this.selectedProgramID = selectedProgramID;
		initComponents();
		this.setFocusable(false);
		this.productsJTable.setFocusable(false);// remove from focus cycle

		this.productsJTable.getColumnModel().getColumn(0).setMinWidth(120);
		this.productsJTable.getColumnModel().getColumn(0).setMaxWidth(120);
		this.productsJTable.getColumnModel().getColumn(2).setMinWidth(0);
		this.productsJTable.getColumnModel().getColumn(2).setMaxWidth(0);
		this.productsJTable.getColumnModel().getColumn(3).setMinWidth(0);
		this.productsJTable.getColumnModel().getColumn(3).setMaxWidth(0);
		this.productsJTable.getColumnModel().getColumn(4).setMinWidth(0);
	    this.productsJTable.getColumnModel().getColumn(4).setMaxWidth(0);

	

		try {
				// if a program area is selected
				
					callfacility = new FacilitySetUpDAO();
					Facilityproductsmapper = new FacilityProductsDAO();

					facility = callfacility.getFacility();
					System.out.println(facility.getCode()
							+ "We have the facility code");
					facilitytype = callfacility.selectAllwithTypes(facility);
					typeCode = facilitytype.getCode();

				    this.facilityprogramcode = "ARV";// ProgramsJP.selectedprogramCode;
					String dbdriverStatus = System.getProperty("dbdriver");
					if (!(facilityprogramcode == null)) {
						//reset program code

						if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
							adjustmensproductsList = Facilityproductsmapper
									.dogetcurrentFacilityApprovedProducts(
											facilityprogramcode, typeCode);
							
							this.populateProductsTable(this.adjustmensproductsList);
						} else {
							adjustmensproductsList = Facilityproductsmapper
									.dogetcurrentFacilityApprovedProductshsqldb(
											facilityprogramcode, typeCode);
							
							this.populateProductsTable(this.adjustmensproductsList);
						}
					}

			
			

		}catch(NullPointerException e){
		   e.getMessage();
		}
	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		productsJTable = new javax.swing.JTable();

		productsJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		productsJTable.setModel(tableModel_products);
		productsJTable.setRowHeight(60);
		productsJTable.setTableHeader(null);
		productsJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				productsJTableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(productsJTable);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE));
	}// </editor-fold>

	// GEN-END:initComponents

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void productsJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
       
		arvproductcaller = new  ARVDispensingDAO();
		
		
		try{
		int selectedProductID = Integer
			.parseInt(this.tableModel_products.getValueAt(
						this.productsJTable.getSelectedRow(), 2).toString());
		
		 selectedProductCode = this.tableModel_products.getValueAt(
				this.productsJTable.getSelectedRow(), 0).toString();
		
		 PrimaryName = this.tableModel_products.getValueAt(
				this.productsJTable.getSelectedRow(), 1).toString();
		
			PackSize = this.tableModel_products.getValueAt(
				this.productsJTable.getSelectedRow(), 4).toString();
			
		}catch(java.lang.NullPointerException n){
			n.getMessage();
		}


		products = new VW_Program_Facility_ApprovedProducts();
       
		Integer dpid = AppJFrame.getDispensingId("ARV");
       
		@SuppressWarnings("rawtypes")
		HashMap map = new HashMap();
		
		map.put("prodcode",selectedProductCode);
		map.put("dpid", dpid);
		@SuppressWarnings({ "unused", "rawtypes" })
		List arvdispAdjustPCList  = arvproductcaller .SelectARVdispensaryproductsAdjustPhysicalcountbalances(map);
		
		for(int i = 0; i < arvdispAdjustPCList.size(); i++){
			System.out.println(arvdispAdjustPCList.toString());
		}
		
		parameterMap.put("primaryname", PrimaryName);
		parameterMap.put("packsize", PackSize);
		parameterMap.put("productcode", selectedProductCode);
		parameterMap.put("dispenserInitial", AppJFrame.userLoggedIn);
		parameterMap.put("facilityname", System.getProperty("facilityname"));
		parameterMap.put("district", System.getProperty("district"));
		parameterMap.put("dispensaryname", AppJFrame.getDispensingPointName("ARV"));
		
		//new GenerateJasperReports().createStockControlCardbycode(selectedProductCode,PrimaryName,PackSize);
	
		
		AppJFrame.glassPane.activate(null);
		try {
			print = JasperFillManager.fillReport("Reports/ARVdispensarystockcontrolcard.jasper",
					parameterMap, new JRBeanCollectionDataSource(arvdispAdjustPCList));
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
		JRViewer jrViewer =  new JRViewer(print);
		showReport(jrViewer);
		
		
		
		Window w = SwingUtilities.getWindowAncestor(ProductsJP.this);
		w.setVisible(false);
	}

	
	public void showReport ( JRViewer jv) {

		JDialog jf = new JDialog();
		jf.setTitle("Report");
		jf.setModal(true);
		jv.setPreferredSize(new Dimension(1000, 600));
		jf.getContentPane().add(jv);
	
		 jf.validate();
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		jf.pack();
		jf.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - getHeight()/2);
		
		jf.setVisible(true); 
		}


	public static void populateProductsTable(List progProducts) {

		tableModel_products.clearTable();
		productsIterator = progProducts.listIterator();

		while (productsIterator.hasNext()) {

			products = productsIterator.next();

			defaultv_products[0] = products.getCode();
			defaultv_products[1] = products.getPrimaryname();

			defaultv_products[2] = products.getId();

			defaultv_products[3] = products.getPacksize();
		     
			defaultv_products[4] = products.getStrength();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_products.length; j++) {
				cols.add(defaultv_products[j]);

			}

			tableModel_products.insertRow(cols);

			productsIterator.remove();
		}
	}

	private void createStockControlCardX(int selectedProductID) {

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();
		StockControlCard scc = new StockControlCard();
		List sccList = new ArrayList();
		try {

			// scc
			sccList = session.selectList("selectBySCCByProductID",
					selectedProductID);// ("getProgramesSupported");

			// System.out.println(this.products.getPrimaryname()
			// +" "+IssuingJD.selectedProductID);

			// this.populateProgramsTable(this.programsList);

		} finally {
			session.close();
		}

		// TODO add your handling code here:

		// new Items_issue().createBeanCollection()
		try {

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport(
					"Reports/stockcontrolcard.jasper", parameterMap,
					new JRBeanCollectionDataSource(sccList));

			new JasperViewer(print);
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}

	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable productsJTable;
	// End of variables declaration//GEN-END:variables

}