/*
 * ProgramsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest.physicalcount;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.JOptionPane;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.AdjustmentDao;
import org.elmis.facility.domain.model.HivTestProducts;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.TableModel;

/**
 * 
 * @author Joe Banda
 */
public class ProductsJP extends javax.swing.JPanel {

	// Create tableModel for Program Products for each facility
	private static final String[] columns_products = { "Product Code","Product name","QTY" ,"id" };
	private static final Object[] defaultv_products = { "","","", "" };
	private static final int rows_products = 0;
	private Map<String, Integer> productBalanceMap;
	public static TableModel tableModel_products = new TableModel(
			columns_products, defaultv_products, rows_products);

	//private static Products products;
	//private static ListIterator<Products> productsIterator;
	
	private HivTestProducts hivTestProducts; 
	private ListIterator<HivTestProducts> hivTestProductsIterator;
	private List <HivTestProducts> hivTestProductsList = new LinkedList();
	
	//private  List<Products> productsList = new LinkedList();
	private int selectedProgramID;
	
	
	private static Map parameterMap = new HashMap();
	private JasperPrint print;

	/** Creates new form ProgramsJP */
	public ProductsJP(int selectedProgramID, int siteId) {
		productBalanceMap = new AdjustmentDao().getHivProductBalances(siteId);
		this.selectedProgramID = selectedProgramID;
		initComponents();
		this.setFocusable(false);
		this.productsJTable.setFocusable(false);// remove from focus cycle
		
		
		this.productsJTable.getColumnModel().getColumn(0).setMinWidth(120);
		this.productsJTable.getColumnModel().getColumn(0).setMaxWidth(120);
		
		this.productsJTable.getColumnModel().getColumn(2).setMinWidth(120);
		this.productsJTable.getColumnModel().getColumn(2).setMaxWidth(120);
		
		this.productsJTable.getColumnModel().getColumn(3).setMinWidth(0);
		this.productsJTable.getColumnModel().getColumn(3).setMaxWidth(0);

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			try {
				this.hivTestProductsList = session.selectList("selectHIVTestProducts");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				AppJFrame.glassPane.deactivate();
			}
			this.populateProductsTable(hivTestProductsList);

		} finally {
			session.close();
		}
	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		productsJTable = new javax.swing.JTable();

		productsJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		productsJTable.setModel(tableModel_products);
		productsJTable.setRowHeight(60);
		productsJTable.setTableHeader(null);
		productsJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				productsJTableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(productsJTable);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE));
	}// </editor-fold>

	// GEN-END:initComponents

	private void productsJTableMouseClicked(java.awt.event.MouseEvent evt) {

		String selectedProductCode = tableModel_products.getValueAt(productsJTable.getSelectedRow(), 0).toString();
		int productBalance = productBalanceMap.get(selectedProductCode);

		String qty =	new NumberPadJD(javax.swing.JOptionPane.getFrameForComponent(this),	true).getQty();

		int physicalCount = Integer.parseInt(qty);

		if (physicalCount > productBalance){
			//new MessageDialog().showDialog(this, selectedProductCode+" Physical Count("+physicalCount+") you have entered is greater than the System calculated balance("+productBalance+"). \nPlease carry out the necessary adjustment first.", "Cancelled","Ok");
			JOptionPane.showMessageDialog(this,
					"<html> <font color=red>"+selectedProductCode+"</font> Physical Count <font color=red>("+physicalCount+")</font> you have entered is <font color=red>greater</font> than the System calculated balance <font color=red>("+productBalance+")</font>. <br>Please carry out the necessary adjustment first.</html>",	selectedProductCode+" Physical Count - Calculated Balance Anomaly",JOptionPane.WARNING_MESSAGE);
		}
		else if(physicalCount < productBalance){
			//new MessageDialog().showDialog(this, selectedProductCode+" Physical Count("+physicalCount+") you have entered is less than the System calculated balance("+productBalance+"). \nPlease carry out the necessary adjustment first.", "Cancelled","Ok");
			JOptionPane.showMessageDialog(this,
					"<html> <font color=red>"+selectedProductCode+"</font> Physical Count <font color=red>("+physicalCount+")</font> you have entered is <font color=red>less</font> than the System calculated balance <font color=red>("+productBalance+")</font>. <br>Please carry out the necessary adjustment first.</html>",	selectedProductCode+" Physical Count - Calculated Balance Anomaly",JOptionPane.WARNING_MESSAGE);
		}
		else{
			this.tableModel_products.setValueAt(qty, this.productsJTable.getSelectedRow(),2);
			this.tableModel_products.fireTableDataChanged();
		}
	}

	public  void populateProductsTable(List progProducts) {

		tableModel_products.clearTable();
		hivTestProductsIterator = progProducts.listIterator();

		while (hivTestProductsIterator.hasNext()) {

			hivTestProducts = hivTestProductsIterator.next();
			
			defaultv_products[0] = hivTestProducts.getProduct_code();// products.getCode();
			defaultv_products[1] = hivTestProducts.getDar_display_name();//products.getPrimaryname();

			defaultv_products[3] = hivTestProducts.getId();//products.getId();

			// defaultv_prgramproducts[5] = products.g

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_products.length; j++) {
				cols.add(defaultv_products[j]);

			}

			tableModel_products.insertRow(cols);

			hivTestProductsIterator.remove();
		}
	}
	
	
	private void createStockControlCardX(int selectedProductID){
		
		SqlSessionFactory factory = new MyBatisConnectionFactory().getSqlSessionFactory();

		SqlSession session = factory.openSession();
		StockControlCard	scc = new StockControlCard();
		List sccList = new ArrayList();
		try {
			//scc
			sccList = session.selectList("selectBySCCByProductID",selectedProductID);//("getProgramesSupported");
		} finally {
			session.close();
		}

		// new Items_issue().createBeanCollection()
		try {

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport("Reports/stockcontrolcard.jasper",
					parameterMap, new JRBeanCollectionDataSource(sccList));

			new JasperViewer(print);
			JasperViewer.viewReport(print, false);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}
	
		
		
		
	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable productsJTable;
	// End of variables declaration//GEN-END:variables

}