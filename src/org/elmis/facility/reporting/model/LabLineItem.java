package org.elmis.facility.reporting.model;

import java.sql.Date;

public class LabLineItem {

	@Override
	public String toString() {
		return "LabLineItem [productCode=" + productCode
				+ ", machineTestCount=" + machineTestCount
				+ ", equipFunctioning=" + equipFunctioning + ", beginDate="
				+ beginDate + ", endDate=" + endDate + ", remarks=" + remarks
				+ ", orderStatus=" + orderStatus + "]";
	}
	private int id;
	private String productCode;
	private int machineTestCount;
	private String equipFunctioning;
	private Date beginDate;
	private Date endDate;
	private String remarks;
	private boolean orderStatus;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public int getMachineTestCount() {
		return machineTestCount;
	}
	public void setMachineTestCount(int machineTestCount) {
		this.machineTestCount = machineTestCount;
	}
	public String getEquipFunctioning() {
		return equipFunctioning;
	}
	public void setEquipFunctioning(String equipFunctioning) {
		this.equipFunctioning = equipFunctioning;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public boolean isOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(boolean orderStatus) {
		this.orderStatus = orderStatus;
	}
}
