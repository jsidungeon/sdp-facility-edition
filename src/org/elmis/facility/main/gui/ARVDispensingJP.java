/*
 * StartJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.JFrame;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.swing.JRViewer;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.reports.utils.ReportStorageManager;
import org.elmis.forms.stores.arv_dispensing.SelectProductsJD;
import org.elmis.forms.stores.arv_dispensing.arvdispensingProductsPhysicalCountJD;
import org.elmis.forms.stores.arv_dispensing.arvdispensingproductAdjustmentsJD;
import org.elmis.forms.stores.arv_dispensing.copyReceivingJD;

import javax.swing.JLabel;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;

/**
 * 
 * @author __USER__
 */
public class ARVDispensingJP extends javax.swing.JPanel implements
		ActionListener {

	List<String> userRightsList;
	private String userRights;
	// private static Map parameterMap = new HashMap();
	// private JasperPrint print;
	private static java.util.Date startDate;
	private static java.util.Date endDate;
	private int siteId;
	private static Map parameterMap = new HashMap();
    private ProgressBarJD showmyProgressbar ;
	ARVDispensingDAO callmapper;
	List<Elmis_Dar_Transactions> arvfinalARVdarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvfixeddarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumallproductsList  =  new LinkedList();
	List<Elmis_Dar_Transactions> arvalldardoseproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvalldardoseproductsListjasper = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumdarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumsingledosedarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumliquidpowderdarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsumcotrimoxazoledarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvsingledosedarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvliquid_powderdarproductsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvcotridarproductsList = new LinkedList();
	Collection<List<Elmis_Dar_Transactions>> coll = new LinkedList();

	// private Logger logger = Logger.getLogger(this.getClass());
	private JasperPrint print;
	private ReportStorageManager reportstoragemanager;

	/** Creates new form StartJP */
	public ARVDispensingJP(List<String> userRightsList) {
		this.userRightsList = userRightsList;
		this.siteId = siteId;
		initComponents();

		/*
		 * dispenseJL1.setVisible(false); ARVdarJL.setVisible(false);
		 * viewARVdarJL.setVisible(false); physicalCountJL.setVisible(false);
		 */

		for (String userRights : userRightsList) {

			// hiv Testing
			/*
			 * if
			 * (this.hivTestingJL.getName().equalsIgnoreCase(userRights.trim()))
			 * { hivTestingJL.setVisible(true); }
			 */
		}

	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		dispenseJL = new javax.swing.JLabel();
		viewARVdarJL = new javax.swing.JLabel();
		ARVdarJL = new javax.swing.JLabel();
		dispenseJL1 = new javax.swing.JLabel();
		physicalCountJL = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		dispenseJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		dispenseJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/income_reports_55.png"))); // NOI18N
		dispenseJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Dispensing"));
		dispenseJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				// dispenseJLMouseClicked(evt);
			}
		});

		viewARVdarJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		viewARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view dar icon.png"))); // NOI18N
		viewARVdarJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "View DAR",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		viewARVdarJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewARVdarJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				viewARVdarJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				viewARVdarJLMouseExited(evt);
			}
		});

		ARVdarJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		ARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS losses&adjust.png"))); // NOI18N
		ARVdarJL.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
				"Adjustments",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		ARVdarJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ARVdarJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				ARVdarJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				ARVdarJLMouseExited(evt);
			}
		});

		dispenseJL1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		dispenseJL1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS Dispensing icon.png"))); // NOI18N
		dispenseJL1.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Dispensing",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		dispenseJL1.setPreferredSize(new java.awt.Dimension(113, 113));
		dispenseJL1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				dispenseJL1MouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				dispenseJL1MouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				dispenseJL1MouseExited(evt);
			}
		});

		physicalCountJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		physicalCountJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS Physical count.png"))); // NOI18N
		physicalCountJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "Physical Count",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		physicalCountJL
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		physicalCountJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				physicalCountJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				physicalCountJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				physicalCountJLMouseExited(evt);
			}
		});
		
		ViewARVproductAdjustmentPCJL = new JLabel("");
		ViewARVproductAdjustmentPCJL
		.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		ViewARVproductAdjustmentPCJL.setIcon(new ImageIcon(ARVDispensingJP.class.getResource("/elmis_images/eLMIS view dar icon.png"))); // NOI18N
		ViewARVproductAdjustmentPCJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
		null, "View Product Status",
		javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
		javax.swing.border.TitledBorder.DEFAULT_POSITION,
		new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
				255, 255)));
		ViewARVproductAdjustmentPCJL
		.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		ViewARVproductAdjustmentPCJL.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				ViewARVproductAdjustmentPCJLJLMouseClicked(e);
			}

			
		});
		
	
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(Alignment.LEADING)
						.addGroup(layout.createSequentialGroup()
							.addGap(456)
							.addComponent(dispenseJL, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE))
						.addGroup(layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(dispenseJL1, GroupLayout.PREFERRED_SIZE, 121, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(ARVdarJL, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(viewARVdarJL, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(physicalCountJL, GroupLayout.PREFERRED_SIZE, 124, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(ViewARVproductAdjustmentPCJL, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(121, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(dispenseJL, GroupLayout.PREFERRED_SIZE, 0, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(layout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(ViewARVproductAdjustmentPCJL, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(ARVdarJL, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(viewARVdarJL, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(physicalCountJL, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(dispenseJL1, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE))
					.addContainerGap(147, Short.MAX_VALUE))
		);
		this.setLayout(layout);
	}// </editor-fold>
		// GEN-END:initComponents

	private void physicalCountJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		physicalCountJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS Physical count.png")));
	}

	private void physicalCountJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		physicalCountJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMIS physical count big.png")));
	}

	private void viewARVdarJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		viewARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view dar icon.png")));
	}

	private void viewARVdarJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		viewARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS view dar big.png")));
	}

	private void ARVdarJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		ARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS losses&adjust.png")));
	}

	private void ARVdarJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		ARVdarJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS adjustments icon big.png")));
	}

	private void dispenseJL1MouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		dispenseJL1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS Dispensing icon.png")));
	}

	private void dispenseJL1MouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		// TODO add your handling code here:
		dispenseJL1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS dispensing icon big.png")));
	}

	
	private void ViewARVproductAdjustmentPCJLJLMouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		AppJFrame.glassPane.activate(null);
		new SelectProductsJD(
				javax.swing.JOptionPane.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}
	private void physicalCountJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling
		AppJFrame.glassPane.activate(null);
		new arvdispensingProductsPhysicalCountJD(
				javax.swing.JOptionPane.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}

	public static Date toDate(java.sql.Timestamp timestamp) {
		long millisec = timestamp.getTime() + (timestamp.getNanos() / 1000000);
		return new Date(millisec);

	}

	@SuppressWarnings("unused")
	private static JRBeanCollectionDataSource getDataSource(
			Collection<List<Elmis_Dar_Transactions>> coll) {

		return new JRBeanCollectionDataSource(coll);
	}

	private void viewARVdarJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		// TODO add your handling code here:
		// elmis_dar_transactions = new Elmis_Dar_Transactions();

		// create a timestamp
	//showmyProgressbar = new ProgressBarJD("Processing Report Data", null, AppJFrame.getDispensingId("ARV"));   
		Timestamp productdeliverdate = null;
		String oldDateString;
		String mydate = new java.util.Date().toString();
		String newDateString;
		final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
		// final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
		final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
		oldDateString = mydate;

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		java.util.Date d;
		d = null;
		try {
			d = sdf.parse(oldDateString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
		productdeliverdate = Timestamp.valueOf(newDateString);

		// end create time stamp

		Date Startdate = new Date();
		Date Enddate = new Date();
		if (productdeliverdate != null) {

			// convert to date value
			Date convertdate = toDate(productdeliverdate);
			// date split up methods
			Calendar cal = Calendar.getInstance();
			cal.setTime(convertdate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			if (!(day == 1)) {
				day = 1;
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.DATE, day);
				cal.set(Calendar.YEAR, year);
				Startdate = cal.getTime();
			}
			Enddate = toDate(productdeliverdate);
		}

		NumberFormat formatter = new DecimalFormat("#0.00");     
		System.out.println(formatter.format(4.0));
		
		callmapper = new ARVDispensingDAO();

		arvfinalARVdarproductsList = callmapper.doselectfinalsumDARarv(Startdate, Enddate,AppJFrame.getDispensingId("ARV"));
		
		arvfixeddarproductsList = callmapper.dogetcurrentARVdar(Startdate,
				Enddate);
		arvsingledosedarproductsList = callmapper.dosingledoseARVdar(Startdate,
				Enddate);
		this.arvliquid_powderdarproductsList = callmapper
				.doliquidpowderdoseARVdar(Startdate, Enddate);
		this.arvcotridarproductsList = callmapper.docotrimoxazoledoseARVdar(
				Startdate, Enddate);
		arvalldardoseproductsList = callmapper.dogetcurrentfinalARVdar(Startdate, Enddate,AppJFrame.getDispensingId("ARV"));
		arvsumallproductsList = callmapper.doselectfinalsumDARarv(Startdate,
				Enddate,AppJFrame.getDispensingId("ARV"));
		arvsumdarproductsList = callmapper.doselectsumDARarv(Startdate, Enddate);


		System.out.println("I am size..." + arvfixeddarproductsList.size());

		System.out.println(new java.io.File("").getAbsolutePath());

		Map<String, Object> parameters = new HashMap<String, Object>();

		try {
			double totalmonthdispensed = 0;
			int sumtotal = 0;
			int sumliquidpowder = 0;
			int sumsingledose = 0;
			int sumcotrimoxazole = 0;
			Timestamp thisdate = null;
			String thisartnumber = null;
			String thisproductname = null;
			Double thisdispensedbottle = 0.0;

			Timestamp thisliquiddate = null;
			String thisliquidartnumber = null;
			String thisliquidproductname = null;
			Double thisliquiddispensedbottle = 0.0;

			for (Elmis_Dar_Transactions e : arvsumdarproductsList) {
				if (!(e == null)) {
					sumtotal = e.getTotaldispensed();
				}
			}
			for (Elmis_Dar_Transactions e : arvsumsingledosedarproductsList) {
				if (!(e == null)) {
					sumsingledose = e.getTotaldispensed();
				}
			}
			for (Elmis_Dar_Transactions e : arvsumcotrimoxazoledarproductsList) {
				if (!(e == null)) {
					sumcotrimoxazole = e.getTotaldispensed();
				}

			}
			for (Elmis_Dar_Transactions e : arvsumliquidpowderdarproductsList) {

				if (!(e == null)) {
					sumliquidpowder = e.getTotaldispensed();
				}

			}

			for (Elmis_Dar_Transactions e : arvsingledosedarproductsList) {
				thisdate = e.getTransaction_date();
				thisartnumber = e.getArt_number();
				thisproductname = e.getPrimaryname();
				thisdispensedbottle = e.getNo_equiv_bottles();

			}

			for (Elmis_Dar_Transactions e : arvliquid_powderdarproductsList) {

				thisliquiddate = e.getTransaction_date();
				thisliquidartnumber = e.getArt_number();
				thisliquidproductname = e.getPrimaryname();
				thisliquiddispensedbottle = e.getNo_equiv_bottles();

			}

			for (Elmis_Dar_Transactions u : arvsumallproductsList ){
				try{
				totalmonthdispensed = u.getQtydispensedforMonth();
				}catch(java.lang.NullPointerException n){
					n.getMessage();
				}
			}
            
			
			
			// load report location
			// String masterfile = "./Reports/ARVdarreport.jrxml";
			String masterfile = "./Reports/ARVdarreportdesign.jrxml";
			String subfile = "./Reports/arv_singledosereport.jrxml";
			//String subfixeddose = "./Reports/arv_fixeddosereport.jrxml";

			// totalmonthdispensed = sumtotal + sumsingledose + sumcotrimoxazole
			// + sumliquidpowder;
			// parameters.put("printdetail3",arvsingledosedarproductsList.size())
			// ;
			// parameters.put("totaldispensedformonth", totalmonthdispensed);
			// parameters.put("ARVdarsingledose", arvsingledosedarproductsList);

			// Single dose parameters
			/*
			 * parameters.put("dispensedate", thisdate);
			 * parameters.put("patientnumber",thisartnumber);
			 * parameters.put("arvproductname", thisproductname);
			 * parameters.put("dispensedbottles", thisdispensedbottle);
			 */
			
			/*
			 * parameters.put("liquidpowderdate", thisliquiddate);
			 * parameters.put("liquidpowderpatientnumber",thisliquidartnumber);
			 * parameters.put("liquidpowderproductname", thisliquidproductname);
			 * parameters.put("liquidpowderdispensedbottle",
			 * thisliquiddispensedbottle);
			 */
		/*coll.add(arvfixeddarproductsList);
			coll.add(arvsingledosedarproductsList);
			coll.add(arvliquid_powderdarproductsList);
			coll.add(arvcotridarproductsList);*/

			@SuppressWarnings("unused")
			JasperReport rptsingledose, rptmaster, rptmastermain, rptmastersingledose;// =
																						// null;
			@SuppressWarnings("unused")
			JasperPrint prtsingledose, prtmaster, prtmastermain;// = null;
			@SuppressWarnings("unused")
			JasperDesign subReportsingledose, master, mastermain, masterfixeddose, mastersingledose;// =
																									// null;

			JRBeanCollectionDataSource masterbeanColDataSource = new JRBeanCollectionDataSource(
					arvfinalARVdarproductsList);
			@SuppressWarnings("unused")
			JRBeanCollectionDataSource singledosebeanColDataSource = new JRBeanCollectionDataSource(
					arvsingledosedarproductsList);
			// master = JRXmlLoader.load("./Reports/ARVdarreport.jrxml");
			JRBeanCollectionDataSource fixeddosebeanColDataSource = new JRBeanCollectionDataSource(
					arvfixeddarproductsList);
			JRBeanCollectionDataSource finalmasterbeanColDataSource = new JRBeanCollectionDataSource(arvalldardoseproductsList);
			mastermain = JRXmlLoader.load("./Reports/ARVdarreportdesign.jrxml");
			//masterfixeddose = JRXmlLoader					.load("./Reports/arv_fixeddosereport.jrxml");
			mastersingledose = JRXmlLoader
					.load("./Reports/arv_singledosereport.jrxml");
			// subReportsingledose =
			// JRXmlLoader.load("C:\\Users\\mkausa\\Documents\\programmers\\developerclone_workspace\\elmis-facility (withAllAdmin)\\ARVdarsingledosereport.jrxml");
			rptmastermain = JasperCompileManager.compileReport(mastermain);
			//rptmaster = JasperCompileManager.compileReport(masterfixeddose);
			rptmastersingledose = JasperCompileManager
					.compileReport(mastersingledose);

			parameters.put("dispenserInitial", AppJFrame.userLoggedIn);
			parameters.put("facilityname", System.getProperty("facilityname"));
			parameters.put("district", System.getProperty("district"));
			parameters.put("totaldispensedformonth", totalmonthdispensed);
			parameters.put("ARVselectedDispensingname", AppJFrame.getDispensingPointName("ARV"));
			//parameters.put("fixeddoseSUBREPORT", arvfixeddarproductsList);
			//parameters.put("SUBREPORT_DIR2", singledosebeanColDataSource);
			// Liquid powder parameters
			//@SuppressWarnings("unused")
			//JRBeanCollectionDataSource arvdarfinalDatasource = getDataSource(coll);
			//prtmaster = JasperFillManager.fillReport(rptmaster, parameters,fixeddosebeanColDataSource);
			//prtsingledose = JasperFillManager.fillReport(rptmastersingledose,parameters, singledosebeanColDataSource);

		 //  prtmastermain = JasperFillManager.fillReport(rptmastermain,parameters, finalmasterbeanColDataSource);
		   prtmastermain = JasperFillManager.fillReport(rptmastermain,parameters,finalmasterbeanColDataSource);
		
			reportstoragemanager = new ReportStorageManager();
			String pdfpath = null;
			if (reportstoragemanager.isFolderAvailable() == true) {
				System.out.println("found folder for pdf");
				/*
				 * pdfpath = reportstoragemanager.getReportFolder();
				 * JasperExportManager.exportReportToPdfFile(prtmaster, pdfpath
				 * + "/" + "arvdarreport.pdf");
				 * reportstoragemanager.getReportFolder(); RenderPdf renderPdf =
				 * new RenderPdf(); renderPdf.renderPdf(pdfpath + "/" +
				 * "arvdarreport.pdf");
				 */
				/* frame for fixed dose */
				
				
				/* frame for single dose */
				
				/* frame for master report */
				JFrame frame2 = new JFrame("Report");
				frame2.getContentPane().setPreferredSize(
						new Dimension(1300, 650));
				frame2.getContentPane().add(new JRViewer(prtmastermain));
				frame2.pack();
				frame2.setVisible(true);

			}

			/*
			 * JasperExportManager.exportReportToPdfFile(prtmaster,
			 * "C:\\Users\\mkausa\\Desktop\\arvdarreport.pdf");
			 */

			// RenderPdf renderPdf = new RenderPdf();

			// renderPdf.renderPdf("C:\\Users\\mkausa\\Desktop\\arvdarreport.pdf");

		} catch (JRException ex) {
			System.out.println("---" + ex.getLocalizedMessage());
		}

		
	}

	private void ARVdarJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		try {
			AppJFrame.glassPane.activate(null);
			new arvdispensingproductAdjustmentsJD(
					javax.swing.JOptionPane.getFrameForComponent(this), true);
		} finally {
			AppJFrame.glassPane.deactivate();
		}

	}

	private void dispenseJL1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		AppJFrame.glassPane.activate(null);
		new copyReceivingJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);

		AppJFrame.glassPane.deactivate();

	}

	/********* Action Listener Coding Starts *************/
	// this action Listener for phone book
	//
	public void actionPerformed(ActionEvent evt) {

		AppJFrame.glassPane.activate(null);
		// new
		// ContactAdminJD(javax.swing.JOptionPane.getFrameForComponent(this),

		AppJFrame.glassPane.deactivate();

	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel ARVdarJL;
	private javax.swing.JLabel dispenseJL;
	private javax.swing.JLabel dispenseJL1;
	private javax.swing.JLabel physicalCountJL;
	private javax.swing.JLabel viewARVdarJL;
	private JLabel ViewARVproductAdjustmentPCJL;
	// End of variables declaration//GEN-END:variables

}