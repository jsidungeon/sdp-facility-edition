package org.elmis.facility.service.arv;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.main.gui.AppJFrame;

public class ARVDispensing implements IARVDispensing{
	
	private ARVDispensingDAO arvDispensingDAO = new ARVDispensingDAO();
	
	@Override
	public void saveDARTxN(Elmis_Dar_Transactions darTxn) {
		arvDispensingDAO
		.insertdispensedProducts(darTxn);
	}

	@Override
	public void updateARVBalace(Double qtyDeducted,String productCode,Integer dispensationUnitId) {
		arvDispensingDAO.updateBalance(
				qtyDeducted,
				productCode,
				AppJFrame.getDispensingId("ARV"));
		
	}

	@Override
	public Elmis_Patient getARVClientByART(String art) {
		return arvDispensingDAO.dogetcurrentARVclientbyART(art);
	}

	@Override
	public Elmis_Patient getARVClientByNRC(String nrc) {
		return arvDispensingDAO
				.dogetcurrentARVclientbyNRC(nrc);
	}

	@Override
	public void registerClient(Elmis_Patient client) {
		arvDispensingDAO.Registerpatient(client);
	}

	@Override
	public void updateARVClient(Elmis_Patient client) {
		arvDispensingDAO.doupdateARVClientdetails(
				client.getNrc_number(),
				client.getDateofbirth(),
				client.getFirstname(),
				client.getLastname(), 
				client.getSex(),
				client.getArt_number());
		
	}

}
