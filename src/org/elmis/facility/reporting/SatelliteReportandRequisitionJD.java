/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.reporting;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.MutableComboBoxModel;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityPhysicalCountDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.SatelliteRandR;
import org.elmis.facility.domain.model.Store_Physical_Count;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.elmis.forms.stores.receiving.arvmessageDialog;

import com.oribicom.tools.TableModel;

import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.GroupLayout;

import java.awt.Label;

import javax.swing.JLabel;

import java.awt.Color;
import java.awt.TextField;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import java.awt.event.KeyAdapter;

import com.toedter.calendar.JDateChooser;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class SatelliteReportandRequisitionJD extends javax.swing.JDialog {

	TableColumnAligner tablecolumnaligner;
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;
	private Integer Productid = 0; 
	private List<Character> charList = new LinkedList();
	private char letter;
	private String searchText = "";
	private String pname;
	private String pstrength;
	private String SearchResult = "";
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> searcharvproductsList = new LinkedList();
	List<VW_Systemcalculatedproductsbalance> productsPhysicalcountList = new LinkedList();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
    private Elmis_Stock_Control_Card 	savestockcontrolcard = null;
	private String mydateformat = "yyyy-MM-dd";// hh:mm:ss";
	public Timestamp satellitereportdeliverdate;
	public String oldDateString;
	public String newDateString;
	private Date  Startdate;
	private Date Enddate;
	public Boolean productsearchenabled = false;
	public Boolean calltracer = false;
	public Boolean saveresponse = false;
	private Integer  row1 ;
	private  Integer row2 ;
	
	ARVDispensingDAO satellitemapper;
	
	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
		"Product Code","Drug Product","Unit",
			
			"Total Dispensed",
			"Losses and Adjustments" ,"Physical Count",};
	
	private static final String[] columns_searchfacilityApprovedproducts = {"Product Code","Drug Product","Unit",
		
		"Total Dispensed",
		"Losses and Adjustments" ,"Physical Count"
		};
	private static final Object[] defaultv_facilityapprovedproducts = {"", "", "",
			"", "", "" };
	private static final Object[] defaultv_searchfacilityapprovedproducts = {"", "", "",
		"", "", ""};
	private static final int rows_fproducts = 0;
	private static final int rows_searchfproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] {false,false, false, true, true, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 6) {

				return getValueAt(0,6).getClass();
			}
		
			return super.getColumnClass(columnIndex);
		}

	
	};

	public static TableModel tableModel_searchfproducts = new TableModel(
			columns_searchfacilityApprovedproducts,
			defaultv_searchfacilityapprovedproducts, rows_searchfproducts) {

		boolean[] canEdit = new boolean[] {false, false, false, true, true,
				true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
			

				if (columnIndex == 6) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,6).getClass();
					// }	
				}
				

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

	

	};

	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsbalanceList = new LinkedList();
	ArrayList<Store_Physical_Count> sccproductbalanceList = new ArrayList<Store_Physical_Count>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> productbalanceprogramsList = new LinkedList();
	
	ListIterator sccproductbalanceiterator = productsbalanceList.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProductsbalance;
	private Products facilitysccproduct;
	private FacilityPhysicalCountDAO physicalcountupdater = null;
	FacilityProductsDAO Facilityproductsmapper = null;
	private Timestamp shipmentdate;
	private Store_Physical_Count storephysicalcount;
	private SatelliteRandR saveSatelliteRandR;
	
	public static Boolean cansave = false;
	public static Boolean Adjustmentboolean = true;
	private String Pcode = "";
	private String Prodcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private int rnrid;
	private int intQtyreceived = 0;
	public String facilityprogramcode = "";
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private String programname = "";
	public static final String  STOREPHYSICAlCOUNTCALL = "STOREPHYSICAlCOUNTCALL"; 
	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();

	private JComboBox balanceProgramList = new JComboBox();
	MutableComboBoxModel modelbalanceProgramslist = (MutableComboBoxModel) balanceProgramList
			.getModel();

	/** Creates new form receiveProducts */
	public SatelliteReportandRequisitionJD(java.awt.Frame parent,
			boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(1350, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		facilityapprovedProductsJT = new javax.swing.JTable();
		CancelJBtn = new javax.swing.JButton();
		SaveJBtn = new javax.swing.JButton();
		jPanel2 = new javax.swing.JPanel();
		StartDatedateChooser = new com.toedter.calendar.JDateChooser();
		EndDatedateChooser = new com.toedter.calendar.JDateChooser();
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Satellite Report and Requisition");
		setBackground(new java.awt.Color(51, 51, 255));
		setFont(new java.awt.Font("Ebrima", 0, 20));
		setForeground(new java.awt.Color(204, 153, 0));
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		facilityapprovedProductsJT.setFont(new java.awt.Font("Ebrima", 0, 20));
		facilityapprovedProductsJT.setModel(tableModel_fproducts);
		facilityapprovedProductsJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						facilityapprovedProductsJTMouseClicked(evt);
					}
				});
		facilityapprovedProductsJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						facilityapprovedProductsJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(facilityapprovedProductsJT);

		CancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBtn.setText("Close");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save  ");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));
		
		JLableSearch = new JLabel("Product Name Search");
		JLableSearch.setForeground(Color.WHITE);
		JLableSearch.setFont(new Font("Ebrima", Font.BOLD, 18));
		
		SearchproductsJTF = new JTextField();
		SearchproductsJTF.addKeyListener(new KeyAdapter() {
		
			public void keyTyped(java.awt.event.KeyEvent evt) {
				
				SearchproductsJTFKeyTyped(evt);
			}
		});
		SearchproductsJTF.setFont(new Font("Ebrima", Font.PLAIN, 18));
		SearchproductsJTF.setColumns(10);
		
		
		StartDatedateChooser.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				StartDatedateChooserPropertyChange(evt);
			}


		});
		StartDatedateChooser.getCalendarButton().setFont(new Font("Ebrima", Font.PLAIN, 13));
		StartDatedateChooser.setPreferredSize(new Dimension(150, 20));
		StartDatedateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		StartDatedateChooser.setDateFormatString("yyyy-MM-dd hh:mm:ss");
		
		
		EndDatedateChooser.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				EndDatedateChooserpropertyChange(evt)
				;
			}

			
		});
		EndDatedateChooser.getCalendarButton().setFont(new Font("Ebrima", Font.PLAIN, 13));
		EndDatedateChooser.setPreferredSize(new Dimension(150, 20));
		EndDatedateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		EndDatedateChooser.setDateFormatString("yyyy-MM-dd hh:mm:ss");
		
		JLabel lblReportingPeriodStart = new JLabel("Reporting Period Start Date ");
		lblReportingPeriodStart.setForeground(Color.WHITE);
		lblReportingPeriodStart.setFont(new Font("Ebrima", Font.BOLD, 18));
		
		JLabel lblReportingPeriodEnd = new JLabel("Reporting Period End Date");
		lblReportingPeriodEnd.setForeground(Color.WHITE);
		lblReportingPeriodEnd.setFont(new Font("Ebrima", Font.BOLD, 18));

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(StartDatedateChooser, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblReportingPeriodStart, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(57)
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(EndDatedateChooser, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(lblReportingPeriodEnd, GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(JLableSearch, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(SearchproductsJTF, GroupLayout.DEFAULT_SIZE, 313, Short.MAX_VALUE)))
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblReportingPeriodEnd, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblReportingPeriodStart, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
						.addComponent(JLableSearch))
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel2Layout.createSequentialGroup()
							.addGap(18)
							.addComponent(StartDatedateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel2Layout.createSequentialGroup()
							.addGap(18)
							.addGroup(jPanel2Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(EndDatedateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
								.addComponent(SearchproductsJTF, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))))
					.addContainerGap(20, Short.MAX_VALUE))
		);
		jPanel2.setLayout(jPanel2Layout);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
							.addGap(10)
							.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 887, Short.MAX_VALUE))
						.addGroup(Alignment.LEADING, jPanel1Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addContainerGap(705, Short.MAX_VALUE)
							.addComponent(CancelJBtn)
							.addGap(18)
							.addComponent(SaveJBtn)))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(18)
					.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 341, GroupLayout.PREFERRED_SIZE)
					.addGap(29)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(CancelJBtn)
						.addComponent(SaveJBtn))
					.addContainerGap(76, Short.MAX_VALUE))
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}// </editor-fold>
	
	private void EndDatedateChooserpropertyChange(
			PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
		
		try {

			Calendar currentDate = Calendar.getInstance(); // Get the current
															// date
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // format
																				// it
																				// as
																				// per
																				// your
													
			// requirement
			String dateNow = formatter.format(currentDate.getTime());
			System.out.println("Now the date is :=>  " + dateNow);

			Date mydate = this.EndDatedateChooser.getDate();
			String sdate = formatter.format(mydate);
			System.out.println(sdate);
			// this.registerdate = mydate;
			Date d = mydate;
			if (d.after(currentDate.getTime())) {
				System.out.println("Test date diff ");
				
				JOptionPane.showMessageDialog(this,
						"Dispensing Date cannnot be in the future",

						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				//this.StartDatedateChooser.setDate(new Date());
			}else{
				this.Enddate = d;
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		
		
		
		
	}
	
	private void SearchproductsJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:
				productsearchenabled  = true ;
				System.out.println(this.arvsearchproductsList.size() + "list not null");

				letter = evt.getKeyChar();

				StringBuilder s = new StringBuilder(charList.size());

				if ((letter == KeyEvent.VK_BACK_SPACE)
						|| (letter == KeyEvent.VK_DELETE)) {

					// do nothing
					searchText = this.SearchproductsJTF.getText();
				    System.out.println("I detedcted a back space ah! did you call me ??");
			           //re populate table with original list 
			           this.callbackARVproductlist();
					if (charList.size() > 0) {

						charList.remove(charList.size() - 1);

					}

					if (charList.size() <= 0) {

						s = new StringBuilder(charList.size());
						charList.clear();
					}

					for (char c : charList) {

						s.append(c);

					}
				} else {

					charList.add(letter);

					for (char c : charList) {

						s.append(c);

					}

					//get ARV list 
					try {
						if (!(this.SearchproductsJTF.getText() == "")) {

							if (!(s.substring(0, 1).matches("[0-9]"))) {

								callfacility = new FacilitySetUpDAO();
								Facilityproductsmapper = new FacilityProductsDAO();

					

								facility = callfacility.getFacility();

								facilitytype = callfacility
										.selectAllwithTypes(facility);
								typeCode = facilitytype.getCode();

								productsList = Facilityproductsmapper
										.dogetcurrentFacilityApprovedProducts(
												facilityprogramcode, typeCode);
								arvsearchproductsList = productsList;

						

								for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {
									if (p.getPrimaryname().toLowerCase().contains(s.toString().toLowerCase())) {
										this.arvproductsList.add(p);
									}
									
								}

							

								this.searchPopulateProgram_ProductTable(
										arvproductsList, "STRING_FOUND");

							}
						}

					} catch (NullPointerException e) {
						e.getMessage();
					}

				}

	}
	


	
	private void callbackARVproductlist(){
		//get ARV list 
		try {
			if (!(this.SearchproductsJTF.getText() == "")) {

		
					callfacility = new FacilitySetUpDAO();
					Facilityproductsmapper = new FacilityProductsDAO();

					//this.facilityprogramcode = "ARV";
				    facility = callfacility.getFacility();

					facilitytype = callfacility
							.selectAllwithTypes(facility);
					typeCode = facilitytype.getCode();

					productsList = Facilityproductsmapper
							.dogetcurrentFacilityApprovedProducts(
									facilityprogramcode, typeCode);
					arvsearchproductsList = productsList;

					this.searchPopulateProgram_ProductTable(
							arvsearchproductsList, "STRING_FOUND");

				
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}

	@SuppressWarnings("unchecked")
	private void searchPopulateProgram_ProductTable(List fapprovProducts,
			String mysearch) {

		try {

			this.SearchResult = mysearch;

		
			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : searcharvproductsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			tableModel_searchfproducts.clearTable(); 
			this.facilityapprovedProductsJT
					.setModel(tableModel_searchfproducts);


			fapprovedproductsIterator = fapprovProducts.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");

				defaultv_searchfacilityapprovedproducts[0] = facilitysccProducts
						.getCode();

				defaultv_searchfacilityapprovedproducts[1] = facilitysccProducts
						.getPrimaryname();
				defaultv_searchfacilityapprovedproducts[2] = facilitysccProducts
						.getStrength();
	

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_searchfacilityApprovedproducts.length; j++) {
					cols.add(defaultv_searchfacilityapprovedproducts[j]);
					
				}

				tableModel_searchfproducts.insertRow(cols);
				//tableModel_searchfproducts.fireTableDataChanged();
				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	public static Date toDate(java.sql.Timestamp timestamp) {
		long millisec = timestamp.getTime() + (timestamp.getNanos() / 1000000);
		return new Date(millisec);

	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.dispose();
	}

	/*private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			java.util.Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productdeliverdate;

	}*/

	
	private boolean checktableCellinput(String s){
	
			 try { 
			        Integer.parseInt(s); 
			    } catch(NumberFormatException e) { 
			        return false; 
			    }
			    // only got here if we didn't return false
			    return true;
	}
	private void facilityapprovedProductsJTPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		/*facilityapprovedProductsJT.getColumnModel().getColumn(0).setMinWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setMaxWidth(0);
		facilityapprovedProductsJT.getColumnModel().getColumn(0).setWidth(0);*/
		
		
		facilityapprovedProductsJT.getColumnModel().getColumn(1).setMinWidth(530);
		facilityapprovedProductsJT.getColumnModel().getColumn(1).setMaxWidth(530);
		facilityapprovedProductsJT.getColumnModel().getColumn(1).setWidth(530);
		
		facilityapprovedProductsJT.getColumnModel().getColumn(2).setMinWidth(115);
		facilityapprovedProductsJT.getColumnModel().getColumn(2).setMaxWidth(115);
		facilityapprovedProductsJT.getColumnModel().getColumn(2).setWidth(115);
		
		facilityapprovedProductsJT.getColumnModel().getColumn(3).setMinWidth(160);
		facilityapprovedProductsJT.getColumnModel().getColumn(3).setMaxWidth(160);
		facilityapprovedProductsJT.getColumnModel().getColumn(3).setWidth(160);
		
		facilityapprovedProductsJT.getColumnModel().getColumn(4).setMinWidth(220);
		facilityapprovedProductsJT.getColumnModel().getColumn(4).setMaxWidth(220);
		facilityapprovedProductsJT.getColumnModel().getColumn(4).setWidth(220);
				
		if ("tableCellEditor".equals(evt.getPropertyName())) {
		
			//saveShipped_Line_Items = new Shipped_Line_Items();
			if (this.facilityapprovedProductsJT.isColumnSelected(3)) {
				
			}

			if (this.facilityapprovedProductsJT.isColumnSelected(4)) {
				if (facilityapprovedProductsJT.isEditing()) {
			
					System.out.println("THIS CELL HAS STARTED EDITING");
					//get column index
					colindex = this.facilityapprovedProductsJT
							.getSelectedColumn();
					rowindex = this.facilityapprovedProductsJT
							.getSelectedRow();
					Prodcode = tableModel_fproducts.getValueAt(
							this.facilityapprovedProductsJT
									.getSelectedRow(),1).toString();
					this.row1 = facilityapprovedProductsJT.getSelectedRow();

				} else if (!facilityapprovedProductsJT.isEditing()) {
					this.row2 = facilityapprovedProductsJT.getSelectedRow();
					this.calltracer = false;
					String chars
					 = tableModel_fproducts.getValueAt(
								this.facilityapprovedProductsJT.getSelectedRow(), 4).toString();
		          		if(!(chars.isEmpty())){			
						if (!(chars.substring(0, 1).matches("[0-9]"))) {	
							
							if(!(checktableCellinput((chars.substring(0, 1))))){
								
						    	JOptionPane.showMessageDialog(null, String.format("Physical Count must be a positive number", chars.substring(0, 1)));	
						    	//facilityapprovedProductsJT.editCellAt(this.facilityapprovedProductsJT.getSelectedRow(), 3);
						    }
							
						}
		          		}
					
		          	if(calltracer == false){
		          	  if ((chars.substring(0, 1).matches("[0-9]"))) {	
					    
		          		}
		          	
					}
					 //call tracer = true;
					System.out.println("THIS CELL IS NOT EDITING");
					 
				}
			}
		}
	}
	
	private void StartDatedateChooserPropertyChange(
			PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
		try {

			Calendar currentDate = Calendar.getInstance(); // Get the current
															// date
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // format
																				// it
																				// as
																				// per
																				// your
													
			// requirement
			String dateNow = formatter.format(currentDate.getTime());
			System.out.println("Now the date is :=>  " + dateNow);

			Date mydate = this.StartDatedateChooser.getDate();
			String sdate = formatter.format(mydate);
			System.out.println(sdate);
			// this.registerdate = mydate;
			Date d = mydate;
			if (d.after(currentDate.getTime())) {
				System.out.println("Test date diff ");
				
				JOptionPane.showMessageDialog(this,
						"Dispensing Date cannnot be in the future",

						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				//this.StartDatedateChooser.setDate(new Date());
			}else{
				this.Startdate =  d;
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		
	}

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	private void facilityapprovedProductsJTMouseClicked(
			java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call create  shipment object 
  	    	
		
		/* if ( productsearchenabled == true){
		        
	    	    		
	    		
	    		Pcode = tableModel_searchfproducts.getValueAt(
	    				this.facilityapprovedProductsJT
	    						.getSelectedRow(), 1).toString();
	    		
	    		Productid = (Integer
	    				.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 0)
	    						.toString()));
	    		
	    		pname = tableModel_searchfproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),2).toString();
	    		
	    		pstrength = tableModel_searchfproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),3).toString();
		
		 }else{
	    		
	    		Pcode = tableModel_fproducts.getValueAt(
	    				this.facilityapprovedProductsJT
	    						.getSelectedRow(), 1).toString();
	    		
	    		Productid = (Integer
	    				.parseInt(tableModel_fproducts.getValueAt(rowindex, 0)
	    						.toString()));
	    		
	    		pname = tableModel_fproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),2).toString();
	    		
	    		pstrength = tableModel_fproducts.getValueAt(
	    				this.facilityapprovedProductsJT
						.getSelectedRow(),3).toString();
					
		 }*/

	}



	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		String displaymessage = "";
		Boolean persist  = false;
		Boolean clickonce = false;
	
		satellitemapper = new ARVDispensingDAO();
		
			if(!(this.productsearchenabled == true)){
		
				saveSatelliteRandR = new  SatelliteRandR();
					//	System.out.println(elmisstockcontrolcardList.size());
			 		this.facilityapprovedProductsJT.editCellAt(-1,-1);
			 		int j = 0 ;
			 		for(int i = 0 ; i < this.facilityapprovedProductsJT.getRowCount();i++){
			 			
						
			 			if (this.Startdate == null) {
		 					java.util.Date date = new java.util.Date();
		 					Startdate = date;
		 				}
			 			 if (this.Enddate == null) {
		 					java.util.Date date = new java.util.Date();
		 					Enddate = date;  
		 					
		 				}
			 			if(!(this.facilityapprovedProductsJT.getValueAt(i,5).toString().equals(""))){
			 				if(!(this.facilityapprovedProductsJT.getValueAt(i,4).toString().equals(""))){
			 					if(!(this.facilityapprovedProductsJT.getValueAt(i,3).toString().equals(""))){
			 				System.out.println("HOW WBIG IS THE SELECTION IN JTABLE");
			 				persist = true;	
							//call me update methods
			 				
			 				saveSatelliteRandR.setProductcode(tableModel_fproducts.getValueAt(
									i, 0).toString());
			 				saveSatelliteRandR.setName(tableModel_fproducts.getValueAt(i,1).toString());
			 				saveSatelliteRandR.setTotalquantitydispensed(Double.parseDouble(tableModel_fproducts.getValueAt(i,3).toString()));
			 				saveSatelliteRandR.setTotallossesandadjustments(Double.parseDouble(tableModel_fproducts.getValueAt(i,4).toString()));
			 				saveSatelliteRandR.setPhysicalcount(Double.parseDouble(tableModel_fproducts.getValueAt(i,5).toString()));
			 				saveSatelliteRandR.setReportingdatefrom(Startdate);
			 				saveSatelliteRandR.setReportingdateto(Enddate);
			 							 							 				
			 				clickonce = true;
			 			   //Save Satellite RandR to database.
			 				satellitemapper.RecordSatelliteRandR(saveSatelliteRandR);
				 				
			 				
			 					}	
			 			}	
			 			}else{
			 				//print message qty required.
			 				
			 						 				  
			 				
			 			}
			 			
			 		}
			
					if (clickonce == true){
					JOptionPane.showMessageDialog(this, "Products saved to database",
							"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
					}
			
			}else if (productsearchenabled == true){
			
				saveSatelliteRandR = new  SatelliteRandR();
				//	System.out.println(elmisstockcontrolcardList.size());
		 		this.facilityapprovedProductsJT.editCellAt(-1,-1);
		 		int j = 0 ;
		 		for(int i = 0 ; i < this.facilityapprovedProductsJT.getRowCount();i++){
		 			
					
		 			if (this.Startdate == null) {
	 					java.util.Date date = new java.util.Date();
	 					Startdate = date;
	 				}
		 			 if (this.Enddate == null) {
	 					java.util.Date date = new java.util.Date();
	 					Enddate = date;  
	 					
	 				}
		 			if(!(this.facilityapprovedProductsJT.getValueAt(i,5).toString().equals(""))){
		 				if(!(this.facilityapprovedProductsJT.getValueAt(i,4).toString().equals(""))){
		 					if(!(this.facilityapprovedProductsJT.getValueAt(i,3).toString().equals(""))){
		 				System.out.println("HOW WBIG IS THE SELECTION IN JTABLE");
		 				persist = true;	
						//call me update methods
		 				
		 				saveSatelliteRandR.setProductcode(tableModel_searchfproducts.getValueAt(
								i, 0).toString());
		 				saveSatelliteRandR.setName(tableModel_searchfproducts.getValueAt(i,1).toString());
		 				saveSatelliteRandR.setTotalquantitydispensed(Double.parseDouble(tableModel_searchfproducts.getValueAt(i,3).toString()));
		 				saveSatelliteRandR.setTotallossesandadjustments(Double.parseDouble(tableModel_searchfproducts.getValueAt(i,4).toString()));
		 				saveSatelliteRandR.setPhysicalcount(Double.parseDouble(tableModel_searchfproducts.getValueAt(i,5).toString()));
		 				saveSatelliteRandR.setReportingdatefrom(Startdate);
		 				saveSatelliteRandR.setReportingdateto(Enddate);
		 							 							 				
		 				clickonce = true;
		 			   //Save Satellite RandR to database.
		 				satellitemapper.RecordSatelliteRandR(saveSatelliteRandR);
			 				
		 				
		 					}	
		 			}	
		 			}else{
		 				//print message qty required.
		 				
		 						 				  
		 				
		 			}
		 			
		 		}
		
				if (clickonce == true){
				JOptionPane.showMessageDialog(this, "Products saved to database",
						"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
				}
				
		
			}
			this.facilityapprovedProductsJT.validate();
			callbackARVproductlist();
		
	}

	
	public void GetLastDayOfMonth(Store_Physical_Count sccphysicalcount,Date  mydate, @SuppressWarnings("rawtypes") List<VW_Systemcalculatedproductsbalance> myproductlist,Double double1) {
		
		//Date today = new Date();  
		physicalcountupdater = new FacilityPhysicalCountDAO();
        Calendar calendar = Calendar.getInstance();  
        calendar.setTime(mydate);  
        calendar.add(Calendar.MONTH, 1);  
        calendar.set(Calendar.DAY_OF_MONTH, 1);  
        calendar.add(Calendar.DATE, -1);  
        Date lastDayOfMonth = calendar.getTime();  
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
        System.out.println("Today            : " + sdf.format(mydate));  
        System.out.println("Last Day of Month: " + sdf.format(lastDayOfMonth));  
        int comparison  = lastDayOfMonth.compareTo(mydate);
       // if( comparison == 0){
        	//then update the stock control, card balance with the Physical count
        	System.out.println("PRINT OUT MY DATES");
			//check for date and time for updating stock control card products balance
			for (@SuppressWarnings("unused")
			VW_Systemcalculatedproductsbalance p : myproductlist) {
			
				//physicalcountupdater.callstockcontrolcardupdater(createsccobject(p));
				
			}
			
			
        //}
		
	}
	
	
	private void createsccobjectsamePC( Store_Physical_Count myphyscialcountproduct){
		
		savestockcontrolcard = new Elmis_Stock_Control_Card();
		//	System.out.println(elmisstockcontrolcardList.size());
			try{				
			savestockcontrolcard.setRefno("");
			savestockcontrolcard.setId(GenerateGUID());
			savestockcontrolcard.setProductcode(myphyscialcountproduct.getProductcode());
			savestockcontrolcard.setIssueto_receivedfrom("Physical Count");
			//savestockcontrolcard.setQty_received(sp.getQty_received());

			//savestockcontrolcard.setQty_isssued(sp.getQty_isssued());
			//savestockcontrolcard.setAdjustments(sp.getAdjustments());
			savestockcontrolcard.setBalance(myphyscialcountproduct.getPhysicalcount());
			//savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
			//savestockcontrolcard.setProductid(myphyscialcountproduct..getProductid());

			savestockcontrolcard.setCreatedby(myphyscialcountproduct.getCreatedby());
			savestockcontrolcard.setCreateddate(myphyscialcountproduct.getCreateddate());
			//savestockcontrolcard.setProgram_area(myphyscialcountproduct.);
		    savestockcontrolcard.setRemark(myphyscialcountproduct.getComments());
		    
		    physicalcountupdater.callstockcontrolcardupdater(savestockcontrolcard);
			}catch(NullPointerException e){
				e.getMessage();
			}
	}
	
	private Elmis_Stock_Control_Card createsccobject( Store_Physical_Count storephysicalcount2){
	
		savestockcontrolcard = new Elmis_Stock_Control_Card();
		//	System.out.println(elmisstockcontrolcardList.size());
			try{				
			savestockcontrolcard.setRefno("");
			savestockcontrolcard.setId(GenerateGUID());
			savestockcontrolcard.setProductcode(storephysicalcount2.getProductcode());
			savestockcontrolcard.setIssueto_receivedfrom("Physical Count");
			//savestockcontrolcard.setQty_received(sp.getQty_received());

			//savestockcontrolcard.setQty_isssued(sp.getQty_isssued());
			//savestockcontrolcard.setAdjustments(sp.getAdjustments());
			savestockcontrolcard.setBalance(storephysicalcount2.getPhysicalcount());
			//savestockcontrolcard.setAdjustmenttype(sp.getAdjustmenttype());
			//savestockcontrolcard.setProductid(myphyscialcountproduct..getProductid());

			savestockcontrolcard.setCreatedby(storephysicalcount2.getCreatedby());
			savestockcontrolcard.setCreateddate(storephysicalcount2.getCreateddate());
			//savestockcontrolcard.setProgram_area(myphyscialcountproduct.);
		    savestockcontrolcard.setRemark(storephysicalcount2.getComments());
	       }catch(NullPointerException e){
				e.getMessage();
			}
		  return savestockcontrolcard;
			
	}
	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
				
		tablecolumnaligner = new TableColumnAligner();
		tablecolumnaligner.rightAlignColumn(this.facilityapprovedProductsJT,3);
		storephysicalcount = new Store_Physical_Count();
		this.facilityapprovedProductsJT.getTableHeader()
		.setFont(new Font("Ebrima", Font.PLAIN, 20));
		this.facilityapprovedProductsJT.setRowHeight(30);

		//this.ShipmentdateJDate.setFocusable(isFocusable());
		Facilityproductsmapper = new FacilityProductsDAO();
		
		//productsbalanceList = Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalances(valueA)

	     PopulateProgram_ProductTable(productsbalanceList);
		rnrid = 4;
		//this.ShipmentdateJDate.setDate(new Date());
		//this.ShipmentdateJDate.setFocusable(isFocusable());
		shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");
	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fapprovProducts) {

		try {
			callfacility = new FacilitySetUpDAO();
			Facilityproductsmapper = new FacilityProductsDAO();

			facility = callfacility.getFacility();
			System.out
					.println(facility.getCode() + "We have the facility code");
			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();

			if (fapprovProducts.size() == 0) {
				callfacility = new FacilitySetUpDAO();
				Facilityproductsmapper = new FacilityProductsDAO();

				this.facilityprogramcode = "ARV";
			

				if (!(facilityprogramcode.equals(""))) {
				
					String dbdriverStatus = System.getProperty("dbdriver");
					if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {

						productsbalanceList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
					} else {
						productsbalanceList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
					}
				}

			} else {
				productsbalanceList = fapprovProducts;
			}

		
			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = productsbalanceList.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProductsbalance = fapprovedproductsIterator.next();

			
				defaultv_facilityapprovedproducts[0] = facilitysccProductsbalance.getCode();
				defaultv_facilityapprovedproducts[1] = facilitysccProductsbalance.getPrimaryname();

				
				defaultv_facilityapprovedproducts[2] = facilitysccProductsbalance
						.getStrength();
		

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SatelliteReportandRequisitionJD dialog = new SatelliteReportandRequisitionJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton CancelJBtn;
	private javax.swing.JButton SaveJBtn;
	private javax.swing.JTable facilityapprovedProductsJT;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private JLabel JLableSearch;
	private JTextField SearchproductsJTF;
   private com.toedter.calendar.JDateChooser  StartDatedateChooser;
   private com.toedter.calendar.JDateChooser  EndDatedateChooser;
   
}