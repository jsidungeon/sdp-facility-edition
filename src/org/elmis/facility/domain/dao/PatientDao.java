package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;

public class PatientDao {
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public void updatePatient(String artNumber,String nrcNumber,Date dateOfBirth, String firstName,String lastName, String sex){
		Map<String, Object> map = new HashMap<>();
		map.put("artNumber", artNumber);
		map.put("nrcNumber",nrcNumber);
		map.put("dateOfBirth", dateOfBirth);
		map.put("firstName", firstName);
		map.put("lastName", lastName);
		map.put("sex", sex);
		SqlSession session = sqlSessionFactory.openSession();
		
		try
		{
			session.update("Patient.update_patient", map);
			session.commit();
		}
		finally{
			session.close();
		}
	}
}
