package org.elmis.forms.stores.receiving;

import java.awt.Component;
import java.sql.Date;
import java.util.Calendar;

import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import com.toedter.calendar.JDateChooser;

@SuppressWarnings("serial")
public class JDateChooserRenderer extends JDateChooser implements TableCellRenderer{
	 Date inDate;
     
     public Component getTableCellRendererComponent(JTable table, Object value,
	            boolean isSelected, boolean hasFocus, int row, int column) {
	        // TODO Auto-generated method stub

	        if (value instanceof Date){
	            this.setDate((Date) value);
	        } else if (value instanceof Calendar){
	            this.setCalendar((Calendar) value);
	        }
	        return this;
	
	
	
}
}