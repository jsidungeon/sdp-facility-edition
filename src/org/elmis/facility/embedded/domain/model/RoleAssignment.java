package org.elmis.facility.embedded.domain.model;

public class RoleAssignment {
	private int userId;
	private int roleId;
	private int programId;
	private	int supervisoryNodeId;
	private int deliveryZoneId;
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getRoleId() {
		return roleId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public int getProgramId() {
		return programId;
	}
	public void setProgramId(int programId) {
		this.programId = programId;
	}
	public int getSupervisoryNodeId() {
		return supervisoryNodeId;
	}
	public void setSupervisoryNodeId(int supervisoryNodeId) {
		this.supervisoryNodeId = supervisoryNodeId;
	}
	public int getDeliveryZoneId() {
		return deliveryZoneId;
	}
	public void setDeliveryZoneId(int deliveryZoneId) {
		this.deliveryZoneId = deliveryZoneId;
	}
}
