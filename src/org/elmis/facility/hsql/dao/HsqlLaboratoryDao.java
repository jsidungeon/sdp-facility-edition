package org.elmis.facility.hsql.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.database.connection.MyBatisHSQLConnectionFactory;
import org.elmis.facility.domain.dao.LabTestDao;
import org.elmis.facility.reporting.model.Equipment;
import org.elmis.facility.reporting.model.LabTest;

public class HsqlLaboratoryDao {

	private SqlSessionFactory sqlSessionFactory;

	{
		sqlSessionFactory = MyBatisHSQLConnectionFactory.getSqlSessionFactory();
	}

	public List<Equipment> getFacilityEquipment() {
		SqlSession session = sqlSessionFactory.openSession();

		try {
			return session.selectList("Equipment.hsql_facility_equipment");
		} finally {
			session.close();
		}
	}
	public List<LabTest> getReagents()
	{
		SqlSession session = sqlSessionFactory.openSession();
		
		try
		{
			return session.selectList("LabTest.reagents");
		}
		finally
		{
			session.close();
		}
	}
	public void copyPsqlEquipmentHsql(List<Equipment> listEquipmentFromPsql)
	{
		  
		SqlSession session = sqlSessionFactory.openSession();

		try {
			for (Equipment e : listEquipmentFromPsql)
			{
				session.insert("Equipment.psql_to_hsql", e);
				session.commit();
			}
			//
		} finally {
			session.close();
		}
		
	}
	public void copyPsqlTestReagentsHsql(List<LabTest> listReagentsFromPsql)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			for (LabTest e : listReagentsFromPsql)
			{
				session.insert("LabTest.psql_to_hsql", e);
				session.commit();
			}
			//
		} finally {
			session.close();
		}
	}
	public static void main(String...args)
	{
		/*List<Equipment> list = new EquipmentDao().getFacilityApprovedEquipment(); 
		new HsqlLaboratoryDao().copyPsqlEquipmentHsql(list);*/
		/*List<Equipment> list2 = new HsqlLaboratoryDao().getFacilityEquipment();
		System.out.println("size is HSQL "+list2.size());
		for (Equipment e : list)
			System.out.println("HSQL "+ e);*/
		/*List<LabTest> psqlLabTestReagents = new LabTestDao().getPsqlReagents();
		for (LabTest l : psqlLabTestReagents)
		{
			System.out.println(l.getReagentCode()+" --> "+l.getMachineCode());
		}
		new HsqlLaboratoryDao().copyPsqlTestReagentsHsql(psqlLabTestReagents);*/
		List<LabTest> hsqlReagentsList = new HsqlLaboratoryDao().getReagents();
		for (LabTest e : hsqlReagentsList)
			System.out.println("HSQL "+ e);
	}
}
