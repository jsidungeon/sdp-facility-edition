package org.elmis.facility.dashboard_elmis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.elmis.facility.domain.dao.NotificationDao;
import org.elmis.facility.reporting.model.Notification;
import org.elmis.facility.reports.utils.CalendarUtil;

public class NotificationTrigger {
	
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy"); 

	public static void triggerNotification(String message, int severityLevel)
	{
		Notification notification = new Notification();
		notification.setNotificationMessage(message);
		notification.setSeverityLevel(severityLevel);
		new NotificationDao().addNotification(notification);
	}
	public static int triggerSubmitNotification() throws ParseException
	{
		CalendarUtil cal = new CalendarUtil();
		String currentDate = cal.getCurrentDate();
		Date dateCurrent = sdf.parse(currentDate);
		Date deadLine = sdf.parse(cal.getSubmissionDeadLine());
		Date beginReportPeriod = sdf.parse(cal.getBeginSubmissionPeriod());
		if( dateCurrent.compareTo(beginReportPeriod) >= 0 && dateCurrent.compareTo(deadLine) <= 0){
			return cal.numberOfDaysBetweenDates(deadLine, dateCurrent);
		}
		
		return 0;
	}
	public static boolean triggerFinalDayNotification() throws ParseException
	{
		CalendarUtil cal = new CalendarUtil();
		String currentDate = cal.getCurrentDate();
		Date dateCurrent = sdf.parse(currentDate);
		Date deadLine = sdf.parse(cal.getSubmissionDeadLine());
		if (dateCurrent.compareTo(deadLine) == 0)
			return true;
		return false;
	}
}
