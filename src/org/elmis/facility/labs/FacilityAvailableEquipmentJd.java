package org.elmis.facility.labs;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.swing.AbstractButton;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.elmis.facility.domain.dao.EquipmentDao;
import org.elmis.facility.reporting.model.Equipment;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.elmis.facility.utils.ElmisAESencrpDecrp;

public class FacilityAvailableEquipmentJd extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private JTable table;
	private TableColumnAligner colAligner = new TableColumnAligner();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Properties prop = new Properties(System.getProperties());
			FileInputStream fis = null;
			try {
				fis = new FileInputStream("Programmproperties.properties");
				prop.load(fis);

				System.setProperties(prop);

				fis.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
			String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
			System.setProperty("dbpassword", dbpassword);
			FacilityAvailableEquipmentJd dialog = new FacilityAvailableEquipmentJd(new javax.swing.JFrame(), true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FacilityAvailableEquipmentJd(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setTitle("Managed Laboratory Equipment");
		setIconImage(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/appicon.png")).getImage());
		setBounds(100, 100, 841, 505);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setBackground(new java.awt.Color(102, 102, 102));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel panel = new JPanel();
			panel.setBackground(new java.awt.Color(102, 102, 102));
			panel.setBounds(0, 0, 825, 400);
			contentPanel.add(panel);
			panel.setLayout(null);
			{
				JLabel titleLbl = new JLabel("Managed Laboratory Equipment");
				titleLbl.setFont(new Font("Tahoma", Font.BOLD, 13));
				titleLbl.setForeground(new java.awt.Color(255, 255, 255));
				titleLbl.setBounds(88, 11, 208, 14);
				panel.add(titleLbl);
			}
			
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(88, 36, 673, 353);
			panel.add(scrollPane);
			List<Equipment> equipmentList = new EquipmentDao().getFacilityEquipment();
			table = new JTable();
			table.setFont(new java.awt.Font("Ebrima", 0, 14));
			table.setModel(tableModel);
			JCheckBox equipAvailabilityStatus = new JCheckBox();
			equipAvailabilityStatus.addActionListener(this);
			table.getColumn("<html><b>Equipment<br>available</b></html>").setCellEditor(new DefaultCellEditor(equipAvailabilityStatus));
			for(int i = 0; i< equipmentList.size(); i++)
			{
				tableModel.addRow(new String[equipmentList.size()-1]);
				table.getModel().setValueAt(equipmentList.get(i).getEquipCode().trim(), i, 0);
				table.getModel().setValueAt(equipmentList.get(i).getName().trim(), i, 1);

				if (equipmentList.get(i).isEquipAvailable())
					table.getModel().setValueAt(new Boolean(true), i, 2);
				else
					table.getModel().setValueAt(new Boolean(false), i, 2);
			}
			
			table.setFillsViewportHeight(true);
			table.setCellSelectionEnabled(true);
			table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); 
			table.getColumnModel().getColumn(0).setPreferredWidth(90);
			table.getColumnModel().getColumn(1).setPreferredWidth(425);
			table.getColumnModel().getColumn(2).setPreferredWidth(90);
			table.getTableHeader().setReorderingAllowed(false);
			table.getTableHeader().setPreferredSize(new Dimension(30, 45));
			table.setRowHeight(35);
			colAligner.centreAlignColumn(table, 0);
			colAligner.centreAlignColumn(table, 1);
			scrollPane.setViewportView(table);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new java.awt.Color(102, 102, 102));
			buttonPane.setBounds(0, 400, 825, 56);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(null);
			{
				JButton cancelButton = new JButton("Close");
				cancelButton.setFont(new java.awt.Font("Ebrima", 1, 12));
				cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Cancel.png")));
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				cancelButton.setBounds(659, 11, 103, 34);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		setLocationRelativeTo(parent);
	}
	private String[][] data = null;
	private String[] columnNames = { "<HTML><b>Equipment<br>Code</b></html>","<html><b>Equipment Name</b></html>",
			"<html><b>Equipment<br>available</b></html>"};
	private DefaultTableModel tableModel = new DefaultTableModel(data, columnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {
			return (column == 2);
		}
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2)
			return Boolean.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};
	@Override
	public void actionPerformed(ActionEvent e) {
		int selectedRow = table.getSelectedRow();
		String facilityMachineName = ""+table.getValueAt(selectedRow, 1);
		String facilityMachineCode = ""+table.getValueAt(selectedRow, 0);
		
		AbstractButton abstractButton = (AbstractButton) e.getSource();
		boolean selected = abstractButton.getModel().isSelected();
		String str = "";
		String str2 = "";
		if(selected){
			str = "add";
			str2 = "to";
		}
		else {
			str = "remove";
			str2 = "from";
		}
		int ack = JOptionPane.showConfirmDialog(FacilityAvailableEquipmentJd.this, "Do you want to "+str+" "+facilityMachineName+" "+str2+ " list of facility managed equipment?", facilityMachineCode, JOptionPane.YES_NO_OPTION);
		if (ack == JOptionPane.YES_OPTION)
		{
			new EquipmentDao().changeAvailabilityStatus(facilityMachineCode, selected);
		}
		else
			table.setValueAt((!selected), selectedRow, 2);
	}
}
