package org.elmis.facility.dashboard.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class TakeNotes {

	//private String fileName;
	private Charset charset = Charset.forName("US-ASCII");
	private Path pathToFileName;
	
	public TakeNotes(String fileName)
	{
		pathToFileName = Paths.get(fileName);
	}
	public void saveNotes(StringBuilder notes)
	{	
		try
		(
				BufferedWriter bw = Files.newBufferedWriter(pathToFileName, charset);
				)
		{
			bw.write("");
			bw.append(notes.toString()+"\n", 0, notes.length());
		}
		catch(IOException e)
		{
			System.out.println("Some error while saving to Notes"+e.getMessage());
		}
	}
	public void clearNotes()
	{
		try
		(
				BufferedWriter bw = Files.newBufferedWriter(pathToFileName, charset);
				)
		{
			bw.write("");
		}
		catch(IOException e)
		{
			System.out.println("Some error while clearing the Notes"+e.getMessage());
		}
	}
	public String retrieveNotes()
	{
		StringBuffer sBuffer = new StringBuffer();
		try
		(
			BufferedReader br = Files.newBufferedReader(pathToFileName, charset);
				)
		{
			String line = null;
			while((line = br.readLine()) != null)
			{
				sBuffer.append(line+"\n");
			}
		}
		catch(IOException e)
		{
			System.out.println("Some error while retrieving Notes"+e.getMessage());
		}
		return sBuffer.toString();
	}
}
