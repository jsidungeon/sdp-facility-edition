package org.elmis.facility.database.connection;

public enum DBType {

	MYSQL, HSQL, ORACLE, MSQL, POSTGRESQL;
}
