/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.json;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * @author MMwebaze
 *
 */
public class JSONService {

	public void postEmlip(String emlipString)
	{
		try {

			Client client = Client.create();

			WebResource webResource = client
					.resource("http://localhost:8181/RestServices/rest/hello/post");


			ClientResponse response = webResource.type("text/plain")
					.post(ClientResponse.class, emlipString);

			if (response.getStatus() != 201) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			System.out.println("Output from Server .... \n");
			String output = response.getEntity(String.class);
			System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	}
	public static void main(String[] args)
	{
		String emlipString = "{\"singer\":\"Metallica\",\"title\":\"Fade To Black\"}";
		new JSONService().postEmlip(emlipString);
	}
}
