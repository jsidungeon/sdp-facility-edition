package org.elmis.facility.setup;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListSelectionModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import org.elmis.facility.domain.dao.FacilityDao;
import org.elmis.facility.domain.dao.GeographicZoneDao;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.GeographicZone;
import org.elmis.facility.utils.SdpStyleSheet;

public class SecondPanel extends JPanel {
	private String[] provinceArray;
	private Map<String, Object> facilityDetails = new HashMap<>();
	private String province;
	private List<Facility> facilityList;
	private int facilityTypeId;
	private int selectedRow;
	private String district;
	private String facility;
	private String facilityCode;
	private List<GeographicZone> districtList;
	private Map<String, String> provinceMap = new HashMap<>();
	private Map<String, Integer> districtMap = new HashMap<>();
	private JTable facilitiesJTable;
	private String[] facilityColumnNames = { "<HTML><b>Code</b></html>","<HTML><b>Facilitly</b></html>"};
	private String[][] data = null;
	private JComboBox provinceJCombobox;
	private JComboBox districtComboBox;
	String[] columns_districts = { "Districts" };
	private DefaultTableModel facilityTableModel = new DefaultTableModel(data,facilityColumnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {
				return false;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2)
				return Integer.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};

	/**
	 * Create the panel.
	 */
	public SecondPanel() {
		SdpStyleSheet.configJPanelBackground(this);
		setLayout(null);
		GeographicZoneDao geoZoneDao = new GeographicZoneDao();
		List<GeographicZone> provinceList = geoZoneDao.getGeographicZones();
		provinceArray = new String[provinceList.size()+1];
		provinceArray[0] = "Select a province";
		for (int i = 1; i < provinceArray.length; i++){
			provinceArray[i] = provinceList.get(i -1).getGeographicZoneName();
			provinceMap.put(provinceList.get(i-1).getGeographicZoneName(), provinceList.get(i-1).getCode());
		}
		
		JLabel lblSetupFacility = new JLabel("Facility Setup");
		SdpStyleSheet.configTitleJLabel(lblSetupFacility);
		lblSetupFacility.setBounds(10, 11, 162, 14);
		add(lblSetupFacility);
		
		provinceJCombobox = new JComboBox();
		provinceJCombobox.setModel(new javax.swing.DefaultComboBoxModel(provinceArray));
		provinceJCombobox.setBounds(10, 56, 179, 39);
		add(provinceJCombobox);
		
		districtComboBox = new JComboBox();
		districtComboBox.setBounds(10, 118, 179, 39);
		districtComboBox.setEnabled(false);
		provinceJCombobox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				provinceComboBox1ItemStateChanged(evt);
			}
		});
		districtComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				districtComboBox1ItemStateChanged(evt);
			}
		});
		add(districtComboBox);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 178, 507, 167);
		add(scrollPane);
		
		facilitiesJTable = new JTable();
		facilitiesJTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		facilitiesJTable.getTableHeader().setReorderingAllowed(false);
		facilitiesJTable.setSelectionModel(new SingleListSelectionModel());
		facilitiesJTable.addMouseListener(new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e) {
				if (e.getClickCount() == 1) {
					selectedRow = facilitiesJTable.getSelectedRow();
					facilityCode = ""+facilitiesJTable.getValueAt(selectedRow, 0);
					facility = ""+facilitiesJTable.getValueAt(selectedRow, 1);
					facilityDetails.put("facility", facility);
					facilityDetails.put("facilityCode", facilityCode);
					facilityDetails.put("facilityTypeId", facilityList.get(selectedRow).getTypeid());
					facilityDetails.put("facilityId", facilityList.get(selectedRow).getId());
					
					//ElmisProperties.saveProperties(province, district, facility);
				}
			}
		});
		SdpStyleSheet.configTable(facilitiesJTable);
		scrollPane.setViewportView(facilitiesJTable);

	}
	private void provinceComboBox1ItemStateChanged(java.awt.event.ItemEvent evt)
	{
		 if (java.awt.event.ItemEvent.SELECTED == evt.getStateChange()) {

			String selectedProvince = evt.getItem().toString();
			int x = provinceJCombobox.getSelectedIndex();
			if (x != 0){
				//province = selectedProvince;
				facilityDetails.put("province", selectedProvince);
				districtComboBox.setEnabled(true);
				districtComboBox.removeAllItems();
				GeographicZoneDao geoZoneDao = new GeographicZoneDao();
				districtList = geoZoneDao.getGeographicZones(provinceMap.get(selectedProvince));
				if(districtList.size() != 0)
					districtComboBox.addItem("Select district");
				for (GeographicZone g : districtList)
					districtComboBox.addItem(g.getGeographicZoneName());
			}
			else
			{
				districtComboBox.removeAllItems();
				districtComboBox.setEnabled(false);
			}
			
		}
	}
	private void districtComboBox1ItemStateChanged(java.awt.event.ItemEvent evt)
	{
		if (java.awt.event.ItemEvent.SELECTED == evt.getStateChange()) {
			String selectedDistrict = evt.getItem().toString();
			int x = districtComboBox.getSelectedIndex();
			if (x != 0){
				int geoZoneId = 0;
				if (facilitiesJTable.getRowCount() != 0)
					((DefaultTableModel)facilitiesJTable.getModel()).setRowCount(0);
				//district = selectedDistrict;
				facilityDetails.put("district", selectedDistrict);
				for (GeographicZone g : districtList)
				{
					if (g.getGeographicZoneName().equalsIgnoreCase(selectedDistrict))
						geoZoneId = g.getId();
				}
				facilityList = new FacilityDao().getFacilities(geoZoneId);
				Collections.sort(facilityList);
				facilitiesJTable.setModel(facilityTableModel);
				facilitiesJTable.getColumnModel().getColumn(0).setPreferredWidth(80);
				facilitiesJTable.getColumnModel().getColumn(1).setPreferredWidth(400);
				for (int i = 0; i < facilityList.size(); i++){
					facilityTableModel.addRow(new String[facilityList.size()]);
					facilitiesJTable.getModel().setValueAt(facilityList.get(i).getCode().trim(), i, 0);
					facilitiesJTable.getModel().setValueAt(facilityList.get(i).getName().trim(), i, 1);
				}
			}
			else
			{
				if (facilitiesJTable.getRowCount() != 0)
					((DefaultTableModel)facilitiesJTable.getModel()).setRowCount(0);
			}
		}
	}
	public class SingleListSelectionModel extends DefaultListSelectionModel {

	    public SingleListSelectionModel () {
	        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	    }

	    @Override
	    public void clearSelection() {
	    }

	    @Override
	    public void removeSelectionInterval(int index0, int index1) {
	    }
	}
	public Map<String, Object> getFacilityDetails()
	{
		return facilityDetails;
	}
}
