package org.elmis.facility.reporting.model;

public class RegimenCategory {
	
	private int regimenCategoryId;
	private String regimenCategoryName;
	
	@Override
	public String toString() {
		return regimenCategoryName;
	}
	public int getRegimenCategoryId() {
		return regimenCategoryId;
	}
	public void setRegimenCategoryId(int regimenCategoryId) {
		this.regimenCategoryId = regimenCategoryId;
	}
	public String getRegimenCategoryName() {
		return regimenCategoryName;
	}
	public void setRegimenCategoryName(String regimenCategoryName) {
		this.regimenCategoryName = regimenCategoryName;
	}

}
