package org.elmis.facility.main.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;

import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import net.sf.jasperreports.swing.JRViewer;

public class SCCReportDialogue extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SCCReportDialogue dialog = new SCCReportDialogue(null, null, true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SCCReportDialogue(JRViewer jrViwer,java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setTitle("Regimen Report");
		
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim = toolkit.getScreenSize();
		int x = (int)(0.05 * dim.getWidth());
		int y = (int)(0.05 * dim.getHeight()); 
		int width = (int)(0.9 * dim.getWidth());
		int height = (int)(0.9 * dim.getHeight());  
		setBounds(x, y,  width , height);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(jrViwer, BorderLayout.CENTER);
	}
}
	