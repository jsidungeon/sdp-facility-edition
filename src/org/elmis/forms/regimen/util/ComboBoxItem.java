package org.elmis.forms.regimen.util;

import org.elmis.facility.domain.model.RegimenProductDosage;
import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.ElmisRegimenCategory;

public class ComboBoxItem {
	public ComboBoxItem(Object obj){
		this.value = obj;
	}
	private Object value;
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
	
	public String toString(){
		if(value instanceof ElmisRegimenCategory){
			return ((ElmisRegimenCategory)value).getName();
		}
		else if (value instanceof ElmisRegimen){
			return ((ElmisRegimen)value).getName();
		}
		else if (value instanceof RegimenProductDosage){
			return ((RegimenProductDosage)value).getName();
		}
		return null;
	}

}
