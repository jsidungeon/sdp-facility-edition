package org.elmis.facility.reports.regimen;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.*;

import org.elmis.forms.regimen.dao.RegimenDao;

@SuppressWarnings("unused")
public class RegimenReportData {
	public RegimenReportData() {};
	private List<RegimenLine> regimenLines;
	private Long numberOfPatients = 0L;
	private List<RegimenReportDataRaw> regimenStat;

	public void populateRegimenReport(Date startDate,Date endDate){
		RegimenDao dao = RegimenDao.getInstance();
		regimenLines = new ArrayList<>();
		List<RegimenReportBean> reportBeans = dao.fetchRegimenReportBeans(startDate, endDate);
		Long totalNoOfPatients = 0L;
		for (RegimenReportBean regimenReportBean : reportBeans) {
			totalNoOfPatients += regimenReportBean.getPatientsOnRegimen();
		}
		RegimenLine regLine = null;
		for (RegimenReportBean regimenReportBean : reportBeans) {
//			System.out.print(regimenReportBean.getRegimenLineId()+ ",");
//			System.out.println((regLine!=null)?regLine.getRegimenLineId():null);
			if(regLine == null || regimenReportBean.getRegimenLineId() != regLine.getRegimenLineId()){
				regLine = new RegimenLine();
				regLine.setRegimenLineId(regimenReportBean.getRegimenLineId());
				regLine.setRegimenLineName(regimenReportBean.getRegimenLineName());
				regimenLines.add(regLine);
			} 
			regLine.addReportBean(regimenReportBean);
			regLine.setNumberOfPatients(regLine.getNumberOfPatients() + regimenReportBean.getPatientsOnRegimen());
		}
		this.setRegimenLines(regimenLines);
		
		for (RegimenLine regimenLine : this.getRegimenLines()) {
			for (RegimenReportBean regimenReportBean : regimenLine.getRegimenReportBeans()) {
				regimenReportBean.setPercentageOutOfLine((regimenReportBean.getPatientsOnRegimen()/Double.valueOf(regimenLine.getNumberOfPatients()))*100);
				regimenReportBean.setPercentageOutOfLine(Math.round(regimenReportBean.getPercentageOutOfLine() * 100.0) / 100.0);
				regimenReportBean.setPercentageOutOfTotalPatients((regimenReportBean.getPatientsOnRegimen()/Double.valueOf(totalNoOfPatients))*100);
				regimenReportBean.setPercentageOutOfTotalPatients(Math.round(regimenReportBean.getPercentageOutOfTotalPatients() * 100.0) / 100.0);
			}
		}
	}
	
	public void printRegimenReportDate(){

		for (RegimenLine regLine : this.regimenLines) {
			System.out.println(regLine.getRegimenLineName());
			for (RegimenReportBean bean : regLine.getRegimenReportBeans()) {
				System.out.println("\t"+ bean.getRegimenName() +" , " +bean.getPatientsOnRegimen());
			}
		}
	}
	
	public Long getNumberOfPatients() {
		return numberOfPatients;
	}
	public void setNumberOfPatients(Long numberOfPatients) {
		this.numberOfPatients = numberOfPatients;
	}
	
	public List<RegimenLine> getRegimenLines() {
		return regimenLines;
	}
	
	public void setRegimenLines(List<RegimenLine> regimenLines) {
		this.regimenLines = regimenLines;
	}
	
	public List<RegimenReportDataRaw> getRegimenStat() {
		return new ArrayList<RegimenReportDataRaw>();
	}
	
	public List<RegimenReportDataRaw> setRegimenStat(){
		return new ArrayList<RegimenReportDataRaw>();
	}
	
	public void setRegimenStat(List<RegimenReportDataRaw> regimenStat) {
		this.regimenStat = regimenStat;
	}
	
	public static List<RegimenReportDataRaw> createBeanCollection(){
		return new ArrayList<RegimenReportDataRaw>();
	}
	
	public static void main(String args[]){
		RegimenDao.setSystemProperties();
		RegimenReportData reportData =new RegimenReportData();
		reportData.populateRegimenReport(new Date(), new Date());
		reportData.printRegimenReportDate();
	}
}
