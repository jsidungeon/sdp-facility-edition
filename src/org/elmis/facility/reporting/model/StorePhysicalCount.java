/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.model;

/**
 * @author MMwebaze
 *
 */
public class StorePhysicalCount {

	private String productCode;
	private int previousCount;
	private int currentCount;
	private String physicalCountDate;
	
	public String getPhysicalCountDate() {
		return physicalCountDate;
	}
	public void setPhysicalCountDate(String physicalCountDate) {
		this.physicalCountDate = physicalCountDate;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public int getPreviousCount() {
		return previousCount;
	}
	public void setPreviousCount(int previousCount) {
		this.previousCount = previousCount;
	}
	public int getCurrentCount() {
		return currentCount;
	}
	public void setCurrentCount(int currentCount) {
		this.currentCount = currentCount;
	}
}
