package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.Equipment;

public class EquipmentDao {

	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<Equipment> getEquipment()
	{
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("Equipment.get_equipment");
		}
		finally
		{
			session.close();
		}
	}
	public List<Equipment> getFacilityEquipment()
	{
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("Equipment.facility_equipment");
		}
		finally
		{
			session.close();
		}
	}
	public List<Equipment> getFacilityApprovedEquipment()
	{
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("Equipment.get_sdp_approved_equip");
		}
		finally
		{
			session.close();
		}
	}
	public void changeFunctionalityStatus(@Param("equipmentCode")String equipmentCode, @Param("newStatus") boolean newStatus)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("equipmentCode", equipmentCode);
		map.put("newStatus", newStatus);
		
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			session.update("Equipment.change_functionality_status", map);
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public void changeAvailabilityStatus(@Param("equipmentCode")String equipmentCode, @Param("newStatus") boolean newStatus)
	{
		Map<String, Object> map = new HashMap<>();
		map.put("equipmentCode", equipmentCode);
		map.put("newStatus", newStatus);
		
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			session.update("Equipment.change_availability_status", map);
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public static void main(String...arf)
	{
		List<Equipment> list = new EquipmentDao().getFacilityApprovedEquipment();
		for(Equipment q: list)
			System.out.println(q);
	}
	public void changeReagentTestcountStatus(@Param("reagentCode")String reagentCode, @Param("newStatus") boolean newStatus) {
		Map<String, Object> map = new HashMap<>();
		map.put("reagentCode", reagentCode);
		map.put("newStatus", newStatus);
		
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			session.update("Equipment.change_testcount_status", map);
			session.commit();
		}
		finally
		{
			session.close();
		}
		
	}
}
