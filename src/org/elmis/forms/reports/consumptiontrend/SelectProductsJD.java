/*
 * SelectProductsJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.reports.consumptiontrend;

import java.awt.event.KeyEvent;
import java.util.LinkedList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.reports.stocktrends.TimeSeriesChart;
import org.elmis.facility.reports.stocktrends.TimeSeriesJD;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author  __USER__
 */
public class SelectProductsJD extends javax.swing.JDialog {

	private String searchText = "";
	private char letter;
	private List<Character> charList = new LinkedList();
	public static String barcodeScanned;
	public static int selectedProductID;
	public static Products selectedProduct;

	private List<Products> productsList = new LinkedList();

	/** Creates new form SelectProductsJD */
	public SelectProductsJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(500, 450);
		this.setLocationRelativeTo(null);

		jTabbedPane1.addTab("Program", new ProgramsJP());

		//dont show this 
		barcodeJTF.setVisible(false);
		searchJL.setVisible(false);

		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		searchJL = new javax.swing.JLabel();
		barcodeJTF = new javax.swing.JTextField();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Consumption Trend");

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});

		searchJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/SEARCH.PNG"))); // NOI18N
		searchJL.setText("Search");

		barcodeJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				barcodeJTFKeyReleased(evt);
			}

			public void keyTyped(java.awt.event.KeyEvent evt) {
				barcodeJTFKeyTyped(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 403,
				Short.MAX_VALUE).addGroup(
				layout.createSequentialGroup().addGap(26, 26, 26).addComponent(
						searchJL).addPreferredGap(
						javax.swing.LayoutStyle.ComponentPlacement.RELATED)
						.addComponent(barcodeJTF,
								javax.swing.GroupLayout.DEFAULT_SIZE, 287,
								Short.MAX_VALUE).addGap(33, 33, 33)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addComponent(
												jTabbedPane1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												374,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(searchJL)
														.addComponent(
																barcodeJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(15, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void barcodeJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
			searchText = barcodeJTF.getText();

			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}
			for (char c : charList) {

				s.append(c);

			}

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				System.out.println("Delete " + s);

				this.productsList = session.selectList(
						"selectProductBySearchTerm", s.toString());

				ProductsJP.populateProductsTable(this.productsList);

			} finally {
				session.close();
			}

		} else {

			// searchText += Character.toString(letter);
			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				System.out.println(s.toString());

				this.productsList = session.selectList(
						"selectProductBySearchTerm", s.toString());

				ProductsJP.populateProductsTable(this.productsList);

			} finally {
				session.close();
			}

		}
	}

	private void barcodeJTFKeyReleased(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:

		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			// selectedProduct = new Products();

			// IssuingJD.selectedProductID = 1;
			// IssuingJD.selectedProduct.setPrimaryname("Scanned Item Barcode :"
			// + searchJFT.getText());

			// IssuingJD.jTabbedPane1.addTab("Quantity", new IssueQtyJP());
			// IssuingJD.jTabbedPane1.setSelectedIndex(1);

			this.barcodeScanned = barcodeJTF.getText();

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				this.selectedProduct = session.selectOne("getByProductBarcode",
						this.barcodeScanned);// ("getProgramesSupported");

				// System.out.println(this.products.getPrimaryname()
				// +" "+IssuingJD.selectedProductID);

				// this.populateProgramsTable(this.programsList);

			} finally {
				session.close();
			}

			if (this.selectedProduct != null) {

				// IssuingJD.jTabbedPane1.addTab("Quantity", new IssueQtyJP());
				// IssuingJD.jTabbedPane1.setSelectedIndex(3);

				// show the number pad to get quantity
				//	new NumberPadJD(javax.swing.JOptionPane
				//		.getFrameForComponent(this), true);
				
				
				JFreeChart chartIn = new TimeSeriesChart().createChart("Consumption Trend for "+ this.selectedProduct.getPrimaryname() ,this.selectedProduct.getId() );
				
//				AppJFrame.glassPane.activate(null);

				new TimeSeriesJD(javax.swing.JOptionPane.getFrameForComponent(this),
						true, chartIn);

				barcodeJTF.setText("");

				if (barcodeJTF.getText().equals("")) {

					charList.clear();
				}
				barcodeJTF.requestFocusInWindow();

			}
			barcodeJTF.setText("");

		}
	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getSelectedIndex());

		if (jTabbedPane1.getSelectedIndex() == 1
				&& jTabbedPane1.getTabCount() == 3) {// &&
			// jTabbedPane1.getSelectedIndex()
			// ==2){

			// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getTabCount());

			jTabbedPane1.remove(2);
		}

		if (jTabbedPane1.getSelectedIndex() == 0
				&& jTabbedPane1.getTabCount() == 2) {// &&

			//remove the products JTable
			jTabbedPane1.remove(1);

		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				SelectProductsJD dialog = new SelectProductsJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	public static javax.swing.JTextField barcodeJTF;
	public static javax.swing.JTabbedPane jTabbedPane1;
	public static javax.swing.JLabel searchJL;
	// End of variables declaration//GEN-END:variables

}