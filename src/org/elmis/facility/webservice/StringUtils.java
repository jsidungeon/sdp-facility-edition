package org.elmis.facility.webservice;

/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/2/13
 * Time: 10:13 PM
 */
public class StringUtils {

    public static String padRight(String s, int n) {
        return String.format("%-" + n + "s", s).substring(0,n -1 );
    }

    public static String padLeft(String s, int n) {
        return String.format("%" + n + "s", s).substring(0,n - 1);
    }
}
