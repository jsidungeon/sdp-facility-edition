package org.elmis.facility.domain.model;

import java.util.Date;

public class users {
	@Override
	public String toString() {
		return "users [id=" + id + ", username=" + username + ", password="
				+ password + ", firstname=" + firstname + ", lastname="
				+ lastname + ", employeeid=" + employeeid + ", jobtitle="
				+ jobtitle + ", primarynotificationmethod="
				+ primarynotificationmethod + ", officephone=" + officephone
				+ ", cellphone=" + cellphone + ", email=" + email
				+ ", supervisorid=" + supervisorid + ", facilityid="
				+ facilityid + ", active=" + active + ", modifiedby="
				+ modifiedby + ", modifieddate=" + modifieddate
				+ ", createdby=" + createdby + ", createddate=" + createddate
				+ ", vendorid=" + vendorid + "]";
	}

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.id
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Integer id;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.username
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String username;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.password
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String password;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.firstname
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String firstname;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.lastname
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String lastname;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.employeeid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String employeeid;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.jobtitle
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String jobtitle;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.primarynotificationmethod
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String primarynotificationmethod;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.officephone
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String officephone;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.cellphone
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String cellphone;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.email
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private String email;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.supervisorid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Integer supervisorid;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.facilityid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Integer facilityid;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.active
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Boolean active;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.modifiedby
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Integer modifiedby;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.modifieddate
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Date modifieddate;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.createdby
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Integer createdby;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.createddate
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Date createddate;

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column users.vendorid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	private Integer vendorid;
	
	private String description;
	private String primary_name;
	private String purpose;
	private String site_role;
	private String name;
	private String dispensingPoint;
	

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.active
	 * 
	 * @return the value of users.active
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.cellphone
	 * 
	 * @return the value of users.cellphone
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getCellphone() {
		return cellphone;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.createdby
	 * 
	 * @return the value of users.createdby
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Integer getCreatedby() {
		return createdby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.createddate
	 * 
	 * @return the value of users.createddate
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Date getCreateddate() {
		return createddate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.email
	 * 
	 * @return the value of users.email
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.employeeid
	 * 
	 * @return the value of users.employeeid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getEmployeeid() {
		return employeeid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.facilityid
	 * 
	 * @return the value of users.facilityid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Integer getFacilityid() {
		return facilityid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.firstname
	 * 
	 * @return the value of users.firstname
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.id
	 * 
	 * @return the value of users.id
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.jobtitle
	 * 
	 * @return the value of users.jobtitle
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getJobtitle() {
		return jobtitle;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.lastname
	 * 
	 * @return the value of users.lastname
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.modifiedby
	 * 
	 * @return the value of users.modifiedby
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Integer getModifiedby() {
		return modifiedby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.modifieddate
	 * 
	 * @return the value of users.modifieddate
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Date getModifieddate() {
		return modifieddate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.officephone
	 * 
	 * @return the value of users.officephone
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getOfficephone() {
		return officephone;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.password
	 * 
	 * @return the value of users.password
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.primarynotificationmethod
	 * 
	 * @return the value of users.primarynotificationmethod
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getPrimarynotificationmethod() {
		return primarynotificationmethod;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.supervisorid
	 * 
	 * @return the value of users.supervisorid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Integer getSupervisorid() {
		return supervisorid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.username
	 * 
	 * @return the value of users.username
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column users.vendorid
	 * 
	 * @return the value of users.vendorid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public Integer getVendorid() {
		return vendorid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.active
	 * 
	 * @param active
	 *            the value for users.active
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.cellphone
	 * 
	 * @param cellphone
	 *            the value for users.cellphone
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone == null ? null : cellphone.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.createdby
	 * 
	 * @param createdby
	 *            the value for users.createdby
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setCreatedby(Integer createdby) {
		this.createdby = createdby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.createddate
	 * 
	 * @param createddate
	 *            the value for users.createddate
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setCreateddate(Date createddate) {
		this.createddate = createddate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.email
	 * 
	 * @param email
	 *            the value for users.email
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setEmail(String email) {
		this.email = email == null ? null : email.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.employeeid
	 * 
	 * @param employeeid
	 *            the value for users.employeeid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setEmployeeid(String employeeid) {
		this.employeeid = employeeid == null ? null : employeeid.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.facilityid
	 * 
	 * @param facilityid
	 *            the value for users.facilityid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setFacilityid(Integer facilityid) {
		this.facilityid = facilityid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.firstname
	 * 
	 * @param firstname
	 *            the value for users.firstname
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname == null ? null : firstname.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.id
	 * 
	 * @param id
	 *            the value for users.id
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.jobtitle
	 * 
	 * @param jobtitle
	 *            the value for users.jobtitle
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle == null ? null : jobtitle.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.lastname
	 * 
	 * @param lastname
	 *            the value for users.lastname
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname == null ? null : lastname.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.modifiedby
	 * 
	 * @param modifiedby
	 *            the value for users.modifiedby
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setModifiedby(Integer modifiedby) {
		this.modifiedby = modifiedby;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.modifieddate
	 * 
	 * @param modifieddate
	 *            the value for users.modifieddate
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setModifieddate(Date modifieddate) {
		this.modifieddate = modifieddate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.officephone
	 * 
	 * @param officephone
	 *            the value for users.officephone
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setOfficephone(String officephone) {
		this.officephone = officephone == null ? null : officephone.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.password
	 * 
	 * @param password
	 *            the value for users.password
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.primarynotificationmethod
	 * 
	 * @param primarynotificationmethod
	 *            the value for users.primarynotificationmethod
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setPrimarynotificationmethod(String primarynotificationmethod) {
		this.primarynotificationmethod = primarynotificationmethod == null ? null
				: primarynotificationmethod.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.supervisorid
	 * 
	 * @param supervisorid
	 *            the value for users.supervisorid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setSupervisorid(Integer supervisorid) {
		this.supervisorid = supervisorid;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.username
	 * 
	 * @param username
	 *            the value for users.username
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column users.vendorid
	 * 
	 * @param vendorid
	 *            the value for users.vendorid
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	public void setVendorid(Integer vendorid) {
		this.vendorid = vendorid;
	}
	
	 public String getDescription() {
	        return description;
	    }

	    
	    public void setDescription(String description) {
	        this.description = description == null ? null : description.trim();
	    }
	    public String getprimary_name() {
	        return purpose;
	    }
	    public void setprimary_name(String primary_name) {
	        this.primary_name = primary_name == null ? null : primary_name.trim();
	    }
	    
	    public String getpurpose() {
	        return purpose;
	    }

	    
	    public void setpurpose(String purpose) {
	        this.purpose = purpose == null ? null : purpose.trim();
	    }
	    
	    public String getsite_role() {
	        return site_role;
	    }

	    
	    public void setsite_role(String site_role) {
	        this.site_role = site_role == null ? null : site_role.trim();
	    }
	    
	    public String getname() {
	        return name;
	    }

	    
	    public void setname(String name) {
	        this.name = name == null ? null : name.trim();
	    }
	    
	    public String getdispensingPoint() {
	        return name;
	    }

	    
	    public void setdispensingPoint(String dispensingPoint) {
	        this.dispensingPoint = dispensingPoint == null ? null : dispensingPoint.trim();
	    }

}