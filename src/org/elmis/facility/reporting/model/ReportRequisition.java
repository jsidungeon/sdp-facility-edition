package org.elmis.facility.reporting.model;

import java.sql.Date;


public class ReportRequisition {
	@Override
	public String toString() {
		return " code "+productCode+" name "+drugProduct+" Begin "+beginningBal+" dispensed "+qtyDispensed+" phy count "+phyCount+" test "+typeTest+" amc "+amc;
	}
	private int id;
	private double currentPrice;
	public double getCurrentPrice() {
		return currentPrice;
	}
	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}
	private String regimenId;
	private String drugProduct;
	private String unit;
	private int beginningBal;
	private int totalReceived;
	private int qtyDispensed;
	private int lossAdjustment;
	private int phyCount;
	private double amc;
	private int maxQty;
	private int orderQty;
	private Date reportPeriodFrm;
	private Date reportPeriodTo;
	private String province;
	private String district;
	private String facility;
	private String explnLossAdj;
	private int reportMonth;
	private int reportYear;
	private String typeTest;
	private int daysStockedOut;
	private String productCode;
	private String emergencyOrder;
	private double emergencyOrderPoint;
	private int maxStockLevel;
	private int reportStatus;
	private int programCode;
	private String dateOfSubmission;
	private String productCategory;
	private String remarks;

	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}
	private String labMachine;

	public String getDateOfSubmission() {
		return dateOfSubmission;
	}
	public void setDateOfSubmission(String dateOfSubmission) {
		this.dateOfSubmission = dateOfSubmission;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getReportYear() {
		return reportYear;
	}
	public void setReportYear(int reportYear) {
		this.reportYear = reportYear;
	}
	public int getReportMonth() {
		return reportMonth;
	}
	public void setReportMonth(int reportMonth) {
		this.reportMonth = reportMonth;
	}
	
	public Date getReportPeriodFrm() {
		return reportPeriodFrm;
	}
	public void setReportPeriodFrm(Date reportPeriodFrm) {
		this.reportPeriodFrm = reportPeriodFrm;
	}
	public Date getReportPeriodTo() {
		return reportPeriodTo;
	}
	public void setReportPeriodTo(Date reportPeriodTo) {
		this.reportPeriodTo = reportPeriodTo;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getFacility() {
		return facility;
	}
	public void setFacility(String facility) {
		this.facility = facility;
	}
	public String getExplnLossAdj() {
		return explnLossAdj;
	}
	public void setExplnLossAdj(String explnLossAdj) {
		this.explnLossAdj = explnLossAdj;
	}
	public String getRegimenId() {
		return regimenId;
	}
	public void setRegimenId(String regimenId) {
		this.regimenId = regimenId;
	}

	public String getDrugProduct() {
		return drugProduct;
	}
	public void setDrugProduct(String drugProduct) {
		this.drugProduct = drugProduct;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getBeginningBal() {
		return beginningBal;
	}
	public void setBeginningBal(int beginningBal) {
		this.beginningBal = beginningBal;
	}
	public int getTotalReceived() {
		return totalReceived;
	}
	public void setTotalReceived(int totalReceived) {
		this.totalReceived = totalReceived;
	}
	public int getQtyDispensed() {
		return qtyDispensed;
	}
	public void setQtyDispensed(int qtyDispensed) {
		this.qtyDispensed = qtyDispensed;
	}
	public int getLossAdjustment() {
		return lossAdjustment;
	}
	public void setLossAdjustment(int lossAdjustment) {
		this.lossAdjustment = lossAdjustment;
	}
	public int getPhyCount() {
		return phyCount;
	}
	public void setPhyCount(int phyCount) {
		this.phyCount = phyCount;
	}
	public double getAmc() {
		return amc;
	}
	public void setAmc(double amc) {
		this.amc = amc;
	}
	public int getMaxQty() {
		return maxQty;
	}
	public void setMaxQty(int maxQty) {
		this.maxQty = maxQty;
	}
	public int getOrderQty() {
		return orderQty;
	}
	public void setOrderQty(int orderQty) {
		this.orderQty = orderQty;
	}
	public String getTypeTest() {
		return typeTest;
	}
	public void setTypeTest(String typeTest) {
		this.typeTest = typeTest;
	}
	public int getDaysStockedOut() {
		return daysStockedOut;
	}
	public void setDaysStockedOut(int daysStockedOut) {
		this.daysStockedOut = daysStockedOut;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getEmergencyOrder() {
		return emergencyOrder;
	}
	public void setEmergencyOrder(String emergencyOrder) {
		this.emergencyOrder = emergencyOrder;
	}
	public double getEmergencyOrderPoint() {
		return emergencyOrderPoint;
	}
	public void setEmergencyOrderPoint(double emergencyOrderPoint) {
		this.emergencyOrderPoint = emergencyOrderPoint;
	}
	public int getMaxStockLevel() {
		return maxStockLevel;
	}
	public void setMaxStockLevel(int maxStockLevel) {
		this.maxStockLevel = maxStockLevel;
	}
	public int getReportStatus() {
		return reportStatus;
	}
	public void setReportStatus(int reportStatus) {
		this.reportStatus = reportStatus;
	}
	public int getProgramCode() {
		return programCode;
	}
	public void setProgramCode(int programCode) {
		this.programCode = programCode;
	}
	public String getLabMachine() {
		return labMachine;
	}
	public void setLabMachine(String labMachine) {
		this.labMachine = labMachine;
	}
}
