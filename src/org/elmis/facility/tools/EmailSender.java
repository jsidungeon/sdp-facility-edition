package org.elmis.facility.tools;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.elmis.facility.email.EmailSettings;

public class EmailSender extends Thread {

	private EmailSettings settings;

	public EmailSender() {
	}

	public EmailSender(String bodytext, String subject) {
		// TODO Auto-generated constructor stub

		settings = new EmailSettings().getSetting();

		final String username = settings.getUserName();
		final String password = settings.getPassword();

		/*
		 * System.out.println("user name "+ settings.getUserName());
		 * System.out.println("password "+ settings.getPassword());
		 * System.out.println("host "+ settings.getOutgoingMail());
		 * System.out.println("port "+ settings.getOutgoingPort());
		 */

		Properties props = new Properties();
		props.put("mail.smtp.auth", settings.getSmtpAuth());
		props.put("mail.smtp.starttls.enable", settings.getSecurePassword());
		props.put("mail.smtp.host", settings.getOutgoingMail());
		props.put("mail.smtp.port", settings.getOutgoingPort().toString());
		
		
		System.out.println("mail.smtp.auth "+ settings.getSmtpAuth());
		System.out.println("mail.smtp.starttls.enable "+ settings.getSecurePassword());
		System.out.println("mail.smtp.host "+ settings.getOutgoingMail());
		System.out.println("mail.smtp.port "+ settings.getOutgoingPort().toString());
		
		

		/*
		 * props.put("mail.smtp.auth", "true");
		 * props.put("mail.smtp.starttls.enable", "true");
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		 * props.put("mail.smtp.port", "587");
		 */

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("from-"
					+ settings.getDisplayName()));
			// message.setFrom(new
			// InternetAddress("from-joeanubi@googlemail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress
					.parse("to-joe@oribicom.com"));
			message.setSubject(subject);
			message.setText(bodytext);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 */

	public void MailingListSender(String bodytext, String subject,
			List<String> mailingList) {
		// TODO Auto-generated constructor stub

		settings = new EmailSettings().getSetting();

		final String username = settings.getUserName();
		final String password = settings.getPassword();

		Properties props = new Properties();
		props.put("mail.smtp.auth", settings.getSmtpAuth());
		props.put("mail.smtp.starttls.enable", settings.getSecurePassword());
		props.put("mail.smtp.host", settings.getOutgoingMail());
		props.put("mail.smtp.port", settings.getOutgoingPort().toString());

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("from-"
					+ settings.getDisplayName()));

			for (int i = 0; i < mailingList.size(); i++) { // changed from a
															// while loop

				message.setRecipients(Message.RecipientType.TO, InternetAddress
						.parse(mailingList.get(i)));

			}

			// message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("to-joe@oribicom.com"));

			message.setSubject(subject);
			message.setText(bodytext);

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	public void SendTestMailTLS(String emailAddress) {

		settings = new EmailSettings().getSetting();

		final String username = settings.getUserName();
		final String password = settings.getPassword();

		/*
		 * System.out.println("user name "+ settings.getUserName());
		 * System.out.println("password "+ settings.getPassword());
		 * System.out.println("host "+ settings.getOutgoingMail());
		 * System.out.println("port "+ settings.getOutgoingPort());
		 */

		Properties props = new Properties();
		props.put("mail.smtp.auth", settings.getSmtpAuth());
		props.put("mail.smtp.starttls.enable", settings.getSecurePassword());
		props.put("mail.smtp.host", settings.getOutgoingMail());
		props.put("mail.smtp.port", settings.getOutgoingPort().toString());
		
		
		//System.out.println("mail.smtp.auth "+ settings.getSmtpAuth());
		//System.out.println("mail.smtp.starttls.enable "+ settings.getSecurePassword());
		//System.out.println("mail.smtp.host "+ settings.getOutgoingMail());
		//System.out.println("mail.smtp.port "+ settings.getOutgoingPort().toString());

		/*
		 * props.put("mail.smtp.auth", "true");
		 * props.put("mail.smtp.starttls.enable", "true");
		 * props.put("mail.smtp.host", "smtp.gmail.com");
		 * props.put("mail.smtp.port", "587");
		 */

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("from-"
					+ settings.getDisplayName()));
			// message.setFrom(new
			// InternetAddress("from-joeanubi@googlemail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress
					.parse(emailAddress));
			message.setSubject("LegalManager Test email");
			message.setText("Dear Sir /Madam,"
					+ "\n\n This is a test email from LegalManager!");

			Transport.send(message);

			javax.swing.JOptionPane
					.showMessageDialog(null,
							"Test Email Sent !!! please check your mail box to confirm");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

}
