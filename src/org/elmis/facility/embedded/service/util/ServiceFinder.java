package org.elmis.facility.embedded.service.util;

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.ElmisProperties;

public class ServiceFinder implements Runnable{

	private String USERNAME_POSTGRESQL = System.getProperty("dbuser");//postgres
	private String PASSWORD_POSTGRESQL = System.getProperty("dbpassword");//"p@ssw0rd";
	private String CONN_STRING_ELMIS_POSTGRES = System.getProperty("dburl");//"jdbc:postgresql://elmis1:5432/postgres";
	private JLabel informationLabel;
	public ServiceFinder(JLabel informationLabel)
	{
		this.informationLabel = informationLabel;
	}
	@Override
	public void run() {
		while(true){
			if (isPsqlRunning()){
				informationLabel.setText("Postgres Database on "+System.getProperty("dbhost")+" available");
				informationLabel.setForeground(Color.WHITE);
			}
			else{
				//System.out.println("Database on "+System.getProperty("dbhost")+" not available");

				informationLabel.setForeground(Color.RED);
				informationLabel.setText("Postgres Database on "+System.getProperty("dbhost")+" not available");
			}
			try {
				Thread.sleep(60000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	public boolean isPsqlRunning()
	{
		Connection connection = null;
		try{
			connection = DriverManager.getConnection(CONN_STRING_ELMIS_POSTGRES, USERNAME_POSTGRESQL, PASSWORD_POSTGRESQL);

			return true;
		}
		catch(SQLException e)
		{
			System.out.println("Error postgress: "+e.getMessage());
		}
		finally
		{
				try {
					if (connection != null)
					connection.close();
					//System.out.println("Connection closed...");
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					//e.printStackTrace();
				}
		}
		return false;
	}
}
