package org.elmis.forms.stores.receiving;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

public class sccfilechooser {
	
	
	public class OpenFileAction extends AbstractAction {
	    JFrame frame;
	    JFileChooser chooser;

	    OpenFileAction(JFrame frame, JFileChooser chooser) {
	        super("Open...");
	        this.chooser = chooser;
	        this.frame = frame;
	    }

	    public void actionPerformed(ActionEvent evt) {
	        // Show dialog; this method does not return until dialog is closed
	        chooser.showOpenDialog(frame);

	        // Get the selected file
	        File file = chooser.getSelectedFile();
	    }
	};

	// This action creates and shows a modal save-file dialog.
	public class SaveFileAction extends AbstractAction {
	    JFileChooser chooser;
	    JFrame frame;

	    SaveFileAction(JFrame frame, JFileChooser chooser) {
	        super("Save As...");
	        this.chooser = chooser;
	        this.frame = frame;
	    }

	    public void actionPerformed(ActionEvent evt) {
	        // Show dialog; this method does not return until dialog is closed
	        chooser.showSaveDialog(frame);

	        // Get the selected file
	        File file = chooser.getSelectedFile();
	    }
	};

}
