package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.model.HivTestPhysicalCount;
import org.elmis.facility.domain.model.HivTestProduct;
import org.elmis.facility.domain.model.HivTestResult;
import org.elmis.facility.domain.model.Transaction;
import org.elmis.facility.reports.utils.CalendarUtil;

public class HivTestResultDao {
	@SuppressWarnings("unused")
	private Map<String, Object> map = new HashMap<>();
	private CalendarUtil calU = new CalendarUtil();
	private SqlSessionFactory sqlSessionFactory;
	
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<HivTestResult> getHivTestResult(int testId, int dispensaryId){
		Map<String, Object> map = new HashMap<>();
		map.put("testId", testId);
		map.put("date", calU.getSqlDate(calU.getTodayDate()));
		map.put("dispensaryId", dispensaryId);
		SqlSession session = sqlSessionFactory.openSession(false);
		try{
			List<HivTestResult> hivTestResult = session.selectList("HivTestResult.HivTest", map);
			return hivTestResult;
		}
		finally{
			session.close();
		}
	}
	public List<Transaction> getLastScreeningTransactions(){
		SqlSession session = sqlSessionFactory.openSession(false);
		try{
			List<Transaction> transactions = session.selectList("Transaction.hiv_screening_transactions");
			for (Transaction t : transactions){
				System.out.println("Last screening transaction "+t);
			}
			return transactions;
		}
		finally{
			session.close();
		}
	}
	public List<Transaction> getLastConfirmatoryTransactions(){
		SqlSession session = sqlSessionFactory.openSession(false);
		try{
			List<Transaction> transactions = session.selectList("Transaction.hiv_confirmatory_transactions");
			for (Transaction t : transactions){
				System.out.println("Last confirmatory transaction "+t);
			}
			return transactions;
		}
		finally{
			session.close();
		}
	}
	public int getLastTestId(){
		SqlSession session = sqlSessionFactory.openSession(false);
		try{
			return session.selectOne("HivTestResult.last_test_id");
		}
		finally{
			session.close();
		}
	}
	public int insertScreeningResult(String screeningResult, String purpose, String productCode, int siteId, int balance, Date testDate, String otherPurpose){
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap<>();
		map.put("screeningResult", screeningResult);
		map.put("productCode", productCode);
		map.put("siteId", siteId);
		map.put("balance", balance);
		map.put("purpose", purpose);
		map.put("testDate", testDate);
		map.put("otherPurpose", otherPurpose);
		try{
			session.insert("HivTestResult.save_screening_result", map);
			session.commit();
			return session.selectOne("HivTestResult.last_test_id");
		}
		finally{
			session.close();
		}
	}
	public void insertConfirmatory(String confirmatoryResult, String purpose, String productCode, int testId, int siteId, int balance, Date testDate, String otherPurpose){
		Map<String, Object> map = new HashMap<>();
		map.put("confirmatoryResult", confirmatoryResult);
		map.put("productCode", productCode);
		map.put("testId", testId);
		map.put("siteId", siteId);
		map.put("balance", balance);
		map.put("purpose", purpose);
		map.put("testDate", testDate);
		map.put("otherPurpose", otherPurpose);
		SqlSession session = sqlSessionFactory.openSession(false);
		try{
			session.insert("HivTestResult.save_confirmatory_result", map);
			session.commit();
		}
		finally{
			session.close();
		}
	}
	public void insertFinalTestResult(int testId, char finalResult,String purpose, Date testDate, int siteId){
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap<>();
		map.put("testId", testId);
		map.put("finalResult", finalResult);
		map.put("purpose", purpose);
		map.put("testDate", testDate);
		map.put("siteId", siteId);
		try{
			session.insert("HivTestResult.save_test_result", map);
			session.commit();
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public   Map<String, Object>  getTestingProducts(int siteId){
		SqlSession session = sqlSessionFactory.openSession();
		//Map<String, Object> map = new HashMap<>();
		try{
			List<Map<String, Object>> testProducts = session.selectList("HivTestResult.test_products",siteId);
			for (Map<String, Object> m : testProducts){
				map.put(m.get("type_of_test").toString(), m.get("type_of_test"));
				map.put( m.get("product_code").toString(), m.get("product_code"));
				map.put( m.get("dar_display_name").toString(), m.get("dar_display_name"));
				map.put(m.get("type_of_test").toString()+"qty", m.get("product_balance").toString());
			}

			return map;
		}
		finally{
			session.close();
		}
	}
	public Map<String, Object> getPhysicalCountStatus(int siteId){
		Map<String, Object> m = new HashMap<>();
		m.put("siteId", siteId);
		//m.put("date", physicalCountDate);
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		try{
			java.sql.Date screeningTimestamp = session.selectOne("HivTestResult.screen_last_transaction",siteId);
			m.put("date", screeningTimestamp);
			List<Map<String, Object>> productPhysicalCountList = session.selectList("HivTestResult.product_physicalcount",m);
			for (Map<String, Object> p : productPhysicalCountList){
				map.put(p.get("product_code").toString(), p.get("count_carried"));
			}
			return map;
		}
		finally{
			session.close();
		}
	}
	public Date getLastTractionDate(int siteId){
		SqlSession session = sqlSessionFactory.openSession();
		try{
			return session.selectOne("HivTestResult.screen_last_transaction",siteId);
		}
		finally{
			session.close();
		}
	}
	public Map<String, Object> getBeginBalance(Date date, int siteId){
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> m = new HashMap<>();
		m.put("date", date);
		m.put("siteId", siteId);
		Map<String, Object> map = new HashMap<>();
		try{
			
			List<Map<String, Object>> beginBalance =  session.selectList("HivTestResult.beginningBalance", m);
			for (Map<String, Object> p : beginBalance){
				String darDisplayName =  p.get("dar_display_name").toString();
				map.put(p.get("type_of_test").toString(),darDisplayName);
				map.put(darDisplayName, p.get("qty"));
			}
			return map;
		}
		finally{
			session.close();
		}
	}
	public Map<String, Object> getPhysicalCount(Date date, int siteId){
		System.out.println("HivTestResultDao getPhysicalCount "+date);
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> m = new HashMap<>();
		m.put("date", date);
		m.put("siteId", siteId);
		Map<String, Object> map = new HashMap<>();
		try{
			
			List<Map<String, Object>> beginBalance =  session.selectList("HivTestResult.physicalcount", m);
			for (Map<String, Object> p : beginBalance){
				String darDisplayName =  p.get("dar_display_name").toString();
				map.put(p.get("type_of_test").toString(),darDisplayName);
				map.put(darDisplayName, p.get("qty"));
			}
			return map;
		}
		finally{
			session.close();
		}
	}
	public List<HivTestResult> getDailyActivityRegisterTransaction(Date date, int siteId){
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("siteId", siteId);
		map.put("date", date);
		try{
			return session.selectList("HivTestResult.dar", map);
		}
		finally{
			session.close();
		}
	}
	public List<HivTestResult> getDailyActivityRegisterTransactionRange(Date date1, Date date2, int siteId){
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> map = new HashMap<>();
		map.put("siteId", siteId);
		map.put("date1", date1);
		map.put("date2", date2);
		try{
			return session.selectList("HivTestResult.dar2", map);
		}
		finally{
			session.close();
		}
	}
	public Map<String, Object> getAdjustments(Date date, int siteId){
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> m = new HashMap<>();
		m.put("date", date);
		m.put("siteId", siteId);
		Map<String, Object> map = new HashMap<>();
		try{
			
			List<Map<String, Object>> beginBalance =  session.selectList("HivTestResult.adjustments", m);
			for (Map<String, Object> p : beginBalance){
				map.put(p.get("type_of_test").toString(),p.get("qty"));
			}
			return map;
		}
		finally{
			session.close();
		}
	}
	public Map<String, String> getTestProducts(){
		SqlSession session = sqlSessionFactory.openSession();

		try{
			Map<String, String> map = new HashMap<>();
			List<Map<String, String>> testProductList =  session.selectList("HivTestResult.products");
			for (Map<String, String> p : testProductList){
				map.put(p.get("type_of_test").toString(),p.get("dar_display_name"));
			}
			return map;
		}
		finally{
			session.close();
		}
	}
	public boolean checkPhysicalCountCarriedOut(int siteId, String productCode, Date date) {
		Map<String, Object> map = new HashMap<>();
		map.put("siteId", siteId);
		map.put("productCode", productCode);
		map.put("date", date);
		SqlSession session = sqlSessionFactory.openSession();

		try{
			Boolean result = session.selectOne("HivTestResult.check_pc", map);
			if(result == null)
				result = false;
			return result;
		}
		finally{
			session.close();
		}
	}
	public void insertPhysicalCount(Date date, int siteId, List<HivTestPhysicalCount> list){
		Map<String, Object> map = new HashMap<>();
		map.put("siteId", siteId);
		map.put("date", date);

		SqlSession session = sqlSessionFactory.openSession();
		try{
			for (int i = 0; i < list.size(); i++){
				String productCode = list.get(i).getProduct_code();
				int quantity = list.get(i).getQty_count();
				map.put("quantity", quantity);
				map.put("productCode", productCode);
				session.insert("HivTestResult.insert_physical_count", map);
			}
			session.commit();
		}
		finally{
			session.close();
		}
	}
	
	public Map<String, Object> getQuantityIssued(Date date, int siteId){
		Map<String, Object> map = new HashMap<>();
		map.put("siteId", siteId);
		map.put("date", date);
		SqlSession session = sqlSessionFactory.openSession();
		Map<String, Object> m = new HashMap<>();
		try{
			List<Map<String, Object>> quantityIssued =  session.selectList("HivTestResult.quantity_issued", map);
			for (Map<String, Object> p : quantityIssued){
				m.put(p.get("type_of_test").toString(), p.get("qty"));
			}
			return m;
		}
		finally{
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public  int  doselectproductDPid(String pcode ,Integer  dpid){
		   @SuppressWarnings("rawtypes")
		Map map = new HashMap();
	       SqlSession session = sqlSessionFactory.openSession();
	     	Integer olddpid ;
	     	map.put("productCode", pcode);
	     	map.put("dpid", dpid);
			try {
				 olddpid = session.selectOne("HivTestResult.dpid_exists",map);
				  if(olddpid == null){
					  olddpid = 0;
				  }
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			return olddpid;
			
		}
	
	public boolean checkPhysicalCount(int siteId){
		Map<String, Object> map = new HashMap<>();
		map.put("siteId", siteId);
		map.put("date", calU.getSqlDate(calU.getTodayDate()));
		SqlSession session = sqlSessionFactory.openSession(false);
		try{
			Boolean carriedPhysicalCountScreening = session.selectOne("Transaction.check_physical_count_screening");
			Boolean carriedPhysicalCountConfirmatory = session.selectOne("Transaction.check_physical_count_screening");
			if (carriedPhysicalCountScreening == null)
				carriedPhysicalCountScreening = false;
			if (carriedPhysicalCountConfirmatory == null)
				carriedPhysicalCountConfirmatory = false;
		
			return (carriedPhysicalCountScreening && carriedPhysicalCountConfirmatory);
		}
		finally{
			session.close();
		}
	}
	//inserts for new DPid
	public  void  Insertdpidhivtestbalances(HashMap map){

		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.insert("HivTestResult.insertwithnewdpid",map);
			session.commit();
		} finally {
			session.close();
		}
	}
	public void updateTransaction(HivTestResult testResult, int screeningAdj, int confirmatoryAdj, boolean deleteConfirmatoryTest, boolean insertConfirmatoryTest){
		System.out.println(testResult);
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap<>();
		map.put("screeningAdj", screeningAdj);
		map.put("confirmatoryAdj", confirmatoryAdj);
		map.put("siteId", testResult.getSiteId());
		map.put("testId", testResult.getTestId());
		map.put("productCode", testResult.getConfirmatoryProduct());
		map.put("testDate", calU.getSqlDate(calU.getTodayDate()));
		try {
			session.update("HivTestResult.update_screening_transaction",testResult);
			session.update("HivTestResult.update_balances", map);
			if (deleteConfirmatoryTest)
				session.delete("HivTestResult, delete_confirmatorytest", map);
			
			if (insertConfirmatoryTest)
				session.insert("HivTestResult.save_confirmatory_result", map);
			session.commit();
			JOptionPane.showMessageDialog(null, "Sampe ID "+testResult.getTestId()+" has been updated.");

		} finally {
			session.close();
		}
	}
}
