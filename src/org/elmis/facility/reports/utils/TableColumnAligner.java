package org.elmis.facility.reports.utils;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * TableColumnAligner.java
 * Purpose: used in aligning columns in JTables but not column headers
 * @author Michael Mwebaze
 * @version 1.0
 */
public class TableColumnAligner {
	private DefaultTableCellRenderer alignmentRenderer = new DefaultTableCellRenderer();
	
	/**
	 * Right aligns a specific column in a table. Column numbering begins with a 0 index
	 * @param table Table whose columns are to be right aligned
	 * @param column column in Table which will be right aligned. 
	 */
	public void rightAlignColumn(JTable table, int column) {
		alignmentRenderer.setHorizontalAlignment(DefaultTableCellRenderer.RIGHT );
	    table.getColumnModel().getColumn(column).setCellRenderer(alignmentRenderer);
	}
	/**
	 * Left aligns a specific column in a table. Column numbering begins with a 0 index
	 * @param table Table whose columns are to be left aligned
	 * @param column column in Table which will be left aligned. 
	 */
	public void leftAlignColumn(JTable table, int column) {  
		alignmentRenderer.setHorizontalAlignment(DefaultTableCellRenderer.LEFT );
	    table.getColumnModel().getColumn(column).setCellRenderer(alignmentRenderer);
	}
	/**
	 * Centre aligns a specific column in a table. Column numbering begins with a 0 index
	 * @param table Table whose columns are to be centre aligned
	 * @param column column in Table which will be centre aligned. 
	 */
	public void centreAlignColumn(JTable table, int column) {
		alignmentRenderer.setHorizontalAlignment(DefaultTableCellRenderer.CENTER );
	    table.getColumnModel().getColumn(column).setCellRenderer(alignmentRenderer);
	}
}
