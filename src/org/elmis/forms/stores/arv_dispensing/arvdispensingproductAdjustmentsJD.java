/*
 * receiveProducts.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Label;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.MutableComboBoxModel;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.elmis.forms.stores.receiving.ArvAdjustmentJD;
import org.elmis.forms.stores.receiving.storeAdjustmentTypeJD;

import com.oribicom.tools.TableModel;

import javax.swing.JTextField;
//import com.oribicom.tools.TableModel;


/**
 *
 * @author  __USER__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class arvdispensingproductAdjustmentsJD extends javax.swing.JDialog {
	private final static Cursor busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final static Cursor defaultCursor = Cursor.getDefaultCursor();
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	private String pname;
	private String pstrength;
	public String typeCode;
	private List<Character> charList = new LinkedList();
	private char letter;
	private String searchText = "";
	private String SearchResult = "";
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	
	ARVDispensingDAO aFacilityproductsmapper = null;
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	private String mydateformat = "yyyy-MM-dd";//hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	private String adjustmentname = "";
	private String programname = "";
	public String digit = "";
	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
			"Product Id", "Product Code", "Product Name", "Generic Strength" };
	
	private static final String[] columns_searchfacilityApprovedproducts = {
		"Product Id", "Product Code", "Product Name", "Generic Strength"
		 };
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", ""};
	private static final Object[] defaultv_searchfacilityapprovedproducts = { "", "",
		"", ""};
	private static final int rows_fproducts = 0;
	private static final int rows_searchfproducts = 0;
	private JComboBox facilityProgList = new JComboBox();
	MutableComboBoxModel modelPrograms = (MutableComboBoxModel) facilityProgList
			.getModel();

	private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false};

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 4) {
				return getValueAt(0,4).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static TableModel tableModel_searchfproducts = new TableModel(
			columns_searchfacilityApprovedproducts,
			defaultv_searchfacilityapprovedproducts, rows_searchfproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				/*if (columnIndex == 2) {

					return getValueAt(0, 2).getClass();
				}*/

				if (columnIndex == 4) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,4).getClass();
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		/*public Class getColumnClass(int c) 
		{     
		for(int rowIndex = 0; rowIndex < data.size(); rowIndex++)
		{
		    Object[] row = data.get(rowIndex);
		    if (row[c] != null) {
		        return getValueAt(rowIndex, c).getClass();
		    }   
		}
		return String.class;
		}*/

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static int total_programs = 0;
	public static Map parameterMap_fproducts = new HashMap();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> adjustmensproductsList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	FacilityProductsDAO Facilityproductsmapper = null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private JCheckBox checkbox;
	public static Boolean cansave = false;
	public  String Pcode = "";
	public int colindex = 0;
	public  int rowindex = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilitytypeCode;
	public Boolean  productsearchenabled = false;
    public Boolean  calltracer = false; 
    int rowcheck = 0;
	int rowcheckinit = -1;
	ArrayList<Elmis_Stock_Control_Card> adjustmentstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();

	ListIterator elmistockcontrolcarditerator = adjustmentstockcontrolcardList
			.listIterator();

	private Elmis_Stock_Control_Card Adjustmentelmis_stock_control_card;
	private Elmis_Stock_Control_Card Adjustmentsavestockcontrolcard;
	public String facilityproductsource;
	private Integer productid = 0;

	/** Creates new form receiveProducts */
	public arvdispensingproductAdjustmentsJD(java.awt.Frame parent,
			boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(1000, 600);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		SearchproductsJTF.setPreferredSize(new Dimension(150, 20));
		ShipmentdateJDate.setPreferredSize(new Dimension(150, 20));
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		ShipmentdateJDate = new com.toedter.calendar.JDateChooser();
		jScrollPane1 = new javax.swing.JScrollPane();
		AdjustmentfacilityapprovedProductsJT = new javax.swing.JTable();
		CancelJBtn = new javax.swing.JButton();
		jLabel3 = new javax.swing.JLabel();
		label1 = new java.awt.Label();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Product Adjustments");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		ShipmentdateJDate.setDateFormatString(mydateformat);
		ShipmentdateJDate.setFont(new java.awt.Font("Ebrima", 0, 20));
		ShipmentdateJDate.setPreferredSize(new java.awt.Dimension(150, 20));
		ShipmentdateJDate
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ShipmentdateJDatePropertyChange(evt);
					}
				});

		AdjustmentfacilityapprovedProductsJT.setFont(new java.awt.Font(
				"Ebrima", 0, 20));
		AdjustmentfacilityapprovedProductsJT.setModel(tableModel_fproducts);
		AdjustmentfacilityapprovedProductsJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						AdjustmentfacilityapprovedProductsJTMouseClicked(evt);
					}
				});
		AdjustmentfacilityapprovedProductsJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						AdjustmentfacilityapprovedProductsJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(AdjustmentfacilityapprovedProductsJT);

		CancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBtn.setText("Close");
		CancelJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				CancelJBtnMouseClicked(evt);
			}
		});
		CancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBtnActionPerformed(evt);
			}
		});

		jLabel3.setFont(new Font("Ebrima", Font.BOLD, 18));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Adjustment date");

		label1.setFont(new Font("Ebrima", Font.BOLD, 18));
		label1.setForeground(new java.awt.Color(255, 255, 255));
		label1.setText("Product Name Search");

		SearchproductsJTF = new JTextField();
		SearchproductsJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			
			public void keyTyped(java.awt.event.KeyEvent evt) {
				SearchproductsJTFKeyTyped(evt);
			}
		});
		
		
		//SearchproductsJTF.setColumns(10);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 860, Short.MAX_VALUE)
						.addComponent(CancelJBtn)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(jLabel3)
								.addComponent(ShipmentdateJDate, GroupLayout.PREFERRED_SIZE, 262, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED, 199, Short.MAX_VALUE)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(SearchproductsJTF, GroupLayout.PREFERRED_SIZE, 284, GroupLayout.PREFERRED_SIZE)
								.addComponent(label1, GroupLayout.PREFERRED_SIZE, 264, GroupLayout.PREFERRED_SIZE))
							.addGap(115)))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(jLabel3)
						.addComponent(label1, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addGap(9)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addComponent(ShipmentdateJDate, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
						.addComponent(SearchproductsJTF, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
					.addGap(61)
					.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 329, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(CancelJBtn, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(62))
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE,
				javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void SearchproductsJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
		productsearchenabled  = true ;
		System.out.println(this.arvsearchproductsList.size() + "list not null");

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
			searchText = this.SearchproductsJTF.getText();
			
		       System.out.println("I detedcted a back space ah! did you call me ??");
	           //re populate table with original list 
	           this.callbackARVproductlist();

			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}

			for (char c : charList) {

				s.append(c);

			}
		} else {

			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			//get ARV list 
			try {
				if (!(this.SearchproductsJTF.getText() == "")) {

					if (!(s.substring(0, 1).matches("[0-9]"))) {

						callfacility = new FacilitySetUpDAO();
						Facilityproductsmapper = new FacilityProductsDAO();

						this.facilityprogramcode = "ARV";
						//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
						//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

						facility = callfacility.getFacility();

						facilitytype = callfacility
								.selectAllwithTypes(facility);
						typeCode = facilitytype.getCode();

						productsList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
						arvsearchproductsList = productsList;

						/*for(VW_Program_Facility_ApprovedProducts p: arvsearchproductsList ){
							
							if(p.getPrimaryname().contains(s)){
								this.arvproductsList.add(p);
							}
						}*/

						for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

							if (p.getPrimaryname().toLowerCase().contains(s.toString().toLowerCase())) {
								this.arvproductsList.add(p);
							}
						}

						//	System.out.println("Delete " + s);
						//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						this.searchPopulateProgram_ProductTable(
								arvproductsList, "STRING_FOUND");

					}
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}

		}

		// searchText += Character.toString(letter);
		/*	charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			try {
				//check if char is a digit and escape the loop.
				if(!(this.searchjTextField.getText() == "")){
				if(!(s.substring(0,1).matches("[0-9]"))){ 

				callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper.dogetcurrentFacilityApprovedProducts(facilityprogramcode, typeCode);
					
				arvsearchproductsList = productsList;

				for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

					if (p.getPrimaryname().contains(s)) {
						this.arvproductsList.add(p);
					}
				}
				//System.out.println("Delete " + s);
				//arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

				searchPopulateProgram_ProductTable(arvproductsList,
						"STRING_FOUND");
				
				}
				
			}
			} catch (NullPointerException e) {
				e.getMessage();
			}*/
	}
	
	
	
	private void callbackARVproductlist(){
		//get ARV list 
		try {
			if (!(this.SearchproductsJTF.getText() == "")) {

		
					callfacility = new FacilitySetUpDAO();
					Facilityproductsmapper = new FacilityProductsDAO();

					this.facilityprogramcode = "ARV";
				    facility = callfacility.getFacility();

					facilitytype = callfacility
							.selectAllwithTypes(facility);
					typeCode = facilitytype.getCode();

					productsList = Facilityproductsmapper
							.dogetcurrentFacilityApprovedProducts(
									facilityprogramcode, typeCode);
					arvsearchproductsList = productsList;

					this.searchPopulateProgram_ProductTable(
							arvsearchproductsList, "STRING_FOUND");

				
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}

	@SuppressWarnings("unchecked")
	private void searchPopulateProgram_ProductTable(List fapprovProducts,
			String mysearch) {

		try {

			this.SearchResult = mysearch;

			/*	callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProducts(facilityprogramcode);*/
			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}

			this.AdjustmentfacilityapprovedProductsJT
					.setModel(tableModel_searchfproducts);
			tableModel_searchfproducts.clearTable();

			fapprovedproductsIterator = fapprovProducts.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength().toString();
				/*defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getPacksize();*/
				//	defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
				/*	TableColumn column1 = facilityapprovedProductsJT
							.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());*/

				//defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}
		tableModel_searchfproducts.insertRow(cols);
				//tableModel_searchfproducts.fireTableDataChanged();
				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	protected void AdjustmentfacilityapprovedProductsJTMouseClicked(
			MouseEvent evt) {
		// TODO Auto-generated method stub
		
		rowcheck = this.AdjustmentfacilityapprovedProductsJT.getSelectedRow();
		System.out.println("I got the position" + "Y"  + "X" + rowcheck);
		 if (productsearchenabled == true){
		        
	    	    		
	    		
	    		Pcode = tableModel_searchfproducts.getValueAt(
	    				this.AdjustmentfacilityapprovedProductsJT
	    						.getSelectedRow(), 1).toString();
	    		
	    		productid = (Integer
	    				.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 0)
	    						.toString()));
	    		pname = tableModel_searchfproducts.getValueAt(
	    				this.AdjustmentfacilityapprovedProductsJT
						.getSelectedRow(),2).toString();
	    		
	    		pstrength = tableModel_searchfproducts.getValueAt(
	    				this.AdjustmentfacilityapprovedProductsJT
						.getSelectedRow(),3).toString();
	    		
	    	
	    		if(!(productdeliverdate == null)){
	    			try {
						arvdispensingproductAdjustmentsJD.this
								.setCursor(busyCursor);

						new ArvAdjustmentJD(
								javax.swing.JOptionPane.getFrameForComponent(this),
								true, Pcode, productid, productdeliverdate,
								this.facilityprogramcode, pname, pstrength);
					} catch (Exception e) {
						JOptionPane.showMessageDialog(this,
								"" + e.getLocalizedMessage(),
								"Error - Contact System Administrator",
								JOptionPane.ERROR_MESSAGE);
					} finally {
						arvdispensingproductAdjustmentsJD.this
								.setCursor(defaultCursor);
					}
	    			
	    		}
	    		
		 }else{
		
		Pcode = tableModel_fproducts.getValueAt(
				this.AdjustmentfacilityapprovedProductsJT
						.getSelectedRow(), 1).toString();
		
		productid = (Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 0)
						.toString()));
		
		pname = tableModel_fproducts.getValueAt(
				this.AdjustmentfacilityapprovedProductsJT
				.getSelectedRow(),2).toString();
		
		pstrength = tableModel_fproducts.getValueAt(
				this.AdjustmentfacilityapprovedProductsJT
				.getSelectedRow(),3).toString();
		
			if (!(productdeliverdate == null)) {

				try {
					arvdispensingproductAdjustmentsJD.this
							.setCursor(busyCursor);

					new ArvAdjustmentJD(
							javax.swing.JOptionPane.getFrameForComponent(this),
							true, Pcode, productid, productdeliverdate,
							this.facilityprogramcode, pname, pstrength);
				} catch (Exception e) {
					JOptionPane.showMessageDialog(this,
							"" + e.getLocalizedMessage(),
							"Error - Contact System Administrator",
							JOptionPane.ERROR_MESSAGE);
				} finally {
					arvdispensingproductAdjustmentsJD.this
							.setCursor(defaultCursor);
				}

			}
	
		 }

	}

	private void CancelJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		this.dispose();
	}

	private void ShipmentdateJDatePropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		try {

			String mydate = this.ShipmentdateJDate.getDate().toString();
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;
			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	public  void AdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		AdjustmentfacilityapprovedProductsJT.getColumnModel().getColumn(0)
				.setMinWidth(0);
		AdjustmentfacilityapprovedProductsJT.getColumnModel().getColumn(0)
				.setMaxWidth(0);
		AdjustmentfacilityapprovedProductsJT.getColumnModel().getColumn(0)	
				.setWidth(0);
		
			//saveShipped_Line_Items = new Shipped_Line_Items();
		//TableColumn column1 = AdjustmentfacilityapprovedProductsJT
				//.getColumnModel().;
		
		if ("tableCellEditor".equals(evt.getPropertyName())) {
	    
			if (this.AdjustmentfacilityapprovedProductsJT.isColumnSelected(4)) {
				System.out.println("IM READY TO POP FORM- 2014");
				int col = AdjustmentfacilityapprovedProductsJT.getSelectedColumn();
				System.out.println(col);
							
			/*	if(col == 5){
					//if(calltracer == false){
					if(!(this.rowcheck == this.rowcheckinit)){
						calltracer = false;
						rowcheckinit = rowcheck;
					
					new arvstoreAdjustmentTypeJD(javax.swing.JOptionPane.getFrameForComponent(this),
							true);
					}
					//}
					try {
					if (!(digit.equals(""))){
					this.AdjustmentfacilityapprovedProductsJT.setValueAt(digit, AdjustmentfacilityapprovedProductsJT.getSelectedRow(), 5);
										
					}
					}catch(NullPointerException n){
						n.getStackTrace();
					}
				//this.AdjustmentfacilityapprovedProductsJT.setValueAt(arvstoreAdjustmentTypeJD.this.paramAdjustment, AdjustmentfacilityapprovedProductsJT.getSelectedRow(), 5);
				
				System.out.println("POPUP FORM NOW!!");
				try{
		
		
			    if (AdjustmentfacilityapprovedProductsJT.isEditing())
			    {
			    	//this.AdjustmentfacilityapprovedProductsJT.editCellAt(AdjustmentfacilityapprovedProductsJT.getSelectedRow(),6);
			    	//AdjustmentfacilityapprovedProductsJT.getEditorComponent().requestFocus();
			    }				
			
				}
				catch(StackOverflowError o)	{
					o.getStackTrace();
				}
				

				
			//}
	 }*/
	//set check outside column 
	 if (!AdjustmentfacilityapprovedProductsJT.isEditing()) {
		 
		 createshipmentObj(Pcode, rnrid, shipmentdate);
	}
				
				
				
	}
			
		
			if (this.AdjustmentfacilityapprovedProductsJT.isColumnSelected(4)) {
				if (AdjustmentfacilityapprovedProductsJT.isEditing()) {
					System.out.println("THIS CELL HAS STARTED EDITING");
					//get column index
					colindex = this.AdjustmentfacilityapprovedProductsJT
							.getSelectedColumn();
					rowindex = this.AdjustmentfacilityapprovedProductsJT
							.getSelectedRow();
					Pcode = tableModel_fproducts.getValueAt(
							this.AdjustmentfacilityapprovedProductsJT
									.getSelectedRow(), 1).toString();
					/*System.out.println(colindex);	
					System.out.println(rowindex);	
					System.out.println(Pcode);*/

			} else if (!AdjustmentfacilityapprovedProductsJT.isEditing()) {

					//createshipmentObj(Pcode, rnrid, shipmentdate);

					System.out.println("THIS CELL IS NOT EDITING");

				}
			}
		}
		//this.AdjustmentfacilityapprovedProductsJT.editCellAt(AdjustmentfacilityapprovedProductsJT.getSelectedRow(), 6);
		

	}

	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	public void createshipmentObj(String apcode, int arnrid,
			Timestamp amodifieddate) {
		//System.out.println("Did object call me ???");
		//created a shippment object 
		//created a shippment object 
		Double A = 0.0;
		Double B = 0.0;
		double C = 0.0;
		if (productdeliverdate == null) {
			java.util.Date date = new java.util.Date();
			amodifieddate = new Timestamp(date.getTime());
		} else {
			amodifieddate = productdeliverdate;
		}

		System.out.println(amodifieddate);
		System.out.println(A);
		System.out.println(B);
		System.out.println(C);
        if ( productsearchenabled == true){
        
    		String prodAdjustremark = tableModel_searchfproducts.getValueAt(rowindex, 6)
    				.toString();
    		

    		if (prodAdjustremark != null) {
    			Adjustmentelmis_stock_control_card.setRemark(prodAdjustremark);
    		}
    		Adjustmentelmis_stock_control_card.setProductcode(apcode);

    		Adjustmentelmis_stock_control_card.setProductid(Integer
    				.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 0)
    						.toString()));
          	
        	
        }else{
		
		String prodAdjustremark = tableModel_fproducts.getValueAt(rowindex, 6)
				.toString();
		

		if (prodAdjustremark != null) {
			Adjustmentelmis_stock_control_card.setRemark(prodAdjustremark);
		}
		Adjustmentelmis_stock_control_card.setProductcode(apcode);

		Adjustmentelmis_stock_control_card.setProductid(Integer
				.parseInt(tableModel_fproducts.getValueAt(rowindex, 0)
						.toString()));
		
        }
		if (this.adjustmentname.equals("TRANSFER_OUT")) {
			
			 if ( productsearchenabled == true){
					try {
						
					
						 Adjustmentelmis_stock_control_card.setAdjustments(Integer
								.parseInt(tableModel_searchfproducts.getValueAt(rowindex,4)
										.toString()));
						
						}catch(NumberFormatException e){
							e.getStackTrace();
						}

						Adjustmentelmis_stock_control_card.setQty_received(0.0);
						Adjustmentelmis_stock_control_card.setQty_issued(0.0); 
						
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							if( b.getProductcode().equals(apcode)){
																
								@SuppressWarnings("unused")
								double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
								Adjustmentelmis_stock_control_card.setBalance(increase);
													
						}
					
						}
						
						try{
							aFacilityproductsmapper = new ARVDispensingDAO();
							productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
							
							if(productcodesstockQtyList.size() > 0){
								for (@SuppressWarnings("unused")
								ProductQty p : productcodesstockQtyList) {
									
									if(p.getProduct_code().equals(apcode)){
										//update call here now 
										p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
										aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
									}
									
									}
								
							
						}
							
							}catch (java.lang.NullPointerException j)	{
								j.getMessage();
							}
						
						
				 
			 }else {
			try {
				
			
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}

			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
					
					
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
					
		
				}
			}
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
			
			

		}
		}
		
		if (this.adjustmentname.equals("TRANSFER_IN")) {
			
			 if ( productsearchenabled == true){
					try {
						
					
						Adjustmentelmis_stock_control_card.setAdjustments(Integer
								.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 4)
										.toString()));
						}catch(NumberFormatException e){
							e.getStackTrace();
						}

						Adjustmentelmis_stock_control_card.setQty_issued(0.0);
						Adjustmentelmis_stock_control_card.setQty_received(0.0);
								
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							if( b.getProductcode().equals(apcode)){
																
								@SuppressWarnings("unused")
								double increase = b.getBalance() + Adjustmentelmis_stock_control_card.getAdjustments();
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}else{
								Adjustmentelmis_stock_control_card.setBalance( Adjustmentelmis_stock_control_card.getAdjustments());
							}
						}
						
						
						try{
						
							productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
							
							if(productcodesstockQtyList.size() > 0){
								for (@SuppressWarnings("unused")
								ProductQty p : productcodesstockQtyList) {
									
									if(p.getProduct_code().equals(apcode)){
										//update call here now 
										p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
										Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc());
									}
									
									}
								
							
						}
							
							}catch (java.lang.NullPointerException j)	{
								j.getMessage();
							}

				 
				 
			 }else{
			try {
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}

			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
					
					
					@SuppressWarnings("unused")
					double increase = b.getBalance() + Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			
			try{
			
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc());
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}

		}
	 }
		this.adjustmentname.trim();
		if (this.adjustmentname.equals("DAMAGED")) {
		
			 if ( productsearchenabled == true){
				 
				 try{
						Adjustmentelmis_stock_control_card.setAdjustments(Integer
								.parseInt(tableModel_searchfproducts.getValueAt(rowindex,4)
										.toString()));
			         }catch(NumberFormatException e){
							e.getStackTrace();
						}

						Adjustmentelmis_stock_control_card.setQty_received(0.0);
						Adjustmentelmis_stock_control_card.setQty_issued(0.0);
						
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
						}
						
						try{
							aFacilityproductsmapper = new ARVDispensingDAO();
							productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
							
							if(productcodesstockQtyList.size() > 0){
								for (@SuppressWarnings("unused")
								ProductQty p : productcodesstockQtyList) {
									
									if(p.getProduct_code().equals(apcode)){
										//update call here now 
										p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
										aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
									}
									
									}
								
							
						}
							
							}catch (java.lang.NullPointerException j)	{
								j.getMessage();
							}
				 
			 }else{
			
         try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 4)
							.toString()));
         }catch(NumberFormatException e){
				e.getStackTrace();
			}

			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
					
					
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
		}
		}		 

		if (this.adjustmentname.equals("LOST")) {
			 if ( productsearchenabled == true){
				 try{
						Adjustmentelmis_stock_control_card.setAdjustments(Integer
								.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 4)
										.toString()));
						}catch(NumberFormatException e){
							e.getStackTrace();
						}
						Adjustmentelmis_stock_control_card.setQty_received(0.0);
						Adjustmentelmis_stock_control_card.setQty_issued(0.0);
						
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
						}
						
						try{
							aFacilityproductsmapper = new ARVDispensingDAO();
							productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
							
							if(productcodesstockQtyList.size() > 0){
								for (@SuppressWarnings("unused")
								ProductQty p : productcodesstockQtyList) {
									
									if(p.getProduct_code().equals(apcode)){
										//update call here now 
										p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
										aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
									}
									
									}
								
							
						}
							
							}catch (java.lang.NullPointerException j)	{
								j.getMessage();
							}
				 
			 }else{
			
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
					
					
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
		}
			 
		}
		if (this.adjustmentname.equals("STOLEN")) {
			 if ( productsearchenabled == true){
				 try{
						Adjustmentelmis_stock_control_card.setAdjustments(Integer
								.parseInt(tableModel_searchfproducts.getValueAt(rowindex,4)
										.toString()));
						}catch(NumberFormatException e){
							e.getStackTrace();
						}
						Adjustmentelmis_stock_control_card.setQty_received(0.0);
						Adjustmentelmis_stock_control_card.setQty_issued(0.0);	
						
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							if( b.getProductcode().equals(apcode)){
								@SuppressWarnings("unused")
								double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
						}
						
						
						try{
							aFacilityproductsmapper = new ARVDispensingDAO();
							productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
							
							if(productcodesstockQtyList.size() > 0){
								for (@SuppressWarnings("unused")
								ProductQty p : productcodesstockQtyList) {
									
									if(p.getProduct_code().equals(apcode)){
										//update call here now 
										p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
										aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
									}
									
									}
								
							
						}
							
							}catch (java.lang.NullPointerException j)	{
								j.getMessage();
							}
				 
			 }else{
			
			
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex,4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
					
					
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
			
		}
			 
		}

		if (this.adjustmentname.equals("EXPIRED")) {
			if ( productsearchenabled == true){
				try{
					Adjustmentelmis_stock_control_card.setAdjustments(Integer
							.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 4)
									.toString()));
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new ARVDispensingDAO();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					
					
			}else{
			
			
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex,4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
					
					
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
			
			
			
			
		}
		}

		if (this.adjustmentname.equals("PASSED_OPEN_VIAL_TIME_LIMIT")) {
			
			if ( productsearchenabled == true){
				try{
					Adjustmentelmis_stock_control_card.setAdjustments(Integer
							.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 4)
									.toString()));
					}catch(NumberFormatException e){
						e.getStackTrace();
					} 
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
						aFacilityproductsmapper = new ARVDispensingDAO();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					
			}else {
			
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex,4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			} 
			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
					
					
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
			
		}
		}

		if (this.adjustmentname.equals("COLD_CHAIN_FAILURE")) {
			
			if ( productsearchenabled == true){
				try{
					Adjustmentelmis_stock_control_card.setAdjustments(Integer
							.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 4)
									.toString()));
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					try{
						aFacilityproductsmapper = new ARVDispensingDAO();
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
									aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
					
			}else{
			
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex,4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
										
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
			
		}
		}
		if (this.adjustmentname.equals("CLINIC_RETURN")) {
		
			if ( productsearchenabled == true){
				
				try{
				Adjustmentelmis_stock_control_card.setAdjustments(Integer
						.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 4)
								.toString()));
				}catch(NumberFormatException e){
					e.getStackTrace();
				}  
				Adjustmentelmis_stock_control_card.setQty_received(0);
				Adjustmentelmis_stock_control_card.setQty_issued(0.0);
				//changes Mkausa - 2014
				if(!(facilityprogramcode == null)){
				electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
				}
				for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
					if( b.getProductcode().equals(apcode)){
												
						@SuppressWarnings("unused")
						double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
						Adjustmentelmis_stock_control_card.setBalance(increase);
					}
				}
				
				
				
				try{
					aFacilityproductsmapper = new ARVDispensingDAO();
					productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
					
					if(productcodesstockQtyList.size() > 0){
						for (@SuppressWarnings("unused")
						ProductQty p : productcodesstockQtyList) {
							
							if(p.getProduct_code().equals(apcode)){
								//update call here now 
								p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
								aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
							}
							
							}
						
					
				}
					
					}catch (java.lang.NullPointerException j)	{
						j.getMessage();
					}
				
			}else{
			
			
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}  
			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
								
					@SuppressWarnings("unused")
					double increase = b.getBalance() - Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}
			}
			
			
			try{
				aFacilityproductsmapper = new ARVDispensingDAO();
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							aFacilityproductsmapper.doupdatearvdispenseqtyless(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc(),AppJFrame.getDispensingId(facilityprogramcode));
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
		}
		}
		if (this.adjustmentname.equals("FOUND")) {
			if ( productsearchenabled == true){
				try{
					Adjustmentelmis_stock_control_card.setAdjustments(Integer
							.parseInt(tableModel_searchfproducts.getValueAt(rowindex,4)
									.toString()));
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
													
							@SuppressWarnings("unused")
							double increase = b.getBalance() + Adjustmentelmis_stock_control_card.getAdjustments();
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(Adjustmentelmis_stock_control_card.getAdjustments());
						}
					}
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
			}else{
			
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex,4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			Adjustmentelmis_stock_control_card.setQty_received(0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
									
					@SuppressWarnings("unused")
					double increase = b.getBalance() + Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}else{
					Adjustmentelmis_stock_control_card.setBalance(Adjustmentelmis_stock_control_card.getAdjustments());
				}
			}
			
			
			
			try{
			
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc());
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
		}
		}
		if (this.adjustmentname.equals("PURCHASE")) {
			
			if ( productsearchenabled == true){
				try{
					Adjustmentelmis_stock_control_card.setAdjustments(Integer
							.parseInt(tableModel_searchfproducts.getValueAt(rowindex, 4)
									.toString()));
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
													
							@SuppressWarnings("unused")
							double increase = b.getBalance() + Adjustmentelmis_stock_control_card.getAdjustments();
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(Adjustmentelmis_stock_control_card.getAdjustments());
						}
					}
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
						
						if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
									Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
			}else{
			try{
			Adjustmentelmis_stock_control_card.setAdjustments(Integer
					.parseInt(tableModel_fproducts.getValueAt(rowindex, 4)
							.toString()));
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
									
					@SuppressWarnings("unused")
					double increase = b.getBalance() + Adjustmentelmis_stock_control_card.getAdjustments();
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}else{
					Adjustmentelmis_stock_control_card.setBalance(Adjustmentelmis_stock_control_card.getAdjustments());
				}
			}
			
			
			try{
			
				productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
				
				if(productcodesstockQtyList.size() > 0){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(apcode)){
							//update call here now 
							p.setQty(p.getQty()- Adjustmentelmis_stock_control_card.getAdjustments());
							Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),Adjustmentelmis_stock_control_card.getAdjustments(),p.getLocation(),p.getStockonhand_scc());
						}
						
						}
					
				
			}
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
			
		}
		}

		Adjustmentelmis_stock_control_card
				.setId(com.oribicom.tools.publicMethods.createGUID());//this.GenerateGUID());
		A = Adjustmentelmis_stock_control_card.getQty_received();
		B = Adjustmentelmis_stock_control_card.getQty_issued();
		C = Adjustmentelmis_stock_control_card.getAdjustments();

	   /*	if (!(this.adjustmentname.equals("PURCHASE"))) {
			try{
			C = -C;
			}catch(NullPointerException e){
				e.getStackTrace();
			}
		} else {
			try{
			C = +C;}catch(NullPointerException e){
				e.getStackTrace();
			}
		}*/

		System.out.println(A + "WHAT IS  THE NUMBER");
		System.out.println(B);
		System.out.println(C);

		Adjustmentelmis_stock_control_card
				.setAdjustmenttype(this.adjustmentname);
		System.out.println(Adjustmentelmis_stock_control_card.getBalance());
		//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));
		Adjustmentelmis_stock_control_card.setStoreroomadjustment(false);
		Adjustmentelmis_stock_control_card.setProgram_area(facilityprogramcode);
		Adjustmentelmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
		Adjustmentelmis_stock_control_card.setCreateddate(amodifieddate);

		//created linked list object 

		//adjustmentstockcontrolcardList.add(savestockcontrolcard);
		adjustmentstockcontrolcardList.add(Adjustmentelmis_stock_control_card);
		System.out.println(adjustmentstockcontrolcardList.size()
				+ "ARRAY SIZE???");
		System.out.println("Array size here &&&");
		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
		//return saveShipped_Line_Items;

	}

	private void CancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:

		this.AdjustmentfacilityapprovedProductsJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		
		this.AdjustmentfacilityapprovedProductsJT.setRowHeight(30);
		this.ShipmentdateJDate.setFocusable(isFocusable());
		Facilityproductsmapper = new FacilityProductsDAO();
		facilityprogramsList = Facilityproductsmapper.selectAllPrograms();
		for (Programs p : facilityprogramsList) {
			modelPrograms.addElement(p.getCode());

		}

		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		for (Losses_Adjustments_Types l : facilityAdjustmentList) {
			modelAdjustments.addElement(l.getName());

		}

		Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
        this.ShipmentdateJDate.setDate(new Date()); 
		PopulateProgram_ProductTable(adjustmensproductsList);
		rnrid = 4;
		shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");

	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List fapprovProducts) {
		checkbox = new JCheckBox();
		try {

			if (fapprovProducts.isEmpty()) {
				callfacility = new FacilitySetUpDAO();
				Facilityproductsmapper = new FacilityProductsDAO();

				facility = callfacility.getFacility();
				System.out.println(facility.getCode()
						+ "We have the facility code");
				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

			    this.facilityprogramcode = "ARV";// ProgramsJP.selectedprogramCode;
				String dbdriverStatus = System.getProperty("dbdriver");
				if (!(facilityprogramcode == null)) {
					//reset program code

					if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {
						adjustmensproductsList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
					} else {
						adjustmensproductsList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProductshsqldb(
										facilityprogramcode, typeCode);
					}
				}

			} else {
				adjustmensproductsList = fapprovProducts;
			}
			//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;
			System.out.println(facilityprogramcode + "Got it right ??");
			System.out.println(typeCode + "Got it again right ??");
			rnrid = 4;
			System.out.println(rnrid + "Got it again right ??");

			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : adjustmensproductsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}
			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = adjustmensproductsList.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getProductid().toString();

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getStrength().toString();
				//Set the value of the entered product quantity 

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}
				System.out.print(cols.size()
						+ "What is the SIZE of Array to Table??");
				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	/**
	 * @param args the command line arguments
	 */
	/*public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				arvdispensingproductAdjustmentsJD dialog = new arvdispensingproductAdjustmentsJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}*/
	private javax.swing.JTable AdjustmentfacilityapprovedProductsJT;
	private javax.swing.JButton CancelJBtn;
	private com.toedter.calendar.JDateChooser ShipmentdateJDate;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private java.awt.Label label1;
	private JTextField SearchproductsJTF;
	// End of variables declaration//GEN-END:variables
	
	
	/***************************************************
	 * STARTING CHILD FORM FOR ADJUSTMENTS 
	 ***************************************************/
/**
 *
 * @author  __MKausa__
 */
@SuppressWarnings( { "unused", "serial", "unchecked" })
public class arvstoreAdjustmentTypeJD extends javax.swing.JDialog   {
	

	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;
	TableColumnAligner tablecolumnaligner; 
	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	Integer ProdQty = null;
	public String paramAdjustment = null; 
	//Facility Approved Products JTable *******************

	
	
	//public static int total_programs = 0;
	//public static Map parameterMap_fproducts = new HashMap();
	private final ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator = null;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//List<Shipped_Line_Items>  shippedItemsList =  new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator elmistockcontrolcarditerator = elmisstockcontrolcardList
			.listIterator();
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	FacilityProductsDAO Facilityproductsmapper = null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	//public static Boolean cansave = false;
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer Productid = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	

	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	//List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//private VW_Systemcalculatedproductsbalance facilitysccProducts;
	public  List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	private final String[] columns_RegisterAdjustmentsfacilityApprovedproducts = {
		"Adjustment Type", "Quantity", "Remarks"};
	private static final int rows_RegisterAdjustmentsfproducts = 0;
    private final Object[] defaultv_RegisterAdjustmentsfacilityapprovedproducts = { "", "",
		""};

	public TableModel tableModel_RegisterAdjustments= new TableModel(
			columns_RegisterAdjustmentsfacilityApprovedproducts,
			defaultv_RegisterAdjustmentsfacilityapprovedproducts, rows_RegisterAdjustmentsfproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				
				if (columnIndex == 2) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,2).getClass();
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		

	};


	/*private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();*/

	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public arvstoreAdjustmentTypeJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		System.out.println("********************************************************************8888888");
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(500, 400);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		//AdjustmentTypeJCB1.setPreferredSize(new Dimension(200, 30));

	}

	

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel3 = new javax.swing.JLabel();
		AdjustmentTypeJCB1 = new javax.swing.JComboBox();
		SaveJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record Store Adjustment Type ");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Adjustment Type");

	   	AdjustmentTypeJCB1.setFont(new java.awt.Font("Ebrima", 0, 18));
		AdjustmentTypeJCB1.setModel(modelAdjustments);
		AdjustmentTypeJCB1
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						AdjustmentTypeJCB1ActionPerformed(evt);
					}
				});

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Ok");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap(493, Short.MAX_VALUE)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(SaveJBtn)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(jLabel3)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(AdjustmentTypeJCB1, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE)))
					.addGap(51))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(32)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(AdjustmentTypeJCB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel3))
					.addPreferredGap(ComponentPlacement.RELATED, 251, Short.MAX_VALUE)
					.addComponent(SaveJBtn)
					.addGap(69))
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 983, Short.MAX_VALUE)
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(124, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	

	private void AdjustmentTypeJCB1ActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		JComboBox cb = (JComboBox) evt.getSource();
		adjustmentname = (String) cb.getSelectedItem();
		paramAdjustment = adjustmentname;
			
		System.out.println(adjustmentname);
	}

	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		return productdeliverdate;

	}
	public  void RegisterAdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		
		System.out.println("test pop up adjustments");		

	}

	
	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	
	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
		//this.AdjustmentfacilityapprovedProductsJT.isColumnSelected(5
		arvdispensingproductAdjustmentsJD.this.digit = this.paramAdjustment;
		arvdispensingproductAdjustmentsJD.this.adjustmentname = this.paramAdjustment;
		arvdispensingproductAdjustmentsJD.this.calltracer = true;
		//arvdispensingproductAdjustmentsJD.this.AdjustmentfacilityapprovedProductsJT.editCellAt(AdjustmentfacilityapprovedProductsJT.getSelectedRow(), 6);
		//arvdispensingproductAdjustmentsJD.this.AdjustmentfacilityapprovedProductsJT.editCellAt(-1,-1); 
		
		arvstoreAdjustmentTypeJD.this.dispose();
		
	}

	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new FacilityProductsDAO();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		for (Losses_Adjustments_Types l : facilityAdjustmentList) {
			modelAdjustments.addElement(l.getName());

		}
		
		rnrid = 4;
				shipmentdate = Timestamp.valueOf("2013-07-30 10:10:13");
	}

	@SuppressWarnings( { "unused", "unchecked" })
	
	/**
	 * @param args the command line arguments
	 */

	/*public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				arvstoreAdjustmentTypeJD dialog = new arvstoreAdjustmentTypeJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}*/

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JComboBox AdjustmentTypeJCB1;
	private javax.swing.JButton SaveJBtn;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JPanel jPanel1;
	// End of variables declaration//GEN-END:variables

}
	
	/**********************************************************
	 * END CHILD FORM 
	 **********************************************************/
			 

}