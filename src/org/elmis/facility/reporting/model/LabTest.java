package org.elmis.facility.reporting.model;

import java.sql.Date;

public class LabTest {
	private String remarks;
	private Boolean isCountable;
	private int machineTestCount;

	public int getMachineTestCount() {
		return machineTestCount;
	}
	public void setMachineTestCount(int machineTestCount) {
		this.machineTestCount = machineTestCount;
	}
	public Boolean getIsCountable() {
		return isCountable;
	}
	public void setIsCountable(Boolean isCountable) {
		this.isCountable = isCountable;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMachineName() {
		return machineName;
	}
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}
	public String getMachineCode() {
		return machineCode;
	}
	public void setMachineCode(String machineCode) {
		this.machineCode = machineCode;
	}
	public String getReagentCode() {
		return reagentCode;
	}
	public void setReagentCode(String reagentCode) {
		this.reagentCode = reagentCode;
	}
	public String getReagentName() {
		return reagentName;
	}
	public void setReagentName(String reagentName) {
		this.reagentName = reagentName;
	}
	public int getTestCount() {
		return testCount;
	}
	public void setTestCount(int testCount) {
		this.testCount = testCount;
	}
	public int getNumberDaysOut() {
		return numberDaysOut;
	}
	public void setNumberDaysOut(int numberDaysOut) {
		this.numberDaysOut = numberDaysOut;
	}
	public Date getBeginDate() {
		return beginDate;
	}
	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
	public String machineName;
	public String machineCode;
	public String reagentCode;
	public String reagentName;
	public int testCount;
	private Boolean isEquipmentFunctioning;
	public Boolean getIsEquipmentFunctioning() {
		return isEquipmentFunctioning;
	}
	public void setIsEquipmentFunctioning(Boolean isEquipmentFunctioning) {
		this.isEquipmentFunctioning = isEquipmentFunctioning;
	}
	public Boolean getIsEmergency() {
		return isEmergency;
	}
	public void setIsEmergency(Boolean isEmergency) {
		this.isEmergency = isEmergency;
	}

	public Boolean isEmergency;
	//public boolean isEquipmentFunctioning;
	
	

	public int numberDaysOut;
	public Date beginDate;
	public Date endDate;
	public boolean isSubmitted;
	
	public boolean isSubmitted() {
		return isSubmitted;
	}
	public void setSubmitted(boolean isSubmitted) {
		this.isSubmitted = isSubmitted;
	}
}
