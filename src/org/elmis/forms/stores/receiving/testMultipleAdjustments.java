package org.elmis.forms.stores.receiving;

/***************************************************
	 * STARTING CHILD FORM FOR ADJUSTMENTS 
	 ***************************************************/
/**
 *
 * @author  __MKausa__
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import javax.swing.ComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ListSelectionModel;
import javax.swing.MutableComboBoxModel;
import javax.swing.text.JTextComponent;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.dao.AdjustmentDao;
import org.elmis.facility.domain.model.Adjustment;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.forms.stores.arv_dispensing.arvdispensingProductsPhysicalCountJD;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.domain.model.facilities;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.elmis.forms.stores.arv_dispensing.arvmessageDialog;
import org.elmis.forms.stores.receiving.ProductsPhysicalCountJD;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

import com.oribicom.tools.TableModel;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import javax.swing.JRadioButton;

import java.awt.event.MouseAdapter;

public class testMultipleAdjustments extends javax.swing.JDialog   {
	

	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	public String typeCode;
	TableColumnAligner tablecolumnaligner; 
	private Double calAdjustmenttotal;
	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	public String oldDateString;
	public String newDateString;
	Integer ProdQty = null;
	private Double  showdouble  ;  
	public String paramAdjustment = null; 
	private Boolean Transfer_IN = false;
	private Boolean Transfer_OUT = false;
	public String Adjustmentform = "";
	public String prodAdjustremark = "";
	private String selectedfacilitytoname = "";
	private String selectedfacilityfromname = "";
	//Facility Approved Products JTable *******************
	List<VW_Program_Facility_ApprovedProducts> adjustmensproductsList = new LinkedList();
	//Combo filter serach *********************************************************************************
    JComboBox comboBox;
    ComboBoxModel model;
    JTextComponent editor;
    boolean selecting=false;
    //*****************************************************************************************************
	List<facilities> facilitiesnameList =  new LinkedList();
	private Adjustment  Adjustmentelmis_stock_control_card; 
	private Elmis_Stock_Control_Card Adjustmentsavestockcontrolcard;
	public Boolean  productsearchenabled = false;
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	//public static int total_programs = 0;
	//public static Map parameterMap_fproducts = new HashMap();
	private JComboBox facilityList = new JComboBox();
	MutableComboBoxModel modelfacilities = (MutableComboBoxModel) facilityList
			.getModel();
	private JComboBox facilitynamecomboList = new JComboBox();
	MutableComboBoxModel modelfacilitiesList = (MutableComboBoxModel) facilitynamecomboList
			.getModel();
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	ARVDispensingDAO aFacilityproductsmapper = null;
	private ListIterator<Losses_Adjustments_Types> registeradjustmentfapprovedproductsIterator = null;
	@SuppressWarnings("unchecked")
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//List<Shipped_Line_Items>  shippedItemsList =  new LinkedList();
	ArrayList<Shipped_Line_Items> shippedItemsList = new ArrayList<Shipped_Line_Items>();
	ArrayList<Elmis_Stock_Control_Card> elmisstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<Losses_Adjustments_Types> facilityAdjustmentList = new LinkedList();
	List<Programs> facilityprogramsList = new LinkedList();
	ListIterator shipedItemsiterator = shippedItemsList.listIterator();
	ListIterator elmistockcontrolcarditerator = elmisstockcontrolcardList
			.listIterator();
	private Losses_Adjustments_Types registerAdjustmentsfacilitysccProducts;
	FacilityProductsDAO Facilityproductsmapper = null;
	FacilitySetUpDAO facilitiesmapper =  null;
	private int rnrid;
	private Timestamp shipmentdate;
	private Shipped_Line_Items shipped_line_items;
	private Shipped_Line_Items saveShipped_Line_Items;
	private Elmis_Stock_Control_Card elmis_stock_control_card;
	private Elmis_Stock_Control_Card savestockcontrolcard;
	private JCheckBox checkbox;
	//public static Boolean cansave = false;
	private String testAdjustcall;
	private String Pname = "";
	private String Pstrength = "";
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer Productid = 0;
	private int intQtyreceived = 0;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private Boolean Adjustmentadditive;
	private String facilityfromadjustmentname = "";
	private String facilitytoadjustmentname = "";
	ArrayList<Elmis_Stock_Control_Card> adjustmentstockcontrolcardList = new ArrayList<Elmis_Stock_Control_Card>();
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	//List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	//private VW_Systemcalculatedproductsbalance facilitysccProducts;
	public  List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	private final String[] columns_RegisterAdjustmentsfacilityApprovedproducts = {
		"Adjustment Type", "Quantity", "Remarks","Additive"};
	private static final int rows_RegisterAdjustmentsfproducts = 0;
    private final Object[] defaultv_RegisterAdjustmentsfacilityapprovedproducts = { "", "",
		"",""};

	public TableModel tableModel_RegisterAdjustments= new TableModel(
			columns_RegisterAdjustmentsfacilityApprovedproducts,
			defaultv_RegisterAdjustmentsfacilityapprovedproducts, rows_RegisterAdjustmentsfproducts) {

		boolean[] canEdit = new boolean[] { false, true, true,false};

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				
				if (columnIndex == 3) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0,3).getClass();
					// }	
				}
				if (columnIndex == 1) {
					//if(getValueAt(0, 3) != null){
					 // return Integer.class;
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		

	};


	/*private JComboBox facilityAdjustTypeList = new JComboBox();
	MutableComboBoxModel modelAdjustments = (MutableComboBoxModel) facilityAdjustTypeList
			.getModel();*/

	/** Creates new form receiveProducts 
	 * @wbp.parser.constructor*/
	public testMultipleAdjustments(java.awt.Frame parent, boolean modal,String adjustpcode,int adjustproductid,Timestamp adjustmentdateandtime,String progname,String pname, String pstrenth,Double adjusttotal,String adjustmentcaller,String formcaller) {
		super(parent, modal);
		
		Adjustmentform = formcaller;
		this.testAdjustcall =  adjustmentcaller;
		calAdjustmenttotal = adjusttotal;
		productdeliverdate = adjustmentdateandtime;
		Pcode = adjustpcode;
		Productid = adjustproductid;
		facilityprogramcode = progname;
		Pname = pname;
		Pstrength = pstrenth;
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(1001, 450);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//AdjustmentTypeJCB1.setPreferredSize(new Dimension(200, 30));

	}

	public testMultipleAdjustments(java.awt.Frame parent, boolean modal,String adjustpcode,int adjustproductid,Timestamp adjustmentdateandtime,String progname,String pname, String pstrenth,String formcaller) {
		super(parent, modal);
		
		Adjustmentform = formcaller;
		productdeliverdate = adjustmentdateandtime;
		Pcode = adjustpcode;
		Productid = adjustproductid;
		facilityprogramcode = progname;
		Pname = pname;
		Pstrength = pstrenth;
		setIconImage(Toolkit.getDefaultToolkit().getImage(storeAdjustmentTypeJD.class.getResource("/elmis_images/Add.png")));
		initComponents();
	   // digit = this.paramAdjustment;
		this.setSize(1001, 450);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		
		//AdjustmentTypeJCB1.setPreferredSize(new Dimension(200, 30));

	}

	
	
	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		SaveJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Record ARV Dispensary Adjustments ");
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowOpened(java.awt.event.WindowEvent evt) {
				formWindowOpened(evt);
			}
			public void windowActivated(java.awt.event.WindowEvent evt) {
				
				formwindowActivated(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));

		SaveJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		SaveJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		SaveJBtn.setText("Save");
		SaveJBtn.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
		SaveJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				SaveJBtnMouseClicked(evt);
			}
		});
		SaveJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				SaveJBtnActionPerformed(evt);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		
		JButton btnCancel = new JButton();
		btnCancel.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnCancelMouseClicked(evt);
			}

			
		});
		btnCancel.setText("Cancel");
		btnCancel.setFont(new Font("Ebrima", Font.BOLD, 12));
		
		enterfacilityname = new JLabel("Transfer from facility");
		enterfacilityname.setIcon(new ImageIcon(testMultipleAdjustments.class.getResource("/elmis_images/eLMIS basic info small.png")));
		enterfacilityname.setFont(new Font("Ebrima", Font.BOLD, 13));
		enterfacilityname.setForeground(Color.WHITE);
		
	   transfertofacilityJCB = new JComboBox();
	   transfertofacilityJCB.addItemListener(new ItemListener() {
	   	public void itemStateChanged(ItemEvent evt) {
	   		checkSelectedFacilitytoName(evt);
	   	}

		
	   });
		transfertofacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transfertofacilityJCBActionPerformed(evt);
			}
		});
		transfertofacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transfertofacilityJCB.setModel(modelfacilities);
	    transferoutJL = new JLabel("Transfer to facility");
		transferoutJL.setIcon(new ImageIcon(testMultipleAdjustments.class.getResource("/elmis_images/eLMIS basic info small.png")));
		transferoutJL.setForeground(Color.WHITE);
		transferoutJL.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		 transferfromfacilityJCB = new JComboBox();
		 transferfromfacilityJCB.addItemListener(new ItemListener() {
		 	public void itemStateChanged(ItemEvent evt) {
		 		checkSelectedFacilityfromName(evt);
		 	}
		 });
		transferfromfacilityJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				transferfromfacilityJCBActionPerformed(evt);
			}
		});
		transferfromfacilityJCB.setFont(new Font("Ebrima", Font.PLAIN, 13));
		transferfromfacilityJCB.setModel(modelfacilitiesList);
		
		JLabel lblNewLabel = new JLabel("Adjustments for : ");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Ebrima", Font.BOLD, 13));
		
		Adjustmentproductnamelbl = new JLabel("");
		Adjustmentproductnamelbl.setForeground(Color.WHITE);
		Adjustmentproductnamelbl.setFont(new Font("Ebrima", Font.BOLD, 14));
		
		transferoutrdbtn = new JRadioButton("Transfer Out");
		transferoutrdbtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				transferOutComboEnabled(evt);
			}
		});
		transferoutrdbtn.setForeground(Color.WHITE);
		transferoutrdbtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		transferoutrdbtn.setBackground(new java.awt.Color(102, 102, 102));
		
		transferinrdbtn = new JRadioButton("Transfer In");
		transferinrdbtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent evt) {
				transferInComboEnabled(evt);
			}
		});
		transferinrdbtn.setForeground(Color.WHITE);
		transferinrdbtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		transferinrdbtn.setBackground(new java.awt.Color(102, 102, 102));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(39)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(Adjustmentproductnamelbl, GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
					.addGap(424))
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(21)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addComponent(transferoutrdbtn, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
									.addGap(62)
									.addComponent(transferinrdbtn, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
								.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(transferfromfacilityJCB, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(transferoutJL)
									.addComponent(transfertofacilityJCB, GroupLayout.PREFERRED_SIZE, 294, GroupLayout.PREFERRED_SIZE))
								.addComponent(enterfacilityname))
							.addPreferredGap(ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
							.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 532, GroupLayout.PREFERRED_SIZE))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(SaveJBtn)))
					.addGap(47))
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
						.addComponent(Adjustmentproductnamelbl, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(transferoutrdbtn)
								.addComponent(transferinrdbtn))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(enterfacilityname)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transferfromfacilityJCB, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(transferoutJL)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(transfertofacilityJCB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(SaveJBtn)
						.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(238, Short.MAX_VALUE))
		);

	
		RegisterAdjustmentJT = new JTable();
		scrollPane.setViewportView(RegisterAdjustmentJT);
		RegisterAdjustmentJT.setShowVerticalLines(false);
		RegisterAdjustmentJT.setShowHorizontalLines(true);
		RegisterAdjustmentJT.setShowGrid(false);
		RegisterAdjustmentJT.setRowSelectionAllowed(true);
		RegisterAdjustmentJT.setModel(tableModel_RegisterAdjustments);
		RegisterAdjustmentJT.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		RegisterAdjustmentJT
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						RegisterAdjustmentJTMouseClicked(evt);
					}
				});
		RegisterAdjustmentJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						RegisterAdjustmentJTPropertyChange(evt);
					}

					
				});
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 995, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 559, Short.MAX_VALUE)
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>

	
	protected void transferOutComboEnabled(MouseEvent evt) {
		// TODO Auto-generated method stub
		
		this.Transfer_OUT = true;
		this.transfertofacilityJCB.setEnabled(true);
        this.transferoutJL.setEnabled(true);
        this.transferfromfacilityJCB.setEnabled(false);
        this.enterfacilityname.setEnabled(false);
        this.transferinrdbtn.setEnabled(false);
	}
	
	
	private void transferInComboEnabled(MouseEvent evt) {
		// TODO Auto-generated method stub
		    Transfer_IN = true;
			this.transferfromfacilityJCB.setEnabled(true);
	        this.enterfacilityname.setEnabled(true);
	        
			this.transfertofacilityJCB.setEnabled(false);
	        this.transferoutJL.setEnabled(false);
	        this.transferoutrdbtn.setEnabled(false);
			//this.createshipmentObj(Pcode, Productid, productdeliverdate);
		
	}
	
	
	private void checkSelectedFacilitytoName(ItemEvent evt) {
		// TODO Auto-generated method stub
				if (evt.getStateChange() == ItemEvent.SELECTED) {
	          Object item = evt.getItem();
	          // do something with object
	          selectedfacilitytoname = item.toString();
	       }
	}

	protected void checkSelectedFacilityfromName(ItemEvent evt) {
		
		// TODO Auto-generated method stub
				if (evt.getStateChange() == ItemEvent.SELECTED) {
			          Object item = evt.getItem();
			          // do something with object
			          selectedfacilityfromname = item.toString();
			       }
				//selecteddispensaryname = (String) cb.getSelectedItem();
	}

	
	
	protected void transferfromfacilityJCBActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		
		JComboBox cb = (JComboBox) evt.getSource();
		facilityfromadjustmentname = (String) cb.getSelectedItem();
		System.out.println(facilityfromadjustmentname);
		
	}


	protected void transfertofacilityJCBActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox) evt.getSource();
		facilitytoadjustmentname = (String) cb.getSelectedItem();
		System.out.println(facilitytoadjustmentname);
	}


	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(oldDateString);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);
			System.out.println(newDateString);
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		return productdeliverdate;

	}
	public  void RegisterAdjustmentfacilityapprovedProductsJTPropertyChange(
			PropertyChangeEvent evt) {

		System.out.println("test pop up adjustments");		

	}

	
	private void shipmentdatejFormattedTextFieldActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	@SuppressWarnings( { "unused", "unchecked" })
	private void PopulateProgram_ProductTable(List registerAdjustmentfapprovProducts) {
		checkbox = new JCheckBox();
		
		tableModel_RegisterAdjustments.clearTable();

		registeradjustmentfapprovedproductsIterator = registerAdjustmentfapprovProducts.listIterator();

			while (registeradjustmentfapprovedproductsIterator.hasNext()) {

				registerAdjustmentsfacilitysccProducts = registeradjustmentfapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");
				defaultv_RegisterAdjustmentsfacilityapprovedproducts[0] = registerAdjustmentsfacilitysccProducts.getName().toString();
				defaultv_RegisterAdjustmentsfacilityapprovedproducts[3]	 = registerAdjustmentsfacilitysccProducts.getAdditive();			
				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_RegisterAdjustmentsfacilityApprovedproducts.length; j++) {
					cols.add(defaultv_RegisterAdjustmentsfacilityapprovedproducts[j]);

				}
				System.out.print(cols.size()
						+ "What is the SIZE of Array to Table??");
				tableModel_RegisterAdjustments.insertRow(cols);

				registeradjustmentfapprovedproductsIterator.remove();
			

	}
	
	}	
	
	
	private void SaveJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		//TODO add your handling code here:
      
	
		////////
		
	    String dispensaryName = AppJFrame.getDispensingPointName("ARV");
		int siteId = AppJFrame.getDispensingId("ARV");
		int rowCount = RegisterAdjustmentJT.getRowCount();
		java.util.Date date = new java.util.Date();
		Timestamp timestamp = new Timestamp(date.getTime());
		aFacilityproductsmapper = new ARVDispensingDAO();
		Double showdouble = 0.0;   
		List<Adjustment> adjustmentList = new ArrayList<>();
		for (int i = 0; i < rowCount; i++)
		{
			try {
				
				
				Object adjustmentQtyObj = tableModel_RegisterAdjustments.getValueAt(i,1);
				Object adjustmentRmksObj = tableModel_RegisterAdjustments.getValueAt(i, 2);
				
				if (adjustmentQtyObj != null){
					
					//changes to validate for adjustment balance 
					Adjustment adjustment = new Adjustment();
					double positiveAdjustment = (Double.parseDouble
							(adjustmentQtyObj.toString()));
					if (positiveAdjustment < 0)
						positiveAdjustment = positiveAdjustment * -1;
					//check adjustment value against dispensary value
					productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(Pcode,AppJFrame.getDispensingId("ARV"));
					for (@SuppressWarnings("unused")ProductQty p : productcodesstockQtyList) {
						showdouble  = p.getQty();
					if(p.getQty() >= positiveAdjustment ){
					adjustment.setSiteId(siteId);
					adjustment.setProductCode(Pcode);
					adjustment.setDispensaryName(dispensaryName);
					adjustment.setAdditive(Boolean.parseBoolean(tableModel_RegisterAdjustments.getValueAt(i, 3).toString()));
					
					String adjustmentType = ""+RegisterAdjustmentJT.getValueAt(i, 0);
					adjustment.setAdjustmentType(adjustmentType);
					if (Adjustmentadditive)
						adjustment.setAdjustmentQty(positiveAdjustment);
					else
						adjustment.setAdjustmentQty(positiveAdjustment * -1);
					if (Transfer_IN){
						adjustment.setTransferFrom(selectedfacilityfromname);
						adjustment.setTransferTo("");
					}
					else if (Transfer_OUT){
						adjustment.setTransferTo(selectedfacilitytoname);
						adjustment.setTransferFrom("");
					}
					else{
						adjustment.setTransferTo("");
						adjustment.setTransferFrom("");
					}
					if (adjustmentRmksObj != null)
						adjustment.setAdjustmentRemark(adjustmentRmksObj.toString());
					else
						adjustment.setAdjustmentRemark("");
					adjustment.setTimestamp(timestamp);
					
					adjustmentList.add(adjustment);
				}else{
					
					if (!(Adjustmentadditive)){
					JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than product \n" +" dispensary balance \n\n" +
							"dispensary balance = "+showdouble,
										"Adjustment Error", JOptionPane.ERROR_MESSAGE);
					}
								
				}
				}
					
		}					
				
			}catch(NumberFormatException e){
				e.getStackTrace();
			}

		}
		
		if(!(adjustmentList.isEmpty())){
		// Need to reduce the quantities of balance at dispensary - Moses Kausa 2014
		new updateARVproductBalances(adjustmentList);
		
		new AdjustmentDao().saveArvAdjustments(adjustmentList);
		
		javax.swing.JOptionPane.showMessageDialog(this,Pname+" ("+Pcode+ ")\n adjustments have been saved.");
		dispose();
		}
		 if(!( this.transferoutrdbtn.isEnabled())){
		 this.transferoutrdbtn.setEnabled(true);
		 
		 }
		 if(!( this.transferinrdbtn.isEnabled())){
			 this.transferinrdbtn.setEnabled(true);
		 }
		
	}
	private void btnCancelMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		testMultipleAdjustments.this.dispose();
	}
	private void SaveJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}
	
	protected void RegisterAdjustmentJTMouseClicked(
			MouseEvent evt) {
		System.out.println("test me now ");
		this.adjustmentname = this.RegisterAdjustmentJT.getValueAt(RegisterAdjustmentJT.getSelectedRow(),0).toString();
		this.Adjustmentadditive = Boolean.parseBoolean(this.RegisterAdjustmentJT.getValueAt(RegisterAdjustmentJT.getSelectedRow(),3).toString());
		System.out.println(adjustmentname);
		
		if(this.adjustmentname.equals("TRANSFER_IN")) {
		//this.transferfromfacilityJCB.setEnabled(true);
        //this.enterfacilityname.setEnabled(true);
        
		//this.transfertofacilityJCB.setEnabled(false);
       // this.transferoutJL.setEnabled(false);
       
		//this.createshipmentObj(Pcode, Productid, productdeliverdate);
		}else if (this.adjustmentname.equals("TRANSFER_OUT")){
			//this.transfertofacilityJCB.setEnabled(true);
	      //  this.transferoutJL.setEnabled(true);
	      //  this.transferfromfacilityJCB.setEnabled(false);
	      //  this.enterfacilityname.setEnabled(false);
	        
	       
		}else{
			//this.transfertofacilityJCB.setEnabled(false);
			//this.transferfromfacilityJCB.setEnabled(false);;
	       // this.enterfacilityname.setEnabled(false);
	       // this.transferoutJL.setEnabled(false);
		}
		
	}
	
private boolean checktableCellinput(String s){
		
		 try { 
			 
			 if (s.contains(".")){
				Double.parseDouble(s) ;
			 }else{
		        Integer.parseInt(s); 
			 }
		    } catch(NumberFormatException e) { 
		        return false; 
		    }
		    // only got here if we didn't return false
		    return true;
}	
private void RegisterAdjustmentJTPropertyChange(
			PropertyChangeEvent evt) {
		// TODO Auto-generated method stub

if ("tableCellEditor".equals(evt.getPropertyName())) {       

	if (this.RegisterAdjustmentJT.isColumnSelected(1)) {
		System.out.println("IM READY TO POP FORM- 2014");
		int col = RegisterAdjustmentJT.getSelectedColumn();
		 if (RegisterAdjustmentJT.isEditing())
		    {
		    	//get the adjustment type , quantity entered , and  or remarks 
			
				colindex = this.RegisterAdjustmentJT
						.getSelectedColumn();
				rowindex = this.RegisterAdjustmentJT
						.getSelectedRow();
			
				
		    }else{
		    	
		    	 String num;
		    	   try{
				    num = (RegisterAdjustmentJT.getValueAt(RegisterAdjustmentJT.getSelectedRow(), 1).toString());
				    if(!(checktableCellinput(num))){
				    	JOptionPane.showMessageDialog(null, String.format("Adjustment Quantity must be a positive number",  num));	
				    	//RegisterAdjustmentJT.editCellAt(RegisterAdjustmentJT.getSelectedRow(), 1);
				    }
				    if((checktableCellinput(num))){   
		    	      createshipmentObj(Pcode, Productid, productdeliverdate);
				    }
		    	   }catch(java.lang.NullPointerException n){
		    		   n.getMessage();
		    	   }
	}					


}
	
		
		
}
	


	}



	public void createshipmentObj(String apcode, int prodid,
			Timestamp amodifieddate) {
		//System.out.println("Did object call me ???");
				//created a shipment object 
				Double A = 0.0;
				Double B = 0.0;
				double C = 0;
				double negativeadjust = 0.0;
				Double positive = 0.0;
				showdouble = 0.0 ;
				if (productdeliverdate == null) {
					java.util.Date date = new java.util.Date();
					amodifieddate = new Timestamp(date.getTime());
				} else {
					amodifieddate = productdeliverdate;
				}

				System.out.println(amodifieddate);
				System.out.println(A);
				System.out.println(B);
				System.out.println(C);
		
				
			     prodAdjustremark = this.tableModel_RegisterAdjustments.getValueAt(rowindex,2)
						.toString();
				

				if (!(prodAdjustremark.equals(""))) {
					Adjustmentelmis_stock_control_card.setAdjustmentRemark(prodAdjustremark);
				}
				Adjustmentelmis_stock_control_card.setProductCode(apcode);
			
				
		        if(!(Transfer_IN)){
				if (!(this.Adjustmentadditive)) {
					
				
					try {
						
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						Adjustmentelmis_stock_control_card.setAdjustmentQty(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * - 1;
						
						//changes Mkausa - 2014
							if(!(facilityprogramcode == null)){
							electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
							}	 
							for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
								this.showdouble =  b.getBalance();
								if(b.getBalance() >= positive ){
								if( b.getProductcode().equals(apcode)){
								
									@SuppressWarnings("unused")
									double increase = b.getBalance() - positive;
								//	Adjustmentelmis_stock_control_card.setBalance(increase);
								//negativeadjust = Adjustmentelmis_stock_control_card.getAdjustments();
									
								}
				
					if(!(this.facilitytoadjustmentname.equals(""))){
					Adjustmentelmis_stock_control_card.setTransferTo(this.facilitytoadjustmentname);
					}
					
					
					}else {
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
					
			
			}
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					

				}
	}	
				if(Transfer_IN){
				if (this.Adjustmentadditive) {
					
		
					try {
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustmentQty(positive);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}

					//Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					//Adjustmentelmis_stock_control_card.setQty_received(0.0);
					//Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.facilityfromadjustmentname);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
							//Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
					
						productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(apcode,AppJFrame.getDispensingId(facilityprogramcode));
						
				   /*		if(productcodesstockQtyList.size() > 0){
							for (@SuppressWarnings("unused")
							ProductQty p : productcodesstockQtyList) {
								
								if(p.getProduct_code().equals(apcode)){
									//update call here now 
									//p.setQty(p.getQty()- positive );
									p.setStockonhand_scc(p.getStockonhand_scc() + positive);
								Facilityproductsmapper.doupdatedispensaryqty(p.getProduct_code(),positive,p.getLocation(),p.getStockonhand_scc());
								}
								
								}
							
						
					}*/
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}

				}
				
				}	
				
				if (this.Adjustmentadditive) {
					
					
					try {
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustmentQty(positive);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}

					//Adjustmentelmis_stock_control_card.setQty_issued(0.0);
				//	Adjustmentelmis_stock_control_card.setQty_received(0.0);
					//Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(this.adjustmentname);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
						//	Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
					
					
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}

				}
				
			
			
				
				if (!(this.Adjustmentadditive)) {
				
	
					
		         try{
		        	 negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
		        	 if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						 Adjustmentelmis_stock_control_card.setAdjustmentQty(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							
							this.showdouble =  b.getBalance();
							if (b.getBalance() >= positive) {
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive ;
							//	Adjustmentelmis_stock_control_card.setBalance(increase);
							}

					//Adjustmentelmis_stock_control_card.setQty_received(0.0);
					//Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					//Adjustmentelmis_stock_control_card.setIssueto_receivedfrom(adjustmentname);
					
					
					}else {
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
					
			
				}
						
					  }catch(NumberFormatException e){
							e.getStackTrace();
						}
				}
				
				
				
				
		/*		
			 
				this.adjustmentname.trim();
				if (this.adjustmentname.equalsIgnoreCase("DAMAGED")) {
				
	
					
		         try{
		        	 negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
		        	 if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							this.showdouble = b.getBalance();
							if(b.getBalance() >= positive) {
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive ;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
		        

					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					
					}else{
						
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
						
					}
					
			
			}
						
					 }catch(NumberFormatException e){
							e.getStackTrace();
						}
				}
				 

				if (this.adjustmentname.equalsIgnoreCase("LOST")) {

					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						
					
						//changes Mkausa - 2014
							if(!(facilityprogramcode == null)){
							electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
							}
							for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
								this.showdouble = b.getBalance();
								if(b.getBalance() >= positive){
								if( b.getProductcode().equals(apcode)){
																		
									@SuppressWarnings("unused")
									double increase = b.getBalance() - positive ;
									Adjustmentelmis_stock_control_card.setBalance(increase);
								}
						 
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
					
				}
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
				}
					 
				
				if (this.adjustmentname.equalsIgnoreCase("STOLEN")) {

					
					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
					
					
							//changes Mkausa - 2014
							if(!(facilityprogramcode == null)){
							electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
							}
							for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
								this.showdouble = b.getBalance();
								if(b.getBalance() >= positive){
								if( b.getProductcode().equals(apcode)){
									
									
									@SuppressWarnings("unused")
									double increase = b.getBalance() - positive;
									Adjustmentelmis_stock_control_card.setBalance(increase);
								} 
						 
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
			
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
					
					}
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
				}
					 
				

				if (this.adjustmentname.equalsIgnoreCase("EXPIRED")) {

								
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							this.showdouble = b.getBalance();
							if(b.getBalance() >= positive ){
							if( b.getProductcode().equals(apcode)){
								
								
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive ;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}
					
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
				
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
					
					
					}		
						
					
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					
					
				}
				

				if (this.adjustmentname.equalsIgnoreCase("PASSED_OPEN_VIAL_TIME_LIMIT")) {
					

					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					} 
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
							
							
							@SuppressWarnings("unused")
							double increase = b.getBalance() - positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					  try{
					
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				

				if (this.adjustmentname.equalsIgnoreCase("COLD_CHAIN_FAILURE")) {
					

					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
												
							@SuppressWarnings("unused")
							double increase = b.getBalance() -positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
						try{
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
					
				}
				
				if (this.adjustmentname.equalsIgnoreCase("CLINIC_RETURN")) {
					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}  
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Loss");
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
										
							@SuppressWarnings("unused")
							double increase = b.getBalance() - positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}
					}
					
					
					try{
						
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
		if (this.adjustmentname.equalsIgnoreCase("COMPUTER_GENERATED (-)")) {
					
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
						System.out.println(negativeadjust);
						
						positive = negativeadjust * -1;
						//changes Mkausa - 2014
						if(!(facilityprogramcode == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
						}
						for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
							this.showdouble = b.getBalance();
							if(b.getBalance() >= positive){
							if( b.getProductcode().equals(apcode)){
											
								@SuppressWarnings("unused")
								double increase = b.getBalance() - positive;
								Adjustmentelmis_stock_control_card.setBalance(increase);
							}	
					
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Computer Generated Adjusment (-)");
					
					}else{
						JOptionPane.showMessageDialog(this, "Adjustment value can not be greater than  product store balance \n\n" +
								"Store balance = "+showdouble,
											"Adjustment Error", JOptionPane.ERROR_MESSAGE);
									showdouble = 0.0;
									tableModel_RegisterAdjustments.setValueAt(showdouble, rowindex,1);
					}
					
						}
					
						
					}catch(NumberFormatException e){
						e.getStackTrace();
					}  
				}
				
		
		
		
		if (this.adjustmentname.equalsIgnoreCase("FOUND")) {

			try{
				negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
								.toString()));
				if (negativeadjust < 0)
					negativeadjust = negativeadjust * -1;
				negativeadjust = - negativeadjust;
				System.out.println(negativeadjust);
				positive = negativeadjust * -1;
				 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
			}catch(NumberFormatException e){
				e.getStackTrace();
			}
			Adjustmentelmis_stock_control_card.setQty_received(0.0);
			Adjustmentelmis_stock_control_card.setQty_issued(0.0);
			
			
			//changes Mkausa - 2014
			if(!(facilityprogramcode == null)){
			electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
			}
			for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
				if( b.getProductcode().equals(apcode)){
									
					@SuppressWarnings("unused")
					double increase = b.getBalance() + positive;
					Adjustmentelmis_stock_control_card.setBalance(increase);
				}else{
					Adjustmentelmis_stock_control_card.setBalance(positive);
				}
			}
			
			
			
			try{
			
	
				
				}catch (java.lang.NullPointerException j)	{
					j.getMessage();
				}
		}
				
				if (this.adjustmentname.equalsIgnoreCase("COMPUTER_GENERATED(+)")) {

					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
								.toString()));
				        if (negativeadjust < 0){
					      negativeadjust = negativeadjust * -1;
				       }
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					Adjustmentelmis_stock_control_card.setIssueto_receivedfrom("Computer Generated Adjusment");
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					
					try{
					
		
						
						}catch (java.lang.NullPointerException j)	{
							j.getMessage();
						}
				}
				
				if (this.adjustmentname.equalsIgnoreCase("PURCHASED")) {
					
	
					try{
						negativeadjust = (Double.parseDouble(tableModel_RegisterAdjustments.getValueAt(rowindex,1)
										.toString()));
						if (negativeadjust < 0)
							negativeadjust = negativeadjust * -1;
						negativeadjust = - negativeadjust;
						System.out.println(negativeadjust);
						positive = negativeadjust * -1;
						 Adjustmentelmis_stock_control_card.setAdjustments(negativeadjust);
					}catch(NumberFormatException e){
						e.getStackTrace();
					}
					Adjustmentelmis_stock_control_card.setQty_received(0.0);
					Adjustmentelmis_stock_control_card.setQty_issued(0.0);
					
					//changes Mkausa - 2014
					if(!(facilityprogramcode == null)){
					electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(facilityprogramcode,apcode);
					}
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(apcode)){
											
							@SuppressWarnings("unused")
							double increase = b.getBalance() + positive ;
							Adjustmentelmis_stock_control_card.setBalance(increase);
						}else{
							Adjustmentelmis_stock_control_card.setBalance(positive);
						}
					}
					
					
					try{
					
						
						}catch (java.lang.NullPointerException j){
							j.getMessage();
						}
			
	}
	
	*/
			//	Adjustmentelmis_stock_control_card.setId(this.GenerateGUID());
				///A = Adjustmentelmis_stock_control_card.getQty_received();
				//B = Adjustmentelmis_stock_control_card.getQty_issued();
				//C = Adjustmentelmis_stock_control_card.getAdjustments();

			  
				System.out.println(A + "WHAT IS  THE NUMBER");
				System.out.println(B);
				System.out.println(C);

			//	Adjustmentelmis_stock_control_card			.setAdjustmenttype(this.adjustmentname);
				//System.out.println(Adjustmentelmis_stock_control_card.getBalance());
				//elmis_stock_control_card.setBalance(elmis_stock_control_card.getBalance()+ (A + B +C));
			//	Adjustmentelmis_stock_control_card.setStoreroomadjustment(true);
			//	Adjustmentelmis_stock_control_card.setProgram_area(facilityprogramcode);
			//	Adjustmentelmis_stock_control_card.setCreatedby(AppJFrame.userLoggedIn);
			//	Adjustmentelmis_stock_control_card.setCreateddate(amodifieddate);

				//created linked list object 

				//adjustmentstockcontrolcardList.add(savestockcontrolcard);
				//adjustmentstockcontrolcardList.add(Adjustmentelmis_stock_control_card);
				System.out.println(adjustmentstockcontrolcardList.size()
						+ "ARRAY SIZE???");
				System.out.println("Array size here &&&");
				//Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
				//return saveShipped_Line_Items; 		
 }

	private void formwindowActivated(java.awt.event.WindowEvent evt){
		this.transferfromfacilityJCB.setEnabled(false);
		this.transferfromfacilityJCB.setEnabled(false);
		this.transfertofacilityJCB.setEnabled(false);
       this.enterfacilityname.setEnabled(false);
       transferoutJL.setEnabled(false);
	}
	

	private void formWindowOpened(java.awt.event.WindowEvent evt) {
		// TODO add your handling code here:
		tablecolumnaligner =  new TableColumnAligner();
		Facilityproductsmapper = new FacilityProductsDAO();
		elmis_stock_control_card = new Elmis_Stock_Control_Card();
		facilitiesmapper = new FacilitySetUpDAO();
		facilitiesnameList = facilitiesmapper.getfacilitynameList();
		
		for (facilities f : facilitiesnameList) {
			modelfacilities.addElement(f.getName());

		}
        
		for (facilities f : facilitiesnameList) {
			this.modelfacilitiesList.addElement(f.getName());

		}
		//AutoCompleteDecorator.decorate(this.transferfromfacilityJCB);
		//AutoCompleteDecorator.decorate(this.transfertofacilityJCB);

	
		facilityAdjustmentList = Facilityproductsmapper.selectAllAdjustments();
		this.RegisterAdjustmentJT.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		RegisterAdjustmentJT.setFont(new java.awt.Font(
				"Ebrima", 0, 20));
		this.RegisterAdjustmentJT.setRowHeight(30);
		this.PopulateProgram_ProductTable(facilityAdjustmentList);

		if (!this.Pname.equals("")) {

				//if the name is too long cut it short for display purpose
				if (this.Pname.length() >= 40) {

					this.Adjustmentproductnamelbl.setText(Pname.substring(
							0, 40) + "\n ," + " " + Pstrength);
				} else {

					Adjustmentproductnamelbl.setText(Pname + " " + Pstrength);
				}
			}

	//	Adjustmentelmis_stock_control_card = new Elmis_Stock_Control_Card();
		
	}
	
		/**
	 * @param args the command line arguments
	 */

	private javax.swing.JButton SaveJBtn;
	private javax.swing.JPanel jPanel1;
	private JTable RegisterAdjustmentJT;
	private JLabel enterfacilityname;
	private JLabel transferoutJL;
	private JLabel Adjustmentproductnamelbl;
	@SuppressWarnings("rawtypes")
	private static JComboBox transfertofacilityJCB;
	@SuppressWarnings("rawtypes")
	private  JComboBox  transferfromfacilityJCB;
	private JRadioButton transferoutrdbtn;
	private JRadioButton transferinrdbtn;
}
	
	/**********************************************************
	 * END CHILD FORM 
	 **********************************************************/
			 
