package org.elmis.forms.admin.dispensingpoints;

import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Dispensing_point;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.utils.SdpStyleSheet;

import com.oribicom.tools.TableModel;


public class DispensingPointAdminJD extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// dispensingPointJTable *********************************************
	private static final String[] columns_dispensingPoint = { "Name",
			 "Description" };
	private static final Object[] defaultv_dispensingPoint = { "", "" };
	private static final int rows_dispensingPoint = 0;
	public static TableModel tableModel_dispensingPoint = new TableModel(
			columns_dispensingPoint, defaultv_dispensingPoint,
			rows_dispensingPoint);
	public static int total_dispensingPoint = 0;
	public static Map parameterMap_dispensingPoint = new HashMap();

	private static Dispensing_point dp;// 

	private static ListIterator<Dispensing_point> dpIterator;

	List<Dispensing_point> dpList = new LinkedList();

	public DispensingPointAdminJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);

		initComponents();

		class RightTableCellRenderer extends DefaultTableCellRenderer {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			protected RightTableCellRenderer() {
				setHorizontalAlignment(SwingConstants.RIGHT);
			}

		}

		RightTableCellRenderer rightAlign = new RightTableCellRenderer();

		this.dispensingPointsJTable.getColumnModel().getColumn(0)
				.setPreferredWidth(50);
		//this.dispensingPointsJTable.getColumnModel().getColumn(1)
		//		.setPreferredWidth(50);
		this.dispensingPointsJTable.getColumnModel().getColumn(1)
				.setPreferredWidth(120);

		this.setSize((AppJFrame.desktop.getWidth() / 2 + AppJFrame.desktop
				.getWidth() / 7), AppJFrame.desktop.getHeight() / 2
				+ AppJFrame.desktop.getWidth() / 7);
		this.setLocationRelativeTo(null);

		this.setVisible(true);
		this.dispensingPointsJTable.getColumnModel().getColumn(1).setWidth(1);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		dispensingPointsJTable = new javax.swing.JTable();
		addUserJL = new javax.swing.JLabel();
		printListJL1 = new javax.swing.JLabel();
		jSeparator3 = new javax.swing.JSeparator();
		viewUsersJL = new javax.swing.JLabel();
		deletedpJL = new javax.swing.JLabel();
		editUserDetailsJL = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Dispensing Points");

		SdpStyleSheet.configJPanelBackground(jPanel1);
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		SdpStyleSheet.configTable(dispensingPointsJTable);
		dispensingPointsJTable.setModel(tableModel_dispensingPoint);
		jScrollPane1.setViewportView(dispensingPointsJTable);

		addUserJL.setFont(new java.awt.Font("Gulim", 0, 11));
		addUserJL.setForeground(new java.awt.Color(255, 255, 255));
		addUserJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/add18x18_icon.png"))); // NOI18N
		addUserJL.setText("Add new dispensing point");
		addUserJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				addUserJLMouseClicked(evt);
			}
		});

		printListJL1.setFont(new java.awt.Font("Gulim", 0, 11));
		printListJL1.setForeground(new java.awt.Color(255, 255, 255));
		printListJL1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/PRINT.PNG"))); // NOI18N
		printListJL1.setText("Print list");
		printListJL1.setToolTipText("Print List");
		printListJL1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				printListJL1MouseClicked(evt);
			}
		});

		jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

		viewUsersJL.setFont(new java.awt.Font("Gulim", 0, 11));
		viewUsersJL.setForeground(new java.awt.Color(255, 255, 255));
		viewUsersJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view user small.png"))); // NOI18N
		viewUsersJL.setText("View Dispensing Points");
		viewUsersJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewUsersJLMouseClicked(evt);
			}
		});

		deletedpJL.setFont(new java.awt.Font("Gulim", 0, 11));
		deletedpJL.setForeground(new java.awt.Color(255, 255, 255));
		deletedpJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS delete role small.png"))); // NOI18N
		deletedpJL.setText("Delete dispensing point");
		deletedpJL.setToolTipText("Delete user");
		deletedpJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				deletedpJLMouseClicked(evt);
			}
		});

		editUserDetailsJL.setFont(new java.awt.Font("Gulim", 0, 11));
		editUserDetailsJL.setForeground(new java.awt.Color(255, 255, 255));
		editUserDetailsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS change password small.png"))); // NOI18N
		editUserDetailsJL.setText("Edit dispensing point details");
		editUserDetailsJL.setToolTipText("Edit user details");
		editUserDetailsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				editUserDetailsJLMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jScrollPane1,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																810,
																Short.MAX_VALUE)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				viewUsersJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				addUserJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				jSeparator3,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				11,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				editUserDetailsJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				deletedpJL)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				printListJL1)))
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addGroup(
																jPanel1Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								viewUsersJL)
																						.addComponent(
																								addUserJL))
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								editUserDetailsJL)
																						.addComponent(
																								deletedpJL)
																						.addComponent(
																								printListJL1)))
														.addComponent(
																jSeparator3,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																10,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												512, Short.MAX_VALUE)
										.addContainerGap()));

		jPanel1Layout
				.linkSize(javax.swing.SwingConstants.VERTICAL,
						new java.awt.Component[] { addUserJL, jSeparator3,
								printListJL1 });

		jTabbedPane1.addTab("Dispensing Points", new javax.swing.ImageIcon(
				getClass().getResource(
						"/elmis_images/eLMIS dispensing point small.png")),
				jPanel1); // NOI18N

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jTabbedPane1));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE,
						587, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void editUserDetailsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:

		//if jtable has content
		if (tableModel_dispensingPoint.getRowCount() > 0) {

			//if use is selected
			if (dispensingPointsJTable.getSelectedRow() >= 0) {

				Dispensing_point dp = new Dispensing_point();

				try {
					dp.setName(tableModel_dispensingPoint.getValueAt(
							dispensingPointsJTable.getSelectedRow(), 0)
							.toString());
					dp.setDescription(tableModel_dispensingPoint.getValueAt(
							dispensingPointsJTable.getSelectedRow(), 1)
							.toString());
					/*dp.setLocation(tableModel_dispensingPoint.getValueAt(
							dispensingPointsJTable.getSelectedRow(), 2)
							.toString());
					*/
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				new EditDispensingPointJD(javax.swing.JOptionPane
						.getFrameForComponent(this), true, dp);

			} else {

				javax.swing.JOptionPane.showMessageDialog(this,
						"Please select Dispensing Point from list");
			}

		} else {

			javax.swing.JOptionPane
					.showMessageDialog(
							this,
							"Please click on view Dispensing Point , then select the Dispensing Point you wish to edit");
		}

	}

	private void deletedpJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:

		//if jtable has content
		if (tableModel_dispensingPoint.getRowCount() > 0) {

			//if use is selected
			if (dispensingPointsJTable.getSelectedRow() >= 0) {

				Dispensing_point dp = new Dispensing_point();

				try {
					dp.setName(tableModel_dispensingPoint.getValueAt(
							dispensingPointsJTable.getSelectedRow(), 0)
							.toString());
					dp.setDescription(tableModel_dispensingPoint.getValueAt(
							dispensingPointsJTable.getSelectedRow(), 1)
							.toString());
					/*dp.setLocation(tableModel_dispensingPoint.getValueAt(
							dispensingPointsJTable.getSelectedRow(), 2)
							.toString());
					*/
					int ok = javax.swing.JOptionPane.showConfirmDialog(this,
							"Are you sure you would like to delete Dispensing Point: "
									+ dp.getName());

					if (ok == 0) {

						SqlSessionFactory factory = new MyBatisConnectionFactory()
								.getSqlSessionFactory();

						SqlSession session = factory.openSession();

						try {

							session.update("deleteBydpPrimaryKey", dp);
							session.commit();

							dpList = session.selectList("selectBydp");

							populatedpTable(dpList);

						} finally {

							session.close();
							javax.swing.JOptionPane.showMessageDialog(this,
									"Dispensing Point deleted ");

						}

						// reload table 

					}

				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {

				javax.swing.JOptionPane.showMessageDialog(this,
						"Please select Dispensing Point from list");
			}

		} else {

			javax.swing.JOptionPane
					.showMessageDialog(
							this,
							"Please click on view Dispensing Poin , then select the Dispensing Poin you wish to delete");
		}

	}

	private void viewUsersJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		new MyBatisConnectionFactory();
		SqlSessionFactory factory = MyBatisConnectionFactory
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			dpList = session.selectList("selectBydp");

			populatedpTable(dpList);

		} finally {
			session.close();
		}
	}

	private void addUserJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		new AddDispensingPointJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);
	}

	private void printListJL1MouseClicked(java.awt.event.MouseEvent evt) {

		try {
			dispensingPointsJTable.print();
		} catch (PrinterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void populatedpTable(List dataList) {

		tableModel_dispensingPoint.clearTable();

		dpIterator = dataList.listIterator();

		while (dpIterator.hasNext()) {

			dp = dpIterator.next();
			defaultv_dispensingPoint[0] = dp.getName();
			//defaultv_dispensingPoint[1] = dp.getLocation();
			defaultv_dispensingPoint[1] = dp.getDescription();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_dispensingPoint.length; j++) {
				cols.add(defaultv_dispensingPoint[j]);

			}

			tableModel_dispensingPoint.insertRow(cols);

			dpIterator.remove();
		}

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				DispensingPointAdminJD dialog = new DispensingPointAdminJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	/*
	 * File Balances
	 * ************************************************************
	 * ********************
	 */

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel addUserJL;
	private javax.swing.JLabel deletedpJL;
	private javax.swing.JTable dispensingPointsJTable;
	private javax.swing.JLabel editUserDetailsJL;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JSeparator jSeparator3;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JLabel printListJL1;
	private javax.swing.JLabel viewUsersJL;
	// End of variables declaration//GEN-END:variables

}