/*
 * SelectProductsJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest.physicalcount;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.elmis.facility.domain.dao.HivTestResultDao;
import org.elmis.facility.domain.model.HivTestPhysicalCount;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.tools.MessageDialog;
import org.elmis.facility.utils.ElmisAESencrpDecrp;

import com.oribicom.tools.publicMethods;
import com.toedter.calendar.JDateChooser;

/**
 * 
 * @author Joe Banda
 */
public class SelectProductToCountJD extends javax.swing.JDialog {

	private String searchText = "";
	private char letter;
	private JDateChooser dateChooser;
	private Date todayDate;
	private List<Character> charList = new LinkedList<>();
	public static String barcodeScanned;
	public static int selectedProductID;
	public static Products selectedProduct;
	public int dispensaryId;
	private List<Products> productsList = new LinkedList<>();
	private List<HivTestPhysicalCount> PhysicalCountList = new LinkedList<>();
	private HivTestPhysicalCount physicalCount;

	/** Creates new form SelectProductsJD */
	public SelectProductToCountJD(java.awt.Frame parent, boolean modal,	String title, String programArea, int dispensaryId) {
		super(parent, modal);
		this.dispensaryId = dispensaryId;
		initComponents();

		this.setTitle(title);
		this.setSize(479, 344);
		this.setLocationRelativeTo(null);

		jTabbedPane1.addTab("Products", new ProductsJP(2, dispensaryId));

		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		saveCountJBtn = new javax.swing.JButton();
		cancelJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Stock Control card");

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});

		saveCountJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		saveCountJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png"))); // NOI18N
		saveCountJBtn.setText("Save Count ");
		saveCountJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				saveCountJBtnActionPerformed(evt);
			}
		});

		cancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.setText("Cancel");
		cancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelJBtnActionPerformed(evt);
			}
		});
		todayDate = new Date();
		dateChooser = new JDateChooser();
		dateChooser.setDateFormatString("dd-MM-yyyy");
		dateChooser.setDate(todayDate);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGap(27)
					.addComponent(saveCountJBtn)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(cancelJBtn, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(131, Short.MAX_VALUE))
				.addComponent(jTabbedPane1, GroupLayout.DEFAULT_SIZE, 403, Short.MAX_VALUE)
				.addGroup(Alignment.TRAILING, layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, 139, GroupLayout.PREFERRED_SIZE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(dateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(jTabbedPane1, GroupLayout.PREFERRED_SIZE, 198, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(saveCountJBtn, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
						.addComponent(cancelJBtn, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		layout.linkSize(SwingConstants.VERTICAL, new Component[] {saveCountJBtn, cancelJBtn});
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void cancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		new MessageDialog().showDialog(this, "Action Cancelled", "Cancelled","Ok");
		this.dispose();
	}

	private void saveCountJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		/*HivTestResultDao hivTestResultDao = new HivTestResultDao();
		TableModel hivTableModel = new ProductsJP(1).getJTable().getModel();
		for (int i = 0; i < hivTableModel.getRowCount(); i++){
			Object productObj = hivTableModel.getValueAt(i, 0);
			Object physicalCountQtyObj = hivTableModel.getValueAt(i, 2);
			System.out.println("****");
			try{
				Integer physicalCount = (Integer
						.parseInt(physicalCountQtyObj.toString()));
				System.out.println("#****");
				System.out.println(productObj+" : "+physicalCount);
				System.out.println("****%%%%");
			}
			catch(NumberFormatException e){
				System.out.println(e.getMessage());
			}
			catch(Exception e){
				System.out.println(e.getMessage()+"<----");
			}
		}*/

		// TODO add your handling code here:
		if (dateChooser.getDate().compareTo(todayDate) <= 0){
			ProductsJP.tableModel_products.getRowCount();

			for (int x = 0; x < ProductsJP.tableModel_products.getRowCount(); x++) {

				// if qty field is empty skip the item
				if (!ProductsJP.tableModel_products.getValueAt(x, 2).toString()
						.equals("")) {

					physicalCount = new HivTestPhysicalCount();

					physicalCount.setId(publicMethods.createGUID());

					physicalCount.setQty_count(Integer
							.parseInt(ProductsJP.tableModel_products.getValueAt(x,
									2).toString()));

					physicalCount.setCount_type("Begining count");
					physicalCount.setTesting_site(System.getProperty("dp_name"));
					physicalCount.setCreated_by(AppJFrame.userLoggedIn);
					physicalCount.setProduct_code(ProductsJP.tableModel_products
							.getValueAt(x, 0).toString());

					PhysicalCountList.add(physicalCount);
				} // end if qty field is empty

			}
			CalendarUtil cal = new CalendarUtil();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			java.sql.Date datum = cal.getSqlDate(sdf.format(dateChooser.getDate()));
			if (!PhysicalCountList.isEmpty()) {
				new HivTestResultDao().insertPhysicalCount(datum, dispensaryId, PhysicalCountList);
				new MessageDialog().showDialog(this, "Physical Count", "Saved",	"Ok  ");

				this.dispose();

			} else {

				//javax.swing.JOptionPane.showMessageDialog(this,"Saved");
				new MessageDialog().showDialog(this,
						"You have not added anything to count", "Error", "Close  ");
			}
		}
		else if(dateChooser.getDate().compareTo(todayDate) > 0){
			new MessageDialog().showDialog(this,
					"This is a backlog...t", "Error", "Close  ");
		}
		else
			new MessageDialog().showDialog(this,
					"Physical count cannot be carried out in a future date", "Date Error", "Close  ");
	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {

		if (jTabbedPane1.getSelectedIndex() == 1
				&& jTabbedPane1.getTabCount() == 3) {// &&

			jTabbedPane1.remove(2);
		}

		if (jTabbedPane1.getSelectedIndex() == 0
				&& jTabbedPane1.getTabCount() == 2) {// &&

			// remove the products JTable
			jTabbedPane1.remove(1);

		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				Properties prop = new Properties(System.getProperties());
				FileInputStream fis = null;
				try {
					fis = new FileInputStream("Programmproperties.properties");
					prop.load(fis);
					System.setProperties(prop);
					fis.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
				String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
				System.setProperty("dbpassword", dbpassword);
				Date date = new Date();
				SelectProductToCountJD dialog = new SelectProductToCountJD(
						new javax.swing.JFrame(), true, new String(),new String(), 1);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton cancelJBtn;
	public static javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JButton saveCountJBtn;
}