package org.elmis.forms.regimen.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import org.elmis.facility.utils.ElmisAESencrpDecrp;

public class GenericUtil {
	public static boolean isInteger(String str)  
	{  
	  try  
	  {  
	    int i = Integer.parseInt(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	public static boolean isDouble(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	public static boolean isNumeric(String str)  
	{  
	  try  
	  {  
	    double d = Double.parseDouble(str);  
	  }  
	  catch(NumberFormatException nfe)  
	  {  
	    return false;  
	  }  
	  return true;  
	}
	
	public static Properties getProperties() {
		Properties prop = new Properties(System.getProperties());
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("Programmproperties.properties");
			prop.load(fis);

			System.setProperties(prop);

			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
		System.setProperty("dbpassword", dbpassword);
		return System.getProperties();
	}
	public static void clearTable(JTable table){
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
	}
}
