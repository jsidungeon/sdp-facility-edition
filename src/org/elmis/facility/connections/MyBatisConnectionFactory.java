package org.elmis.facility.connections;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.utils.ElmisProperties;

/**
 * MyBatisConnectionFactory.java Purpose: Manages all connection resources
 * (Opening & closing) database(s).
 * 
 * @author Michael Mwebaze
 * @version 1.0
 */
public class MyBatisConnectionFactory {

	private static SqlSessionFactory sqlSessionFactory;

	static {
		try {

			String resource = null;
			Reader reader = null;

			resource = "postgres-mybatis-config.xml";
			reader = Resources.getResourceAsReader(resource);
			if (sqlSessionFactory == null) {
				sqlSessionFactory = new SqlSessionFactoryBuilder().build(
						reader, System.getProperties());
			}
		} catch (FileNotFoundException fileNotFoundException) {
			fileNotFoundException.printStackTrace();
		} catch (IOException iOEx) {
			System.out.println("Database connection issues "
					+ iOEx.getMessage());
		}
	}

	/**
	 * 
	 * @return an SqlSessionFactory which is used to open or close a database
	 *         connection resource. Each open SqlSessionFactory that is opened
	 *         must after use be closed
	 */
	public static SqlSessionFactory getSqlSessionFactory() {

		return sqlSessionFactory;
	}
}