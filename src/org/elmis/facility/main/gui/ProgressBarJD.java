package org.elmis.facility.main.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.sql.Date;

import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.JLabel;

import org.elmis.facility.reporting.generator.ReportGenerator;


public class ProgressBarJD extends JDialog implements Runnable{
	private int siteId;
	private Date date;

	/**
	 * Create the dialog.
	 */
	public ProgressBarJD(String message, Date date, int siteId) {
		setTitle("Please wait processing.....");
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		setVisible(true);
		setResizable(false);
		this.siteId = siteId;
		this.date = date;
		setBounds(100, 100, 424, 148);
		setLocationRelativeTo(null);
		JProgressBar progressBar = new JProgressBar();
		progressBar.setIndeterminate(true);
		getContentPane().add(progressBar, BorderLayout.SOUTH);
		
		JLabel messageLable = new JLabel(message);
		getContentPane().add(messageLable, BorderLayout.CENTER);
		//new Thread(new ReportThread(this, siteId)).start();

	}

	@Override
	public void run() {
		ReportGenerator reportGenerator = new ReportGenerator();
		reportGenerator.generateHivDailyActivityRegister(date, siteId);
		dispose();
		
	}

}
