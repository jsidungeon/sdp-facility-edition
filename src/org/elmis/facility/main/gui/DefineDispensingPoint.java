package org.elmis.facility.main.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableColumn;

import org.elmis.facility.domain.model.Dispensary;
import org.elmis.facility.generic.GenericDao;

public class DefineDispensingPoint extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel pnlMain = new JPanel();
	private JTextField txtDispensationPoint;
	private DefineDispensingPoint _this = this;
	private JComboBox cboPrograms;
	private Integer siteId;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DefineDispensingPoint dialog = new DefineDispensingPoint(null, true , 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("serial")
	private void setUpRegimenProductDosagesTable() {
	
		pnlMain.setLayout(null);
				
	}
	


	private void hideColumn(TableColumn column){
		column.setMinWidth(0);
		column.setMaxWidth(0);
		column.setPreferredWidth(0);
	}
	
	private void resetForm(){
		_this.txtDispensationPoint.setText("");
	}

	private Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prop;
	}

	/**
	 * Create the dialog.
	 */
	public DefineDispensingPoint(java.awt.Frame parent, boolean modal , Integer siteId) {
		super(parent, modal);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
			}
		});
		_this.siteId = siteId;
		setTitle("Create Dispensing Point");
		setResizable(false);
		setBounds(100, 100, 529, 202);
		getContentPane().setLayout(new BorderLayout());
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnlMain, BorderLayout.CENTER);
		{
			setUpRegimenProductDosagesTable();
		}
		
		JSeparator separator = new JSeparator();
		separator.setBounds(438, 60, 1, 2);
		pnlMain.add(separator);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 24, 499, 135);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlMain.add(panel);
		panel.setLayout(null);
				
						
								txtDispensationPoint = new JTextField();
								txtDispensationPoint.setBounds(189, 49, 256, 20);
								panel.add(txtDispensationPoint);
								txtDispensationPoint.setColumns(10);
								
										JLabel lblProgramArea = new JLabel("Program Area");
										lblProgramArea.setBounds(20, 21, 134, 14);
										panel.add(lblProgramArea);
										
										String[] programs = {"ARV" , "HIV" , "LAB" , "EM"};
										cboPrograms = new JComboBox(programs);
										cboPrograms.setBounds(189, 18, 256, 20);
										panel.add(cboPrograms);
										
										JLabel lblDispensingPoint = new JLabel("Dispensing Point");
										lblDispensingPoint.setBounds(20, 57, 134, 14);
										panel.add(lblDispensingPoint);
										
										JButton btnSave = new JButton("Save");
										btnSave.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent arg0) {
												int confirm = JOptionPane.showOptionDialog(_this,  "Confrim save!" , "Confirm Save",
														JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
														null, null, null);
												
												if(confirm == JOptionPane.YES_OPTION){
													GenericDao dao = GenericDao.getInstance();
													Dispensary disp = new Dispensary();
													disp.setDispensingPointName(_this.txtDispensationPoint.getText().trim());
													disp.setSiteId(_this.siteId);
													disp.setProgramArea(_this.cboPrograms.getSelectedItem().toString());
													dao.insert(disp);
													JOptionPane.showMessageDialog(_this, "Dispensing point created!");	
												} 
												_this.resetForm();
											}
										});
										btnSave.setBounds(161, 85, 89, 23);
										panel.add(btnSave);
										
										this.setVisible(true);
	}
}
