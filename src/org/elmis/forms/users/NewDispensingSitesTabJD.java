/*
 * created by Friday Simfukwe
 *
 * 19th May 2014
 */

package org.elmis.forms.users;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Rights;
import org.elmis.facility.domain.model.Role_rights;
import org.elmis.facility.domain.model.Roles;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.main.gui.DefineDispensingPoint;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.tools.CustomTableCellRenderer;
import org.elmis.facility.utils.SdpStyleSheet;
import org.elmis.forms.Roles.AddNewRoleJD;
import org.elmis.forms.Roles.EditRoleJD;

import com.oribicom.tools.TableModel;
import com.oribicom.tools.publicMethods;

/**
 *
 * @author  __USER__
 */
public class NewDispensingSitesTabJD extends javax.swing.JDialog implements WindowListener
 {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static java.util.Date eventDate;
	public Integer MySiteID=0;

	//Dispensing Site JTable *********************************************
	private static final String[] columns_userAdmin = { "Site Description",
			"Purpose", "Site Name", "Site Role","" };
	private static final Object[] defaultv_userAdmin = { "", "", "", "", ""};
	private static final int rows_userAdmin = 0;
	public static TableModel tableModel_userAdmin = new TableModel(
			columns_userAdmin, defaultv_userAdmin, rows_userAdmin);
	public static int total_userAdmin = 0;
	public static Map parameterMap_userAdmin = new HashMap();

	String selectedAccountId;//from the accountName JCBox
	String caseId;

	//file Balances **************************************

	private static final String[] fileBalanceColumns = { "", "Client Name",
			"File Number", "Matter Title", "Bal on a/c ", "" };
	private static final Object[] fileBalanceDefaultv = { "", "", "", "", 0.00,
			"" };
	private static final int fileBalanceRows = 0;
	public static TableModel fileBalanceTableModel = new TableModel(
			fileBalanceColumns, fileBalanceDefaultv, fileBalanceRows);
	public static String fileBalanceView = "non zero balance";

	private Map balanceMap = new HashMap();

	/**
	 * Finance JTable ***********************************************************************
	 */

	private static final String[] finance_columns = { "First Name",
			"Last Name", "Phone Number", "Email Address", "User Role",
			"Service", "id" };
	private static final Object[] finance_defaultv = { "", "", "", "", "", "",
			"", "", "", "" };

	private static int finance_rows = 0;
	public static TableModel finance_tableModel = new TableModel(
			finance_columns, finance_defaultv, finance_rows);

	public static int finance_total = 0;

	private static Map finance_parameterMap = new HashMap();
	TableCellRenderer renderer = new CustomTableCellRenderer();

	public static String lastQuery; // the last query run, used to reload table

	/**
	 * end of finance JTable **********************************************************************
	 */
	//Sites
	private static final String[] columns_sites = { "Site Description", "Purpose","Site Name",
			"Site Role" };
	private static final Object[] defaultv_sites = { "", "", "","" };
	private static final int rows_sites = 0;
	public static TableModel tableModel_sites = new TableModel(columns_sites,
			defaultv_sites, rows_sites);
	private static Sites sites;
	private static ListIterator<Sites> sitesIterator;
	List<Sites> sitesList = new LinkedList();

	
	
	//Roles
	private static final String[] columns_roles = { "Role name", "Description",
			"id" };
	private static final Object[] defaultv_roles = { "", "", "" };
	private static final int rows_roles = 0;
	public static TableModel tableModel_roles = new TableModel(columns_roles,
			defaultv_roles, rows_roles);
	private static Roles roles;
	private static ListIterator<Roles> rolesIterator;
	List<Roles> rolesList = new LinkedList();

	//Users 
	
	private static final String[] columns_users = { "user name", "Job Title" };
	private static final Object[] defaultv_users = { "", "" };
	private static final int rows_users = 0;
	public static TableModel tableModel_users = new TableModel(columns_users,
			defaultv_users, rows_users);
	private static users users;
	private static Roles Roles;
	private Sites site;
	private static ListIterator<users> usersIterator;
	
	List<Roles> usersList = new LinkedList();

	//Rights 

	private static final String[] columns_rights = { "Name", "Description" };
	private static final Object[] defaultv_rights = { "", "" };
	private static final int rows_rights = 0;
	public static TableModel tableModel_rights = new TableModel(columns_rights,
			defaultv_rights, rows_rights);
	private static Rights rights;
	private static ListIterator<Rights> rightsIterator;
	List<Roles> rightsList = new LinkedList();
	
	
	public NewDispensingSitesTabJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);

		// super(title);
		//viewAll();

		initComponents();
		//tableModel.clearTable();
		
		class RightTableCellRenderer extends DefaultTableCellRenderer {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			protected RightTableCellRenderer() {
				setHorizontalAlignment(SwingConstants.RIGHT);
			}

		}

		RightTableCellRenderer rightAlign = new RightTableCellRenderer();
		
		this.userAdminJTable.getColumnModel().getColumn(0)
				.setPreferredWidth(70);
		this.userAdminJTable.getColumnModel().getColumn(1)
				.setPreferredWidth(70);
		this.userAdminJTable.getColumnModel().getColumn(2).setPreferredWidth(
				70);
		this.userAdminJTable.getColumnModel().getColumn(3).setPreferredWidth(
				70);
		this.userAdminJTable.getColumnModel().getColumn(4).setMaxWidth(0);
		//this.userAdminJTable.getColumnModel().getColumn(6).setMinWidth(0);
		//this.userAdminJTable.getColumnModel().getColumn(6).setMaxWidth(0);

		this.rolesJtable.getColumnModel().getColumn(2).setMinWidth(0);
		this.rolesJtable.getColumnModel().getColumn(2).setMaxWidth(0);

		//	this.userAdminJTable.getColumnModel().getColumn(6).setCellRenderer(rightAlign);

		eventDate = publicMethods.toDay();
		
		//****************************************************
		// file balance 

		this.setSize((AppJFrame.desktop.getWidth() / 2 + AppJFrame.desktop
				.getWidth() / 7), AppJFrame.desktop.getHeight() / 2
				+ AppJFrame.desktop.getWidth() / 7);
		this.setLocationRelativeTo(null);

		//jTabbedPane1.remove(2);//reportsJP.setVisible(false);
		//jTabbedPane1.remove(4);//exchangeRateJP.setVisible(false);
		//jTabbedPane1.remove(this.expenseJP);//expenseJP.setVisible(false);

		/*manageJL.setVisible(false);
		vewAccountRecordJL.setVisible(false);
		recordDepositJL.setVisible(false);
		AddDisburseJL.setVisible(false);
		printListJL.setVisible(false);*/

		//fileBalanceTableModel.clearTable();
		
		

		this.setVisible(true);

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		userAdminJTable = new javax.swing.JTable();
		addUserJL = new javax.swing.JLabel();
		changeUserPasswordJL = new javax.swing.JLabel();
		printListJL1 = new javax.swing.JLabel();
		jSeparator3 = new javax.swing.JSeparator();
		viewUsersJL = new javax.swing.JLabel();
		deleteUserJL = new javax.swing.JLabel();
		editDPointJL = new javax.swing.JLabel();
		editUserDetailsJL = new javax.swing.JLabel();
		creditNoteJP = new javax.swing.JPanel();
		viewRolesJL = new javax.swing.JLabel();
		addRoleJL = new javax.swing.JLabel();
		editRightsJL = new javax.swing.JLabel();
		deleteRoleJL = new javax.swing.JLabel();
		jScrollPane2 = new javax.swing.JScrollPane();
		rolesJtable = new javax.swing.JTable();
		jScrollPane4 = new javax.swing.JScrollPane();
		permissionJtable = new javax.swing.JTable();
		jLabel4 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jScrollPane5 = new javax.swing.JScrollPane();
		usersJtable = new javax.swing.JTable();
		changeUserPasswordJL.setVisible(false);
		printListJL1.setVisible(false);
		

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Dispensing Site Administration");

		jTabbedPane1.setFont(new java.awt.Font("Ebrima", 1, 12));

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		SdpStyleSheet.configTable(userAdminJTable);
		userAdminJTable.setModel(tableModel_userAdmin);
		
		userAdminJTable.setRowHeight(30);
		jScrollPane1.setViewportView(userAdminJTable);

		addUserJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		addUserJL.setForeground(new java.awt.Color(255, 255, 255));
		addUserJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS add user small.png"))); // NOI18N
		addUserJL.setText("Add New Site");
		addUserJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				addUserJLMouseClicked(evt);
			}
		});
		
		changeUserPasswordJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		changeUserPasswordJL.setForeground(new java.awt.Color(255, 255, 255));
		changeUserPasswordJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS change password small.png"))); // NOI18N
		changeUserPasswordJL.setText("Change user's password");
		changeUserPasswordJL.setToolTipText("Change selected user's password");
		changeUserPasswordJL
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						changeUserPasswordJLMouseClicked(evt);
					}
				});

		printListJL1.setFont(new java.awt.Font("Ebrima", 1, 12));
		printListJL1.setForeground(new java.awt.Color(255, 255, 255));
		printListJL1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/PRINT.PNG"))); // NOI18N
		printListJL1.setText("Print user list");
		printListJL1.setToolTipText("Print List");
		printListJL1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				printListJL1MouseClicked(evt);
			}
		});

		jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

		viewUsersJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		viewUsersJL.setForeground(new java.awt.Color(255, 255, 255));
		viewUsersJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view user small.png"))); // NOI18N
		viewUsersJL.setText("View Dispensing Sites");
		viewUsersJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewUsersJLMouseClicked(evt);
			}
		});
		
		deleteUserJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		deleteUserJL.setForeground(new java.awt.Color(255, 255, 255));
		deleteUserJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/EXIT.PNG"))); // NOI18N
		deleteUserJL.setText("Delete Dispensing Site");
		deleteUserJL.setToolTipText("Delete Dispensing Site");
		deleteUserJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				deleteUserJLMouseClicked(evt);
			}
		});
		
		editDPointJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		editDPointJL.setForeground(new java.awt.Color(255, 255, 255));
		editDPointJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS edit user small.png"))); // NOI18N
		editDPointJL.setText("Edit Dispensing Site");
		editDPointJL.setToolTipText("Edit Dispensing Site");
		editDPointJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				editDPointJLMouseClicked(evt);
			}
		});
		

		editUserDetailsJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		editUserDetailsJL.setForeground(new java.awt.Color(255, 255, 255));
		editUserDetailsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS edit user small.png"))); // NOI18N
		editUserDetailsJL.setText("Add Dispensing Points");
		editUserDetailsJL.setToolTipText("Add Dispensing Points");
		editUserDetailsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				editUserDetailsJLMouseClicked(evt);
			}
		});

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jScrollPane1,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																810,
																Short.MAX_VALUE)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				viewUsersJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				addUserJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				jSeparator3,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				11,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				editUserDetailsJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				changeUserPasswordJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		
																		.addComponent(
																				deleteUserJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				editDPointJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)

																		.addComponent(
																				printListJL1)))
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addGroup(
																jPanel1Layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.LEADING)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								viewUsersJL)
																						.addComponent(
																								addUserJL))
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								editUserDetailsJL)
																						.addComponent(
																								changeUserPasswordJL)
																						.addComponent(
																								deleteUserJL)
																						.addComponent(
																								editDPointJL)
																						.addComponent(
																								printListJL1)))
														.addComponent(
																jSeparator3,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																10,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jScrollPane1,
												javax.swing.GroupLayout.DEFAULT_SIZE,
												510, Short.MAX_VALUE)
										.addContainerGap()));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { addUserJL, changeUserPasswordJL,
						jSeparator3, printListJL1 });

		jTabbedPane1.addTab("Dispensing Sites", new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS user admin small.png")),
				jPanel1); // NOI18N

		creditNoteJP.setBackground(new java.awt.Color(102, 102, 102));
		creditNoteJP.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		viewRolesJL.setFont(new java.awt.Font("Gulim", 0, 11));
		viewRolesJL.setForeground(new java.awt.Color(255, 255, 255));
		viewRolesJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/CompositeQuery16.png"))); // NOI18N
		viewRolesJL.setText("View Site Roles");
		viewRolesJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				viewRolesJLMouseClicked(evt);
			}
		});

		addRoleJL.setFont(new java.awt.Font("Gulim", 0, 11));
		addRoleJL.setForeground(new java.awt.Color(255, 255, 255));
		addRoleJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS add user small.png"))); // NOI18N
		addRoleJL.setText("Add new role");
		addRoleJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				addRoleJLMouseClicked(evt);
			}
		});

		editRightsJL.setFont(new java.awt.Font("Gulim", 0, 11));
		editRightsJL.setForeground(new java.awt.Color(255, 255, 255));
		editRightsJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS change password small.png"))); // NOI18N
		editRightsJL.setText("Edit role rights");
		editRightsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				editRightsJLMouseClicked(evt);
			}
		});

		deleteRoleJL.setFont(new java.awt.Font("Gulim", 0, 11));
		deleteRoleJL.setForeground(new java.awt.Color(255, 255, 255));
		deleteRoleJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS delete role small.png"))); // NOI18N
		deleteRoleJL.setText("Delete role");
		deleteRoleJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				deleteRoleJLMouseClicked(evt);
			}
		});

		rolesJtable.setModel(tableModel_roles);
		rolesJtable.setRowHeight(30);
		rolesJtable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				rolesJtableMouseClicked(evt);
			}
		});
		jScrollPane2.setViewportView(rolesJtable);

		permissionJtable.setModel(tableModel_rights);
		permissionJtable.setRowHeight(30);
		jScrollPane4.setViewportView(permissionJtable);

		jLabel4.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS dashboard small icon.png"))); // NOI18N
		jLabel4.setText("Right");

		jLabel6.setFont(new java.awt.Font("Gulim", 0, 11));
		jLabel6.setForeground(new java.awt.Color(255, 255, 255));
		jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS users.png"))); // NOI18N
		jLabel6.setText("Users");

		usersJtable.setModel(tableModel_users);
		usersJtable.setRowHeight(30);
		jScrollPane5.setViewportView(usersJtable);

		javax.swing.GroupLayout creditNoteJPLayout = new javax.swing.GroupLayout(
				creditNoteJP);
		creditNoteJP.setLayout(creditNoteJPLayout);
		creditNoteJPLayout
				.setHorizontalGroup(creditNoteJPLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								creditNoteJPLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												creditNoteJPLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																creditNoteJPLayout
																		.createSequentialGroup()
																		.addComponent(
																				viewRolesJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				addRoleJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				editRightsJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				deleteRoleJL))
														.addComponent(
																jScrollPane2,
																javax.swing.GroupLayout.Alignment.TRAILING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																810,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																creditNoteJPLayout
																		.createSequentialGroup()
																		.addGroup(
																				creditNoteJPLayout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel4)
																						.addComponent(
																								jScrollPane4,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								471,
																								Short.MAX_VALUE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				creditNoteJPLayout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addGroup(
																								creditNoteJPLayout
																										.createSequentialGroup()
																										.addComponent(
																												jLabel6)
																										.addGap(
																												282,
																												282,
																												282))
																						.addComponent(
																								jScrollPane5,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								327,
																								javax.swing.GroupLayout.PREFERRED_SIZE))))
										.addContainerGap()));
		creditNoteJPLayout
				.setVerticalGroup(creditNoteJPLayout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								creditNoteJPLayout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												creditNoteJPLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																viewRolesJL)
														.addComponent(addRoleJL)
														.addComponent(
																editRightsJL)
														.addComponent(
																deleteRoleJL))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												jScrollPane2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												168,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												creditNoteJPLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel4)
														.addComponent(jLabel6))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												creditNoteJPLayout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jScrollPane4,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																315,
																Short.MAX_VALUE)
														.addComponent(
																jScrollPane5,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																315,
																Short.MAX_VALUE))
										.addContainerGap()));

		jTabbedPane1.addTab("Site Roles", new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS roles small.png")),
				creditNoteJP); // NOI18N

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 839,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE,
						587, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void deleteRoleJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:

		//if jtable has content
		if (this.tableModel_roles.getRowCount() > 0) {

			//if use is selected
			if (this.rolesJtable.getSelectedRow() >= 0) {

				//check if users are assigned to this role
				if (tableModel_users.getRowCount() <= 0) {

					try {

						int ok = javax.swing.JOptionPane.showConfirmDialog(
								this,
								"Are you sure you would like to delete Role");

						if (ok == 0) {

							SqlSessionFactory factory = new MyBatisConnectionFactory()
									.getSqlSessionFactory();

							SqlSession session = factory.openSession();

							try {

								//delete roleid reference from role_assignment table					
								session
										.delete(
												"deleteFromRoleAssignmentsByRoleId",
												Integer
														.parseInt(this.tableModel_roles
																.getValueAt(
																		this.rolesJtable
																				.getSelectedRow(),
																		2)
																.toString()));
								session.commit();

								//delete role reference in role_right table
								session
										.delete(
												"deleteByRoleId",
												Integer
														.parseInt(this.tableModel_roles
																.getValueAt(
																		this.rolesJtable
																				.getSelectedRow(),
																		2)
																.toString()));
								session.commit();

								//delete role from role table 
								session
										.delete(
												"deleteRoleByPrimaryKey",
												Integer
														.parseInt(this.tableModel_roles
																.getValueAt(
																		this.rolesJtable
																				.getSelectedRow(),
																		2)
																.toString()));
								session.commit();

								//refresh roles table
								rolesList = session.selectList("selectByRoles");

								populateRolesTable(rolesList);

								tableModel_rights.clearTable();

							} finally {

								session.close();
								javax.swing.JOptionPane.showMessageDialog(this,
										"Role deleted ");

							}

							// reload table 

						}

					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} else {

					javax.swing.JOptionPane
							.showMessageDialog(
									this,
									"Users assigned to this role,"
											+ " Please move these users to\n another role before deleting the selected role");
				}

			} else {

				javax.swing.JOptionPane.showMessageDialog(this,
						"Please select role from list");
			}

		} else {

			javax.swing.JOptionPane
					.showMessageDialog(this,
							"Please click on view site roles , then select the role you wish to delete");
		}
	}
	
	private void editDPointJLMouseClicked(java.awt.event.MouseEvent evt) {
		if (userAdminJTable.getSelectedRow() >= 0) {

			users user = new users();
			site = new Sites();

			try {
				site.setDescription(tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 0).toString());
				site.setpurpose(tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 1).toString());
				site.setname(tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 2).toString());
				site.setsite_role(tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 3).toString());
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Integer MySiteID=(Integer) tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 4);
			System.out.println("My SITE ID: "+ MySiteID);
			
			new editDPointJD(javax.swing.JOptionPane
					.getFrameForComponent(this), true, MySiteID,site);
		
			
			

		} else {

			javax.swing.JOptionPane.showMessageDialog(this,
					"Please select dispensing site from list");
		}
	}
	
	private void deleteUserJLMouseClicked(java.awt.event.MouseEvent evt) {

		// TODO add your handling code here:

		//if jtable has content
		if (tableModel_userAdmin.getRowCount() > 0) {

			//if use is selected
			if (userAdminJTable.getSelectedRow() >= 0) {

				//users user = new users();
				site = new Sites();

				try {
					site.setname(tableModel_userAdmin.getValueAt(
							userAdminJTable.getSelectedRow(), 0).toString());
						
					
					int ok = javax.swing.JOptionPane.showConfirmDialog(this, "Are you sure you would like to delete Dispensing Site:"+ site.getname(), "Confirm Site Deletion",0);
					//Check if site has transactions before delete
					SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

					SqlSession sessioncheck = factory.openSession();
					Integer MySiteIDs=(Integer) tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 4);
					List<Sites> sitesListCheck = sessioncheck.selectList("getAllUsedSites",MySiteIDs);
					int TranExists=0;
					for (int i = 0; i < sitesListCheck.size(); i++) {
					
						TranExists++;
					}
					//End delete check
					if (TranExists==0){
						
					if (ok == 0) {

						//SqlSessionFactory factory = new MyBatisConnectionFactory()
						//		.getSqlSessionFactory();

						SqlSession session = factory.openSession();

						try {

							session.delete("deleteDispensingSite",
									tableModel_userAdmin.getValueAt(
											userAdminJTable.getSelectedRow(), 4));
							session.commit();
							
							session.delete("deleteDispensingPoints",
									tableModel_userAdmin.getValueAt(
											userAdminJTable.getSelectedRow(), 4));
							session.commit();
							List<Sites> sitesList = session.selectList("getAllSites");
							populateUserAdminTable(sitesList);

						} finally {

							session.close();
							javax.swing.JOptionPane.showMessageDialog(this,
									"Site Successfully Deleted!!");

						}

						// reload table 

					}
				}else
				{
					javax.swing.JOptionPane.showMessageDialog(this,
							"You CANNOT delete this site because it has transactions associated with it!!");
				}

				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {

				javax.swing.JOptionPane.showMessageDialog(this,
						"Please select user from list");
			}

		} else {

			javax.swing.JOptionPane
					.showMessageDialog(this,
							"Please click on view sites , then select the site you wish to edit");
		}

	}

	private void changeUserPasswordJLMouseClicked(java.awt.event.MouseEvent evt) {

		// TODO add your handling code here:

		//if jtable has content
		if (tableModel_userAdmin.getRowCount() > 0) {

			//if use is selected
			if (userAdminJTable.getSelectedRow() >= 0) {

				users user = new users();

				try {
					user.setFirstname(tableModel_userAdmin.getValueAt(
							userAdminJTable.getSelectedRow(), 0).toString());
					user.setLastname(tableModel_userAdmin.getValueAt(
							userAdminJTable.getSelectedRow(), 1).toString());
					user.setUsername(tableModel_userAdmin.getValueAt(
							userAdminJTable.getSelectedRow(), 2).toString());

					if (tableModel_userAdmin.getValueAt(userAdminJTable
							.getSelectedRow(), 3) != null) {
						user
								.setJobtitle(tableModel_userAdmin.getValueAt(
										userAdminJTable.getSelectedRow(), 3)
										.toString());

					}
					if (tableModel_userAdmin.getValueAt(userAdminJTable
							.getSelectedRow(), 4) != null) {

						user
								.setCellphone(tableModel_userAdmin.getValueAt(
										userAdminJTable.getSelectedRow(), 4)
										.toString());

					}
					if (tableModel_userAdmin.getValueAt(userAdminJTable
							.getSelectedRow(), 5) != null) {

						user
								.setEmail(tableModel_userAdmin.getValueAt(
										userAdminJTable.getSelectedRow(), 5)
										.toString());

					}
					user.setId(Integer.parseInt(tableModel_userAdmin
							.getValueAt(userAdminJTable.getSelectedRow(), 6)
							.toString()));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				new changeUserPasswordJD(javax.swing.JOptionPane
						.getFrameForComponent(this), true, user);

			} else {

				javax.swing.JOptionPane.showMessageDialog(this,
						"Please select site from list");
			}

		} else {

			javax.swing.JOptionPane
					.showMessageDialog(this,
							"Please click on view sites , then select the site you wish to edit");
		}

	}

	private void editUserDetailsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//if jtable has content
		if (tableModel_userAdmin.getRowCount() > 0) {

			//if use is selected
			if (userAdminJTable.getSelectedRow() >= 0) {

				users user = new users();
				site = new Sites();

				try {
					site.setname(tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 0).toString());
					//MySiteID=(Integer) tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 0);
					//More Code Here Mr. Simf
					//
					//
					//
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Integer MySiteID=(Integer) tableModel_userAdmin.getValueAt(userAdminJTable.getSelectedRow(), 4);
				System.out.println("My SITE ID: "+ MySiteID);
				
				new DefineDispensingPoint(javax.swing.JOptionPane
						.getFrameForComponent(this), true, MySiteID);
			
				
				

			} else {

				javax.swing.JOptionPane.showMessageDialog(this,
						"Please select dispensing site from list");
			}

		} else {

			javax.swing.JOptionPane
					.showMessageDialog(this,
							"Please click on view dispensing sites , then select the site to which you wish to add dispensing points");
		}
	}

	private void viewUsersJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {
			System.out.print("Geting ALl Users");
			List<Sites> sitesList = session.selectList("getAllSites");

			//populateSitesTable(sitesList);
			populateUserAdminTable(sitesList);

		} finally {
			session.close();
		}

	}

	private void viewRolesJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			rolesList = session.selectList("selectByRoles");

			populateRolesTable(rolesList);

		} finally {
			session.close();
		}
	}

	private void editRightsJLMouseClicked(java.awt.event.MouseEvent evt) {
		
		//if jtable has content
		if (tableModel_roles.getRowCount() > 0) {

			//if use is selected
			if (rolesJtable.getSelectedRow() >= 0) {

				
				Roles role=new Roles();
				try {
					role.setName(tableModel_roles.getValueAt(
							rolesJtable.getSelectedRow(), 0).toString());
					
					role.setDescription(setDescription(tableModel_roles.getValueAt(
							rolesJtable.getSelectedRow(), 1).toString()));
					

					role.setId(Integer.parseInt(tableModel_roles
							.getValueAt(rolesJtable.getSelectedRow(), 2)
							.toString()));
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				new EditRoleJD(javax.swing.JOptionPane.getFrameForComponent(this),true,role);

			} else {

				javax.swing.JOptionPane.showMessageDialog(this,
						"Please select role from list");
			}

		} else {

			javax.swing.JOptionPane
					.showMessageDialog(this,
							"Please click on view site roles , then select the role you wish to edit");
		}
		
	}

	private String setDescription(String string) {
		// TODO Auto-generated method stub
		return null;
	}

	private void addRoleJLMouseClicked(java.awt.event.MouseEvent evt) {
		new AddNewRoleJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);
	}

	private void rolesJtableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		//javax.swing.JOptionPane.showMessageDialog(this,	tableModel_roles.getValueAt(rolesJtable.getSelectedRow(), 0));

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			//usersList = session.selectList("selectByusers");	

			usersList = session.selectList("selectUsersByRole",
					tableModel_roles
							.getValueAt(rolesJtable.getSelectedRow(), 0));
			populateUsersTable(usersList);

			rightsList = session.selectList("selectRightByRole",
					tableModel_roles
							.getValueAt(rolesJtable.getSelectedRow(), 0));
			populateRightsTable(rightsList);

		} finally {
			session.close();
		}
	}

	private void addUserJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		new AddDispensingPointsJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);
	}

	private void printListJL1MouseClicked(java.awt.event.MouseEvent evt) {

		try {
			userAdminJTable.print();
		} catch (PrinterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void populateUserAdminTable(List dataList) {

		tableModel_userAdmin.clearTable();
		usersIterator = dataList.listIterator();

		while (usersIterator.hasNext()) {

			users = usersIterator.next();
			defaultv_userAdmin[0] = users.getDescription();
			defaultv_userAdmin[1] = users.getpurpose();
			defaultv_userAdmin[2] = users.getname();
			defaultv_userAdmin[3] = users.getsite_role();
			defaultv_userAdmin[4] = users.getId();
			//defaultv_userAdmin[4] = users.getCellphone();
			//defaultv_userAdmin[5] = users.getEmail();
			//;

			//defaultv_userAdmin[1] = users.getJobtitle();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_userAdmin.length; j++) {
				cols.add(defaultv_userAdmin[j]);

			}

			tableModel_userAdmin.insertRow(cols);
			// jScrollPane1.repaint();
			usersIterator.remove();
		}

	}
	
	public static void populateSitesTable(List dataList) {

		//tableModel_userAdmin.clearTable();
		tableModel_sites.clearTable();
		sitesIterator = dataList.listIterator();

		while (sitesIterator.hasNext()) {

			sites = sitesIterator.next();
			defaultv_sites[0] = sites.getprimary_name();

			defaultv_sites[1] = sites.getpurpose();

			defaultv_sites[2] = sites.getname();
			defaultv_sites[3] = sites.getsite_role();
			
			defaultv_sites[6] = sites.getId();

			//defaultv_userAdmin[1] = users.getJobtitle();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_sites.length; j++) {
				cols.add(defaultv_sites[j]);

			}

			tableModel_sites.insertRow(cols);
			// jScrollPane1.repaint();
			sitesIterator.remove();
		}

	}

	public static void populateRolesTable(List dataList) {

		tableModel_roles.clearTable();
		rolesIterator = dataList.listIterator();

		while (rolesIterator.hasNext()) {

			roles = rolesIterator.next();
			defaultv_roles[0] = roles.getName();
			defaultv_roles[1] = roles.getDescription();
			defaultv_roles[2] = roles.getId();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_roles.length; j++) {
				cols.add(defaultv_roles[j]);

			}

			tableModel_roles.insertRow(cols);
			// jScrollPane1.repaint();
			rolesIterator.remove();
		}

	}

	public static void populateUsersTable(List dataList) {

		tableModel_users.clearTable();
		usersIterator = dataList.listIterator();

		while (usersIterator.hasNext()) {

			users = usersIterator.next();
			defaultv_users[0] = users.getFirstname() + " "
					+ users.getLastname();
			defaultv_users[1] = users.getJobtitle();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_users.length; j++) {
				cols.add(defaultv_users[j]);

			}

			tableModel_users.insertRow(cols);
			// jScrollPane1.repaint();
			usersIterator.remove();
		}

	}

	public static void populateRightsTable(List dataList) {

		tableModel_rights.clearTable();
		rightsIterator = dataList.listIterator();
		
		while (rightsIterator.hasNext()) {

			rights = rightsIterator.next();
			defaultv_rights[0] = rights.getName();
			defaultv_rights[1] = rights.getDescription();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_rights.length; j++) {
				cols.add(defaultv_rights[j]);

			}

			tableModel_rights.insertRow(cols);
			// jScrollPane1.repaint();
			rightsIterator.remove();
		}

	}

	/**
	 * Creates a chart.
	 * 
	 * @param dataset  the dataset.
	 * 
	 * @return The chart.
	 */

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				NewDispensingSitesTabJD dialog = new NewDispensingSitesTabJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowActivated(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				
				dialog.setVisible(true);
			}
		});
	}

	/*
	 * File Balances ********************************************************************************
	 * 
	 */

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel addRoleJL;
	private javax.swing.JLabel addUserJL;
	private javax.swing.JLabel changeUserPasswordJL;
	private javax.swing.JPanel creditNoteJP;
	private javax.swing.JLabel deleteRoleJL;
	private javax.swing.JLabel deleteUserJL;
	private javax.swing.JLabel editDPointJL;
	private javax.swing.JLabel editRightsJL;
	private javax.swing.JLabel editUserDetailsJL;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane4;
	private javax.swing.JScrollPane jScrollPane5;
	private javax.swing.JSeparator jSeparator3;
	private javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JTable permissionJtable;
	private javax.swing.JLabel printListJL1;
	private javax.swing.JTable rolesJtable;
	private javax.swing.JTable userAdminJTable;
	private javax.swing.JTable usersJtable;
	private javax.swing.JLabel viewRolesJL;
	private javax.swing.JLabel viewUsersJL;
	// End of variables declaration//GEN-END:variables


	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		SqlSessionFactory factory = new MyBatisConnectionFactory()
		.getSqlSessionFactory();
		System.out.println("We are Here Babies7!!");
		SqlSession session = factory.openSession();
		
		try {
		
			List<users> usersList = session.selectList("getAllUsers");
		
			populateUserAdminTable(usersList);
		
		} finally {
			session.close();
		}

		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("We are Here Babies6!!");
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("We are Here Babies5!!");
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("We are Here Babies4!!");
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("We are Here Babies3!!");
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("We are Here Babies2!!");
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		System.out.println("We are Here Babies1!!");
	}

}