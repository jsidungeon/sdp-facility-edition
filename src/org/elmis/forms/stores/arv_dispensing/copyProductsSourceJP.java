/*
 * ProgramsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JFileChooser;

import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.forms.stores.receiving.ProductsPhysicalCountJD;
import org.elmis.forms.stores.receiving.ProgramsJP;
import org.elmis.forms.stores.receiving.ReceivingJD;
import org.elmis.forms.stores.receiving.productAdjustmentsJD;

/**
 *
 * @author  __USER__
 * 
 * 
 */

public class copyProductsSourceJP extends javax.swing.JPanel {

	public static String selectedproductsource = null;
	@SuppressWarnings("unused")
	//private sccfilechooser mysccfilechooser;

	//ReceivingJD receivingjd = null;
	/** Creates new form ProgramsJP */
	public copyProductsSourceJP() {
		initComponents();
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		ProductsSourcejTable = new javax.swing.JTable();
		ReceiveitemJL = new javax.swing.JLabel();
		RecordPhysicalCountJL = new javax.swing.JLabel();
		ImportshipmentfileJL = new javax.swing.JLabel();

		ProductsSourcejTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		ProductsSourcejTable.setModel(new javax.swing.table.DefaultTableModel(
				new Object[][] {

				}, new String[] {

				}));
		ProductsSourcejTable.setRowHeight(60);
		ProductsSourcejTable.setTableHeader(null);
		ProductsSourcejTable
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						ProductsSourcejTableMouseClicked(evt);
					}
				});
		jScrollPane1.setViewportView(ProductsSourcejTable);

		ReceiveitemJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		ReceiveitemJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/office_expenses_65.png"))); // NOI18N
		ReceiveitemJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Record Adjustment"));
		ReceiveitemJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ReceiveitemJLMouseClicked(evt);
			}
		});

		RecordPhysicalCountJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		RecordPhysicalCountJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/office_expenses_65.png"))); // NOI18N
		RecordPhysicalCountJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Record Physical Count"));
		RecordPhysicalCountJL
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						RecordPhysicalCountJLMouseClicked(evt);
					}
				});

		ImportshipmentfileJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		ImportshipmentfileJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/office_expenses_65.png"))); // NOI18N
		ImportshipmentfileJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Import shipment File"));
		ImportshipmentfileJL
				.addMouseListener(new java.awt.event.MouseAdapter() {
					public void mouseClicked(java.awt.event.MouseEvent evt) {
						try {
							ImportshipmentfileJLMouseClicked(evt);
						} catch (FileNotFoundException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jScrollPane1,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																697,
																Short.MAX_VALUE)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addComponent(
																				ReceiveitemJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				153,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				RecordPhysicalCountJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				163,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				ImportshipmentfileJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				157,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addContainerGap(
																				208,
																				Short.MAX_VALUE)))));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addGroup(
				layout.createSequentialGroup().addContainerGap().addComponent(
						jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE,
						159, javax.swing.GroupLayout.PREFERRED_SIZE).addGap(43,
						43, 43).addGroup(
						layout.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
								.addComponent(ReceiveitemJL).addComponent(
										RecordPhysicalCountJL).addComponent(
										ImportshipmentfileJL))
						.addContainerGap()));
	}// </editor-fold>
	//GEN-END:initComponents

	private void ImportshipmentfileJLMouseClicked(java.awt.event.MouseEvent evt)
			throws FileNotFoundException {
		// TODO add your handling code here:
		/*AppJFrame.glassPane.activate(null);
		new importshipmentfilesJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();*/

		//mysccfilechooser =  new sccfilechooser();

		final JFileChooser fc = new JFileChooser();
		fc.showOpenDialog(this);

		try {
			// Open an input stream
			Scanner reader = new Scanner(fc.getSelectedFile());
		} finally {

		}
	}

	@SuppressWarnings("unused")
	private void RecordPhysicalCountJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling
		AppJFrame.glassPane.activate(null);
		new ProductsPhysicalCountJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}

	private void ReceiveitemJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		AppJFrame.glassPane.activate(null);
		new productAdjustmentsJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
	}

	private void ProductsSourcejTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// enable Program Area tab.
		@SuppressWarnings("unused")
		String productsourceselection = this.ProductsSourcejTable.getValueAt(
				ProductsSourcejTable.getSelectedRow(), 0).toString();
		if (productsourceselection == "From MSL") {
			selectedproductsource = productsourceselection;

			ReceivingJD.jTabbedPane1.addTab("Program", new ProgramsJP());
			ReceivingJD.jTabbedPane1.setSelectedIndex(1);
			productsourceselection = "";
		} else if (productsourceselection == "CHAZ") {
			selectedproductsource = productsourceselection;
			ReceivingJD.jTabbedPane1.addTab("Program", new ProgramsJP());
			ReceivingJD.jTabbedPane1.setSelectedIndex(1);
			productsourceselection = "";
		}
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel ImportshipmentfileJL;
	private javax.swing.JTable ProductsSourcejTable;
	private javax.swing.JLabel ReceiveitemJL;
	private javax.swing.JLabel RecordPhysicalCountJL;
	private javax.swing.JScrollPane jScrollPane1;
	// End of variables declaration//GEN-END:variables

}