package org.elmis.forms.stores.arv_dispensing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EventObject;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultCellEditor;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;

import net.sf.jasperreports.components.map.ItemDataXmlFactory;

import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.RegimenProductDosage;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.service.arv.ARVDispensing;
import org.elmis.facility.service.arv.IARVDispensing;
import org.elmis.forms.regimen.ChangeRegimen;
import org.elmis.forms.regimen.DefineRegimen;
import org.elmis.forms.regimen.DefineRegimenII;
import org.elmis.forms.regimen.dao.RegimenDao;
import org.elmis.forms.regimen.util.ComboBoxItem;
import org.elmis.forms.regimen.util.GenericUtil;

import com.oribicom.tools.TableModel;
import com.toedter.calendar.JDateChooser;

import java.awt.Dimension;

import javax.swing.DefaultComboBoxModel;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;

/**
 * 
 * @author __USER__
 */
@SuppressWarnings({ "serial", "unused", "unchecked" })
public class ARTNumberJP extends javax.swing.JPanel {

	/**
	 * Creates new form ARTNumber
	 * 
	 * @param b
	 * @param frame
	 */

	// Facility Approved Products JTable
	// **************************************************
	private Boolean ARTsearchmode = false;
	private Boolean NRCsearchmode = false;
	public static String Artcode = "";
	public static String Artnrc = "";
	public static Boolean registerNumpadcall = false;
	private static final String[] columns_facilityApprovedproducts = { " ",
			"Product Code", "Pack Size", "Product Name", "Dosage",
			"Quantity Dispensed", "Bottles / Packs", "Balance" };
	private static final Object[] defaultv_facilityapprovedproducts = { true,
			"", "", "", "", "", "", " " };
	List<ComboBoxCellEditor> editors = new ArrayList<ComboBoxCellEditor>();
	private static final int rows_fproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { true, false, false, false, true,
				true, false, false };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		// create a check box value in table
		@Override
		public Class<?> getColumnClass(int columnIndex) {

			if (columnIndex == 4) {

				return getValueAt(0, 4).getClass();
			} else if (columnIndex == 0) {
				return Boolean.class;
			}

			return super.getColumnClass(columnIndex);
		}

		/*
		 * @Override public boolean isCellEditable(int row, int column) { return
		 * column == CHECK_COL; }
		 */

	};

	// Facility Approved products JTable for REGIMEN only
	// "Product Code", "Pack size", "Product name","Dosage" ,
	// "Quantity Dispensed" , "Bottles"
	public static final Integer TBL_IDX_INCLUDED_IN_DISPENSATION = 0;
	public static final Integer TBL_IDX_PRODUCT_CODE = 1;
	public static final Integer TBL_IDX_PACK_SIZE = 2;
	public static final Integer TBL_IDX_PRODUCT_NAME = 3;
	public static final Integer TBL_IDX_DOSAGE = 4;
	public static final Integer TBL_IDX_QTY_DISPENSED = 5;
	public static final Integer TBL_IDX_EQUIV_BOTTLES = 6;
	public static final Integer TBL_IDX_BALANCE = 7;

	private Boolean qtyDispensedValid = true;
	private Integer currentRegid;
	private static final String INDETERMINATE = "Undetermined";
	private Boolean checkdigit = false;
	private Boolean EDITING = false;
	private static final String[] columns_arvfacilityApprovedproducts = {
			"Product Code", "Product Nname", "Generic Strength", "Pack Size",
			"Quantity Dispensed" };
	private static final Object[] defaultv_arvfacilityapprovedproducts = { "",
			"", "", "", "" };
	private static final int rows_arvfproducts = 0;
	public static TableModel tableModel_arvfproducts = new TableModel(
			columns_arvfacilityApprovedproducts,
			defaultv_arvfacilityapprovedproducts, rows_arvfproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		// create a check box value in table
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 4) {

				return getValueAt(0, 4).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/*
		 * @Override public boolean isCellEditable(int row, int column) { return
		 * column == CHECK_COL; }
		 */

	};

	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;

	private Date registerdate;
	private String selectedsex = "";
	public Timestamp selecteddate;
	private Timestamp shipmentdate;
	public String oldDateString;
	public static String searchedARTnumber = "";
	public String artnumber = "";
	public String nrcnumber = "";
	public String setARTtxtfield;
	public String setNRCtxtfield;
	public String newDateString;
	private char letter;
	private List<Character> charList = new LinkedList();
	private IARVDispensing arvDispensing = new ARVDispensing();
	private Elmis_Patient arvClient;
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	List<Elmis_Dar_Transactions> arvpatientdarproductsList = new LinkedList();
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	private static ListIterator<Elmis_Dar_Transactions> arvpatientdarproductsIterator;
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	ArrayList<Elmis_Dar_Transactions> elmisstockcontrolcardList = new ArrayList<Elmis_Dar_Transactions>();
	FacilityProductsDAO Facilityproductsmapper = null;
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	Elmis_Dar_Transactions elmis_dar_transactions;
	Elmis_Dar_Transactions arvpatientdarProducts;
	Elmis_Dar_Transactions saveelmis_dar_transactions;
	public String typeCode;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer PACKSIZE;
	private Double checkbottleqty;
	private Integer Convert_tablet_to_bottle;
	public boolean artfound;
	public static boolean usenumberpad = true;
	public Object myobject;
	private Integer regimenId;
	private boolean indeterminate = false;
	private static SqlSessionFactory sqlMapper = null;
	private ARTNumberJP _this = this;

	public ARTNumberJP() {
		initComponents();
	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	@SuppressWarnings("rawtypes")
	private void initComponents() {
		// this.closeJBtn.setVisible(false);
		sexcomboBox = new JComboBox();
		dateofbirthdateChooser = new com.toedter.calendar.JDateChooser();
		regimendateChooser = new com.toedter.calendar.JDateChooser();
		PatientARTJTP = new javax.swing.JTabbedPane();
		SearchJP = new javax.swing.JPanel();
		jPanel7 = new javax.swing.JPanel();
		ARTnumberJT2 = new javax.swing.JTextField();
		ARTnumberJT2.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {

			}

			@Override
			public void focusLost(FocusEvent e) {

			}
		});
		jLabel3 = new javax.swing.JLabel();
		ARTsearchJB1 = new javax.swing.JButton();
		ARTsearchJB1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		ARTmessagedisplay = new javax.swing.JLabel();
		RegimenjPanel = new javax.swing.JPanel();
		jPanel4 = new javax.swing.JPanel();
		currentregimenJL = new javax.swing.JLabel();
		dispenseJBtn = new javax.swing.JButton();
		jScrollPane3 = new javax.swing.JScrollPane();
		tblARVdispense = new javax.swing.JTable() {
			// Determine editor to be used by row
			public TableCellEditor getCellEditor(int row, int column) {
				int modelColumn = convertColumnIndexToModel(column);

				if (modelColumn == TBL_IDX_DOSAGE) {
					return editors.get(row);
				} else
					return super.getCellEditor(row, column);
			}
		};

		productQtyjLabel = new javax.swing.JLabel();
		lblPatientRegimen = new javax.swing.JLabel();
		lblPatientRegimen.setFont(new Font("Ebrima", Font.PLAIN, 20));
		btnChangeRegimen.setFont(new Font("Tahoma", Font.BOLD, 11));
		btnChangeRegimen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// bahoho
			}
		});
		RegisterjPanel = new javax.swing.JPanel();
		jPanel5 = new javax.swing.JPanel();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		NRCJT = new javax.swing.JTextField();
		NRCJT.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent e) {
				popupNRCNumberpad();
			}
		});
		RegisterJBtn1 = new javax.swing.JButton();
		closeJBtn1 = new javax.swing.JButton();
		closeJBtn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		ARTnumberJT1 = new javax.swing.JTextField();
		ARTnumberJT1.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {
				System.out.println("test this here");
				popupARTNumberpad();
			}
		});

		setBackground(new java.awt.Color(102, 102, 102));
		addComponentListener(new java.awt.event.ComponentAdapter() {
			public void componentShown(java.awt.event.ComponentEvent evt) {
				JPaneldisplayed(evt);
			}
		});

		PatientARTJTP.addFocusListener(new java.awt.event.FocusAdapter() {
			public void focusGained(java.awt.event.FocusEvent evt) {
				PatientARTJTP(evt);
			}
		});

		SearchJP.setFont(new java.awt.Font("Gulim", 0, 11));

		jPanel7.setBackground(new java.awt.Color(102, 102, 102));

		ARTnumberJT2.setFont(new java.awt.Font("Ebrima", 0, 20));
		ARTnumberJT2.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				ARTnumberJT2KeyPressed(evt);
			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				if (ARTnumberJT2.getText().length() == 0) {
					invokeEnableJT();
				}
			}
		});

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Enter ART Number : 13 digits");

		ARTsearchJB1.setFont(new java.awt.Font("Ebrima", 1, 12));
		ARTsearchJB1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Search icon.png"))); // NOI18N
		ARTsearchJB1.setText("Search");
		ARTsearchJB1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ARTsearchJB1MouseClicked(evt);
			}
		});

		ARTmessagedisplay.setFont(new java.awt.Font("Tahoma", 0, 18));

		NRCsearchJTF = new JTextField();
		NRCsearchJTF.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				invokeDisableARTJT();
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (NRCsearchJTF.getText().length() == 0) {
					invokeEnabledARTJT();
				}
			}
		});
		NRCsearchJTF.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(FocusEvent arg0) {

			}

			@Override
			public void focusLost(FocusEvent e) {
			}
		});
		NRCsearchJTF.setFont(new Font("Ebrima", Font.PLAIN, 20));

		lblEnterNrcNumber = new JLabel();
		lblEnterNrcNumber.setText("Enter NRC Number :  9 digits");
		lblEnterNrcNumber.setForeground(Color.WHITE);
		lblEnterNrcNumber.setFont(new Font("Ebrima", Font.BOLD, 12));

		NRCtwoJT = new JTextField();
		NRCtwoJT.setFont(new Font("Ebrima", Font.PLAIN, 20));
		NRCtwoJT.setColumns(10);

		NRCthreeJT = new JTextField();
		NRCthreeJT.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
			}
		});
		NRCthreeJT.setFont(new Font("Ebrima", Font.PLAIN, 20));
		NRCthreeJT.setColumns(10);

		searchORJL = new JLabel("OR");
		searchORJL.setIcon(null);
		searchORJL.setForeground(Color.WHITE);
		searchORJL.setFont(new Font("Ebrima", Font.PLAIN, 15));

		EditartclientJB = new JButton();
		EditartclientJB.addMouseListener(new MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				EditartclientJBmouseClicked(evt);
			}
		});
		EditartclientJB.setText("Edit");
		EditartclientJB.setFont(new Font("Ebrima", Font.BOLD, 12));

		ARTnewsearchJB = new JButton();
		ARTnewsearchJB.addMouseListener(new MouseAdapter() {

			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ARTnewsearchJBmouseClicked(evt);
			}
		});
		ARTnewsearchJB.setText("Detail Search");
		ARTnewsearchJB.setFont(new Font("Ebrima", Font.BOLD, 12));

		javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(
				jPanel7);
		jPanel7Layout
				.setHorizontalGroup(jPanel7Layout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								jPanel7Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel7Layout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																jPanel7Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel7Layout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								jLabel3)
																						.addComponent(
																								ARTmessagedisplay))
																		.addContainerGap(
																				684,
																				Short.MAX_VALUE))
														.addGroup(
																jPanel7Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel7Layout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								ARTnumberJT2,
																								GroupLayout.PREFERRED_SIZE,
																								201,
																								GroupLayout.PREFERRED_SIZE)
																						.addGroup(
																								jPanel7Layout
																										.createSequentialGroup()
																										.addGap(7)
																										.addComponent(
																												lblEnterNrcNumber,
																												GroupLayout.PREFERRED_SIZE,
																												223,
																												GroupLayout.PREFERRED_SIZE))
																						.addGroup(
																								jPanel7Layout
																										.createSequentialGroup()
																										.addComponent(
																												NRCsearchJTF,
																												GroupLayout.PREFERRED_SIZE,
																												92,
																												GroupLayout.PREFERRED_SIZE)
																										.addPreferredGap(
																												ComponentPlacement.UNRELATED)
																										.addComponent(
																												NRCtwoJT,
																												GroupLayout.PREFERRED_SIZE,
																												44,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(18)
																										.addComponent(
																												NRCthreeJT,
																												GroupLayout.PREFERRED_SIZE,
																												31,
																												GroupLayout.PREFERRED_SIZE)))
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				460,
																				Short.MAX_VALUE)
																		.addGroup(
																				jPanel7Layout
																						.createParallelGroup(
																								Alignment.LEADING,
																								false)
																						.addComponent(
																								EditartclientJB,
																								GroupLayout.PREFERRED_SIZE,
																								117,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								ARTsearchJB1,
																								GroupLayout.PREFERRED_SIZE,
																								117,
																								GroupLayout.PREFERRED_SIZE)
																						.addGroup(
																								jPanel7Layout
																										.createSequentialGroup()
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addComponent(
																												ARTnewsearchJB,
																												GroupLayout.PREFERRED_SIZE,
																												142,
																												GroupLayout.PREFERRED_SIZE)))
																		.addContainerGap(
																				23,
																				Short.MAX_VALUE))))
						.addGroup(
								jPanel7Layout
										.createSequentialGroup()
										.addGap(82)
										.addComponent(searchORJL,
												GroupLayout.PREFERRED_SIZE,
												119, GroupLayout.PREFERRED_SIZE)
										.addContainerGap(656, Short.MAX_VALUE)));
		jPanel7Layout
				.setVerticalGroup(jPanel7Layout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								jPanel7Layout
										.createSequentialGroup()
										.addComponent(jLabel3)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												jPanel7Layout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																ARTsearchJB1,
																GroupLayout.PREFERRED_SIZE,
																42,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																ARTnumberJT2,
																GroupLayout.PREFERRED_SIZE,
																42,
																GroupLayout.PREFERRED_SIZE))
										.addGroup(
												jPanel7Layout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																jPanel7Layout
																		.createSequentialGroup()
																		.addGap(12)
																		.addComponent(
																				ARTmessagedisplay)
																		.addGap(11)
																		.addComponent(
																				searchORJL,
																				GroupLayout.PREFERRED_SIZE,
																				16,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(18)
																		.addComponent(
																				lblEnterNrcNumber,
																				GroupLayout.PREFERRED_SIZE,
																				17,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel7Layout
																						.createParallelGroup(
																								Alignment.BASELINE)
																						.addComponent(
																								NRCsearchJTF,
																								GroupLayout.PREFERRED_SIZE,
																								42,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								NRCtwoJT,
																								GroupLayout.PREFERRED_SIZE,
																								42,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								NRCthreeJT,
																								GroupLayout.PREFERRED_SIZE,
																								40,
																								GroupLayout.PREFERRED_SIZE)))
														.addGroup(
																jPanel7Layout
																		.createSequentialGroup()
																		.addGap(77)
																		.addComponent(
																				EditartclientJB,
																				GroupLayout.PREFERRED_SIZE,
																				42,
																				GroupLayout.PREFERRED_SIZE)))
										.addGap(79)
										.addComponent(ARTnewsearchJB,
												GroupLayout.PREFERRED_SIZE, 42,
												GroupLayout.PREFERRED_SIZE)
										.addGap(53)));
		jPanel7.setLayout(jPanel7Layout);

		javax.swing.GroupLayout SearchJPLayout = new javax.swing.GroupLayout(
				SearchJP);
		SearchJPLayout.setHorizontalGroup(SearchJPLayout.createParallelGroup(
				Alignment.LEADING).addComponent(jPanel7, Alignment.TRAILING,
				GroupLayout.DEFAULT_SIZE, 857, Short.MAX_VALUE));
		SearchJPLayout.setVerticalGroup(SearchJPLayout.createParallelGroup(
				Alignment.LEADING).addComponent(jPanel7, Alignment.TRAILING,
				GroupLayout.DEFAULT_SIZE, 366, Short.MAX_VALUE));
		SearchJP.setLayout(SearchJPLayout);

		PatientARTJTP.addTab("Client Search", SearchJP);

		RegimenjPanel
				.addComponentListener(new java.awt.event.ComponentAdapter() {
					public void componentShown(java.awt.event.ComponentEvent evt) {
						RegimenjPanelComponentShown(evt);
					}
				});

		jPanel4.setBackground(new java.awt.Color(102, 102, 102));
		// jPanel4.setBorder(new
		// TitledBorder(UIManager.getBorder("TitledBorder.border"), "",
		// TitledBorder.LEADING, null, null, null));

		currentregimenJL.setFont(new java.awt.Font("Gulim", 0, 11));
		currentregimenJL.setForeground(new java.awt.Color(255, 255, 255));

		dispenseJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		dispenseJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Dispense icon.png"))); // NOI18N
		dispenseJBtn.setText("Dispense");
		dispenseJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				dispenseJBtnMouseClicked(evt);
			}
		});
		dispenseJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dispenseJBtnActionPerformed(evt);
			}
		});

		tblARVdispense.setFont(new java.awt.Font("Ebrima", 0, 20));
		tblARVdispense.setModel(tableModel_fproducts);
		tblARVdispense
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						tblARVdispensePropertyChange(evt);
					}
				});

		/*
		 * tblARVdispense.addFocusListener(new FocusAdapter() {
		 * 
		 * @Override public void focusLost(FocusEvent e) { TableCellEditor tce =
		 * tblARVdispense.getCellEditor(); System.out.print("jajajja"); } });
		 */
		jScrollPane3.setViewportView(tblARVdispense);

		productQtyjLabel.setFont(new java.awt.Font("Segoe UI", 1, 15));
		productQtyjLabel.setForeground(new java.awt.Color(255, 255, 255));

		lblPatientRegimen.setForeground(new java.awt.Color(255, 255, 255));
		lblPatientRegimen.setText("Regimen");

		btnChangeRegimen.setText("Initiate/Change Regimen");
		btnChangeRegimen.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnChangeRegimenMouseClicked(evt);
			}
		});

		txtNoOfDays = new JTextField();
		txtNoOfDays.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent arg0) {

			}
		});
		txtNoOfDays.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				updateQtyFields(Double.valueOf(txtNoOfDays.getText().trim()));
			}
		});
		txtNoOfDays.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				handleNoDaysTextChage();
			}

			public void removeUpdate(DocumentEvent e) {
				handleNoDaysTextChage();
			}

			public void insertUpdate(DocumentEvent e) {
				handleNoDaysTextChage();
			}

			public void handleNoDaysTextChage() {
				if (GenericUtil.isDouble(txtNoOfDays.getText().trim())) {
					updateQtyFields(Double
							.valueOf(txtNoOfDays.getText().trim()));
				} else if (txtNoOfDays.getText().trim().length() == 0) {
					if (!indeterminate) {
						clearQtyFields();
					}
				}
			}
		});
		txtNoOfDays.setColumns(10);

		JLabel lblNumberOfDays = new JLabel("Number of Days");
		lblNumberOfDays.setFont(new Font("Ebrima", Font.PLAIN, 13));
		lblNumberOfDays.setForeground(Color.WHITE);

		// regimendateChooser = new JDateChooser();
		regimendateChooser.getCalendarButton().setFont(
				new Font("Ebrima", Font.PLAIN, 13));
		regimendateChooser
				.addPropertyChangeListener(new PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						regimendateChooserPropertyChange(evt);
					}
				});

		regimendateChooser.setDateFormatString(mydateformat);
		regimendateChooser.setPreferredSize(new Dimension(150, 20));
		regimendateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		regimendateChooser.setDateFormatString("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		regimendateChooser.setDate(cal.getTime());

		JLabel lblNewLabel = new JLabel("Date Dispensed");
		lblNewLabel.setFont(new Font("Ebrima", Font.PLAIN, 13));
		lblNewLabel.setForeground(Color.WHITE);

		JButton button = new JButton();
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				removeregimentab();
			}
		});
		button.setText("Close");
		button.setMinimumSize(new Dimension(159, 41));
		button.setMaximumSize(new Dimension(159, 41));
		button.setFont(new Font("Gulim", Font.BOLD, 11));

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4Layout
				.setHorizontalGroup(jPanel4Layout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				productQtyjLabel)
																		.addContainerGap(
																				845,
																				Short.MAX_VALUE))
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel4Layout
																						.createParallelGroup(
																								Alignment.TRAILING)
																						.addComponent(
																								lblPatientRegimen,
																								GroupLayout.PREFERRED_SIZE,
																								403,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								currentregimenJL,
																								GroupLayout.DEFAULT_SIZE,
																								868,
																								Short.MAX_VALUE))
																		.addGap(23))
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel4Layout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblNumberOfDays,
																								GroupLayout.PREFERRED_SIZE,
																								112,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								lblNewLabel))
																		.addGap(18)
																		.addGroup(
																				jPanel4Layout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								regimendateChooser,
																								GroupLayout.PREFERRED_SIZE,
																								168,
																								GroupLayout.PREFERRED_SIZE)
																						.addGroup(
																								jPanel4Layout
																										.createSequentialGroup()
																										.addComponent(
																												txtNoOfDays,
																												GroupLayout.PREFERRED_SIZE,
																												GroupLayout.DEFAULT_SIZE,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(221)
																										.addComponent(
																												lblPatientRegimen,
																												GroupLayout.PREFERRED_SIZE,
																												403,
																												GroupLayout.PREFERRED_SIZE)))
																		.addGap(31))
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				jScrollPane3,
																				GroupLayout.PREFERRED_SIZE,
																				598,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addGroup(
																				jPanel4Layout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								jPanel4Layout
																										.createSequentialGroup()
																										.addComponent(
																												button,
																												GroupLayout.PREFERRED_SIZE,
																												213,
																												GroupLayout.PREFERRED_SIZE)
																										.addContainerGap())
																						.addGroup(
																								Alignment.TRAILING,
																								jPanel4Layout
																										.createSequentialGroup()
																										.addGroup(
																												jPanel4Layout
																														.createParallelGroup(
																																Alignment.TRAILING)
																														.addComponent(
																																btnChangeRegimen,
																																Alignment.LEADING,
																																GroupLayout.DEFAULT_SIZE,
																																216,
																																Short.MAX_VALUE)
																														.addComponent(
																																dispenseJBtn,
																																GroupLayout.DEFAULT_SIZE,
																																216,
																																Short.MAX_VALUE))
																										.addGap(73)))))));
		jPanel4Layout
				.setVerticalGroup(jPanel4Layout
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap(49, Short.MAX_VALUE)
										.addComponent(productQtyjLabel)
										.addGap(5)
										.addComponent(currentregimenJL)
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																Alignment.TRAILING)
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				lblPatientRegimen,
																				GroupLayout.PREFERRED_SIZE,
																				25,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblNumberOfDays,
																				GroupLayout.PREFERRED_SIZE,
																				17,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(18))
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				lblPatientRegimen,
																				GroupLayout.PREFERRED_SIZE,
																				25,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(30)
																		.addGroup(
																				jPanel4Layout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblNewLabel)
																						.addComponent(
																								regimendateChooser,
																								GroupLayout.PREFERRED_SIZE,
																								32,
																								GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				txtNoOfDays,
																				GroupLayout.PREFERRED_SIZE,
																				34,
																				GroupLayout.PREFERRED_SIZE)))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addComponent(
																jScrollPane3,
																GroupLayout.PREFERRED_SIZE,
																138,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				dispenseJBtn,
																				GroupLayout.PREFERRED_SIZE,
																				38,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addComponent(
																				btnChangeRegimen,
																				GroupLayout.PREFERRED_SIZE,
																				38,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				button,
																				GroupLayout.PREFERRED_SIZE,
																				38,
																				GroupLayout.PREFERRED_SIZE)))
										.addGap(70)));
		jPanel4.setLayout(jPanel4Layout);

		javax.swing.GroupLayout RegimenjPanelLayout = new javax.swing.GroupLayout(
				RegimenjPanel);
		RegimenjPanelLayout.setHorizontalGroup(RegimenjPanelLayout
				.createParallelGroup(Alignment.LEADING).addComponent(jPanel4,
						Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, 857,
						Short.MAX_VALUE));
		RegimenjPanelLayout.setVerticalGroup(RegimenjPanelLayout
				.createParallelGroup(Alignment.LEADING).addComponent(jPanel4,
						GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE));
		RegimenjPanel.setLayout(RegimenjPanelLayout);

		PatientARTJTP.addTab("Regimen Details", RegimenjPanel);

		jPanel5.setBackground(new java.awt.Color(102, 102, 102));
		jPanel5.setBorder(null);

		jLabel4.setFont(new Font("Ebrima", Font.PLAIN, 15));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Enter ART Number here:");

		jLabel5.setFont(new Font("Ebrima", Font.PLAIN, 15));
		jLabel5.setForeground(new java.awt.Color(255, 255, 255));
		jLabel5.setText("Enter NRC here: 9 digits");

		NRCJT.setFont(new java.awt.Font("Ebrima", 0, 20));
		NRCJT.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				NRCJTKeyPressed(evt);
			}
		});

		RegisterJBtn1.setFont(new java.awt.Font("Gulim", 1, 11));
		RegisterJBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Register icon.png"))); // NOI18N
		RegisterJBtn1.setText("Register");
		RegisterJBtn1.setMaximumSize(new java.awt.Dimension(159, 41));
		RegisterJBtn1.setMinimumSize(new java.awt.Dimension(159, 41));
		RegisterJBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				RegisterJBtn1MouseClicked(evt);
			}
		});
		RegisterJBtn1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				RegisterJBtn1ActionPerformed(evt);
			}
		});

		closeJBtn1.setFont(new java.awt.Font("Gulim", 1, 11));
		closeJBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		closeJBtn1.setText("Close");
		closeJBtn1.setMaximumSize(new java.awt.Dimension(159, 41));
		closeJBtn1.setMinimumSize(new java.awt.Dimension(159, 41));
		closeJBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				closeJBtn1MouseClicked(evt);
			}
		});

		ARTnumberJT1.setFont(new java.awt.Font("Ebrima", 0, 20));
		ARTnumberJT1.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyPressed(java.awt.event.KeyEvent evt) {
				ARTnumberJT1KeyPressed(evt);
			}
		});

		JLabel label = new JLabel("First Name");
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Ebrima", Font.PLAIN, 15));

		firstnameJT = new JTextField();
		firstnameJT.setFont(new Font("Ebrima", Font.PLAIN, 20));
		firstnameJT.setColumns(10);

		label_1 = new JLabel("Last Name");
		label_1.setForeground(Color.WHITE);
		label_1.setFont(new Font("Ebrima", Font.PLAIN, 15));

		lastnameJT = new JTextField();
		lastnameJT.setFont(new Font("Ebrima", Font.PLAIN, 20));
		lastnameJT.setColumns(10);

		lblDateOfBirth = new JLabel("Date of Birth");
		lblDateOfBirth.setForeground(Color.WHITE);
		lblDateOfBirth.setFont(new Font("Ebrima", Font.PLAIN, 15));

		dateofbirthdateChooser.setDateFormatString(mydateformat);
		dateofbirthdateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		dateofbirthdateChooser
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						dateofbirthdateChooserPropertyChange(evt);
					}
				});
		dateofbirthdateChooser.getCalendarButton().setFont(
				new Font("Ebrima", Font.PLAIN, 13));
		dateofbirthdateChooser.setPreferredSize(new Dimension(150, 20));
		dateofbirthdateChooser.setDateFormatString("yyyy-MM-dd");

		sexcomboBox_1 = new JComboBox();
		sexcomboBox_1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				sexcomboBoxActionPerformed(evt);
			}
		});
		sexcomboBox_1.setFont(new Font("Ebrima", Font.PLAIN, 20));

		sexcomboBox_1.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				sexcomboBoxpropertyChange(evt);
			}

		});
		sexcomboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Select Gender", "Male", "Female"}));
		sexcomboBox_1.setFont(new Font("Ebrima", Font.PLAIN, 20));

		JLabel lblDateOfRegistration = new JLabel("Date of Registration");
		lblDateOfRegistration.setForeground(Color.WHITE);
		lblDateOfRegistration.setFont(new Font("Ebrima", Font.PLAIN, 15));

		RegistrationdateChooser = new JDateChooser();
		RegistrationdateChooser.getCalendarButton().setFont(
				new Font("Ebrima", Font.PLAIN, 13));
		RegistrationdateChooser.setPreferredSize(new Dimension(150, 20));
		RegistrationdateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		RegistrationdateChooser.setDateFormatString("yyyy-MM-dd hh:mm:ss");
		
		JButton btnCancel = new JButton();
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
				closeRegisterTab();
	
				
			}

			
		});
		btnCancel.setText("Cancel");
		btnCancel.setMinimumSize(new Dimension(159, 41));
		btnCancel.setMaximumSize(new Dimension(159, 41));
		btnCancel.setFont(new Font("Gulim", Font.BOLD, 11));

		javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(
				jPanel5);
		jPanel5Layout.setHorizontalGroup(
			jPanel5Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel5Layout.createSequentialGroup()
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addGroup(jPanel5Layout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(jPanel5Layout.createSequentialGroup()
									.addGap(14)
									.addComponent(jLabel4))
								.addGroup(jPanel5Layout.createSequentialGroup()
									.addContainerGap()
									.addComponent(NRCJT, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE))
								.addGroup(jPanel5Layout.createSequentialGroup()
									.addContainerGap()
									.addComponent(ARTnumberJT1, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE))
								.addGroup(jPanel5Layout.createSequentialGroup()
									.addContainerGap()
									.addComponent(jLabel5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
							.addGap(177))
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(label, GroupLayout.PREFERRED_SIZE, 92, GroupLayout.PREFERRED_SIZE)
							.addGap(422))
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addContainerGap()
							.addGroup(jPanel5Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(lastnameJT, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
								.addComponent(firstnameJT, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 341, Short.MAX_VALUE)
								.addGroup(jPanel5Layout.createSequentialGroup()
									.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED, 197, Short.MAX_VALUE)))
							.addGap(176)))
					.addPreferredGap(ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(closeJBtn1, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE))
						.addComponent(RegisterJBtn1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(sexcomboBox_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addGroup(jPanel5Layout.createParallelGroup(Alignment.LEADING)
							.addComponent(dateofbirthdateChooser, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblDateOfRegistration, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
							.addComponent(RegistrationdateChooser, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblDateOfBirth, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)))
					.addGap(48))
		);
		jPanel5Layout.setVerticalGroup(
			jPanel5Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel5Layout.createSequentialGroup()
					.addComponent(jLabel4)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(ARTnumberJT1, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
					.addGap(24)
					.addComponent(jLabel5)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(NRCJT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
					.addComponent(label, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(firstnameJT, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lastnameJT, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(64, Short.MAX_VALUE))
				.addGroup(jPanel5Layout.createSequentialGroup()
					.addGap(42)
					.addComponent(dateofbirthdateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDateOfRegistration, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
					.addComponent(RegistrationdateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 32, Short.MAX_VALUE)
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addComponent(sexcomboBox_1, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
							.addGap(100))
						.addGroup(jPanel5Layout.createSequentialGroup()
							.addComponent(RegisterJBtn1, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
							.addGap(29)))
					.addGroup(jPanel5Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(closeJBtn1, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnCancel, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
					.addGap(50))
				.addGroup(jPanel5Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblDateOfBirth, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(360, Short.MAX_VALUE))
		);
		jPanel5.setLayout(jPanel5Layout);

		javax.swing.GroupLayout RegisterjPanelLayout = new javax.swing.GroupLayout(
				RegisterjPanel);
		RegisterjPanelLayout.setHorizontalGroup(RegisterjPanelLayout
				.createParallelGroup(Alignment.LEADING).addComponent(jPanel5,
						Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 857,
						Short.MAX_VALUE));
		RegisterjPanelLayout.setVerticalGroup(RegisterjPanelLayout
				.createParallelGroup(Alignment.LEADING).addComponent(jPanel5,
						Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 366,
						Short.MAX_VALUE));
		RegisterjPanel.setLayout(RegisterjPanelLayout);

		PatientARTJTP.addTab("Client Registration", RegisterjPanel);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addContainerGap()
								.addComponent(PatientARTJTP,
										GroupLayout.PREFERRED_SIZE, 862,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(30, Short.MAX_VALUE)));
		layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
				.addGroup(
						layout.createSequentialGroup()
								.addGap(29)
								.addComponent(PatientARTJTP,
										GroupLayout.PREFERRED_SIZE,
										GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addContainerGap(50, Short.MAX_VALUE)));
		this.setLayout(layout);
	}// </editor-fold>

	protected void enablenrcjtextfields() {
		// TODO Auto-generated method stub

	}

	
	private void closeRegisterTab() {
		// TODO Auto-generated method stub
		
		if (this.PatientARTJTP.getTabCount() > 1) {
			for (int i = 0; i <= this.PatientARTJTP.getTabCount(); i++) {
				if (i != 0) {
					this.PatientARTJTP.removeTabAt(i);
				}
			}

	

		} 
		
		
	}
	protected void removeregimentab() {
		// TODO Auto-generated method stub
		NRCsearchJTF.setText("");
		NRCtwoJT.setText("");
		NRCthreeJT.setText("");
		ARTnumberJT2.setText("");
		this.emptyRegistertextfields();
		if (this.PatientARTJTP.getTabCount() > 1) {
			for (int i = 0; i <= this.PatientARTJTP.getTabCount(); i++) {
				if (i != 0) {
					this.PatientARTJTP.removeTabAt(i);
				}
			}

		}
		this.invokeEnabledARTJT();
		this.invokeEnableJT();
	}

	protected void invokeDisableJT() {
		// TODO Auto-generated method stub
		this.NRCsearchJTF.setEnabled(false);
		this.NRCthreeJT.setEnabled(false);
		this.NRCtwoJT.setEnabled(false);
		this.NRCsearchJTF.setBackground(getBackground().DARK_GRAY);
		this.NRCthreeJT.setBackground(getBackground().DARK_GRAY);
		this.NRCtwoJT.setBackground(getBackground().DARK_GRAY);
	}

	private void invokeDisableARTJT() {
		// TODO Auto-generated method stub
		this.ARTnumberJT2.setEnabled(false);
		this.ARTnumberJT2.setBackground(getBackground().DARK_GRAY);

	}

	protected void invokeEnabledARTJT() {
		this.ARTnumberJT2.setEnabled(true);
		this.ARTnumberJT2.setBackground(getBackground().WHITE);
	}

	protected void invokeEnableJT() {
		// TODO Auto-generated method stub
		this.NRCsearchJTF.setEnabled(true);
		this.NRCthreeJT.setEnabled(true);
		this.NRCtwoJT.setEnabled(true);
		this.NRCsearchJTF.setBackground(getBackground().WHITE);
		this.NRCthreeJT.setBackground(getBackground().WHITE);
		this.NRCtwoJT.setBackground(getBackground().WHITE);

	}

	@SuppressWarnings("static-access")
	protected void ARTnewsearchJBmouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub
		new artclientadvancedsearchJD(
				javax.swing.JOptionPane.getFrameForComponent(this), true);
		if (!(this.Artcode.equals(""))) {
			// client found by advanced search
			this.currentregimenJL.setText("ART number : " + Artcode
					+ ", NRC number : " + Artnrc + ", "  );
			currentregimenJL.setFont(new Font("Ebrima", Font.PLAIN, 20));

			if (this.PatientARTJTP.getTabCount() > 1) {
				for (int i = 0; i <= this.PatientARTJTP.getTabCount(); i++) {
					if (i != 0) {
						this.PatientARTJTP.removeTabAt(i);
					}
				}

				this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
				// this.PatientARTJTP.
				this.PatientARTJTP.setSelectedIndex(1);

			} else {
				this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
				// this.PatientARTJTP.
				this.PatientARTJTP.setSelectedIndex(1);

			}

		}

	}

	// GEN-END:initComponents

	private void btnChangeRegimenMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		System.out.println("Change Regimen");
		AppJFrame.glassPane.activate(null);
		new ChangeRegimen(this,
				javax.swing.JOptionPane.getFrameForComponent(this), true,
				artnumber);
		AppJFrame.glassPane.deactivate();
	}

	private Timestamp Convertdatestr(String mydate) {

		try {

			System.out.println(mydate + "Error source 2014");
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			// final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
				Date d = sdf.parse(oldDateString);

				sdf.applyPattern(NEW_FORMAT);
				newDateString = sdf.format(d);
				selecteddate = Timestamp.valueOf(newDateString);
				System.out.println(selecteddate + "for Reveive EXPIRY");

			} catch (ParseException e) {
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
		System.out.println(selecteddate + "same expiry date??");
		return selecteddate;

	}

	private void regimendateChooserPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		try {

			Calendar currentDate = Calendar.getInstance(); // Get the current
															// date
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // format
																				// it
																				// as
																				// per
																				// your
																				// requirement
			String dateNow = formatter.format(currentDate.getTime());
			System.out.println("Now the date is :=>  " + dateNow);

			Date mydate = this.regimendateChooser.getDate();
			String sdate = formatter.format(mydate);
			System.out.println(sdate);
			// this.registerdate = mydate;
			Date d = mydate;
			if (d.after(currentDate.getTime())) {
				System.out.println("Test date diff ");
				productdeliverdate = Convertdatestr(currentDate.getTime()
						.toString());
				JOptionPane.showMessageDialog(this,
						"Dispensing Date cannnot be in the future",

						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				this.regimendateChooser.setDate(new Date());
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

	}

	private Date convertstringtodate(String s) {
		SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
		// String dateInString = "7-Jun-2013";
		Date date = null;
		try {
			date = formatter.parse(s);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		System.out.println(date);
		System.out.println(formatter.format(date));
		return date;
	}

	private void dateofbirthdateChooserPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		try {

			Calendar currentDate = Calendar.getInstance(); // Get the current
															// date
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // format
																				// it
																				// as
																				// per
																				// your
																				// requirement
			String dateNow = formatter.format(currentDate.getTime());
			System.out.println("Now the date is :=>  " + dateNow);

			Date mydate = this.dateofbirthdateChooser.getDate();
			String sdate = formatter.format(mydate);
			System.out.println(sdate);
			this.registerdate = mydate;
			Date d = mydate;
			if (d.after(currentDate.getTime())) {
				System.out.println("Test date diff ");
				productdeliverdate = Convertdatestr(currentDate.getTime()
						.toString());
				JOptionPane.showMessageDialog(this,
						"Registeration Date cannnot be in the future",
						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				this.dateofbirthdateChooser.setDate(new Date());
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

	}

	private void RegimenjPanelComponentShown(java.awt.event.ComponentEvent evt) {
		// TODO add your handling code here:

		// fetch the regimen for the patient here .
		this.tblARVdispense.getTableHeader().setFont(
				new Font("Ebrima", Font.PLAIN, 20));
		Facilityproductsmapper = new FacilityProductsDAO();
		PopulateProgram_ProductTable(arvpatientdarproductsList);
		if ((!(setNRCtxtfield == null)) && (!(this.artnumber == null))) {
			this.currentregimenJL.setText("ART number : " + artnumber
					+ ", NRC number : " + setNRCtxtfield);
			currentregimenJL.setFont(new Font("Ebrima", Font.PLAIN, 20));

		}
	}

	private void hideColumn(TableColumn column) {
		column.setMinWidth(0);
		column.setMaxWidth(0);
		column.setPreferredWidth(0);
	}

	private void closeJBtn1MouseClicked(java.awt.event.MouseEvent evt) {
		this.PatientARTJTP.remove(RegisterjPanel);
		this.ARTmessagedisplay.setText("");
		this.currentregimenJL.setText("");
		// ARTnumberJT1.setEnabled(false);
		ARTnumberJT2.setText("");
		NRCsearchJTF.setText("");
		NRCtwoJT.setText("");
		NRCthreeJT.setText("");
		if (closeJBtn1.getText().toString().equals("Save")) {
			System.out.println("Test ");

			arvClient = new Elmis_Patient();
			if (!(this.ARTnumberJT1.getText().toString().equals(""))) {
				// check length of string
				// ARTnumberJT1.setDocument(new JTextFieldLimit(16));
				String mystring = this.ARTnumberJT1.getText().toString();
				boolean notAlphaNumeric = isAlphaNumeric(mystring);

				// if((validateartnumber(this.ARTnumberJT2.getText().toString())
				// != false) && (this.checkdigit != false)){

				// elmis_patient.setId(this.GenerateGUID());

				arvClient.setArt_number(mystring);
				arvClient.setFirstname(firstnameJT.getText().toString());
				arvClient.setLastname(lastnameJT.getText().toString());
				arvClient.setRegimenId(currentRegid);
				try {
					arvClient.setDateofbirth(registerdate);
					System.out.println(registerdate);
					arvClient.setSex(selectedsex);
					System.out.println(selectedsex);
				} catch (java.lang.NullPointerException c) {
					c.getMessage();
				}
				if (this.NRCJT.getText().toString().length() == 11) {
					arvClient
							.setNrc_number(this.NRCJT.getText().toString());
				} else if (this.NRCJT.getText().toString().length() == 9) {
					String saveNRC = this.conventnrctstring(this.NRCJT
							.getText());
					arvClient.setNrc_number(saveNRC);
				}
				/*
				 * try {
				 * elmis_patient.setRegistration_date(clientregisterdate()); }
				 * catch (ParseException e) { // TODO Auto-generated catch block
				 * e.printStackTrace(); }
				 */

				arvDispensing.updateARVClient(arvClient);
				System.out.println(arvClient);
				arvClient = new Elmis_Patient();
				searchedARTnumber = mystring;
				JOptionPane.showMessageDialog(this, "Update complete !",
						"Saving to Database", JOptionPane.INFORMATION_MESSAGE);
				if (this.PatientARTJTP.getTabCount() > 1) {
					for (int i = 0; i <= this.PatientARTJTP.getTabCount(); i++) {
						if (i != 0) {
							this.PatientARTJTP.removeTabAt(i);
						}
					}

					this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
					// this.PatientARTJTP.
					this.PatientARTJTP.setSelectedIndex(1);

				} else {
					this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
					// this.PatientARTJTP.
					this.PatientARTJTP.setSelectedIndex(1);

				}
				// call dispense form
			} else {
				JOptionPane.showMessageDialog(this, "Invalid ART number!",
						"ART Number Error", JOptionPane.ERROR_MESSAGE);
			}

		} else {
			/*
			 * JOptionPane.showMessageDialog(this, "ART Number is Empty!",
			 * "Update Error", JOptionPane.ERROR_MESSAGE);
			 */
		}

		emptyRegistertextfields();

	}

	private void emptyRegistertextfields() {
		// TODO Auto-generated method stub
		ARTnumberJT1.setText("");
		NRCJT.setText("");
		nrcnumber = "";

	}

	private void NRCJTKeyPressed(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
		// NRCJT.setText("");
		if (this.usenumberpad == true) {
			new arvregisterNumberPadJD(
					javax.swing.JOptionPane.getFrameForComponent(this), true,
					"NRCNUMBER");

			this.NRCJT.setText(arvregisterNumberPadJD.fillregisternrc);
		} else {
			// use keyboard
			// System.out.println("am ready to use keyboard now 2014!!");
			// if(!(NRCJT.getText() == "")){

			// }
		}
	}

	private void popupNRCNumberpad() {
		if (this.NRCsearchmode == false) {
			if (this.usenumberpad == true) {
				new arvregisterNumberPadJD(
						javax.swing.JOptionPane.getFrameForComponent(this),
						true, "NRCNUMBER");

				this.NRCJT.setText(arvregisterNumberPadJD.fillregisternrc);
				this.firstnameJT.requestFocusInWindow();
			} else {
				// use keyboard
				// System.out.println("am ready to use keyboard now 2014!!");
				// if(!(NRCJT.getText() == "")){

				// }
			}
		}
	}

	private void popupARTNumberpad() {
		if (this.ARTsearchmode == false) {
			if (this.usenumberpad == true) {
				new arvregisterNumberPadJD(
						javax.swing.JOptionPane.getFrameForComponent(this),
						true, "ARTNUMBER");
				// ARTnumberJT1.setText(arvregisterNumberPadJD.fillregister);
				this.ARTnumberJT1.setText(arvregisterNumberPadJD.fillregister);
				this.firstnameJT.requestFocusInWindow();
			} else {
				// use keyboard
				// System.out.println("am ready to use keyboard now 2014!!");
				// if(!(ARTnumberJT1.getText() == "")){

				// }
			}
		}
	}

	private void ARTnumberJT1KeyPressed(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:
		// pop up touch screen panel
		// ARTnumberJT1.setText("");
		if (this.usenumberpad == true) {
			new arvregisterNumberPadJD(
					javax.swing.JOptionPane.getFrameForComponent(this), true,
					"ARTNUMBER");
			// ARTnumberJT1.setText(arvregisterNumberPadJD.fillregister);
			this.ARTnumberJT1.setText(arvregisterNumberPadJD.fillregister);
		} else {
			// use keyboard
			// System.out.println("am ready to use keyboard now 2014!!");
			// if(!(ARTnumberJT1.getText() == "")){

			// }
		}
	}

	public void setBoolean(boolean setkeyboradvalue) {
		this.usenumberpad = setkeyboradvalue;
	}

	private void ARTnumberJT2KeyPressed(java.awt.event.KeyEvent evt) {
		// disable the other text field .

		invokeDisableJT();
	}

	private boolean checktableCellinput(String n) {

		try {
			Integer.parseInt(n);
		} catch (NumberFormatException e) {
			return false;

		}
		// only got here if we didn't return false
		return true;
	}

	private void ARTnumberJT2keyTyped(java.awt.event.KeyEvent evt) {
		// call validation function
		letter = evt.getKeyChar();

		if (!(letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {
			if (!(checktableCellinput(ARTnumberJT2.getText()))) {
				ARTnumberJT2.setForeground(Color.RED);
				JOptionPane.showMessageDialog(null, String.format(
						"Physical Count must be a positive number",
						ARTnumberJT2.getText()));
				ARTnumberJT2.setText("");
			}
			System.out.println("test me yeah!!");
		}
	}

	private void JPaneldisplayed(java.awt.event.ComponentEvent evt) {
		// TODO add your handling code here:
		this.PatientARTJTP.remove(RegimenjPanel);
		this.PatientARTJTP.remove(RegisterjPanel);
		this.tblARVdispense.setRowHeight(30);
		NRCsearchJTF.setDocument(new JTextFieldLimit(6));
		NRCtwoJT.setDocument(new JTextFieldLimit(2));
		NRCthreeJT.setDocument(new JTextFieldLimit(1));
		ARTnumberJT2.setDocument(new JTextFieldLimit(13));
		NRCJT.setDocument(new JTextFieldLimit(11));
		lblPatientRegimen.setVisible(false);
		// dateofbirthdateChooser.setToolTipText("Pick a date of birth for client before click on Register");

	}

	@SuppressWarnings("deprecation")
	private void SearchJPdisplayed(java.awt.event.ComponentEvent evt) {
		// TODO add your handling code here:
		System.out.println("I can set button default right??");

		// ARTsearchJB1.isFocusable();
		// ARTsearchJB1.isDefaultButton();

	}

	private void ARTnumberJT2actionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		/*
		 * ARTsearchJB1.requestFocus(); arvDispensingDAO = new
		 * facilityarvdispensingsessionsmappercalls(); elmis_patient = new
		 * Elmis_Patient(); //elmis_patient =
		 * arvDispensingDAO.selectARTnumber(this.ARTnumberJT.getText()); try {
		 * if (!(this.ARTnumberJT2.getText().toString().equals(""))) {
		 * elmis_patient = arvDispensingDAO.dogetcurrentARVclient(art, nrc);
		 * 
		 * if (elmis_patient != null) {
		 * 
		 * this.setNRCtxtfield = elmis_patient.getNrc_number(); artnumber =
		 * elmis_patient.getArt_number(); searchedARTnumber = artnumber; if
		 * (!(artnumber.equals(""))) {
		 * 
		 * artfound = true; if (artfound == true) {
		 * this.ARTmessagedisplay.setText("ART Number found" + "=" + artnumber);
		 * this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
		 * //this.PatientARTJTP. this.PatientARTJTP.setSelectedIndex(1); /*
		 * JOptionPane.showMessageDialog(this,
		 * "ART number found - Patient exists!", artnumber,
		 * JOptionPane.INFORMATION_MESSAGE); } } } else { //patient object is
		 * null
		 * 
		 * if (artnumber.equals("")) {
		 * 
		 * artfound = false; if (artfound == false) { this.ARTmessagedisplay
		 * .setText("ART Number  not found !"); int ok = new arvmessageDialog()
		 * .showDialog( this, "Do  you want to Register client on ART\n\n",
		 * "Process Client details", "YES\n ", "NO \n " );
		 * 
		 * if (ok == JOptionPane.YES_OPTION) {
		 * 
		 * this.PatientARTJTP.addTab( "Client Registration", RegisterjPanel);
		 * this.PatientARTJTP.setSelectedIndex(1); } else if (ok ==
		 * JOptionPane.NO_OPTION) { //this.PatientARTJTP.removeTabAt(2); } } }
		 * 
		 * } } /*if (!(artnumber.equals(""))) {
		 * 
		 * artfound = true; if (artfound == true) {
		 * this.ARTmessagedisplay.setText("ART Number found" + "=" + artnumber);
		 * this.PatientARTJTP.addTab("Current Regimen", RegimenjPanel);
		 * //this.PatientARTJTP. this.PatientARTJTP.setSelectedIndex(1);
		 * JOptionPane.showMessageDialog(this,
		 * "ART number found - Patient exists!", artnumber,
		 * JOptionPane.INFORMATION_MESSAGE); } }
		 */
		/*
		 * if (artnumber.equals("")) {
		 * 
		 * artfound = false; if (artfound == false) {
		 * this.ARTmessagedisplay.setText("ART Number  not found !"); int ok =
		 * new arvmessageDialog() .showDialog( this,
		 * "Do  you want to Registerclient on ART\n\n",
		 * "Process Client details", "YES, To \n Register",
		 * "NO, To\n Exit and consult Clinician ", "Cancel");
		 * 
		 * int confirm = javax.swing.JOptionPane.showConfirmDialog( this,
		 * "Do  you want to register patient on ART\n\n" +
		 * "Yes - to Register\n\n" + "No -to Exit and consult Clinician\n\n",
		 * 
		 * "Start patient ART registration", JOptionPane.YES_NO_OPTION);
		 * 
		 * if (ok == JOptionPane.YES_OPTION) {
		 * 
		 * this.PatientARTJTP.addTab("Client Registration", RegisterjPanel);
		 * this.PatientARTJTP.setSelectedIndex(1); } else if (ok ==
		 * JOptionPane.NO_OPTION) { //this.PatientARTJTP.removeTabAt(2); } } }
		 * 
		 * } catch (NullPointerException e) { e.getMessage(); }
		 */

	}

	private boolean validatenrcnumber(String nrcno) {
		Boolean setTRUE = false;

		if (nrcno.length() == 9) {
			// check if numeric
			for (int i = 0; i <= nrcno.length(); i++) {
				if (nrcno.substring(i).matches("[0-9]")) {
					System.out.println("you are good Number format");
					setTRUE = true;

				}
			}

		} else {
			setTRUE = false;
		}

		return setTRUE;

	}

	private boolean validateartnumber(String artno) {
		Boolean setTRUE = false;
		Integer facone = 0;
		Integer factwo = 0;
		Integer facthre = 0;
		Integer facfour = 0;
		Integer facfive = 0;
		Integer finalfac = 0;
		String cal = "";
		if (artno.length() == 13) {
			// check if numberic
			for (int i = 0; i <= artno.length(); i++) {
				if (artno.substring(i).matches("[0-9]")) {
					System.out.println("you are good Number format");
					setTRUE = true;

				}
			}
			// check sum;
			cal = artno.substring(7, 12);
			System.out.println(cal);
			System.out.println(cal.length());
			String one = Character.toString(cal.charAt(0));
			String two = Character.toString(cal.charAt(1));
			String three = Character.toString(cal.charAt(2));
			String four = Character.toString(cal.charAt(3));
			String five = Character.toString(cal.charAt(4));

			facone += Integer.parseInt(one);
			factwo += Integer.parseInt(two);
			facthre += Integer.parseInt(three);
			facfour += Integer.parseInt(four);
			facfive += Integer.parseInt(five);

			finalfac = (facone + factwo + facthre + facfour + facfive);
			System.out.println(finalfac);
			String str = Integer.toString(finalfac);
			String str1 = artno.substring(artno.length() - 1);
			String str2 = str.substring(str.length() - 1);
			System.out.println(str);
			if (str1.equalsIgnoreCase(str2)) {
				checkdigit = true;
			}

		} else {
			setTRUE = false;
		}

		return setTRUE;

	}

	private String conventartstring(String newartstr) {

		String artformat;

		artformat = newartstr.substring(0, 4) + "-" + newartstr.substring(4, 7)
				+ "-" + newartstr.substring(7, 12) + "-"
				+ newartstr.substring(newartstr.length() - 1);
		System.out.println(artformat);
		return artformat;

	}

	private String conventnrctstring(String newnrctstr) {

		String nrcformat;

		nrcformat = newnrctstr.substring(0, 6) + "/"
				+ newnrctstr.substring(6, 8) + "/"
				+ newnrctstr.substring(newnrctstr.length() - 1);
		System.out.println(nrcformat);

		return nrcformat;
	}

	private void populatePatientARTJTPTabfields(Elmis_Patient object) {
		try {
			currentRegid = object.getRegimenid();
			ARTnumberJT1.setText(object.getArt_number());
			NRCJT.setDocument(new JTextFieldLimit(11));
			NRCJT.setText(object.getNrc_number());
			firstnameJT.setText(object.getFirstname().toString());
			lastnameJT.setText(object.getLastname().toString());
			dateofbirthdateChooser.setDate(object.getDateofbirth());
			this.RegistrationdateChooser.setDate(object.getRegistration_date());
			sexcomboBox_1.setSelectedItem(object.getSex());
		} catch (NullPointerException n) {
			n.getMessage();

		}
	}

	private void EditartclientJBmouseClicked(java.awt.event.MouseEvent evt) {
		EDITING = true;
		RegisterJBtn1.setEnabled(false);
		closeJBtn1.setText("Save");
		closeJBtn1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Save icon.png")));
		String Validstr = "";
		Boolean valid = false;
		String Validnrcstr = "";
		Boolean validnrc = false;
		if ((!(this.ARTnumberJT2.getText().toString().equals("")))) {

			Validstr = this.ARTnumberJT2.getText().toString().trim();

			valid = validateartnumber(Validstr);
			if (valid == true) {
				System.out.println("VALID NOW ");

			}

		}

		if ((!(this.NRCsearchJTF.getText().toString().equals("")))) {
			if ((!(this.NRCtwoJT.getText().toString().equals("")))) {
				if ((!(this.NRCthreeJT.getText().toString().equals("")))) {

					Validnrcstr = this.NRCsearchJTF.getText().toString().trim()
							+ this.NRCtwoJT.getText().toString().trim()
							+ this.NRCthreeJT.getText().toString().trim();

					validnrc = validatenrcnumber(Validnrcstr);
					if (validnrc == true) {
						System.out.println("VALID NOW ");
					}
				}
			}

		}

		arvClient = new Elmis_Patient();
		String artstr;
		String nrcstr;
		// elmis_patient =
		// arvDispensingDAO.selectARTnumber(this.ARTnumberJT.getText());
		if ((!(this.ARTnumberJT2.getText().toString().equals("")))) {

			if ((valid != false) && (this.checkdigit != false)) {
				try {
					// || (!(this.NRCsearchJTF.getText().toString().equals(""))
					// )) {

					artstr = Validstr;
					nrcstr = Validnrcstr;
					artstr = artstr.trim();
					nrcstr = nrcstr.trim();
					if (!(artstr.equals(""))) {
						artnumber = conventartstring(artstr);
					}
					if (!(nrcstr.equals(""))) {
						nrcnumber = conventnrctstring(nrcstr);
					}
					if (!(artstr.equals(""))) {

						arvDispensing.getARVClientByART(artnumber);
					} else if (!(nrcstr.equals(""))) {
						if (arvClient == null) {
							arvClient = arvDispensing.getARVClientByNRC(nrcnumber);
						}
					}

					if (arvClient != null) {
						// if (!(elmis_patient.getArt_number().equals("")) ){

						this.setNRCtxtfield = arvClient.getNrc_number();
						artnumber = arvClient.getArt_number();
						nrcnumber = arvClient.getNrc_number();
						searchedARTnumber = artnumber;
						if (!(artnumber.equals(""))) {

							artfound = true;
							if (artfound == true) {
								if (EDITING == true) {
									// populate tab textfields
									populatePatientARTJTPTabfields(this.arvClient);
									if (this.PatientARTJTP.getTabCount() > 1) {
										for (int i = 0; i <= this.PatientARTJTP
												.getTabCount(); i++) {
											if (i != 0) {
												this.PatientARTJTP
														.removeTabAt(i);
											}
										}
										this.PatientARTJTP.addTab(
												"Edit Client Details",
												RegisterjPanel);
										this.PatientARTJTP.setSelectedIndex(1);
									} else {
										this.PatientARTJTP.addTab(
												"Edit Client Details",
												RegisterjPanel);
										this.PatientARTJTP.setSelectedIndex(1);
									}
								}
							}
						} else if (!(nrcnumber.equals(""))) {

							if (this.PatientARTJTP.getTabCount() > 1) {
								this.PatientARTJTP.removeTabAt(2);
								this.PatientARTJTP.addTab(
										"Edit Client Details", RegisterjPanel);
								this.PatientARTJTP.setSelectedIndex(1);
							} else {
								this.PatientARTJTP.addTab(
										"Edit Client Details", RegisterjPanel);
								this.PatientARTJTP.setSelectedIndex(1);
							}

						}
					} else {
						JOptionPane.showMessageDialog(this,
								"Register Client to Edit!", "ART Cleint ",
								JOptionPane.INFORMATION_MESSAGE);

					}
				} catch (NullPointerException e) {
					e.getMessage();
				}
			} else {

				JOptionPane.showMessageDialog(this, "Invalid ART number!",
						"ART Number Error", JOptionPane.ERROR_MESSAGE);

			}
		}

		/*********************************************************************
		 * /use NRC to search now
		 **********************************************************************/
		if ((!(this.NRCsearchJTF.getText().toString().equals("")))) {

			if ((validnrc != false)) {
				try {

					nrcstr = Validnrcstr;

					nrcstr = nrcstr.trim();

					if (!(nrcstr.equals(""))) {
						nrcnumber = conventnrctstring(nrcstr);
					}

					arvClient = arvDispensing.getARVClientByNRC(nrcnumber);

					if (arvClient != null) {

						this.setNRCtxtfield = arvClient.getNrc_number();
						artnumber = arvClient.getArt_number();
						nrcnumber = arvClient.getNrc_number();
						searchedARTnumber = artnumber;
						if (!(artnumber.equals(""))) {

							artfound = true;
							if (artfound == true) {
								if (this.PatientARTJTP.getTabCount() > 1) {
									this.PatientARTJTP.removeTabAt(2);
									this.PatientARTJTP.addTab(
											"Edit Client Details",
											RegisterjPanel);
									this.PatientARTJTP.setSelectedIndex(1);
								} else {
									this.PatientARTJTP.addTab(
											"Edit Client Details",
											RegisterjPanel);
									this.PatientARTJTP.setSelectedIndex(1);
								}
							}
						} else if (!(nrcnumber.equals(""))) {

							if (this.PatientARTJTP.getTabCount() > 1) {
								this.PatientARTJTP.removeTabAt(2);
								this.PatientARTJTP.addTab(
										"Edit Client Details", RegisterjPanel);
								this.PatientARTJTP.setSelectedIndex(1);
							} else {
								this.PatientARTJTP.addTab(
										"Edit Client Details", RegisterjPanel);
								this.PatientARTJTP.setSelectedIndex(1);
							}
						}
					} else {
						// patient object is null
						JOptionPane.showMessageDialog(this,
								"Register Client to Edit!", "ART Cleint ",
								JOptionPane.INFORMATION_MESSAGE);

					}
				} catch (NullPointerException e) {
					e.getMessage();
				}
			} else {

				JOptionPane.showMessageDialog(this, "Invalid ART number!",
						"ART Number Error", JOptionPane.ERROR_MESSAGE);
				this.ARTnumberJT2.setBackground(getBackground().RED);

			}
		}

	}

	@SuppressWarnings("static-access")
	private void ARTsearchJB1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		// TODO add your handling code here:
		String Validstr = "";
		Boolean valid = false;
		String Validnrcstr = "";
		Boolean validnrc = false;
		if ((!(this.ARTnumberJT2.getText().toString().equals("")))) {
			ARTsearchmode = true;
			// if((!(this.ARTtwoJT.getText().toString().equals("")))){
			// if((!(this.ARTthreeJT.getText().toString().equals("")))){
			// if((!(this.ARTfourJT.getText().toString().equals("")))){
			Validstr = this.ARTnumberJT2.getText().toString().trim();
			// + this.ARTtwoJT.getText().toString().trim()+
			// this.ARTthreeJT.getText().toString().trim() +
			// this.ARTfourJT.getText().toString().trim();

			valid = validateartnumber(Validstr);
			if (valid == true) {
				System.out.println("VALID NOW ");

			}

		}

		if ((!(this.NRCsearchJTF.getText().toString().equals("")))) {
			if ((!(this.NRCtwoJT.getText().toString().equals("")))) {
				if ((!(this.NRCthreeJT.getText().toString().equals("")))) {
					this.NRCsearchmode = true;
					Validnrcstr = this.NRCsearchJTF.getText().toString().trim()
							+ this.NRCtwoJT.getText().toString().trim()
							+ this.NRCthreeJT.getText().toString().trim();

					validnrc = validatenrcnumber(Validnrcstr);
					if (validnrc == true) {
						System.out.println("VALID NOW ");
					}
				}
			}

		}

		arvClient = new Elmis_Patient();
		String artstr;
		String nrcstr;
		// elmis_patient =
		// arvDispensingDAO.selectARTnumber(this.ARTnumberJT.getText());
		if ((!(this.ARTnumberJT2.getText().toString().equals("")))) {

			if ((valid != false) && (this.checkdigit != false)) {
				try {
					// || (!(this.NRCsearchJTF.getText().toString().equals(""))
					// )) {

					artstr = Validstr;
					nrcstr = Validnrcstr;
					artstr = artstr.trim();
					nrcstr = nrcstr.trim();
					if (!(artstr.equals(""))) {
						artnumber = conventartstring(artstr);
					}
					if (!(nrcstr.equals(""))) {
						nrcnumber = conventnrctstring(nrcstr);
					}
					if (!(artstr.equals(""))) {

						arvClient = arvDispensing.getARVClientByART(artnumber);
					} else if (!(nrcstr.equals(""))) {
						if (arvClient == null) {
							arvClient = arvDispensing.getARVClientByNRC(nrcnumber);
						}
					}

					if (arvClient != null) {
						// if (!(elmis_patient.getArt_number().equals("")) ){

						this.setNRCtxtfield = arvClient.getNrc_number();
						artnumber = arvClient.getArt_number();
						nrcnumber = arvClient.getNrc_number();
						searchedARTnumber = artnumber;
						if (!(artnumber.equals(""))) {

							artfound = true;
							if (artfound == true) {

								if (this.PatientARTJTP.getTabCount() > 1) {
									for (int i = 0; i <= this.PatientARTJTP
											.getTabCount(); i++) {
										if (i != 0) {
											this.PatientARTJTP.removeTabAt(i);
										}
									}

									this.PatientARTJTP.addTab(
											"Current Regimen", RegimenjPanel);
									// this.PatientARTJTP.
									this.PatientARTJTP.setSelectedIndex(1);

								} else {
									this.PatientARTJTP.addTab(
											"Current Regimen", RegimenjPanel);
									// this.PatientARTJTP.
									this.PatientARTJTP.setSelectedIndex(1);

								}

							}
						} else if (!(nrcnumber.equals(""))) {

							if (this.PatientARTJTP.getTabCount() > 1) {
								for (int i = 0; i <= this.PatientARTJTP
										.getTabCount(); i++) {
									if (i != 0) {
										this.PatientARTJTP.removeTabAt(i);
									}
								}

								this.PatientARTJTP.addTab("Current Regimen",
										RegimenjPanel);
								// this.PatientARTJTP.
								this.PatientARTJTP.setSelectedIndex(1);

							} else {
								this.PatientARTJTP.addTab("Current Regimen",
										RegimenjPanel);
								// this.PatientARTJTP.
								this.PatientARTJTP.setSelectedIndex(1);

							}

						}
					} else {
						// patient object is null

						// if (artnumber.equals("")) {

						artfound = false;
						if (artfound == false) {
							/*
							 * this.ARTmessagedisplay
							 * .setText("ART Number  not found !");
							 */
							// artnumber = conventartstring( artstr);
							if (!(nrcstr == "")) {
								nrcnumber = conventnrctstring(nrcstr);
							}
							int ok = new arvmessageDialog()
									.showDialog(
											this,
											"Do  you want to Register client on ART\n\n",
											"Process Client details", "YES \n",
											"NO \n ", "New Search");

							if (ok == JOptionPane.YES_OPTION) {
								if (!(ARTnumberJT1.equals(""))) {
									ARTnumberJT1.setText(artnumber);
									System.out.println(artnumber);
									NRCJT.setText(nrcnumber);

								}
								if (this.PatientARTJTP.getTabCount() > 1) {
									for (int i = 0; i <= this.PatientARTJTP
											.getTabCount(); i++) {
										if (i != 0) {
											this.PatientARTJTP.removeTabAt(i);
										}
									}
									this.PatientARTJTP.addTab(
											"Client Registration",
											RegisterjPanel);
									this.PatientARTJTP.setSelectedIndex(1);
								} else {
									this.PatientARTJTP.addTab(
											"Client Registration",
											RegisterjPanel);
									this.PatientARTJTP.setSelectedIndex(1);
								}
							} else if (ok == JOptionPane.NO_OPTION) {
								// this.PatientARTJTP.removeTabAt(2);
							} else if (ok == JOptionPane.CANCEL_OPTION) {
								new artclientadvancedsearchJD(
										javax.swing.JOptionPane
												.getFrameForComponent(this),
										true);
								if (!(this.Artcode.equals(""))) {
									// client found by advanced search
									this.currentregimenJL
											.setText("ART number : " + Artcode
													+ ", NRC number : "
													+ Artnrc);
									currentregimenJL.setFont(new Font("Ebrima",
											Font.PLAIN, 20));

									if (this.PatientARTJTP.getTabCount() > 1) {
										for (int i = 0; i <= this.PatientARTJTP
												.getTabCount(); i++) {
											if (i != 0) {
												this.PatientARTJTP
														.removeTabAt(i);
											}
										}

										this.PatientARTJTP.addTab(
												"Current Regimen",
												RegimenjPanel);
										// this.PatientARTJTP.
										this.PatientARTJTP.setSelectedIndex(1);

									} else {
										this.PatientARTJTP.addTab(
												"Current Regimen",
												RegimenjPanel);
										// this.PatientARTJTP.
										this.PatientARTJTP.setSelectedIndex(1);

									}

								}
							}
						}

					}
				} catch (NullPointerException e) {
					e.getMessage();
				}
			} else {

				JOptionPane.showMessageDialog(this, "Invalid ART number!",
						"ART Number Error", JOptionPane.ERROR_MESSAGE);
				this.ARTnumberJT2.setBackground(getBackground().RED);

			}
		}

		/*********************************************************************
		 * /use NRC to search now
		 **********************************************************************/

		if (((this.ARTnumberJT2.getText().toString().equals("")))) {
			if ((!(this.NRCsearchJTF.getText().toString().equals("")))) {

				if ((validnrc != false)) {
					try {
						// ||
						// (!(this.NRCsearchJTF.getText().toString().equals(""))
						// )) {

						nrcstr = Validnrcstr;

						nrcstr = nrcstr.trim();

						if (!(nrcstr.equals(""))) {
							nrcnumber = conventnrctstring(nrcstr);
						}

						arvClient = arvDispensing.getARVClientByNRC(nrcnumber);

						if (arvClient != null) {
							// if (!(elmis_patient.getArt_number().equals(""))
							// ){

							this.setNRCtxtfield = arvClient.getNrc_number();
							artnumber = arvClient.getArt_number();
							nrcnumber = arvClient.getNrc_number();
							searchedARTnumber = artnumber;
							if (!(artnumber.equals(""))) {

								artfound = true;
								if (artfound == true) {
									/*
									 * this.ARTmessagedisplay.setText(
									 * "ART Number found" + "=" + artnumber);
									 */
									if (this.PatientARTJTP.getTabCount() > 1) {
										for (int i = 0; i <= this.PatientARTJTP
												.getTabCount(); i++) {
											if (i != 0) {
												this.PatientARTJTP
														.removeTabAt(i);
											}
										}

										this.PatientARTJTP.addTab(
												"Current Regimen",
												RegimenjPanel);
										// this.PatientARTJTP.
										this.PatientARTJTP.setSelectedIndex(1);

									} else {
										this.PatientARTJTP.addTab(
												"Current Regimen",
												RegimenjPanel);
										// this.PatientARTJTP.
										this.PatientARTJTP.setSelectedIndex(1);

									}
								}
							} else if (!(nrcnumber.equals(""))) {

								if (this.PatientARTJTP.getTabCount() > 1) {
									for (int i = 0; i <= this.PatientARTJTP
											.getTabCount(); i++) {
										if (i != 0) {
											this.PatientARTJTP.removeTabAt(i);
										}
									}

									this.PatientARTJTP.addTab(
											"Current Regimen", RegimenjPanel);
									// this.PatientARTJTP.
									this.PatientARTJTP.setSelectedIndex(1);

								} else {
									this.PatientARTJTP.addTab(
											"Current Regimen", RegimenjPanel);
									// this.PatientARTJTP.
									this.PatientARTJTP.setSelectedIndex(1);

								}

							}
						} else {
							// patient object is null

							// if (artnumber.equals("")) {

							artfound = false;
							if (artfound == false) {
								/*
								 * this.ARTmessagedisplay
								 * .setText("ART Number  not found !");
								 */
								nrcnumber = conventnrctstring(nrcstr);
								int ok = new arvmessageDialog()
										.showDialog(
												this,
												"Do  you want to Register client on ART\n\n",
												"Process Client details",
												"YES \n", "NO \n ",
												"New Search");

								if (ok == JOptionPane.YES_OPTION) {
									if (!(ARTnumberJT1.equals(""))) {
										ARTnumberJT1.setText(artnumber);
										NRCJT.setText(nrcnumber);

									}
									if (this.PatientARTJTP.getTabCount() > 1) {
										for (int i = 0; i <= this.PatientARTJTP
												.getTabCount(); i++) {
											if (i != 0) {
												this.PatientARTJTP
														.removeTabAt(i);
											}
										}
										this.PatientARTJTP.addTab(
												"Client Registration",
												RegisterjPanel);
										this.PatientARTJTP.setSelectedIndex(1);
									} else {
										this.PatientARTJTP.addTab(
												"Client Registration",
												RegisterjPanel);
										this.PatientARTJTP.setSelectedIndex(1);
									}
								} else if (ok == JOptionPane.NO_OPTION) {
									// this.PatientARTJTP.removeTabAt(2);

								} else if (ok == JOptionPane.CANCEL_OPTION) {
									new artclientadvancedsearchJD(
											javax.swing.JOptionPane
													.getFrameForComponent(this),
											true);
									// art found by advanced search
									if (!(this.Artcode.equals(""))) {
										this.currentregimenJL
												.setText("ART number : "
														+ Artcode
														+ ", NRC number : "
														+ Artnrc);
										currentregimenJL.setFont(new Font(
												"Ebrima", Font.PLAIN, 20));
										if (this.PatientARTJTP.getTabCount() > 1) {
											for (int i = 0; i <= this.PatientARTJTP
													.getTabCount(); i++) {
												if (i != 0) {
													this.PatientARTJTP
															.removeTabAt(i);
												}
											}

											this.PatientARTJTP.addTab(
													"Current Regimen",
													RegimenjPanel);
											// this.PatientARTJTP.
											this.PatientARTJTP
													.setSelectedIndex(1);

										} else {
											this.PatientARTJTP.addTab(
													"Current Regimen",
													RegimenjPanel);
											// this.PatientARTJTP.
											this.PatientARTJTP
													.setSelectedIndex(1);

										}

									}
								}
							}

						}
					} catch (NullPointerException e) {
						e.getMessage();
					}
				} else {

					JOptionPane.showMessageDialog(this, "Invalid NRC number!",
							"NRC Number Error", JOptionPane.ERROR_MESSAGE);
					// this.ARTnumberJT2.setBackground(getBackground().RED);

				}
			}

		}
		// end here
		// emptyRegistertextfields();

	}

	private void regimencloseJbtnMouseClicked(MouseEvent evt) {
		// TODO Auto-generated method stub

		this.ARTmessagedisplay.setText("");
		ARTnumberJT2.setText("");
		ARTnumberJT2.requestFocus();
		searchedARTnumber = "";
		artnumber = "";
		nrcnumber = "";
		this.currentregimenJL.setText("");
		arvpatientdarproductsList.clear();
		this.PatientARTJTP.remove(RegimenjPanel);

		// dispenseJBtn.setMaximumSize(new java.awt.Dimension(159, 41));
		dispenseJBtn.setVisible(true);
		// regimencloseJbtn.setMaximumSize(new java.awt.Dimension(159, 41));
		// closeJBtn.setMaximumSize(new java.awt.Dimension(159, 41));
	}

	private void tblARVdispensePropertyChange(java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		/*
		 * tblARVdispense.getColumnModel().getColumn(TBL_IDX_PRODUCT_CODE).
		 * setMinWidth(0);
		 * tblARVdispense.getColumnModel().getColumn(TBL_IDX_PRODUCT_CODE
		 * ).setMaxWidth(0);
		 * tblARVdispense.getColumnModel().getColumn(TBL_IDX_PRODUCT_CODE
		 * ).setWidth(0);
		 */
		// setting changes for columns
		tblARVdispense.getColumnModel().getColumn(TBL_IDX_PACK_SIZE)
				.setMinWidth(0);
		tblARVdispense.getColumnModel().getColumn(TBL_IDX_PACK_SIZE)
				.setMaxWidth(0);
		tblARVdispense.getColumnModel().getColumn(TBL_IDX_PACK_SIZE)
				.setWidth(0);

		// hideColumn(tblARVdispense.getColumnModel().getColumn(TBL_IDX_BALANCE));

		if ("tableCellEditor".equals(evt.getPropertyName())) {

			// saveShipped_Line_Items = new Shipped_Line_Items();
			/*
			 * if (this.tblARVdispense.isColumnSelected(4)) {
			 * 
			 * } if (this.facilityapprovedProductsJT.isColumnSelected(5)) {
			 * 
			 * }
			 */
			if (this.tblARVdispense.isColumnSelected(TBL_IDX_DOSAGE)) {
				if (tblARVdispense.isEditing()) {
					System.out.println("THIS CELL HAS STARTED EDITING");
					// get column index
					colindex = this.tblARVdispense.getSelectedColumn();
					rowindex = this.tblARVdispense.getSelectedRow();
					Pcode = tableModel_fproducts.getValueAt(
							this.tblARVdispense.getSelectedRow(), 0).toString();

				} else if (!tblARVdispense.isEditing()) {

					try {
						if (GenericUtil.isDouble(txtNoOfDays.getText().trim())) {
							updateQtyFields(Double.valueOf(txtNoOfDays
									.getText().trim()));
						}
						createshipmentObj(Pcode, shipmentdate);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					System.out.println("THIS CELL IS NOT EDITING");

				}
			}
			if (this.tblARVdispense.isColumnSelected(TBL_IDX_QTY_DISPENSED)) {
				if (tblARVdispense.isEditing()) {
					System.out.println("THIS CELL HAS STARTED EDITING");
					// get column index
					colindex = this.tblARVdispense.getSelectedColumn();
					rowindex = this.tblARVdispense.getSelectedRow();
					Pcode = tableModel_fproducts.getValueAt(
							this.tblARVdispense.getSelectedRow(),
							TBL_IDX_PRODUCT_CODE).toString();

				} else if (!tblARVdispense.isEditing()) {
					colindex = this.tblARVdispense.getSelectedColumn();
					rowindex = this.tblARVdispense.getSelectedRow();
					numberOfDaysIsConsistent();
					Double qty = (tableModel_fproducts.getValueAt(rowindex,
							TBL_IDX_QTY_DISPENSED) != null) ? Double
							.parseDouble((tableModel_fproducts.getValueAt(
									rowindex, TBL_IDX_QTY_DISPENSED).toString()
									.trim())) : 0;
					Integer packSize = (tableModel_fproducts.getValueAt(
							rowindex, TBL_IDX_PACK_SIZE) != null) ? Integer
							.parseInt((tableModel_fproducts.getValueAt(
									rowindex, TBL_IDX_PACK_SIZE).toString()
									.trim())) : 0;
					tableModel_fproducts.setValueAt(qty / packSize, rowindex,
							TBL_IDX_EQUIV_BOTTLES);
					tableModel_fproducts.fireTableCellUpdated(rowindex,
							TBL_IDX_EQUIV_BOTTLES);

					System.out.println("THIS CELL IS NOT EDITING");

				}
			}

		}
	}

	public void createshipmentObj(String apcode, Timestamp amodifieddate)
			throws ParseException {
		// System.out.println("Did object call me ???");
		if (GenericUtil.isInteger(tableModel_fproducts.getValueAt(rowindex,
				TBL_IDX_PACK_SIZE).toString())
				&& GenericUtil.isInteger(tableModel_fproducts.getValueAt(
						rowindex, TBL_IDX_EQUIV_BOTTLES).toString())) {
			elmis_dar_transactions = new Elmis_Dar_Transactions();

			if (productdeliverdate == null) {
				java.util.Date date = new java.util.Date();
				amodifieddate = new Timestamp(date.getTime());
			} else {
				amodifieddate = productdeliverdate;
			}

			Integer ARVbottle = 0;

			elmis_dar_transactions.setId(this.GenerateGUID());
			elmis_dar_transactions.setProductcode(apcode);

			this.PACKSIZE = Integer.parseInt(tableModel_fproducts.getValueAt(
					rowindex, TBL_IDX_PACK_SIZE).toString());
			/*
			 * Convert_tablet_to_bottle = Integer.parseInt(tableModel_fproducts
			 * .getValueAt(rowindex, 4).toString()); //"Product Code",
			 * "Pack size", "Product name","Dosage" , "Quantity Dispensed" ,
			 * "Bottles" if (!(this.PACKSIZE == null)) { if
			 * (!(Convert_tablet_to_bottle == null)) {
			 * 
			 * ARVbottle = Convert_tablet_to_bottle / PACKSIZE;
			 * elmis_dar_transactions.setNo_equiv_bottles(ARVbottle);
			 * 
			 * }
			 * 
			 * }
			 */
			elmis_dar_transactions.setNo_tablets_dispensed(Double
					.parseDouble(tableModel_fproducts.getValueAt(rowindex,
							TBL_IDX_QTY_DISPENSED).toString()));
			elmis_dar_transactions.setNo_equiv_bottles(Double
					.parseDouble(tableModel_fproducts.getValueAt(rowindex,
							TBL_IDX_QTY_DISPENSED).toString()));
			elmis_dar_transactions.setTransaction_date(amodifieddate);

			if (!(this.artnumber == null)) {
				elmis_dar_transactions.setArt_number(artnumber);
			}
			// created linked list object

			elmisstockcontrolcardList.add(elmis_dar_transactions);
			System.out.println(elmisstockcontrolcardList.size()
					+ "ARRAY SIZE???");
			System.out.println("Array size here &&&");
			elmis_dar_transactions = new Elmis_Dar_Transactions();
		}

	}

	public void populateClientregistertxtfields(String now, String fillme) {
		if (now == "ARTNUMBER") {
			this.ARTnumberJT1.setText(fillme);

			this.PatientARTJTP.addTab("Client Registration", RegisterjPanel);
		}
		if (now == "NRCNUMBER") {
			this.NRCJT.setText(fillme);
		}
	}

	private void PatientARTJTP(java.awt.event.FocusEvent evt) {
		// TODO add your handling code here:
		/*
		 * Facilityproductsmapper = new
		 * facilityproductssetupsessionmapperscalls();
		 * PopulateProgram_ProductTable(arvpatientdarproductsList);
		 */
	}

	private Double computeDosage(String dosageString) {
		Double numericComponent = extractNumber(dosageString);
		Double quantity = null;
		if (dosageString.contains("tab")) {
			if (dosageString.contains("bd")) {
				quantity = 2 * numericComponent;
			} else if (dosageString.contains("od")) {
				quantity = numericComponent;
			} else {
				return 0.0;
			}
		} else {
			if (dosageString.contains("bd")) {
				quantity = 2 * numericComponent;
			} else if (dosageString.contains("od")) {
				quantity = numericComponent;
			} else {
				return 0.0;
			}
		}
		return quantity;
	}

	private Integer computeDosageInteger(String dosageString) {
		Integer numericComponent = extractInteger(dosageString);
		Integer quantity = null;
		if (dosageString.contains("tab")) {
			if (dosageString.contains("bd")) {
				quantity = 2 * numericComponent;
			} else if (dosageString.contains("od")) {
				quantity = numericComponent;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
		return quantity;
	}

	private Double extractNumber(String s) {
		StringBuffer sBuffer = new StringBuffer();
		Pattern p = Pattern.compile("[0-9]+.[0-9]*|[0-9]*.[0-9]+|[0-9]+");
		Matcher m = p.matcher(s);
		while (m.find()) {
			sBuffer.append(m.group());
		}
		Double numericValue = (sBuffer.toString().trim().length() > 0) ? Double
				.parseDouble(sBuffer.toString()) : 0.0;
		return numericValue;

	}

	private Integer extractInteger(String s) {
		StringBuffer sBuffer = new StringBuffer();
		Pattern p = Pattern.compile("[0-9]+");
		Matcher m = p.matcher(s);
		while (m.find()) {
			sBuffer.append(m.group());
		}
		Integer numericValue = (sBuffer.toString().trim().length() > 0) ? Integer
				.parseInt(sBuffer.toString()) : 0;
		return numericValue;

	}

	private void addCellEditor(List<RegimenProductDosage> dosageUnits,
			final int row) {
		JComboBox<ComboBoxItem> comboBox = new JComboBox();

		for (RegimenProductDosage dosageUnit : dosageUnits) {
			ComboBoxItem item = new ComboBoxItem(dosageUnit);
			comboBox.addItem(item);
		}
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				tblARVdispense.requestFocus();
				// tblARVdispense.changeSelection(row, 6, false, false);
				System.out.print("xxx");
			}
		});
		ComboBoxCellEditor dce = new ComboBoxCellEditor(comboBox);
		editors.add(dce);

	}

	private void updateQtyFields(Double noOfDays) {
		int rows = tableModel_fproducts.getRowCount();
		int columns = tableModel_fproducts.getColumnCount();

		for (int i = 0; i < rows; i++) {
			String dosage = tableModel_fproducts.getValueAt(i, TBL_IDX_DOSAGE)
					.toString().trim();
			Double qtyDispensed = noOfDays * computeDosage(dosage);
			Integer packSize = Integer.valueOf(tableModel_fproducts
					.getValueAt(i, TBL_IDX_PACK_SIZE).toString().trim());

			tableModel_fproducts.setValueAt(qtyDispensed, i,
					TBL_IDX_QTY_DISPENSED);
			tableModel_fproducts.fireTableCellUpdated(i, TBL_IDX_QTY_DISPENSED);

			Double noOfBottles = qtyDispensed / packSize;
			tableModel_fproducts.setValueAt(noOfBottles, i,
					TBL_IDX_EQUIV_BOTTLES);
			tableModel_fproducts.fireTableCellUpdated(i, TBL_IDX_EQUIV_BOTTLES);

		}
	}

	private boolean qtyDispensedIsValid() {
		int rows = tableModel_fproducts.getRowCount();
		int columns = tableModel_fproducts.getColumnCount();
		boolean validDispensation = true;
		for (int row = 0; row < rows; row++) {
			// String dosage = tableModel_fproducts.getValueAt(row,
			// 3).toString().trim();
			String dispensedString = (tableModel_fproducts.getValueAt(row,
					TBL_IDX_EQUIV_BOTTLES) != null) ? tableModel_fproducts
					.getValueAt(row, TBL_IDX_EQUIV_BOTTLES).toString().trim()
					: "";
			String balanceString = (tableModel_fproducts.getValueAt(row,
					TBL_IDX_BALANCE) != null) ? tableModel_fproducts
					.getValueAt(row, TBL_IDX_BALANCE).toString().trim() : "";
			Double qtyDispensed = (GenericUtil.isDouble(dispensedString)) ? Double
					.parseDouble(dispensedString) : null;
			Double balance = (GenericUtil.isDouble(balanceString)) ? Double
					.parseDouble(balanceString) : null;
			Boolean isIncluded = Boolean.valueOf(tableModel_fproducts
					.getValueAt(row, TBL_IDX_INCLUDED_IN_DISPENSATION)
					.toString());
			if (qtyDispensed != null && balance != null && isIncluded) {
				if (qtyDispensed > balance) {
					validDispensation = qtyDispensedValid = false;
					break;
				}
			}
			validDispensation = qtyDispensedValid = true;

		}
		return validDispensation;
	}

	private boolean numberOfDaysIsConsistent() {
		int rows = tableModel_fproducts.getRowCount();
		int columns = tableModel_fproducts.getColumnCount();
		boolean noOfDaysIsConsistent = true;
		Double noOfDays = null;
		for (int row = 0; row < rows; row++) {
			// String dosage = tableModel_fproducts.getValueAt(row,
			// 3).toString().trim();
			String dispensedString = tableModel_fproducts
					.getValueAt(row, TBL_IDX_QTY_DISPENSED).toString().trim();
			Double qtyDispensed = (GenericUtil.isDouble(dispensedString)) ? Double
					.parseDouble(dispensedString) : null;
			Double balance = (GenericUtil.isDouble(dispensedString)) ? Double
					.parseDouble(dispensedString) : null;

			String dosageString = tableModel_fproducts
					.getValueAt(row, TBL_IDX_DOSAGE).toString().trim();
			Double dosage = computeDosage(dosageString);
			if (noOfDays != null && dosage != 0) {
				if (qtyDispensed / dosage != noOfDays) {
					noOfDaysIsConsistent = false;
					indeterminate = true;
					txtNoOfDays.setText(INDETERMINATE);
					break;
				} else {
					DecimalFormat format = new DecimalFormat("0.#");
					if (row == rows - 1) {
						indeterminate = false;
						txtNoOfDays.setText(String.valueOf(format
								.format(qtyDispensed / dosage)));
					}
				}
			} else {
				if (qtyDispensed != null && dosage != 0) {
					noOfDays = qtyDispensed / dosage;
				}
			}
		}
		return noOfDaysIsConsistent;
	}

	private void updateBottleBasedOnQtyDispensed() {

	}

	private void clearQtyFields() {
		int rows = tableModel_fproducts.getRowCount();
		int columns = tableModel_fproducts.getColumnCount();

		for (int i = 0; i < rows; i++) {

			tableModel_fproducts.setValueAt("", i, TBL_IDX_QTY_DISPENSED);
			tableModel_fproducts.fireTableCellUpdated(i, TBL_IDX_QTY_DISPENSED);

			tableModel_fproducts.setValueAt("", i, TBL_IDX_EQUIV_BOTTLES);
			tableModel_fproducts.fireTableCellUpdated(i, TBL_IDX_EQUIV_BOTTLES);
		}
	}

	public void PopulateProgram_ProductTable(List fapprovProducts) {
		tableModel_fproducts.clearTable();
		RegimenDao dao = RegimenDao.getInstance();

		List<Map<String, Object>> prodList = dao.fetchPatientProducts(
				this.artnumber, AppJFrame.getDispensingId("ARV"));
		// lblPatientRegimen shouldn't pick from the list but have a query of
		// its own sth like
		// select regimenname where artnumber
		ElmisRegimen regimen = dao.getRegimenByART(this.artnumber);
		try{
		this.regimenId = regimen.getId();
		
		/*
		 * this.regimenId = (prodList.size() > 0) ? Integer.parseInt(prodList
		 * .get(0).get("regimenid").toString()) : null;
		 */
		lblPatientRegimen.setText("Regimen : " + regimen.getName());
		}catch(NullPointerException n){
			n.getMessage();
		}
		/*
		 * lblPatientRegimen .setText("Regimen : " + String.valueOf((prodList !=
		 * null && prodList.size() > 0) ? prodList .get(0).get("name") : ""));
		 */
		lblPatientRegimen.setVisible(true);
		editors.clear();
		int i = 0;
		Boolean firstTime = false;
		// traitor
		for (Map<String, Object> map : prodList) {
			// "Product Code", "Pack size", "Product name","Dosage" ,
			// "Quantity Dispensed" , "Bottles"
			defaultv_facilityapprovedproducts[TBL_IDX_PRODUCT_CODE] = map
					.get("code");

			defaultv_facilityapprovedproducts[TBL_IDX_PACK_SIZE] = map
					.get("packsize");

			defaultv_facilityapprovedproducts[TBL_IDX_PRODUCT_NAME] = map
					.get("primaryname");

			RegimenProductDosage dUnit = dao.fetchLastDosageForProduct(
					this.artnumber, String.valueOf(map.get("code")), regimenId);

			firstTime = dUnit == null;

			defaultv_facilityapprovedproducts[TBL_IDX_DOSAGE] = (dUnit != null) ? dUnit
					.getName() : "";

			defaultv_facilityapprovedproducts[TBL_IDX_QTY_DISPENSED] = (dUnit != null) ? dao
					.fetchLastQtyDispensed(this.artnumber,
							String.valueOf(map.get("code")), regimenId) : "";

			defaultv_facilityapprovedproducts[TBL_IDX_EQUIV_BOTTLES] = (dUnit != null) ? (Double) defaultv_facilityapprovedproducts[TBL_IDX_QTY_DISPENSED]
					/ (Integer) defaultv_facilityapprovedproducts[TBL_IDX_PACK_SIZE]
					: "";

			defaultv_facilityapprovedproducts[TBL_IDX_BALANCE] = (map
					.get("qty") != null) ? map.get("qty") : 0;
			// ((Integer)
			// defaultv_facilityapprovedproducts[4])/computeDosage((String)
			// defaultv_facilityapprovedproducts[3]);

			addCellEditor(
					RegimenDao.getInstance().fetchDosages(
							(Integer) map.get("regimen_product_id")), i);

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
				cols.add(defaultv_facilityapprovedproducts[j]);
			}

			tableModel_fproducts.insertRow(cols);
		}
		if (!firstTime)
			numberOfDaysIsConsistent();
		tblARVdispense.getColumnModel().getColumn(TBL_IDX_QTY_DISPENSED)
				.setCellRenderer(new QtyDispensedRenderer());
		tblARVdispense.getColumnModel().getColumn(TBL_IDX_EQUIV_BOTTLES)
		.setCellRenderer(new QtyDispensedRenderer());
		/*
		 * try { callfacility = new facilitysetupsessionsmappercalls();
		 * Facilityproductsmapper = new
		 * facilityproductssetupsessionmapperscalls(); arvDispensingDAO = new
		 * facilityarvdispensingsessionsmappercalls();
		 * 
		 * this.facilityprogramcode = "ARV"; //this.facilityproductsource =
		 * ProductsSourceJP.selectedproductsource; //this.facilitytypeCode =
		 * SystemSettingJD.selectedfacilitytypecode;
		 * 
		 * facility = callfacility.getFacility();
		 * 
		 * facilitytype = callfacility.selectAllwithTypes(facility); typeCode =
		 * facilitytype.getCode(); if (!(searchedARTnumber.equals(""))) {
		 * arvpatientdarproductsList = arvDispensingDAO
		 * .doselectpatientarvDispensation(this.searchedARTnumber); //set the
		 * button flags for regimens if(arvpatientdarproductsList.isEmpty()){
		 * closeJBtn.setText("Add Regimen"); closeJBtn.setSize(159,41);
		 * dispenseJBtn.setVisible(false);
		 * 
		 * } } for (@SuppressWarnings("unused") Elmis_Dar_Transactions p :
		 * arvpatientdarproductsList) {
		 * System.out.println(p.getPrimaryname().toString());
		 * System.out.println(p.getProductcode().toString());
		 * System.out.println(p.getArt_number());
		 * System.out.println(p.getNo_equiv_bottles());
		 * 
		 * } tableModel_fproducts.clearTable();
		 * 
		 * arvpatientdarproductsIterator = arvpatientdarproductsList
		 * .listIterator();
		 * 
		 * while (arvpatientdarproductsIterator.hasNext()) {
		 * 
		 * arvpatientdarProducts = arvpatientdarproductsIterator.next();
		 * 
		 * //System.out.print(facilitysccProducts + "");
		 * 
		 * defaultv_facilityapprovedproducts[0] = arvpatientdarProducts
		 * .getProductcode().toString();
		 * 
		 * defaultv_facilityapprovedproducts[1] = arvpatientdarProducts
		 * .getArt_number().toString();
		 * 
		 * defaultv_facilityapprovedproducts[2] = arvpatientdarProducts
		 * .getPrimaryname().toString();
		 * 
		 * defaultv_facilityapprovedproducts[3] = arvpatientdarProducts
		 * .getNo_tablets_dispensed(); TableColumn column1 =
		 * facilityapprovedProductsJT .getColumnModel().getColumn(4);
		 * column1.setCellRenderer(new JDateChooserRenderer());
		 * column1.setCellEditor(new JDateChooserCellEditor());
		 * 
		 * //defaultv_facilityapprovedproducts[4] = column1;
		 * 
		 * ArrayList cols = new ArrayList(); for (int j = 0; j <
		 * columns_facilityApprovedproducts.length; j++) {
		 * cols.add(defaultv_facilityapprovedproducts[j]);
		 * 
		 * }
		 * 
		 * tableModel_fproducts.insertRow(cols);
		 * 
		 * arvpatientdarproductsIterator.remove(); } } catch
		 * (NullPointerException e) { //do something here e.getMessage(); }
		 */
	}

	private Timestamp clientregisterdate() throws ParseException {
		try {
			DateFormat dateFormat = new SimpleDateFormat(
					"EEE MMM d HH:mm:ss z yyyy");
			Date date = new Date();
			dateFormat.format(date);

			String mydate = dateFormat.format(date);
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			// final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";

			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);

			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		}
		return productdeliverdate;
	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	public boolean isAlphaNumeric(String s) {
		String pattern = "^[a-zA-Z0-9]*$";
		if (s.matches(pattern)) {
			return true;
		}
		return false;
	}

	private void sexcomboBoxActionPerformed(ActionEvent evt) {
		JComboBox cb = (JComboBox) evt.getSource();
		selectedsex = (String) cb.getSelectedItem();
		System.out.println(selectedsex);
	}

	private void sexcomboBoxpropertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		try {
			selectedsex = sexcomboBox_1.getSelectedItem().toString();
		} catch (NullPointerException n) {
			n.getMessage();
		}

		System.out.println(selectedsex);
	}

	private void RegisterJBtn1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		arvClient = new Elmis_Patient();

		Date date = new Date();
		if (!(this.ARTnumberJT1.getText().toString().equals(""))) {
			if (!(firstnameJT.getText().equals(""))) {
				if (!(lastnameJT.getText().equals(""))) {
					if (!(selectedsex.equals(""))) {
						if (!(registerdate.after(date))) {

							// check length of string
							// ARTnumberJT1.setDocument(new
							// JTextFieldLimit(16));
							String saveNRC = this.NRCJT.getText();
							String mystring = this.ARTnumberJT1.getText()
									.toString();
							boolean notAlphaNumeric = isAlphaNumeric(mystring);
							if (notAlphaNumeric == false) {
								if (mystring.length() == 16) {

									if (saveNRC.length() == 9
											|| saveNRC.length() == 11) {

										// if((validateartnumber(this.ARTnumberJT2.getText().toString())
										// != false) && (this.checkdigit !=
										// false)){

										if (registerNumpadcall == false) {
											saveNRC = this
													.conventnrctstring(this.NRCJT
															.getText());
										}
										arvClient
												.setId(this.GenerateGUID());
										arvClient.setArt_number(mystring);
										arvClient.setFirstname(firstnameJT
												.getText().toString());
										arvClient.setLastname(lastnameJT
												.getText().toString());
										arvClient.setRegimenId(null);
										try {
											arvClient
													.setDateofbirth(registerdate);
											System.out.println(registerdate);
											arvClient.setSex(selectedsex);
											System.out.println(selectedsex);
										} catch (java.lang.NullPointerException c) {
											c.getMessage();
										}

										arvClient.setNrc_number(saveNRC);
										try {
											arvClient
													.setRegistration_date(clientregisterdate());
										} catch (ParseException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										arvDispensing.registerClient(arvClient);
										arvClient = new Elmis_Patient();
										searchedARTnumber = mystring;
										JOptionPane
												.showMessageDialog(
														this,
														"Registration complete !",
														"Saving to Database",
														JOptionPane.INFORMATION_MESSAGE);
										if (this.PatientARTJTP.getTabCount() > 1) {
											for (int i = 0; i <= this.PatientARTJTP
													.getTabCount(); i++) {
												if (i != 0) {
													this.PatientARTJTP
															.removeTabAt(i);
												}
											}

											this.PatientARTJTP.addTab(
													"Current Regimen",
													RegimenjPanel);
											// this.PatientARTJTP.
											this.PatientARTJTP
													.setSelectedIndex(1);

										} else {
											this.PatientARTJTP.addTab(
													"Current Regimen",
													RegimenjPanel);
											// this.PatientARTJTP.
											this.PatientARTJTP
													.setSelectedIndex(1);

										}
										// call dispense form
										// }
										// set else of the NRC length here .
									} else {
										JOptionPane.showMessageDialog(this,
												"Invalid NRC number!",
												"NRC Number Error",
												JOptionPane.ERROR_MESSAGE);
									}
								} else {
									JOptionPane.showMessageDialog(this,
											"Invalid ART number!",
											"ART Number Error",
											JOptionPane.ERROR_MESSAGE);
								}
							}
						} else {
							JOptionPane
									.showMessageDialog(
											this,
											"Date of Birth can not be now or in the future",
											"Registration Error",
											JOptionPane.ERROR_MESSAGE);
						}

					} else {
						JOptionPane.showMessageDialog(this,
								"choose client sex!", "Registration Error",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(this,
							"Last Name can not be empty!",
							"Registration Error", JOptionPane.ERROR_MESSAGE);
				}
				// end of last name check
			} else {
				JOptionPane.showMessageDialog(this,
						"First Name can not be empty!", "Registration Error",
						JOptionPane.ERROR_MESSAGE);
			}
			// end of first name check
		} else {
			JOptionPane.showMessageDialog(this, "ART Number is Empty!",
					"Registration Error", JOptionPane.ERROR_MESSAGE);
		}

		// Searched by NRC Number
		// ************************************************************************************
		Date date1 = new Date();
		if (!(this.NRCJT.getText().toString().equals(""))) {
			String mysaveART = "";
			if (!(firstnameJT.getText().equals(""))) {
				if (!(lastnameJT.getText().equals(""))) {
					if (!(selectedsex.equals(""))) {
						if (!(registerdate.after(date1))) {
							// Register client
							String mystring = "";
							mystring = NRCJT.getText();
							if ((validateartnumber(this.ARTnumberJT1.getText()
									.toString()) != false)
									&& (this.checkdigit != false)) {
								if (registerNumpadcall == false) {

									mysaveART = conventartstring(this.ARTnumberJT1
											.getText().toString());
								}

								// if((this.validatenrcnumber(mystring)!=
								// false)){
								// String saveART =
								// this.conventnrctstring(mystring);
								arvClient.setId(this.GenerateGUID());
								arvClient.setArt_number(mysaveART);
								arvClient.setNrc_number(this.NRCJT
										.getText());
								arvClient.setFirstname(firstnameJT
										.getText().toString());
								arvClient.setLastname(lastnameJT.getText()
										.toString());
								try {
									arvClient
											.setDateofbirth(dateofbirthdateChooser
													.getDate());
									arvClient.setSex(sexcomboBox_1
											.getSelectedItem().toString());
								} catch (java.lang.NullPointerException c) {
									c.getMessage();
								}
								arvClient.setRegimenId(null);
								try {
									arvClient
											.setRegistration_date(clientregisterdate());
								} catch (ParseException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								arvDispensing.registerClient(arvClient);
								arvClient = new Elmis_Patient();
								searchedARTnumber = mysaveART;
								JOptionPane.showMessageDialog(this,
										"Registration complete !",
										"Saving to Database",
										JOptionPane.INFORMATION_MESSAGE);
								if (this.PatientARTJTP.getTabCount() > 1) {
									this.PatientARTJTP.removeTabAt(2);
									this.PatientARTJTP.addTab(
											"Current Regimen", RegimenjPanel);
									// this.PatientARTJTP.
									this.PatientARTJTP.setSelectedIndex(1);

								} else {
									this.PatientARTJTP.addTab(
											"Current Regimen", RegimenjPanel);
									// this.PatientARTJTP.
									this.PatientARTJTP.setSelectedIndex(1);

								}
								// }

							}
						} else {
							JOptionPane
									.showMessageDialog(
											this,
											"Date of Birth can not be now or in the future!",
											"Registration Error",
											JOptionPane.ERROR_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(this,
								"choose client sex!", "Registration Error",
								JOptionPane.ERROR_MESSAGE);
					}// end of check sex
				} else {
					JOptionPane.showMessageDialog(this,
							"Last Name can not be empty!",
							"Registration Error", JOptionPane.ERROR_MESSAGE);
				}
				// end of last name check

			} else {
				JOptionPane.showMessageDialog(this,
						"first Name can not be empty!", "Registration Error",
						JOptionPane.ERROR_MESSAGE);
			}
			// end of fist name check
		}

		/*
		 * AppJFrame.glassPane.activate(null); new dispenseReceivingJD(
		 * javax.swing.JOptionPane.getFrameForComponent(this), true,
		 * searchedARTnumber); AppJFrame.glassPane.deactivate();
		 */
	}

	private void RegisterJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

	}

	private void dispenseJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void dispenseJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		DecimalFormat df = new DecimalFormat("#.00");

		try {
			if (qtyDispensedIsValid()) {
				if (!(txtNoOfDays.getText().toString().trim().equals(""))) {

					int ok = new arvmessageDialog()
							.showDialog(
									this,
									"Do  you want to save the Dispensed Quantities to the database\n\n",
									"Saving Quantity Dispensed", "YES \n",
									"NO \n ");
					if (ok == JOptionPane.YES_OPTION) {
						int rows = tableModel_fproducts.getRowCount();
						int columns = tableModel_fproducts.getColumnCount();
						Elmis_Dar_Transactions darTxn = new Elmis_Dar_Transactions();
						// "Product Code", "Pack size", "Product name","Dosage"
						// ,
						// "Quantity Dispensed" , "Bottles"
						for (int i = 0; i < rows; i++) {
							boolean itemIncludedInDispensation = Boolean
									.valueOf(tableModel_fproducts.getValueAt(i,
											TBL_IDX_INCLUDED_IN_DISPENSATION)
											.toString());
							if (itemIncludedInDispensation) {
								darTxn.setId(this.GenerateGUID());
								darTxn.setProductcode(tableModel_fproducts
										.getValueAt(i, TBL_IDX_PRODUCT_CODE)
										.toString().trim());

								darTxn.setNo_tablets_dispensed(Double
										.parseDouble(tableModel_fproducts
												.getValueAt(i,
														TBL_IDX_QTY_DISPENSED)
												.toString().trim()));

								double jasperNo_equiv_bottles = Double
										.parseDouble(tableModel_fproducts
												.getValueAt(i,
														TBL_IDX_EQUIV_BOTTLES)
												.toString().trim());

								System.out.println(jasperNo_equiv_bottles
										+ "BEFORE AM CONVERTED");
								jasperNo_equiv_bottles = (Math
										.round(jasperNo_equiv_bottles * 100.0) / 100.0);
								System.out.println(jasperNo_equiv_bottles
										+ "I AM CONVERTED");
								darTxn.setNo_equiv_bottles(jasperNo_equiv_bottles);
								ComboBoxCellEditor editor = editors.get(i);
								darTxn.setDosage_unit_id(((RegimenProductDosage) (((ComboBoxItem) ((JComboBox) editor
										.getComponent()).getSelectedItem())
										.getValue())).getId());
								if (!(productdeliverdate == null)) {
									darTxn.setTransaction_date(productdeliverdate);
								} else {
									darTxn.setTransaction_date(new Timestamp(
											new java.util.Date().getTime()));
								}
								darTxn.setRegimenid(this.regimenId);
								darTxn.setDispensationUnitId(AppJFrame
										.getDispensingId("ARV"));
								if (!(this.artnumber == null)) {
									darTxn.setArt_number(artnumber);
								}
								arvDispensing.saveDARTxN(darTxn);

								arvDispensing.updateARVBalace(darTxn.getNo_equiv_bottles(), darTxn.getProductcode(), AppJFrame.getDispensingId("ARV"));

							}
						}
						JOptionPane.showMessageDialog(this,
								"Products Dispensed from Dispensary!",
								"Updating Database",
								JOptionPane.INFORMATION_MESSAGE);
						this.PatientARTJTP.remove(RegimenjPanel);
						this.artnumber = "";
						this.ARTmessagedisplay.setVisible(false);
						this.ARTnumberJT2.repaint();
						this.ARTnumberJT2.requestFocus();

					} else if (ok == JOptionPane.NO_OPTION) {
						this.txtNoOfDays.requestFocusInWindow();
						this.tblARVdispense.validate();
					}
				} else {
					JOptionPane.showMessageDialog(this,
							"Quantity Dispensed can not be empty",
							"Saving Error", JOptionPane.ERROR_MESSAGE);

				}
			} else {
				JOptionPane.showMessageDialog(_this,
						"You can not dispense more than you have in stock.");
			}
		} catch (java.lang.NullPointerException n) {
			n.getLocalizedMessage();
		}
	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private JButton ARTnewsearchJB;
	private javax.swing.JLabel ARTmessagedisplay;
	private javax.swing.JTextField ARTnumberJT1;
	private javax.swing.JTextField ARTnumberJT2;
	private javax.swing.JButton ARTsearchJB1;
	private javax.swing.JTable tblARVdispense;
	private javax.swing.JTextField NRCJT;
	private javax.swing.JTabbedPane PatientARTJTP;
	private javax.swing.JPanel RegimenjPanel;
	private javax.swing.JButton RegisterJBtn1;
	private javax.swing.JPanel RegisterjPanel;
	private javax.swing.JPanel SearchJP;
	private javax.swing.JButton btnChangeRegimen = new JButton();
	private javax.swing.JButton closeJBtn1;
	private javax.swing.JLabel currentregimenJL;
	private javax.swing.JButton dispenseJBtn;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JPanel jPanel5;
	private javax.swing.JPanel jPanel7;
	private javax.swing.JScrollPane jScrollPane3;
	private javax.swing.JLabel lblPatientRegimen;
	private javax.swing.JLabel productQtyjLabel;
	private JTextField txtNoOfDays;
	private JTextField NRCsearchJTF;
	private JLabel lblEnterNrcNumber;
	private com.toedter.calendar.JDateChooser RegistrationdateChooser;
	private com.toedter.calendar.JDateChooser regimendateChooser;
	private com.toedter.calendar.JDateChooser dateofbirthdateChooser;
	@SuppressWarnings("rawtypes")
	private JComboBox sexcomboBox;
	private JComboBox sexcomboBox_1;
	private JTextField NRCtwoJT;
	private JTextField NRCthreeJT;
	private JLabel searchORJL;
	private JTextField firstnameJT;
	private JLabel label_1;
	private JTextField lastnameJT;
	private JLabel lblDateOfBirth;
	private JButton EditartclientJB;

	// End of variables declaration//GEN-END:variables

	/***************************************************
	 * //start of child class
	 * ***********************************************/

	public static class arvregisterNumberPadJD extends javax.swing.JDialog {

		// private static int numberOfItems = 0;
		// private static String barcodeIn;

		private List<Character> charList = new LinkedList();
		private char c;
		private String ARTnumberstr = "";
		private Products product;
		private String Numberpadcaller;
		// private ProductQty qty;
		public static String fillregister = null;
		public static String fillregisternrc;
		public String fillsubstr;
		public String filldistsubstr;
		public String fillcitizensubsstr;
		public ARTNumberJP artnumberjp;
		public Boolean usekeyboard = false;

		/** Creates new form NumberPadJD */
		/** Creates new form NumberPadJD */
		public arvregisterNumberPadJD(java.awt.Frame parent, boolean modal,
				String src) {
			super(parent, modal);
			initComponents();

			Numberpadcaller = src;
			// this.qtyJTF.setText(numberOfItems + "");

			// this.qtyJTF.setText(qty.getQty() + "");

			this.setSize(320, 550);
			this.setLocationRelativeTo(null);

			this.getRootPane().setDefaultButton(okJBtn);
			this.qtyJTF.setFocusable(false);
			/*
			 * this.subtractJBtn.setFocusable(false);
			 * this.deleteJBtn.setFocusable(false);
			 * this.jButton1.setFocusable(false);
			 * this.jButton2.setFocusable(false);
			 * this.jButton3.setFocusable(false);
			 * this.jButton4.setFocusable(false);
			 * this.jButton5.setFocusable(false);
			 * this.jButton6.setFocusable(false);
			 * this.jButton7.setFocusable(false);
			 * this.jButton8.setFocusable(false);
			 * this.jButton9.setFocusable(false);
			 * this.jButton10.setFocusable(false);
			 */
			// this.jButton1.setFocusable(false);

			this.setVisible(true);

		}

		/**
		 * @wbp.parser.constructor
		 */
		public arvregisterNumberPadJD(java.awt.Frame parent, boolean modal) {
			super(parent, modal);
			initComponents();
			// this.barcodeIn = IssuingJD.barcodeScanned;

			/*
			 * if (!this.product.getPrimaryname().equals("")) {
			 * 
			 * //if the name is too long cut it short for display purpose if
			 * (this.product.getPrimaryname().length() >= 40) {
			 * 
			 * productNameJL.setText(this.product.getPrimaryname().substring( 0,
			 * 40)); } else {
			 * 
			 * productNameJL.setText(this.product.getPrimaryname()); } }
			 * 
			 * this.qtyInStockJL.setText(this.qty.getQty() + "");
			 */
			// this.qtyJTF.setText(numberOfItems + "");

			// this.qtyJTF.setText(qty.getQty() + "");
			this.setSize(332, 550);
			this.setLocationRelativeTo(null);
			btnusekeyboard.setSize(150, 41);

			this.getRootPane().setDefaultButton(okJBtn);
			this.qtyJTF.setFocusable(false);
			/*
			 * this.subtractJBtn.setFocusable(false);
			 * this.deleteJBtn.setFocusable(false);
			 * this.jButton1.setFocusable(false);
			 * this.jButton2.setFocusable(false);
			 * this.jButton3.setFocusable(false);
			 * this.jButton4.setFocusable(false);
			 * this.jButton5.setFocusable(false);
			 * this.jButton6.setFocusable(false);
			 * this.jButton7.setFocusable(false);
			 * this.jButton8.setFocusable(false);
			 * this.jButton9.setFocusable(false);
			 * this.jButton10.setFocusable(false);
			 */
			// this.jButton1.setFocusable(false);

			this.setVisible(true);

		}

		// GEN-BEGIN:initComponents
		// <editor-fold defaultstate="collapsed" desc="Generated Code">
		private void initComponents() {

			jPanel1 = new javax.swing.JPanel();
			jPanel2 = new javax.swing.JPanel();
			btnChangeRegimen = new javax.swing.JButton();
			jButton2 = new javax.swing.JButton();
			jButton3 = new javax.swing.JButton();
			jButton4 = new javax.swing.JButton();
			jButton5 = new javax.swing.JButton();
			jButton6 = new javax.swing.JButton();
			jButton7 = new javax.swing.JButton();
			jButton8 = new javax.swing.JButton();
			jButton9 = new javax.swing.JButton();
			jButton10 = new javax.swing.JButton();
			okJBtn = new javax.swing.JButton();
			deleteJBtn = new javax.swing.JButton();
			subtractJBtn = new javax.swing.JButton();
			addJBtn = new javax.swing.JButton();
			qtyJTF = new javax.swing.JTextField();
			btnusekeyboard = new javax.swing.JButton();
			setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

			jPanel1.setBackground(new java.awt.Color(153, 153, 153));
			jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(
					javax.swing.border.BevelBorder.RAISED));

			jPanel2.setBackground(new java.awt.Color(102, 102, 102));
			jPanel2.setBorder(javax.swing.BorderFactory
					.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

			btnChangeRegimen.setFont(new java.awt.Font("Ebrima", 0, 24));
			btnChangeRegimen.setText("1");
			btnChangeRegimen
					.addMouseListener(new java.awt.event.MouseAdapter() {
						public void mouseClicked(java.awt.event.MouseEvent evt) {
							jButton1MouseClicked(evt);
						}
					});
			btnChangeRegimen
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							jButton1ActionPerformed(evt);
						}
					});

			jButton2.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton2.setText("2");
			jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton2MouseClicked(evt);
				}
			});
			jButton2.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton2ActionPerformed(evt);
				}
			});

			jButton3.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton3.setText("3");
			jButton3.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton3MouseClicked(evt);
				}
			});
			jButton3.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton3ActionPerformed(evt);
				}
			});

			jButton4.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton4.setText("4");
			jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton4MouseClicked(evt);
				}
			});
			jButton4.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton4ActionPerformed(evt);
				}
			});

			jButton5.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton5.setText("5");
			jButton5.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton5MouseClicked(evt);
				}
			});
			jButton5.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton5ActionPerformed(evt);
				}
			});

			jButton6.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton6.setText("6");
			jButton6.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton6MouseClicked(evt);
				}
			});
			jButton6.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton6ActionPerformed(evt);
				}
			});

			jButton7.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton7.setText("7");
			jButton7.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton7MouseClicked(evt);
				}
			});
			jButton7.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton7ActionPerformed(evt);
				}
			});

			jButton8.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton8.setText("8");
			jButton8.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton8MouseClicked(evt);
				}
			});
			jButton8.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton8ActionPerformed(evt);
				}
			});

			jButton9.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton9.setText("9");
			jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton9MouseClicked(evt);
				}
			});
			jButton9.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton9ActionPerformed(evt);
				}
			});

			jButton10.setFont(new java.awt.Font("Ebrima", 0, 24));
			jButton10.setText("0");
			jButton10.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					jButton10MouseClicked(evt);
				}
			});
			jButton10.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					jButton10ActionPerformed(evt);
				}
			});

			okJBtn.setBackground(new java.awt.Color(0, 153, 0));
			okJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
			okJBtn.setForeground(new java.awt.Color(153, 0, 0));
			okJBtn.setText("OK");
			okJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					okJBtnActionPerformed(evt);
				}
			});
			okJBtn.addFocusListener(new java.awt.event.FocusAdapter() {
				public void focusLost(java.awt.event.FocusEvent evt) {
					okJBtnFocusLost(evt);
				}
			});
			deleteJBtn.setFont(new java.awt.Font("Ebrima", 0, 18));
			deleteJBtn.setText("DEL");
			deleteJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					deleteJBtnActionPerformed(evt);
				}
			});

			subtractJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
			subtractJBtn.setIcon(new javax.swing.ImageIcon(getClass()
					.getResource("/images/sort_descending.png"))); // NOI18N
			subtractJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					subtractJBtnActionPerformed(evt);
				}
			});
			// JButton btnusekeyboard = new JButton("use keyboard");
			btnusekeyboard.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent arg0) {
					// set form variable

				}
			});
			btnusekeyboard
					.addActionListener(new java.awt.event.ActionListener() {
						public void actionPerformed(
								java.awt.event.ActionEvent evt) {
							btnusekeyboardActionPerformed(evt);
						}
					});

			addJBtn.setFont(new java.awt.Font("Ebrima", 0, 24));
			addJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
					"/images/sort_ascending.png"))); // NOI18N
			addJBtn.addActionListener(new java.awt.event.ActionListener() {
				public void actionPerformed(java.awt.event.ActionEvent evt) {
					addJBtnActionPerformed(evt);
				}
			});
			addJBtn.addKeyListener(new java.awt.event.KeyAdapter() {
				public void keyTyped(java.awt.event.KeyEvent evt) {
					addJBtnKeyTyped(evt);
				}
			});

			javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
					jPanel2);
			jPanel2.setLayout(jPanel2Layout);
			jPanel2Layout
					.setHorizontalGroup(jPanel2Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel2Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.TRAILING)
															.addComponent(
																	btnChangeRegimen,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	jButton4,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	jButton7,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	subtractJBtn,
																	javax.swing.GroupLayout.Alignment.LEADING,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE)
															.addComponent(
																	deleteJBtn,
																	javax.swing.GroupLayout.DEFAULT_SIZE,
																	70,
																	Short.MAX_VALUE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	addJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	146,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addGroup(
																	jPanel2Layout
																			.createSequentialGroup()
																			.addGroup(
																					jPanel2Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									jButton10,
																									javax.swing.GroupLayout.DEFAULT_SIZE,
																									70,
																									Short.MAX_VALUE)
																							.addComponent(
																									jButton8,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									67,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton5,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									67,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton2,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									67,
																									javax.swing.GroupLayout.PREFERRED_SIZE))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																			.addGroup(
																					jPanel2Layout
																							.createParallelGroup(
																									javax.swing.GroupLayout.Alignment.LEADING)
																							.addComponent(
																									okJBtn,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton9,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton6,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE)
																							.addComponent(
																									jButton3,
																									javax.swing.GroupLayout.PREFERRED_SIZE,
																									70,
																									javax.swing.GroupLayout.PREFERRED_SIZE))
																			.addPreferredGap(
																					javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
											.addContainerGap()));

			jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
					new java.awt.Component[] { deleteJBtn, btnChangeRegimen,
							jButton10, jButton2, jButton3, jButton4, jButton5,
							jButton6, jButton7, jButton8, jButton9, okJBtn,
							subtractJBtn });

			jPanel2Layout
					.setVerticalGroup(jPanel2Layout
							.createParallelGroup(
									javax.swing.GroupLayout.Alignment.LEADING)
							.addGroup(
									jPanel2Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	addJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	subtractJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton3,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton2,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	btnChangeRegimen,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton6,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton5,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton4,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	jButton9,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton8,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton7,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addPreferredGap(
													javax.swing.LayoutStyle.ComponentPlacement.RELATED)
											.addGroup(
													jPanel2Layout
															.createParallelGroup(
																	javax.swing.GroupLayout.Alignment.LEADING)
															.addComponent(
																	okJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	55,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	jButton10,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	58,
																	javax.swing.GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	deleteJBtn,
																	javax.swing.GroupLayout.PREFERRED_SIZE,
																	59,
																	javax.swing.GroupLayout.PREFERRED_SIZE))
											.addContainerGap(
													javax.swing.GroupLayout.DEFAULT_SIZE,
													Short.MAX_VALUE)));

			jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
					new java.awt.Component[] { deleteJBtn, btnChangeRegimen,
							jButton10, jButton2, jButton3, jButton4, jButton5,
							jButton6, jButton7, jButton8, jButton9, okJBtn,
							subtractJBtn });

			qtyJTF.setFont(new java.awt.Font("Ebrima", 0, 14));
			qtyJTF.addFocusListener(new java.awt.event.FocusAdapter() {

				public void focusLost(java.awt.event.FocusEvent evt) {
					qtyJTFFocusLost(evt);
				}
			});
			qtyJTF.addKeyListener(new java.awt.event.KeyAdapter() {
				public void keyTyped(java.awt.event.KeyEvent evt) {
					qtyJTFKeyTyped(evt);
				}
			});

			usekeyboardJbtn = new JButton("Use keyboard");
			usekeyboardJbtn.setIcon(new ImageIcon(arvNumberPadJD.class
					.getResource("/elmis_images/Repeat.png")));
			usekeyboardJbtn.addMouseListener(new MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					usekeyboardJbtnMouseClicked(evt);
				}
			});

			javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
					jPanel1);
			jPanel1Layout
					.setHorizontalGroup(jPanel1Layout
							.createParallelGroup(Alignment.LEADING)
							.addGroup(
									Alignment.TRAILING,
									jPanel1Layout
											.createSequentialGroup()
											.addContainerGap()
											.addGroup(
													jPanel1Layout
															.createParallelGroup(
																	Alignment.TRAILING)
															.addComponent(
																	jPanel2,
																	Alignment.LEADING,
																	GroupLayout.DEFAULT_SIZE,
																	245,
																	Short.MAX_VALUE)
															.addComponent(
																	qtyJTF,
																	Alignment.LEADING,
																	GroupLayout.PREFERRED_SIZE,
																	245,
																	GroupLayout.PREFERRED_SIZE)
															.addComponent(
																	usekeyboardJbtn,
																	GroupLayout.PREFERRED_SIZE,
																	133,
																	GroupLayout.PREFERRED_SIZE))
											.addContainerGap()));
			jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
					Alignment.LEADING).addGroup(
					jPanel1Layout
							.createSequentialGroup()
							.addContainerGap()
							.addComponent(qtyJTF, GroupLayout.PREFERRED_SIZE,
									46, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED,
									GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE,
									GroupLayout.DEFAULT_SIZE,
									GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(usekeyboardJbtn,
									GroupLayout.PREFERRED_SIZE, 35,
									GroupLayout.PREFERRED_SIZE).addGap(384)));
			jPanel1.setLayout(jPanel1Layout);

			javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
					getContentPane());
			layout.setHorizontalGroup(layout.createParallelGroup(
					Alignment.LEADING).addGroup(
					layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE,
									GroupLayout.DEFAULT_SIZE,
									GroupLayout.PREFERRED_SIZE)
							.addContainerGap(27, Short.MAX_VALUE)));
			layout.setVerticalGroup(layout.createParallelGroup(
					Alignment.LEADING).addGroup(
					layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE,
									574, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(301, Short.MAX_VALUE)));
			getContentPane().setLayout(layout);

			pack();
		}// </editor-fold>

		private void usekeyboardJbtnMouseClicked(MouseEvent evt) {
			// TODO Auto-generated method stub
			System.out.println("close the form now");
			usekeyboard = false;
			usenumberpad = usekeyboard;
			System.out.println(usekeyboard + "~~~");
			System.out.println(usenumberpad + "is now");
			this.dispose();
		}

		// GEN-END:initComponents

		private void jButton10MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton10.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton9MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton9.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton8MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:

			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton8.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton7MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton7.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton6MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton6.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton5MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton5.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton4.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton3MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:

			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton3.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton2MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			this.ARTnumberstr = this.ARTnumberstr + "" + jButton2.getText();
			this.qtyJTF.setText(this.ARTnumberstr);
		}

		private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {
			// TODO add your handling code here:
			System.out.println("One button clicked ??");
			this.ARTnumberstr = this.ARTnumberstr + ""
					+ btnChangeRegimen.getText();
			this.qtyJTF.setText(this.ARTnumberstr);

		}

		private void addJBtnKeyTyped(java.awt.event.KeyEvent evt) {
			// TODO add your handling code here:

			c = evt.getKeyChar();

			charList.add(c);

		}

		private void okJBtnFocusLost(java.awt.event.FocusEvent evt) {
			// TODO add your handling code here:
			okJBtn.requestFocus();
		}

		private void addJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:

		}

		private void subtractJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			if (!qtyJTF.getText().equals("")) {

				qtyJTF.setText(qtyJTF.getText().substring(0,
						qtyJTF.getText().length() - 1));

			}
		}

		private void btnusekeyboardActionPerformed(
				java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:

			usekeyboard = true;
			// System.out.println("close the child form");
			// System.out.println("Test Close form");
			// arvNumberPadJD.this.dispose();

		}

		private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButton18A7ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButton1n15ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButtoton13ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void jButleteJBtn1ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
		}

		private void okJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// check source of call
			System.out.println("Check OK button !");
			if (this.Numberpadcaller.equals("ARTNUMBER")) {

				ARTNumberJP.registerNumpadcall = true;

				if (this.ARTnumberstr.length() == 13) {

					String newartstr = this.ARTnumberstr;
					String artformat;

					artformat = newartstr.substring(0, 4) + "-"
							+ newartstr.substring(4, 7) + "-"
							+ newartstr.substring(7, 12) + "-"
							+ newartstr.substring(newartstr.length() - 1);

					this.fillregister = artformat;

				} else if (this.ARTnumberstr.length() < 13) {
					JOptionPane.showMessageDialog(this,
							"ART number can not be less than 13 digits",
							"ART Number Error", JOptionPane.ERROR_MESSAGE);
				} else if (this.ARTnumberstr.length() > 13) {
					JOptionPane.showMessageDialog(this,
							"ART number can not be more than 13 digits",
							"ART Number Error", JOptionPane.ERROR_MESSAGE);
				}

			} else if (this.Numberpadcaller.equals("NRCNUMBER")) {

				ARTNumberJP.registerNumpadcall = true;
				try {
					String split = "/";
					if (this.ARTnumberstr.length() == 9) {
						this.fillregister = this.ARTnumberstr;
						fillsubstr = this.fillregister.substring(0, 6);
						filldistsubstr = this.fillregister.substring(6, 8);
						fillcitizensubsstr = this.fillregister.substring(8, 9);
						this.fillregisternrc = fillsubstr + split
								+ filldistsubstr + split + fillcitizensubsstr;
					} else if (this.ARTnumberstr.length() > 9) {
						JOptionPane.showMessageDialog(this,
								"NRC number can not be more than 9 digits",
								"NRC Number Error", JOptionPane.ERROR_MESSAGE);

					} else if (this.ARTnumberstr.length() < 9) {
						JOptionPane.showMessageDialog(this,
								"NRC number can not be less than 9 digits",
								"NRC Number Error", JOptionPane.ERROR_MESSAGE);

					}

				} catch (java.lang.StringIndexOutOfBoundsException n) {
					n.getMessage();
				}

			}
			// arvNumberPadJD.this.setDefaultCloseOperation(HIDE_ON_CLOSE);
			arvregisterNumberPadJD.this.dispose();

		}

		public String getData() {

			return qtyJTF.getText();

		}

		private void qtyJTFFocusLost(java.awt.event.FocusEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.requestFocusInWindow();
		}

		private void deleteJBtnActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			if (!qtyJTF.getText().equals("")) {

				qtyJTF.setText("");
				this.ARTnumberstr = "";

			}
		}

		private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "0");

			getStockLimit(0);

		}

		private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "9");
			getStockLimit(9);

		}

		private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "8");
			getStockLimit(8);
		}

		private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "7");

			getStockLimit(7);
		}

		private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "6");
			getStockLimit(6);
		}

		private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "5");
			getStockLimit(5);
		}

		private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "4");
			getStockLimit(4);
		}

		private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "3");
			getStockLimit(3);
		}

		private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "2");
			getStockLimit(2);
		}

		private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {

			// TODO add your handling code here:
			// qtyJTF.setText(qtyJTF.getText() + "1");

			// getStockLimit(1);

			this.qtyJTF.setFocusable(true);

		}

		private void qtyJTFKeyTyped(java.awt.event.KeyEvent evt) {
			// TODO add your handling code here:

			final char c = evt.getKeyChar();
			if (!(Character.isDigit(c) || (c == KeyEvent.VK_PERIOD)
					|| (c == KeyEvent.VK_COMMA)
					|| (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
				this.getToolkit().beep();
				evt.consume();
			}
		}

		private void getStockLimit(int qtyToAdd) {

			// int qtyInStock = Integer.parseInt(qtyInStockJL.getText());
			// int requestedQty = Integer.parseInt(qtyJTF.getText() + qtyToAdd);

			/*
			 * if (requestedQty > qtyInStock) {
			 * 
			 * javax.swing.JOptionPane.showMessageDialog(this, "Over limit "); }
			 * else if (requestedQty <= qtyInStock) {
			 * 
			 * //javax.swing.JOptionPane.showMessageDialog(this,
			 * "Qty requested " // + requestedQty + "\nStock " + qtyInStock +
			 * "\nQty is JTF " // + qtyJTF.getText());
			 * 
			 * qtyJTF.setText(qtyJTF.getText() + qtyToAdd); }
			 */

			// requestedQty = 0;
			// return overLimit;

		}

		/**
		 * @param args
		 *            the command line arguments
		 */
		/*
		 * public static void main(String args[]) {
		 * java.awt.EventQueue.invokeLater(new Runnable() { public void run() {
		 * arvNumberPadJD dialog = new arvNumberPadJD( new javax.swing.JFrame(),
		 * true); dialog.addWindowListener(new java.awt.event.WindowAdapter() {
		 * public void windowClosing(java.awt.event.WindowEvent e) {
		 * System.exit(0); } }); dialog.setVisible(true); } }); }
		 */

		// GEN-BEGIN:variables
		// Variables declaration - do not modify
		private javax.swing.JButton addJBtn;
		private javax.swing.JButton deleteJBtn;
		private javax.swing.JButton btnChangeRegimen;
		private javax.swing.JButton jButton10;
		private javax.swing.JButton jButton2;
		private javax.swing.JButton jButton3;
		private javax.swing.JButton jButton4;
		private javax.swing.JButton jButton5;
		private javax.swing.JButton jButton6;
		private javax.swing.JButton jButton7;
		private javax.swing.JButton jButton8;
		private javax.swing.JButton jButton9;
		private javax.swing.JPanel jPanel1;
		private javax.swing.JPanel jPanel2;
		private javax.swing.JButton okJBtn;
		private javax.swing.JTextField qtyJTF;
		private javax.swing.JButton subtractJBtn;
		private javax.swing.JButton btnusekeyboard;
		private JButton usekeyboardJbtn;
		// End of variables declaration//GEN-END:variables

	}

	public class ComboBoxCellEditor extends DefaultCellEditor {

		/**
		 * Creates a new ComboBoxCellEditor.
		 * 
		 * @param comboBox
		 *            the comboBox that should be used as the cell editor.
		 */
		public ComboBoxCellEditor(final JComboBox<ComboBoxItem> comboBox) {
			super(comboBox);

			comboBox.removeActionListener(this.delegate);

			this.delegate = new EditorDelegate() {
				@Override
				public void setValue(final Object value) {
					comboBox.setSelectedItem(value);
				}

				@Override
				public Object getCellEditorValue() {
					return comboBox.getSelectedItem();
				}

				@Override
				public boolean shouldSelectCell(final EventObject anEvent) {
					if (anEvent instanceof MouseEvent) {
						final MouseEvent e = (MouseEvent) anEvent;
						return e.getID() != MouseEvent.MOUSE_DRAGGED;
					}
					return true;
				}

				@Override
				public boolean stopCellEditing() {
					if (comboBox.isEditable()) {
						// Commit edited value.
						comboBox.actionPerformed(new ActionEvent(
								ComboBoxCellEditor.this, 0, ""));
					}
					return super.stopCellEditing();
				}

				@Override
				public void actionPerformed(final ActionEvent e) {
					ComboBoxCellEditor.this.stopCellEditing();
				}
			};
			comboBox.addActionListener(this.delegate);
		}
	}
}
