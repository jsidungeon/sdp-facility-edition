/*
 * NetworkSettingsJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.util.Properties;

import org.elmis.facility.network.NetworkPropertiesFile;
import org.elmis.facility.tools.NetworkProperties;
import org.elmis.facility.ws.client.LocalWSClient;
import org.elmis.facility.ws.client.SyncRequestResult;

import javax.swing.JButton;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 *
 * @author  __USER__
 */
public class NetworkSettingsJD extends javax.swing.JDialog {

	private NetworkPropertiesFile details;
	private Properties prop;
	private NetworkSettingsJD _this = this;
	/** Creates new form NetworkSettingsJD */
	public NetworkSettingsJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setTitle("View Database Settings");
		initComponents();

		prop = new NetworkProperties().readPropertiesFile();

		this.dbHostJTF.setText(prop.getProperty("dbhost"));

		this.dbNameJTF.setText(prop.getProperty("dbname"));
		this.dbHostJTF.setText(prop.getProperty("dbhost"));
		this.dbPasswordJTF.setText(prop.getProperty("dbpassword"));
		this.dbPortJTF.setText(prop.getProperty("dbport"));
		this.dbUserJTF.setText(prop.getProperty("dbuser"));

		//TODO improve on this
		if (prop.getProperty("dbdriver").equalsIgnoreCase(
				"org.hsqldb.jdbcDriver")) {

			this.dbTypeJCB.setSelectedItem("Local");

		} else if (prop.getProperty("dbdriver").equalsIgnoreCase(
				"org.postgresql.Driver")) {

			this.dbTypeJCB.setSelectedItem("Server");

		}

		this.setSize(500, 455);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();
		dbTypeJCB = new javax.swing.JComboBox();
		jLabel4 = new javax.swing.JLabel();
		dbNameJTF = new javax.swing.JTextField();
		jLabel5 = new javax.swing.JLabel();
		dbHostJTF = new javax.swing.JTextField();
		jLabel7 = new javax.swing.JLabel();
		jLabel8 = new javax.swing.JLabel();
		dbUserJTF = new javax.swing.JTextField();
		jLabel9 = new javax.swing.JLabel();
		dbPortJTF = new javax.swing.JTextField();
		dbPasswordJTF = new javax.swing.JTextField();
		saveAndConnectJBtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS database side icon.png"))); // NOI18N

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Database settings");

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel3.setText("Database Type");

		dbTypeJCB.setFont(new java.awt.Font("Gulim", 0, 11));
		dbTypeJCB.setModel(new javax.swing.DefaultComboBoxModel(new String[] {
				"Server", "Satellite", "Standalone" }));
		dbTypeJCB.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dbTypeJCBActionPerformed(evt);
			}
		});

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel4.setText("Database Name");

		jLabel5.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel5.setForeground(new java.awt.Color(255, 255, 255));
		jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel5.setText("Database Password");

		jLabel7.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel7.setForeground(new java.awt.Color(255, 255, 255));
		jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel7.setText("Database Host");

		jLabel8.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel8.setForeground(new java.awt.Color(255, 255, 255));
		jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel8.setText("Database User");

		jLabel9.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel9.setForeground(new java.awt.Color(255, 255, 255));
		jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel9.setText("Database Port");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								javax.swing.GroupLayout.Alignment.TRAILING,
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												90,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								jLabel3,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								111,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel4,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								111,
																								Short.MAX_VALUE)
																						.addComponent(
																								jLabel9,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								111,
																								Short.MAX_VALUE)
																						.addComponent(
																								jLabel7,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								111,
																								Short.MAX_VALUE)
																						.addComponent(
																								jLabel8,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								111,
																								Short.MAX_VALUE)
																						.addComponent(
																								jLabel5,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								119,
																								Short.MAX_VALUE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.LEADING)
																						.addComponent(
																								dbPasswordJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								156,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								dbNameJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								156,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								dbUserJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								156,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								dbHostJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								156,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								dbPortJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								156,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								dbTypeJCB,
																								0,
																								156,
																								Short.MAX_VALUE))
																		.addGap(
																				47,
																				47,
																				47))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel2)
																		.addContainerGap()))));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { dbHostJTF, dbNameJTF, dbPasswordJTF,
						dbPortJTF, dbTypeJCB, dbUserJTF });

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { jLabel3, jLabel4, jLabel5, jLabel7,
						jLabel8, jLabel9 });

		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(jLabel1)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel2)
																		.addGap(
																				16,
																				16,
																				16)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel3,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								19,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								dbTypeJCB,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addGap(
																				7,
																				7,
																				7)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jLabel4)
																						.addComponent(
																								dbNameJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								dbPortJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel9))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.TRAILING)
																						.addComponent(
																								dbHostJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel7))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								dbUserJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel8))
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								dbPasswordJTF,
																								javax.swing.GroupLayout.PREFERRED_SIZE,
																								javax.swing.GroupLayout.DEFAULT_SIZE,
																								javax.swing.GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								jLabel5))))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { dbHostJTF, dbNameJTF, dbPasswordJTF,
						dbPortJTF, dbTypeJCB, dbUserJTF });

		jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { jLabel3, jLabel4, jLabel5, jLabel7,
						jLabel8, jLabel9 });

		saveAndConnectJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		saveAndConnectJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Save icon.png"))); // NOI18N
		saveAndConnectJBtn.setText("Save And Connect");
		saveAndConnectJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						saveAndConnectJBtnActionPerformed(evt);
					}
				});
		
		JButton btnSyncDatabase = new JButton("Sync Database");
		btnSyncDatabase.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				LocalWSClient wsClient = new LocalWSClient();
				SyncRequestResult result = wsClient.sync();
					JOptionPane.showMessageDialog(_this, result.getRemark());
				
			}
		});
		btnSyncDatabase.setFont(new Font("Ebrima", Font.BOLD, 12));
		btnSyncDatabase.setHorizontalAlignment(SwingConstants.LEADING);
		btnSyncDatabase.setIcon(new ImageIcon(NetworkSettingsJD.class.getResource("/images/sync-db.jpg")));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(layout.createSequentialGroup()
							.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(layout.createSequentialGroup()
							.addComponent(btnSyncDatabase, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(saveAndConnectJBtn)
							.addGap(26))))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSyncDatabase)
						.addComponent(saveAndConnectJBtn))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void dbTypeJCBActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		if (dbTypeJCB.getSelectedItem().toString().equalsIgnoreCase(
				"Standalone")
				|| dbTypeJCB.getSelectedItem().toString().equalsIgnoreCase(
						"Satellite")) {
			dbHostJTF.setVisible(false);
			dbNameJTF.setVisible(false);
			dbPasswordJTF.setVisible(false);
			dbPortJTF.setVisible(false);
			dbUserJTF.setVisible(false);
			jLabel4.setVisible(false);
			jLabel5.setVisible(false);
			jLabel7.setVisible(false);
			jLabel8.setVisible(false);
			jLabel9.setVisible(false);

		} else if (dbTypeJCB.getSelectedItem().toString().equalsIgnoreCase(
				"Server")) {

			dbHostJTF.setVisible(true);
			dbNameJTF.setVisible(true);
			dbPasswordJTF.setVisible(true);
			dbPortJTF.setVisible(true);
			dbUserJTF.setVisible(true);
			jLabel4.setVisible(true);
			jLabel5.setVisible(true);
			jLabel7.setVisible(true);
			jLabel8.setVisible(true);
			jLabel9.setVisible(true);

			//	dbHostJTF.setText(System.getProperty("dbParentServer"));
		}
	}

	private void saveAndConnectJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		this.details = new NetworkPropertiesFile();

		this.details.setdbname(this.dbNameJTF.getText());
		this.details.setdbhost(this.dbHostJTF.getText());
		this.details.setdbpassword(this.dbPasswordJTF.getText());
		this.details.setdbport(this.dbPortJTF.getText());
		this.details.setdbuser(this.dbUserJTF.getText());

		if (this.dbTypeJCB.getSelectedItem().toString().trim()
				.equalsIgnoreCase("Server")) {

			this.details.setdbdriver("org.postgresql.Driver");
			this.details.setdbMode("Server");

			//if this is a server save as parent database server as well
			this.details.setdbParentServer(this.dbHostJTF.getText());

		} else if (dbTypeJCB.getSelectedItem().toString().equalsIgnoreCase(
				"Standalone")
				|| dbTypeJCB.getSelectedItem().toString().equalsIgnoreCase(
						"Satellite")) {

			//this.details.setdbdriver("org.hsqldb.jdbcDriver");
			this.details.setClientType(dbTypeJCB.getSelectedItem().toString()
					.trim());
			this.details.setdbMode("local");
		}

		//only Write to properties file if
		//Server Mode is selected 

		boolean ok = new NetworkProperties().writeToPropertiesFile(details);

		if (ok == true) {

			javax.swing.JOptionPane
					.showMessageDialog(
							this,
							"Database settings saved \n you must restart the application for setting to \n for new setting to take effect");

			int yes = javax.swing.JOptionPane.showConfirmDialog(this,
					"Restart Now ?");
			if (yes == 0) {

				System.exit(0);
			} else {

				this.dispose();
			}
		} else {

			javax.swing.JOptionPane.showMessageDialog(this,
					"Error saving settings");
		}

	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				NetworkSettingsJD dialog = new NetworkSettingsJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JTextField dbHostJTF;
	private javax.swing.JTextField dbNameJTF;
	private javax.swing.JTextField dbPasswordJTF;
	private javax.swing.JTextField dbPortJTF;
	private javax.swing.JComboBox dbTypeJCB;
	private javax.swing.JTextField dbUserJTF;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JLabel jLabel8;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JButton saveAndConnectJBtn;
}