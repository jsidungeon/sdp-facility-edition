package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.model.Facility;

public class FacilityDao {
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<Facility> selectAll()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<Facility> list = session.selectList("Facility.getAll");
			return list;
		} finally {
			session.close();
		}
	}
	public List<Facility> getFacilities(int geoZoneId)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			return session.selectList("Facility.getDistrict_facilities",geoZoneId);
		} finally {
			session.close();
		}
	}
	public void setSelectedFacility(String facilityCode)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			session.delete("Facility.deletefacility");
			int x = session.insert("Facility.setfacility",facilityCode);
			session.commit();
			if (x != 0)
				System.out.println("Select insert worked...");
			else
				System.out.println("Conflict somewhere...");
		} finally {
			session.close();
		}
	}
}
