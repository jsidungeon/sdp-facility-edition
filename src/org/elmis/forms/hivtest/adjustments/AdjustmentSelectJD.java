/*
 * SelectProductsJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.hivtest.adjustments;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.HivTestLossesAdustments;
import org.elmis.facility.domain.model.HivTestProducts;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.forms.hivtest.physicalcount.NumberPadJD;

import com.oribicom.tools.TableModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import java.awt.Component;

/**
 * 
 * @author __USER__
 */
public class AdjustmentSelectJD extends javax.swing.JDialog {

	//public static String barcodeScanned;
	//public static int selectedProductID;
	//public static Products selectedProduct;

	//private List<Products> productsList = new LinkedList();
	//private List<HivTestPhysicalCount> PhysicalCountList = new LinkedList();
	//private HivTestPhysicalCount physicalCount;

	// Create tableModel for Program Products for each facility
	private static final String[] columns_products = { "Product Code",
			"Product name" };
	private static final Object[] defaultv_products = { "", "" };
	private static final int rows_products = 0;
	public static TableModel tableModel_products = new TableModel(
			columns_products, defaultv_products, rows_products);

	private HivTestProducts hivTestProducts;
	private ListIterator<HivTestProducts> hivTestProductsIterator;
	private List<HivTestProducts> hivTestProductsList = new LinkedList();

	// Create tableModel for Program Products for each facility
	private static final String[] columns_adjustType = { "Adjustment Type" };
	private static final Object[] defaultv_adjustType = { "", "" };
	private static final int rows_adjustType = 0;
	public static TableModel tableModel_adjustType = new TableModel(
			columns_adjustType, defaultv_adjustType, rows_adjustType);

	private Losses_Adjustments_Types adjustments;
	private ListIterator<Losses_Adjustments_Types> adjustmentsIterator;
	private List<Losses_Adjustments_Types> adjustmentsList = new LinkedList();

	private HivTestLossesAdustments lossAdjust;
	private List<HivTestLossesAdustments> adjustmentList = new LinkedList();

	/** Creates new form SelectProductsJD */

	public AdjustmentSelectJD(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		initComponents();

		this.setSize(700, 500);
		this.setLocationRelativeTo(null);
		
		backJBtn.setVisible(false);

		this.jPanel3.getRootPane().setDefaultButton(this.addAdjustmentJBtn);

		//this.loginJIF.getRootPane().setDefaultButton(loginJBtn);

		this.jTabbedPane1.remove(2);
		this.jTabbedPane1.remove(1);

		this.productsJTable.getColumnModel().getColumn(0).setMinWidth(120);
		this.productsJTable.getColumnModel().getColumn(0).setMaxWidth(120);

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			this.hivTestProductsList = session
					.selectList("selectHIVTestProducts");

			this.populateProductsTable(hivTestProductsList);

			this.adjustmentsList = session.selectList("selectAllAdjustments");

			this.populateAdjustmentsType(adjustmentsList);

		} finally {
			session.close();
		}

		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jTabbedPane1 = new javax.swing.JTabbedPane();
		jPanel1 = new javax.swing.JPanel();
		jScrollPane1 = new javax.swing.JScrollPane();
		productsJTable = new javax.swing.JTable();
		jPanel2 = new javax.swing.JPanel();
		jScrollPane2 = new javax.swing.JScrollPane();
		adjustTypeJTable = new javax.swing.JTable();
		jPanel3 = new javax.swing.JPanel();
		jPanel4 = new javax.swing.JPanel();
		jLabel2 = new javax.swing.JLabel();
		adjustTypeJL = new javax.swing.JLabel();
		numberPadJL = new javax.swing.JLabel();
		qtyJTF = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		addAdjustmentJBtn = new javax.swing.JButton();
		cancelJBtn = new javax.swing.JButton();
		cancelJBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				dispose();
			}
		});
		backJBtn = new javax.swing.JButton();
		backJBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				int selectedtab = jTabbedPane1.getSelectedIndex() ;
				
				if( selectedtab == 1){
					
					backJBtn.setVisible(false);
				}
				if(selectedtab > 0 ){

				for (int j = jTabbedPane1.getTabCount(); j > selectedtab; j--) {

					jTabbedPane1.remove(j - 1);

				}
				}
				
				
			}
		});
		jSeparator1 = new javax.swing.JSeparator();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Select Product for losses and adjustment");

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});

		productsJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		productsJTable.setModel(tableModel_products);
		productsJTable.setRowHeight(60);
		productsJTable.setTableHeader(null);
		productsJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				productsJTableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(productsJTable);

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout.setHorizontalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 444,
				Short.MAX_VALUE));
		jPanel1Layout.setVerticalGroup(jPanel1Layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE, 428, Short.MAX_VALUE));

		jTabbedPane1.addTab("Select Product", jPanel1);

		adjustTypeJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		adjustTypeJTable.setModel(tableModel_adjustType);
		adjustTypeJTable.setRowHeight(60);
		adjustTypeJTable.setTableHeader(null);
		adjustTypeJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				adjustTypeJTableMouseClicked(evt);
			}
		});
		jScrollPane2.setViewportView(adjustTypeJTable);

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addComponent(jScrollPane2, GroupLayout.DEFAULT_SIZE, 444, Short.MAX_VALUE)
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addComponent(jScrollPane2, GroupLayout.PREFERRED_SIZE, 331, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		jPanel2.setLayout(jPanel2Layout);

		jTabbedPane1.addTab("tab2", jPanel2);

		jPanel3.setBackground(new java.awt.Color(102, 102, 102));

		jPanel4.setBackground(new java.awt.Color(1, 129, 130));

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 14));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("Adjustment Type:");

		adjustTypeJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		adjustTypeJL.setForeground(new java.awt.Color(255, 255, 255));
		adjustTypeJL.setText("jLabel5");

		numberPadJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Calculator icon.png"))); // NOI18N
		numberPadJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				numberPadJLMouseClicked(evt);
			}
		});

		qtyJTF.setFont(new java.awt.Font("Tahoma", 0, 24));
		qtyJTF.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
		qtyJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyTyped(java.awt.event.KeyEvent evt) {
				qtyJTFKeyTyped(evt);
			}
		});

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 14));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Qty :");

		javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(
				jPanel4);
		jPanel4.setLayout(jPanel4Layout);
		jPanel4Layout
				.setHorizontalGroup(jPanel4Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(jLabel2)
														.addComponent(jLabel3))
										.addGap(10, 10, 10)
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																jPanel4Layout
																		.createSequentialGroup()
																		.addComponent(
																				qtyJTF,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				125,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				numberPadJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				57,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addComponent(
																adjustTypeJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																277,
																Short.MAX_VALUE))
										.addContainerGap()));
		jPanel4Layout
				.setVerticalGroup(jPanel4Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel4Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addComponent(jLabel2)
														.addComponent(
																adjustTypeJL,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																39,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel4Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																false)
														.addComponent(
																qtyJTF,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																62,
																Short.MAX_VALUE)
														.addComponent(
																jLabel3,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																62,
																Short.MAX_VALUE)
														.addComponent(
																numberPadJL,
																javax.swing.GroupLayout.Alignment.LEADING,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE))
										.addContainerGap(85, Short.MAX_VALUE)));

		jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { adjustTypeJL, jLabel2 });

		addAdjustmentJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		addAdjustmentJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/new adjustmet.png"))); // NOI18N
		addAdjustmentJBtn.setText("Add Adjustment");
		addAdjustmentJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						addAdjustmentJBtnActionPerformed(evt);
					}
				});

		javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(
				jPanel3);
		jPanel3Layout.setHorizontalGroup(
			jPanel3Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel3Layout.createSequentialGroup()
					.addGroup(jPanel3Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel3Layout.createSequentialGroup()
							.addContainerGap()
							.addComponent(jPanel4, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE))
						.addGroup(jPanel3Layout.createSequentialGroup()
							.addGap(123)
							.addComponent(addAdjustmentJBtn, GroupLayout.PREFERRED_SIZE, 193, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		jPanel3Layout.setVerticalGroup(
			jPanel3Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
					.addContainerGap(58, Short.MAX_VALUE)
					.addComponent(jPanel4, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(addAdjustmentJBtn, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
					.addGap(31))
		);
		jPanel3.setLayout(jPanel3Layout);

		jTabbedPane1.addTab("tab3", jPanel3);

		cancelJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.setText("Cancel");

		backJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		backJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Back.png"))); // NOI18N
		backJBtn.setText("Back");

		jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGap(59)
					.addComponent(backJBtn, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(cancelJBtn)
					.addGap(18)
					.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(122, Short.MAX_VALUE))
				.addComponent(jTabbedPane1, GroupLayout.DEFAULT_SIZE, 453, Short.MAX_VALUE)
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jTabbedPane1, GroupLayout.PREFERRED_SIZE, 357, GroupLayout.PREFERRED_SIZE)
					.addGap(30)
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(cancelJBtn, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
							.addComponent(backJBtn, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
						.addComponent(jSeparator1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(90, Short.MAX_VALUE))
		);
		layout.linkSize(SwingConstants.VERTICAL, new Component[] {cancelJBtn, backJBtn});
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void addAdjustmentJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//lossAdjust = new HivTestLossesAdustments();

		ArrayList<String> adjustList = new ArrayList<String>();

		//lossAdjust.setCreated_by(AppJFrame.userName);
		//lossAdjust.setQty(Integer.parseInt(qtyJTF.getText()));
		//lossAdjust.setLoss_adjustment_type(tableModel_adjustType.getValueAt(adjustTypeJTable.getSelectedRow(), 0).toString());

		//adjustmentList.add(lossAdjust);

		adjustList.add(tableModel_products.getValueAt(
				this.productsJTable.getSelectedRow(), 0).toString());
		adjustList.add(tableModel_products.getValueAt(
				this.productsJTable.getSelectedRow(), 1).toString());
		adjustList.add(tableModel_adjustType.getValueAt(
				this.adjustTypeJTable.getSelectedRow(), 0).toString());

		if (adjustTypeJL.getText().toString().equals("DAMAGED")||adjustTypeJL.getText().toString().equals("TRANSFER_OUT")	
			||adjustTypeJL.getText().toString().equals("LOST")||adjustTypeJL.getText().toString().equals("STOLEN")
			||adjustTypeJL.getText().toString().equals("EXPIRED")||adjustTypeJL.getText().toString().equals("PASSED_OPEN_VIAL_TIME_LIMIT")
			||adjustTypeJL.getText().toString().equals("COLD_CHAIN_FAILURE"))
		{
			adjustList.add("-"+ qtyJTF.getText());
		}else
		{
			adjustList.add(qtyJTF.getText());
		}
		
		adjustList.add("DELETE");


		AdjustmentTableJD.tableModel_adjustmenst.insertRow(adjustList);

		///AdjustmentTableJD.populateAdjustmentsTable(adjustmentList);

		this.dispose();
	}

	private void qtyJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		final char c = evt.getKeyChar();
		if (!(Character.isDigit(c) || (c == KeyEvent.VK_PERIOD)
				|| (c == KeyEvent.VK_COMMA) || (c == KeyEvent.VK_BACK_SPACE) || (c == KeyEvent.VK_DELETE))) {
			this.getToolkit().beep();
			evt.consume();
		}

	}

	private void numberPadJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		String qty = new NumberPadJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true).getQty();

		qtyJTF.setText(qty);
	}

	private void adjustTypeJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		this.jTabbedPane1.addTab("QTY", this.jPanel3);

		this.jTabbedPane1.setSelectedIndex(2);

		this.adjustTypeJL.setText(tableModel_adjustType.getValueAt(
				this.adjustTypeJTable.getSelectedRow(), 0).toString());
	}

	private void productsJTableMouseClicked(java.awt.event.MouseEvent evt) {

		this.jTabbedPane1.addTab("Adjustments Type", this.jPanel2);

		this.jTabbedPane1.setSelectedIndex(1);
		backJBtn.setVisible(true);
	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		if (jTabbedPane1.getSelectedIndex() == 1
				&& jTabbedPane1.getTabCount() == 3) {

			jTabbedPane1.remove(2);
		}

		if (jTabbedPane1.getSelectedIndex() == 0
				&& jTabbedPane1.getTabCount() == 2) {// &&

			// remove the products JTable
			jTabbedPane1.remove(1);

		}
	}

	public void populateProductsTable(List progProducts) {

		tableModel_products.clearTable();
		hivTestProductsIterator = progProducts.listIterator();

		while (hivTestProductsIterator.hasNext()) {

			hivTestProducts = hivTestProductsIterator.next();

			defaultv_products[0] = hivTestProducts.getProduct_code();
			defaultv_products[1] = hivTestProducts.getDar_display_name();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_products.length; j++) {
				cols.add(defaultv_products[j]);

			}

			tableModel_products.insertRow(cols);

			hivTestProductsIterator.remove();
		}
	}

	public void populateAdjustmentsType(List progProducts) {

		tableModel_adjustType.clearTable();
		adjustmentsIterator = progProducts.listIterator();

		while (adjustmentsIterator.hasNext()) {

			adjustments = adjustmentsIterator.next();

			defaultv_adjustType[0] = adjustments.getName();

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_adjustType.length; j++) {
				cols.add(defaultv_adjustType[j]);

			}

			tableModel_adjustType.insertRow(cols);

			adjustmentsIterator.remove();
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				AdjustmentSelectJD dialog = new AdjustmentSelectJD(
						new javax.swing.JFrame(), true);
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton addAdjustmentJBtn;
	private javax.swing.JLabel adjustTypeJL;
	private javax.swing.JTable adjustTypeJTable;
	private javax.swing.JButton cancelJBtn;
	private javax.swing.JButton backJBtn;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel4;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JSeparator jSeparator1;
	public static javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JLabel numberPadJL;
	private javax.swing.JTable productsJTable;
	private javax.swing.JTextField qtyJTF;
	// End of variables declaration//GEN-END:variables

}