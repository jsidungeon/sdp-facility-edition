
package org.elmis.forms.regimen;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.ElmisRegimenCategory;
import org.elmis.forms.regimen.dao.RegimenDao;
import org.elmis.forms.regimen.util.ComboBoxItem;
import org.elmis.forms.stores.arv_dispensing.ARTNumberJP;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChangeRegimen extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel pnlMain = new JPanel();
	private JTable table;
	private String artNumber = "";
	private ARTNumberJP artNoJP;
	private JLabel lblPatientArt = new JLabel("<Dynamic>");
	private JComboBox<ComboBoxItem> cboRegimens = new JComboBox<ComboBoxItem>();
	private JComboBox<ComboBoxItem> cboRegimenCategories =  new JComboBox<ComboBoxItem>();
	private ChangeRegimen _this;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			RegimenDao.setSystemProperties();
			ChangeRegimen dialog = new ChangeRegimen(null ,null, true, "");
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void setUpCategoriesCombo() {
		RegimenDao dao = RegimenDao.getInstance();
		if(cboRegimenCategories == null) cboRegimenCategories = new JComboBox<ComboBoxItem>();
		List<ElmisRegimenCategory> regCategories = dao.fetchRegimenCategories();
		cboRegimenCategories.setBounds(224, 67, 232, 20);
		cboRegimenCategories.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setUpRegimensCombo();
			}
		});
		cboRegimenCategories.removeAllItems();
		for (ElmisRegimenCategory elmisRegimenCategory : regCategories) {
			cboRegimenCategories
					.addItem(new ComboBoxItem(elmisRegimenCategory));
		}

	}
	
	@SuppressWarnings({ })
	private void setUpRegimensCombo() {
		RegimenDao dao = RegimenDao.getInstance();
		if(cboRegimens == null) cboRegimens =  new JComboBox<ComboBoxItem>();
		Integer selectedCategoryId = ((ElmisRegimenCategory)(((ComboBoxItem)(this.cboRegimenCategories.getSelectedItem())).getValue())).getId();
		List<ElmisRegimen> regimens = dao.fetchElmisRegimenByCategory(selectedCategoryId);
		cboRegimens.setBounds(224, 102, 232, 20);
		cboRegimens.removeAllItems();
		for (ElmisRegimen elmisRegimen : regimens) {
			cboRegimens.addItem(new ComboBoxItem(elmisRegimen));
		}

	}
	
private Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prop;
	}

	/**
	 * Create the dialog.
	 */
	public ChangeRegimen(ARTNumberJP artNumberJP, java.awt.Frame parent, boolean modal , String artNo) {
		super(parent, modal);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("Initiate/Change Regimen");
		setResizable(false);
		setBounds(100, 100, 548, 264);
		this.artNumber = artNo;
		lblPatientArt.setBounds(224, 32, 232, 14);
		lblPatientArt.setText(artNo);
		this.artNoJP = artNumberJP;
		this.setLocationRelativeTo(null);
		_this = this;
		getContentPane().setLayout(new BorderLayout());
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnlMain, BorderLayout.CENTER);
		pnlMain.setLayout(null);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(89, 20, 0, 2);
		pnlMain.add(separator);
										
										table = new JTable();
										table.setBounds(94, 21, 0, 0);
										pnlMain.add(table);
										
										JButton btnCancel = new JButton("Cancel");
										btnCancel.setBounds(360, 157, 102, 23);
										btnCancel.addMouseListener(new MouseAdapter() {
											@Override
											public void mouseClicked(MouseEvent e) {
												_this.dispose();
											}
										});
										pnlMain.add(btnCancel);
										
										JButton btnChangeRegimen = new JButton("Save");
										btnChangeRegimen.setBounds(234, 157, 102, 23);
										btnChangeRegimen.addMouseListener(new MouseAdapter() {
											@Override
											public void mouseClicked(MouseEvent arg0) {
												RegimenDao dao = RegimenDao.getInstance();
												
												int dialogButton = JOptionPane.YES_NO_OPTION;
												if (JOptionPane.showConfirmDialog (null, "Confirm New Regimen!","Warning",dialogButton) == JOptionPane.YES_OPTION){
													dao.updatePatientRegimenByART(artNumber, ((ElmisRegimen)(((ComboBoxItem)cboRegimens.getSelectedItem()).getValue())).getId());
													artNoJP.PopulateProgram_ProductTable(null);
													_this.dispose();	
												}

											}
										});
										pnlMain.add(btnChangeRegimen);
										pnlMain.add(cboRegimens);
										
										JLabel label = new JLabel("Regimen");
										label.setBounds(69, 105, 113, 14);
										pnlMain.add(label);
										
										JLabel label_1 = new JLabel("Regimen Category");
										label_1.setBounds(69, 70, 152, 14);
										pnlMain.add(label_1);
										pnlMain.add(cboRegimenCategories);
										pnlMain.add(lblPatientArt);
										
										JLabel label_3 = new JLabel("ART Number");
										label_3.setBounds(69, 33, 113, 14);
										pnlMain.add(label_3);
										
										setUpCategoriesCombo();
										setUpRegimensCombo();
										_this.setVisible(true);
										System.out.println();
	}
	
}

