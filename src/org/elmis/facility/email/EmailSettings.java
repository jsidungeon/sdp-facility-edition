package org.elmis.facility.email;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.oribicom.tools.NetworkMode;

/**
 * EmailSettings entity. @author MyEclipse Persistence Tools
 */
//@Entity
//@Table(name = "email_settings", catalog = "legalserver")
public class EmailSettings implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String displayName;
	private String emailAddress;
	private String accountType;
	private String incomingMail;
	private String outgoingMail;
	private String incomingPort;
	private String outgoingPort;
	private String userName;
	private String password;
	private String securePassword;
	private String smtpAuth;

	// Constructors

	/** default constructor */
	public EmailSettings() {
	}

	/** minimal constructor */
	public EmailSettings(String id) {
		this.id = id;
	}

	/** full constructor */
	public EmailSettings(String id, String displayName, String emailAddress,
			String accountType, String incomingMail, String outgoingMail,
			String incomingPort, String outgoingPort, String userName,
			String password, String securePassword, String smtpAuth) {
		this.id = id;
		this.displayName = displayName;
		this.emailAddress = emailAddress;
		this.accountType = accountType;
		this.incomingMail = incomingMail;
		this.outgoingMail = outgoingMail;
		this.incomingPort = incomingPort;
		this.outgoingPort = outgoingPort;
		this.userName = userName;
		this.password = password;
		this.securePassword = securePassword;
		this.smtpAuth = smtpAuth;
	}

	// Property accessors
	//@Id
	//@Column(name = "id", unique = true, nullable = false, length = 100)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	//@Column(name = "display_name", length = 225)
	public String getDisplayName() {
		return this.displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	//@Column(name = "email_address", length = 300)
	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	//@Column(name = "account_type", length = 100)
	public String getAccountType() {
		return this.accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	//@Column(name = "incoming_mail")
	public String getIncomingMail() {
		return this.incomingMail;
	}

	public void setIncomingMail(String incomingMail) {
		this.incomingMail = incomingMail;
	}

	//@Column(name = "outgoing_mail")
	public String getOutgoingMail() {
		return this.outgoingMail;
	}

	public void setOutgoingMail(String outgoingMail) {
		this.outgoingMail = outgoingMail;
	}

	//@Column(name = "incoming_port", length = 12)
	public String getIncomingPort() {
		return this.incomingPort;
	}

	public void setIncomingPort(String incomingPort) {
		this.incomingPort = incomingPort;
	}

	//@Column(name = "outgoing_port", length = 12)
	public String getOutgoingPort() {
		return this.outgoingPort;
	}

	public void setOutgoingPort(String outgoingPort) {
		this.outgoingPort = outgoingPort;
	}

	//@Column(name = "user_name", length = 300)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	//@Column(name = "password", length = 300)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	//@Column(name = "secure_password", length = 5)
	public String getSecurePassword() {
		return this.securePassword;
	}

	public void setSecurePassword(String securePassword) {
		this.securePassword = securePassword;
	}

	//@Column(name = "smtp_auth", length = 5)
	public String getSmtpAuth() {
		return this.smtpAuth;
	}

	public void setSmtpAuth(String smtpAuth) {
		this.smtpAuth = smtpAuth;
	}

	public boolean saveSetting(EmailSettings emailSettings) {
		String deleteSQL = "delete from email_settings";

		String sql = "insert into email_settings(" + "id," + "display_name ,"
				+ "email_address ," + "account_type ," + "incoming_mail,"
				+ "outgoing_mail," + "incoming_port," + "outgoing_port,"
				+ "user_name," + "password," + "secure_password," + "smtp_auth"
				+ ")" + "values( '"
				+ emailSettings.getId()
				+ "','"
				+ emailSettings.getDisplayName()
				+ "','"
				+ emailSettings.getEmailAddress()
				+ "','"
				+ emailSettings.getAccountType()
				+ "','"
				+ emailSettings.getIncomingMail()
				+ "','"
				+ emailSettings.getOutgoingMail()
				+ "','"
				+ emailSettings.getIncomingPort()
				+ "','"
				+ emailSettings.getOutgoingPort()
				+ "','"
				+ emailSettings.getUserName()
				+ "','"
				+ emailSettings.getPassword()
				+ "','"
				+ emailSettings.getSecurePassword()
				+ "','"
				+ emailSettings.getSmtpAuth() + "')";

		if (NetworkMode.c == null) {

			NetworkMode.getConn();
		}
		try {
			NetworkMode.c.createStatement().executeUpdate(deleteSQL);

			NetworkMode.c.createStatement().executeUpdate(sql);

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}

	public EmailSettings getSetting() {

		EmailSettings emailSettings = new EmailSettings();

		String query = "select * from email_settings";

		if (NetworkMode.c == null) {

			NetworkMode.getConn();
		}
		try {
			ResultSet rs = NetworkMode.c.createStatement().executeQuery(query);

			while (rs.next()) {

				emailSettings.setAccountType(rs.getString("account_type"));
				emailSettings.setDisplayName(rs.getString("display_name"));
				emailSettings.setEmailAddress(rs.getString("email_address"));
				emailSettings.setId(rs.getString("id"));
				emailSettings.setIncomingMail(rs.getString("incoming_mail"));
				emailSettings.setIncomingPort(rs.getString("incoming_port"));
				emailSettings.setOutgoingMail(rs.getString("outgoing_mail"));
				emailSettings.setOutgoingPort(rs.getString("outgoing_port"));
				emailSettings.setPassword(rs.getString("password"));
				emailSettings
						.setSecurePassword(rs.getString("secure_password"));
				emailSettings.setSmtpAuth(rs.getString("smtp_auth"));
				emailSettings.setUserName(rs.getString("user_name"));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return emailSettings;
	}

}