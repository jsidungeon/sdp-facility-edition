package org.elmis.facility.embedded.domain.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.database.connection.MyBatisSqliteConnectionFactory;

public class RoleDao {
	
	private SqlSessionFactory sqliteSqlSessionFactory;
	private SqlSessionFactory psqlSessionFactory; 
	{
		sqliteSqlSessionFactory = MyBatisSqliteConnectionFactory.getSqlSessionFactory();
		psqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public void getRoleByUser(int userId)
	{
		SqlSession sqliteSession = sqliteSqlSessionFactory.openSession(false);
		try{
			sqliteSession.selectList("Role.roleByUser", userId);
		}
		finally{
			sqliteSession.close();
		}
	}

}
