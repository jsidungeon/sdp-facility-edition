package org.elmis.facility.main.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JWindow;
/**
 * 
 * @author Michael Mwebaze
 *
 */
public class ElmisSplashWindow extends JWindow{

	public ElmisSplashWindow(String filename, Frame f)
	{
		super(f);
		JLabel l = new JLabel(new ImageIcon(filename));
		getContentPane().add(l, BorderLayout.CENTER);
		pack();
		Dimension screenSize =
				Toolkit.getDefaultToolkit().getScreenSize();
		Dimension labelSize = l.getPreferredSize();
		setLocation(screenSize.width/2 - (labelSize.width/2),
				screenSize.height/2 - (labelSize.height/2));

		addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				setVisible(false);
				dispose();
			}
		});
		
		setVisible(true);
	}
	public static void main(String[] args) {
		System.out.println("Starting...");
		ElmisSplashWindow w = new ElmisSplashWindow("splash.png", new Frame());
		w.setVisible(true);
		System.out.println("Ending....");
	}
}
