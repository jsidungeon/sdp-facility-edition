package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;

public class HivTestProductDao {
	private Map<String, String> map = new HashMap<>();
	
	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public Map<String, String> getHivTestProducts(){
		SqlSession session = sqlSessionFactory.openSession();
		try{
			List<Map<String, Object>> testProducts = session.selectList("HivTestProduct.getTestProducts");
			for (Map<String, Object> m : testProducts){
				String typeOfTest = m.get("type_of_test").toString().trim();
				String productCode = m.get("product_code").toString().trim();
				map.put(typeOfTest, productCode);
				map.put(typeOfTest+"Test", m.get("dar_display_name").toString());
			}
			return map;//session.selectList("HivTestProduct.getTestProducts");
		}
		finally{
			session.close();
		}
	}
}
