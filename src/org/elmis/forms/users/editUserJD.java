/*
 * AddUserJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.users;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.UsersDAO;
import org.elmis.facility.domain.model.Role_assignments;
import org.elmis.facility.domain.model.Role_rights;
import org.elmis.facility.domain.model.User_sites;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.email.MailTrigger;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.tools.PopulateJCBox;
import org.elmis.facility.utils.SdpStyleSheet;

/**
 * 
 * @author __USER__
 */
public class editUserJD extends javax.swing.JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private users user;
	private static UsersDAO userDAO;
	MailTrigger userAdd = new MailTrigger();
	private String roleBeforeEdit;

	/** Creates new form AddUserJD */
	public editUserJD(java.awt.Frame parent, boolean modal, users user) {
		super(parent, modal);

		this.user = user;
		this.roleBeforeEdit = user.getJobtitle();

		initComponents();
		SdpStyleSheet.configJDialogBackground(this);
		firstNameJTF.setText(user.getFirstname());
		lastNameJTF.setText(user.getLastname());

		if (user.getJobtitle() != null) {
			jobRoleJC.setSelectedItem(user.getJobtitle().trim());

		}
		cellPhoneJTF.setText(user.getCellphone());
		userNameJTF.setText(user.getUsername());
		emailAddressJTF.setText(user.getEmail());

		this.setLocationRelativeTo(null);
		this.setSize(400, 455);
		this.setVisible(true);
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel2 = new javax.swing.JLabel();
		firstNameJTF = new javax.swing.JTextField();
		jLabel3 = new javax.swing.JLabel();
		lastNameJTF = new javax.swing.JTextField();
		jobRoleJC = new javax.swing.JComboBox();
		jSeparator1 = new javax.swing.JSeparator();
		jLabel4 = new javax.swing.JLabel();
		jLabel5 = new javax.swing.JLabel();
		jLabel6 = new javax.swing.JLabel();
		jLabel9 = new javax.swing.JLabel();
		userNameJTF = new javax.swing.JTextField();
		jLabel10 = new javax.swing.JLabel();
		emailAddressJTF = new javax.swing.JTextField();
		jLabel1 = new javax.swing.JLabel();
		cellPhoneJTF = new javax.swing.JTextField();
		CancelJBnt = new javax.swing.JButton();
		updateUserJbtn = new javax.swing.JButton();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Edit user details");
		setForeground(new java.awt.Color(212, 208, 200));

		//jPanel1.setBackground(new java.awt.Color(1, 129, 130));
		SdpStyleSheet.configJPanelBackground(jPanel1);
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jLabel2.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel2.setForeground(new java.awt.Color(255, 255, 255));
		jLabel2.setText("First Name");

		jLabel3.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel3.setForeground(new java.awt.Color(255, 255, 255));
		jLabel3.setText("Last Name");

		jobRoleJC.setModel(new PopulateJCBox().FillLocationJCBox());

		jLabel4.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel4.setForeground(new java.awt.Color(255, 255, 255));
		jLabel4.setText("Dispensing Site");

		jLabel5.setFont(new java.awt.Font("Gulim", 1, 12));
		jLabel5.setForeground(new java.awt.Color(255, 255, 255));
		jLabel5.setText("Login Details");

		jLabel6.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel6.setForeground(new java.awt.Color(255, 255, 255));
		jLabel6.setText("User Name");

		jLabel10.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel10.setForeground(new java.awt.Color(255, 255, 255));
		jLabel10.setText("Email Address");

		jLabel1.setFont(new java.awt.Font("Ebrima", 1, 12));
		jLabel1.setForeground(new java.awt.Color(255, 255, 255));
		jLabel1.setText("Cell Phone:");

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																jLabel2,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																86,
																Short.MAX_VALUE)
														.addComponent(jLabel3)
														.addComponent(jLabel4)
														.addComponent(jLabel10)
														.addComponent(jLabel1)
														.addComponent(jLabel5)
														.addComponent(jLabel6))
										.addGap(14, 14, 14)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																cellPhoneJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																232,
																Short.MAX_VALUE)
														.addComponent(
																emailAddressJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																224,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																jobRoleJC,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																204,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																lastNameJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																232,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																firstNameJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																232,
																Short.MAX_VALUE)
														.addComponent(
																userNameJTF,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																232,
																Short.MAX_VALUE))
										.addContainerGap()).addGroup(
								jPanel1Layout.createSequentialGroup().addGap(
										10, 10, 10).addComponent(jLabel9,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										308,
										javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(37, 37, 37)).addGroup(
								jPanel1Layout.createSequentialGroup().addGap(
										10, 10, 10).addComponent(jSeparator1,
										javax.swing.GroupLayout.PREFERRED_SIZE,
										321,
										javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(24, Short.MAX_VALUE)));

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { jLabel1, jLabel10, jLabel2, jLabel3,
						jLabel4, jLabel5, jLabel6 });

		jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { cellPhoneJTF, emailAddressJTF,
						firstNameJTF, jobRoleJC, lastNameJTF, userNameJTF });

		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addGap(48, 48, 48)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel2)
														.addComponent(
																firstNameJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel3)
														.addComponent(
																lastNameJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel4)
														.addComponent(
																jobRoleJC,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel10)
														.addComponent(
																emailAddressJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(jLabel1)
														.addComponent(
																cellPhoneJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(40, 40, 40)
										.addComponent(
												jSeparator1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												13,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel5)
																		.addGap(
																				12,
																				12,
																				12)
																		.addComponent(
																				jLabel6))
														.addComponent(
																userNameJTF,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addGap(57, 57, 57)
										.addComponent(jLabel9)
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));

		jPanel1Layout
				.linkSize(javax.swing.SwingConstants.VERTICAL,
						new java.awt.Component[] { jLabel10, jLabel2, jLabel3,
								jLabel4 });

		CancelJBnt.setFont(new java.awt.Font("Ebrima", 1, 12));
		CancelJBnt.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/Cancel.png"))); // NOI18N
		CancelJBnt.setText("Cancel");
		CancelJBnt.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				CancelJBntActionPerformed(evt);
			}
		});

		updateUserJbtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		updateUserJbtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Reset password.png"))); // NOI18N
		updateUserJbtn.setText("Update User Details");
		updateUserJbtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				updateUserJbtnActionPerformed(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addGap(
																				10,
																				10,
																				10)
																		.addComponent(
																				updateUserJbtn)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				CancelJBnt,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				94,
																				javax.swing.GroupLayout.PREFERRED_SIZE))
														.addComponent(
																jPanel1,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(
												javax.swing.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)));
		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jPanel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												356,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																updateUserJbtn,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																25,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																CancelJBnt,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																25,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(21, Short.MAX_VALUE)));

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void updateUserJbtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		// update the user details

		this.user.setFirstname(firstNameJTF.getText());
		this.user.setLastname(lastNameJTF.getText());
		this.user.setUsername(userNameJTF.getText());
		this.user.setEmail(emailAddressJTF.getText());
		this.user.setCellphone(cellPhoneJTF.getText());
		this.user.setJobtitle(this.jobRoleJC.getSelectedItem().toString());

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();
		SqlSession session1 = factory.openSession();
		SqlSession session2 = factory.openSession();

		try {

			session.update("updateByUserPrimaryKeySelective", this.user);
			session.commit();

			javax.swing.JOptionPane.showMessageDialog(this,
					" User Details updated ");

			//refresh table 
			List<users> usersList = session.selectList("getAllUsers");

			UsersAdminTabJD.populateUserAdminTable(usersList);

			
			//delete user role assignment
			session2.delete("deleteRole_assignmentsSelectiveByUserid",
					this.user.getId());
			session2.commit();
			
			//if a different role selected 
			//update the role_assignment table 
			//get the id of the selected role
			//int selectedRole = jobRoleJC.getSelectedIndex();
			//int selectedRoleId = new PopulateJCBox().roleIdList[selectedRole];
			UsersDAO UsersDAO = new UsersDAO();
			users selectedSite = UsersDAO.getBySite(jobRoleJC.getSelectedItem().toString());
			
			List<Role_rights> roleRightList = session.selectList(
					"selectRightBySiteId", selectedSite.getId());
			Integer SiteID = selectedSite.getId();

			//create role_assignment object
			//Role_assignments roleAssignment = new Role_assignments();
			//roleAssignment.setRoleid(selectedRoleId);
			//roleAssignment.setUserid(this.user.getId());
			
			User_sites userSites=new User_sites();
			userSites.setSite_id(SiteID);
			userSites.setUser_id(user.getId());
			
			
			//Delete Previous Site assignments TO BE IMPLEMENTED
			//session1.delete("deleteSelectiveUserSites",user.getId());
			//session1.commit();
			
			//persist site assignment to database
			session1.insert("insertSelectiveUserSites",
					userSites);

			session1.commit();
			

		} finally {
			session.close();

			this.dispose();

		}

	}

	private void CancelJBntActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:

		int ok = javax.swing.JOptionPane.showConfirmDialog(this,
				"Are you sure you want to cancel", "", 2, 1);
		// int ok =
		// publicMethods.confirmDialog("Are you sure you want to cancel");
		if (ok == 0) {
			this.dispose();
		}
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				editUserJD dialog = new editUserJD(new javax.swing.JFrame(),
						true, new users());
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					@Override
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton CancelJBnt;
	private javax.swing.JTextField cellPhoneJTF;
	private javax.swing.JTextField emailAddressJTF;
	private javax.swing.JTextField firstNameJTF;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JSeparator jSeparator1;
	private javax.swing.JComboBox jobRoleJC;
	private javax.swing.JTextField lastNameJTF;
	private javax.swing.JButton updateUserJbtn;
	private javax.swing.JTextField userNameJTF;
	// End of variables declaration//GEN-END:variables

}