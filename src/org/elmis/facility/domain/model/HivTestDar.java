/**
 * this object is passed to the jasperreports to render that HIV Test Dar
 * 
 */
package org.elmis.facility.domain.model;

/**
 * @author jbanda
 *
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HivTestDar {

	private String id;

	private Integer facility_id;

	private Integer testing_point_id;

	private Integer client_id;

	private String purpose;

	private String screening_result;
	
	private String confirmatory_result;
	
	private String tiebreaker_result;	

	private Date date_of_test;

	private String positive_final_test_results;
	
	private String negative_final_test_results;
	
	private String indeterminate_final_test_results;	

	private String typeoftest;

	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public Integer getFacility_id() {
		return facility_id;
	}



	public void setFacility_id(Integer facilityId) {
		facility_id = facilityId;
	}



	public Integer getTesting_point_id() {
		return testing_point_id;
	}



	public void setTesting_point_id(Integer testingPointId) {
		testing_point_id = testingPointId;
	}



	public Integer getClient_id() {
		return client_id;
	}



	public void setClient_id(Integer clientId) {
		client_id = clientId;
	}



	public String getPurpose() {
		return purpose;
	}



	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}



	public String getScreening_result() {
		return screening_result;
	}



	public void setScreening_result(String screeningResult) {
		screening_result = screeningResult;
	}



	public String getConfirmatory_result() {
		return confirmatory_result;
	}



	public void setConfirmatory_result(String confirmatoryResult) {
		confirmatory_result = confirmatoryResult;
	}



	public String getTiebreaker_result() {
		return tiebreaker_result;
	}



	public void setTiebreaker_result(String tiebreakerResult) {
		tiebreaker_result = tiebreakerResult;
	}



	public Date getDate_of_test() {
		return date_of_test;
	}



	public void setDate_of_test(Date dateOfTest) {
		date_of_test = dateOfTest;
	}



	public String getPositive_final_test_results() {
		return positive_final_test_results;
	}



	public void setPositive_final_test_results(String positiveFinalTestResults) {
		positive_final_test_results = positiveFinalTestResults;
	}



	public String getNegative_final_test_results() {
		return negative_final_test_results;
	}



	public void setNegative_final_test_results(String negativeFinalTestResults) {
		negative_final_test_results = negativeFinalTestResults;
	}



	public String getIndeterminate_final_test_results() {
		return indeterminate_final_test_results;
	}



	public void setIndeterminate_final_test_results(
			String indeterminateFinalTestResults) {
		indeterminate_final_test_results = indeterminateFinalTestResults;
	}



	public String getTypeoftest() {
		return typeoftest;
	}



	public void setTypeoftest(String typeoftest) {
		this.typeoftest = typeoftest;
	}



	public static List createBeanCollection() {
		List list = new ArrayList();

		HivTestDar dar = new HivTestDar();

		dar.setClient_id(2);
		dar.setConfirmatory_result("NR");
		

		list.add(dar);

		return list;
	}
}
