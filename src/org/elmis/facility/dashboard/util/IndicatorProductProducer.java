package org.elmis.facility.dashboard.util;

import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.elmis.facility.domain.dao.IndicatorProductDao;
import org.elmis.facility.reporting.model.IndicatorProduct;

public class IndicatorProductProducer extends TimerTask{

	private BlockingQueue<List<IndicatorProduct>> list = new ArrayBlockingQueue<>(1);
	private IndicatorProductDao indicatorProductDao = new IndicatorProductDao();
	private List<IndicatorProduct> products = null;
	
	public IndicatorProductProducer()
	{
		products = indicatorProductDao.getIndicatorProducts();
		try {
			list.put(products);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void run() {
		//int i = 0;

		try {
			//while(true)
			//{		
			//System.out.println("Going to check in the database "+i);
			//i++;
				products = indicatorProductDao.getIndicatorProducts();
				list.take();
				list.put(products);
				//Thread.sleep(20000);
			//}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public BlockingQueue<List<IndicatorProduct>> getBlockingQueue()
	{
		return list;
	}
}
