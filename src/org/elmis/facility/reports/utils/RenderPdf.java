package org.elmis.facility.reports.utils;

import java.awt.Desktop;
import java.io.File;

public class RenderPdf {

	public static void renderPdf(String pdfName)
	{
		try {

			File pdfFile = new File(pdfName);
			if (pdfFile.exists()) {

				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(pdfFile);
				} else {
					System.out.println("Awt Desktop is not supported!");
				}

			} else {
				System.out.println("File is not exists!");
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
