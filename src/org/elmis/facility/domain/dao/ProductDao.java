package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.database.connection.MyBatisHSQLConnectionFactory;
import org.elmis.facility.reporting.model.Product;
import org.elmis.facility.reporting.model.ReportLineItem;
/**
 * 
 * @author Michael MWebaze
 *
 */
public class ProductDao {
	private SqlSessionFactory postgresqlSessionFactory;
	private SqlSessionFactory hsqlSessionFactory;
	{
		postgresqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
		hsqlSessionFactory = MyBatisHSQLConnectionFactory.getSqlSessionFactory();
	}
	/**
	 * Retrieves facility approved products
	 * @param programArea e.g HIV, EM, ARV or LAB
	 * @param facilityTypeCode Facility unique code
	 * @return
	 */
	public List<Product> getApprovedProducts(String programArea, String facilityTypeCode){
		SqlSession session = postgresqlSessionFactory.openSession();
		Map<String, String> map = new HashMap<>();
		map.put("programArea", programArea);
		map.put("facilityTypeCode", facilityTypeCode);
		try
		{
			return session.selectList("Product.approved_products", map);
		}
		finally
		{
			session.close();
		}
	}
	/*
	 * Gets products from postgres database
	 */
	public List<Product> getPostgresProducts()
	{
		SqlSession session = postgresqlSessionFactory.openSession();
		try
		{
			return session.selectList("Product.get_Products_from_Postgres");
		}
		finally
		{
			session.close();
		}
	}
	/**
	 * Gets products from embedded HSQL database for a specific program area
	 * @param programId
	 * @return
	 */
	public List<Product> getHsqlProducts(@Param("programId")int programId)
	{
		Map<String, Integer> map = new HashMap<>();
		map.put("programId", programId);
		SqlSession session = hsqlSessionFactory.openSession();

		try
		{
			return session.selectList("Product.get_Products_from_hsql", map);
		}
		finally
		{
			session.close();
		}
	}
	public void insertProductsToHsql(List<Product> products)
	{
		SqlSession session = hsqlSessionFactory.openSession(false);

		try
		{
			for (Product p : products)
			{
				session.insert("Product.insert_products_to_hsql", p);
				session.commit();
			}
			

		}
		finally
		{
			session.close();
		}

	}
	public List<Product> getIndicatorProducts()
	{
		SqlSession session = postgresqlSessionFactory.openSession();
		try
		{
			return session.selectList("Product.get_indicator_products_postgres");
		}
		finally
		{
			session.close();
		}
	}
	public void insertProductToHsql(Product product)
	{
		SqlSession session = hsqlSessionFactory.openSession();

		try
		{
			session.insert("Product.insert_products_to_hsql", product);
			session.commit();
		}
		finally
		{
			session.close();
		}
	}
	public static void main(String...arg)
	{
		ProductDao dao = new ProductDao();
		List<Product> products = dao.getIndicatorProducts();
		System.out.println("Size "+products.size());
		dao.insertProductsToHsql(products);
	}
}
