package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.AmcCalculator;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.reports.utils.SysProp;

public class HivRequisitionDao {
	private SqlSessionFactory sqlSessionFactory;
	
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getHivDispensaryIssues(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String remarks, String normalOrder)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		
		if (remarks.trim().length() != 0)
			map.put("remark", remarks);
		
		if (normalOrder.equalsIgnoreCase("YES"))
		{
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonthEmergency());
			map.put("prevFrmDate",new CalendarUtil().getFirstDayOfPreviousMonth());
		}
		else
		{
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonth());
			map.put("prevFrmDate",new CalendarUtil().getFirstDayOfPreviousMonth());
		}
		try
		{
			deleteHivRandRData(toDate, session);
			
			List<ReportRequisition> issues = session.selectList("HivRequisition.issues", map);
			
			for (ReportRequisition r: issues)
			{
				map.put("productCode", r.getProductCode());
				Integer beginBal = session.selectOne("HivRequisition.beginBalance", map);
				if (beginBal == null){
					r.setBeginningBal(0);
					beginBal=0;}
				
				else{
					r.setBeginningBal(beginBal);
				}
				//Pick Balances from Dispensing Sites
				//Integer beginBalSites = session.selectOne("HivRequisition.beginBalanceHIVSites", map);
				//beginBalSites=beginBalSites+beginBal;
				r.setBeginningBal(beginBal);
				
				Integer adjustments = session.selectOne("HivRequisition.dar_adjustments", map);
				if (adjustments == null)
					r.setLossAdjustment(0);
				else
					r.setLossAdjustment(adjustments);
				
				Integer receipts = session.selectOne("HivRequisition.receipts", map);
				if (receipts == null)
					r.setTotalReceived(0);
				else
					r.setTotalReceived(receipts);
				
				Integer phyCount = session.selectOne("HivRequisition.store_phycount", map);
				//Integer sitesPhyCount = session.selectOne("HivRequisition.sites_physicalcount", map);

			/*	if (sitesPhyCount == null)
					sitesPhyCount = 0;*/
				if (phyCount == null)
					r.setPhyCount(0);
				else
					r.setPhyCount(phyCount);
				
				r.setEmergencyOrder(normalOrder);
				if (r.getReportPeriodFrm() == null)
					r.setReportPeriodFrm(fromDate);
				if (r.getReportPeriodTo() == null)
					r.setReportPeriodTo(toDate);
				int amc = new AmcCalculator().amcCalculator(r.getProductCode(), r.getQtyDispensed());

				/*if (amc == 0) //implies there are no r and r's entered previously
				{
					int firstAmc = r.getQtyDispensed(); 
					r.setAmc(firstAmc);
					r.setMaxQty( firstAmc * 3);
					r.setOrderQty(r.getMaxQty() - r.getPhyCount());
				}
				else
				{*/
					r.setAmc(amc);
					int maxQty = amc * 3;
					r.setMaxQty( maxQty);
					r.setOrderQty(maxQty - r.getPhyCount());
				//}
				if (r.getOrderQty() < 0)
					r.setOrderQty(0);
				r.setCurrentPrice(Math.round(r.getCurrentPrice() * r.getOrderQty()*100.0)/100.0);
				if (remarks.trim().length() != 0)
					r.setRemarks(remarks);
           /****
            * Add Satellite data here and get the 3 logistics data items from Satellite RnR line items table 
            * Aggregate with Parent RnR line items if data is not null.
            * Moses Kausa - 2014 August*/

				
				session.insert("HivRequisition.insertHivrandr", r);	
			}
			/*if (remarks.trim().length() != 0)
				session.insert("HivRequisition.insertRemark", map);*/
			session.commit();
			return issues;
		}
		finally
		{
			session.close();
		}
	}
	private void deleteHivRandRData(Date toDate, SqlSession session) {
		session.selectList("HivRequisition.delhivdata", toDate);
		session.delete("HivRequisition.deleteRemark", toDate);
		session.commit();
		
	}
}
