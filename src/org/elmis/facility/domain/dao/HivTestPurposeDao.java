package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.domain.model.HivTestPurpose;

public class HivTestPurposeDao {

	private SqlSessionFactory sqlSessionFactory;
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<HivTestPurpose> getHivTestPurposes()
	{
		SqlSession session = sqlSessionFactory.openSession();
		try
		{
			return session.selectList("HivTestPurpose.get_purposes");
		}
		finally
		{
			session.close();
		}
	}
}
