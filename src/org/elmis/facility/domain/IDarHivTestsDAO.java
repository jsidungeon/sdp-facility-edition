package org.elmis.facility.domain;

import java.util.List;

/**
 * Interface for DarHivTestsDAO.
 * 
 * @author MyEclipse Persistence Tools
 */

public interface IDarHivTestsDAO {
	/**
	 * Perform an initial save of a previously unsaved DarHivTests entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#persist(Object)
	 * EntityManager#persist} operation.
	 * 
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IDarHivTestsDAO.save(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 * 
	 * @param entity
	 *            DarHivTests entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(DarHivTests entity);

	/**
	 * Delete a persistent DarHivTests entity. This operation must be performed
	 * within the a database transaction context for the entity's data to be
	 * permanently deleted from the persistence store, i.e., database. This
	 * method uses the {@link javax.persistence.EntityManager#remove(Object)
	 * EntityManager#delete} operation.
	 * 
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * IDarHivTestsDAO.delete(entity);
	 * EntityManagerHelper.commit();
	 * entity = null;
	 * </pre>
	 * 
	 * @param entity
	 *            DarHivTests entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(DarHivTests entity);

	/**
	 * Persist a previously saved DarHivTests entity and return it or a copy of
	 * it to the sender. A copy of the DarHivTests entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity. This operation must be performed within the a database
	 * transaction context for the entity's data to be permanently saved to the
	 * persistence store, i.e., database. This method uses the
	 * {@link javax.persistence.EntityManager#merge(Object) EntityManager#merge}
	 * operation.
	 * 
	 * <pre>
	 * EntityManagerHelper.beginTransaction();
	 * entity = IDarHivTestsDAO.update(entity);
	 * EntityManagerHelper.commit();
	 * </pre>
	 * 
	 * @param entity
	 *            DarHivTests entity to update
	 * @return DarHivTests the persisted DarHivTests entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public DarHivTests update(DarHivTests entity);

	public DarHivTests findById(String id);

	/**
	 * Find all DarHivTests entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the DarHivTests property to query
	 * @param value
	 *            the property value to match
	 * @return List<DarHivTests> found by query
	 */
	public List<DarHivTests> findByProperty(String propertyName, Object value);

	public List<DarHivTests> findByFacilityId(Object facilityId);

	public List<DarHivTests> findByTestingPointId(Object testingPointId);

	public List<DarHivTests> findByClientId(Object clientId);

	public List<DarHivTests> findByPurpose(Object purpose);

	public List<DarHivTests> findByResult(Object result);

	public List<DarHivTests> findByScreening(Object screening);

	public List<DarHivTests> findByConfirmatory(Object confirmatory);

	public List<DarHivTests> findByTiebreaker(Object tiebreaker);

	public List<DarHivTests> findByFinalTestResults(Object finalTestResults);

	public List<DarHivTests> findByPositiveResult(Object positiveResult);

	public List<DarHivTests> findByIndeterminateResult(
			Object indeterminateResult);

	public List<DarHivTests> findByNegativeResult(Object negativeResult);

	/**
	 * Find all DarHivTests entities.
	 * 
	 * @return List<DarHivTests> all DarHivTests entities
	 */
	public List<DarHivTests> findAll();
}