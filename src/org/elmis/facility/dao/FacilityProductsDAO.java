package org.elmis.facility.dao;
import java.util.HashMap;
import java.util.List;

import javax.mail.Session;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.Elmis_Stock_Control_CardMapper;
import org.elmis.facility.domain.dao.Elmis_moses_Stock_Control_CardMapper;
import org.elmis.facility.domain.dao.SP_Program_Facility_ApprovedProductsMapper;
import org.elmis.facility.domain.dao.SP_SystemcalculatedproductsbalanceMapper;
import org.elmis.facility.domain.dao.artdispenseProductQtyMapper;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.dao.Store_Physical_CountMapper;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Role_rights;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.Store_Physical_Count;
import  org.elmis.facility.domain.model.Dispensary_Store_Physical_Count;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.main.gui.AppJFrame;
import org.elmis.facility.network.MyBatisConnectionFactory;
import java.util.Vector;

public class FacilityProductsDAO {
	
	private String stra ;
	private SqlSessionFactory sqlSessionFactory; 
		
	public FacilityProductsDAO(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
		
   	
/*	public List<VW_Program_Facility_ApprovedProducts> getFacilityApprovedProducts(){

		SqlSession session = sqlSessionFactory.openSession();
		
		//FacilitysccProductsMapper fpMapper = session.getMapper(FacilitysccProductsMapper.class);
		
		try {
			  
			List<VW_Program_Facility_ApprovedProducts> fplist = session.selectList("getcurrentFacilityApprovedProducts");
			return fplist;
		} finally {
			session.close();
		}
	}*/
	
	public List<Programs> dogetselectedprogram(int programid){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Programs> proglist = session.selectList("getselectedprogram",programid);
						
			return proglist;
		} finally {
			session.close();
		}
	}
		
	public List<Programs> selectAllPrograms(){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<Programs> proglist = session.selectList("getFacilityProgs");
			
			
			return proglist;
		} finally {
			session.close();
		}
	}
	
/********************************************************************************
 * get the product from products list 
 * 	
 */
	
	@SuppressWarnings("unchecked")
	
	public VW_Program_Facility_ApprovedProducts barcodeselectProduct(String  barcode){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			VW_Program_Facility_ApprovedProducts   barcodeProduct = session.selectOne("getByProductBarcode",barcode);
			
			
			return barcodeProduct;
		} finally {
			session.close();
		}
	}
	
	
	public Integer barcodeselectProductQty(String  code){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			Integer   ProductQty = session.selectOne("org.elmis.facility.domain.dao.SP_Program_Facility_ApprovedProductsMapper.selectProductQty",code);
			
			
			return ProductQty;
		} finally {
			session.close();
		}
	}
		
	
	public List<VW_Program_Facility_ApprovedProducts> quickfiltersearchselectProduct(String  barcode){

		SqlSession session = sqlSessionFactory.openSession();
		
		try {
			List<VW_Program_Facility_ApprovedProducts> barcodeProductList = session.selectList("selectProductBySearchTerm",barcode);
			
			
			return barcodeProductList;
		} finally {
			session.close();
		}
	}
	
	
/*************************************************
 * method returns the list of loses_adjustment types defined  in the system
 * Calls mybatis mapper method selectAllAdjustments
 * 	
 *************************************************/
	
	public List<Losses_Adjustments_Types> selectAllAdjustments(){

		SqlSession session = sqlSessionFactory.openSession();
		int Manager=0;
		try {
			List<Role_rights> roleRightList = session.selectList(
					"selectRightBySiteId", AppJFrame.getSiteId());
			for (Role_rights roleRights : roleRightList) {
				if(roleRights.getRightname().equals("MANAGE_USER")||roleRights.getRightname().equals("MANAGE_DATABASE")||roleRights.getRightname().equals("MANAGE_FACILITY"))
				{
					Manager=1;
					break;
				}else
				{
					Manager=0;
				}
				
				System.out.print(" " + roleRights.getRightname());
	
			}
			if(Manager==1){
				List<Losses_Adjustments_Types> Adjustmentlist = session.selectList("selectAllAdjustments");
				return Adjustmentlist;
			}else
			{
				List<Losses_Adjustments_Types> Adjustmentlist = session.selectList("selectSomeAdjustments");
				return Adjustmentlist;
			}
					
			
		} finally {
			session.close();
		}
	}
/***********************************************
 * Method return list of current product store room balances	
 ***********************************************/
	public  List<VW_Systemcalculatedproductsbalance>
	dogetcurrentSystemcalculatedproductbalances (String valueA,java.util.Date startdate,java.util.Date enddate) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		SP_SystemcalculatedproductsbalanceMapper mapper = session.getMapper(SP_SystemcalculatedproductsbalanceMapper.class);
     List<VW_Systemcalculatedproductsbalance> listbal = mapper.getsystemcalculatedProductbalance(valueA,startdate,enddate);  
     return listbal;
		}finally{
			session.close();
		}
     }
	
	public  List<VW_Systemcalculatedproductsbalance>
	dogetcurrentSystemcalculatedproductbalancesbyprogram (String valueA,String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		SP_SystemcalculatedproductsbalanceMapper mapper = session.getMapper(SP_SystemcalculatedproductsbalanceMapper.class);
     List<VW_Systemcalculatedproductsbalance> listbal = mapper.getsystemcalculatedProductbalancebyprogram(valueA,valueB);  
     return listbal;
		}finally{
			session.close();
		}
     }
	
	
	
	
	
	public  List<Elmis_Stock_Control_Card>
	docheckifPCisthelasttransactionoftheMonth(java.util.Date startdate,java.util.Date enddate,String valueA) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			Elmis_Stock_Control_CardMapper mapper = session.getMapper(Elmis_Stock_Control_CardMapper.class);
     List<Elmis_Stock_Control_Card> listbal = mapper.checkifPCisthelasttransactionoftheMonth(startdate,enddate,valueA);  
     return listbal;
		}finally{
			session.close();
		}
     }
	
	
	
	
	
	
	public  List<VW_Systemcalculatedproductsbalance>
	dogetcurrentSystemcalculatedproductbalanceshsqldb (String valueA,String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
		SP_SystemcalculatedproductsbalanceMapper mapper = session.getMapper(SP_SystemcalculatedproductsbalanceMapper.class);
     List<VW_Systemcalculatedproductsbalance> listbal = mapper.getsystemcalculatedProductbalancehsqldb(valueA,valueB);  
     return listbal;
		}finally{
			session.close();
		}
     }
	
	
/*****************************************************
 * Method returns list of facility Approved products
 * 	
 *****************************************************/
	
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetcurrentFacilityApprovedProducts (String valueA, String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getcurrentFacilityApprovedProducts(valueA,valueB);  
     return listFoo;
		}finally{
			session.close();
		}
     }
	
	//test Vector for tabel model *************************************************
	public  Vector<Vector>
	dogetcurrentFacilityApprovedProductsVector (String valueA, String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     Vector<Vector> listFoo = mapper.getcurrentFacilityApprovedProductsVector(valueA,valueB);  
     return listFoo;
		}finally{
			session.close();
		}
     }
	
	
	
	
	//*****************************************************************************
	
	
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetcurrentFacilityApprovedProductshsqldb (String valueA, String valueB) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getcurrentFacilityApprovedProductshsqldb(valueA,valueB);  
     return listFoo;
		}finally{
			session.close();
		}
     }
	
		
	
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetARVcurrentFacilityApprovedProducts (String valueA) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getARVcurrentFacilityApprovedProducts(valueA);  
     return listFoo;
		}finally{
			session.close();
		}
     }
	
	
	public  List<VW_Program_Facility_ApprovedProducts>
	dogetARVcurrentFacilityApprovedProductshsqldb (String valueA) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getARVcurrentFacilityApprovedProductshsqldb(valueA);  
     return listFoo;
		}finally{
			session.close();
		}
     }
	
	public  
	void doupdateHIV_test_balances (@SuppressWarnings("rawtypes") HashMap hivmap) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
				
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updateelmishivsitebalances(hivmap); 
        
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			
		}
         
       
     }	
	
	
	
	public  
	void doupdatedispensaryqty (String prodcode,Double double1,String loc,double d) {
		
		try{
			System.out.println(prodcode + "qty params" + double1 + "" + loc);
		SqlSession session = sqlSessionFactory.openSession();
		artdispenseProductQtyMapper mapper = session.getMapper(artdispenseProductQtyMapper.class);
        mapper.updatedispenaryproductqty(prodcode,double1,loc,d); 
        // mapper.updateproductqty(prodcode,1); 
         session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			
			
		}
         
       
     }	
	/*
	 * 
	 */
	public  
	void doupdatestockcontrolcardProductswithPC(Elmis_Stock_Control_Card myscc) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			
	
		Elmis_Stock_Control_CardMapper mapper = session.getMapper(Elmis_Stock_Control_CardMapper.class);
        mapper.updatestockcontrolcardproductbalancewithPC(myscc); 
        session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			session.close();
			
		}
         
       
     }	
	
	
	public  void  InsertshipmentProducts(Shipped_Line_Items shipment){
		
       SqlSession session = sqlSessionFactory.openSession();
	
		try {
			session.insert("org.elmis.facility.domain.dao.Shipped_Line_ItemsMapper.insert",shipment);
			session.commit();
			System.out.println("Check perhaps we succeeded!");
			
		} finally {
			session.close();
		}
		
	}

	
	
	public  
	void doupdateHIV_test_balancesset (@SuppressWarnings("rawtypes") HashMap hivmap) {
		SqlSession session = sqlSessionFactory.openSession();
       
		try {
			session.update("org.elmis.facility.domain.dao.artdispenseProductQtyMapper.updateelmishivsitebalanceset",hivmap);
			session.commit();
			System.out.println("Check perhaps we succeeded!");
			
		} finally {
			session.close();
		}
		
		
     }	
	
/***********************************************************
 * Method inserts new products received into database using 
 * mybatis mappers 
 * 	
 ***********************************************************/
	
	public  void  InsertstockcontrolcardProducts(Elmis_Stock_Control_Card stock){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Elmis_Stock_Control_CardMapper.insert",stock);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	/*****
	 * 
	 */
	
	public  void  AdjustmentInsertstockcontrolcardProducts(List<Elmis_Stock_Control_Card> sccList){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				
				for(Elmis_Stock_Control_Card  s :  sccList ){
				session.insert("org.elmis.facility.domain.dao.Elmis_Stock_Control_CardMapper.insert",s);
				session.commit();
				
				}
				System.out.println("Check perhaps we succeeded!");
			} finally {
				session.close();
			}
			
		}
	
	
	
	
		
	
	
	/***************************************************************************
	 * inserts the physical count object into database using mybatis mappers
	 * 
	 ***************************************************************************/
	
	public  void  Insertproductsphysicalcount(Store_Physical_Count pc){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Store_Physical_CountMapper.insert",pc);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	public  void  Insertdispensaryproductsphysicalcount(Dispensary_Store_Physical_Count pc){
		
	       SqlSession session = sqlSessionFactory.openSession();
		
			
			try {
				session.insert("org.elmis.facility.domain.dao.Dispensary_Store_Physical_CountMapper.insert",pc);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	
	public  void  InsertproductQuantity(ProductQty  Pqty){
		
	       SqlSession session = sqlSessionFactory.openSession();
			
			try {
				session.insert("org.elmis.facility.domain.dao.artdispenseProductQtyMapper.insert",Pqty);
				session.commit();
				System.out.println("Check perhaps we succeeded!");
				
			} finally {
				session.close();
			}
			
		}
	
	
	

	public  List<VW_Program_Facility_ApprovedProducts>
	docheckSCCPhysicalcounttransactiondate (String valueA) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
     SP_Program_Facility_ApprovedProductsMapper mapper = session.getMapper(SP_Program_Facility_ApprovedProductsMapper.class);
     List<VW_Program_Facility_ApprovedProducts> listFoo = mapper.getARVcurrentFacilityApprovedProducts(valueA);  
     return listFoo;
		}finally{
			session.close();
		}
     }
	
	
	
	public  
	void doupdatestorephysicalcountBoolean(String  pcode) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			
	
		Store_Physical_CountMapper mapper = session.getMapper(Store_Physical_CountMapper.class);
        mapper.updatestorephysicalcountBoolean(pcode); 
        session.commit();
		}catch(Exception e){
			e.getMessage();
		}finally{
			session.close();
			
		}
         
       
     }	
	
	
	
	
	
	

}
