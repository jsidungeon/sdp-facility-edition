/*
 * StartJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * 
 * @author __USER__
 */
public class RegimensJP extends javax.swing.JPanel implements ActionListener {

	/** Creates new form StartJP */
	public RegimensJP(List<String> userRightsList) {
		initComponents();

		

		for (String userRights : userRightsList) {

			//hiv Testing
			if (this.hivTestingJL.getName().equalsIgnoreCase(userRights.trim())) {
				hivTestingJL.setVisible(true);
			}
		}

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		hivTestingJL = new javax.swing.JLabel();
		dispenseJL = new javax.swing.JLabel();
		jLabel1 = new javax.swing.JLabel();
		jLabel2 = new javax.swing.JLabel();
		jLabel3 = new javax.swing.JLabel();

		setBackground(new java.awt.Color(255, 255, 255));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());

		hivTestingJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		hivTestingJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/system_setup_55.png"))); // NOI18N
		hivTestingJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("HIV Testing"));
		hivTestingJL
				.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
		hivTestingJL.setName("HIV_TESTING");

		dispenseJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		dispenseJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/income_reports_55.png"))); // NOI18N
		dispenseJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Dispensing"));

		jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/BusinessContact32.png"))); // NOI18N
		jLabel1.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Start Count"));
		jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

		jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/AddNewFile.png"))); // NOI18N
		jLabel2.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Adjustments"));

		jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/bModify.png"))); // NOI18N
		jLabel3.setBorder(javax.swing.BorderFactory
				.createTitledBorder("View DAR"));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout
				.setHorizontalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addComponent(
												jLabel1,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												79,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												hivTestingJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												102,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
										.addComponent(
												jLabel3,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												110,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addGap(10, 10, 10)
										.addComponent(
												jLabel2,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												100,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addComponent(
												dispenseJL,
												javax.swing.GroupLayout.PREFERRED_SIZE,
												96,
												javax.swing.GroupLayout.PREFERRED_SIZE)
										.addContainerGap(116, Short.MAX_VALUE)));

		layout.linkSize(javax.swing.SwingConstants.HORIZONTAL,
				new java.awt.Component[] { dispenseJL, hivTestingJL, jLabel1,
						jLabel2 });

		layout
				.setVerticalGroup(layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addComponent(
																				hivTestingJL,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																layout
																		.createSequentialGroup()
																		.addComponent(
																				jLabel1,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				81,
																				Short.MAX_VALUE)
																		.addContainerGap())
														.addGroup(
																layout
																		.createParallelGroup(
																				javax.swing.GroupLayout.Alignment.TRAILING,
																				false)
																		.addComponent(
																				jLabel3,
																				javax.swing.GroupLayout.Alignment.LEADING,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				jLabel2,
																				javax.swing.GroupLayout.Alignment.LEADING,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				81,
																				Short.MAX_VALUE)
																		.addComponent(
																				dispenseJL,
																				0,
																				0,
																				Short.MAX_VALUE)))));

		layout.linkSize(javax.swing.SwingConstants.VERTICAL,
				new java.awt.Component[] { dispenseJL, hivTestingJL, jLabel1,
						jLabel2 });

	}// </editor-fold>
	//GEN-END:initComponents

	/********* Action Listener Coding Starts *************/
	// this action Listener for phone book
	//
	public void actionPerformed(ActionEvent evt) {

		// if (evt.getActionCommand().equalsIgnoreCase("a")) {

		// 888888888888888 use this for getting what letter was clicked and
		// getting the letter for the query
		// javax.swing.JOptionPane
		// .showMessageDialog(
		// null,
		// "Sorry this feature is not available for LegalManager Basic Edition \nUpgrade to activate this feature");

		// javax.swing.JOptionPane.showMessageDialog(null,
		// evt.getActionCommand()
		// .toString());
		// }
		AppJFrame.glassPane.activate(null);
		//new ContactAdminJD(javax.swing.JOptionPane.getFrameForComponent(this),
		//		true, evt.getActionCommand().toString());
		AppJFrame.glassPane.deactivate();

		// if (evt.getActionCommand().equalsIgnoreCase("b")) {
		// javax.swing.JOptionPane.showMessageDialog(null, "b");
		// }
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel dispenseJL;
	private javax.swing.JLabel hivTestingJL;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel3;
	// End of variables declaration//GEN-END:variables

}