package org.elmis.forms.regimen;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.ElmisRegimenCategory;
import org.elmis.facility.domain.model.ElmisRegimenProduct;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.generic.GenericDao;
import org.elmis.forms.regimen.dao.RegimenDao;
import org.elmis.forms.regimen.util.ComboBoxItem;
import org.elmis.forms.regimen.util.GenericUtil;

import org.elmis.forms.regimen.util.TableButton;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.ImageIcon;

public class DefineRegimenII extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel pnlMain = new JPanel();
	private JTable tblProducts = new JTable();
	private JTextField txtRegimenName;
	private JComboBox<ComboBoxItem> cboRegimenCategories;
	private final JCheckBox chckbxActive;
	private JScrollPane scrollPane;
	private JTable table;
	private JTable tblRegimens = new JTable();
	private JScrollPane scrollPane_1;
	private List<ElmisRegimen> regList = new ArrayList<ElmisRegimen>();
	private List<ElmisRegimenCategory> regCategories = new ArrayList<ElmisRegimenCategory>();
	private List<Map<String, Object>> prodList = new ArrayList<Map<String,Object>>();
	private final DefineRegimenII _this = this;
	private ElmisRegimen selectedRegimen = null;
	private boolean regimenDataChage;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			RegimenDao.setSystemProperties();
			DefineRegimenII dialog = new DefineRegimenII(null, true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			//dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void updateRegimenProductList(){
		int i = 0;
		for (Map<String, Object> prod : prodList) {
			//System.out.println(prod.get("code")+ " - " + tblProducts.getValueAt(i, 1));
			//System.out.println(prod.get("selected")+ " - " + tblProducts.getValueAt(i, 0));
			if((boolean)prod.get("selected") == true && Boolean.valueOf(tblProducts.getValueAt(i, 0).toString()) == false){
				/*System.out.println("delete" + prod.get("code"));
				System.out.println((boolean)prod.get("selected") + " - " +Boolean.valueOf(tblProducts.getValueAt(i, 0).toString()));*/
				
				RegimenDao.getInstance().deleteRegimenProductDosages((Integer)prod.get("productid"), _this.selectedRegimen.getId());
				RegimenDao.getInstance().deleteRegimenProduct((Integer) prod.get("productid"), _this.selectedRegimen.getId());
				
			} else if ((boolean)prod.get("selected") == false && Boolean.valueOf(tblProducts.getValueAt(i, 0).toString()) == true){
				/*System.out.println("insert" + prod.get("code"));
				System.out.println((boolean)prod.get("selected") + " - " +Boolean.valueOf(tblProducts.getValueAt(i, 0).toString()));*/
				ElmisRegimenProduct regProduct = new ElmisRegimenProduct();
				regProduct.setRegimenId(_this.selectedRegimen.getId());
				regProduct.setProductId(Integer.valueOf(tblProducts.getValueAt(i, 3).toString()));
				RegimenDao.getInstance().insertRegimenProduct(regProduct);
				
			}
			i++;
		}
		syncSelectionStatus();
		
	}
	private void updateRegimen(){
		_this.regimenDataChage = true;
		_this.selectedRegimen.setActive(chckbxActive.isSelected());
		_this.selectedRegimen.setName(txtRegimenName.getText().trim());
		_this.selectedRegimen.setCode("");
		_this.selectedRegimen.setCategoryId(((ElmisRegimenCategory)(((ComboBoxItem)cboRegimenCategories.getSelectedItem()).getValue())).getId());
		RegimenDao.getInstance().updateRegimen(_this.selectedRegimen);
		//populateProductsTable();
	}
	private void saveNewRegimen(){
		ElmisRegimen regimen = new ElmisRegimen();
		regimen.setActive(chckbxActive.isSelected());
		regimen.setName(txtRegimenName.getText().trim());
		regimen.setCode("");
		regimen.setCategoryId(((ElmisRegimenCategory)(((ComboBoxItem)cboRegimenCategories.getSelectedItem()).getValue())).getId());
		RegimenDao dao = RegimenDao.getInstance();
		System.out.println("id = " + regimen.getId());
		dao.insert(regimen);
		for (int i = 0; i < tblProducts.getRowCount(); i++) {
			if (Boolean
					.valueOf(tblProducts.getValueAt(i, 0).toString())){
				System.out.println(tblProducts.getValueAt(i, 0) + ","
						+ tblProducts.getValueAt(i, 1) + ","
						+ tblProducts.getValueAt(i, 2) +","+ tblProducts.getValueAt(i, 3));
				ElmisRegimenProduct regProduct = new ElmisRegimenProduct();
				regProduct.setRegimenId(regimen.getId());
				regProduct.setProductId(Integer.valueOf(tblProducts.getValueAt(i, 3).toString()));
				dao.insertRegimenProduct(regProduct);
			}
		}
		populateRegimensTable();
		populateProductsTable();
		resetRegimenForm();
	}
	
	private void resetRegimenForm(){
		this.txtRegimenName.setText("");
		this.chckbxActive.setSelected(false);
		this.cboRegimenCategories.setSelectedIndex(0);
		_this.selectedRegimen = null;
	}
	
	private Integer findRegimenCategoryInCombo(int categoryId){
		for (int i=0;i < regCategories.size(); i++) {
			if (categoryId == regCategories.get(i).getId()){
				return i;
			}
		}
		return null;
	}
	
	private void selectRegimen() {
		_this.selectedRegimen = regList.get(tblRegimens.getSelectedRow());
		this.txtRegimenName.setText(tblRegimens.getValueAt(tblRegimens.getSelectedRow(), 0).toString());
		int categoryId = regList.get(tblRegimens.getSelectedRow()).getCategoryId();
		this.cboRegimenCategories.setSelectedIndex(findRegimenCategoryInCombo(categoryId));
		this.chckbxActive.setSelected(regList.get(tblRegimens.getSelectedRow()).isActive());
		System.out.println(tblRegimens.getValueAt(tblRegimens.getSelectedRow(), 0).toString());
		setSelectionStatusForProducts();
		
	/*	TableColumnModel m=tblProducts.getColumnModel();
		TableColumn col=m.getColumn(3);
		m.removeColumn(col);*/
		
	}
	private void syncSelectionStatus(){
		DefaultTableModel model = (DefaultTableModel) tblProducts.getModel();
		int i=0;
		for (Map<String, Object> prod : prodList) {
			prod.put("selected", Boolean.valueOf(tblProducts.getValueAt(i, 0).toString()));
			i++;
		}
	}
	
	private void setSelectionStatusForProducts(){
		List<Map<String, Object>> regProduts = RegimenDao.getInstance().getRegimenProducts(regList.get(tblRegimens.getSelectedRow()).getId());
		DefaultTableModel model = (DefaultTableModel) tblProducts.getModel();
		int i=0;
		for (Map<String, Object> prod : prodList) {
			if(isProductInRegimen((Integer)prod.get("productid"),regProduts)){
			model.setValueAt(Boolean.TRUE, i, 0);
			prod.put("selected", Boolean.TRUE);
			} else {
				model.setValueAt(Boolean.FALSE, i, 0);
				prod.put("selected", Boolean.FALSE);
			}
			i++;
		}
		tblProducts.setModel(model);
	}
	private boolean isProductInRegimen(int productId, List<Map<String , Object>> regimenProducts){
		boolean isFound =false;
		for (Map<String, Object> regProd : regimenProducts) {
			if(productId == (Integer)regProd.get("productid")){
				isFound = true;
			}
		}
		return isFound;
		
	}
	@SuppressWarnings("unused")
	private void populateRegimensTable(){
		DefaultTableModel model = (DefaultTableModel) tblRegimens.getModel();
		clearTable(tblRegimens);
		
		regList = RegimenDao.getInstance().fetchElmisRegimens();
		for (ElmisRegimen elmisRegimen : regList) {
			model.addRow(new Object[] { elmisRegimen.getName(), elmisRegimen.getCategory().getName(), elmisRegimen.isActive()});
		}
		
		tblRegimens.setModel(model);

	}
	
	private void addRegimenTblRegimen(ElmisRegimen elmisRegimen){

		/*((DefaultTableModel) this.tblRegimens.getModel()).addRow(new Object[] { elmisRegimen.getName(), elmisRegimen.getCategory().getName(), elmisRegimen.isActive()});
		_this.selectedRegimen = elmisRegimen;*/
			
	}

	private void setUpProductsTable(){
		DefaultTableModel model = new DefaultTableModel(null, new String[] {
				"", "Code", "Primary Name" , "Product Id","Dosage"}) {
			/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int c) {
				switch (c) {
				case 0:
					return Boolean.class;
				default:
					return String.class;
				}
			}
		};
		
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(398, 56, 695, 549);
		pnlMain.add(scrollPane);
		tblProducts = new JTable();
		scrollPane.setViewportView(tblProducts);
		tblProducts.setModel(model);
		tblProducts.getColumnModel().getColumn(1).setPreferredWidth(158);
		tblProducts.getColumnModel().getColumn(2).setPreferredWidth(193);
		
		TableColumn dosageColumn = tblProducts.getColumnModel().getColumn(4);
		hideColumn(tblProducts.getColumnModel().getColumn(3));
		
        TableButton buttons = new TableButton();
        buttons.addHandler(new TableButton.TableButtonPressedHandler() {
			
			@Override
			public void onButtonPress(int row, int column) {
				// TODO Auto-generated method stub
				System.out.println(tblProducts.getSelectedRow());
				if(_this.selectedRegimen != null){
					System.out.println(_this.selectedRegimen.getId()+","+(Integer) _this.prodList.get(tblProducts.getSelectedRow()).get("productid"));
				    new DefineRegimenProductDosage(javax.swing.JOptionPane.getFrameForComponent(_this), true, _this.selectedRegimen.getId(), (Integer) _this.prodList.get(tblProducts.getSelectedRow()).get("productid"));
				} else{
					JOptionPane.showMessageDialog(_this, "Please select regimen.");
				}
				
			}
		});
        
        dosageColumn.setCellRenderer(buttons);
        dosageColumn.setCellEditor(buttons);
        dosageColumn.setPreferredWidth(10);
		
	}
	private void populateProductsTable() {
		clearTable(tblProducts);
		DefaultTableModel model = (DefaultTableModel) tblProducts.getModel();
		GenericDao dao = GenericDao.getInstance();
		String facilityCode = GenericUtil.getProperties().getProperty("facilityCode");
		Facility_Types t = dao.selectFacilityTypeByCode(facilityCode);
		String facilityTypeCode =  t.getCode();
		prodList  = RegimenDao.getInstance().fetchProductsForFacility("ARV",facilityTypeCode);
		for (Map<String, Object> map : prodList) {
			
			model.addRow(new Object[] { false, map.get("code"),
					map.get("primaryname") , map.get("productid"),"+"});
		}
	/*	TableColumnModel m=tblProducts.getColumnModel();
		TableColumn col=m.getColumn(3);
		m.removeColumn(col);*/
		tblProducts.setModel(model);

	}
	public void clearTable(JTable table){
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.setRowCount(0);
	}
	
	private void hideColumn(TableColumn column){
		column.setMinWidth(0);
		column.setMaxWidth(0);
		column.setPreferredWidth(0);
	}

	public void setUpCategoriesCombo() {
		RegimenDao dao = RegimenDao.getInstance();
		 regCategories  = dao.fetchRegimenCategories();
		 try{
		 cboRegimenCategories.removeAllItems();
		 }catch (Exception ex){
			 
		 }
		for (ElmisRegimenCategory elmisRegimenCategory : regCategories) {
			cboRegimenCategories
					.addItem(new ComboBoxItem(elmisRegimenCategory));
		}

	}

	
	

	private Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prop;
	}

	/**
	 * Create the dialog.
	 */
	public DefineRegimenII(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				
			}
		});
		setTitle("Define Regimen");
		setResizable(false);
		setBounds(100, 100, 1125, 673);
		getContentPane().setLayout(new BorderLayout());
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnlMain, BorderLayout.CENTER);
		pnlMain.setLayout(null);
		{
			populateProductsTable();
		}
		
		JSeparator separator = new JSeparator();
		separator.setBounds(438, 60, 1, 2);
		pnlMain.add(separator);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBounds(29, 56, 352, 247);
		pnlMain.add(panel);
		panel.setLayout(null);
		
				cboRegimenCategories = new JComboBox();
				cboRegimenCategories.setBounds(20, 101, 241, 20);
				setUpCategoriesCombo();
				setUpProductsTable();
				populateProductsTable();
				panel.add(cboRegimenCategories);
				{
					JLabel lblNewLabel = new JLabel("Regimen Line");
					lblNewLabel.setBounds(20, 84, 132, 14);
					panel.add(lblNewLabel);
				}
				
						chckbxActive = new JCheckBox("Active");
						chckbxActive.setBounds(20, 191, 97, 23);
						panel.add(chckbxActive);
						
								txtRegimenName = new JTextField();
								txtRegimenName.setBounds(20, 42, 241, 20);
								panel.add(txtRegimenName);
								txtRegimenName.setColumns(10);
								
										JLabel lblRegimenName = new JLabel("Regimen Name");
										lblRegimenName.setBounds(20, 21, 132, 14);
										panel.add(lblRegimenName);
										
										JButton btnNew = new JButton("Define Regimen line");
										btnNew.setBounds(20, 128, 241, 23);
										panel.add(btnNew);
										btnNew.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent arg0) {
												new DefineRegimenLine(_this,javax.swing.JOptionPane.getFrameForComponent(_this), true);
											}
										});
										
										table = new JTable();
										table.setBounds(404, 272, -365, 54);
										pnlMain.add(table);
										
										scrollPane_1 = new JScrollPane();
										scrollPane_1.setBounds(29, 314, 352, 291);
										pnlMain.add(scrollPane_1);
										
										tblRegimens = new JTable();
										tblRegimens.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
									        

											public void valueChanged(ListSelectionEvent event) {
									            // do some actions here, for example
									            // print first column value from selected row
									        	if(!_this.regimenDataChage){
									        		System.out.println(tblRegimens.getValueAt(tblRegimens.getSelectedRow(), 0).toString());
									        		selectRegimen();
									        	} 
									            //set checked products
									        	_this.regimenDataChage = false;
									        }
									    });
										scrollPane_1.setViewportView(tblRegimens);
										tblRegimens.setModel( new DefaultTableModel(null, new String[] {
												"Name", "Line", "Active" }) {
											private static final long serialVersionUID = 1L;
											@SuppressWarnings({ "rawtypes", "unchecked" })
											public Class getColumnClass(int c) {
												switch (c) {
												case 2:
													return Boolean.class;
												default:
													return String.class;
												}
											}
											boolean[] canEdit = new boolean[] { false, false, false};

											public boolean isCellEditable(int rowIndex, int columnIndex) {
												return canEdit[columnIndex];
											}
											
											
										});
										
										JPanel panel_1 = new JPanel();
										panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
										panel_1.setBounds(29, 11, 1064, 34);
										pnlMain.add(panel_1);
												panel_1.setLayout(null);
										
												JButton btnDefineRegimen = new JButton("");
												btnDefineRegimen.setIcon(new ImageIcon(DefineRegimenII.class.getResource("/elmis_images/Save icon.png")));
												btnDefineRegimen.setBounds(23, 7, 24, 23);
												panel_1.add(btnDefineRegimen);
												btnDefineRegimen.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent e) {
														
														if(_this.selectedRegimen == null){
															_this.saveNewRegimen();
														} else{
															_this.updateRegimen();
															updateRegimenProductList();
															populateRegimensTable();
														}
														JOptionPane.showMessageDialog(_this, "Changes saved successfully!");
													
													}
												});
										
										JButton btnNewRegimen = new JButton("");
										btnNewRegimen.setIcon(new ImageIcon(DefineRegimenII.class.getResource("/elmis_images/Add.png")));
										btnNewRegimen.setBounds(57, 7, 29, 23);
										panel_1.add(btnNewRegimen);
										
										JButton btnDelete = new JButton("");
										btnDelete.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent arg0) {
												_this.deleteRegimen();
											}
										});
										btnDelete.setIcon(new ImageIcon(DefineRegimenII.class.getResource("/elmis_images/eLMIS delete role small.png")));
										btnDelete.setBounds(97, 7, 33, 23);
										panel_1.add(btnDelete);
										btnNewRegimen.addActionListener(new ActionListener() {
											public void actionPerformed(ActionEvent arg0) {
												{
											    _this.selectedRegimen = null;
												_this.txtRegimenName.setText("");
												_this.cboRegimenCategories.setSelectedIndex(0);
												_this.chckbxActive.setSelected(true);
												populateProductsTable();
												}//clear
											}
										});
										populateRegimensTable();
										hideColumn(tblRegimens.getColumnModel().getColumn(1));
										hideColumn(tblRegimens.getColumnModel().getColumn(2));
						chckbxActive.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								System.out.println(chckbxActive.isSelected());
							}
						});
				cboRegimenCategories.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ComboBoxItem item = (ComboBoxItem) cboRegimenCategories
								.getSelectedItem();
						ElmisRegimenCategory category = (ElmisRegimenCategory) item
								.getValue();
						System.out.println(category.getId() + " , "
								+ category.getName());
					}
				});
				this.setVisible(true);
	}
	protected void deleteRegimen() {
		int confirm = JOptionPane.showOptionDialog(_this,  "Are you sure you want to delete regimen? All associated data with be deleted!" , "Confirm Delete Regimen ",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
				null, null, null);
		if(confirm == JOptionPane.YES_OPTION){
			_this.regimenDataChage = true;
			RegimenDao.getInstance().deleteRegimen(_this.selectedRegimen);
			populateRegimensTable();
			populateProductsTable();
			resetRegimenForm();
			JOptionPane.showMessageDialog(_this, "Regimen deleted!");
			
		}
		
	}
}
