package org.elmis.facility.email;

import java.util.List;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MailingListSender extends Thread {

	private EmailSettings settings;
	private String bodytext;
	private String subject;
	private List<String> mailingList;

	public MailingListSender(String bodytext, String subject,
			List<String> mailingList) {

		this.bodytext = bodytext;
		this.subject = subject;
		this.mailingList = mailingList;

	}

	@Override
	public void run() {

		// TODO Auto-generated constructor stub

		settings = new EmailSettings().getSetting();

		final String username = settings.getUserName();
		final String password = settings.getPassword();

		Properties props = new Properties();
		props.put("mail.smtp.auth", settings.getSmtpAuth());
		props.put("mail.smtp.starttls.enable", settings.getSecurePassword());
		props.put("mail.smtp.host", settings.getOutgoingMail());
		props.put("mail.smtp.port", settings.getOutgoingPort().toString());

		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {
					@Override
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(username, password);
					}
				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("from-"
					+ settings.getDisplayName()));
			// message.set

			InternetAddress[] toAddress = new InternetAddress[mailingList
					.size()];

			for (int i = 0; i < mailingList.size(); i++) { // changed from a
															// while loop

				toAddress[i] = new InternetAddress(mailingList.get(i));

				// message.setRecipients(Message.RecipientType.TO,InternetAddress.parse(mailingList.get(i)));

				// System.out.println("Sent to "+ mailingList.get(i));

			}

			for (int i = 0; i < toAddress.length; i++) { // changed from a while
															// loop
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			// message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("to-joe@oribicom.com"));

			message.setSubject(subject);
			message.setText(bodytext);

			Transport.send(message);

			// System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
			
		
		}

	}

}
