package org.elmis.facility.reporting.model;

public class Program {
	public int getProgramId() {
		return programId;
	}
	public void setProgramId(int programId) {
		this.programId = programId;
	}
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	@Override
	public String toString() {
		return "Program [programId=" + programId + ", programCode="
				+ programCode + ", programName=" + programName + "]";
	}
	public String getProgramName() {
		return programName;
	}
	public void setProgramName(String programName) {
		this.programName = programName;
	}
	private int programId;
	private String programCode;
	private String programName;
}
