package org.elmis.facility.reporting.model;

public class Reagent {
	private int id;
	private String commodityUnit;
	private boolean commodityStatus;
	private String commodityName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCommodityUnit() {
		return commodityUnit;
	}
	public void setCommodityUnit(String commodityUnit) {
		this.commodityUnit = commodityUnit;
	}
	public boolean isCommodityStatus() {
		return commodityStatus;
	}
	public void setCommodityStatus(boolean commodityStatus) {
		this.commodityStatus = commodityStatus;
	}
	public String getCommodityName() {
		return commodityName;
	}
	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}
	public String getCommodityCode() {
		return commodityCode;
	}
	public void setCommodityCode(String commodityCode) {
		this.commodityCode = commodityCode;
	}
	private String commodityCode;

	@Override
	public String toString() {
		return "Reagent [commodityStatus=" + commodityStatus
				+ ", commodityCode=" + commodityCode + "]";
	}
}
