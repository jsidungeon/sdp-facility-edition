/*
 * ReportAndRequisition.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.reporting;

import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ItemEvent;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import org.elmis.facility.connections.DatabaseSwitcher;
import org.elmis.facility.domain.dao.ProgramDao;
import org.elmis.facility.domain.dao.ReportRequisitionDao;
import org.elmis.facility.reporting.generator.ReportGenerator;
import org.elmis.facility.reporting.model.Program;
import org.elmis.facility.reports.utils.CalendarUtil;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.SdpStyleSheet;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * ReportAndRequisition.java
 * Purpose: Used for generation of Reports & Requisitions
 * @author Michael Mwebaze Kitobe
 * @version 1.0
 */
public class ReportAndRequisition extends javax.swing.JDialog {

	private String frmDate; //reporting period begin date
	private String toDate;  //reporting period end date
	private int programSelector;  //Each program has a predefined number e.g ARV is 0, EMLIP=1, HIV=2, LAB = 3
	//private StorePhysicalCountDao spcDao = new StorePhysicalCountDao();
	private JProgressBar progressBar = new JProgressBar(0, 100);
	private int fontSize = 16;
	private JLabel placeHolder = new JLabel();
	//private HsqlProgramDao hsqlProgramDao = new HsqlProgramDao();

	/** Creates new form ReportAndRequisition */
	public ReportAndRequisition(java.awt.Frame parent, boolean modal, String userId) {
		super(parent, modal);
		this.parent = parent;
		
		frmDate = calendarUtil.getFirstDayOfReportingMonth();
		toDate = calendarUtil.getLastDayOfReportingMonth();
		getContentPane().add(progressBar);
		progressBar.setBounds(260, 540, 280, 20);
		progressBar.setVisible(false);
		
		
		
		/*if (DatabaseSwitcher.switchToSqlite())//checks if connection to Embedded Database is available
		{
			List<Program> sqliteProgramsList = new org.elmis.facility.embedded.domain.dao.ProgramDao().getPrograms();
			for (Program p: sqliteProgramsList)
				programsList.add(p.getProgramName().trim());
		}
		else //if (postgresDatabaseAvailable)// gets the avail programs from postgres database which are eventually inserted into the Combobox for selection
		{*/
			if (DatabaseSwitcher.switchToPostgres())
			{
				Iterator<Program> programs = new ProgramDao().getPrograms().iterator();

				while (programs.hasNext())
					programsList.add(programs.next().getProgramName());
			}
			else //Displays error JOptionPane if either Embedded HSQL and Postgres are not available
			{
				JOptionPane.showMessageDialog(this,
						"<html>UNABLE POPULATE PROGRAMS FROM <br> <font color=red>EMBEDDED OR CENTRAL DATABASE NOT AVAILABLE.</font></html>",	"DATABASE CONNECTION ERRORS",JOptionPane.WARNING_MESSAGE);
			}
		//}
		
		reportTypeArray = programsList.toArray(new String[programsList.size()]);
		initComponents();
		SdpStyleSheet.configJDialogBackground(this);
		//getContentPane().setBackground(new java.awt.Color(102, 102, 102));
		remarkTextArea.setBackground(Color.white);
		remarkTextArea.setLineWrap(true);
		remarkTextArea.setWrapStyleWord(true);
		setLocationRelativeTo(parent);
		fromDateTextField.setText(frmDate);
		toDateTextField.setText(toDate);
		//}
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		fromDateTextField = new javax.swing.JTextField();
		toDateTextField = new javax.swing.JTextField();
		emergencyOrderCheckBox = new javax.swing.JCheckBox();
		toJLabel = new javax.swing.JLabel();
		fromJLabel = new javax.swing.JLabel();
		selectProgramAreajLabel = new javax.swing.JLabel();
		reportTypeComboBox = new javax.swing.JComboBox();
		cancelButton = new javax.swing.JButton();
		viewReportButton = new javax.swing.JButton();
		submitReportButton = new javax.swing.JButton();
		remarkJLabel = new javax.swing.JLabel();
		jScrollPane1 = new javax.swing.JScrollPane();
		jLabel5 = new javax.swing.JLabel();

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("View & save Report and Requisitions");

		fromDateTextField.setEditable(false);

		toDateTextField.setEditable(false);

		//emergencyOrderCheckBox.setFont(new java.awt.Font("Ebrima", 1, 12));
		//emergencyOrderCheckBox.setForeground(new java.awt.Color(255, 0, 0));
		emergencyOrderCheckBox.setText("<html><b><h3>Custom Period</h3><b></html>");
		emergencyOrderCheckBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				try {
					emergencyOrderCheckBoxItemStateChanged(evt);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		//toJLabel.setFont(new java.awt.Font("Ebrima", 1, fontSize));
		SdpStyleSheet.configOtherJLabel(toJLabel);
		toJLabel.setText("To:");

		//fromJLabel.setFont(new java.awt.Font("Ebrima", 1, fontSize));
		SdpStyleSheet.configOtherJLabel(fromJLabel);
		fromJLabel.setText("Reporting Period from:");

		//selectProgramAreajLabel.setFont(new java.awt.Font("Ebrima", 1, fontSize));
		SdpStyleSheet.configOtherJLabel(selectProgramAreajLabel);
		selectProgramAreajLabel.setText("Select report type:");

		reportTypeComboBox.setModel(new javax.swing.DefaultComboBoxModel(reportTypeArray));
		reportTypeComboBox.addItemListener(new java.awt.event.ItemListener() {
			public void itemStateChanged(java.awt.event.ItemEvent evt) {
				reportTypeComboBoxItemStateChanged(evt);
			}
		});

		cancelButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Cancel.png"))); // NOI18N
		cancelButton.setText("Cancel");
		cancelButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelButtonActionPerformed(evt);
			}
		});

		viewReportButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		viewReportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/View report.png"))); // NOI18N
		viewReportButton.setText("View report");
		viewReportButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				try {
					viewReportButtonActionPerformed(evt);
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
		});

		//submitReportButton.setFont(new java.awt.Font("Ebrima", 1, 12));
		//submitReportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Save icon.png"))); // NOI18N
		//submitReportButton.setText("Search submitted R and Rs");
		/*submitReportButton.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				submitReportButtonActionPerformed(evt);
			}
		});*/

		remarkJLabel.setFont(new java.awt.Font("Ebrima", 1, fontSize));
		SdpStyleSheet.configOtherJLabel(remarkJLabel);
		remarkJLabel.setText("Remarks:");

		jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/reports requisition.png")));
		remarkTextArea = new javax.swing.JTextArea();
		
				remarkTextArea.setColumns(20);
				remarkTextArea.setRows(5);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jLabel5, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(layout.createSequentialGroup()
									.addComponent(fromJLabel)
									.addGap(18)
									.addComponent(fromDateTextField, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
									.addGap(18)
									.addComponent(toJLabel)
									.addGap(11))
								.addGroup(layout.createSequentialGroup()
									.addGroup(layout.createParallelGroup(Alignment.TRAILING)
										.addComponent(remarkTextArea, GroupLayout.PREFERRED_SIZE, 305, GroupLayout.PREFERRED_SIZE)
										.addComponent(reportTypeComboBox, 0, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
							.addPreferredGap(ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
							.addComponent(toDateTextField, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE)
							.addGap(31)
							.addComponent(emergencyOrderCheckBox))
						.addGroup(layout.createSequentialGroup()
							.addGroup(layout.createParallelGroup(Alignment.LEADING)
								.addComponent(remarkJLabel)
								.addComponent(selectProgramAreajLabel))
							.addGap(0, 529, Short.MAX_VALUE))
						.addGroup(layout.createSequentialGroup()
							.addGap(0, 384, Short.MAX_VALUE)
							.addComponent(cancelButton)
							.addGap(18)
							.addComponent(viewReportButton)
							.addGap(18)
							.addComponent(placeHolder)))
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addGap(36)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(fromJLabel)
						.addComponent(toJLabel)
						.addComponent(toDateTextField, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
						.addComponent(fromDateTextField, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(emergencyOrderCheckBox, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
					.addGap(61)
					.addComponent(selectProgramAreajLabel)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(reportTypeComboBox, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
					.addGap(33)
					.addComponent(remarkJLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
						.addComponent(remarkTextArea, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
					.addGroup(layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancelButton, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addComponent(placeHolder, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE)
						.addComponent(viewReportButton, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
					.addGap(68))
				.addGroup(layout.createSequentialGroup()
					.addGap(28)
					.addComponent(jLabel5)
					.addContainerGap(37, Short.MAX_VALUE))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	//GEN-END:initComponents
	/**
	 * Handles R & R submission events
	 * @param evt
	 */
	/*private void submitReportButtonActionPerformed(
			java.awt.event.ActionEvent evt) {
		//new RandRSearchJd(null, true).setVisible(true);
	}*/
	/**
	 * Handles the Event that is created as a result of selecting the close button
	 * @param evt
	 */
	private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {
		dispose();
	}
	/**
	 * Handles events that a generated as a result of switching from one Program Area to another.
	 * Then sets the program number which determines which Program Area R & R is generated or 
	 * submitted.  
	 * @param evt
	 */
	private void reportTypeComboBoxItemStateChanged(java.awt.event.ItemEvent evt) {

		int index = reportTypeComboBox.getSelectedIndex();
		programSelector = index;
	}
	/**
	 * Handles event that is generated as a result of clicking the View Button. Once the View
	 * Button is clicked, an R & R report in PDF is created for the User to preview before
	 * they can submit it to the Central eLMIS. 
	 * @param evt
	 * @throws ParseException
	 */
	private void viewReportButtonActionPerformed(java.awt.event.ActionEvent evt)
			throws ParseException {
		final ReportRequisitionDao checkLineItemDao = new ReportRequisitionDao();
		final String remarks = remarkTextArea.getText().toUpperCase();
		if (isNonEmergency)//controls submission on non-emergency reports
		{
			if (true/*calendarUtil.compareWithDeadlineDate(false)*/ )//checks if deadline date has been exceeded 
			{
				final String normalOrder = "NO";

				int selected = reportTypeComboBox.getSelectedIndex(); //stores index when an item is selected
				if (selected == 0)//ARV SECTION
				{
					class ReportWorker extends SwingWorker<String, Object> {
						protected String doInBackground() {
							progressBar.setVisible(true);
							progressBar.setIndeterminate(true);
							CalendarUtil cal = new CalendarUtil();
							
							/*List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "ARV");
							
							if (spcList.size() > 0)
							{*/
							if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 3, normalOrder))
								new ReportGenerator().generateArvDrugsReport(fromDateTextField.getText(),
										toDateTextField.getText(), normalOrder, remarks, ReportAndRequisition.this);
							else{
								//JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
								int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
								if (ack == JOptionPane.YES_OPTION) {
									new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "ARV"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "ARV", normalOrder, ReportAndRequisition.this);
								}
							}
							/*}
							else
							{
								int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
								if (ack == JOptionPane.YES_OPTION) {
									List<StorePhysicalCount> spc = new StorePhysicalCountDao().checkForLastPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "ARV");
									if (spc.size() > 0)
									new ReportGenerator().generatePhysicalCountReport(spc, fromDateTextField.getText(),
											toDateTextField.getText());
									else
										JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>No Physical count for <font color=red>ARV</font> has ever been carried out.</html>", "Information", JOptionPane.WARNING_MESSAGE);
								}
							}*/
							return "Done.";
						}

						protected void done() {
							progressBar.setVisible(false);
						}
					}

					new ReportWorker().execute();
				}
				else if (selected == 1)//EMLIP SECTION
				{
						class ReportWorker extends SwingWorker<String, Object> {
							protected String doInBackground() {
								progressBar.setVisible(true);
								progressBar.setIndeterminate(true);
								CalendarUtil cal = new CalendarUtil();
								/*List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM");

								if (spcList.size() > 0)
								{*/
								if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 1, normalOrder))
									new ReportGenerator().generateEmlipReport(fromDateTextField.getText(),
								toDateTextField.getText(), normalOrder, remarks, ReportAndRequisition.this);
								else{
									//JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>Essential Medicines</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>EM</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "EM", normalOrder, ReportAndRequisition.this);
									}
								}
								/*}
								/*}
								else
								{
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										List<StorePhysicalCount> spc = new StorePhysicalCountDao().checkForLastPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM");
										if (spc.size() > 0)
										new ReportGenerator().generatePhysicalCountReport(spc, fromDateTextField.getText(),
												toDateTextField.getText());
										else
											JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>No Physical count for <font color=red>Essential Medicines</font> has ever been carried out.</html>", "Information", JOptionPane.WARNING_MESSAGE);
									}
								}*/
								return "Done.";
							}

							protected void done() {
								progressBar.setVisible(false);
							}
						}

						new ReportWorker().execute();
					
				} 
				else if (selected == 2)//HIV TESTS SECTION
				{
					 System.out.println("got inside hiv emrgency ~~~~");
						class ReportWorker extends SwingWorker<String, Object> {
							
							protected String doInBackground() {
								progressBar.setVisible(true);
								progressBar.setIndeterminate(true);
								CalendarUtil cal = new CalendarUtil();
								System.out.println("got inside hiv emrgency inside doInbackgroud ~~~~");
								/*List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "HIV");

								if (spcList.isEmpty())
								{*/
								if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 2, normalOrder))
									new ReportGenerator().generateHivReport(fromDateTextField.getText(),
										toDateTextField.getText(), normalOrder, remarks, ReportAndRequisition.this);
								else{
									//JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>HIV</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "HIV"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "HIV", normalOrder,ReportAndRequisition.this);
									}
								}
								/*}
								else
								{
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
												toDateTextField.getText());
									}
								}*/
								return "Done.";
							}

							protected void done() {
								progressBar.setVisible(false);
							}
						}

						new ReportWorker().execute();
				} 
				else if (selected == 3)//LAB USAGE SECTION
				{
						class ReportWorker extends SwingWorker<String, Object> {
							protected String doInBackground() {
								progressBar.setVisible(true);
								progressBar.setIndeterminate(true);
								CalendarUtil cal = new CalendarUtil();
/*								List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB");

								if (spcList.size() > 0)
								{*/
								if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 4, normalOrder))
									new ReportGenerator().generateLabUsageReport(fromDateTextField.getText(),
										toDateTextField.getText(), normalOrder, remarks, ReportAndRequisition.this);
								else{
									//JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>LAB</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>LAB</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "LAB", normalOrder, ReportAndRequisition.this);
									}
								}
								/*}
								else
								{
									int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
									if (ack == JOptionPane.YES_OPTION) {
										List<StorePhysicalCount> spc = new StorePhysicalCountDao().checkForLastPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB");
										if (spc.size() > 0)
										new ReportGenerator().generatePhysicalCountReport(spc, fromDateTextField.getText(),
												toDateTextField.getText());
										else
											JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>No Physical count for <font color=red>LAB</font> has ever been carried out.</html>", "Information LABS", JOptionPane.WARNING_MESSAGE);
									}
								}*/
								return "Done.";
							}

							protected void done() {
								progressBar.setVisible(false);
							}
						}

						new ReportWorker().execute();
				} 
				else // TB SECTION
				{

				}
				remarkTextArea.setText("");
			}
			//viewReportButton.setEnabled(false);
		} 
		else /*if (calendarUtil.compareWithDeadlineDate(true) )*///controls submission of emergency reports
		{
			final String emergencyOrder = "YES";

			int selected = reportTypeComboBox.getSelectedIndex(); //stores index when an item is selected
			if (selected == 0)//ARV SECTION
			{
				class ReportWorker extends SwingWorker<String, Object> {
					protected String doInBackground() {
						progressBar.setVisible(true);
						progressBar.setIndeterminate(true);
						CalendarUtil cal = new CalendarUtil();
												/*List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "ARV");

						if (spcList.size() > 0)
						{*/
						if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 3, emergencyOrder)){
							new ReportGenerator().generateArvDrugsReport(fromDateTextField.getText(),
									toDateTextField.getText(), emergencyOrder, remarks, ReportAndRequisition.this);
						}
						else{
							//JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "ARV"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "ARV", emergencyOrder, ReportAndRequisition.this);
							}
						}
						/*}
						else
						{
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								List<StorePhysicalCount> spc = new StorePhysicalCountDao().checkForLastPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "ARV");
								if (spc.size() > 0)
								new ReportGenerator().generatePhysicalCountReport(spc, fromDateTextField.getText(),
										toDateTextField.getText());
								else
									JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>No Physical count for <font color=red>ARV</font> has ever been carried out.</html>", "Information", JOptionPane.WARNING_MESSAGE);
							}
						}*/
						return "Done.";
					}

					protected void done() {
						progressBar.setVisible(false);
					}
				}

				new ReportWorker().execute();

			} else if (selected == 1)//EMLIP SECTION
			{
				class ReportWorker extends SwingWorker<String, Object> {
					protected String doInBackground() {
						progressBar.setVisible(true);
						progressBar.setIndeterminate(true);
						CalendarUtil cal = new CalendarUtil();
						/*List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM");

						if (spcList.size() > 0)
						{*/
						if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 1, emergencyOrder))
							new ReportGenerator().generateEmlipReport(fromDateTextField.getText(),
						toDateTextField.getText(), emergencyOrder, remarks, ReportAndRequisition.this);
						else{
							//JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>Essential Medicines</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "EM", emergencyOrder, ReportAndRequisition.this);
							}
						}
						/*}
						else
						{
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								List<StorePhysicalCount> spc = new StorePhysicalCountDao().checkForLastPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "EM");
								if (spc.size() > 0)
								new ReportGenerator().generatePhysicalCountReport(spc, fromDateTextField.getText(),
										toDateTextField.getText());
								else
									JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>No Physical count for <font color=red>Essential Medicines</font> has ever been carried out.</html>", "Information", JOptionPane.WARNING_MESSAGE);
							}
						}*/
						return "Done.";
					}

					protected void done() {
						progressBar.setVisible(false);
					}
				}

				new ReportWorker().execute();
			} else if (selected == 2)//HIV TESTS SECTION
			{
				System.out.println("Test RR view ~~~");
				
				class ReportWorker extends SwingWorker<String, Object> {
					protected String doInBackground() {
						progressBar.setVisible(true);
						progressBar.setIndeterminate(true);
						CalendarUtil cal = new CalendarUtil();
						
						/*List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "HIV");

						if (spcList.isEmpty())
						{*/
						if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 2, emergencyOrder))
							new ReportGenerator().generateHivReport(fromDateTextField.getText(),
								toDateTextField.getText(), emergencyOrder, remarks, ReportAndRequisition.this);
						else{
							//JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>HIV</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "HIV"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "HIV", emergencyOrder, ReportAndRequisition.this);
							}
						}
						/*}
						else
						{
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generatePhysicalCountReport(spcList, fromDateTextField.getText(),
										toDateTextField.getText());
							}
						}*/
						return "Done.";
					}

					protected void done() {
						progressBar.setVisible(false);
					}
				}

				new ReportWorker().execute();
			} 
			else if (selected == 3)//LAB USAGE SECTION
			{
				class ReportWorker extends SwingWorker<String, Object> {
					protected String doInBackground() {
						progressBar.setVisible(true);
						progressBar.setIndeterminate(true);
						CalendarUtil cal = new CalendarUtil();
						/*List<StorePhysicalCount> spcList = new StorePhysicalCountDao().checkForPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB");

						if (spcList.size() > 0)
						{*/
						if (!checkLineItemDao.isReportSubmitted(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), 4, emergencyOrder))
							new ReportGenerator().generateLabUsageReport(fromDateTextField.getText(),
								toDateTextField.getText(), emergencyOrder, remarks, ReportAndRequisition.this);
						else{
						//	JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>LAB</font> has already been submitted.</html>", "Information", JOptionPane.WARNING_MESSAGE);
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>R & R for <font color=red>ARV</font> has already been submitted. Would you want to view it?</html>", "Information", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								new ReportGenerator().generateSubmitedRandRs(new ReportRequisitionDao().getSubmitedRandRs(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB"), cal.changeDateFormat(fromDateTextField.getText()), cal.changeDateFormat(toDateTextField.getText()), "LAB", emergencyOrder, ReportAndRequisition.this);
							}
						}
						/*}
						else
						{
							int ack = JOptionPane.showConfirmDialog(ReportAndRequisition.this, "<html>Physical count for some products has not been carried out? <br>Do you want to view a Physical Count Report?</html>", "Confirm Physical Count Report", JOptionPane.YES_NO_OPTION);
							if (ack == JOptionPane.YES_OPTION) {
								List<StorePhysicalCount> spc = new StorePhysicalCountDao().checkForLastPhysicalCount(cal.getSqlDate(cal.changeDateFormat(fromDateTextField.getText())), cal.getSqlDate(cal.changeDateFormat(toDateTextField.getText())), "LAB");
								if (spc.size() > 0)
								new ReportGenerator().generatePhysicalCountReport(spc, fromDateTextField.getText(),
										toDateTextField.getText());
								else
									JOptionPane.showMessageDialog(ReportAndRequisition.this, "<html>No Physical count for <font color=red>LAB</font> has ever been carried out.</html>", "Information", JOptionPane.WARNING_MESSAGE);
							}
						}*/
						return "Done.";
					}

					protected void done() {
						progressBar.setVisible(false);
					}
				}

				new ReportWorker().execute();
			} 
			else // TB SECTION
			{
				javax.swing.JOptionPane.showMessageDialog(null,	"This Program Area not available for Zambia");
			}
			remarkTextArea.setText("");
		}
	}
	/**
	 * Handles event generated as a result of checking the Emergency Order
	 * Check Box. Once checked, the report submitted will be an Emergency
	 * Order R & R
	 * @param evt
	 * @throws ParseException 
	 */
	private void emergencyOrderCheckBoxItemStateChanged(
			java.awt.event.ItemEvent evt) throws ParseException {
		if (evt.getStateChange() == ItemEvent.SELECTED) {
			if (calendarUtil.compareWithDeadlineDate(true)){
				fromDateTextField.setText("");
				fromDateTextField.setText(calendarUtil.getFirstDayOfCurrentMonth());
				//fromDateTextField.setText("01/07/2014");
				toDateTextField.setText("");
				toDateTextField.setText(calendarUtil.getCurrentDate());
				//toDateTextField.setText("31/07/2014");
				isNonEmergency = false;
				emergencyOrderCheckBox.setText("<html><b><h3><font color=red>Custom Period</font></h3><b></html>");
				fromDateTextField.setEditable(true);
				toDateTextField.setEditable(true);
			}
		} else {
			fromDateTextField.setText("");
			fromDateTextField.setText(calendarUtil
					.getFirstDayOfReportingMonth());
			toDateTextField.setText(calendarUtil.getLastDayOfReportingMonth());
			isNonEmergency = true;
			emergencyOrderCheckBox.setText("<html><b><h3>Custom Period</h3><b></html>");
			fromDateTextField.setEditable(false);
			toDateTextField.setEditable(false);
		}
	}

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				Properties prop = new Properties(System.getProperties());
				FileInputStream fis = null;
				try {
					fis = new FileInputStream("Programmproperties.properties");
					prop.load(fis);

					System.setProperties(prop);

					fis.close();

				} catch (IOException e) {
					e.printStackTrace();
				}
				String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
				System.setProperty("dbpassword", dbpassword);
				ReportAndRequisition dialog = new ReportAndRequisition(new javax.swing.JFrame(), true, "msolomon");
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton cancelButton;
	private javax.swing.JCheckBox emergencyOrderCheckBox;
	private javax.swing.JTextField fromDateTextField;
	private javax.swing.JLabel fromJLabel;
	private javax.swing.JLabel toJLabel;
	private javax.swing.JLabel selectProgramAreajLabel;
	private javax.swing.JLabel remarkJLabel;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTextArea remarkTextArea;
	private javax.swing.JComboBox<String> reportTypeComboBox;
	private javax.swing.JButton submitReportButton;
	private javax.swing.JTextField toDateTextField;
	private javax.swing.JButton viewReportButton;
	// End of variables declaration//GEN-END:variables
	private boolean isNonEmergency = true; //this variable determines whether a non-emergency report or emergency report will be submitted
	//private ProgramDao programDao = new ProgramDao();
	private List<String> programsList = new ArrayList<>();
	private String[] reportTypeArray;
	private Frame parent;
	private CalendarUtil calendarUtil = new CalendarUtil();
}