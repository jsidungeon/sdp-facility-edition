package org.elmis.facility.embedded.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.database.connection.MyBatisSqliteConnectionFactory;
import org.elmis.facility.reporting.model.Program;

public class ProgramDao {
	private SqlSessionFactory sqliteSqlSessionFactory;
	private SqlSessionFactory psqlSessionFactory; 
	{
		sqliteSqlSessionFactory = MyBatisSqliteConnectionFactory.getSqlSessionFactory();
		psqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public void updateSqliteTablePrograms()
	{
		SqlSession sqliteSession = sqliteSqlSessionFactory.openSession(false);
		SqlSession psqlSession = psqlSessionFactory.openSession();
		
		try
		{
			int numberOfSqlitePrograms = sqliteSession.selectList("Program.getPrograms").size();
			
			if (numberOfSqlitePrograms == 0)
			{
				List<Program> psqlPrograms = psqlSession.selectList("Program.getPrograms");
				for (Program p : psqlPrograms){
					sqliteSession.insert("Program.insert_program", p);
				}
			}
			else{
				
			}
		}
		finally{
			sqliteSession.close();
			psqlSession.close();
		}
	}
	public List<Program> getPrograms()
	{
		SqlSession sqliteSession = sqliteSqlSessionFactory.openSession();
		SqlSession psqlSession = psqlSessionFactory.openSession();
		try
		{
			return sqliteSession.selectList("Program.getPrograms");
		}
		finally{
			sqliteSession.close();
			psqlSession.close();
		}
	}
}
