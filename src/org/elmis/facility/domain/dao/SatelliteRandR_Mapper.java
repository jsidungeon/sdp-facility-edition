package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.ProductQtyExample;
import org.elmis.facility.domain.model.SatelliteRandR;

public interface SatelliteRandR_Mapper {

    

    int insert(SatelliteRandR record);
    

    ProductQty getcurrentarvproductqty(@Param("prodcode") String prodcode,@Param("dpid") Integer dpid);
    @SuppressWarnings("rawtypes")
	List  getARVproductAdjustmentsPhysicalcountBalance(@Param("prodcode") String prodcode,@Param("dpid") Integer dpid);
    List<SatelliteRandR> getcurrentListSatelliteRandR(@Param("startdate") java.util.Date startdate, @Param("enddate") java.util.Date enddate);
     
   int  updateproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Double prodqty,  @Param ("dpid") Integer dpid);
   int  updateproductqtybyPC(@Param ("prodcode") String prodcode,@Param ("prodqty") Double prodqty, @Param ("dpid")  Integer dpid);
   
   int  setupdatedispenaryproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Integer prodqty,@Param ("dispensary") String dispensary);
   
   int  updatedispenaryproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Double double1,@Param ("dispensary") String dispensary,@Param ("stockonhand_scc")double d);
   
   int updatedispenaryproductqtyless(@Param ("prodcode") String prodcode,@Param ("prodqty") Double positive,@Param ("dispensary") String dispensary,@Param ("stockonhand_scc")double d,
		   @Param ("dpid") Integer  dpid);

  
    int updateelmishivsitebalances(@Param("record") HashMap record);

  
    
    int updateProductQtyBalance(@Param("qtyDeducted") Double qtyDeducted,@Param("productCode") String productCode,@Param("dpid") Integer dispensationUnitId);
}