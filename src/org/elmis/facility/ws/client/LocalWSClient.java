package org.elmis.facility.ws.client;


import org.elmis.forms.regimen.util.GenericUtil;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

public class LocalWSClient {
	
private static String BASE_URL = GenericUtil.getProperties().getProperty("eLMISLocalServerBaseURL");
 public static void main(String[] args) {
	// sendRnR("xx");
	 sync();
//	 sendRnR("");
	}
 public static RnRRequestResult sendRnR(String json){
	 RnRRequestResult result = null;
	 try {
			ClientConfig clientConfig = new DefaultClientConfig();

			clientConfig.getFeatures().put(
					JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

			Client client = Client.create(clientConfig);

			WebResource webResource = client
					.resource(ElmisConstants.LOCAL_ELMIS_SERVER_URL+"/submit-rnr");

			ClientResponse response = webResource.accept("application/json")
					.type("application/json").post(ClientResponse.class, json);

			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}

			String output = response.getEntity(String.class);
			Gson gson = new Gson();
			result = gson.fromJson(output, RnRRequestResult.class);

			System.out.println("Server response .... \n");
			System.out.println(output);

		} catch (Exception e) {

			e.printStackTrace();

		}
	return result;

 }
 
 public static SyncRequestResult sync(){
		try {

			Client client = Client.create();

			WebResource webResource = client
			   .resource(BASE_URL+"/sync");

			ClientResponse response = webResource.accept("application/json")
	                  .get(ClientResponse.class);

			if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
			}

			String resultString = response.getEntity(String.class);
			
			Gson gson = new Gson();

			SyncRequestResult result = (SyncRequestResult)gson.fromJson(resultString, SyncRequestResult.class);
			return result;

		  } catch (Exception e) {

			e.printStackTrace();

		  }
		return null;
}
 
 
 
 public static void sendRequest(String url){
		try {

			Client client = Client.create();

			WebResource webResource = client
			   .resource(url);

			ClientResponse response = webResource.accept("application/json")
	                  .get(ClientResponse.class);

			if (response.getStatus() != 200) {
			   throw new RuntimeException("Failed : HTTP error code : "
				+ response.getStatus());
			}

			String output = response.getEntity(String.class);

			System.out.println("Output from Server .... \n");
			System.out.println(output);

		  } catch (Exception e) {

			e.printStackTrace();

		  }
 }
}