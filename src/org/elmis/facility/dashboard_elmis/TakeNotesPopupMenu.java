package org.elmis.facility.dashboard_elmis;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.elmis.facility.dashboard.util.TakeNotes;

public class TakeNotesPopupMenu extends JPopupMenu{

	private  JMenuItem saveNotes = new JMenuItem("Save");
	private  JMenuItem clearNotes = new JMenuItem("Clear");
	private  JMenuItem printNotes = new JMenuItem("Print");
	private  JTextArea textArea;

	public TakeNotesPopupMenu(JTextArea textArea)
	{
		this.textArea = textArea;
		 ActionListener menuListener = new ActionListener() {
		      public void actionPerformed(ActionEvent event) {
		        if (event.getActionCommand().equalsIgnoreCase("clear"))
		        {
		        	int ack = JOptionPane.showConfirmDialog(TakeNotesPopupMenu.this.textArea, "<html>Clearing will delete all your Notes. <br>Do you want to clear?</html>", "Confirm clear", JOptionPane.YES_NO_OPTION);
					if (ack == JOptionPane.YES_OPTION) {
						TakeNotesPopupMenu.this.textArea.setText("");
						new TakeNotes("TakeNotes.txt").clearNotes();
					}
		        }
		        else if (event.getActionCommand().equalsIgnoreCase("Save"))
		        {
		        	
		        }
		        else //handles print of notes
		        {
		        	
		        }
		      }
		    };
		add(saveNotes);
		saveNotes.setHorizontalTextPosition(JMenuItem.RIGHT);
		saveNotes.addActionListener(menuListener);
		addSeparator();
		add(clearNotes);
		clearNotes.setHorizontalTextPosition(JMenuItem.RIGHT);
		clearNotes.addActionListener(menuListener);
		setBorder(new BevelBorder(BevelBorder.RAISED));
		addSeparator();
		printNotes.setHorizontalTextPosition(JMenuItem.RIGHT);
		printNotes.addActionListener(menuListener);
		setBorder(new BevelBorder(BevelBorder.RAISED));
		add(printNotes);
		addMouseListener(new MousePopupListener());
	}
	class PopupPrintListener implements PopupMenuListener
	{
		public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
			System.out.println("Popup menu will be visible!");
		}

		public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
			System.out.println("Popup menu will be invisible!");
		}

		public void popupMenuCanceled(PopupMenuEvent e) {
			System.out.println("Popup menu is hidden!");
		}
	}
	class MousePopupListener extends MouseAdapter {
	    public void mousePressed(MouseEvent e) {
	      checkPopup(e);
	    }

	    public void mouseClicked(MouseEvent e) {
	      checkPopup(e);
	    }

	    public void mouseReleased(MouseEvent e) {
	      checkPopup(e);
	    }

	    private void checkPopup(MouseEvent e) {
	      if (e.isPopupTrigger()) {
	    	  //saveNotes.show(TakeNotesPopupMenu.this, e.getX(), e.getY());
	      }
	    }
	  }
}
