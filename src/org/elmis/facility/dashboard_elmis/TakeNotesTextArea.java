package org.elmis.facility.dashboard_elmis;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTextArea;

import org.elmis.facility.dashboard.util.TakeNotes;

public class TakeNotesTextArea extends JTextArea{
	private TakeNotes takeNotes = new TakeNotes("TakeNotes.txt");
	public TakeNotesTextArea()
	{
		setComponentPopupMenu(new TakeNotesPopupMenu(this));
		append(takeNotes.retrieveNotes());
		this.addKeyListener(new KeyAdapter() {
			@Override
			  public void keyPressed(KeyEvent evt) {
			    if(evt.getKeyCode() == KeyEvent.VK_ENTER)
			    {
			    	StringBuilder sb = new StringBuilder(getText());
			         takeNotes.saveNotes(sb);
			    }
			  }
		});
	}

}
