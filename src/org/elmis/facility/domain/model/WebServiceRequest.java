package org.elmis.facility.domain.model;

public class WebServiceRequest {
	private int id;
	private String programArea;
	private String reportingPeriod;
	private String requestStatus;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProgramArea() {
		return programArea;
	}
	public void setProgramArea(String programArea) {
		this.programArea = programArea;
	}
	public String getReportingPeriod() {
		return reportingPeriod;
	}
	public void setReportingPeriod(String reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
}
