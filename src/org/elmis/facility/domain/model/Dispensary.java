package org.elmis.facility.domain.model;

public class Dispensary {
	private int id;
	private int siteId;
	private String dispensingPointName;
	private String programArea;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public String getDispensingPointName() {
		return dispensingPointName;
	}
	public void setDispensingPointName(String dispensingPointName) {
		this.dispensingPointName = dispensingPointName;
	}
	public String getProgramArea() {
		return programArea;
	}
	public void setProgramArea(String programArea) {
		this.programArea = programArea;
	}
	@Override
	public String toString() {
		return "Dispensary [id=" + id + ", siteId=" + siteId
				+ ", dispensingPointName=" + dispensingPointName
				+ ", programArea=" + programArea + "]";
	}
	
}
