package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportType;

public class ReportTypeDao {
	private SqlSessionFactory sqlSessionFactory;

	public ReportTypeDao()
	{	
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<ReportType> selectAll()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try {
			List<ReportType> list = session.selectList("ReportType.getAll");
			return list;
		} finally {
			session.close();
		}
	}
}
