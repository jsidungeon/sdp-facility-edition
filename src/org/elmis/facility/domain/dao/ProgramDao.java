package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.database.connection.MyBatisSqliteConnectionFactory;
import org.elmis.facility.reporting.model.Program;

public class ProgramDao {
	private SqlSessionFactory psqlSqlSessionFactory;
	{
		psqlSqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}

	public List<Program> getPrograms()
	{
		SqlSession session = psqlSqlSessionFactory.openSession();;

		try {

			return session.selectList("Program.getPrograms");

		} finally {
			session.close();
		}
	}
}
