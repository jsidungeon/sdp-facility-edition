package org.elmis.facility.webservice.request;

public class WebServiceRequest {
	
	private Integer id;
	private String uuid;
	private String name;
	private String type;
	private String status;
	private String parameters;
	
	public WebServiceRequest(String rUUID, String rName, String rType, String rStatus, String rParameters){
		this.uuid  = rUUID;
		this.name = rName;
		this.type = rType;
		this.status = rStatus;
		this.parameters = rParameters;
	}
	public WebServiceRequest(){ };
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getParameters() {
		return parameters;
	}
	
	public void setParameters(String parameters) {
		this.parameters = parameters;
	}
	
	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

}
