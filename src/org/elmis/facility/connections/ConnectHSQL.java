package org.elmis.facility.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectHSQL {

	private static final String USERNAME_HSQL = "dbuser";
	private static final String PASSWORD_HSQL = "dbpassword";
	private static final String USERNAME_POSTGRESQL = "postgres";
	private static final String PASSWORD_POSTGRESQL = "p@ssw0rd";
	private static final String CONN_STRING_ELMIS_HSQL = "jdbc:hsqldb:database/explorecalifornia";
	//private static final String CONN_STRING_ELMIS_HSQL = "jdbc:hsqldb:database/elmis_facility";
	private static final String CONN_STRING_ELMIS_POSTGRES = "jdbc:postgresql://localhost:5432/postgres";

	public static void main(String[] args) throws ClassNotFoundException {

		//Class.forName("org.hsqldb.jdbcDriver");

		try (
				Connection connection = DriverManager.getConnection(CONN_STRING_ELMIS_HSQL, USERNAME_HSQL, PASSWORD_HSQL
						)){

			System.out.println("Connected to HSQLDB ELMIS");

		} catch (SQLException e) {
			System.err.println("ERROR HSQL "+e.getMessage());
		}
		
		try(
				Connection connection = DriverManager.getConnection(CONN_STRING_ELMIS_POSTGRES, USERNAME_POSTGRESQL, PASSWORD_POSTGRESQL
						)){
			
			System.out.println("Connected to database elmis db on postgres");
			
		}
		catch(SQLException e)
		{
			System.out.println("Error postgress: "+e.getMessage());
		}

	}

}
