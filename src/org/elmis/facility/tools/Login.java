package org.elmis.facility.tools;

import java.sql.Connection;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.elmis.facility.domain.dao.UsersDAO;
import org.elmis.facility.domain.model.users;

import com.oribicom.tools.publicMethods;

public class Login extends Thread {

	private String password = "";
	private String userName;
	private users user;
	private Statement st;
	private Connection conn;
	public static String userLoggedIn;
	private static UsersDAO mUserdao;

	List<users> usersList = new LinkedList();

	public Login() {

	}

	public users doLogin(String userName, String password) {

		this.userName = userName;
		this.password = password;
		users user = new users();

		password = publicMethods.hash(this.password);

		// call User DAO

		try {
			mUserdao = new UsersDAO();
			user = mUserdao.getByUser(password, userName);

			return user;
		} finally {

		}

	}
}
