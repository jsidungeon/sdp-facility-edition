package org.elmis.facility.domain.model;

import java.sql.Timestamp;

public class arvdispensarystockstatus {
    
	private String  productcode ;
	private String   transactiontype ;
	private Double  transactionquantity;
	private Double  balance;
	private String   createdby ;
	private Timestamp createddate;
	private String  transactionremark;
	private Integer dpid ;
	
	public String getProductcode() {
		return productcode;
	}
	public void setProductcode(String productcode) {
		this.productcode = productcode;
	}
	public String getTransactiontype() {
		return transactiontype;
	}
	public void setTransactiontype(String transactiontype) {
		this.transactiontype = transactiontype;
	}
	public Double getTransactionquantity() {
		return transactionquantity;
	}
	public void setTransactionquantity(Double transactionquantity) {
		this.transactionquantity = transactionquantity;
	}
	public Double getBalance() {
		return balance;
	}
	public void setBalance(Double balance) {
		this.balance = balance;
	}
	public String getCreatedby() {
		return createdby;
	}
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}
	public Timestamp getCreateddate() {
		return createddate;
	}
	public void setCreateddate(Timestamp createddate) {
		this.createddate = createddate;
	}
	public String getTransactionremark() {
		return transactionremark;
	}
	public void setTransactionremark(String transactionremark) {
		this.transactionremark = transactionremark;
	}
	public Integer getDpid() {
		return dpid;
	}
	public void setDpid(Integer dpid) {
		this.dpid = dpid;
	}

	
	
}