package org.elmis.forms.regimen;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.domain.model.RegimenProductDosage;
import org.elmis.forms.regimen.dao.RegimenDao;
import org.elmis.forms.regimen.util.GenericUtil;

public class DefineRegimenProductDosage extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel pnlMain = new JPanel();
	private JTable tblProductDosages;
	private JTextField txtProductDosage;
	private JScrollPane scrollPane;
	private RegimenProductDosage selectedDosageUnit = null;
	private DefineRegimenProductDosage _this = this;
	private List<RegimenProductDosage> dosages = null;
	private Integer selectedRegimenId = null;
	private Integer selectedProdcutId = null;
	private JComboBox cboDosageUnit;
	private JComboBox cboFrequency;
	protected boolean dataChange = false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DefineRegimenProductDosage dialog = new DefineRegimenProductDosage(null, true , 1, 1070);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void populateProductDosages(Integer regimenId, Integer productId){
		GenericUtil.clearTable(tblProductDosages);
		RegimenDao dao = RegimenDao.getInstance();
		dosages   = dao.fetchDosages(regimenId,productId);//
		DefaultTableModel model = (DefaultTableModel) tblProductDosages.getModel();
		for (RegimenProductDosage dosage : dosages) {
			model.addRow(new Object[] { dosage.getName()});
		}

	/*	TableColumnModel m=tblProducts.getColumnModel();
		TableColumn col=m.getColumn(3);
		m.removeColumn(col);*/
		tblProductDosages.setModel(model);
	}
	
	private void addDosageToGUITable(RegimenProductDosage dosage){
		DefaultTableModel model = (DefaultTableModel) tblProductDosages.getModel();
		model.addRow(new Object[] { dosage.getName()});
		this.dosages.add(dosage);
	}
	
	private void updateDosageGUITable(RegimenProductDosage dosage){
		DefaultTableModel model = (DefaultTableModel) tblProductDosages.getModel();
		model.setValueAt(dosage.getName(), tblProductDosages.getSelectedRow(), 0);
	}

	@SuppressWarnings("serial")
	private void setUpRegimenProductDosagesTable() {
	
		pnlMain.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(29, 165, 287, 155);
		pnlMain.add(scrollPane);
		
				tblProductDosages = new JTable();
				scrollPane.setViewportView(tblProductDosages);
				tblProductDosages.setModel(new DefaultTableModel(
					new Object[][] {
					},
					new String[] {
						"Product Dosages"
					}
				));
				tblProductDosages.getColumnModel().getColumn(0).setPreferredWidth(377);
				tblProductDosages.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			        public void valueChanged(ListSelectionEvent event) {
//			             do some actions here, for example
//			             print first column value from selected row
//			            System.out.println(tblProductDosages.getValueAt(tblProductDosages.getSelectedRow(), 0).toString());
			        	if(!_this.dataChange){
			        		_this.selectProductDosages();
			        	}
			            //set checked products
			        }
			    });
				
	}
	
	protected void selectProductDosages() {
		// TODO Auto-generated method stub
		_this.selectedDosageUnit = this.dosages.get(tblProductDosages.getSelectedRow());
		String [] parts = this.dosages.get(tblProductDosages.getSelectedRow()).getName().toString().split("\\s+");
		_this.txtProductDosage.setText(parts[0]);
		_this.cboDosageUnit.setSelectedItem(parts[1]);
		_this.cboFrequency.setSelectedItem(parts[2]);
		System.out.println(_this.selectedDosageUnit.getName());
	}

	private void hideColumn(TableColumn column){
		column.setMinWidth(0);
		column.setMaxWidth(0);
		column.setPreferredWidth(0);
	}

	private Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prop;
	}

	/**
	 * Create the dialog.
	 */
	public DefineRegimenProductDosage(java.awt.Frame parent, boolean modal , Integer regimenId , Integer productId) {
		super(parent, modal);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
			}
		});
		_this.selectedProdcutId = productId;
		_this.selectedRegimenId = regimenId;
		setTitle("Product Dosages");
		setResizable(false);
		setBounds(100, 100, 361, 410);
		getContentPane().setLayout(new BorderLayout());
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnlMain, BorderLayout.CENTER);
		{
			setUpRegimenProductDosagesTable();
		}
		
		JSeparator separator = new JSeparator();
		separator.setBounds(438, 60, 1, 2);
		pnlMain.add(separator);
		
		JPanel panel = new JPanel();
		panel.setBounds(29, 56, 287, 88);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlMain.add(panel);
		panel.setLayout(null);
				
						
								txtProductDosage = new JTextField();
								txtProductDosage.setBounds(20, 42, 95, 20);
								panel.add(txtProductDosage);
								txtProductDosage.setColumns(10);
								txtProductDosage.getDocument().addDocumentListener(new DocumentListener() {
									public void changedUpdate(DocumentEvent e) {
										handleTextChage();
									}

									public void removeUpdate(DocumentEvent e) {
										handleTextChage();
									}

									public void insertUpdate(DocumentEvent e) {
										handleTextChage();
									}

									public void handleTextChage() {
										cboDosageUnit.removeAllItems();
										   if (txtProductDosage.getText().length() > 0) {
											if (Double.valueOf(txtProductDosage
													.getText()) == 1) {
												String[] dosageUnitStrings = {
														"tab", "ml", "mg",
														"cap" };
												for (String string : dosageUnitStrings) {
													cboDosageUnit
															.addItem(string);
												}
											} else {
												String[] dosageUnitStrings = {
														"tabs", "ml", "mg",
														"caps" };
												for (String string : dosageUnitStrings) {
													cboDosageUnit
															.addItem(string);
												}
											}
										}
										}
									}
								);
								
										JLabel lblProductDosage = new JLabel("Product Dosage");
										lblProductDosage.setBounds(20, 21, 200, 14);
										panel.add(lblProductDosage);
										
										String[] dosageUnitStrings = {"tab" , "ml" , "mg" , "cap"};
										cboDosageUnit = new JComboBox(dosageUnitStrings);
										cboDosageUnit.setBounds(125, 42, 71, 20);
										panel.add(cboDosageUnit);
										
										String[] frequencyStrings = { "bd" , "od"};
										cboFrequency = new JComboBox(frequencyStrings);
										cboFrequency.setBounds(206, 42, 71, 20);
										panel.add(cboFrequency);
										
										JPanel panel_1 = new JPanel();
										panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
										panel_1.setBounds(0, 5, 355, 40);
										pnlMain.add(panel_1);
												panel_1.setLayout(null);
										
										
												JButton btnSave = new JButton("");
												btnSave.setIcon(new ImageIcon(DefineRegimenProductDosage.class.getResource("/elmis_images/Save icon.png")));
												btnSave.setBorderPainted(false);
												btnSave.setContentAreaFilled(false);
												btnSave.setBounds(10, 5, 20, 23);
												panel_1.add(btnSave);
												
												JButton btnDelete = new JButton("");
												btnDelete.setBorderPainted(false);
												btnDelete.setContentAreaFilled(false);
												btnDelete.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent arg0) {
														int confirm = JOptionPane.showOptionDialog(_this,  "Are you sure you want to delete dosage?" , "Confirm Delete Dosage ",
																JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
																null, null, null);
														
														_this.dataChange = true;
														if( confirm == JOptionPane.YES_OPTION){
															RegimenDao.getInstance().deleteDosage(_this.selectedDosageUnit);
															populateProductDosages(_this.selectedRegimenId, _this.selectedProdcutId);
															_this.selectedDosageUnit = null;
															_this.txtProductDosage.setText("");
														}
														_this.dataChange = false;
													}
												});
												btnDelete.setIcon(new ImageIcon(DefineRegimenProductDosage.class.getResource("/elmis_images/eLMIS delete role small.png")));
												btnDelete.setBounds(64, 5, 26, 23);
												panel_1.add(btnDelete);
												
												JButton btnNew = new JButton("");
												btnNew.setBorderPainted(false);
												btnNew.setContentAreaFilled(false);
												btnNew.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent arg0) {
														_this.selectedDosageUnit = null;
														_this.txtProductDosage.setText("");
													}
												});
												btnNew.setIcon(new ImageIcon(DefineRegimenProductDosage.class.getResource("/elmis_images/Add.png")));
												btnNew.setBounds(37, 5, 20, 23);
												panel_1.add(btnNew);
												
												btnSave.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent e) {
														_this.dataChange = true;
														if(_this.selectedDosageUnit == null){
															RegimenProductDosage dosage = new RegimenProductDosage();
															System.out.println(_this.txtProductDosage.getText().trim() + " "+ cboDosageUnit.getSelectedItem().toString()+" "+cboFrequency.getSelectedItem().toString());
															dosage.setName(_this.txtProductDosage.getText().trim() + " "+ cboDosageUnit.getSelectedItem().toString()+" "+cboFrequency.getSelectedItem().toString());
															dosage.setCode(_this.txtProductDosage.getText().trim() + " "+ cboDosageUnit.getSelectedItem().toString()+" "+cboFrequency.getSelectedItem().toString());
															dosage.setPrductRegimenId(RegimenDao.getInstance().getProductRegimenIdByRegAndProd(_this.selectedRegimenId, _this.selectedProdcutId));
															RegimenDao.getInstance().insertDosage(dosage);
														} else {
															_this.selectedDosageUnit.setName(_this.txtProductDosage.getText().trim() + " "+ cboDosageUnit.getSelectedItem().toString()+" "+cboFrequency.getSelectedItem().toString());
															_this.selectedDosageUnit.setCode(_this.txtProductDosage.getText().trim() + " "+ cboDosageUnit.getSelectedItem().toString()+" "+cboFrequency.getSelectedItem().toString());
															RegimenDao.getInstance().updateDosage(_this.selectedDosageUnit);
														}
														_this.populateProductDosages(_this.selectedRegimenId, _this.selectedProdcutId);
														_this.dataChange = false;
														JOptionPane.showMessageDialog(_this, "Changes Saved Successfully!");
													}
												});
				_this.selectedProdcutId = productId;
				_this.selectedRegimenId = regimenId;
			   populateProductDosages(regimenId,productId);					
				this.setVisible(true);
	}
}
