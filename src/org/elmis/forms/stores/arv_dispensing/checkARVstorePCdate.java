package org.elmis.forms.stores.arv_dispensing;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.domain.model.Dispensary_Store_Physical_Count;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;

public class checkARVstorePCdate {
	ARVDispensingDAO Facilityproductsmapper;
	List<Elmis_Dar_Transactions> ARVstorephyscalcountcheckList = new LinkedList();
	
	List<Dispensary_Store_Physical_Count> ARVstorePCdateList = new LinkedList();
	private String  prodCode ;
	private Integer DPid;
	public checkARVstorePCdate(String productcode, Integer dpid){
     		
		prodCode = productcode;
		DPid = dpid;
		createdateObjects();
	}
	
	
	
	public static Date toDate(java.sql.Timestamp timestamp){
		//long millisec = timestamp.getTime() + (timestamp.getNanos() / 1000000);
		//return new Date(millisec);
		 Date date = new Date(timestamp.getTime());
		return date;

	}
	
	public void createdateObjects(){
		
		Date dar_date =  null;
		
		Date disp_store_date =  null;
		
		Facilityproductsmapper = new ARVDispensingDAO();
		Timestamp productdeliverdate = null;
		String oldDateString;
		String mydate = new java.util.Date().toString();
		String newDateString;
		final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
		// final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
		final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
		oldDateString = mydate;

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		java.util.Date d;
		d = null;
		try {
			d = sdf.parse(oldDateString);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(d);
		productdeliverdate = Timestamp.valueOf(newDateString);

		// end create time stamp

		Date Startdate = new Date();
		Date Enddate = new Date();
		if (productdeliverdate != null) {

			// convert to date value
			Date convertdate = toDate(productdeliverdate);
			// date split up methods
			Calendar cal = Calendar.getInstance();
			cal.setTime(convertdate);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			if (!(day == 1)) {
				day = 1;
				cal.set(Calendar.MONTH, month);
				cal.set(Calendar.DATE, day);
				cal.set(Calendar.YEAR, year);
				Startdate = cal.getTime();
			}
			Enddate = toDate(productdeliverdate);
		}
		
		ARVstorephyscalcountcheckList = Facilityproductsmapper.docheckifOtherlasttransactionoftheMonth(Startdate, Enddate, prodCode,DPid);
		ARVstorePCdateList = Facilityproductsmapper.docheckifPCdateOtherlasttransactionoftheMonth(Startdate, Enddate, prodCode, DPid);
		for( Elmis_Dar_Transactions e  : ARVstorephyscalcountcheckList ){
			if(e.getTransaction_date() != null){ 
			dar_date = toDate(e.getTransaction_date());
			}
		}
		
			
			for(  Dispensary_Store_Physical_Count f  :  ARVstorePCdateList){
				 if(f.getCreateddate() !=  null){
					 disp_store_date = f.getCreateddate();
			}
			
			}
			//if(toDate(e.getTransaction_date()).after(f.getCreateddate())){
			if(dar_date != null  &&  disp_store_date != null){
				if(dar_date.after(disp_store_date)){
			// check if transaction date >  PC date 				
				Facilityproductsmapper.doupdateARVstorephysicalcountBoolean(prodCode, DPid);
			
			}else{
				Facilityproductsmapper.doupdateARVstorephysicalcountBoolean(prodCode, DPid);
			}
			}else{
				//save physical count the first time around
				Facilityproductsmapper.doupdateARVstorephysicalcountBoolean(prodCode, DPid);
			}
		
		
		}
		
	}


