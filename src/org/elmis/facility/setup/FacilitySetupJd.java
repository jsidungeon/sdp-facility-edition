package org.elmis.facility.setup;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.elmis.facility.connections.ConnectionManager;
import org.elmis.facility.connections.DBType;
import org.elmis.facility.domain.dao.FacilityDao;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.ElmisProperties;
import org.elmis.facility.utils.SdpStyleSheet;

public class FacilitySetupJd extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private SecondPanel secondPanel;
	private Map<String, Object> dbSettingsMap = new HashMap<>();
	private final JButton nextButton;
	private JLabel connectionStatusLbl;
	private final JButton cancelButton;
	private JPanel firstPanel;
	private JTextField domainTxtfield;
	private JTextField dbNameTxtfield;
	private JTextField dbUserTxtfield;
	private JPasswordField dbPasswordTxtfield;
	private JPasswordField dbConfirmPasswordTxtfield;
	private boolean installationIsComplete = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Properties prop = new Properties(System.getProperties());
			FileInputStream fis = null;
			try {
				fis = new FileInputStream("Programmproperties.properties");
				prop.load(fis);

				System.setProperties(prop);

				fis.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
			String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
			System.setProperty("dbpassword", dbpassword);
			FacilitySetupJd dialog = new FacilitySetupJd(new javax.swing.JFrame(), true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public FacilitySetupJd(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setBounds(100, 100, 694, 468);
		getContentPane().setLayout(new BorderLayout());
		SdpStyleSheet.configJPanelBackground(contentPanel);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel buttonPane = new JPanel();
			SdpStyleSheet.configJPanelBackground(buttonPane);
			buttonPane.setBounds(0, 388, 678, 42);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(null);
			{
				nextButton = new JButton("next >>");
				nextButton.setEnabled(false);
				nextButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						cancelButton.setText("<< Back");
						if (nextButton.getText().equalsIgnoreCase("next >>"))
							ElmisProperties.saveDatabaseServerSettings(dbSettingsMap.get("domain").toString(), dbSettingsMap.get("username").toString(), dbSettingsMap.get("password").toString(), dbSettingsMap.get("database").toString());
						if (secondPanel == null)
						secondPanel = new SecondPanel();
						firstPanel.setVisible(false);
						secondPanel.setBounds(0, 0, 668, 385);
						contentPanel.add(secondPanel);
						secondPanel.setVisible(true);
						Map<String, Object> map = secondPanel.getFacilityDetails();
						//map.putAll(dbSettingsMap);
						
						if (nextButton.getText().equalsIgnoreCase("finish"))
						{
							int mapSize = map.size();
							if(mapSize > 3){
								installationIsComplete = true;
								String facilityCode = map.get("facilityCode").toString();
								ElmisProperties.saveFacilitySettings(map.get("province").toString(), map.get("district").toString(), map.get("facility").toString(), facilityCode, map.get("facilityTypeId").toString(), map.get("facilityId").toString());
								new FacilityDao().setSelectedFacility(facilityCode);
								//ElmisProperties.saveSetupSettings(map.get("province").toString(), map.get("district").toString(), map.get("facility").toString(),map.get("domain").toString(), map.get("username").toString(), map.get("password").toString(), map.get("database").toString());
								dispose();
							}
							else
							{
								switch(mapSize)
								{
								case 0:
									javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Please select a province, district & facility.");
									break;
								case 1:
									javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Please select a district & facility.");
									break;
								case 2:
									javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Please select a facility.");
									break;
								default:
									javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"General error at set-up.");
								}
							}
						}
						nextButton.setText("finish");
					}
				});
				nextButton.setBounds(570, 0, 83, 31);
				nextButton.setActionCommand("NEXT");
				buttonPane.add(nextButton);
				getRootPane().setDefaultButton(nextButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
						if (cancelButton.getText().equalsIgnoreCase("Cancel"))
							dispose();
						else{
							nextButton.setText("next >>");
							nextButton.setEnabled(false);
							connectionStatusLbl.setText("");
							cancelButton.setText("Cancel");
							secondPanel.setVisible(false);
							firstPanel.setVisible(true);
						}
					}
				});
				cancelButton.setBounds(477, 0, 83, 31);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		
		firstPanel = new JPanel();
		SdpStyleSheet.configJPanelBackground(firstPanel);
		firstPanel.setBounds(0, 0, 668, 385);
		contentPanel.add(firstPanel);
		firstPanel.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Configure Database Server Settings");
		SdpStyleSheet.configTitleJLabel(lblNewLabel);
		lblNewLabel.setBounds(10, 11, 281, 14);
		firstPanel.add(lblNewLabel);
		
		JLabel lblDomainNameOr = new JLabel("Domain name or IP Address:");
		SdpStyleSheet.configOtherJLabel(lblDomainNameOr);
		lblDomainNameOr.setBounds(35, 52, 211, 14);
		firstPanel.add(lblDomainNameOr);
		
		domainTxtfield = new JTextField();
		domainTxtfield.setBounds(256, 48, 131, 35);
		firstPanel.add(domainTxtfield);
		domainTxtfield.setColumns(10);
		
		JLabel lblDatabaseName = new JLabel("Database name:");
		SdpStyleSheet.configOtherJLabel(lblDatabaseName);
		lblDatabaseName.setBounds(35, 100, 119, 14);
		firstPanel.add(lblDatabaseName);
		
		dbNameTxtfield = new JTextField();
		dbNameTxtfield.setBounds(256, 96, 131, 33);
		firstPanel.add(dbNameTxtfield);
		dbNameTxtfield.setColumns(10);
		
		JLabel lblDatabaseUser = new JLabel("Database user:");
		SdpStyleSheet.configOtherJLabel(lblDatabaseUser);
		lblDatabaseUser.setBounds(35, 144, 106, 33);
		firstPanel.add(lblDatabaseUser);
		
		dbUserTxtfield = new JTextField();
		dbUserTxtfield.setText("postgres");
		dbUserTxtfield.setBounds(256, 140, 131, 33);
		firstPanel.add(dbUserTxtfield);
		dbUserTxtfield.setColumns(10);
		
		JLabel lblDatabasePassword = new JLabel("Database password:");
		SdpStyleSheet.configOtherJLabel(lblDatabasePassword);
		lblDatabasePassword.setBounds(35, 191, 145, 14);
		firstPanel.add(lblDatabasePassword);
		
		dbPasswordTxtfield = new JPasswordField();
		dbPasswordTxtfield.setBounds(256, 187, 131, 33);
		firstPanel.add(dbPasswordTxtfield);
		
		dbConfirmPasswordTxtfield = new JPasswordField();
		dbConfirmPasswordTxtfield.setBounds(256, 233, 131, 33);
		firstPanel.add(dbConfirmPasswordTxtfield);
		
		JLabel lblConfirmPassword = new JLabel("Confirm password:");
		SdpStyleSheet.configOtherJLabel(lblConfirmPassword);
		lblConfirmPassword.setBounds(35, 237, 145, 14);
		firstPanel.add(lblConfirmPassword);
		
		JLabel label = new JLabel("*");
		label.setForeground(Color.RED);
		label.setBounds(243, 52, 19, 14);
		firstPanel.add(label);
		
		JLabel label_1 = new JLabel("*");
		label_1.setForeground(Color.RED);
		label_1.setBounds(140, 144, 14, 33);
		firstPanel.add(label_1);
		
		JLabel label_2 = new JLabel("*");
		label_2.setForeground(Color.RED);
		label_2.setBounds(166, 100, 14, 14);
		firstPanel.add(label_2);
		
		JLabel label_3 = new JLabel("*");
		label_3.setForeground(Color.RED);
		label_3.setBounds(175, 191, 14, 14);
		firstPanel.add(label_3);
		
		JLabel label_4 = new JLabel("*");
		label_4.setForeground(Color.RED);
		label_4.setBounds(190, 242, 14, 14);
		firstPanel.add(label_4);
		
		JButton btnTestDatabaseSettings = new JButton("Test database settings");
		btnTestDatabaseSettings.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String dbUsername = dbUserTxtfield.getText().trim();
				String dbName = dbNameTxtfield.getText().trim();
				String domain = domainTxtfield.getText().trim();
				String dbPassword = new String(dbPasswordTxtfield.getPassword()).trim();
				String confirmPassword = new String(dbConfirmPasswordTxtfield.getPassword()).trim();
				if(domain.length() == 0)
					javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Domain Name or IP field cannot be empty.");
				else if(dbName.length() == 0)
					javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Database Name field cannot be empty.");
				else if (dbUsername.length() < 6){
					if (dbUsername.length() == 0)
						javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Empty Database Username field.");
					else
						javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Database Username "+dbUsername+" should be atleast six characters in lenth.");
				}
				else if (dbPassword.length() == 0 || dbPassword.length() < 8)
				{
					javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Empty Database Password field or Password has less than Eight characters.");
					dbPasswordTxtfield.setText("");
				}
				else if (confirmPassword.length() == 0 || confirmPassword.length() < 8)
				{
					javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Empty Confirm Database Password field or Confirm Password has less than Eight characters.");
					dbConfirmPasswordTxtfield.setText("");
				}
				else
				{
					if (dbPassword.equals(confirmPassword)){
						dbSettingsMap.put("domain", domain);
						dbSettingsMap.put("username", dbUsername);
						dbSettingsMap.put("database", dbName);
						try {
							dbSettingsMap.put("password", ElmisAESencrpDecrp.encrypt(dbPassword));
						} catch (Exception e2) {
							// TODO Auto-generated catch block
							e2.printStackTrace();
						}
						boolean success = false;
						ConnectionManager conManager = ConnectionManager.getInstance();
						conManager.setDBType(DBType.POSTGRESQL);
						conManager.testConnection(domain, dbUsername, dbPassword, dbName);
						try {
							conManager.getConnection();
							success = true;
						} catch (ClassNotFoundException | SQLException e1) {
							javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,e1.getMessage());
						}
						finally{
							conManager.close();
						}
						if (success){
							connectionStatusLbl.setText("Successful");
							connectionStatusLbl.setForeground(Color.GREEN);
							nextButton.setEnabled(true);
						}
						else
						{
							connectionStatusLbl.setText("Failed");
							connectionStatusLbl.setForeground(Color.RED);
							nextButton.setEnabled(false);
						}
					}
					else{
						javax.swing.JOptionPane.showMessageDialog(FacilitySetupJd.this,"Database Password and Confirm Password must be the same.");
						dbPasswordTxtfield.setText("");
						dbConfirmPasswordTxtfield.setText("");
					}
				}
			}
		});
		btnTestDatabaseSettings.setBounds(175, 300, 212, 35);
		firstPanel.add(btnTestDatabaseSettings);
		
		connectionStatusLbl = new JLabel("");
		connectionStatusLbl.setBounds(544, 11, 106, 14);
		firstPanel.add(connectionStatusLbl);
	}
	public boolean installationComplete()
	{
		return installationIsComplete;
	}
}
