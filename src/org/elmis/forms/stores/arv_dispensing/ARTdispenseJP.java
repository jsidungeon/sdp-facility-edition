/*
 * ARTdispenseJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.arv_dispensing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.KeyEvent;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.UUID;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableCellRenderer;

import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.VW_Program_Facility_ApprovedProducts;
import org.elmis.facility.main.gui.AppJFrame;

import com.oribicom.tools.TableModel;

/**
 *
 * @author  __USER__
 */
public class ARTdispenseJP extends javax.swing.JPanel {

	/** Creates new form ARTdispenseJP */

	//Facility Approved Products JTable **************************************************

	private static final String[] columns_facilityApprovedproducts = {
			"Product Code", "Product name", "Strength", "Pack size" };
	private static final Object[] defaultv_facilityapprovedproducts = { "", "",
			"", "" };
	private static final int rows_fproducts = 0;
	public static TableModel tableModel_fproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				/*if (columnIndex == 2) {

					return getValueAt(0, 2).getClass();
				}*/

				if (columnIndex == 3) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0, 3).getClass();
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	public static TableModel tableModel_searchfproducts = new TableModel(
			columns_facilityApprovedproducts,
			defaultv_facilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			try {
				/*if (columnIndex == 2) {
	
					return getValueAt(0, 2).getClass();
				}*/

				if (columnIndex == 3) {
					//if(getValueAt(0, 3) != null){
					return getValueAt(0, 3).getClass();
					// }	
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}
			return super.getColumnClass(columnIndex);
		}

		/*public Class getColumnClass(int c) 
		{     
		for(int rowIndex = 0; rowIndex < data.size(); rowIndex++)
		{
		    Object[] row = data.get(rowIndex);
		    if (row[c] != null) {
		        return getValueAt(rowIndex, c).getClass();
		    }   
		}
		return String.class;
		}*/

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	//Facility Approved products JTable for REGIMEN only
	private static final String[] columns_arvfacilityApprovedproducts = {
			"Product Code", "Pack size", "Product name", "Quantity Dispensed" };
	private static final Object[] defaultv_arvfacilityapprovedproducts = { "",
			"", "", "" };
	private static final int rows_arvfproducts = 0;
	public static TableModel tableModel_arvfproducts = new TableModel(
			columns_arvfacilityApprovedproducts,
			defaultv_arvfacilityapprovedproducts, rows_arvfproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		//@Override
		public Class<?> getColumnClass(int columnIndex) {
			/*if (columnIndex == 4) {

				return getValueAt(0, 4).getClass();
			}*/

			if (columnIndex == 3) {

				return Integer.class;
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/

	};

	//Facility Approved products JTable for BARCODE SCANNER REGIMEN only
	private static final String[] columns_arvbarcodefacilityApprovedproducts = {
			"Product Code", "Pack size", "Product name", "Quantity Dispensed" };
	private static final Object[] defaultv_arvbarcodefacilityapprovedproducts = {
			"", "", "", "" };
	private static final int rows_arvbarcodefproducts = 0;
	public static TableModel tableModel_arvbarcodefproducts = new TableModel(
			columns_arvbarcodefacilityApprovedproducts,
			defaultv_arvbarcodefacilityapprovedproducts,
			rows_arvbarcodefproducts) {

		boolean[] canEdit = new boolean[] { false, false, false, true };

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2) {

				return getValueAt(0, 2).getClass();
			}

			if (columnIndex == 3) {

				return getValueAt(0, 3).getClass();
			}
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/
	};

	private String mydateformat = "yyyy-MM-dd hh:mm:ss";
	public Timestamp productdeliverdate;
	private Timestamp shipmentdate;
	public String oldDateString;
	public String changeregimenartnumber;
	public String newDateString;
	private ARVDispensingDAO callarvdispenser;
	private Elmis_Patient elmis_patient;
	List<VW_Program_Facility_ApprovedProducts> productsList = new LinkedList();
	List<VW_Program_Facility_ApprovedProducts> arvproductsList = new LinkedList();
	public static List<VW_Program_Facility_ApprovedProducts> arvsearchproductsList = new LinkedList();
	List<String> regimenList = new LinkedList();
	private int count = 0;
	private String SearchResult = "";
	private static ListIterator<VW_Program_Facility_ApprovedProducts> fapprovedproductsIterator;
	private static Iterator regimenIterator;
	private VW_Program_Facility_ApprovedProducts facilitysccProducts;
	ArrayList<Elmis_Dar_Transactions> elmisstockcontrolcardList = new ArrayList<Elmis_Dar_Transactions>();
	FacilityProductsDAO Facilityproductsmapper = null;
	FacilitySetUpDAO callfacility;
	Facility_Types facilitytype = null;
	Facility facility = null;
	Elmis_Dar_Transactions elmis_dar_transactions;
	Elmis_Dar_Transactions saveelmis_dar_transactions;
	public String typeCode;
	public String facilityprogramcode;
	public String facilityproductsource;
	public String facilitytypeCode;
	private String adjustmentname = "";
	private String Pcode = "";
	private int colindex = 0;
	private int rowindex = 0;
	private Integer PACKSIZE;
	private Double Convert_tablet_to_bottle;
	public boolean artfound;
	public String pcodeRegimen;
	public String pnameRegimen;
	public String pstrengthRegimen;
	public String regimen;
	public String ppacksize;
	public static String barcodeproductinsert;
	private String searchText = "";
	private char letter;
	private List<Character> charList = new LinkedList();
	public static int selectedProductID;
	public static VW_Program_Facility_ApprovedProducts selectedProduct;
	public static String barcodeScanned;
	private ProductQty arvproductqty;
	private Integer qty;
	private String arvdispenseprodcode;
    private Double abovezero = 0.0;
    private String setdispenserart = "";


public ARTdispenseJP(String dispenseartumber) {
		initComponents();
		setdispenserart = dispenseartumber;
		System.out.println(setdispenserart + " I have been set here new ArT");
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jPanel2 = new javax.swing.JPanel();
		deleteItemLineJBtn = new javax.swing.JButton();
		jScrollPane1 = new javax.swing.JScrollPane();
		changeregimenJT = new javax.swing.JTable();
		searchJL = new javax.swing.JLabel();
		searchjTextField = new javax.swing.JTextField();
		jButton1 = new javax.swing.JButton();
		dispenseJBtn = new javax.swing.JButton();
		jScrollPane2 = new javax.swing.JScrollPane();
		ARVSJT = new javax.swing.JTable();
		artdispenseprodqtyJL = new java.awt.Label();
		dispenserart = new java.awt.Label();  
		addComponentListener(new java.awt.event.ComponentAdapter() {
			public void componentShown(java.awt.event.ComponentEvent evt) {
				formComponentShown(evt);
			}
		});

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));
		jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		deleteItemLineJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		deleteItemLineJBtn.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS delete role small.png"))); // NOI18N
		deleteItemLineJBtn.setText("Delete selected item");
		deleteItemLineJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						deleteItemLineJBtnActionPerformed(evt);
					}
				});
		
		JLabel dispenserart = new JLabel("");

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(deleteItemLineJBtn)
					.addGap(35)
					.addComponent(dispenserart, GroupLayout.DEFAULT_SIZE, 240, Short.MAX_VALUE)
					.addContainerGap())
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGroup(jPanel2Layout.createParallelGroup(Alignment.TRAILING)
						.addComponent(dispenserart, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(deleteItemLineJBtn, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		jPanel2.setLayout(jPanel2Layout);

		changeregimenJT.setFont(new java.awt.Font("Ebrima", 0, 24));
		changeregimenJT.setModel(tableModel_arvfproducts);
		changeregimenJT.setRowHeight(20);
		changeregimenJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						changeregimenJTPropertyChange(evt);
					}
				});
		jScrollPane1.setViewportView(changeregimenJT);

		searchJL.setFont(new java.awt.Font("Ebrima", 1, 12));
		searchJL.setForeground(new java.awt.Color(255, 255, 255));
		searchJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view user small.png"))); // NOI18N
		searchJL.setText("Search");

		searchjTextField.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				searchjTextFieldActionPerformed(evt);
			}
		});
		searchjTextField
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						searchjTextFieldPropertyChange(evt);
					}
				});
		searchjTextField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				searchjTextFieldKeyReleased(evt);
			}

			public void keyTyped(java.awt.event.KeyEvent evt) {
				searchjTextFieldKeyTyped(evt);
			}
		});

		jButton1.setFont(new java.awt.Font("Ebrima", 1, 12));
		jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS cancel md.png"))); // NOI18N
		jButton1.setText("Close");
		jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jButton1MouseClicked(evt);
			}
		});
		jButton1.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton1ActionPerformed(evt);
			}
		});

		dispenseJBtn.setFont(new java.awt.Font("Ebrima", 1, 12));
		dispenseJBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS dispense md.png"))); // NOI18N
		dispenseJBtn.setText("Dispense");
		dispenseJBtn.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				dispenseJBtnMouseClicked(evt);
			}
		});
		dispenseJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				dispenseJBtnActionPerformed(evt);
			}
		});

		ARVSJT.setFont(new java.awt.Font("Ebrima", 0, 24));
		ARVSJT.setModel(tableModel_fproducts);
		ARVSJT.setRowHeight(30);
		ARVSJT.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				ARVSJTMouseClicked(evt);
			}
		});
		ARVSJT
				.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
					public void propertyChange(
							java.beans.PropertyChangeEvent evt) {
						ARVSJTPropertyChange(evt);
					}
				});
		jScrollPane2.setViewportView(ARVSJT);

		artdispenseprodqtyJL.setFont(new Font("Ebrima", Font.PLAIN, 18));
		artdispenseprodqtyJL.setForeground(new java.awt.Color(255, 255, 255));

		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1.setLayout(jPanel1Layout);
		jPanel1Layout
				.setHorizontalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING)
														.addComponent(
																artdispenseprodqtyJL,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																460,
																Short.MAX_VALUE)
														.addComponent(
																jPanel2,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																javax.swing.GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addGroup(
																javax.swing.GroupLayout.Alignment.TRAILING,
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jButton1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				135,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				dispenseJBtn))
														.addComponent(
																jScrollPane1,
																0, 0,
																Short.MAX_VALUE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																false)
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addGap(
																				60,
																				60,
																				60)
																		.addComponent(
																				searchJL)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
																		.addComponent(
																				searchjTextField))
														.addGroup(
																jPanel1Layout
																		.createSequentialGroup()
																		.addGap(
																				50,
																				50,
																				50)
																		.addComponent(
																				jScrollPane2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				405,
																				javax.swing.GroupLayout.PREFERRED_SIZE)))
										.addContainerGap()));
		jPanel1Layout
				.setVerticalGroup(jPanel1Layout
						.createParallelGroup(
								javax.swing.GroupLayout.Alignment.LEADING)
						.addGroup(
								jPanel1Layout
										.createSequentialGroup()
										.addGap(50, 50, 50)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.TRAILING)
														.addGroup(
																javax.swing.GroupLayout.Alignment.LEADING,
																jPanel1Layout
																		.createSequentialGroup()
																		.addComponent(
																				jPanel2,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				javax.swing.GroupLayout.DEFAULT_SIZE,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				javax.swing.LayoutStyle.ComponentPlacement.RELATED)
																		.addComponent(
																				artdispenseprodqtyJL,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				59,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addComponent(
																				jScrollPane1,
																				javax.swing.GroupLayout.PREFERRED_SIZE,
																				198,
																				javax.swing.GroupLayout.PREFERRED_SIZE)
																		.addGap(
																				18,
																				18,
																				18)
																		.addGroup(
																				jPanel1Layout
																						.createParallelGroup(
																								javax.swing.GroupLayout.Alignment.BASELINE)
																						.addComponent(
																								jButton1)
																						.addComponent(
																								dispenseJBtn)))
														.addComponent(
																jScrollPane2,
																0, 0,
																Short.MAX_VALUE))
										.addPreferredGap(
												javax.swing.LayoutStyle.ComponentPlacement.RELATED)
										.addGroup(
												jPanel1Layout
														.createParallelGroup(
																javax.swing.GroupLayout.Alignment.BASELINE)
														.addComponent(
																searchjTextField,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																35,
																javax.swing.GroupLayout.PREFERRED_SIZE)
														.addComponent(
																searchJL,
																javax.swing.GroupLayout.PREFERRED_SIZE,
																32,
																javax.swing.GroupLayout.PREFERRED_SIZE))
										.addContainerGap(34, Short.MAX_VALUE)));

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 955, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(23, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 618, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		this.setLayout(layout);
	}// </editor-fold>
	//GEN-END:initComponents

	private void searchjTextFieldActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		System.out.println(searchjTextField.getText());
	}

	@SuppressWarnings("static-access")
	private void searchjTextFieldKeyReleased(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		//last event is enter after barcode scan event
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			/*javax.swing.JOptionPane.showMessageDialog(null,
					"Check if went inside HERE");*/
			/*    letter = evt.getKeyChar();

			StringBuilder s = new StringBuilder(charList.size());
			
			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			
			}*/

			//if((s.substring(0,1).matches("[0-9]"))){ 
			//get the scanned barcode number
			this.barcodeScanned = searchjTextField.getText();

			/*selectedProduct = Facilityproductsmapper
					.barcodeselectProduct(barcodeScanned);*/
			//Get ARV facility Approved products only iterate through them to get scanned product
			callfacility = new FacilitySetUpDAO();
			Facilityproductsmapper = new FacilityProductsDAO();

			this.facilityprogramcode = "ARV";
			//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
			//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

			facility = callfacility.getFacility();

			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();

			productsList = Facilityproductsmapper
					.dogetcurrentFacilityApprovedProducts(facilityprogramcode,
							typeCode);
			arvsearchproductsList = productsList;

			for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

				System.out.println(p.getCode());

			}

			for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {
				try {
					if (p.getCode().contains(barcodeScanned)) {
						selectedProduct = p;

						javax.swing.JOptionPane.showMessageDialog(null,
								selectedProduct.getCode());
					}

				} catch (NullPointerException e) {
					e.getMessage();
				}
			}

			//product found via barcode scanner
			if (this.selectedProduct != null) {

				//if qty in stock is zero , show message dialog 
				//qty = this.selectedProduct.getProductQty();

				qty = this.Facilityproductsmapper
						.barcodeselectProductQty(selectedProduct.getCode());

				//javax.swing.JOptionPane.showMessageDialog(null, qty
					//	+ "Product QUANTITY");

				//if 
				if (qty != 0) {

					selectedProduct.setProductqty(qty);
					// show the number pad to get quantity
					new arvdispenseNumberPadJD(javax.swing.JOptionPane
							.getFrameForComponent(this), true,
							this.selectedProduct);

				} else {

					javax.swing.JOptionPane
							.showMessageDialog(
									this,
									"<html><body bgcolor=\"#E6E6FA\"><b><p><p><p><p><font size=\"4\" color=\"red\">"
											+ ""
											+ this.selectedProduct
													.getPrimaryname()
											+ " is currently stocked out. </font><p><p><p><p></b></body></html>");
				}

				searchjTextField.setText("");

				if (searchjTextField.getText().equals("")) {

					charList.clear();
				}
				searchjTextField.requestFocusInWindow();

			} else {

				javax.swing.JOptionPane.showMessageDialog(null,
						"Product not found" + "SCANNED BAR CODE!!");
			}

			searchjTextField.setText("");

		}

	}

	@SuppressWarnings("static-access")
	private void searchjTextFieldKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		System.out.println(this.arvsearchproductsList.size() + "list not null");

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
			searchText = searchjTextField.getText();
           System.out.println("I detedcted a back space ah! did you call me ??");
           //re populate table with original list 
           this.callbackARVproductlist();
           
           if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}

			for (char c : charList) {

				s.append(c);

			}
		} else {

			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			//get ARV list 
			try {
				if (!(this.searchjTextField.getText() == "")) {

					if (!(s.substring(0, 1).matches("[0-9]"))) {

						callfacility = new FacilitySetUpDAO();
						Facilityproductsmapper = new FacilityProductsDAO();

						this.facilityprogramcode = "ARV";
						//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
						//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

						facility = callfacility.getFacility();

						facilitytype = callfacility
								.selectAllwithTypes(facility);
						typeCode = facilitytype.getCode();

						productsList = Facilityproductsmapper
								.dogetcurrentFacilityApprovedProducts(
										facilityprogramcode, typeCode);
						arvsearchproductsList = productsList;

						/*for(VW_Program_Facility_ApprovedProducts p: arvsearchproductsList ){
							
							if(p.getPrimaryname().contains(s)){
								this.arvproductsList.add(p);
							}
						}*/

						for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

							if (p.getPrimaryname().contains(s)) {
								this.arvproductsList.add(p);
							}
						}

						//	System.out.println("Delete " + s);
						//	arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

						this.searchPopulateProgram_ProductTable(
								arvproductsList, "STRING_FOUND");

					}
				}

			} catch (NullPointerException e) {
				e.getMessage();
			}

		}

		// searchText += Character.toString(letter);
		/*	charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			try {
				//check if char is a digit and escape the loop.
				if(!(this.searchjTextField.getText() == "")){
				if(!(s.substring(0,1).matches("[0-9]"))){ 

				callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper.dogetcurrentFacilityApprovedProducts(facilityprogramcode, typeCode);
					
				arvsearchproductsList = productsList;

				for (VW_Program_Facility_ApprovedProducts p : arvsearchproductsList) {

					if (p.getPrimaryname().contains(s)) {
						this.arvproductsList.add(p);
					}
				}
				//System.out.println("Delete " + s);
				//arvproductsList = Facilityproductsmapper.quickfiltersearchselectProduct( s.toString());

				searchPopulateProgram_ProductTable(arvproductsList,
						"STRING_FOUND");
				
				}
				
			}
			} catch (NullPointerException e) {
				e.getMessage();
			}*/

	}
	
	private void callbackARVproductlist(){
		//get ARV list 
		try {
			if (!(this.searchjTextField.getText() == "")) {

		
					callfacility = new FacilitySetUpDAO();
					Facilityproductsmapper = new FacilityProductsDAO();

					this.facilityprogramcode = "ARV";
				    facility = callfacility.getFacility();

					facilitytype = callfacility
							.selectAllwithTypes(facility);
					typeCode = facilitytype.getCode();

					productsList = Facilityproductsmapper
							.dogetcurrentFacilityApprovedProducts(
									facilityprogramcode, typeCode);
					arvsearchproductsList = productsList;

					this.searchPopulateProgram_ProductTable(
							arvsearchproductsList, "STRING_FOUND");

				
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}

	private void jButton1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		// TODO add your handling code here:
		//this.setVisible(false);
		//clear table model 
		//tableModel_arvfproducts.clearTable();
		Window w = SwingUtilities.getWindowAncestor(ARTdispenseJP.this);
		w.setVisible(false);

	}

	private void searchjTextFieldPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		String value = searchjTextField.getText();

		for (int row = 0; row <= this.ARVSJT.getRowCount() - 1; row++) {

			for (int col = 0; col <= this.ARVSJT.getColumnCount() - 1; col++) {

				if (value.equals(this.ARVSJT.getValueAt(row, col))) {

					// this will automatically set the view of the scroll in the location of the value
					this.ARVSJT.scrollRectToVisible(this.ARVSJT.getCellRect(
							row, 0, true));

					// this will automatically set the focus of the searched/selected row/value
					this.ARVSJT.setRowSelectionInterval(row, row);

					for (int i = 0; i <= this.ARVSJT.getColumnCount() - 1; i++) {

						this.ARVSJT.getColumnModel().getColumn(i)
								.setCellRenderer(new HighlightRenderer());
					}
				}
			}
		}

	}

	private class HighlightRenderer extends DefaultTableCellRenderer {

		@Override
		public Component getTableCellRendererComponent(JTable table,
				Object value, boolean isSelected, boolean hasFocus, int row,
				int column) {

			// everything as usual
			super.getTableCellRendererComponent(table, value, isSelected,
					hasFocus, row, column);

			// added behavior
			if (row == table.getSelectedRow()) {

				// this will customize that kind of border that will be use to highlight a row
				setBorder(BorderFactory.createMatteBorder(2, 1, 2, 1,
						Color.BLACK));
			}

			return this;
		}
	}

	
	private boolean checktableCellinput(String n){
		
		 try { 
		        Integer.parseInt(n) ;
		    } catch(NumberFormatException e) { 
		        return false; 
		        
		    }
		    // only got here if we didn't return false
		    return true;
}
	private void changeregimenJTPropertyChange(
			java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:
		// TODO add your handling code here:
		this.changeregimenJT.getColumnModel().getColumn(0).setMinWidth(0);
		this.changeregimenJT.getColumnModel().getColumn(0).setMaxWidth(0);
		this.changeregimenJT.getColumnModel().getColumn(0).setWidth(0);

		this.changeregimenJT.getColumnModel().getColumn(1).setMinWidth(0);
		this.changeregimenJT.getColumnModel().getColumn(1).setMaxWidth(0);
		this.changeregimenJT.getColumnModel().getColumn(1).setWidth(0);

		if ("tableCellEditor".equals(evt.getPropertyName())) {

			//saveShipped_Line_Items = new Shipped_Line_Items();
			if (this.changeregimenJT.isColumnSelected(3)) {
				if (this.changeregimenJT.isEditing()) {
					System.out.println("THIS CELL HAS STARTED EDITING");
					//get column index
					//letter =  evt.get;
				
					colindex = this.changeregimenJT.getSelectedColumn();
					rowindex = this.changeregimenJT.getSelectedRow();
					Pcode = tableModel_arvfproducts.getValueAt(
							this.changeregimenJT.getSelectedRow(), 0)
							.toString();

					System.out.println(colindex);
					System.out.println(rowindex);
					System.out.println(Pcode);

				} else if (!this.changeregimenJT.isEditing()) {
					
					String chars
					 = tableModel_arvfproducts.getValueAt(
								this.changeregimenJT.getSelectedRow(),3).toString();
	              					
						if (!(chars.substring(0, 1).matches("[0-9]"))) {	
							
							if(!(checktableCellinput((chars.substring(0, 1))))){
								
						    	JOptionPane.showMessageDialog(null, String.format("Quantity Dispensed must be a positive number", chars.substring(0, 1)));	
						    	
						    }
							
						}
						
						

					try {
						createdartransaction(Pcode);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					System.out.println("THIS CELL IS NOT EDITING");

				}

			}
		}

	}

	private Timestamp clientregisterdate() throws ParseException {
		try {
			DateFormat dateFormat = new SimpleDateFormat(
					"EEE MMM d HH:mm:ss z yyyy");
			Date date = new Date();
			dateFormat.format(date);

			String mydate = dateFormat.format(date);
			System.out.println(mydate);
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";

			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			productdeliverdate = Timestamp.valueOf(newDateString);

			System.out.println(newDateString);

		} catch (NullPointerException e) {
			e.getMessage();
		}
		return productdeliverdate;
	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();

		String Idstring = uuid.toString();
		return Idstring;

	}

	public void createdartransaction(String apcode) throws ParseException {
		//System.out.println("Did object call me ???");

		elmis_dar_transactions = new Elmis_Dar_Transactions();

		Double ARVbottle = 0.0;

		elmis_dar_transactions.setId(this.GenerateGUID());
		elmis_dar_transactions.setProductcode(apcode);

		this.PACKSIZE = Integer.parseInt(tableModel_arvfproducts.getValueAt(
				rowindex, 1).toString());
		Convert_tablet_to_bottle = Double.parseDouble(tableModel_arvfproducts
				.getValueAt(rowindex, 3).toString());

		if (!(this.PACKSIZE == null)) {
			if (!(Convert_tablet_to_bottle == null)) {

				ARVbottle = Convert_tablet_to_bottle / PACKSIZE;
				elmis_dar_transactions.setNo_equiv_bottles(ARVbottle);

			}

		}
		elmis_dar_transactions
				.setNo_tablets_dispensed(Convert_tablet_to_bottle);
		elmis_dar_transactions.setTransaction_date(clientregisterdate());

		changeregimenartnumber = ARTNumberJP.searchedARTnumber;

		if (!(this.changeregimenartnumber == null)) {
			elmis_dar_transactions.setArt_number(changeregimenartnumber);

		}
		//created linked list object 
         //print out object here 
		System.out.println("~~~~~ get set go ~~~~~~ ");
		System.out.println(elmis_dar_transactions.getPrimaryname());
		System.out.println(elmis_dar_transactions.getProductcode());
		System.out.println("~~~~~ rest ~~~~~~ ");
		//end print object 
		
		elmisstockcontrolcardList.add(elmis_dar_transactions);
		System.out.println(elmisstockcontrolcardList.size() + "ARRAY SIZE???");
		System.out.println("Array size here &&&");
		elmis_dar_transactions = new Elmis_Dar_Transactions();

	}

	private void ARVSJTPropertyChange(java.beans.PropertyChangeEvent evt) {
		// TODO add your handling code here:

		// TODO add your handling code here:
		this.ARVSJT.getColumnModel().getColumn(0).setMinWidth(0);
		this.ARVSJT.getColumnModel().getColumn(0).setMaxWidth(0);
		this.ARVSJT.getColumnModel().getColumn(0).setWidth(0);
		if ("tableCellEditor".equals(evt.getPropertyName())) {

			//saveShipped_Line_Items = new Shipped_Line_Items();
			//if (this.ARVSJT.isColumnSelected(3)) {
			if (this.ARVSJT.isEditing()) {
				System.out.println("THIS CELL HAS STARTED EDITING");
				//get column index
				colindex = this.ARVSJT.getSelectedColumn();
				rowindex = this.ARVSJT.getSelectedRow();
				Pcode = tableModel_fproducts.getValueAt(
						this.ARVSJT.getSelectedRow(), 0).toString();
				System.out.println(colindex);
				System.out.println(rowindex);
				System.out.println(Pcode);

			} else if (!this.ARVSJT.isEditing()) {

				//createshipmentObj(Pcode, rnrid, shipmentdate);

				System.out.println("THIS CELL IS NOT EDITING");

			}

		}

	}

	private void ARVSJTMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		//call my keypad if row is selected 

		if (!(SearchResult == "STRING_FOUND")) {

			if (this.ARVSJT.isRowSelected(this.ARVSJT.getSelectedRow())) {

				pcodeRegimen = (tableModel_fproducts.getValueAt(ARVSJT
						.getSelectedRow(), 0).toString());
				pnameRegimen = (tableModel_fproducts.getValueAt(ARVSJT
						.getSelectedRow(), 1).toString());

				pstrengthRegimen = (tableModel_fproducts.getValueAt(ARVSJT
						.getSelectedRow(), 2).toString());
				ppacksize = (tableModel_fproducts.getValueAt(ARVSJT
						.getSelectedRow(), 3).toString());

				@SuppressWarnings("unused")
				String[] list = { pcodeRegimen, ppacksize, pnameRegimen };

				//regimenList.add(pcodeRegimen);
				//regimenList.add(ppacksize);
				regimenList.add(pnameRegimen);
				count = count + regimenList.size();
				System.out.println(regimenList.size());
				System.out.println("SIZE of COUNT" + count);

				this.PopulateProgram_ChangeregimenTable(list);
				//pnameRegimen = "";

			}
		} else if (SearchResult == "STRING_FOUND") {

			if (this.ARVSJT.isRowSelected(this.ARVSJT.getSelectedRow())) {

				pcodeRegimen = (tableModel_searchfproducts.getValueAt(ARVSJT
						.getSelectedRow(), 0).toString());
				pnameRegimen = (tableModel_searchfproducts.getValueAt(ARVSJT
						.getSelectedRow(), 1).toString());

				pstrengthRegimen = (tableModel_searchfproducts.getValueAt(
						ARVSJT.getSelectedRow(), 2).toString());
				ppacksize = (tableModel_searchfproducts.getValueAt(ARVSJT
						.getSelectedRow(), 3).toString());

				@SuppressWarnings("unused")
				String[] list = { pcodeRegimen, ppacksize, pnameRegimen };

				//regimenList.add(pcodeRegimen);
				//regimenList.add(ppacksize);
				regimenList.add(pnameRegimen);

				count = count + regimenList.size();
				System.out.println(regimenList.size());
				System.out.println("SIZE of COUNT" + count);

				this.PopulateProgram_ChangeregimenTable(list);
				//pnameRegimen = "";

			}
		}
		//this.ARVSJT.

		/*	pcodeRegimen = (tableModel_fproducts.getValueAt(
					ARVSJT.getSelectedRow(), 0).toString());
			pnameRegimen = (tableModel_fproducts.getValueAt(
					ARVSJT.getSelectedRow(), 1).toString());

			pstrengthRegimen = (tableModel_fproducts.getValueAt(ARVSJT
					.getSelectedRow(), 2).toString());

			regimenList.add(pnameRegimen);
			count = count + regimenList.size();
			System.out.println(regimenList.size());
			System.out.println("SIZE of COUNT" + count);*/

		//this.PopulateProgram_ChangeregimenTable(regimenList);
		//pnameRegimen = "";

	}

	private void formComponentShown(java.awt.event.ComponentEvent evt) {
		// TODO add your handling code here:
		// TODO add your handling code here:
		System.out.println(setdispenserart + " ~~~~~~ get reay to be set");
	  this.dispenserart.setText(setdispenserart);
		searchjTextField.requestFocusInWindow();
		tableModel_fproducts.clearTable();
		Facilityproductsmapper = new FacilityProductsDAO();
		PopulateProgram_ProductTable(productsList);
		this.ARVSJT.getTableHeader()
				.setFont(new Font("Ebrima", Font.PLAIN, 22));
		this.changeregimenJT.getTableHeader()
		.setFont(new Font("Ebrima", Font.PLAIN, 22));
		this.changeregimenJT.setRowHeight(30);
	}

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	@SuppressWarnings("unchecked")
	private void PopulateProgram_ProductTable(List fapprovProducts) {

		try {

			callfacility = new FacilitySetUpDAO();
			Facilityproductsmapper = new FacilityProductsDAO();

			this.facilityprogramcode = "ARV";
			//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
			//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

			facility = callfacility.getFacility();

			facilitytype = callfacility.selectAllwithTypes(facility);
			typeCode = facilitytype.getCode();

			String dbdriverStatus = System.getProperty("dbdriver");

			if (!(dbdriverStatus == "org.hsqldb.jdbcDriver")) {

				productsList = Facilityproductsmapper
						.dogetcurrentFacilityApprovedProducts(facilityprogramcode,typeCode);

				arvsearchproductsList = productsList;
			} else {
				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProductshsqldb(typeCode);

				arvsearchproductsList = productsList;
			}

			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsList) {
				System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				//System.out.println(p.getStockinhand());
				//System.out.println(p.getRnrid());
				//System.out.println(p.getCreateddate());

			}

			tableModel_fproducts.clearTable();

			fapprovedproductsIterator = productsList.listIterator();
			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");

				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getStrength().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getPacksize();
				//	defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
				/*	TableColumn column1 = facilityapprovedProductsJT
							.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());*/

				//defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_fproducts.insertRow(cols);

				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	@SuppressWarnings("unchecked")
	private void searchPopulateProgram_ProductTable(List fapprovProducts,
			String mysearch) {

		try {

			this.SearchResult = mysearch;

			/*	callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProducts(facilityprogramcode);*/
			for (@SuppressWarnings("unused")
			VW_Program_Facility_ApprovedProducts p : productsList) {
				/*System.out.println(p.getPrimaryname().toString());
				System.out.println(p.getCode().toString());
				System.out.println(p.getStockinhand());
				System.out.println(p.getRnrid());
				System.out.println(p.getCreateddate());*/

			}

			this.ARVSJT.setModel(tableModel_searchfproducts);
			tableModel_searchfproducts.clearTable();

			fapprovedproductsIterator = fapprovProducts.listIterator();

			while (fapprovedproductsIterator.hasNext()) {

				facilitysccProducts = fapprovedproductsIterator.next();

				//System.out.print(facilitysccProducts + "");

				defaultv_facilityapprovedproducts[0] = facilitysccProducts
						.getCode().toString();

				defaultv_facilityapprovedproducts[1] = facilitysccProducts
						.getPrimaryname().toString();
				defaultv_facilityapprovedproducts[2] = facilitysccProducts
						.getStrength().toString();
				defaultv_facilityapprovedproducts[3] = facilitysccProducts
						.getPacksize();
				//	defaultv_facilityapprovedproducts[4] = this.tableModel_fproducts.add(expiryJDate);
				/*	TableColumn column1 = facilityapprovedProductsJT
							.getColumnModel().getColumn(4);
					column1.setCellRenderer(new JDateChooserRenderer());
					column1.setCellEditor(new JDateChooserCellEditor());*/

				//defaultv_facilityapprovedproducts[4] = column1;

				ArrayList cols = new ArrayList();
				for (int j = 0; j < columns_facilityApprovedproducts.length; j++) {
					cols.add(defaultv_facilityapprovedproducts[j]);

				}

				tableModel_searchfproducts.insertRow(cols);
				//tableModel_searchfproducts.fireTableDataChanged();
				fapprovedproductsIterator.remove();
			}
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}

	}

	@SuppressWarnings( { "unused", "unchecked", "static-access" })
	public void PopulateProgram_ChangeregimenTable(String[] myArray) {

		if (!(this.barcodeproductinsert == "BARCODE_CALL")) {
			try {

				callarvdispenser = new ARVDispensingDAO();

				arvdispenseprodcode = myArray[0].toString();

				arvproductqty = callarvdispenser
						.doselectqtyarvdispense(arvdispenseprodcode,AppJFrame.getDispensingId("ARV"));
				if (!(arvproductqty.getQty().toString() == null)) {
					this.artdispenseprodqtyJL
							.setText("Selected Product Quantity in Stock  = "

							+ arvproductqty.getQty().toString() + " " +"bottles");
					abovezero = arvproductqty.getQty();
				}

				/*callfacility = new facilitysetupsessionsmappercalls();
				Facilityproductsmapper = new facilityproductssetupsessionmapperscalls();

				this.facilityprogramcode = "ARV";
				//this.facilityproductsource = ProductsSourceJP.selectedproductsource;
				//this.facilitytypeCode = SystemSettingJD.selectedfacilitytypecode;

				facility = callfacility.getFacility();

				facilitytype = callfacility.selectAllwithTypes(facility);
				typeCode = facilitytype.getCode();

				productsList = Facilityproductsmapper
						.dogetARVcurrentFacilityApprovedProducts(facilityprogramcode);*/
				//regimenIterator = myArray.iterator();
				//tableModel_arvfproducts.clearTable();
				
				for (int i = 0; i <= myArray.length - 1; i++) {
				  if(!(abovezero < 0)){


					defaultv_arvfacilityapprovedproducts[0] = myArray[i]
							.toString();
					defaultv_arvfacilityapprovedproducts[1] = myArray[i + 1]
							.toString();

					defaultv_arvfacilityapprovedproducts[2] = myArray[i + 2]
							.toString();

					//while (regimenIterator.hasNext()) {

					//defaultv_arvfacilityapprovedproducts[2] = regimenIterator
					//.next();

					ArrayList cols = new ArrayList();

					//cols.add(regimenlist);
					for (int j = 0; j < columns_arvfacilityApprovedproducts.length; j++) {
						cols.add(defaultv_arvfacilityapprovedproducts[j]);

					}

					//DefaultTableModel model = (DefaultTableModel) this.ARVSJT
					//	.getModel();
					//	model.addRow(new Object[] { regimenlist, "" });

					tableModel_arvfproducts.insertRow(cols);
					regimenIterator.remove();
					}else{
						JOptionPane.showMessageDialog(this,
								"Products Below minimum , Can't be dispensed!", "Product Stock status",
								JOptionPane.INFORMATION_MESSAGE);
						
					}
				}
			} catch (NullPointerException e) {
				//do something here 
				e.getMessage();
			}
		} else {

			try {

				//fetch the product quantity of added product.

				callarvdispenser = new ARVDispensingDAO();

				arvdispenseprodcode = myArray[0].toString();

				arvproductqty = callarvdispenser
						.doselectqtyarvdispense(arvdispenseprodcode,AppJFrame.getDispensingId("ARV"));
				if (!(arvproductqty.getQty().toString() == null)) {
					this.artdispenseprodqtyJL
							.setText("Selected Product Quantity in Stock  = "
									+ arvproductqty.getQty().toString() +  " "+ "bottles");
				}
				for (int i = 0; i <= myArray.length - 1; i++) {
					
					
					if(!(abovezero < 0)){

					defaultv_arvfacilityapprovedproducts[0] = myArray[i]
							.toString();
					defaultv_arvfacilityapprovedproducts[1] = myArray[i + 1]
							.toString();

					defaultv_arvfacilityapprovedproducts[2] = myArray[i + 2]
							.toString();

					ArrayList cols = new ArrayList();

					//cols.add(regimenlist);
					for (int j = 0; j < columns_arvfacilityApprovedproducts.length; j++) {
						cols.add(defaultv_arvfacilityapprovedproducts[j]);

					}

					tableModel_arvfproducts.insertRow(cols);
					regimenIterator.remove();
					}else{
						JOptionPane.showMessageDialog(this,
								"Products Below minimum , Can't be dispensed!", "Product Stock status",
								JOptionPane.INFORMATION_MESSAGE);
				}
				}
			} catch (NullPointerException e) {
				//do something here 
				e.getMessage();
			}

		}

	}

	private void dispenseJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
	}

	private void dispenseJBtnMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
		String checkqty = "";
		checkqty =   tableModel_arvfproducts.getValueAt(
				this.changeregimenJT.getSelectedRow(),3).toString();
		this.changeregimenJT.editCellAt(-1,-1);
		
		if(!(checkqty.equals("")))	{
		
		int ok = new arvmessageDialog()
		.showDialog(
				this,
				 "Do  you want to save the Dispensed Quantities to the database\n\n",
				"Saving Quantity Dispensed",
				"YES, To \n Save",
				"NO, To\n Edit the Quantity Entered ");
	
		/*int confirm = javax.swing.JOptionPane.showConfirmDialog(this,
				"Do  you want to save the Received Quantities to the database\n\n"
						+ "Yes - to Save\n\n"
						+ "No -to Edit the Quantity Entered\n\n"
						+ "Cancel - to exit Dialog",
				"Saving Quantity Received Information",
				JOptionPane.YES_NO_OPTION);*/
		callarvdispenser = new ARVDispensingDAO();
		if (ok == JOptionPane.YES_OPTION) {
			//Insert the object to database
			saveelmis_dar_transactions = new Elmis_Dar_Transactions();
			for (@SuppressWarnings("unused")
			Elmis_Dar_Transactions sp : elmisstockcontrolcardList) {

				saveelmis_dar_transactions.setId(sp.getId());
				saveelmis_dar_transactions.setNo_tablets_dispensed(sp
						.getNo_tablets_dispensed());
				saveelmis_dar_transactions.setNo_equiv_bottles(sp
						.getNo_equiv_bottles());
				saveelmis_dar_transactions.setProductcode(sp.getProductcode());
				saveelmis_dar_transactions.setArt_number(sp.getArt_number());
				saveelmis_dar_transactions.setTransaction_date(sp
						.getTransaction_date());

			callarvdispenser.doupdatearvdispenseqty(sp.getProductcode(), sp						.getNo_equiv_bottles(),AppJFrame.getDispensingId("ARV"));
              //print out code and names
				System.out.println("~~~Last check ~~~~");
				System.out.println(saveelmis_dar_transactions.getPrimaryname());
				System.out.println(saveelmis_dar_transactions.getProductcode());
				System.out.println(saveelmis_dar_transactions.getNo_equiv_bottles());
				System.out.println("test end here~~");
				callarvdispenser
						.insertdispensedProducts(saveelmis_dar_transactions);
			}
			JOptionPane.showMessageDialog(this,
					"Products Dispensed from Dispensary!", "Updating Database",
					JOptionPane.INFORMATION_MESSAGE);
			
		} else if (ok == JOptionPane.NO_OPTION) {
			this.changeregimenJT.validate();
		}
		}else{
			//
				JOptionPane.showMessageDialog(this, "Quantity Dispensed field is empty",
						"No Changes made", JOptionPane.INFORMATION_MESSAGE);
			}
		
		tableModel_arvfproducts.clearTable();
		Window w = SwingUtilities.getWindowAncestor(ARTdispenseJP.this);
		w.setVisible(false);

	}

	@SuppressWarnings("static-access")
	private void deleteItemLineJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		if (this.changeregimenJT.isRowSelected(this.changeregimenJT
				.getSelectedRow())) {
			int rowindex = changeregimenJT.getSelectedRow();
			this.tableModel_arvfproducts.deleteRow(rowindex);
		}
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JTable ARVSJT;
	private java.awt.Label artdispenseprodqtyJL;
	private java.awt.Label dispenserart;
	private javax.swing.JTable changeregimenJT;
	private javax.swing.JButton deleteItemLineJBtn;
	private javax.swing.JButton dispenseJBtn;
	private javax.swing.JButton jButton1;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JScrollPane jScrollPane2;
	public static javax.swing.JLabel searchJL;
	private javax.swing.JTextField searchjTextField;
}