/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.model;

import java.util.List;

import org.elmis.facility.json.beans.LossAdjustments;

/**
 * @author MMwebaze
 *
 */
public class ReportLineItem {

	private String productCode;
	private int beginningBalance;
	private int quantityReceived;
	private int quantityDispensed;
	private List<LossAdjustments> lossesAndAdjustments;
	private int stockInHand;
	private int newPatientCount;
	private int quantityRequested;
	private int stockOutDays;
	/*private String remarks;*/
	private String reasonForRequestedQuantity;
	//private String emergencyOrder;
	public int getStockOutDays() {
		return stockOutDays;
	}
	@Override
	public String toString() {
		return "ReportLineItem [productCode=" + productCode
				+ ", beginningBalance=" + beginningBalance
				+ ", quantityReceived=" + quantityReceived
				+ ", quantityDispensed=" + quantityDispensed
				+ ", lossesAndAdjustments=" + lossesAndAdjustments
				+ ", stockInHand=" + stockInHand + ", newPatientCount="
				+ newPatientCount + ", quantityRequested=" + quantityRequested
				+ ", stockOutDays=" + stockOutDays
				+ ", reasonForRequestedQuantity=" + reasonForRequestedQuantity
				+ "]";
	}
	public void setStockOutDays(int stockOutDays) {
		this.stockOutDays = stockOutDays;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode.trim();
	}
	public int getBeginningBalance() {
		return beginningBalance;
	}
	public void setBeginningBalance(int beginningBalance) {
		this.beginningBalance = beginningBalance;
	}
	public int getQuantityReceived() {
		return quantityReceived;
	}
	public void setQuantityReceived(int quantityReceived) {
		this.quantityReceived = quantityReceived;
	}
	public int getQuantityDispensed() {
		return quantityDispensed;
	}
	public void setQuantityDispensed(int quantityDispensed) {
		this.quantityDispensed = quantityDispensed;
	}
	public List<LossAdjustments> getLossesAndAdjustments() {
		return lossesAndAdjustments;
	}
	public void setLossesAndAdjustments(List<LossAdjustments> lossesAndAdjustments) {
		this.lossesAndAdjustments = lossesAndAdjustments;
	}
	public int getStockInHand() {
		return stockInHand;
	}
	public void setStockInHand(int stockInHand) {
		this.stockInHand = stockInHand;
	}
	public int getNewPatientCount() {
		return newPatientCount;
	}
	public void setNewPatientCount(int newPatientCount) {
		this.newPatientCount = newPatientCount;
	}
	public int getQuantityRequested() {
		return quantityRequested;
	}
	public void setQuantityRequested(int quantityRequested) {
		this.quantityRequested = quantityRequested;
	}
	public String getReasonForRequestedQuantity() {
		return reasonForRequestedQuantity;
	}
	public void setReasonForRequestedQuantity(String reasonForRequestedQuantity) {
		this.reasonForRequestedQuantity = reasonForRequestedQuantity;
	}
}
