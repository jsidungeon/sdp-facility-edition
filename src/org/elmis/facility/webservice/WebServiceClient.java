package org.elmis.facility.webservice;

import java.util.ArrayList;

import org.elmis.facility.domain.model.Dosage_Units;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Approved_Products;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Processing_Periods;
import org.elmis.facility.domain.model.Product_categories;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Program_products;
import org.elmis.facility.domain.model.Programs;

public interface WebServiceClient {
	 public ArrayList<Products> fetchProducts() throws Exception;
	 public ArrayList<Programs> fetchPrograms() throws Exception;
	 public  ArrayList<Dosage_Units> fetchDosageUnits() throws Exception;
	 public  ArrayList<Facility> fatchFacilities() throws Exception;
	 public  ArrayList<Product_categories> fetchProductCategories() throws Exception;
	 public ArrayList<Facility_Approved_Products> fetchFacilityApprovedProducts() throws Exception;
	 public ArrayList<Program_products> fetchProgramproducts() throws Exception;
	 public  ArrayList<Facility_Types> fetchFacilityTypes() throws Exception;
	 public  ArrayList<Processing_Periods> fetchProcessingPeriods() throws Exception;
	 public  ArrayList<Losses_Adjustments_Types> fetchLossesAndAdjustmentTypes() throws Exception;
}
