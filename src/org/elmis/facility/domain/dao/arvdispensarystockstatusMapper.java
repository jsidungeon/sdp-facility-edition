package org.elmis.facility.domain.dao;

import java.util.HashMap;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.arvdispensarystockstatus;


public interface arvdispensarystockstatusMapper {
 


  
    int insert(arvdispensarystockstatus record);

   
    @SuppressWarnings("rawtypes")
	List<arvdispensarystockstatus>  getARVproductstatus(@Param("prodcode") String prodcode,@Param("dpid") Integer dpid);
    
     
   int  updateproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Double prodqty,  @Param ("dpid") Integer dpid);
   int  updateproductqtybyPC(@Param ("prodcode") String prodcode,@Param ("prodqty") Double prodqty, @Param ("dpid")  Integer dpid);
   
   int  setupdatedispenaryproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Integer prodqty,@Param ("dispensary") String dispensary);
   
   int  updatedispenaryproductqty(@Param ("prodcode") String prodcode,@Param ("prodqty") Double double1,@Param ("dispensary") String dispensary,@Param ("stockonhand_scc")double d);
   
   int updatedispenaryproductqtyless(@Param ("prodcode") String prodcode,@Param ("prodqty") Double positive,@Param ("dispensary") String dispensary,@Param ("stockonhand_scc")double d,
		   @Param ("dpid") Integer  dpid);
 
   
    int updateelmishivsitebalances(@Param("record") HashMap record);

    
    
    int updateProductQtyBalance(@Param("qtyDeducted") Double qtyDeducted,@Param("productCode") String productCode,@Param("dpid") Integer dispensationUnitId);
}