/*
 * ProgramsJP.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.issuing;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.forms.admin.dispensingpoints.NumberPadJD;

import com.oribicom.tools.TableModel;

/**
 * 
 * @author __USER__
 */
public class ProductsJP extends javax.swing.JPanel {

	// Create tableModel for Program Products for each facility
	private static final String[] columns_products = { "Product Code","Product name", "id","qty" };
	private static final Object[] defaultv_products = { "","", "","" };
	private static final int rows_products = 0;
	public static TableModel tableModel_products = new TableModel(
			columns_products, defaultv_products, rows_products);

	private static Products products;
	private static ListIterator<Products> productsIterator;
    
	private static List<Products> mylist =  new LinkedList();
	private static  List<Products> productsList = new LinkedList();
	private static int selectedProgramID;
	
	private Products selectedProduct;
	
	private static ProductQty qty;

	/** Creates new form ProgramsJP */
	public ProductsJP(int selectedProgramID) {
		
		this.selectedProgramID = selectedProgramID;
		initComponents();
		this.setFocusable(false);
		this.productsJTable.setFocusable(false);// remove from focus cycle
		
		
		this.productsJTable.getColumnModel().getColumn(0).setMinWidth(120);
		this.productsJTable.getColumnModel().getColumn(0).setMaxWidth(120);
		this.productsJTable.getColumnModel().getColumn(2).setMinWidth(0);
		this.productsJTable.getColumnModel().getColumn(2).setMaxWidth(0);
		
		this.productsJTable.getColumnModel().getColumn(3).setMinWidth(0);
		this.productsJTable.getColumnModel().getColumn(3).setMaxWidth(0);

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			//if get all products if selected
			if(this.selectedProgramID == 0){
			//this.productsList = session.selectList("getAll");
				
				this.productsList = session.selectList("selectProducts");
		}else{
			
			//if a program area is selected 
			this.productsList = session.selectList("selectProductByProgramID",
					this.selectedProgramID);
		}
			
					
			this.populateProductsTable(this.productsList);
			
		} finally {
			session.close();
		}
	}

	// GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		jScrollPane1 = new javax.swing.JScrollPane();
		productsJTable = new javax.swing.JTable();

		productsJTable.setFont(new java.awt.Font("Tahoma", 0, 24));
		productsJTable.setModel(tableModel_products);
		productsJTable.setRowHeight(60);
		productsJTable.setTableHeader(null);
		productsJTable.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				productsJTableMouseClicked(evt);
			}
		});
		jScrollPane1.setViewportView(productsJTable);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		this.setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 400,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				javax.swing.GroupLayout.Alignment.LEADING).addComponent(
				jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING,
				javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE));
	}// </editor-fold>

	// GEN-END:initComponents

	private void productsJTableMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
		

		IssuingJD.selectedProductID = Integer
				.parseInt(this.tableModel_products.getValueAt(
						this.productsJTable.getSelectedRow(), 2).toString());

	

		SqlSessionFactory factory = new MyBatisConnectionFactory().getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			IssuingJD.selectedProduct = session.selectOne("getByProductId", IssuingJD.selectedProductID);//("getProgramesSupported");
			
			//System.out.println(this.products.getPrimaryname() +" "+IssuingJD.selectedProductID);

			//this.populateProgramsTable(this.programsList);

		} finally {
			session.close();
		}
		
		this.selectedProduct = new Products();
		this.qty = new ProductQty();
		this.selectedProduct.setPrimaryname(this.tableModel_products.getValueAt(
				this.productsJTable.getSelectedRow(), 1).toString());
		this.selectedProduct.setCode(this.tableModel_products.getValueAt(
				this.productsJTable.getSelectedRow(), 0).toString());
		
		
		qty.setQty(Double.parseDouble(this.tableModel_products.getValueAt(
						this.productsJTable.getSelectedRow(), 3).toString()));
		this.selectedProduct.setProductQty(qty);
		//IssuingJD.jTabbedPane1.addTab("Quantity", new IssueQtyJP());
		//IssuingJD.jTabbedPane1.setSelectedIndex(3);
		
		//show the number pad to get quantity
		
		new NumberPadJD(javax.swing.JOptionPane.getFrameForComponent(this), true, this.selectedProduct,this.selectedProgramID);

	}
	
	
	
	
	public static List<Products> callbackInitialproductlist(){
	
	
    	SqlSessionFactory factory = new MyBatisConnectionFactory()
	        .getSqlSessionFactory();

        SqlSession session = factory.openSession();

     try {

//if get all products if selected
    if(selectedProgramID == 0){
      //this.productsList = session.selectList("getAll");
	
	productsList = session.selectList("selectProducts");
   }else{

   //if a program area is selected 
      productsList = session.selectList("selectProductByProgramID",
		selectedProgramID);
}



} finally {
session.close();
 }
	return productsList;

}	
	
	
	@SuppressWarnings("null")
	public static List<Products> searchtheproductsList(StringBuilder s){
		
			/*	for (int i = 0 ; i <=  tableModel_products.getRowCount()-1 ; i++){
					if(tableModel_products.getValueAt(i, 2).toString().toLowerCase().contains(s.toString().toLowerCase())){
						mylist.addAll(tableModel_products.getValueAt(i, 2).toString());
					}
				}*/
		
		
		SqlSessionFactory factory = new MyBatisConnectionFactory()
		.getSqlSessionFactory();

SqlSession session = factory.openSession();

try {

	//if get all products if selected
	if(selectedProgramID == 0){
	//this.productsList = session.selectList("getAll");
		
		productsList = session.selectList("selectProducts");
}else{
	
	//if a program area is selected 
	productsList = session.selectList("selectProductByProgramID",
			selectedProgramID);
}
	

	
} finally {
	session.close();
}
	
		for (Products p : productsList) {

			if (p.getPrimaryname().toString().toLowerCase().contains(s.toString().toLowerCase())) {
				mylist.add(p);
			}
		}

		return mylist;
	}

	public static void populateProductsTable(List progProducts) {

		tableModel_products.clearTable();
		productsIterator = progProducts.listIterator();

		while (productsIterator.hasNext()) {

			products = productsIterator.next();
			qty = products.getProductQty();
			
			defaultv_products[0] = products.getCode();
			defaultv_products[1] = products.getPrimaryname();

			defaultv_products[2] = products.getId();
			
			//System.out.println(qty.getQty());
			
			if(qty!= null){
			defaultv_products[3] = qty.getQty();
			
			}else{
				
				defaultv_products[3] = 0;
			}

			// defaultv_prgramproducts[5] = products.g

			ArrayList cols = new ArrayList();
			for (int j = 0; j < columns_products.length; j++) {
				cols.add(defaultv_products[j]);

			}

			tableModel_products.insertRow(cols);

			productsIterator.remove();
		}
	}

	// GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JScrollPane jScrollPane1;
	private javax.swing.JTable productsJTable;
	// End of variables declaration//GEN-END:variables

}