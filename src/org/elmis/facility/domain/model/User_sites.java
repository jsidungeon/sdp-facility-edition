package org.elmis.facility.domain.model;

public class User_sites {
	

	
	private Integer id;

	 
	private Integer site_id;

	
	private Integer user_id;


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getSite_id() {
		return site_id;
	}


	public void setSite_id(Integer siteId) {
		site_id = siteId;
	}


	public Integer getUser_id() {
		return user_id;
	}


	public void setUser_id(Integer userId) {
		user_id = userId;
	}
	
		
}