/*
 * AdminJPanel.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.facility.main.gui;

import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;

import org.elmis.facility.system.SystemSettingJD;
import org.elmis.forms.admin.dispensingpoints.ProvinceDistrictJD;
import org.elmis.forms.regimen.DefineRegimenII;
import org.elmis.forms.users.NewDispensingSitesTabJD;
import org.elmis.forms.users.UsersAdminTabJD;

/**
 * 
 * @author __USER__
 */
public class AdminJPanel extends javax.swing.JPanel {

	List<String> userRightsList;
	private String userRights;
	private JLabel facilityNameLabel;

	/** Creates new form AdminJPanel */
	public AdminJPanel(List<String> userRightsList, JLabel facilityNameLabel) {
		this.facilityNameLabel = facilityNameLabel;

		this.userRightsList = userRightsList;
		initComponents();

		usersAdminJL.setVisible(false);
		facilitySettingsJL.setVisible(false);
		databaseSettingsJL.setVisible(false);

		for (String userRights : userRightsList) {

			//Manage users
			if (this.usersAdminJL.getName().equalsIgnoreCase(userRights.trim())) {
				usersAdminJL.setVisible(true);
			}

			//Manage facility

			if (this.facilitySettingsJL.getName().equalsIgnoreCase(
					userRights.trim())) {
				facilitySettingsJL.setVisible(true);
			}

			//Manage Database
			if (this.databaseSettingsJL.getName().equalsIgnoreCase(
					userRights.trim())) {
				databaseSettingsJL.setVisible(true);
			}
		}
		
		
		
		
		//disable functions if working offline 
	if(System.getProperty("dburl").equalsIgnoreCase("jdbc:hsqldb:hsql://localhost:5000/elmis_facility")){
		databaseSettingsJL.setEnabled(false);
		facilitySettingsJL.setEnabled(false);
		usersAdminJL.setEnabled(false);
		DispensingPointsJL.setEnabled(false);
		DispensingPointsJL1.setEnabled(false);
		DispensingPointsJL.enableInputMethods(false);
		
		//DispensingPointsJL.removeMouseListener(java.awt.event.MouseEvent evt);
		//
	//	((Component) event.getSource()).removeMouseListener(this);
		
	}

	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private void initComponents() {

		facilitySettingsJL = new javax.swing.JLabel();
		usersAdminJL = new javax.swing.JLabel();
		databaseSettingsJL = new javax.swing.JLabel();
		DispensingPointsJL = new javax.swing.JLabel();
		DispensingPointsJL1 = new javax.swing.JLabel();

		setBackground(new java.awt.Color(102, 102, 102));
		setBorder(javax.swing.BorderFactory.createEtchedBorder());
		setForeground(new java.awt.Color(255, 255, 255));

		facilitySettingsJL.setBackground(new java.awt.Color(102, 102, 102));
		facilitySettingsJL.setForeground(new java.awt.Color(102, 102, 102));
		facilitySettingsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		facilitySettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/eLMIS facility icon.png"))); // NOI18N
		facilitySettingsJL.setText(" ");
		facilitySettingsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Facility",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		facilitySettingsJL.setName("MANAGE_FACILITY");
		facilitySettingsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				companySettingsJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				companySettingsJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				companySettingsJLMouseExited(evt);
			}
		});

		usersAdminJL.setBackground(new java.awt.Color(102, 102, 102));
		usersAdminJL.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/User Admin icon.png"))); // NOI18N
		usersAdminJL.setBorder(javax.swing.BorderFactory.createTitledBorder(
				null, "User Admin",
				javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
				javax.swing.border.TitledBorder.DEFAULT_POSITION,
				new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(255,
						255, 255)));
		usersAdminJL.setName("MANAGE_USER");
		usersAdminJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseExited(evt);
			}
		});

		databaseSettingsJL.setBackground(new java.awt.Color(102, 102, 102));
		databaseSettingsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		databaseSettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/eLMIS database icon.png"))); // NOI18N
		databaseSettingsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Database Settings",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		databaseSettingsJL.setName("MANAGE_DATABASE");
		databaseSettingsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				databaseSettingsJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				databaseSettingsJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				databaseSettingsJLMouseExited(evt);
			}
		});

		DispensingPointsJL.setBackground(new java.awt.Color(102, 102, 102));
		DispensingPointsJL
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		DispensingPointsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Dispensing point.png"))); // NOI18N
		DispensingPointsJL.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Dispensing Point",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		DispensingPointsJL.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				DispensingPointsJLMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				DispensingPointsJLMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				DispensingPointsJLMouseExited(evt);
			}
		});

		DispensingPointsJL1.setBackground(new java.awt.Color(102, 102, 102));
		DispensingPointsJL1
				.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		DispensingPointsJL1.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS RvsNR.png"))); // NOI18N
		DispensingPointsJL1.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Province & District ",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		DispensingPointsJL1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				DispensingPointsJL1MouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				DispensingPointsJL1MouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				DispensingPointsJL1MouseExited(evt);
			}
		});
		
		lblRegimen = new JLabel("");
		lblRegimen.setHorizontalAlignment(SwingConstants.CENTER);
		lblRegimen.setIcon(new ImageIcon(AdminJPanel.class.getResource("/images/eLMIS arv regimen.png")));
		lblRegimen.setBorder(javax.swing.BorderFactory
				.createTitledBorder(null, "Regimens",
						javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
						javax.swing.border.TitledBorder.DEFAULT_POSITION,
						new java.awt.Font("Ebrima", 1, 12), new java.awt.Color(
								255, 255, 255)));
		lblRegimen.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				regimensLabelMouseClicked(evt);
			}

			public void mouseEntered(java.awt.event.MouseEvent evt) {
				regimensLabelMouseEntered(evt);
			}

			public void mouseExited(java.awt.event.MouseEvent evt) {
				usersAdminJLMouseExited(evt);
			}
		});

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(facilitySettingsJL, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(usersAdminJL, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(databaseSettingsJL, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(DispensingPointsJL, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(DispensingPointsJL1, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblRegimen, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.BASELINE)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap(53, Short.MAX_VALUE)
					.addGroup(layout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(lblRegimen, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(DispensingPointsJL1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(DispensingPointsJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(databaseSettingsJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(usersAdminJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(facilitySettingsJL, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 133, Short.MAX_VALUE))
					.addContainerGap(241, Short.MAX_VALUE))
		);
		this.setLayout(layout);
	}// </editor-fold>
	//GEN-END:initComponents

	private void DispensingPointsJL1MouseExited(java.awt.event.MouseEvent evt) {
		DispensingPointsJL1
		.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS RvsNR.png")));
	}

	private void DispensingPointsJL1MouseEntered(java.awt.event.MouseEvent evt) {
		DispensingPointsJL1
		.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS R&NR big.png")));
	}

	private void DispensingPointsJL1MouseClicked(java.awt.event.MouseEvent evt) {
		try{
		AppJFrame.glassPane.activate(null);
		
		new ProvinceDistrictJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true, facilityNameLabel);
		}
		finally{
			AppJFrame.glassPane.deactivate();
		}
	}

	private void DispensingPointsJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		DispensingPointsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/Dispensing point.png")));

	}

	private void DispensingPointsJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		DispensingPointsJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMISdispensing icon big.png")));
	}

	private void databaseSettingsJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		databaseSettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/elmis_images/eLMIS database icon.png")));
	}

	private void databaseSettingsJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		databaseSettingsJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMIS database icon big.png")));
	}

	private void usersAdminJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/eLMIS arv regimen.png")));
	}
	
	private void regimens(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/User Admin icon.png")));
	}

	private void usersAdminJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_mouseover_images/eLMIS user admin icon big.png")));
	}
	
	private void regimensLabelMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		usersAdminJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/images/eLMIS arv regimen.png")));
	}

	private void companySettingsJLMouseExited(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		facilitySettingsJL.setIcon(new javax.swing.ImageIcon(getClass()
				.getResource("/images/eLMIS facility icon.png")));
	}

	private void companySettingsJLMouseEntered(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		facilitySettingsJL
				.setIcon(new javax.swing.ImageIcon(getClass().getResource(
						"/elmis_mouseover_images/eLMIS facility icon big.png")));
	}

	private void DispensingPointsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
		/*
				if(!DispensingPointsJL.isEnabled()){
					
				//if button is disabled because the system is running offline 
					//do nothing 	
					
				}else{
					
					
				AppJFrame.glassPane.activate(null);
				new DispensingPointAdminJD(javax.swing.JOptionPane
						.getFrameForComponent(this), true);
				AppJFrame.glassPane.deactivate();
		
				}
		*/
				AppJFrame.glassPane.activate(null);
		
				new NewDispensingSitesTabJD(javax.swing.JOptionPane
						.getFrameForComponent(this), true);
				AppJFrame.glassPane.deactivate();
		
				AppJFrame.glassPane.deactivate();
				
	}

	private void databaseSettingsJLMouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:
		
		if(!databaseSettingsJL.isEnabled()){
			
			//if button is disabled because the system is running offline 
				//do nothing 	
				
			}else{

		AppJFrame.glassPane.activate(null);

		new NetworkSettingsJD(javax.swing.JOptionPane
				.getFrameForComponent(this), true);

		AppJFrame.glassPane.deactivate();
		
			}

	}

	private void usersAdminJLMouseClicked(java.awt.event.MouseEvent evt) {
		
		
		if(!usersAdminJL.isEnabled()){
			
			//if button is disabled because the system is running offline 
				//do nothing 	
				
			}else{

		  AppJFrame.glassPane.activate(null);

			new UsersAdminTabJD(javax.swing.JOptionPane
					.getFrameForComponent(this), true);
			AppJFrame.glassPane.deactivate();

		/*new UsersAdminTabJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);*/
		AppJFrame.glassPane.deactivate();
		
			}
	}
	
	private void regimensLabelMouseClicked(java.awt.event.MouseEvent evt) {
		
		
		if(!usersAdminJL.isEnabled()){
			
			//if button is disabled because the system is running offline 
				//do nothing 	
				
			}else{

		

			AppJFrame.glassPane.activate(null);

			new DefineRegimenII(javax.swing.JOptionPane
					.getFrameForComponent(this), true);
			AppJFrame.glassPane.deactivate();

		/*new UsersAdminTabJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);*/
		AppJFrame.glassPane.deactivate();
		
			}
	}

	private void companySettingsJLMouseClicked(java.awt.event.MouseEvent evt) {
		
		if(!facilitySettingsJL.isEnabled()){
			
			//if button is disabled because the system is running offline 
			//do nothing 	

		} else {
			AppJFrame.glassPane.activate(null);

			new SystemSettingJD(javax.swing.JOptionPane
					.getFrameForComponent(this), true);

		/*new SystemSettingJD(javax.swing.JOptionPane.getFrameForComponent(this),
				true);*/

		AppJFrame.glassPane.deactivate();
		
			}
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JLabel DispensingPointsJL;
	private javax.swing.JLabel DispensingPointsJL1;
	private javax.swing.JLabel facilitySettingsJL;
	private javax.swing.JLabel databaseSettingsJL;
	private javax.swing.JLabel usersAdminJL;
	private JLabel lblRegimen;
	// End of variables declaration//GEN-END:variables

}