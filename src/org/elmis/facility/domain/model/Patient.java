package org.elmis.facility.domain.model;

import java.sql.Date;
import java.sql.Timestamp;

public class Patient {
	private String id;
	private Date dateOfBirth;
	private String firstName;
	private String lastName;
	private String sex;
	private String nrcNumber;
	private String artNumber;
	private Timestamp registrationDate;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public String getNrcNumber() {
		return nrcNumber;
	}
	public void setNrcNumber(String nrcNumber) {
		this.nrcNumber = nrcNumber;
	}
	public String getArtNumber() {
		return artNumber;
	}
	public void setArtNumber(String artNumber) {
		this.artNumber = artNumber;
	}
	public Timestamp getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Timestamp registrationDate) {
		this.registrationDate = registrationDate;
	}
}
