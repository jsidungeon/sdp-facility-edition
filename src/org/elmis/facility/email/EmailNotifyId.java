package org.elmis.facility.email;


/**
 * EmailNotifyId entity. @author MyEclipse Persistence Tools
 */
//@Embeddable
public class EmailNotifyId implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String feenote;
	private String invoice;
	private String quote;
	private String payment;
	private String newClient;
	private String newFile;
	private String fileClosed;
	private String clientDeleted;
	private String fileDeleted;
	private String newStaff;
	private String staffDeleted;
	private String staffAccessLevel;
	private String weeklyReport;
	private String monthlyReport;

	// Constructors

	/** default constructor */
	public EmailNotifyId() {
	}

	/** full constructor */
	public EmailNotifyId(String id, String feenote, String invoice,
			String quote, String payment, String newClient, String newFile,
			String fileClosed, String clientDeleted, String fileDeleted,
			String newStaff, String staffDeleted, String staffAccessLevel,
			String weeklyReport, String monthlyReport) {
		this.id = id;
		this.feenote = feenote;
		this.invoice = invoice;
		this.quote = quote;
		this.payment = payment;
		this.newClient = newClient;
		this.newFile = newFile;
		this.fileClosed = fileClosed;
		this.clientDeleted = clientDeleted;
		this.fileDeleted = fileDeleted;
		this.newStaff = newStaff;
		this.staffDeleted = staffDeleted;
		this.staffAccessLevel = staffAccessLevel;
		this.weeklyReport = weeklyReport;
		this.monthlyReport = monthlyReport;
	}

	// Property accessors

	//@Column(name = "id", length = 40)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	//@Column(name = "feenote", length = 5)
	public String getFeenote() {
		return this.feenote;
	}

	public void setFeenote(String feenote) {
		this.feenote = feenote;
	}

	//@Column(name = "invoice", length = 5)
	public String getInvoice() {
		return this.invoice;
	}

	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}

	//@Column(name = "quote", length = 5)
	public String getQuote() {
		return this.quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	//@Column(name = "payment", length = 5)
	public String getPayment() {
		return this.payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	//@Column(name = "new_client", length = 5)
	public String getNewClient() {
		return this.newClient;
	}

	public void setNewClient(String newClient) {
		this.newClient = newClient;
	}

	//@Column(name = "new_file", length = 5)
	public String getNewFile() {
		return this.newFile;
	}

	public void setNewFile(String newFile) {
		this.newFile = newFile;
	}

	//@Column(name = "file_closed", length = 5)
	public String getFileClosed() {
		return this.fileClosed;
	}

	public void setFileClosed(String fileClosed) {
		this.fileClosed = fileClosed;
	}

	//@Column(name = "client_deleted", length = 5)
	public String getClientDeleted() {
		return this.clientDeleted;
	}

	public void setClientDeleted(String clientDeleted) {
		this.clientDeleted = clientDeleted;
	}

	//@Column(name = "file_deleted", length = 5)
	public String getFileDeleted() {
		return this.fileDeleted;
	}

	public void setFileDeleted(String fileDeleted) {
		this.fileDeleted = fileDeleted;
	}

	//@Column(name = "new_staff", length = 5)
	public String getNewStaff() {
		return this.newStaff;
	}

	public void setNewStaff(String newStaff) {
		this.newStaff = newStaff;
	}

	//@Column(name = "staff_deleted", length = 5)
	public String getStaffDeleted() {
		return this.staffDeleted;
	}

	public void setStaffDeleted(String staffDeleted) {
		this.staffDeleted = staffDeleted;
	}

	//@Column(name = "staff_access_level", length = 5)
	public String getStaffAccessLevel() {
		return this.staffAccessLevel;
	}

	public void setStaffAccessLevel(String staffAccessLevel) {
		this.staffAccessLevel = staffAccessLevel;
	}

	//@Column(name = "weekly_report", length = 5)
	public String getWeeklyReport() {
		return this.weeklyReport;
	}

	public void setWeeklyReport(String weeklyReport) {
		this.weeklyReport = weeklyReport;
	}

	//@Column(name = "monthly_report", length = 5)
	public String getMonthlyReport() {
		return this.monthlyReport;
	}

	public void setMonthlyReport(String monthlyReport) {
		this.monthlyReport = monthlyReport;
	}

	@Override
	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof EmailNotifyId))
			return false;
		EmailNotifyId castOther = (EmailNotifyId) other;

		return ((this.getId() == castOther.getId()) || (this.getId() != null
				&& castOther.getId() != null && this.getId().equals(
				castOther.getId())))
				&& ((this.getFeenote() == castOther.getFeenote()) || (this
						.getFeenote() != null
						&& castOther.getFeenote() != null && this.getFeenote()
						.equals(castOther.getFeenote())))
				&& ((this.getInvoice() == castOther.getInvoice()) || (this
						.getInvoice() != null
						&& castOther.getInvoice() != null && this.getInvoice()
						.equals(castOther.getInvoice())))
				&& ((this.getQuote() == castOther.getQuote()) || (this
						.getQuote() != null
						&& castOther.getQuote() != null && this.getQuote()
						.equals(castOther.getQuote())))
				&& ((this.getPayment() == castOther.getPayment()) || (this
						.getPayment() != null
						&& castOther.getPayment() != null && this.getPayment()
						.equals(castOther.getPayment())))
				&& ((this.getNewClient() == castOther.getNewClient()) || (this
						.getNewClient() != null
						&& castOther.getNewClient() != null && this
						.getNewClient().equals(castOther.getNewClient())))
				&& ((this.getNewFile() == castOther.getNewFile()) || (this
						.getNewFile() != null
						&& castOther.getNewFile() != null && this.getNewFile()
						.equals(castOther.getNewFile())))
				&& ((this.getFileClosed() == castOther.getFileClosed()) || (this
						.getFileClosed() != null
						&& castOther.getFileClosed() != null && this
						.getFileClosed().equals(castOther.getFileClosed())))
				&& ((this.getClientDeleted() == castOther.getClientDeleted()) || (this
						.getClientDeleted() != null
						&& castOther.getClientDeleted() != null && this
						.getClientDeleted()
						.equals(castOther.getClientDeleted())))
				&& ((this.getFileDeleted() == castOther.getFileDeleted()) || (this
						.getFileDeleted() != null
						&& castOther.getFileDeleted() != null && this
						.getFileDeleted().equals(castOther.getFileDeleted())))
				&& ((this.getNewStaff() == castOther.getNewStaff()) || (this
						.getNewStaff() != null
						&& castOther.getNewStaff() != null && this
						.getNewStaff().equals(castOther.getNewStaff())))
				&& ((this.getStaffDeleted() == castOther.getStaffDeleted()) || (this
						.getStaffDeleted() != null
						&& castOther.getStaffDeleted() != null && this
						.getStaffDeleted().equals(castOther.getStaffDeleted())))
				&& ((this.getStaffAccessLevel() == castOther
						.getStaffAccessLevel()) || (this.getStaffAccessLevel() != null
						&& castOther.getStaffAccessLevel() != null && this
						.getStaffAccessLevel().equals(
								castOther.getStaffAccessLevel())))
				&& ((this.getWeeklyReport() == castOther.getWeeklyReport()) || (this
						.getWeeklyReport() != null
						&& castOther.getWeeklyReport() != null && this
						.getWeeklyReport().equals(castOther.getWeeklyReport())))
				&& ((this.getMonthlyReport() == castOther.getMonthlyReport()) || (this
						.getMonthlyReport() != null
						&& castOther.getMonthlyReport() != null && this
						.getMonthlyReport()
						.equals(castOther.getMonthlyReport())));
	}

	@Override
	public int hashCode() {
		int result = 17;

		result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
		result = 37 * result
				+ (getFeenote() == null ? 0 : this.getFeenote().hashCode());
		result = 37 * result
				+ (getInvoice() == null ? 0 : this.getInvoice().hashCode());
		result = 37 * result
				+ (getQuote() == null ? 0 : this.getQuote().hashCode());
		result = 37 * result
				+ (getPayment() == null ? 0 : this.getPayment().hashCode());
		result = 37 * result
				+ (getNewClient() == null ? 0 : this.getNewClient().hashCode());
		result = 37 * result
				+ (getNewFile() == null ? 0 : this.getNewFile().hashCode());
		result = 37
				* result
				+ (getFileClosed() == null ? 0 : this.getFileClosed()
						.hashCode());
		result = 37
				* result
				+ (getClientDeleted() == null ? 0 : this.getClientDeleted()
						.hashCode());
		result = 37
				* result
				+ (getFileDeleted() == null ? 0 : this.getFileDeleted()
						.hashCode());
		result = 37 * result
				+ (getNewStaff() == null ? 0 : this.getNewStaff().hashCode());
		result = 37
				* result
				+ (getStaffDeleted() == null ? 0 : this.getStaffDeleted()
						.hashCode());
		result = 37
				* result
				+ (getStaffAccessLevel() == null ? 0 : this
						.getStaffAccessLevel().hashCode());
		result = 37
				* result
				+ (getWeeklyReport() == null ? 0 : this.getWeeklyReport()
						.hashCode());
		result = 37
				* result
				+ (getMonthlyReport() == null ? 0 : this.getMonthlyReport()
						.hashCode());
		return result;
	}

}