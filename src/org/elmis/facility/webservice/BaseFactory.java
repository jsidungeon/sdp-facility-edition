package org.elmis.facility.webservice;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;


/**
 * PoC
 * Created by: Elias Muluneh
 * Date: 7/1/13
 * Time: 5:11 PM
 */
public class BaseFactory {

    // TODO: remove the hardcoded default values.
    public static String BaseURL = "http://localhost:9091";

    public static String Authorization  = "Admin123";

    public static String UserName = "jsikazwe";

    public static Long FacilityID = 1L;

    private static void parseHostAndPort(){
      // this assumes that the base URL contains the protocol and all
      HttpClient.PROTOCOL = BaseURL.substring(0, BaseURL.indexOf(':') );

      // set the port
      if(BaseURL.lastIndexOf(':')  != BaseURL.indexOf(':')){
         String port = BaseURL.substring(BaseURL.lastIndexOf(':') + 1);
         HttpClient.PORT = Integer.parseInt(port);
      }else{
        // default to http port 80
        HttpClient.PORT = 80;
      }

      String host = BaseURL.substring(BaseURL.lastIndexOf("//") + 2);
      if(host.indexOf(':') > 0){
        host = host.substring(0, host.indexOf(':'));
      }

      HttpClient.HOST = host;

    }

    public static Object loadJSONLookup(String service, Class<?> type) throws Exception{
        parseHostAndPort();

        HttpClient client = new HttpClient();
        client.createContext();
        

        ResponseEntity response = client.SendJSON("{}", BaseURL + "/rest-api/lookup/" + service, HttpClient.POST, UserName , Authorization );
        System.out.println(response.getResponse());
        try{
            JSONObject array = new JSONObject(response.getResponse());
            JSONArray innerArray = array.getJSONArray(service);
            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
            	   public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            		      return new Date(json.getAsJsonPrimitive().getAsLong()); 
            		   } 
            		}).create();

            List<Object> list = new ArrayList<>() ;
            for(int i = 0; i < innerArray.length(); i++){

                list.add(gson.fromJson(innerArray.getJSONObject(i).toString(),type)) ;
            }
            return list;
        }

        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            throw new Exception ("Authentication Error");
        }

    }


    public static Object uploadJSON(String service, String result , String json , Class<?> type) throws Exception{

        HttpClient client = new HttpClient();
        client.createContext();

        ResponseEntity response = client.SendJSON(json, BaseURL + "/rest-api/" + service, HttpClient.POST, UserName , Authorization );

        JSONObject array = new JSONObject(response.getResponse());

        String errorObject = null;
        try{
            errorObject = array.getString("error");
        }catch(Exception ex){

        }
        if(errorObject != null){
            throw new Exception(errorObject);
        }
        try{
            JSONObject resultJson = array.getJSONObject(result);
            Gson gson = new Gson();
            return gson.fromJson(resultJson.toString(),type);
        }
        catch(Exception ex){

            if(ex instanceof NullPointerException){
                throw new Exception ("Network Error");
            }
            // JSON was not being parsed,
            // this indicates that the authentication has failed.
            throw new Exception("Authentication Error");
        }

    }
    
    public static void main(String args[]){
    	  try{
    		  

             /* Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDeserializer<Date>() { 
                  public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
                      return new Date(json.getAsJsonPrimitive().getAsLong()); 
                   } 
                }).create();
              String myjson = "{\"description\":null,\"enddate\":1362020400000,\"id\":2,\"name\":\"Feb 2013\",\"startdate\":1359687600000}";
                     Processing_Periods p = gson.fromJson(myjson, Processing_Periods.class) ; 
                     System.out.println(p.getName());
                     System.out.println(p.getStartdate());*/
              
          }

          catch(Exception ex){
          }

    }


}

class TimestampDeserializer implements JsonDeserializer<Timestamp>
{
  @Override
  public Timestamp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
      throws JsonParseException
  {
    long time = Long.parseLong(json.getAsString());
    return new Timestamp(time);
  }
}

class DateDeserialize2r implements JsonDeserializer<Date> {

	@Override
	public Date deserialize(JsonElement element, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {
		String date = element.getAsString();
		
		SimpleDateFormat formatter = new SimpleDateFormat();
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		try {
			return formatter.parse(date);
		} catch (ParseException e) {
			System.err.println("Failed to parse Date due to:");
			return null;
		}
	}
}
