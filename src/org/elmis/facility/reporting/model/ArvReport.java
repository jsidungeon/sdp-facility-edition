/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.model;

import java.sql.Date;

/**
 * @author mmwebaze
 *
 */
public class ArvReport {

	private String productCode;
	private String drugProduct;
	private String unit;
	private int beginningBal;
	private int totalReceived;
	private int qtyDispensed;
	private int lossAdjustment;
	private int phyCount;
	private Date fromDate;
	private Date toDate;
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getDrugProduct() {
		return drugProduct;
	}
	public void setDrugProduct(String drugProduct) {
		this.drugProduct = drugProduct;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public int getBeginningBal() {
		return beginningBal;
	}
	@Override
	public String toString() {
		return "ArvReport [productCode=" + productCode + ", drugProduct="
				+ drugProduct + ", unit=" + unit + ", beginningBal="
				+ beginningBal + ", totalReceived=" + totalReceived
				+ ", qtyDispensed=" + qtyDispensed + ", lossAdjustment="
				+ lossAdjustment + ", phyCount=" + phyCount + ", fromDate="
				+ fromDate + ", toDate=" + toDate + "]";
	}
	public void setBeginningBal(int beginningBal) {
		this.beginningBal = beginningBal;
	}
	public int getTotalReceived() {
		return totalReceived;
	}
	public void setTotalReceived(int totalReceived) {
		this.totalReceived = totalReceived;
	}
	public int getQtyDispensed() {
		return qtyDispensed;
	}
	public void setQtyDispensed(int qtyDispensed) {
		this.qtyDispensed = qtyDispensed;
	}
	public int getLossAdjustment() {
		return lossAdjustment;
	}
	public void setLossAdjustment(int lossAdjustment) {
		this.lossAdjustment = lossAdjustment;
	}
	public int getPhyCount() {
		return phyCount;
	}
	public void setPhyCount(int phyCount) {
		this.phyCount = phyCount;
	}
	public Date getFromDate() {
		return fromDate;
	}
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}
	public Date getToDate() {
		return toDate;
	}
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
}
