package org.elmis.forms.regimen;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.domain.model.ElmisRegimenCategory;
import org.elmis.forms.regimen.dao.RegimenDao;
import org.elmis.forms.regimen.util.GenericUtil;
import org.postgresql.util.PSQLException;

import com.sun.media.sound.ModelAbstractChannelMixer;

public class DefineRegimenLine extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel pnlMain = new JPanel();
	private JTable tblRegimenLines;
	private JTextField txtRegimenLine;
	private JScrollPane scrollPane;
	private ElmisRegimenCategory selectedLine = null;
	private DefineRegimenLine _this = this;
	private List<ElmisRegimenCategory> regimenLines = null;
	private DefineRegimenII parentWindow = null;
	protected boolean dataChange = false;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			DefineRegimenLine dialog = new DefineRegimenLine(null , null, true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unused")
	private void setUpRegimenLinesTable(){
		DefaultTableModel model = new DefaultTableModel(null, new String[] {
				"Name", "Line", "Active" }) {
			private static final long serialVersionUID = 1L;
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int c) {
				switch (c) {
				case 0:
					return Boolean.class;
				default:
					return String.class;
				}
			}
		};
	}
	private void populateRegimenLineTable(){
		RegimenDao dao = RegimenDao.getInstance();
		regimenLines   = dao.fetchRegimenCategories();
		GenericUtil.clearTable(tblRegimenLines);
		DefaultTableModel model = (DefaultTableModel) tblRegimenLines.getModel();
		for (ElmisRegimenCategory erCategory : regimenLines) {
			model.addRow(new Object[] { erCategory.getName()});
		}

	/*	TableColumnModel m=tblProducts.getColumnModel();
		TableColumn col=m.getColumn(3);
		m.removeColumn(col);*/
		tblRegimenLines.setModel(model);
	}
	
	private void addRegimenLineToGUITable(ElmisRegimenCategory erCategory){
		DefaultTableModel model = (DefaultTableModel) tblRegimenLines.getModel();
		model.addRow(new Object[] { erCategory.getName()});
		this.regimenLines.add(erCategory);
	}
	
/*	private void updateRegimenLineToGUITable(ElmisRegimenCategory erCategory){
		DefaultTableModel model = (DefaultTableModel) tblRegimenLines.getModel();
		model.setValueAt(erCategory.getName(), tblRegimenLines.getSelectedRow(), 0);
	}
*/
	@SuppressWarnings("serial")
	private void setUpRegimenLineTable() {
	
		pnlMain.setLayout(null);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(29, 165, 410, 185);
		pnlMain.add(scrollPane);
		
				tblRegimenLines = new JTable();
				scrollPane.setViewportView(tblRegimenLines);
				tblRegimenLines.setModel(new DefaultTableModel(
					new Object[][] {
					},
					new String[] {
						"Regimen Lines"
					}
				));
				tblRegimenLines.getColumnModel().getColumn(0).setPreferredWidth(377);
				tblRegimenLines.getSelectionModel().addListSelectionListener(new ListSelectionListener(){
			        public void valueChanged(ListSelectionEvent event) {
//			             do some actions here, for example
//			             print first column value from selected row
//			            System.out.println(tblRegimenLines.getValueAt(tblRegimenLines.getSelectedRow(), 0).toString());
			        	if(!_this.dataChange){
			        		_this.selectRegimenLine();
			        	}
			            //set checked products
			        }
			    });
	}
	
	protected void selectRegimenLine() {
		// TODO Auto-generated method stub
		_this.selectedLine = this.regimenLines.get(tblRegimenLines.getSelectedRow());
		_this.txtRegimenLine.setText(_this.selectedLine.getName());
		System.out.println(_this.selectedLine.getName());
	}

	private void hideColumn(TableColumn column){
		column.setMinWidth(0);
		column.setMaxWidth(0);
		column.setPreferredWidth(0);
	}

	private Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return prop;
	}
	
	private void resetRegimenLineForm(){
		_this.txtRegimenLine.setText("");
		_this.selectedLine = null;
	}

	/**
	 * Create the dialog.
	 */
	public DefineRegimenLine(DefineRegimenII parentWindow, java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				populateRegimenLineTable();
			}
		});
		_this.parentWindow  = parentWindow;
		setTitle("Define Regimen Line");
		setResizable(false);
		setBounds(100, 100, 474, 410);
		getContentPane().setLayout(new BorderLayout());
		pnlMain.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(pnlMain, BorderLayout.CENTER);
		{
			setUpRegimenLineTable();
		}
		
		JSeparator separator = new JSeparator();
		separator.setBounds(438, 60, 1, 2);
		pnlMain.add(separator);
		
		JPanel panel = new JPanel();
		panel.setBounds(29, 56, 410, 88);
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		pnlMain.add(panel);
		panel.setLayout(null);
				
						
								txtRegimenLine = new JTextField();
								txtRegimenLine.setBounds(20, 42, 366, 20);
								panel.add(txtRegimenLine);
								txtRegimenLine.setColumns(10);
								
										JLabel lblRegimenLineName = new JLabel("Regimen Line Name");
										lblRegimenLineName.setBounds(20, 21, 200, 14);
										panel.add(lblRegimenLineName);
										
										JPanel panel_1 = new JPanel();
										panel_1.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
										panel_1.setBounds(0, 5, 484, 40);
										pnlMain.add(panel_1);
												panel_1.setLayout(null);
										
										
												JButton btnSave = new JButton("");
												btnSave.setIcon(new ImageIcon(DefineRegimenLine.class.getResource("/elmis_images/Save icon.png")));
												btnSave.setBorderPainted(false);
												btnSave.setContentAreaFilled(false);
												btnSave.setBounds(10, 5, 20, 23);
												panel_1.add(btnSave);
												
												JButton btnClose = new JButton("Close");
												btnClose.setBounds(310, 5, 79, 23);
												panel_1.add(btnClose);
												
												JButton btnNew = new JButton("");
												btnNew.setBorderPainted(false);
												btnNew.setContentAreaFilled(false);
												btnNew.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent arg0) {
														_this.selectedLine = null;
													}
												});
												btnNew.setIcon(new ImageIcon(DefineRegimenLine.class.getResource("/elmis_images/Add.png")));
												btnNew.setBounds(37, 5, 20, 23);
												panel_1.add(btnNew);
												
												JButton btnDeleteRegimenLine = new JButton("");
												btnDeleteRegimenLine.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent e) {
														_this.dataChange = true;
														int confirm = JOptionPane.showOptionDialog(_this,  "Are you sure you want to delete regimen line?" , "Confirm Delete Regimen Line",
																JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
																null, null, null);
														if(confirm == JOptionPane.YES_OPTION){
															try {
																RegimenDao.getInstance().deleteRegimenLine(_this.selectedLine);
																if(_this.parentWindow != null) _this.parentWindow.setUpCategoriesCombo();
																_this.populateRegimenLineTable();
																_this.resetRegimenLineForm();
															} catch (PSQLException ex) {
																// TODO Auto-generated catch block
																ex.printStackTrace();
																JOptionPane.showMessageDialog(_this, "Regimen line is used!");
															}
														}
														_this.dataChange = false;
													}
													
												});
												btnDeleteRegimenLine.setIcon(new ImageIcon(DefineRegimenLine.class.getResource("/elmis_images/eLMIS delete role small.png")));
												btnDeleteRegimenLine.setBounds(67, 5, 26, 23);
												panel_1.add(btnDeleteRegimenLine);
												btnClose.addMouseListener(new MouseAdapter() {
													@Override
													public void mouseClicked(MouseEvent arg0) {
														
													}
												});
												btnSave.addActionListener(new ActionListener() {
													public void actionPerformed(ActionEvent e) {
														_this.dataChange  = true;
														if(_this.selectedLine == null){
															ElmisRegimenCategory category = new ElmisRegimenCategory();
															category.setName(_this.txtRegimenLine.getText().trim());
															category.setCode(_this.txtRegimenLine.getText().trim());
															RegimenDao.getInstance().insertRegimenLine(category);
//															addRegimenLineToGUITable(category);
															_this.populateRegimenLineTable();
															_this.resetRegimenLineForm();
														} else {
															_this.selectedLine.setName(_this.txtRegimenLine.getText().trim());
															_this.selectedLine.setCode(_this.txtRegimenLine.getText().trim());
															RegimenDao.getInstance().updateRegimenLine(_this.selectedLine);
															populateRegimenLineTable();
															
														}
														JOptionPane.showMessageDialog(_this, "Changes Saved Successfully!");
														_this.dataChange = false;
														if(_this.parentWindow != null) _this.parentWindow.setUpCategoriesCombo();
													}
												});
				if(!this.isVisible())this.setVisible(true);
	}
}
