package org.elmis.facility.main.gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;

import org.elmis.facility.embedded.domain.dao.UserDao;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.elmis.facility.utils.SdpStyleSheet;

public class ChangePasswordJd extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JPasswordField oldPasswordField;
	private JPasswordField newPasswordField;
	private JPasswordField confirmPasswordField;
	private final String USERNAME;
	private final static Cursor busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final static Cursor defaultCursor = Cursor.getDefaultCursor();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Properties prop = new Properties(System.getProperties());
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("Programmproperties.properties");
			prop.load(fis);

			System.setProperties(prop);

			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
		System.setProperty("dbpassword", dbpassword);
		try {
			ChangePasswordJd dialog = new ChangePasswordJd(new javax.swing.JFrame(), true, "eLMIS");
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ChangePasswordJd(java.awt.Frame parent, boolean modal, String userName) {
		super(parent, modal);
		this.USERNAME = userName;
		setTitle("Change password for user "+userName);
		setBounds(100, 100, 602, 340);
		getContentPane().setLayout(new BorderLayout());
		SdpStyleSheet.configJPanelBackground(contentPanel);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JPanel panel = new JPanel();
		SdpStyleSheet.configJPanelBackground(panel);
		panel.setBounds(81, 57, 466, 170);
		contentPanel.add(panel);
		panel.setLayout(null);
		
		JLabel oldPwdLabel = new JLabel("Old password:");
		SdpStyleSheet.configOtherJLabel(oldPwdLabel);
		//oldPwdLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		oldPwdLabel.setBounds(73, 11, 181, 20);
		panel.add(oldPwdLabel);
		
		JLabel newPwdLabel = new JLabel("New password:");
		SdpStyleSheet.configOtherJLabel(newPwdLabel);
		//newPwdLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		newPwdLabel.setBounds(73, 59, 154, 17);
		panel.add(newPwdLabel);
		
		JLabel confirmNewPwdLabel = new JLabel("Confirm new password:");
		SdpStyleSheet.configOtherJLabel(confirmNewPwdLabel);
		//confirmNewPwdLabel.setFont(new Font("Tahoma", Font.BOLD, 12));
		confirmNewPwdLabel.setBounds(73, 101, 200, 17);
		panel.add(confirmNewPwdLabel);
		
		oldPasswordField = new JPasswordField();
		oldPasswordField.setBounds(301, 11, 154, 33);
		panel.add(oldPasswordField);
		
		newPasswordField = new JPasswordField();
		newPasswordField.setBounds(301, 59, 154, 31);
		panel.add(newPasswordField);
		
		confirmPasswordField = new JPasswordField();
		confirmPasswordField.setBounds(301, 101, 154, 33);
		panel.add(confirmPasswordField);
		{
			JPanel buttonPane = new JPanel();
			SdpStyleSheet.configJPanelBackground(buttonPane);
			buttonPane.setBounds(0, 250, 586, 52);
			contentPanel.add(buttonPane);
			buttonPane.setLayout(null);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setBounds(212, 5, 105, 36);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("Confirm Password");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try{
							ChangePasswordJd.this.setCursor(busyCursor);
							String oldPassword = new String(oldPasswordField.getPassword()).trim();
							String newPassword = new String(newPasswordField.getPassword()).trim();
							String confirmPassword = new String(confirmPasswordField.getPassword()).trim();

							if (oldPassword.length() == 0)
								JOptionPane.showMessageDialog(ChangePasswordJd.this, "Old Password should be provided.", "WARNING", JOptionPane.WARNING_MESSAGE);
							else if (newPassword.length() == 0 || newPassword.length() < 8)
								JOptionPane.showMessageDialog(ChangePasswordJd.this,"Provide a New Password or Password has less than Eight characters.");
							else if (confirmPassword.length() == 0 || confirmPassword.length() < 8)
								JOptionPane.showMessageDialog(ChangePasswordJd.this,"Provide the New Password again or Password has less than Eight characters.");
							else
							{
								if (oldPassword.equals(newPassword)){
									JOptionPane.showMessageDialog(ChangePasswordJd.this,"Old password and New password are the same.");
									oldPasswordField.setText("");
									newPasswordField.setText("");
									confirmPasswordField.setText("");
								}
								else{
									if (newPassword.equals(confirmPassword)){
										if (new UserDao().changeUserPassword(USERNAME, oldPassword, newPassword)){
											JOptionPane.showMessageDialog(ChangePasswordJd.this,"Password has been changed for user "+USERNAME);
											dispose();
										}
										else
											JOptionPane.showMessageDialog(ChangePasswordJd.this,"Invalid Old Password for user "+USERNAME);

									}
									else{
										JOptionPane.showMessageDialog(ChangePasswordJd.this,"New Password and Confirmed Password must be the same.");
										oldPasswordField.setText("");
										newPasswordField.setText("");
										confirmPasswordField.setText("");
									}
								}	
							}
						}
						finally{
							ChangePasswordJd.this.setCursor(defaultCursor);
						}
					}
				});
				okButton.setBounds(362, 5, 174, 36);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		setLocationRelativeTo(parent);
	}
}
