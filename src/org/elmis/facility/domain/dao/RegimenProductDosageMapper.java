package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.RegimenProductDosage;

public interface RegimenProductDosageMapper {
	List<RegimenProductDosage> selectDosageUnitsByRegimenProductId(Integer regProdId);
	
	List<RegimenProductDosage> selectDosagesByProductAndRegimen(@Param("productId") Integer productId , @Param("regimenId") Integer regimenId);
	
	RegimenProductDosage selectLastDosageForProduct(@Param("artNumber") String artNo, @Param("productCode") String productCode , @Param("regimenId") Integer regimenId);
	
	Double selectLastQtyForProduct(@Param("artNumber") String artNo, @Param("productCode") String programCode , @Param("regimenId") Integer regimenId);

	int insertDosage(RegimenProductDosage dosage);

	int updateDosage(RegimenProductDosage dosage);

	int deleteDosage(RegimenProductDosage dosage);
}
