package org.elmis.facility.hsql.dao;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.database.connection.MyBatisHSQLConnectionFactory;
import org.elmis.facility.reporting.model.Program;
import org.elmis.facility.reporting.model.ReportLineItem;

public class HsqlProgramDao {

	private SqlSessionFactory sqlSessionFactory;

	{
		sqlSessionFactory = MyBatisHSQLConnectionFactory.getSqlSessionFactory();
	}

	public List<Program> getPrograms()
	{
		SqlSession session = sqlSessionFactory.openSession();

		try
		{
				return session.selectList("HsqlProgram.get_program");		
		}
		finally
		{
			session.close();
		}
	}
	public void savePrograms(List<Program> program)
	{
		SqlSession session = sqlSessionFactory.openSession();

		try
		{
			for (Program p : program)
			{
				System.out.println("Insert to HSQL "+p);
				session.insert("HsqlProgram.save_program", p);
				session.commit();
			}

		}
		finally
		{
			session.close();
		}
	}
}
