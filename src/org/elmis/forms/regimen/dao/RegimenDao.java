package org.elmis.forms.regimen.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import net.sf.jasperreports.engine.export.ooxml.PptxSlideHelper;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.domain.dao.RegimenProductDosageMapper;
import org.elmis.facility.domain.dao.Elmis_PatientMapper;
import org.elmis.facility.domain.dao.RegimenCategoryMapper;
import org.elmis.facility.domain.dao.RegimenMapper;
import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.ElmisRegimenCategory;
import org.elmis.facility.domain.model.ElmisRegimenProduct;
import org.elmis.facility.domain.model.RegimenProductDosage;
import org.elmis.facility.reports.regimen.RegimenReportBean;
import org.elmis.facility.utils.ElmisAESencrpDecrp;
import org.postgresql.util.PSQLException;

public class RegimenDao {
	
	private SqlSessionFactory sqlMapper = null;
	
	private RegimenDao(){
		setSystemProperties();
		setSqlMapper();
	}
	private static RegimenDao regimenDao = null;
	
	public static RegimenDao getInstance(){
		if(regimenDao == null){
			regimenDao = new RegimenDao();
		} 
		return regimenDao;
	}
	
	private SqlSessionFactory getSqlMapper(){
		return this.sqlMapper;
	}
	private SqlSessionFactory setSqlMapper(){
		
		
		String resource = "SqlMapConfig.xml";
		Reader reader = null;

		try {
			reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader,
					System.getProperties());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sqlMapper;
	}
	
	public ElmisRegimen getRegimenByART(String art){

		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session
				.getMapper(RegimenMapper.class);

		ElmisRegimen regimen = null;
		try {

			regimen = mapper.selectRegimenByART(art);
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimen;
	}
	public List<ElmisRegimenCategory> fetchRegimenCategories() {

		SqlSession session = getSqlMapper().openSession();
		RegimenCategoryMapper mapper = session
				.getMapper(RegimenCategoryMapper.class);

		List<ElmisRegimenCategory> regCategories = null;
		try {

			regCategories = mapper.selectAllCategories();
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regCategories;
	}
	
	public List<RegimenReportBean> fetchRegimenReportBeans(Date startDate, Date endDate){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		List<RegimenReportBean> reportBeans= null;
		try {

			reportBeans = mapper.selectRegimenReportBeans(startDate, endDate);
			// System.out.println(prodList);

/*			for (RegimenReportBean regimenReportBean : reportBeans) {
				System.out.println(regimenReportBean.getRegimenName());
				System.out.println(regimenReportBean.getRegimenLineId());
				System.out.println(regimenReportBean.getRegimenLineName());
				System.out.println(regimenReportBean.getRegimenid());
				System.out.println(regimenReportBean.getPatientsOnRegimen());
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~");
			}
*/
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return reportBeans;
	}
	
	public List<RegimenProductDosage> fetchDosages(Integer regProdId) {

		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		List<RegimenProductDosage> dosages = null;
		try {

			dosages = mapper.selectDosageUnitsByRegimenProductId(regProdId);
			// System.out.println(prodList);

/*			for (DosageUnit dosageUnit : dosageUnits) {
				System.out.println(dosageUnit.getName());
			}*/

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return dosages;
	}
	
	public List<RegimenProductDosage> fetchDosages(Integer regimenId, Integer productId) {

		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		List<RegimenProductDosage> regimenProductDosages = null;
		try {

			regimenProductDosages = mapper.selectDosagesByProductAndRegimen(productId, regimenId);
			// System.out.println(prodList);

/*			for (DosageUnit dosageUnit : dosageUnits) {
				System.out.println(dosageUnit.getName());
			}*/

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimenProductDosages;
	}

	
	public RegimenProductDosage fetchLastDosageForProduct(String artNo , String productCode , Integer regimenId) {

		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		RegimenProductDosage dosageUnit = null;
		try {

			dosageUnit = mapper.selectLastDosageForProduct(artNo, productCode, regimenId);
			// System.out.println(prodList);


		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return dosageUnit;
	}
	
	public Double fetchLastQtyDispensed(String artNo , String productCode , Integer regimenId) {

		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);
		Double qty= null;
		try {

			qty = mapper.selectLastQtyForProduct(artNo, productCode, regimenId);
			// System.out.println(prodList);


		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return qty;
	}

	public List<ElmisRegimen> fetchElmisRegimenByCategory(Integer categoryId) {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		List<ElmisRegimen> regimens = null;
		try {

			regimens = mapper.selectRegimensByCategory(categoryId);
			// System.out.println(prodList);

			for (ElmisRegimen elmisRegimen : regimens) {
				System.out.println(elmisRegimen.getName());
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimens;
	}
	
	public List<ElmisRegimen> fetchElmisRegimens() {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		List<ElmisRegimen> regimens = null;
		try {

			regimens = mapper.selectAllRegimens();
			// System.out.println(prodList);

/*			for (ElmisRegimen elmisRegimen : regimens) {
				System.out.println(elmisRegimen.getName());
			}
*/
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regimens;
	}
	
	public List<Map<String, Object>> fetchProductsForFacility(String programCode , String facilityTypeCode) {
        SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		List<Map<String, Object>> prodList = null;
		try {

			prodList = mapper.selectProductsForFacility(programCode, facilityTypeCode);
			// System.out.println(prodList);

			for (Map<String, Object> map : prodList) {
				for (String key : map.keySet()) {
					/*
					 * Object value = map.get(key); System.out.println( key +
					 * " = " + value);
					 */
					//System.out.println(map.get("primaryname"));
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodList;
	}
	
	public List<Map<String , Object>> getRegimenProducts(Integer regimenId){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		List<Map<String, Object>> prodList = null;
		try {

			prodList = mapper.selectRegimenProducts(regimenId);
			 for (Map<String, Object> map : prodList) {
				System.out.println(map.get("productid"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodList;
	}
	
	public void updatePatientRegimenByART(String artNo, Integer regId){
		SqlSession session = getSqlMapper().openSession();
		Elmis_PatientMapper mapper = session.getMapper(Elmis_PatientMapper.class);
		
		try {
			mapper.updatePatientRegimenByARTNo(artNo, regId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public List<Map<String, Object>> fetchPatientProducts(String artNumber, Integer dpid) {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		List<Map<String, Object>> prodList = null;
		try {

			prodList = mapper.selectPatientRegimenProducts(artNumber , dpid);
			// System.out.println(prodList);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return prodList;
	}
	
	public void insert(ElmisRegimen elmisRegimen){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.insert(elmisRegimen);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	public void updateRegimen(ElmisRegimen elmisRegimen){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.updateRegimen(elmisRegimen);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	
	public void insertRegimenProduct(ElmisRegimenProduct elmisRegimenProduct){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.insertRegimenProduct(elmisRegimenProduct);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public void deleteRegimenProduct(Integer productId , Integer regimenId){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.deleteRegimenProduct(productId, regimenId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public void deleteRegimenProductDosages(Integer productId, Integer regimenId){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.deleteRegimenProductDosages(productId, regimenId);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public void insertRegimenLine(ElmisRegimenCategory category) {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.insertRegimenCategory(category);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public void updateRegimenLine(ElmisRegimenCategory category) {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.updateRegimenCategory(category);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public void deleteRegimenLine(ElmisRegimenCategory category) throws PSQLException {
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.deleteRegimenCategory(category);

		} finally {
			session.commit();
			session.close();
		}
	}
	
	public static void setSystemProperties(){
		Properties prop = new Properties(System.getProperties());
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("Programmproperties.properties");
			prop.load(fis);

			System.setProperties(prop);

			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
		System.setProperty("dbpassword", dbpassword);
	}
	
	public static void main(String args[]){
		setSystemProperties();
		//System.out.println(new RegimenDao().fetchLastQtyDispensed("1234-123-12345-5", "ARV0016", 2));
		//new RegimenDao().getRegimenProducts(1);
		RegimenDao.getInstance().deleteRegimenProduct(	1065, 105);
	}

	public void insertDosage(RegimenProductDosage dosage) {
		// TODO Auto-generated method stub
		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		try {

			mapper.insertDosage(dosage);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
	}

	public void updateDosage(RegimenProductDosage dosage) {
		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		try {

			mapper.updateDosage(dosage);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
	}
	
	public void deleteDosage(RegimenProductDosage dosage) {
		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		try {

			mapper.deleteDosage(dosage);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
		
	}
	

	public Integer getProductRegimenIdByRegAndProd(Integer regimenId, Integer productId){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		Integer regProdId = null;
		try {

			regProdId = mapper.selectRegimenProductIdByRegimenAndProduct(productId, regimenId);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return regProdId;
	}

	public void deleteRegimen(ElmisRegimen regimen) {
		// TODO Auto-generated method stub
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.deleteRegimen(regimen);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
	}

}
