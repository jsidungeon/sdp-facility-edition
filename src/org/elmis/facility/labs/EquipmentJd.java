package org.elmis.facility.labs;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import org.elmis.facility.domain.dao.EquipmentDao;
import org.elmis.facility.domain.dao.LabTestDao;
import org.elmis.facility.reporting.model.Equipment;
import org.elmis.facility.reporting.model.LabTest;

public class EquipmentJd extends JDialog implements ListSelectionListener, ActionListener {

	private final JPanel contentPanel = new JPanel();
	private boolean save = false;
	private int machineRowSelected; //Row selected from equipment table
	private Map<String, Integer> reagentTableData = new HashMap<>();
	private final static Cursor busyCursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
	private final static Cursor defaultCursor = Cursor.getDefaultCursor();
											// non-emergency report or emergency
											// report will be submitted
	private JTable machineTable;
	private String[][] data = null;
	private String[] machineColumnNames = { "<HTML><b>Equipment<br>Code</b></html>",
			"<HTML><b>Machine Name</b></html>"
			};
	private String[] reagentColumnNames = { "<HTML><b>Reagent<br>Code</b></html>",
			"<HTML><b>Reagent</b></html>",
			"<html><b>Used in<br>Test</b></html>"
			};
	private DefaultTableModel machineTableModel = new DefaultTableModel(data,machineColumnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {

			return false;
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 4)
				return String.class;
			else
				return super.getColumnClass(columnIndex);
		}
	};
	private DefaultTableModel reagentTableModel = new DefaultTableModel(data,reagentColumnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {
			return (column == 2);
		}

		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 2)
				return Boolean.class;
				else
					return super.getColumnClass(columnIndex);
		}
	};
	private JTable reagentsTable;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			EquipmentJd dialog = new EquipmentJd(new javax.swing.JFrame(), true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public EquipmentJd(java.awt.Frame parent, boolean modal) {
		super(parent, modal);
		setTitle("EQUIPMENT & REAGENT MANAGEMENT");
		setIconImage(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/appicon.png")).getImage());
		setBounds(100, 100, 875, 540);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPanel.setBackground(new java.awt.Color(102, 102, 102));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JPanel panel = new JPanel();
			panel.setBackground(new java.awt.Color(102, 102, 102));
			panel.setBounds(0, 0, 860, 439);
			contentPanel.add(panel);
			panel.setLayout(null);

			JLabel titleLbl = new JLabel(
					"EQUIPMENT & REAGENT MANAGEMENT");
			titleLbl.setFont(new Font("Tahoma", Font.BOLD, 13));
			titleLbl.setForeground(new java.awt.Color(255, 255, 255));
			titleLbl.setBounds(10, 23, 363, 14);
			panel.add(titleLbl);

			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 48, 363, 373);
			panel.add(scrollPane);
			List<Equipment> equipmentList = new EquipmentDao().getFacilityApprovedEquipment();
			{
				machineTable = new JTable();
				machineTable.setFont(new java.awt.Font("Ebrima", 0, 14));
				machineTable.setModel(machineTableModel);

				for (int i = 0; i < equipmentList.size(); i++) {
					machineTableModel.addRow(new String[equipmentList.size()]);
					machineTable.getModel().setValueAt(
							equipmentList.get(i).getEquipCode().trim(), i, 0);
					machineTable.getModel().setValueAt(
							equipmentList.get(i).getName().trim(), i, 1);
				}
				machineTable.setFillsViewportHeight(true);
				machineTable.setCellSelectionEnabled(true);
				machineTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				machineTable.getTableHeader().setReorderingAllowed(false);
				machineTable.getTableHeader().setPreferredSize(new Dimension(30, 65));
				machineTable.getColumnModel().getColumn(0).setPreferredWidth(80);
				machineTable.getColumnModel().getColumn(1).setPreferredWidth(265);
				machineTable.setRowHeight(35);
				scrollPane.setViewportView(machineTable);
				
				JScrollPane scrollPane_1 = new JScrollPane();
				scrollPane_1.setBounds(395, 48, 452, 373);
				panel.add(scrollPane_1);
				
				reagentsTable = new JTable();
				reagentsTable.setFont(new java.awt.Font("Ebrima", 0, 14));
				reagentsTable.setModel(reagentTableModel);
				reagentsTable.setFillsViewportHeight(true);
				reagentsTable.setCellSelectionEnabled(true);
				reagentsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
				reagentsTable.getTableHeader().setReorderingAllowed(false);
				reagentsTable.getTableHeader().setPreferredSize(new Dimension(30, 65));
				reagentsTable.getColumnModel().getColumn(0).setPreferredWidth(65);
				reagentsTable.getColumnModel().getColumn(1).setPreferredWidth(310);
				reagentsTable.getColumnModel().getColumn(2).setPreferredWidth(58);
				reagentsTable.setRowHeight(35);
				scrollPane_1.setViewportView(reagentsTable);
				{
					JPanel buttonPane = new JPanel();
					buttonPane.setBackground(new java.awt.Color(102, 102, 102));
					buttonPane.setBounds(0, 442, 860, 49);
					contentPanel.add(buttonPane);
					{
						JButton cancelButton = new JButton("Cancel");
						cancelButton.setFont(new java.awt.Font("Ebrima", 1, 12));
						cancelButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/elmis_images/Cancel.png")));
						cancelButton.setBounds(744, 11, 106, 36);
						cancelButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								dispose();
							}
						});
						{
							buttonPane.setLayout(null);
						}
						cancelButton.setActionCommand("cancelButton");
						buttonPane.add(cancelButton);
					}
				}
				
				reagentsTable.getModel().addTableModelListener(
						new TableModelListener() {

							@Override
							public void tableChanged(TableModelEvent arg0) {
								
							}
						});
				
				
				machineTable.getSelectionModel().addListSelectionListener(this);
				
			}
		}
		setLocationRelativeTo(parent);
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if (e.getValueIsAdjusting()) // mouse button not released yet
			return;
		 machineRowSelected = machineTable.getSelectedRow();
		 if (save)
		 {
			 
		 }
		if (machineRowSelected < 0) // true when clearSelection
			return;
		int col = machineTable.getSelectedColumn();

		if (col < 0) // true when clearSelection
			return;
		String machineCode = (String) machineTable.getValueAt(machineRowSelected, 0);
		if (reagentsTable.getRowCount() != 0)
			clearTable(reagentsTable);
		populateReagentTable(machineCode);
	}
	private void clearTable(JTable table)
	{
		((DefaultTableModel)table.getModel()).setRowCount(0);
	}
	private void populateReagentTable(String machineCode)
	{
		List<LabTest> labTestlist = new LabTestDao().getReagents(machineCode);
		JCheckBox reagentTestStatus = new JCheckBox();
		reagentTestStatus.addActionListener(this);
		reagentsTable.getColumn("<html><b>Used in<br>Test</b></html>").setCellEditor(new DefaultCellEditor(reagentTestStatus));
		for (int i = 0; i < labTestlist.size(); i++) {
			reagentTableModel.addRow(new String[labTestlist.size()]);
			reagentsTable.getModel().setValueAt(
					labTestlist.get(i).getReagentCode().trim(), i, 0);
			reagentsTable.getModel().setValueAt(
					labTestlist.get(i).reagentName.trim(), i, 1);
			reagentsTable.getModel().setValueAt(
					labTestlist.get(i).getIsCountable(), i, 2);

		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int selectedRow = reagentsTable.getSelectedRow();
		String reagentCode = ""+reagentsTable.getValueAt(selectedRow, 0);
		String reagentName = ""+reagentsTable.getValueAt(selectedRow, 1);
		AbstractButton abstractButton = (AbstractButton) e.getSource();
		boolean selected = abstractButton.getModel().isSelected();
		
		int ack = JOptionPane.showConfirmDialog(EquipmentJd.this, "The reagent "+reagentName+" will not be included in Test Count", reagentCode, JOptionPane.YES_NO_OPTION);
		if (ack == JOptionPane.YES_OPTION)
		{
			new EquipmentDao().changeReagentTestcountStatus(reagentCode, selected);
		}
		else
			reagentsTable.setValueAt((!selected), selectedRow, 2);
	}
}
