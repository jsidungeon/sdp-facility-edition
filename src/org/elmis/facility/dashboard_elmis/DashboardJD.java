package org.elmis.facility.dashboard_elmis;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.jidesoft.converter.ObjectConverterManager;
import com.jidesoft.dashboard.AbstractGadget;
import com.jidesoft.dashboard.Dashboard;
import com.jidesoft.dashboard.DashboardPersistenceUtils;
import com.jidesoft.dashboard.DashboardTabbedPane;
import com.jidesoft.dashboard.Gadget;
import com.jidesoft.dashboard.GadgetComponent;
import com.jidesoft.dashboard.GadgetEvent;
import com.jidesoft.dashboard.GadgetListener;
import com.jidesoft.dashboard.GadgetManager;
import com.jidesoft.dashboard.GadgetPalette;
import com.jidesoft.dialog.ButtonPanel;
import com.jidesoft.dialog.StandardDialog;
import com.jidesoft.dialog.StandardDialogPane;
import com.jidesoft.icons.IconsFactory;
import com.jidesoft.pane.CollapsiblePaneTitleButton;
import com.jidesoft.plaf.UIDefaultsLookup;
import com.jidesoft.swing.JideBoxLayout;
import com.jidesoft.swing.JideButton;

public class DashboardJD extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private ScheduledExecutorService barGraphSchedular = Executors.newSingleThreadScheduledExecutor();
	private ScheduledExecutorService pieChartSchedular = Executors.newSingleThreadScheduledExecutor();
	public static String _lastDirectory = ".";
    protected DashboardTabbedPane tabbedDashboardPane;
    private boolean _vertical = false;
    
    public Component getElmisDashboardPanel() {
        GadgetManager manager = new GadgetManager() {
            @Override
            protected boolean validateGadgetDragging(Gadget gadget, Container targetContainer) {
                if ("TakeNotes".equals(gadget.getName()) && targetContainer instanceof JComponent) {
                    StandardDialog dialog;
                    dialog = new GadgetDragDialog();
                    dialog.setLocationRelativeTo(targetContainer);
                    dialog.pack();
                    dialog.setVisible(true);
                    return dialog.getDialogResult() == StandardDialog.RESULT_AFFIRMED;
                }
                else if ("Notifications".equals(gadget.getName()) && targetContainer instanceof JComponent) {
                    StandardDialog dialog;
                    dialog = new GadgetDragDialog();
                    dialog.setLocationRelativeTo(targetContainer);
                    dialog.pack();
                    dialog.setVisible(true);
                    return dialog.getDialogResult() == StandardDialog.RESULT_AFFIRMED;
                }
                	
                return super.validateGadgetDragging(gadget, targetContainer);
            }
        };
        //manager.addGadget(createGadget("Calculator"));
        manager.addGadget(createGadget("Call"));
        manager.addGadget(createGadget("Clock"));
        manager.addGadget(createGadget("Find"));
        manager.addGadget(createGadget("Image"));
        //manager.addGadget(createGadget("Network"));
        manager.addGadget(createGadget("Notifications"));
        manager.addGadget(createGadget("TakeNotes"));
        manager.addGadget(createGadget("Bargraph"));
        manager.setColumnResizable(true);

        tabbedDashboardPane = new DashboardTabbedPane(manager) {
            @Override
            protected Container createToolBarComponent() {
                // uncomment this to see an extra button on the toolbar area
            	Container toolBarComponent = super.createToolBarComponent();
               toolBarComponent.add(new IndicatorProductButton("Manage Indicator Products"));
                //return super.createToolBarComponent();
               return toolBarComponent;
            }

            @Override
            protected GadgetPalette createGadgetPalette() {
                GadgetPalette palette = new GadgetPalette(getGadgetManager(), this) {
                    @Override
                    protected String getResourceString(String key) {
                        if (_vertical && "GadgetPalette.hint".equals(key)) {
                            return "...";
                        }
                        return super.getResourceString(key);
                    }
                };
                if (_vertical) {
                    palette.setButtonLayout(new GridLayout(0, 1));
                }
                else {
                    palette.setButtonLayout(new GridLayout(1, 0));
                }
//                palette.setButtonLayout(new GridLayout(2, 0)); // you could uncomment this to have two rows of gadget buttons
                return palette;
            }
        };
        tabbedDashboardPane.setPreferredSize(new Dimension(1110, 700));

        final Dashboard dashBoard = tabbedDashboardPane.createDashboard("Home Page");        
        dashBoard.setColumnCount(3);
        dashBoard.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        tabbedDashboardPane.setUseFloatingPalette(false);
        tabbedDashboardPane.getGadgetManager().addDashboard(dashBoard);
        manager.addGadgetListener(new GadgetListener() {
            public void eventHappened(GadgetEvent e) {
                if (e.getID() == GadgetEvent.GADGET_COMPONENT_MAXIMIZED) {
                    GadgetComponent gadgetComponent = e.getGadgetComponent();
                    while (gadgetComponent instanceof ResizableGadgetComponent) {
                        gadgetComponent = ((ResizableGadgetComponent) gadgetComponent).getActualGadgetComponent();
                    }
                    if (gadgetComponent instanceof CollapsiblePaneGadget) {
                        ((CollapsiblePaneGadget) gadgetComponent).setMaximized(true);
                    }
                }
                else if (e.getID() == GadgetEvent.GADGET_COMPONENT_RESTORED) {
                    GadgetComponent gadgetComponent = e.getGadgetComponent();
                    while (gadgetComponent instanceof ResizableGadgetComponent) {
                        gadgetComponent = ((ResizableGadgetComponent) gadgetComponent).getActualGadgetComponent();
                    }
                    if (gadgetComponent instanceof CollapsiblePaneGadget) {
                        ((CollapsiblePaneGadget) gadgetComponent).setMaximized(false);
                    }
                }
            }
        });
              
       
        try {
			DashboardPersistenceUtils.load(tabbedDashboardPane, "dash_saved");
		} catch (ParserConfigurationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SAXException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

        return tabbedDashboardPane;

    }
    
    protected AbstractGadget createGadget(String key) {
        AbstractGadget dashboardElement = new AbstractGadget(key,
                IconsFactory.getImageIcon(DashboardJD.class, "icons/" + key.toLowerCase() + "_32x32.png"),
                IconsFactory.getImageIcon(DashboardJD.class, "icons/" + key.toLowerCase() + "_64x64.png")) {
            public GadgetComponent createGadgetComponent() {
                final CollapsiblePaneGadget gadget = new CollapsiblePaneGadget(this);
                gadget.getContentPane().setLayout(new BorderLayout());
                gadget.getContentPane().setBorder(BorderFactory.createEmptyBorder(6, 6, 6, 6));
                if (getKey().startsWith("Calculator")) {
                    gadget.getContentPane().add(GadgetFactory.createCalculator());
                }
                else if (getKey().startsWith("Call")) {
                    gadget.getContentPane().add(GadgetFactory.createCalendar());
                }
                else if (getKey().startsWith("TakeNotes")) {
                    gadget.getContentPane().add(GadgetFactory.takeNotes());
                }
                else if (getKey().startsWith("Find")) {
                    gadget.getContentPane().add(GadgetFactory.createFind());
                }
                else if (getKey().startsWith("Notifications")) {
                   // gadget.getContentPane().add(GadgetFactory.createPieChart());
                	 gadget.getContentPane().add(GadgetFactory.createNotifications());

                   /* class PieChartRunnable implements Runnable{
                    	BlockingQueue<JComponent> chunks = new ArrayBlockingQueue<>(1);
                    	JComponent panel = null;
                    	int i = 0;
						@Override
						public void run() {

							panel = GadgetFactory.createPieChart();
							chunks.add(panel);
							System.out.println("Pie "+i);
							i++;
							process();
						}
                    	public void process()
                    	{
                    		gadget.getContentPane().removeAll();
            				try {
								gadget.getContentPane().add(chunks.take());
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
            				gadget.getContentPane().invalidate();
            				gadget.getContentPane().validate();
                    	}
                    }
                    PieChartRunnable p = new PieChartRunnable();
                    pieChartSchedular.scheduleAtFixedRate(p, 0, 10, TimeUnit.SECONDS);*/
                }
                else if (getKey().startsWith("Bargraph")) {
                	
                	gadget.getContentPane().add(GadgetFactory.createBarGraph());
                	
                	  /*class ProductIndicatorWorker extends SwingWorker<Void, JComponent> {
              			protected Void doInBackground() {
              				JComponent panel = null;
              				
              				try {int i = 0; 
              					while (true)
              					{System.out.println("Call up date bar "+i);
              					i++;
              						Thread.sleep(10000);
              						panel = GadgetFactory.createBarGraph();
              						System.out.println("Bargrapgh "+Thread.currentThread().getId());
              						publish(panel);
              					}
							} catch (InterruptedException e) {
								
								//
							}
              				return null;
              			}

              			@Override
						protected void process(List<JComponent> chunks) {
              				gadget.getContentPane().removeAll();
              				gadget.getContentPane().add(chunks.get(chunks.size() - 1));
              				gadget.getContentPane().invalidate();
              				gadget.getContentPane().validate();
						}

						protected void done() {
              			}
              		}

              		new ProductIndicatorWorker().execute();
*/
                	
                    class BarChartRunnable implements Runnable{
                    	BlockingQueue<JComponent> chunks = new ArrayBlockingQueue<>(1);
                    	JComponent panel = null;
                    	int i = 0;
						@Override
						public void run() {

							panel = GadgetFactory.createBarGraph();
							chunks.add(panel);
							System.out.println("Bar "+i);
							i++;
							process();
						}
                    	public void process()
                    	{
                    		gadget.getContentPane().removeAll();
            				try {
								gadget.getContentPane().add(chunks.take());
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
            				gadget.getContentPane().invalidate();
            				gadget.getContentPane().validate();
                    	}
                    }
                    BarChartRunnable p = new BarChartRunnable();
                    barGraphSchedular.scheduleAtFixedRate(p, 0, 10, TimeUnit.SECONDS);
                }
                else if (getKey().startsWith("Clock")) {
                    gadget.getContentPane().add(GadgetFactory.createClock());
                }
                else {
                    gadget.getContentPane().setPreferredSize(new Dimension(200, 100 + (int) (Math.random() * 100)));
                }
                CollapsiblePaneTitleButton toolButton = new CollapsiblePaneTitleButton(gadget, IconsFactory.getImageIcon(DashboardJD.class, "icons/gadget_tool.png"));
                toolButton.addActionListener(new AbstractAction() {
                    private static final long serialVersionUID = -6649978301193237112L;

                    public void actionPerformed(ActionEvent e) {
                        JOptionPane.showMessageDialog(gadget, "Settings dialog goes here!");
                    }
                });
                gadget.addButton(toolButton, 1);
                gadget.setMessage("Last update on " + ObjectConverterManager.toString(Calendar.getInstance()));
                final ResizableGadgetComponent actualGadgetComponent = new ResizableGadgetComponent(gadget);
                gadget._maximizeButton.addActionListener(new AbstractAction() {
                    private static final long serialVersionUID = 7214349428761246735L;

                    public void actionPerformed(ActionEvent e) {
                        actualGadgetComponent.getGadget().getGadgetManager().maximizeGadgetComponent(actualGadgetComponent);
                        gadget.setMaximized(true);
                    }
                });
                gadget._restoreButton.addActionListener(new AbstractAction() {
                    private static final long serialVersionUID = 1610272975150978358L;

                    public void actionPerformed(ActionEvent e) {
                        actualGadgetComponent.getGadget().getGadgetManager().restoreGadgetComponent();
                        gadget.setMaximized(false);
                    }
                });
                return actualGadgetComponent;
            }

            public void disposeGadgetComponent(GadgetComponent component) {
                // do nothing in this case as we didn't allocate any resource in createGadgetComponent.
            }
        };
        dashboardElement.setDescription("Description is " + key);
        return dashboardElement;
    }

    public class GadgetDragDialog extends StandardDialog {
        public GadgetDragDialog() throws HeadlessException {
            super((Frame) null, "Gadget Drag and Drop");
        }

        @Override
        public JComponent createBannerPanel() {
            return null;
        }

        @Override
        protected StandardDialogPane createStandardDialogPane() {
            return new DefaultStandardDialogPane() {
                @Override
                protected void layoutComponents(Component bannerPanel, Component contentPanel, ButtonPanel buttonPanel) {
                    setLayout(new JideBoxLayout(this, BoxLayout.Y_AXIS));
                    if (bannerPanel != null) {
                        add(bannerPanel);
                    }
                    if (contentPanel != null) {
                        add(contentPanel);
                    }
                    add(buttonPanel, JideBoxLayout.FIX);
                }
            };
        }

        @Override
        public JComponent createContentPanel() {
            JPanel panel = new JPanel(new BorderLayout(10, 10));
            panel.setBorder(BorderFactory.createEmptyBorder(20, 40, 40, 40));

            JLabel label = new JLabel("Allow drop the gadget to this location?");
            label.setHorizontalAlignment(SwingConstants.CENTER);

            panel.add(label, BorderLayout.CENTER);
            return panel;
        }

        @Override
        public ButtonPanel createButtonPanel() {
            ButtonPanel buttonPanel = new ButtonPanel();
            JButton okButton = new JButton();
            JButton cancelButton = new JButton();
            okButton.setName(OK);
            cancelButton.setName(CANCEL);
            buttonPanel.addButton(okButton, ButtonPanel.AFFIRMATIVE_BUTTON);
            buttonPanel.addButton(cancelButton, ButtonPanel.CANCEL_BUTTON);

            okButton.setAction(new AbstractAction(UIDefaultsLookup.getString("OptionPane.okButtonText")) {
                private static final long serialVersionUID = -8839840510614782122L;

                public void actionPerformed(ActionEvent e) {
                    setDialogResult(RESULT_AFFIRMED);
                   /* try {
            			DashboardPersistenceUtils.save(tabbedDashboardPane, "dash_saved");
            		} catch (ParserConfigurationException e1) {
            			// TODO Auto-generated catch block
            			e1.printStackTrace();
            		}  catch (IOException e1) {
            			// TODO Auto-generated catch block
            			e1.printStackTrace();
            		}*/
                    setVisible(false);
                    System.out.println("Saving location");
                    dispose();
                }
            });
            cancelButton.setAction(new AbstractAction(UIDefaultsLookup.getString("OptionPane.cancelButtonText")) {
                private static final long serialVersionUID = -2467038074716992814L;

                public void actionPerformed(ActionEvent e) {
                    setDialogResult(RESULT_CANCELLED);
                    setVisible(false);
                    dispose();
                }
            });

            setDefaultCancelAction(cancelButton.getAction());
            setDefaultAction(okButton.getAction());
            getRootPane().setDefaultButton(okButton);
            buttonPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
            buttonPanel.setSizeConstraint(ButtonPanel.NO_LESS_THAN); // since the checkbox is quite wide, we don't want all of them have the same size.
            return buttonPanel;
        }
    }
    
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		com.jidesoft.utils.Lm.verifyLicense("Joe Banda", "OpenLMIS","ETAcp2S4Q.dTxOWhjQ1aoCDjZ:Owgrn2");
		try {
			DashboardJD dialog = new DashboardJD(new javax.swing.JFrame(), true);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public DashboardJD(java.awt.Frame parent, boolean modal) {
		
		super(parent, modal);
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim = toolkit.getScreenSize();
		//setPreferredSize(new Dimension(dim.width, dim.height - 100));
		//setBounds(100, 100, 450, 300);
		this.setSize(new Dimension(dim.width, dim.height - 100));
		this.setLocationRelativeTo(null);
		
		//this.setSize(AppJFrame.desktop.getWidth(), AppJFrame.desktop.getHeight());
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		{
			contentPanel.add( getElmisDashboardPanel() );
			/*JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			*/
			setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
			addWindowListener(new WindowAdapter() {
	            @Override
	            public void windowClosing(WindowEvent e) {
	                int ack = JOptionPane.showConfirmDialog(DashboardJD.this, "Closing dashboard?", "Confirm exit", JOptionPane.YES_NO_OPTION);
	                if (ack == JOptionPane.YES_OPTION) {
	                	dispose();
	                	barGraphSchedular.shutdown();
	                	pieChartSchedular.shutdown();
	                }
	            }

	        });
		}
	}
	class IndicatorProductButton extends JideButton
	{
		public IndicatorProductButton(String btnTitle)
		{
			super(btnTitle);
			setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

			addActionListener(new ActionListener()
			{
				@Override
				public void actionPerformed(ActionEvent e) {
					IndicatorProductJd indicatorProductJd = new IndicatorProductJd(null, true);
					indicatorProductJd.setVisible(true); 
				}

			});
		}
	}

}
