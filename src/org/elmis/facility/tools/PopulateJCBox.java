/**
 * 
 */
package org.elmis.facility.tools;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.model.Dispensing_point;
import org.elmis.facility.domain.model.Roles;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.Program;
import org.elmis.facility.tools.DPoints;



/**
 * @author JBanda
 * 
 */
public class PopulateJCBox {

	private static List<Roles> rolesList;
	public static Integer roleIdList[] = new Integer[0];

	private static List<Dispensing_point> dpList;
	private static List<Program> dpList1;
	private static List<Dispensing_point> dpList2;
	private static Integer locationList[] = new Integer[0];
	public static Integer SiteID;

	/**
	 * 
	 */
	public PopulateJCBox() {
		// TODO Auto-generated constructor stub
	}

	public static javax.swing.ComboBoxModel FillRolesJCBox() {

		String cmbList[] = new String[0];
		javax.swing.ComboBoxModel cModel;
		// javax.swing.JComboBox jcomboBox;
		int TotalRow = 0;
		int rowNum = 1;

		// get list of roles

		SqlSessionFactory factory = new MyBatisConnectionFactory()
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			rolesList = session.selectList("selectByRoles");// TODO add the
															// order by clause

		} finally {
			session.close();
		}

		if (rolesList.size() > 0) {

			cmbList = new String[rolesList.size() + 1];
			cmbList[0] = "Please Select Role";

			roleIdList = new Integer[rolesList.size() + 1];

			for (Roles roles : rolesList) {

				cmbList[rowNum] = roles.getName();
				roleIdList[rowNum] = roles.getId();

				rowNum++;

			}

			// cmbList[rowNum++] = "Consultation";
			// cmbList[rowNum++] = "New Matter";
		} else {
			cmbList = new String[TotalRow + 1];
			cmbList[0] = "Add New Role";
		}

		TotalRow = 0;
		cModel = new javax.swing.DefaultComboBoxModel(cmbList);

		return cModel;
	}

	/**************************************************************/
	/*DISPENSING SITES*/

	public static javax.swing.ComboBoxModel FillLocationJCBox() {

		String cmbList[] = new String[0];
		javax.swing.ComboBoxModel cModel;
		// javax.swing.JComboBox jcomboBox;
		int TotalRow = 0;
		int rowNum = 0;

		new MyBatisConnectionFactory();
		SqlSessionFactory factory = MyBatisConnectionFactory
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			dpList = session.selectList("selectBydp");

		} finally {
			session.close();
		}

		if (dpList.size() > 0) {

			cmbList = new String[dpList.size()];
			cmbList[0] = "Please Select Dispensing Site";

			locationList = new Integer[dpList.size()];

			for (Dispensing_point dp : dpList) {

				cmbList[rowNum] = dp.getName();
				//locationList[rowNum] = dp.g

				rowNum++;

			}

			// cmbList[rowNum++] = "Consultation";
			// cmbList[rowNum++] = "New Matter";
		} else {
			cmbList = new String[TotalRow];
			cmbList[0] = "Add Site Location";
		}

		TotalRow = 0;
		cModel = new javax.swing.DefaultComboBoxModel(cmbList);

		return cModel;
	}
	
	/********************************************************************/
	/*PROGRAM AREAS*/
	public static javax.swing.ComboBoxModel FillPAreasJCBox() {

		String cmbList[] = new String[0];
		javax.swing.ComboBoxModel cModel;
		// javax.swing.JComboBox jcomboBox;
		int TotalRow = 0;
		int rowNum = 0;

		new MyBatisConnectionFactory();
		SqlSessionFactory factory = MyBatisConnectionFactory
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			dpList1 = session.selectList("selectByPA");

		} finally {
			session.close();
		}

		if (dpList1.size() > 0) {

			cmbList = new String[dpList1.size()];
			cmbList[0] = "Please Select Program Area";

			locationList = new Integer[dpList1.size()];

			for (Program dp : dpList1) {

				cmbList[rowNum] = dp.getProgramCode();
				//locationList[rowNum] = dp.g

				rowNum++;

			}

			// cmbList[rowNum++] = "Consultation";
			// cmbList[rowNum++] = "New Matter";
		} else {
			cmbList = new String[TotalRow];
			cmbList[0] = "Add Program Area";
		}

		TotalRow = 0;
		cModel = new javax.swing.DefaultComboBoxModel(cmbList);

		return cModel;
	}
	
	/*******************************************************************/
	/* DISPENSING POINTS    */
	public static javax.swing.ComboBoxModel FillDPointsJCBox(Integer SiteId) {

		String cmbList[] = new String[0];
		javax.swing.ComboBoxModel cModel;
		// javax.swing.JComboBox jcomboBox;
		int TotalRow = 0;
		int rowNum = 0;
		SiteID=SiteId;
		new MyBatisConnectionFactory();
		SqlSessionFactory factory = MyBatisConnectionFactory
				.getSqlSessionFactory();

		SqlSession session = factory.openSession();

		try {

			dpList2 = session.selectList("selectBydp2",SiteID);

		} finally {
			session.close();
		}

		if (dpList2.size() > 0) {

			cmbList = new String[dpList2.size()];
			cmbList[0] = "Please Select Dispensing Point";

			locationList = new Integer[dpList2.size()];

			for (Dispensing_point dp : dpList2) {

				cmbList[rowNum] = dp.getDispensing_point_name();
				//locationList[rowNum] = dp.g

				rowNum++;

			}

			// cmbList[rowNum++] = "Consultation";
			// cmbList[rowNum++] = "New Matter";
		} else {
			cmbList = new String[TotalRow];
			//cmbList[0] = "Select Dispensing Point";
		}

		TotalRow = 0;
		cModel = new javax.swing.DefaultComboBoxModel(cmbList);

		return cModel;
	}
	
}
