package org.elmis.facility.json;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.codehaus.jackson.map.ObjectMapper;
import org.elmis.facility.domain.dao.ProcessingPeriodsDao;
import org.elmis.facility.domain.dao.ReportLineItemDao;
import org.elmis.facility.domain.model.Processing_Periods;
import org.elmis.facility.json.beans.LossAdjustments;
import org.elmis.facility.json.beans.ReportBean;
import org.elmis.facility.json.beans.Type;
import org.elmis.facility.reporting.model.ReportLineItem;
import org.elmis.facility.webservice.request.WebServiceRequestService;
import org.elmis.facility.ws.client.LocalWSClient;
import org.elmis.facility.ws.client.RnRRequestResult;
import org.elmis.forms.regimen.util.ElmisDateUtil;

public class GenerateJason {

	public void generateJasonString(Date fromDate, Date toDate, int programCode, ReportBean reportBean)
	{
		List<Processing_Periods> periods = new ProcessingPeriodsDao().selectPeriodByRange(ElmisDateUtil.addDays(fromDate, 1), ElmisDateUtil.addDays(toDate, -1)); 
		Integer periodId = (periods != null && periods.size() > 0)? periods.get(0).getId():null;
		//reportBean.setPeriodId(periodId);
		reportBean.setPeriodId(6);
		List<ReportLineItem> lineItemList = new ReportLineItemDao().getLineItems(fromDate, toDate, programCode);
		
		for (ReportLineItem r: lineItemList)
		{
			r.setReasonForRequestedQuantity("RnR");
			r.setNewPatientCount(1);
			r.setStockInHand(1);
		}
		reportBean.setProducts(lineItemList);
		ObjectMapper mapper = new ObjectMapper();

		try {

			// convert user object to json string, and save to a file
			//mapper.writeValue(new File("d:\\user.json"),reportBean);
			String jsonr = mapper.writeValueAsString(reportBean);
			System.out.println(jsonr);
			// display to console
			if(reportBean.reportIsValid()){
				String json = mapper.writeValueAsString(reportBean);
				System.out.println(json);
				//mesayrnr
				RnRRequestResult result = new LocalWSClient().sendRnR(json);
				JOptionPane.showMessageDialog(null, result.getRemark());
			} else {
				JOptionPane.showMessageDialog(null, "R&R has errors");
			}
			//BaseFactory.uploadJSON("sdp-requisitions", "R&R", json, Long.class);
		} catch (IOException e) {

			e.printStackTrace();
		}
		catch (Exception e)
		{
			System.out.println(">>> "+e.getLocalizedMessage());
		}
	}
}
