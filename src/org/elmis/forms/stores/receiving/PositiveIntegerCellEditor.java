package org.elmis.forms.stores.receiving;

import java.awt.Color;
import java.awt.Component;

import javax.swing.DefaultCellEditor;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
public class PositiveIntegerCellEditor extends DefaultCellEditor  {
	

	

	        Class[] argTypes = new Class[]{String.class};
	        java.lang.reflect.Constructor constructor;
	        Object value;

	        public PositiveIntegerCellEditor() {
	            super(new JTextField());
	            getComponent().setName("Table.editor");
	            ((JTextField)getComponent()).setHorizontalAlignment(JTextField.RIGHT);
	        }

	        public boolean git () {
	            String s = (String)super.getCellEditorValue();
	            if ("".equals(s)) {
	                if (constructor.getDeclaringClass() == String.class) {
	                    value = s;
	                }
	                super.stopCellEditing();
	            }

	            try {
	                value = constructor.newInstance(new Object[]{s});
	                if (value instanceof Number && ((Number) value).doubleValue() > 0)
	                {
	                    return super.stopCellEditing();
	                } else {
	                    throw new RuntimeException("Input must be a positive number."); 
	                }
	            }
	            catch (Exception e) {
	                ((JComponent)getComponent()).setBorder(new LineBorder(Color.red));
	                return false;
	            }
	        }

	        public Component getTableCellEditorComponent(JTable table, Object value,
	                                                 boolean isSelected,
	                                                 int row, int column) {
	            this.value = null;
	            ((JComponent)getComponent()).setBorder(new LineBorder(Color.black));
	            try {
	                Class type = table.getColumnClass(column);
	                if (type == Object.class) {
	                    type = String.class;
	                }
	                constructor = type.getConstructor(argTypes);
	            }
	            catch (Exception e) {
	                return null;
	            }
	            return super.getTableCellEditorComponent(table, value, isSelected, row, column);
	        }

	        public Object getCellEditorValue() {
	            return value;
	        }
	        
	        
	        
	    }


