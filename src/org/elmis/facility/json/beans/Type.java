package org.elmis.facility.json.beans;

public class Type {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
