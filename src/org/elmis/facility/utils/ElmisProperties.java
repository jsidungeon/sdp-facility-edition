package org.elmis.facility.utils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Properties;

/**
 * ElmisProperties.java
 * Purpose: Loads Postgres specific property file into the system
 * @author Michael Mwebaze
 * @version 1.0
 */
public class ElmisProperties {
	/**
	 * 
	 * @return System and Postgres related properties set in the ./Programmproperties.properties file
	 */
	public static Properties getElmisProperties()
	{
		Properties props = System.getProperties();
		/*try{FileInputStream fis = new FileInputStream("Programmproperties.properties"); 
			props = new Properties(System.getProperties());
			props.load(fis);
			System.setProperties(props);
			return props;

		} catch (IOException e) {
			e.printStackTrace();
		}*/
		return props;
	}
	public static Properties getHyperSqlProperties()
	{
		Properties props = null;
		try{FileInputStream fis = new FileInputStream("Config/hsql.properties"); 
			props = new Properties(System.getProperties());
			props.load(fis);
			System.setProperties(props);
			return props;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
	public static Properties getSqliteProperties()
	{
		Properties props = null;
		try{FileInputStream fis = new FileInputStream("Config/sqlite.properties"); 
			props = new Properties(System.getProperties());
			props.load(fis);
			System.setProperties(props);
			return props;

		} catch (IOException e) {
			e.printStackTrace();
		}
		return props;
	}
	public static void saveFacilitySettings(String province, String district, String facility, String facilityCode, String typeId, String facilityId)
	{
		Properties properties = new Properties();
		OutputStream output = null;
		FileInputStream in = null;
		try{
			in = new FileInputStream("Programmproperties.properties");
			properties.load(in);
			in.close();
			output = new FileOutputStream("Programmproperties.properties");
			
			properties.setProperty("province", province);
			properties.setProperty("district", district);
			properties.setProperty("facilityname", facility);
			properties.setProperty("facilityCode", facilityCode);
			properties.setProperty("facilityTypeId", typeId);
			//Below has been commented for purposes of enabling the Deployment training progress
			//properties.setProperty("facilityid", facilityId);
			
			properties.store(output, null);
			System.setProperty("facilityCode", facilityCode);
			//Below has been commented for purposes of enabling the Deployment training progress
			//System.setProperty("facilityid", facilityId);
		}
		catch (IOException io) {
			System.out.println(io.getLocalizedMessage());
		}
		finally{
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static void saveSetupSettings(String province, String district, String facility, String domain, String username, String password, String database)
	{
		Properties properties = new Properties();
		OutputStream output = null;
		FileInputStream in = null;
		try{
			in = new FileInputStream("Programmproperties.properties");
			properties.load(in);
			in.close();
			output = new FileOutputStream("Programmproperties.properties");
			
			properties.setProperty("province", province);
			properties.setProperty("district", district);
			properties.setProperty("facilityname", facility);
			
			properties.setProperty("dbuser", username);
			properties.setProperty("dbname", database);
			properties.setProperty("dbpassword", password);
			//properties.setProperty("dburl", "jdbc\\:postgresql\\://"+domain+"\\:5432/"+database);
			properties.setProperty("dburl", "jdbc:postgresql://"+domain+":5432/"+database);
			properties.setProperty("dbParentServer", domain);
			properties.setProperty("dbhost", domain);
			
			properties.setProperty("facilitySetup", "yes");
			
			properties.store(output, null);
		}
		catch (IOException io) {
			System.out.println(io.getLocalizedMessage());
		}
		finally{
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static void saveDatabaseServerSettings(String domain, String username, String password, String database)
	{
		Properties properties = new Properties();
		OutputStream output = null;
		FileInputStream in = null;
		try{
			in = new FileInputStream("Programmproperties.properties");
			properties.load(in);
			in.close();
			output = new FileOutputStream("Programmproperties.properties");
			
			properties.setProperty("dbuser", username);
			properties.setProperty("dbname", database);
			properties.setProperty("dbpassword", password);
			//properties.setProperty("dburl", "jdbc\\:postgresql\\://"+domain+"\\:5432/"+database);
			properties.setProperty("dburl", "jdbc:postgresql://"+domain+":5432/"+database);
			properties.setProperty("dbParentServer", domain);
			properties.setProperty("dbhost", domain);
			
			properties.setProperty("facilitySetup", "yes");
			properties.setProperty("clientType", "Networked");
			properties.store(output, null);
		}
		catch (IOException io) {
			System.out.println(io.getLocalizedMessage());
		}
		finally{
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static void changeDispensary(String dispensaryName)
	{
		System.setProperty("dp_name", dispensaryName);
		Properties properties = new Properties();
		OutputStream output = null;
		FileInputStream in = null;
		try{
			in = new FileInputStream("Programmproperties.properties");
			properties.load(in);
			in.close();
			output = new FileOutputStream("Programmproperties.properties");
			
			properties.setProperty("dp_name", dispensaryName);
			properties.store(output, null);
		}
		catch (IOException io) {
			System.out.println(io.getLocalizedMessage());
		}
		finally{
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	private static void changeVersionNumber(){
		java.util.Date date = new java.util.Date();
		Timestamp versionTimestamp = new Timestamp(date.getTime());
		Properties properties = new Properties();
		OutputStream output = null;
		FileInputStream in = null;
		try{
			in = new FileInputStream("Programmproperties.properties");
			properties.load(in);
			in.close();
			output = new FileOutputStream("Programmproperties.properties");
			
			properties.setProperty("version", versionTimestamp.toString());
			properties.store(output, null);
		}
		catch (IOException io) {
			System.out.println(io.getLocalizedMessage());
		}
		finally{
			if (output != null) {
				try {
					output.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public static void main(String[] args) {
		changeVersionNumber();
	}
}
