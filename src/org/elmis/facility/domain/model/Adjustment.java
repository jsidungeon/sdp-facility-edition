package org.elmis.facility.domain.model;

import java.sql.Timestamp;

public class Adjustment {

	private int id;
	private Timestamp timestamp;
	private int siteId;
	public int getSiteId() {
		return siteId;
	}
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getAdjustmentRemark() {
		return adjustmentRemark;
	}
	public void setAdjustmentRemark(String adjustmentRemark) {
		this.adjustmentRemark = adjustmentRemark;
	}

	public double getAdjustmentQty() {
		return adjustmentQty;
	}
	public void setAdjustmentQty(double positiveAdjustment) {
		this.adjustmentQty = positiveAdjustment;
	}
	public String getTransferFrom() {
		return transferFrom;
	}
	public void setTransferFrom(String transferFrom) {
		this.transferFrom = transferFrom;
	}
	public String getTransferTo() {
		return transferTo;
	}
	public void setTransferTo(String transferTo) {
		this.transferTo = transferTo;
	}
	
	private String productCode;
	private String adjustmentRemark;
	private double adjustmentQty;
	private String transferFrom;
	private String transferTo;
	private String adjustmentType;
	private Boolean additive;
	
	public String getAdjustmentType() {
		return adjustmentType;
	}
	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
	private String dispensaryName;
	public String getDispensaryName() {
		return dispensaryName;
	}
	public void setDispensaryName(String dispensaryName) {
		this.dispensaryName = dispensaryName;
	}
	public Boolean getAdditive() {
		return additive;
	}
	public void setAdditive(Boolean additive) {
		this.additive = additive;
	}
}
