package org.elmis.facility.dao;
import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.domain.dao.Elmis_Dar_TransactionsMapper;
import org.elmis.facility.domain.dao.Elmis_PatientMapper;
import org.elmis.facility.domain.dao.HivTestResultDao;
import org.elmis.facility.domain.dao.Elmis_Stock_Control_CardMapper;
import org.elmis.facility.domain.dao.SP_SystemcalculatedproductsbalanceMapper;
import org.elmis.facility.domain.dao.artdispenseProductQtyMapper;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.Facility_Operators;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.Geographic_Zones;
import org.elmis.facility.domain.model.Losses_Adjustments_Types;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Elmis_Patient;
import org.elmis.facility.domain.model.Elmis_Dar_Transactions;
import org.elmis.facility.domain.model.Shipped_Line_Items;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.network.MyBatisConnectionFactory;
public class FacilityStoreProductBalanceDAO {
	
	private String stra ;
	private SqlSessionFactory sqlSessionFactory; 
		
	public FacilityStoreProductBalanceDAO(){
		
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();

	}
	
	
	
	
	
	
	
	
	
		
	public  List<Elmis_Stock_Control_Card>
	dogetcurrentstoreproductbalances (String valueA) {
		SqlSession session = sqlSessionFactory.openSession();
		try{
			Elmis_Stock_Control_CardMapper mapper = session.getMapper(Elmis_Stock_Control_CardMapper.class);
     List<Elmis_Stock_Control_Card> listbal = mapper.selectfinalstoreproductbalance(valueA);  
     return listbal;
		}finally{
			
			session.close();
		}
     }
}
