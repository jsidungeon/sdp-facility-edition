/**
 * this file reads and writes to the network properties file
 */
package org.elmis.facility.tools;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.elmis.facility.network.NetworkPropertiesFile;
import org.elmis.facility.utils.ElmisAESencrpDecrp;

/**
 * @author JBanda
 * 
 */
public class NetworkProperties {

	public static Properties prop;
	private FileInputStream fis;
	private FileOutputStream fos;

	public static String dbType;
	public static String dbhost;
	public static String dbport;
	public static String dbname;
	public static String dbuser;
	public static String dbpassword;
	private NetworkPropertiesFile details;

	/**
	  * 
	  */
	public NetworkProperties() {
		// TODO Auto-generated constructor stub
	}

	public boolean writeToPropertiesFile(NetworkPropertiesFile details) {

		this.details = details;

		// prop = new Properties(System.getProperties());

		prop = System.getProperties();

		if (this.details.getdbMode().equalsIgnoreCase("Server")) {

			prop.setProperty("dbdriver", this.details.getdbdriver());
			prop.setProperty("dbhost", this.details.getdbhost());
			prop.setProperty("dbport", this.details.getdbport());
			prop.setProperty("dbname", this.details.getdbname());
			prop.setProperty("dbuser", this.details.getdbuser());
			try {
				prop.setProperty("dbpassword", ElmisAESencrpDecrp
						.encrypt(this.details.getdbpassword()));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			prop.setProperty("dbMode", this.details.getdbMode());
			prop.setProperty("dbParentServer", this.details.getdbParentServer());
			prop.setProperty("clientType", this.details.getClientType());

		} else if (this.details.getdbMode().equalsIgnoreCase("Local")) {
			prop.setProperty("dbMode", this.details.getdbMode());
			prop.setProperty("clientType", this.details.getClientType());

		}
		try {

			// javax.swing.JOptionPane.showMessageDialog(null," write 1");

			prop.store((fos = new FileOutputStream(
					"Programmproperties.properties")), "Author: jb");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}

	public boolean writeToPropertiesFile() {

		// prop = new Properties(System.getProperties());
		// javax.swing.JOptionPane.showMessageDialog(null,"  client "+
		// System.getProperty("clientType"));
		prop = System.getProperties();

		try {

			// javax.swing.JOptionPane.showMessageDialog(null," write 2");

			prop.store((fos = new FileOutputStream(
					"Programmproperties.properties")), "Author: jb");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}

	public boolean writeToPropertiesFile(String key, String value) {

		// prop = new Properties(System.getProperties());
		// javax.swing.JOptionPane.showMessageDialog(null,"  client "+
		// System.getProperty("clientType"));
		prop = System.getProperties();

		try {
			prop.setProperty(key, value);
			// javax.swing.JOptionPane.showMessageDialog(null," write 2");

			prop.store((fos = new FileOutputStream(
					"Programmproperties.properties")), "Author: jb");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		return true;

	}

	public Properties readPropertiesFile() {

		// Properties file

		prop = new Properties(System.getProperties());
		try {
			fis = new FileInputStream("Programmproperties.properties");
			prop.load(fis);

			System.setProperties(prop);

			fis.close();

			// System.getProperties().list(System.out);

		} catch (IOException e) {
			e.printStackTrace();
		}

		dbType = prop.getProperty("dbType");

		dbhost = prop.getProperty("dbhost");

		dbport = prop.getProperty("dbport");
		dbname = prop.getProperty("dbname");
		dbuser = prop.getProperty("dbuser");
		dbpassword = prop.getProperty("dbpassword");

		return prop;
	}

}
