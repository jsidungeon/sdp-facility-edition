package org.elmis.facility.database.connection;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.utils.ElmisProperties;
/**
 * MyBatisConnectionFactory.java
 * Purpose: Manages all connection resources (Opening & closing) database(s). 
 * @author Michael Mwebaze
 *@version 1.0
 */
public class MyBatisHSQLConnectionFactory {

	private static SqlSessionFactory sqlSessionFactory;

	static {
		try {

			//String resource = null;
			//Reader reader = null;

			System.out.println("HSQL DATABASE...");

			/*f (DatabaseSwitcher.switchToHsql())
			{*/
				String resource = "hsql-mybatis-config.xml";

				Reader reader = Resources.getResourceAsReader(resource);

				if (sqlSessionFactory == null) {
					sqlSessionFactory = new SqlSessionFactoryBuilder().build(reader, ElmisProperties.getHyperSqlProperties());
				}
			/*}
			else
			{
				JOptionPane.showMessageDialog(null,
						"<html><b> HSQL DATABASE IS NOT AVAILABLE. <br><font color=red>HSQL NECESSARY FOR PARTS OF THIS SYSTEM TO FUNCTION</font><b></html>",	"HSQL DATABASE CONNECTION ERROR",JOptionPane.WARNING_MESSAGE);
			}*/

		}
		catch (FileNotFoundException fileNotFoundException) {
			fileNotFoundException.printStackTrace();
		}
		catch (IOException iOEx) {
			System.out.println("MyBATIS DB CON ISSUES "+iOEx.getMessage());
		}
	}

	/**
	 * 
	 * @return an SqlSessionFactory which is used to open or close a database connection resource.
	 * Each open SqlSessionFactory that is opened must after use be closed
	 */
	public static SqlSessionFactory getSqlSessionFactory() {

		return sqlSessionFactory;
	}
}