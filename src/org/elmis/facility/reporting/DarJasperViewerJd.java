package org.elmis.facility.reporting;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JDialog;

import net.sf.jasperreports.view.JRViewer;

public class DarJasperViewerJd extends JDialog {

	/**
	 * Create the dialog.
	 */
	public DarJasperViewerJd(java.awt.Frame parent, boolean modal, JRViewer jrViewer) {
		super(parent, modal);
		//setBounds(100, 100, 450, 300);
		Toolkit toolkit =  Toolkit.getDefaultToolkit ();
		Dimension dim = toolkit.getScreenSize();
		jrViewer.setPreferredSize(new Dimension(dim.width, dim.height - 50));
		add(jrViewer,  BorderLayout.CENTER);
		pack();
	}

}
