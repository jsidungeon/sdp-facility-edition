/**
 * 
 *@Michael Mwebaze Kitobe
 */
package org.elmis.facility.reporting.model;

import java.sql.Date;

/**
 * @author MMwebaze
 *
 */
public class ReportingPeriodRemark {

	@Override
	public String toString() {
		return "ReportingPeriodRemark [id=" + id + ", remarks=" + remarks
				+ ", reportPeriodFrm=" + reportPeriodFrm + ", reportPeriodTo="
				+ reportPeriodTo + ", programCode=" + programCode + "]";
	}
	private int id;
	private String remarks;
	private Date reportPeriodFrm;
	private Date reportPeriodTo;
	private int programCode;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public Date getReportPeriodFrm() {
		return reportPeriodFrm;
	}
	public void setReportPeriodFrm(Date reportPeriodFrm) {
		this.reportPeriodFrm = reportPeriodFrm;
	}
	public Date getReportPeriodTo() {
		return reportPeriodTo;
	}
	public void setReportPeriodTo(Date reportPeriodTo) {
		this.reportPeriodTo = reportPeriodTo;
	}
	public int getProgramCode() {
		return programCode;
	}
	public void setProgramCode(int programCode) {
		this.programCode = programCode;
	}
}
