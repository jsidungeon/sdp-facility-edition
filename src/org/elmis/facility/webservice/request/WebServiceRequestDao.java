package org.elmis.facility.webservice.request;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;
import java.util.UUID;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.domain.dao.WebServiceRequestMapper;

public class WebServiceRequestDao {
	private SqlSessionFactory getSqlMapper(){
		String resource = "SqlMapConfig.xml";
		Reader reader = null;

		SqlSessionFactory sqlMapper = null;
		try {
			reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader,
					getProperties());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sqlMapper;
	}
		
	public void insertReqeuest(WebServiceRequest wsRequest){
		SqlSession session = getSqlMapper().openSession();
		WebServiceRequestMapper mapper = session.getMapper(WebServiceRequestMapper.class);
		
		try {
			mapper.insertRequest(wsRequest);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	private Properties getProperties() {
		Properties prop = new Properties();
		try {
			FileInputStream fis = new FileInputStream(
					"Programmproperties.properties");
			prop.load(fis);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return System.getProperties();
	}
	
	public static void main(String args[]){
		
		WebServiceRequest request = new WebServiceRequest();
		request.setName("submitRnR");
		request.setType("RNR");
		request.setStatus("PENDING");
		request.setParameters("{agentCode:00000}");
		request.setUuid(UUID.randomUUID().toString());
		System.out.println(request.getId());
		new WebServiceRequestDao().insertReqeuest(request);
		System.out.println(request.getId());
	}

}
