package org.elmis.facility.reports.regimen;

import java.util.ArrayList;
import java.util.List;

public class RegimenLine {
	private Integer regimenLineId;
	private String regimenLineName;
	private Long numberOfPatients = 0L;
	private Double percentageOutOfAllPatients;
	
	private List<RegimenReportBean> regimenReportBeans;
	
	public void addReportBean(RegimenReportBean bean){
		if(regimenReportBeans == null){
			regimenReportBeans = new ArrayList<RegimenReportBean>();
		}
		regimenReportBeans.add(bean);
	}
	public Integer getRegimenLineId() {
		return regimenLineId;
	}
	public void setRegimenLineId(Integer regimenLineId) {
		this.regimenLineId = regimenLineId;
	}
	public String getRegimenLineName() {
		return regimenLineName;
	}
	public void setRegimenLineName(String regimenLineName) {
		this.regimenLineName = regimenLineName;
	}
	public List<RegimenReportBean> getRegimenReportBeans() {
		return regimenReportBeans;
	}
	public void setRegimenReportBeans(List<RegimenReportBean> regimenReportBeans) {
		this.regimenReportBeans = regimenReportBeans;
	}
	
	public Long getNumberOfPatients() {
		return numberOfPatients;
	}
	
	public void setNumberOfPatients(Long numberOfPatients) {
		this.numberOfPatients = numberOfPatients;
	}
	
	public Double getPercentageOutOfAllPatients() {
		return percentageOutOfAllPatients;
	}
	
	public void setPercentageOutOfAllPatients(Double percentageOutOfAllPatients) {
		this.percentageOutOfAllPatients = percentageOutOfAllPatients;
	}
}
