/**
 * 
 */
package org.elmis.facility.tools;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.oribicom.tools.NetworkMode;

//XXXXXX import org.hsqldb.Server;

/**
 * @author Joe
 * 
 */
public class LocalMode {

	private static String url = "jdbc:hsqldb:Database/legalclient"; // local
	private static String user = "sa";
	private static String password = "";
	private static Connection conn;
	private static Connection c = null;

	// XXXXXX private Server server;

	public LocalMode() {
		setUp();
	}

	protected void setUp() {
		// System.out.println("Local Mode");

		// XXXXXX server = new Server();
		// XXXXXX server.setDatabaseName(0, db_name);
		// XXXXXX server.setDatabasePath(0, url); // local access
		// XXXXXX server.setLogWriter(null);
		// XXXXXX server.setErrWriter(null);
		// XXXXXX server.setRestartOnShutdown(true);
		// XXXXXX server.start();

		try {
			Class.forName("org.hsqldb.jdbcDriver");
		} catch (Exception e) {
			e.printStackTrace();
			// System.out.println(this + ".setUp() error: " + e.getMessage());
			System.exit(1);
		}

	}

	public static Connection getConn() {

		try {
			c = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			// System.exit(1);
		}

		return c;

	}

	public static Connection getLocalConn() {
		try {
			c = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			// System.exit(1);
		}

		return c;

	}

	public static void shutdown() throws SQLException {

		conn = NetworkMode.getConn();
		Statement st = conn.createStatement();

		// db writes out to files and performs clean shuts down
		// otherwise there will be an unclean shutdown
		// when program ends
		st.execute("SHUTDOWN");
		conn.close(); // if there are no other open connection
	}

}
