/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.elmis.facility.reporting;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumn;

import org.elmis.facility.reporting.model.LaboratoryEquipment;
import org.elmis.facility.reporting.model.Reagent;

/**
 *
 * @author mmwebaze
 */
public class ReagentTableModel extends AbstractTableModel{

	private final static String[] header = {"CODE", "LAB COMMODITY", /*"UNIT",*/ "STATUS"};             
	private List<Reagent> reagentList;

	public ReagentTableModel(List<Reagent> reagentList)
	{
		this.reagentList = reagentList;
	}
	@Override
	public int getRowCount() {
		return reagentList.size();
	}

	@Override
	public int getColumnCount() {
		return header.length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Object value = null;
		Reagent reagent = reagentList.get (rowIndex);
		if (columnIndex == 0)
			return value = reagent.getCommodityCode().trim();
		else if (columnIndex == 1)   
			return value = reagent.getCommodityName().trim();
		else
		{
			if (reagent.isCommodityStatus())
				return "Y";
			else
				return "N";
		}
	}
	@Override
	public String getColumnName(int col)
	{
		return header[col];
	}
	@Override
	public void setValueAt(Object value, int row, int col) {
		String valueStr = value.toString().toUpperCase();
		Reagent reagent = reagentList.get(row);
		if (valueStr.equals("N"))
		reagent.setCommodityStatus(false);
		else
			reagent.setCommodityStatus(true);
		   
		fireTableCellUpdated(row, col);
	}  
	@Override
	public boolean isCellEditable(int row, int column)
	{
		if (column == 2)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
