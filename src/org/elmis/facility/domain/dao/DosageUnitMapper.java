package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.DosageUnit;

public interface DosageUnitMapper {
	List<DosageUnit> selectDosageUnitsByRegimenProductId(Integer regProdId);
	
	DosageUnit selectLastDosageForProduct(@Param("artNumber") String artNo, @Param("productCode") String programCode , @Param("regimenId") Integer regimenId);
	
	Double selectLastQtyForProduct(@Param("artNumber") String artNo, @Param("productCode") String programCode , @Param("regimenId") Integer regimenId);
}
