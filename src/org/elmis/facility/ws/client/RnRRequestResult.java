package org.elmis.facility.ws.client;


public class RnRRequestResult {
	private boolean success;
	private String status;
	private String remark;
	
	public RnRRequestResult(){ };
	public RnRRequestResult(Boolean success){
		this.setSuccess(success);
	}
	
	public RnRRequestResult(Boolean success, String stts , String rmrk){
		this.setSuccess(success);
		this.setStatus(stts);
		this.setRemark(rmrk);
	}
	
	
	 public boolean isSuccess() {
		return success;
	}
	 
	 
	 public void setSuccess(boolean successful) {
		this.success = successful;
	}
	 
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getRemark() {
		return remark;
	}
	
	public void setRemark(String remark) {
		this.remark = remark;
	}
	

}
