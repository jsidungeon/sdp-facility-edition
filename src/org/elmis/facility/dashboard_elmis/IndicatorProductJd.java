package org.elmis.facility.dashboard_elmis;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import org.elmis.facility.domain.dao.IndicatorProductDao;
import org.elmis.facility.domain.dao.ProductDao;
import org.elmis.facility.reporting.model.Product;
import org.elmis.facility.reports.utils.TableColumnAligner;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author MMwebaze
 */
public class IndicatorProductJd extends javax.swing.JDialog {

	private String[] productCodes;
	private ProductDao productDao = new ProductDao();
	private List<String> productCodeList = new ArrayList<>();
	private String primaryName = "";
	private int programId = 3;
	private TableColumnAligner colAligner = new TableColumnAligner();
	private Map<String, String> productMap = new HashMap<>();
	private String[][] data = null;
	private String[] columnNames = { "<HTML><b>PRODUCT <br>CODE</b></html>",	"<html><b>PRIMARY NAME</b></html>", "" };
	private DefaultTableModel tableModel = new DefaultTableModel(data, columnNames) {
		@Override
		public boolean isCellEditable(int row, int column) {
			
			return column > 1;
		}
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 3)
			return Integer.class;
			else
				return String.class;
		}
	};
	class ButtonEditor extends DefaultCellEditor {
		protected JButton button;
		private String label;
		private boolean isPushed;

		public ButtonEditor(JCheckBox checkBox) {
			super(checkBox);
			button = new JButton();
			button.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					fireEditingStopped();
					
						String code = (String) indicatorProductTable.getValueAt(indicatorProductTable.getSelectedRow(), 0);
						int ack = JOptionPane.showConfirmDialog(IndicatorProductJd.this.indicatorProductTable, "<html>Deleting will remove <font color=red>"+code+"</font> from list of monitored products. <br>Do you want to delete?</html>", "Confirm delete", JOptionPane.YES_NO_OPTION);
						if (ack == JOptionPane.YES_OPTION) {
							System.out.println(indicatorProductTable.getSelectedRow()+" Deleting row ");
							if (new IndicatorProductDao().deleteIndicatorProduct(code) > 0)
								((DefaultTableModel)indicatorProductTable.getModel()).removeRow(indicatorProductTable.getSelectedRow());
							else
								JOptionPane.showMessageDialog(IndicatorProductJd.this.indicatorProductTable, "<html>Error deleting <font color=red>"+code+"</font> from database.<br>" +
										"Please contact your Systems Administrator.</html>", "Delete Error", JOptionPane.WARNING_MESSAGE);
					}
					
				}
			});
		}

		public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
			if (isSelected) {
				button.setForeground(table.getSelectionForeground());
				button.setBackground(table.getSelectionBackground());
			} else {
				button.setForeground(table.getForeground());
				button.setBackground(table.getBackground());
			}
			isPushed = true;
			return button;
		}

		public Object getCellEditorValue() {
			if (isPushed) { 
				label = "";
			}
			isPushed = false;
			return new String(label);
		}

		public boolean stopCellEditing() {
			isPushed = false;
			return super.stopCellEditing();
		}

		protected void fireEditingStopped() {
			super.fireEditingStopped();
		}
	}

	class ButtonRenderer  extends JButton implements TableCellRenderer
	{
		public ButtonRenderer(String btnTitle)
		{
			super(btnTitle);
			//setOpaque(true);
		}
		public Component getTableCellRendererComponent(
				final JTable table, Object value,
				boolean isSelected, boolean hasFocus,
				int row, int column) {
			return this;
		}
	}
	
    /**
     * Creates new form IndicatorProductJd
     */
    public IndicatorProductJd(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        List<Product> products = productDao.getHsqlProducts(3);
        for (Product p : products)
        {
        	String code = p.getProductCode().trim();
        	String name = p.getPrimaryName().trim();
        	productMap.put(code, name);
        	productCodeList.add(code);
        }
        productCodes = productCodeList.toArray(new String[productCodeList.size()]);
        initComponents();
        //Adding monitored indicator products to table for display
        List<Product> monitoredProductList = productDao.getIndicatorProducts();  

        for (int i = 0; i < monitoredProductList.size(); i++)
        {
        	tableModel.addRow(new String[monitoredProductList.size()]); // adds an empty row to the table
        	indicatorProductTable.getModel().setValueAt(monitoredProductList.get(i).getProductCode().trim(), i, 0);
        	indicatorProductTable.getModel().setValueAt(monitoredProductList.get(i).getPrimaryName().trim(), i, 1);
        }
        indicatorProductTable.getColumn("").setCellRenderer(new ButtonRenderer("Delete"));
        indicatorProductTable.getColumn("").setCellEditor(new ButtonEditor(new JCheckBox()));
        indicatorProductTable.setFillsViewportHeight(true);
        indicatorProductTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF); 
        indicatorProductTable.getColumnModel().getColumn(1).setPreferredWidth(305);
        indicatorProductTable.setRowHeight(30);
        colAligner.centreAlignColumn(indicatorProductTable, 0);
        getContentPane().setBackground(new java.awt.Color(102, 102, 102));        
        AutoCompleteDecorator.decorate(productCodeComboBox);
        setLocationRelativeTo(parent);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        programBtnGrp = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        indicatorProductTable = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        arvRadioButton = new javax.swing.JRadioButton();
        hivRadioButton = new javax.swing.JRadioButton();
        labRadioButton = new javax.swing.JRadioButton();
        emlipRadioButton = new javax.swing.JRadioButton();
        productCodeComboBox = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        cancelButton = new javax.swing.JButton();
        addButton = new javax.swing.JButton();
        primaryNameLbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Indicator Products");

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Indicator Products", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ebrima", 1, 13))); // NOI18N

        indicatorProductTable.setModel(tableModel);
        jScrollPane1.setViewportView(indicatorProductTable);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 141, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Select Program", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ebrima", 1, 13))); // NOI18N

        programBtnGrp.add(arvRadioButton);
        arvRadioButton.setSelected(true);
        arvRadioButton.setText("ARV");
        arvRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                arvRadioBtnHandler(evt);
            }
        });

        programBtnGrp.add(hivRadioButton);
        hivRadioButton.setText("HIV");
        hivRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hivRadioButtonHandler(evt);
            }
        });

        programBtnGrp.add(labRadioButton);
        labRadioButton.setText("LAB");
        labRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labRadioButtonHandler(evt);
            }
        });

        programBtnGrp.add(emlipRadioButton);
        emlipRadioButton.setText("EM");
        emlipRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emRadioButtonHandler(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(arvRadioButton)
                .addGap(38, 38, 38)
                .addComponent(hivRadioButton)
                .addGap(30, 30, 30)
                .addComponent(labRadioButton)
                .addGap(35, 35, 35)
                .addComponent(emlipRadioButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(arvRadioButton)
                    .addComponent(hivRadioButton)
                    .addComponent(labRadioButton)
                    .addComponent(emlipRadioButton))
                .addContainerGap(8, Short.MAX_VALUE))
        );

        productCodeComboBox.setModel(new javax.swing.DefaultComboBoxModel(productCodes));
        productCodeComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                productComboHandler(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Ebrima", 1, 13)); // NOI18N
        jLabel1.setText("Select product");

        cancelButton.setText("Cancel");
        cancelButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelButtonActionPerformed(evt);
            }
        });

        addButton.setText("Add Product");
        addButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addButtonActionPerformed(evt);
            }
        });

        //primaryNameLbl.setFont(new java.awt.Font("Ebrima", 1, 11)); // NOI18N
        primaryNameLbl.setText("<html>Primary Name:<font color=red>"+primaryName+"</font></html>");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 106, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addContainerGap())
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(productCodeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(cancelButton)
                                .addGap(18, 18, 18)
                                .addComponent(addButton)
                                .addGap(98, 98, 98))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(primaryNameLbl)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(productCodeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(addButton, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(primaryNameLbl)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void addButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addButtonActionPerformed
    	boolean isConfigured = true;
    	String code = productCodeComboBox.getSelectedItem().toString().trim();
    	String name = productMap.get(code);
    	int rowCount = indicatorProductTable.getRowCount();

    	for (int i = 0; i < rowCount; i++)
    	{
    		String codeIndicator = (String)indicatorProductTable.getModel().getValueAt(i, 0);
    		if (codeIndicator.equalsIgnoreCase(code))
    		{
    			isConfigured = false;
    			break;
    		}
    	}
    	if (isConfigured)
    	{
    		tableModel.addRow(new String[rowCount]);
    		indicatorProductTable.getModel().setValueAt(code, rowCount, 0);
    		indicatorProductTable.getModel().setValueAt(name, rowCount, 1);

    		Product p = new Product();
    		p.setProductCode(code);
    		p.setProgramId(programId);
    		new IndicatorProductDao().addIndicatorProduct(p);
    	}
    	else
    		JOptionPane.showMessageDialog(this, "<html>The product <font color=red>"+code+"</font> is already being monitored</html>", "Information", JOptionPane.WARNING_MESSAGE);

    }//GEN-LAST:event_addButtonActionPerformed
    private void arvRadioBtnHandler(java.awt.event.ActionEvent evt) {        
    	programId = 3;
    	DefaultComboBoxModel<String> model = (DefaultComboBoxModel)productCodeComboBox.getModel();
    	model.removeAllElements();
        List<Product> products = productDao.getHsqlProducts(3);
        for (Product p : products)
        	model.addElement(p.getProductCode());
    }
    private void hivRadioButtonHandler(java.awt.event.ActionEvent evt) { 
    	programId = 2;
    	DefaultComboBoxModel<String> model = (DefaultComboBoxModel)productCodeComboBox.getModel();
    	model.removeAllElements();
        List<Product> products = productDao.getHsqlProducts(2);
        for (Product p : products)
        	model.addElement(p.getProductCode());
    }
    private void labRadioButtonHandler(java.awt.event.ActionEvent evt) {  
    	programId = 4;
    	DefaultComboBoxModel<String> model = (DefaultComboBoxModel)productCodeComboBox.getModel();
    	model.removeAllElements();
        List<Product> products = productDao.getHsqlProducts(4);
        for (Product p : products)
        	model.addElement(p.getProductCode());
    } 
    private void emRadioButtonHandler(java.awt.event.ActionEvent evt) { 
    	programId = 1;
    	DefaultComboBoxModel<String> model = (DefaultComboBoxModel)productCodeComboBox.getModel();
    	model.removeAllElements();
        List<Product> products = productDao.getHsqlProducts(1);
        for (Product p : products)
        	model.addElement(p.getProductCode());
    } 
    private void cancelButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelButtonActionPerformed
        dispose();
    }//GEN-LAST:event_cancelButtonActionPerformed
    private String getSelectedButtonText(ButtonGroup buttonGroup) {
		for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText();
			}
		}

		return null;
	}
    private void productComboHandler(java.awt.event.ActionEvent evt) {                                     
         
    	//System.out.println(productCodeComboBox.getSelectedItem().toString().trim());
    	primaryNameLbl.setText("Michael");
    } 

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(IndicatorProductJd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(IndicatorProductJd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(IndicatorProductJd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(IndicatorProductJd.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                IndicatorProductJd dialog = new IndicatorProductJd(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addButton;
    private javax.swing.JRadioButton arvRadioButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JRadioButton emlipRadioButton;
    private javax.swing.JRadioButton hivRadioButton;
    private javax.swing.JTable indicatorProductTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton labRadioButton;
    private javax.swing.JLabel primaryNameLbl;
    private javax.swing.JComboBox productCodeComboBox;
    private javax.swing.ButtonGroup programBtnGrp;
    // End of variables declaration//GEN-END:variables
}
