package org.elmis.facility.domain.dao;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.connections.MyBatisConnectionFactory;
import org.elmis.facility.reporting.model.ReportRequisition;
import org.elmis.facility.reports.utils.AmcCalculator;
import org.elmis.facility.reports.utils.CalendarUtil;

public class EmlipRequisitionDao {
	private SqlSessionFactory sqlSessionFactory;
	
	public EmlipRequisitionDao()
	{
		sqlSessionFactory = MyBatisConnectionFactory.getSqlSessionFactory();
	}
	public List<ReportRequisition> getEmlipIssues(@Param("fromDate") Date fromDate, @Param("toDate") Date toDate, String normalOrder, String remarks)
	{
		SqlSession session = sqlSessionFactory.openSession(false);
		Map<String, Object> map = new HashMap <String, Object>();
		map.put("fromDate", fromDate);
		map.put("toDate", toDate);
		if (remarks.trim().length() != 0)
			map.put("remark", remarks);
		if (normalOrder.equalsIgnoreCase("YES"))
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonthEmergency());
		else
			map.put("prevToDate", new CalendarUtil().getLastDayOfPreviousMonth());
		//List<ReportRequisition> emlipIssuesList = new ArrayList<>();
		try
		{
			deleteEmlipRandRData(toDate, session);

			List<ReportRequisition> issuedList = session.selectList("EmlipRequisition.emlipIssues", map);
			
			for (ReportRequisition r : issuedList)
			{
				r.setEmergencyOrder(normalOrder);
				map.put("productCode", r.getProductCode());
				Integer qtyReceived = 0;
				qtyReceived=session.selectOne("EmlipRequisition.em_received", map);
				if (qtyReceived==null)
				{
					qtyReceived=0;
				}else
				{
					Date dateStockedOut = session.selectOne("EmlipRequisition.date_stocked_out", map);
					if (dateStockedOut == null)
						r.setDaysStockedOut(0);
					else
						r.setDaysStockedOut(new CalendarUtil().numberOfDaysBetweenDates(toDate, dateStockedOut));
				}	
					int amc = new AmcCalculator().amcCalculator(r.getProductCode(), r.getQtyDispensed());
				
				/*if (amc == 0)
				{
					amc = r.getQtyDispensed();
					r.setAmc(amc);
					int maxQty = amc * 3;
					r.setMaxQty(maxQty);
				}
				else
				{*/
					r.setAmc(amc);
					int maxQty = amc * 3;
					r.setMaxQty( maxQty);
				//}
				Integer phyCount = session.selectOne("EmlipRequisition.phyCount", map);
				Integer beginBal = session.selectOne("EmlipRequisition.beginBalanceFriday", map);
				Integer adjustments = session.selectOne("EmlipRequisition.adjustments", map);
				if (r.getReportPeriodFrm() == null)
					r.setReportPeriodFrm(fromDate);
				if (r.getReportPeriodTo() == null)
					r.setReportPeriodTo(toDate);
				if (qtyReceived == null)
				{
					
					r.setTotalReceived(0);
				}
				else
					r.setTotalReceived(qtyReceived);
				if (phyCount == null){
					phyCount = 0;
					r.setPhyCount(phyCount);
					r.setOrderQty(r.getMaxQty() - phyCount);
				}
				else{
					r.setPhyCount(phyCount);
					r.setOrderQty(r.getMaxQty() - phyCount);
				}
				if (beginBal == null){
					r.setBeginningBal(0);	
				}
				else{
					r.setBeginningBal(beginBal);
				}
				if (adjustments == null){
					r.setLossAdjustment(0);	
				}
				else{
					r.setLossAdjustment(adjustments);
					if (r.getOrderQty() < 0)
						r.setOrderQty(0);
					r.setCurrentPrice(Math.round(r.getCurrentPrice() * r.getOrderQty()*100.0)/100.0);
				}
				if (remarks.trim().length() != 0)
					r.setRemarks(remarks);
				/*session.commit();*/
				session.insert("EmlipRequisition.insertEmlip",  r);
			}
			session.commit();
			/*if (remarks.trim().length() != 0)
				session.insert("EmlipRequisition.insertRemark", map);
			session.commit();*/
			return issuedList;
		}
		finally
		{
		  session.close();
		}
	}
	private void deleteEmlipRandRData( Date toDate, SqlSession session)
	{
		session.delete("EmlipRequisition.del_emlip_data", toDate);
		session.delete("EmlipRequisition.deleteRemark", toDate);
		session.commit();
	}
}
