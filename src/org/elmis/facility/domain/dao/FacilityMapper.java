package org.elmis.facility.domain.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.elmis.facility.domain.model.Facility;
import org.elmis.facility.domain.model.FacilityExample;

public interface FacilityMapper {
	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int countByExample(FacilityExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int deleteByExample(FacilityExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int deleteByPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int insert(Facility record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int insertSelectiveFacility(Facility record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	List<Facility> selectByFacility(FacilityExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	Facility selectByFacilityPrimaryKey(Integer id);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int updateByExample(@Param("record") Facility record,
			@Param("example") FacilityExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int updateByExampleSelective(@Param("record") Facility record,
			@Param("example") FacilityExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int updateByPrimaryKey(Facility record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds
	 * to the database table facility
	 * 
	 * @mbggenerated Fri Jun 14 15:45:19 CAT 2013
	 */
	int updateByPrimaryKeySelective(Facility record);
	
}