package org.elmis.facility.generic;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.elmis.facility.domain.dao.DispensaryDao;
import org.elmis.facility.domain.dao.RegimenMapper;
import org.elmis.facility.domain.dao.RegimenProductDosageMapper;
import org.elmis.facility.domain.dao.moses_FacilityMapper;
import org.elmis.facility.domain.model.Dispensary;
import org.elmis.facility.domain.model.ElmisRegimen;
import org.elmis.facility.domain.model.Facility_Types;
import org.elmis.facility.domain.model.RegimenProductDosage;
import org.elmis.facility.domain.model.facilities;
import org.elmis.facility.utils.ElmisAESencrpDecrp;

public class GenericDao {
	
	private SqlSessionFactory sqlMapper = null;
	
	private GenericDao(){
		setSystemProperties();
		setSqlMapper();
	}

	private static GenericDao genericDao = null;
	
	public static GenericDao getInstance(){
		if(genericDao == null){
			genericDao = new GenericDao();
		} 
		return genericDao;
	}
	
	private SqlSessionFactory getSqlMapper(){
		return this.sqlMapper;
	}
	private SqlSessionFactory setSqlMapper(){
		
		
		String resource = "SqlMapConfig.xml";
		Reader reader = null;

		try {
			reader = Resources.getResourceAsReader(resource);
			sqlMapper = new SqlSessionFactoryBuilder().build(reader,
					System.getProperties());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sqlMapper;
	}


	public  Facility_Types selectFacilityTypeByCode(String facilityCode) {
        SqlSession session = getSqlMapper().openSession();
        Facility_Types f = null;
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);

		try {

			f = mapper.selectFacilityTypeByFacilityCode(facilityCode);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.close();
		}
		return f;
	}
	
	public void insert(Dispensary disp){
		SqlSession session = getSqlMapper().openSession();
		
		try {
			DispensaryDao dispDao = new DispensaryDao();
			dispDao.addDispensingPoint(disp);
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	public void updateRegimen(ElmisRegimen elmisRegimen){
		SqlSession session = getSqlMapper().openSession();
		RegimenMapper mapper = session.getMapper(RegimenMapper.class);
		
		try {
			mapper.updateRegimen(elmisRegimen);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
	
	public void updateDosage(RegimenProductDosage dosage) {
		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		try {

			mapper.updateDosage(dosage);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
	}
	
	public void deleteDosage(RegimenProductDosage dosage) {
		SqlSession session = getSqlMapper().openSession();
		RegimenProductDosageMapper mapper = session.getMapper(RegimenProductDosageMapper.class);

		try {

			mapper.deleteDosage(dosage);

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
		
		
	}
	
	public static void setSystemProperties(){
		Properties prop = new Properties(System.getProperties());
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("Programmproperties.properties");
			prop.load(fis);

			System.setProperties(prop);

			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String dbpassword =  ElmisAESencrpDecrp.decrypt(prop.getProperty("dbpassword"));
		System.setProperty("dbpassword", dbpassword);
	}
	
}
