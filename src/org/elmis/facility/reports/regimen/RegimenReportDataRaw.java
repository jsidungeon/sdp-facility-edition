package org.elmis.facility.reports.regimen;

import java.util.ArrayList;

public class RegimenReportDataRaw {
	private Integer regimenid;
	private String regimenName;
	private Integer regimenLineId;
	private String regimenLineName;
	private Long patientsOnRegimen;
	private Double percentageOutOfLine;
	private Double percentageOutOfTotalPatients;

	private Long patientsOnRegimenLine;
	private Double percentageOfLineOutOfAllPatients;
	
	public Integer getRegimenid() {
		return regimenid;
	}
	public void setRegimenid(Integer regimenid) {
		this.regimenid = regimenid;
	}
	public String getRegimenName() {
		return regimenName;
	}
	public void setRegimenName(String regimenName) {
		this.regimenName = regimenName;
	}
	public Integer getRegimenLineId() {
		return regimenLineId;
	}
	public void setRegimenLineId(Integer regimenLineId) {
		this.regimenLineId = regimenLineId;
	}
	public String getRegimenLineName() {
		return regimenLineName;
	}
	public void setRegimenLineName(String regimenLineName) {
		this.regimenLineName = regimenLineName;
	}
	public Long getPatientsOnRegimen() {
		return patientsOnRegimen;
	}
	public void setPatientsOnRegimen(Long patientsOnRegimen) {
		this.patientsOnRegimen = patientsOnRegimen;
	}
	
	public Double getPercentageOutOfLine() {
		return percentageOutOfLine;
	}
	
	public void setPercentageOutOfLine(Double percentageOutOfLine) {
		this.percentageOutOfLine = percentageOutOfLine;
	}
	
	public Double getPercentageOutOfTotalPatients() {
		return percentageOutOfTotalPatients;
	}
	
	public void setPercentageOutOfTotalPatients(
			Double percentageOutOfTotalPatients) {
		this.percentageOutOfTotalPatients = percentageOutOfTotalPatients;
	}
	public Long getPatientsOnRegimenLine() {
		return patientsOnRegimenLine;
	}
	public void setPatientsOnRegimenLine(Long patientsOnRegimenLine) {
		this.patientsOnRegimenLine = patientsOnRegimenLine;
	}
	public Double getPercentageOfLineOutOfAllPatients() {
		return percentageOfLineOutOfAllPatients;
	}
	public void setPercentageOfLineOutOfAllPatients(
			Double percentageOfLineOutOfAllPatients) {
		this.percentageOfLineOutOfAllPatients = percentageOfLineOutOfAllPatients;
	}
	
	public static ArrayList<RegimenReportDataRaw> createBeanCollection(){
		return new ArrayList<>();
	}
	
}
