package org.elmis.facility.reporting;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;

import org.elmis.facility.utils.SoftKeyboard;
/**
 * ReagentOrderJd.java
 * @Purpose: Used to confirm if reagent(s) of a faulty piece of Laboratory equipment should be ordered.
 * @author Michael Mwebaze
 * @version 1.0
 */
public class ReagentOrderJd extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private final ButtonGroup btnGroup = new ButtonGroup();
	private final JTextArea remarksTextArea = new JTextArea();
	private String productCode;
	private boolean isOderable;
	private boolean isCancelled = true;

	/**
	 * 
	 * @param parent
	 * @param modal
	 * @param productCode
	 */
	public ReagentOrderJd(JDialog parent, boolean modal, String productCode) {
		super(parent, modal);
		setIconImage(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/appicon.png")).getImage());
		this.productCode = productCode;
		setTitle("Confirm Reagent Order");
		setBounds(100, 100, 748, 432);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		{
			JLabel lblNewLabel = new JLabel("<html>Do you still want to order reagents for <font color=red>"+productCode+"</font> item?</html>");
			//lblNewLabel.setFont(new Font("Ebrima", Font.BOLD, 12));
			lblNewLabel.setBounds(10, 24, 299, 14);
			contentPanel.add(lblNewLabel);
		}

		JRadioButton rdbtnNo = new JRadioButton("NO");
		rdbtnNo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if ((e.getStateChange() == ItemEvent.SELECTED) )
				{
					System.out.println("NO");
					remarksTextArea.setText("");
					ReagentOrderJd.this.isOderable = false;
					remarksTextArea.setEnabled(false);
				}
			}
		});
		rdbtnNo.setSelected(true);
		rdbtnNo.setBounds(10, 75, 48, 23);
		btnGroup.add(rdbtnNo);
		contentPanel.add(rdbtnNo);

		JRadioButton rdbtnYes = new JRadioButton("YES");
		rdbtnYes.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if ((e.getStateChange() == ItemEvent.SELECTED) )
				{
					System.out.println("YES");
					ReagentOrderJd.this.isOderable = true;
					remarksTextArea.setEnabled(true);
					remarksTextArea.requestFocusInWindow();
					SoftKeyboard.startSoftKeyboard();
				}
			}
		});
		btnGroup.add(rdbtnYes);
		rdbtnYes.setBounds(71, 75, 48, 23);
		contentPanel.add(rdbtnYes);

		JLabel lblNewLabel_1 = new JLabel("Remarks");
		lblNewLabel_1.setFont(new Font("Ebrima", Font.BOLD, 12));
		lblNewLabel_1.setBounds(10, 116, 61, 14);
		contentPanel.add(lblNewLabel_1);
		{
			remarksTextArea.setRows(5);

			remarksTextArea.setEnabled(false);
			remarksTextArea.setLineWrap(true);
			remarksTextArea.setBounds(10, 140, 476, 173);
			remarksTextArea.setFont(new Font("Arial Black", Font.PLAIN, 12));
			remarksTextArea.addKeyListener(new KeyAdapter() {
				@Override
				  public void keyPressed(KeyEvent evt) {
					if(evt.getKeyCode() == KeyEvent.VK_ENTER)
					{
						if (remarksTextArea.getText().trim().length() > 0)
						{
							int ack = JOptionPane.showConfirmDialog(ReagentOrderJd.this, "<html>Are you sure you want to continue with the order for <font color=red"+ReagentOrderJd.this.productCode+"</font> ?</html>", "Confirm Order", JOptionPane.YES_NO_OPTION);

							if (ack == JOptionPane.YES_OPTION) {
								ReagentOrderJd.this.isCancelled = false;
								dispose();
							}
						}
						else
						{
							remarksTextArea.setBackground(Color.RED);
							JOptionPane.showMessageDialog(ReagentOrderJd.this, "Please enter a remark", "Provide Remark",JOptionPane.INFORMATION_MESSAGE);
							remarksTextArea.requestFocusInWindow();
							remarksTextArea.setBackground(Color.WHITE);
							remarksTextArea.setCaretPosition(0);
						}
					}
				  }
			});
			contentPanel.add(remarksTextArea);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBackground(new java.awt.Color(102, 102, 102));
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ReagentOrderJd.this.isCancelled = true;
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {	
						if (remarksTextArea.getText().trim().length() > 0)
						{
							int ack = JOptionPane.showConfirmDialog(ReagentOrderJd.this, "<html>Are you sure you want to continue with the order for <font color=red"+ReagentOrderJd.this.productCode+"</font> ?</html>", "Confirm Order", JOptionPane.YES_NO_OPTION);

							if (ack == JOptionPane.YES_OPTION) {
								ReagentOrderJd.this.isCancelled = false;
								dispose();
							}
						}
						else
						{
							remarksTextArea.setBackground(Color.RED);
							JOptionPane.showMessageDialog(ReagentOrderJd.this, "Please enter a remark", "Provide Remark",JOptionPane.INFORMATION_MESSAGE);
							remarksTextArea.requestFocusInWindow();
							remarksTextArea.setBackground(Color.WHITE);
							remarksTextArea.setCaretPosition(0);
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		contentPanel.setBackground(new java.awt.Color(102, 102, 102));
		
		setLocationRelativeTo(parent);
	}
	/**
	 * 
	 * @return
	 */
	public boolean getConfirmedStatus()
	{
		return isOderable;
	}
	/**
	 * 
	 * @return
	 */
	public boolean getConfirmation()
	{
		return isCancelled;
	}
	/**
	 * 
	 * @return Remarks 
	 */
	public String getRemarks()
	{
		return remarksTextArea.getText();
	}
}
