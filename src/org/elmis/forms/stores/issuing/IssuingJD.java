/*
 * IssuingJD.java
 *
 * Created on __DATE__, __TIME__
 */

package org.elmis.forms.stores.issuing;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.security.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.swing.JRViewer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.elmis.facility.dao.ARVDispensingDAO;
import org.elmis.facility.dao.FacilityDispensingPointDAO;
import org.elmis.facility.dao.FacilityProductsDAO;
import org.elmis.facility.dao.FacilitySetUpDAO;
import org.elmis.facility.dao.FacilityStoreProductBalanceDAO;
import org.elmis.facility.domain.dao.HivTestResultDao;
import org.elmis.facility.domain.dao.UsersDAO;
import org.elmis.facility.domain.model.Dispensing_point;
import org.elmis.facility.domain.model.Elmis_Stock_Control_Card;
import org.elmis.facility.domain.model.Items_issue;
import org.elmis.facility.domain.model.ProductQty;
import org.elmis.facility.domain.model.Products;
import org.elmis.facility.domain.model.Programs;
import org.elmis.facility.domain.model.Site_Dispensing_point;
import org.elmis.facility.domain.model.StockControlCard;
import org.elmis.facility.domain.model.User;
import org.elmis.facility.domain.model.VW_Systemcalculatedproductsbalance;
import org.elmis.facility.domain.model.users;
import org.elmis.facility.main.gui.AppJFrame;
//import org.elmis.facility.main.gui.SCCReportDialogue;
import org.elmis.facility.network.MyBatisConnectionFactory;
import org.elmis.forms.admin.dispensingpoints.NumberPadJD;
import org.elmis.forms.admin.dispensingpoints.SelectDispensingPointJD;
import org.jdesktop.swingx.table.DatePickerCellEditor;

import com.oribicom.tools.JasperViewer;
import com.oribicom.tools.TableModel;

import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.MutableComboBoxModel;

import java.awt.Font;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JComboBox;

import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

import com.toedter.calendar.JDateChooser;

/**
 * 
 * @author __USER__
 */
@SuppressWarnings({ "serial", "unused" })
public class IssuingJD extends javax.swing.JDialog {

	// private static final String[] columns_issueJTable = { "Name" };
	// private static final Object[] defaultv_issueJTable = { "" };
	// private static final int rows_issueJTable = 0;
	// public static TableModel tableModel_dispensingPoint = new TableModel(
	// columns_dispensingPoint, defaultv_dispensingPoint,
	// rows_dispensingPoint);
	
	
	private static final String[] columns_barcodefacilityApprovedproducts = {
		"Product", "Qty","id", "Code" ,
		};
   private static final Object[] defaultv_barcodefacilityapprovedproducts = { "", "",
		"", ""};
	
	private static final int rows_fproducts = 0;
	
	public static TableModel tableModel_issueJTable = new TableModel(
			columns_barcodefacilityApprovedproducts,
			defaultv_barcodefacilityapprovedproducts, rows_fproducts) {

		boolean[] canEdit = new boolean[] {false, true,false, false};

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return canEdit[columnIndex];
		}

		//create a check box value in table 
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			if (columnIndex == 4) {

				return getValueAt(0,4).getClass();
			}
			
			
			return super.getColumnClass(columnIndex);
		}

		/* @Override
		 public boolean isCellEditable(int row, int column) {
		     return column == CHECK_COL;
		 }*/
		
		
		

	};
	

/*public static TableModel tableModel_issueJTable = new TableModel(
			new String[] { "Product", "Qty","Issue Date", "id", "Code" }, new Object[] { "",
					"", "", "","" }, 0){
					
					boolean[] canEdit = new boolean[] { false, true,true, false, false};

					public boolean isCellEditable(int rowIndex, int columnIndex) {
						return canEdit[columnIndex];
					}
			
	};*/



	//private int selectedProgramID;
	private String X = "";
	private Integer selecteddpId = null;
	public static String selecteddispensaryname;
	public static String Receivedbyuser; 
	public static String selectedDispensingPointName;
	public static int selectedProductID;
	public static int selectedprogramID  = 0;
	public static Products selectedProduct;
	public static String barcodeScanned;
    public static String issuetoprogram = "";
    public static String SelectedProgramArea = "";
	private String searchText = "";
	private char letter;
	public String newDateString;
	private List<Character> charList = new LinkedList();
    @SuppressWarnings("unchecked")
	private Map<Integer , Integer> map = new HashMap();
	private List<Products>initialsearchproductsList = new LinkedList();
	private List<Products> productsList = new LinkedList();
    private List<Site_Dispensing_point>dispenasryList = new LinkedList();
    @SuppressWarnings({ "unused", "unchecked", "rawtypes" })
	private List<users> UsersList = new LinkedList();
	private static Map parameterMap = new HashMap();
	private JasperPrint print;
	public java.sql.Timestamp  productdeliverdate;
	private Dispensing_point dp;
	public java.sql.Timestamp selecteddate;
	private Timestamp shipmentdate;
	public String oldDateString;
	private ProductQty qty;
	private Date registerdate;
	FacilitySetUpDAO  callusersmapper = null;
	FacilityStoreProductBalanceDAO callhivtestbalances = null;
	FacilityDispensingPointDAO calldispenasrymapper;
	ARVDispensingDAO aFacilityproductsmapper = null;
	FacilityProductsDAO Facilityproductsmapper = null;
	private ProductQty dispensarypqty;
	private List<VW_Systemcalculatedproductsbalance> electronicsccbal = new LinkedList();
	List<ProductQty> productcodesstockQtyList = new LinkedList();
	public List<Programs> programsbalList = new LinkedList();
	private JComboBox dispensarynamecomboList = new JComboBox();
	MutableComboBoxModel modeldispensarylist = (MutableComboBoxModel) dispensarynamecomboList
			.getModel();
	
	private JComboBox ReceivedbyusercomboList = new JComboBox();
	MutableComboBoxModel modelReceivedbyuserlist = (MutableComboBoxModel) ReceivedbyusercomboList
			.getModel();
	/** Creates new form IssuingJD */
	public IssuingJD(java.awt.Frame parent, boolean modal, Dispensing_point dp) {
		super(parent, modal);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent arg0) {
				LoadAllSitedispensingpoints();
			}
		});
		initComponents();

		this.dp = dp;
		searchJL.setVisible(false);

		this.jTabbedPane1.setFocusable(false);
		this.finishIssuingJBtn.setFocusable(false);
		this.cancelJBtn.setFocusable(false);
		this.deleteItemLineJBtn.setFocusable(false);
		this.previewJBtn.setFocusable(false);
		this.issueJTable.setFocusable(false);

		this.setTitle("Issuing to : " + dp.getName().toUpperCase());

		class RightTableCellRenderer extends DefaultTableCellRenderer {
			protected RightTableCellRenderer() {
				setHorizontalAlignment(JLabel.RIGHT);
			}

		}

		RightTableCellRenderer rightAlign = new RightTableCellRenderer();

		this.issueJTable.getColumnModel().getColumn(1).setCellRenderer(
				rightAlign);

		// set column width size
		this.issueJTable.getColumnModel().getColumn(1).setMaxWidth(100);
		this.issueJTable.getColumnModel().getColumn(1).setMaxWidth(100);

		this.issueJTable.getColumnModel().getColumn(2).setMinWidth(0);
		this.issueJTable.getColumnModel().getColumn(2).setMaxWidth(0);

		this.issueJTable.getColumnModel().getColumn(3).setMinWidth(0);
		this.issueJTable.getColumnModel().getColumn(3).setMaxWidth(0);

		// jTabbedPane1.addTab("Dispensing Point", new DispensingPointJP());

		jTabbedPane1.addTab("Program", new ProgramsJP());

		// jTabbedPane1.addTab("Products", new ProductsJP());

		// jTabbedPane1.addTab("Quantity", new IssueQtyJP());

		/*this.setSize(
				(AppJFrame.desktop.getSize().width / 2 + (AppJFrame.desktop
						.getSize().width / 3)),
				(AppJFrame.desktop.getSize().height / 2)
						+ (AppJFrame.desktop.getSize().height / 3));*/
		this.setSize(1090, 500);
		this.setLocationRelativeTo(null);

		// set the focus to barcodeJTF
		this.jPanel1.setOpaque(true);
		Vector<Component> order = new Vector<Component>(7);
		order.add(barcodeJTF);
		order.add(deleteItemLineJBtn);
		order.add(previewJBtn);
		order.add(jTabbedPane1);
		// order.add(tf5);
		// order.add(tf6);
		this.issueJTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		issueJTable.setColumnSelectionAllowed(false);
		issueJTable.setRowSelectionAllowed(true);
		tableModel_issueJTable.clearTable();

		this.barcodeJTF.requestFocusInWindow();
		this.setVisible(true);

	}

	@SuppressWarnings("unchecked")
	protected void LoadAllSitedispensingpoints() {
		// TODO Auto-generated method stub
		calldispenasrymapper = new FacilityDispensingPointDAO();
		callusersmapper  = new FacilitySetUpDAO();
		//dispenasryList = calldispenasrymapper.doselectAllDispensingPoints(dp.getSiteid());
		dispenasryList = calldispenasrymapper.doselectAllDispensingPoints1(dp.getSiteid());
		UsersList = callusersmapper.selectAll();
		int i = 0;
		modeldispensarylist.addElement("Select Dispensing Point");
		for (Site_Dispensing_point d : dispenasryList) {
			modeldispensarylist.addElement(d.getDispensingpointname() + "     " + d.getId());
			map.put(i,d.getId());
			i++;
		}
		
		this.modelReceivedbyuserlist.addElement("Select person receiving");
		for (users r : UsersList) {
			modelReceivedbyuserlist.addElement(r.getFirstname() + "     " + r.getLastname());
			//map.put(i,d.getId());
			//i++;
		}
        
	}

	//GEN-BEGIN:initComponents
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	@SuppressWarnings("unchecked")
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		finishIssuingJBtn = new javax.swing.JButton();
		finishIssuingJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		finishIssuingJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/Save icon.png")));
		finishIssuingJBtn.setText("Finish Issuing");
		cancelJBtn = new javax.swing.JButton();
		cancelJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		cancelJBtn.setText("Cancel");
		previewJBtn = new javax.swing.JButton();
		previewJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		previewJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/eLMIS View details.png")));
		previewJBtn.setText("Preview");
		searchJL = new javax.swing.JLabel();
		barcodeJTF = new javax.swing.JTextField();
		jTabbedPane1 = new javax.swing.JTabbedPane();
		jScrollPane1 = new javax.swing.JScrollPane();
		issueJTable = new javax.swing.JTable();
		jPanel2 = new javax.swing.JPanel();
		deleteItemLineJBtn = new javax.swing.JButton();
		deleteItemLineJBtn.setFont(new Font("Ebrima", Font.BOLD, 12));
		deleteItemLineJBtn.setText("Delete Selected Item");

		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		jPanel1.setBackground(new java.awt.Color(102, 102, 102));
		jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		//finishIssuingJBtn.setIcon(new javax.swing.ImageIcon(getClass()
			//	.getResource("/elmis_images/Finished.png"))); // NOI18N
		finishIssuingJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						finishIssuingJBtnActionPerformed(evt);
					}
				});

		cancelJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/Cancel.png"))); // NOI18N
		cancelJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				cancelJBtnActionPerformed(evt);
			}
		});
		previewJBtn.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				previewJBtnActionPerformed(evt);
			}
		});

		searchJL.setFont(new Font("Ebrima", Font.BOLD, 12));
		searchJL.setForeground(new java.awt.Color(255, 255, 255));
		searchJL.setIcon(new javax.swing.ImageIcon(getClass().getResource(
				"/elmis_images/eLMIS view user small.png"))); // NOI18N
		searchJL.setText("Search");

		barcodeJTF.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				barcodeJTFKeyReleased(evt);
			}

			public void keyTyped(java.awt.event.KeyEvent evt) {
				barcodeJTFKeyTyped(evt);
			}
		});

		jTabbedPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
		jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				jTabbedPane1MouseClicked(evt);
			}
		});
		jTabbedPane1.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				jTabbedPane1KeyReleased(evt);
			}
		});

		issueJTable.setModel(tableModel_issueJTable);
		issueJTable.setCellSelectionEnabled(true);
		issueJTable.setRowHeight(30);
		jScrollPane1.setViewportView(issueJTable);

		jPanel2.setBackground(new java.awt.Color(102, 102, 102));
		jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

		deleteItemLineJBtn.setIcon(new ImageIcon(IssuingJD.class.getResource("/elmis_images/eLMIS delete role small.png"))); // NOI18N
		deleteItemLineJBtn
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						deleteItemLineJBtnActionPerformed(evt);
					}
				});

		javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(
				jPanel2);
		jPanel2Layout.setHorizontalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(deleteItemLineJBtn, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(229, Short.MAX_VALUE))
		);
		jPanel2Layout.setVerticalGroup(
			jPanel2Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel2Layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(deleteItemLineJBtn, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		jPanel2.setLayout(jPanel2Layout);
		
		
		SitedispensingpointcomboBox = new JComboBox();
		SitedispensingpointcomboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent evt) {
				checkSelectedSiteName(evt);
			}
		});
		SitedispensingpointcomboBox.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				
			}
		});
		SitedispensingpointcomboBox.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				
				SitedispensingpointcomboBoxActionPerformed(evt);
			}
		});
		SitedispensingpointcomboBox.setFont(new Font("Ebrima", Font.PLAIN, 13));
		SitedispensingpointcomboBox.setModel(modeldispensarylist);
		JLabel lblSelectDispensingPoint = new JLabel("Select Dispensing Point");
		lblSelectDispensingPoint.setForeground(Color.WHITE);
		lblSelectDispensingPoint.setFont(new Font("Ebrima", Font.BOLD, 15));
		
		DateofissuedateChooser = new JDateChooser();
		DateofissuedateChooser.addPropertyChangeListener(new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent evt) {
				DateofissuedateChooserpropertyChange(evt);
			}
		});
		DateofissuedateChooser.getCalendarButton().setFont(new Font("Ebrima", Font.PLAIN, 13));
		DateofissuedateChooser.setPreferredSize(new Dimension(150, 20));
		DateofissuedateChooser.setFont(new Font("Ebrima", Font.PLAIN, 20));
		DateofissuedateChooser.setDateFormatString("yyyy-MM-dd hh:mm:ss");
		
		JLabel lblSelectDispensingDate = new JLabel("Select Issue Date");
		lblSelectDispensingDate.setForeground(Color.WHITE);
		lblSelectDispensingDate.setFont(new Font("Ebrima", Font.BOLD, 15));
		
		ReceivedbycomboBox = new JComboBox();
		ReceivedbycomboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				ReceivedbycomboBoxitemStateChanged(arg0);
			}
		});
		ReceivedbycomboBox.setFont(new Font("Ebrima", Font.PLAIN, 13));
        ReceivedbycomboBox.setModel(modelReceivedbyuserlist);  
		javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(
				jPanel1);
		jPanel1Layout.setHorizontalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addGap(12)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addComponent(cancelJBtn, GroupLayout.PREFERRED_SIZE, 99, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(previewJBtn, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(finishIssuingJBtn, GroupLayout.PREFERRED_SIZE, 147, GroupLayout.PREFERRED_SIZE))
								.addComponent(jScrollPane1, GroupLayout.PREFERRED_SIZE, 621, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addGroup(jPanel1Layout.createSequentialGroup()
									.addGap(22)
									.addComponent(searchJL)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(barcodeJTF, GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE))
								.addComponent(jTabbedPane1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
						.addGroup(jPanel1Layout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
								.addComponent(ReceivedbycomboBox, GroupLayout.PREFERRED_SIZE, 399, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblSelectDispensingPoint, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblSelectDispensingDate, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
								.addComponent(DateofissuedateChooser, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
								.addComponent(SitedispensingpointcomboBox, 0, 461, Short.MAX_VALUE))))
					.addContainerGap())
		);
		jPanel1Layout.setVerticalGroup(
			jPanel1Layout.createParallelGroup(Alignment.LEADING)
				.addGroup(jPanel1Layout.createSequentialGroup()
					.addContainerGap()
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
							.addComponent(SitedispensingpointcomboBox, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblSelectDispensingPoint))
						.addComponent(jPanel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.LEADING)
						.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING)
							.addComponent(lblSelectDispensingDate, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
							.addComponent(DateofissuedateChooser, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE))
						.addComponent(ReceivedbycomboBox, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED, 19, Short.MAX_VALUE)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(jScrollPane1, 0, 0, Short.MAX_VALUE)
						.addComponent(jTabbedPane1, GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(jPanel1Layout.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancelJBtn, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addComponent(previewJBtn, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
						.addComponent(finishIssuingJBtn, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
						.addComponent(barcodeJTF, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
						.addComponent(searchJL, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		jPanel1.setLayout(jPanel1Layout);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.LEADING)
				.addGroup(layout.createSequentialGroup()
					.addContainerGap()
					.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(35))
		);
		getContentPane().setLayout(layout);

		pack();
	}// </editor-fold>
	protected void ReceivedbycomboBoxitemStateChanged(ItemEvent evt) {
		// TODO Auto-generated method stub
		
		if (evt.getStateChange() == ItemEvent.SELECTED) {
	          Object item = evt.getItem();
	          // do something with object
	          Receivedbyuser = item.toString();
	       }
		
	}

	//GEN-END:initComponents

	
	
	private java.sql.Timestamp  Convertdatestr1(String mydate) {

		try {

			System.out.println(mydate + "Error source 2014");
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			// final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			oldDateString = mydate;

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
				Date d = sdf.parse(oldDateString);

				sdf.applyPattern(NEW_FORMAT);
				newDateString = sdf.format(d);
				selecteddate = java.sql.Timestamp.valueOf(newDateString);
				System.out.println(selecteddate + "for Reveive EXPIRY");

			} catch (ParseException e) {
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
		System.out.println(selecteddate + "same expiry date??");
		return selecteddate;

	}

	protected void DateofissuedateChooserpropertyChange(PropertyChangeEvent evt) {
		// TODO Auto-generated method stub
		
		try {

			Calendar currentDate = Calendar.getInstance(); // Get the current
															// date
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd"); // format
																				// it
																				// as
																				// per
																				// your
																				// requirement
			String dateNow = formatter.format(currentDate.getTime());
			System.out.println("Now the date is :=>  " + dateNow);

			Date mydate = this.DateofissuedateChooser.getDate();
			String sdate = formatter.format(mydate);
			System.out.println(sdate);
			this.registerdate = mydate;
			
			Date d = mydate;
			if (d.after(currentDate.getTime())) {
				System.out.println("Test date diff ");
				productdeliverdate = Convertdatestr1(currentDate.getTime()
						.toString());
				JOptionPane.showMessageDialog(this,
						"Registeration Date cannnot be in the future",
						"Wrong Date entry", JOptionPane.ERROR_MESSAGE);
				this.DateofissuedateChooser.setDate(new Date());
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}

		
	}

	protected void checkSelectedSiteName(ItemEvent evt) {
		// TODO Auto-generated method stub
		if (evt.getStateChange() == ItemEvent.SELECTED) {
	          Object item = evt.getItem();
	          // do something with object
	          selecteddispensaryname = item.toString();
	       }
		//selecteddispensaryname = (String) cb.getSelectedItem();
		
		
	}

	protected void SitedispensingpointcomboBoxActionPerformed(ActionEvent evt) {
		// TODO Auto-generated method stub
		JComboBox cb = (JComboBox) evt.getSource();
		int selectedindex = cb.getSelectedIndex();
		//selecteddispensaryname = (String) cb.getSelectedItem();
		System.out.println(selecteddispensaryname);
		System.out.println(map.get(selectedindex)+ "~~~~");
		selecteddpId = map.get(selectedindex -1);
		}
		
	
     private String ReturnLastchar(String s){
	 //String X = "";
	  for( int i = 0 ; i <= s.length()-1; i++){
	    if(i == s.length()-1)
	    X = s.substring(i);
	  }
	    return X;
	
  }
	

	private void previewJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		List list = new ArrayList();
		Items_issue items;
		for (int i = 0; i < this.tableModel_issueJTable.getRowCount(); i++) {

			items = new Items_issue();

			items.setProduct_code(this.tableModel_issueJTable.getValueAt(i, 3)
					.toString());
			items.setProduct_id(Integer.parseInt(this.tableModel_issueJTable
					.getValueAt(i, 2).toString()));
			items.setProduct_name(this.tableModel_issueJTable.getValueAt(i, 0)
					.toString());
			items.setQtyToIssue(Double.parseDouble(this.tableModel_issueJTable
					.getValueAt(i, 1).toString()));
			
			/*String changetodate = this.tableModel_issueJTable
					.getValueAt(i, 2).toString();*/
			try{
			if(!(registerdate.toString().equals(""))){
				items.setCreateddate(this.DateofissuedateChooser.getDate());
			}else{
				items.setCreateddate(new Date());
			}
			}catch(NullPointerException n){
				n.getMessage();
			}
			list.add(items);
		}

		// new Items_issue().createBeanCollection()
		try {

			parameterMap.put("bg_text", "Draft");
			parameterMap.put("issued_by", AppJFrame.userLoggedIn);
           StringBuffer sb = new StringBuffer(selecteddispensaryname);
		   System.out.print(sb.delete(sb.length()-2, sb.length()) +"removed Ids");
						
			if(!selecteddispensaryname.equalsIgnoreCase("Select Dispensing Site")){
				parameterMap.put("dispensing_point",
						sb);
				}

			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport("Reports/items_issue.jasper",
					parameterMap, new JRBeanCollectionDataSource(list));

			JRViewer jrViewer =  new JRViewer(print);
			showReport(jrViewer);
			//SCCReportDialogue dialog = new SCCReportDialogue(new JRViewer(print), null, true);
			//dialog.setVisible(true);

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}

	}

	public void showReport ( JRViewer jv) {

		JDialog jf = new JDialog();
		jf.setTitle("Report");
		jf.setModal(true);
		jv.setPreferredSize(new Dimension(1000, 600));
		jf.getContentPane().add(jv);
	
		 jf.validate();
		jf.setLocationRelativeTo(null);
		jf.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		jf.pack();
		jf.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - getWidth()/2, (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - getHeight()/2);
		
		jf.setVisible(true); 
		}
	
	private void callbackAlphaproductlist(){
		//get ARV list 
		try {

			initialsearchproductsList = ProductsJP.callbackInitialproductlist();
			ProductsJP.populateProductsTable(
							initialsearchproductsList);
			

		} catch (NullPointerException e) {
			e.getMessage();
		}
	}


	
	
	private void barcodeJTFKeyTyped(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		letter = evt.getKeyChar();

		StringBuilder s = new StringBuilder(charList.size());

		if ((letter == KeyEvent.VK_BACK_SPACE)
				|| (letter == KeyEvent.VK_DELETE)) {

			// do nothing
			searchText = barcodeJTF.getText();
			 System.out.println("I detedcted a back space ah! did you call me ??");
	           //re populate table with original list 
			 if(this.barcodeJTF.getText().toString().length() == 0){
	           this.callbackAlphaproductlist();
			 }

			if (charList.size() > 0) {

				charList.remove(charList.size() - 1);

			}

			if (charList.size() <= 0) {

				s = new StringBuilder(charList.size());
				charList.clear();
			}
			for (char c : charList) {

				s.append(c);

			}

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				System.out.println("Delete " + s);
				
			  // ProductsJP.searchtheproductsList(s);

				//this.productsList = session.selectList(	"selectProductBySearchTerm", s.toString());
			   this.productsList =  ProductsJP.searchtheproductsList(s);
			   if(productsList != null){
				ProductsJP.populateProductsTable(this.productsList);
			   }
			} finally {
				session.close();
			}

		} else {

			// searchText += Character.toString(letter);
			charList.add(letter);

			for (char c : charList) {

				s.append(c);

			}

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				System.out.println(s.toString());

				//this.productsList = session.selectList("selectProductBySearchTerm", s.toString());
				this.productsList =  ProductsJP.searchtheproductsList(s);
				if(productsList != null){
				ProductsJP.populateProductsTable(this.productsList);
				}

			} finally {
				session.close();
			}

		}

	}

	private void barcodeJTFKeyReleased(java.awt.event.KeyEvent evt) {
		// TODO Barcode scanner

		//last event is enter after barcode scan event
		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

			//get the scanned barcode number
			this.barcodeScanned = barcodeJTF.getText();

			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();

			try {

				this.selectedProduct = session.selectOne("getByProductBarcode",
						this.barcodeScanned);// ("getProgramesSupported");

			} finally {
				session.close();
			}

			//product found via barcode scanner
			if (this.selectedProduct != null) {

				//if qty in stock is zero , show message dialog 
				//qty = this.selectedProduct.getProductQty();
               //  if(qty.getQty() < 0){
                	// qty.setQty(0);
                // }
				//if 
				//if (qty.getQty() != 0) {

					// show the number pad to get quantity
					new NumberPadJD(javax.swing.JOptionPane
							.getFrameForComponent(this), true,
							this.selectedProduct,this.selectedprogramID);

				 

					/*javax.swing.JOptionPane
							.showMessageDialog(
									this,
									"<html><body bgcolor=\"#E6E6FA\"><b><p><p><p><p><font size=\"4\" color=\"red\">"
											+ ""
											+ this.selectedProduct
													.getPrimaryname()
											+ " is currently stocked out. </font><p><p><p><p></b></body></html>");*/
				

				
			} else {
				barcodeJTF.setText("");

				if (barcodeJTF.getText().equals("")) {

					charList.clear();
				}
				barcodeJTF.requestFocusInWindow();


				javax.swing.JOptionPane.showMessageDialog(null,
						"Product not found");
			}

			barcodeJTF.setText("");

		}

	}

	private void deleteItemLineJBtnActionPerformed(
			java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		//this.issueJTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	
	    for(int i = 0; i <= this.issueJTable.getRowCount()-1;i++){   
		tableModel_issueJTable.deleteRow(i);
		//allow multiple delete
		// issueJTable.clearSelection();
	    }
	}
	
	private java.sql.Timestamp Convertdatestr(String mydate) {
         String newDateString = "";
         java.sql.Timestamp selecteddate = null;
		try {

			System.out.println(mydate + "Error source 2014");
			final String OLD_FORMAT = "EEE MMM d HH:mm:ss z yyyy";
			//final String NEW_FORMAT = "yyyy-MM-d hh:mm:ss.S";
			final String NEW_FORMAT = "yyyy-MM-dd hh:mm:ss.S";
			

			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			try {
			Date d = sdf.parse(mydate);
			
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);
			selecteddate = java.sql.Timestamp.valueOf(newDateString);
			System.out.println(selecteddate + "for Receive EXPIRY");
			
			}catch(ParseException e){
				e.getCause();
			}

		} catch (NullPointerException e) {
			e.getMessage();
		}
		System.out.println(selecteddate + "same expiry date??");
		return selecteddate;
		

	}
	
	@SuppressWarnings("static-access")
	private void finishIssuingJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO Improve to a batch input
	   if(!(selecteddispensaryname.equalsIgnoreCase("Select Dispensing Point"))){   
		
		@SuppressWarnings("rawtypes")
		HashMap hiv_site_balance  = new HashMap();
	    //Will need to split products by program area here.
		
		 Date date =  new Date();
		  //Moses_Kausa_2014
		   dispensarypqty = new ProductQty();
		   Facilityproductsmapper = new  FacilityProductsDAO();
		   aFacilityproductsmapper = new ARVDispensingDAO();
		   //END_2014
			SqlSessionFactory factory = new MyBatisConnectionFactory()
					.getSqlSessionFactory();

			SqlSession session = factory.openSession();
			
						
			StockControlCard scc;
			try {
			
			for (int i = 0; i < this.tableModel_issueJTable.getRowCount(); i++) {
				
				scc = new StockControlCard();
				
				scc.setId(this.GenerateGUID());

				scc.setProductcode(this.tableModel_issueJTable.getValueAt(i, 3)
						.toString());
				scc.setProductid(Integer.parseInt(this.tableModel_issueJTable
						.getValueAt(i,2).toString()));
				;
				scc.setQty_isssued(Double.parseDouble(this.tableModel_issueJTable
						.getValueAt(i, 1).toString()));
				if(!selecteddispensaryname.equalsIgnoreCase("Select Dispensing Point")){
				scc.setIssueto_receivedfrom(selecteddispensaryname);
				}
				scc.setCreatedby(AppJFrame.userLoggedIn);
			
				try{
				if(!(registerdate.toString().equals(""))){
					scc.setCreateddate(this.DateofissuedateChooser.getDate());
				}else{
					scc.setCreateddate(new Date());
				}
				}catch(NullPointerException n){
					n.getMessage();
				}
				scc.setRemark("Products issue");
				if(selecteddpId != null){
				scc.setDpid(selecteddpId);
				}
				if(!(this.issuetoprogram.equals(""))){
					scc.setProgram_area(issuetoprogram);
				}
				
				//reduce the stock control card balance by issue qty
								
				if(!(scc.getQty_isssued() == null)){
					
					//Changes introduced by Moses Kausa
					// Save the issued quantity to the Dispensary stock  on hand table elmis_productQty
					if(!(selectedprogramID == 0)){
						this.programsbalList =  this.Facilityproductsmapper.dogetselectedprogram(selectedprogramID);
					}
					//crate hash map for HIV
					
					hiv_site_balance.put("adjustmentQty",scc.getQty_isssued());
					hiv_site_balance.put("productCode",scc.getProductcode());
					
										
					dispensarypqty.setProduct_code(scc.getProductcode());
					dispensarypqty.setQty(scc.getQty_isssued());
					dispensarypqty.setLocation(scc.getIssueto_receivedfrom());
					if(selecteddpId != null){
						hiv_site_balance.put("siteId",selecteddpId);
						dispensarypqty.setDPid(selecteddpId);
						}
					
					
					callhivtestbalances =  new  FacilityStoreProductBalanceDAO();
					int testDpidexist = 0;
					testDpidexist
					=  new HivTestResultDao().doselectproductDPid(scc.getProductcode(),selecteddpId);
					productcodesstockQtyList = aFacilityproductsmapper.doselectProductQtydispense(scc.getProductcode(),selecteddpId);
					for( Programs p: programsbalList ){
						if(!(p.getCode() == null)){
						electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(p.getCode(),scc.getProductcode());
						}else{
							electronicsccbal = this.Facilityproductsmapper.dogetcurrentSystemcalculatedproductbalancesbyprogram(p.getCode(),scc.getProductcode());
						}
					}
					
					for(VW_Systemcalculatedproductsbalance b: electronicsccbal){
						if( b.getProductcode().equals(dispensarypqty.getProduct_code())){
							System.out.println(b.getProductcode()  + "= " + dispensarypqty.getProduct_code());
							dispensarypqty.setStockonhand_scc(b.getBalance());
							@SuppressWarnings("unused")
							double reduce = b.getBalance() - scc.getQty_isssued();
							scc.setBalance(reduce);
						}
					}
					
					if(!(productcodesstockQtyList.isEmpty()) ){
					for (@SuppressWarnings("unused")
					ProductQty p : productcodesstockQtyList) {
						
						if(p.getProduct_code().equals(scc.getProductcode())){
							
							//call for HIV products only 
							if((p.getProduct_code().toLowerCase().contains("htk"))){
								
								if(p.getDPid() == testDpidexist){
								Facilityproductsmapper.doupdateHIV_test_balancesset(hiv_site_balance);	
								}else{
									new HivTestResultDao().Insertdpidhivtestbalances(hiv_site_balance);
								}
							}else{
								Facilityproductsmapper.doupdatedispensaryqty(dispensarypqty.getProduct_code(),dispensarypqty.getQty(),dispensarypqty.getLocation(),dispensarypqty.getStockonhand_scc());	
							}
							//update call here now 
							
						}else{
							
							//new HivTestResultDao().Insertdpidhivtestbalances(hiv_site_balance);
							Facilityproductsmapper.InsertproductQuantity(dispensarypqty);
						}
						
						}
					}else{
						
						
						
						if((scc.getProductcode().toLowerCase().contains("htk"))){
							new HivTestResultDao().Insertdpidhivtestbalances(hiv_site_balance);
						}
						Facilityproductsmapper.InsertproductQuantity(dispensarypqty);
						//new HivTestResultDao().Insertdpidhivtestbalances(hiv_site_balance);
					}
					//END _update- change
					
										
				}
                //END - changes mkausa
			session.insert("insertSelectiveSCC", scc);
		 
			session.commit();
		
			}
			
		
			} finally {

				session.close();

				javax.swing.JOptionPane.showMessageDialog(this,
						"Products issued to "+this.dp.getName());
					this.dispose();

			}
			
		

		this.dispose();
		AppJFrame.glassPane.deactivate();
		
		// TODO Remove and change jasperReport To SCC class object

		List list = new ArrayList();
		Items_issue items;
		for (int i = 0; i < this.tableModel_issueJTable.getRowCount(); i++) {

			items = new Items_issue();

			items.setProduct_code(this.tableModel_issueJTable.getValueAt(i, 3)
					.toString());
			items.setProduct_id(Integer.parseInt(this.tableModel_issueJTable
					.getValueAt(i, 2).toString()));
			items.setProduct_name(this.tableModel_issueJTable.getValueAt(i, 0)
					.toString());
			items.setQtyToIssue(Double.parseDouble(this.tableModel_issueJTable
					.getValueAt(i, 1).toString()));
			
			/*String changetodate = this.tableModel_issueJTable
					.getValueAt(i, 2).toString();*/
			try{
			if(!(registerdate.toString().equals(""))){
				items.setCreateddate(this.DateofissuedateChooser.getDate());
			}else{
				items.setCreateddate(new Date());
			}
			}catch(NullPointerException n){
				n.getMessage();
			}
			list.add(items);
		}

		// new Items_issue().createBeanCollection()
		try {
            
			
		    StringBuffer sb = new StringBuffer(selecteddispensaryname);
		    
		    System.out.print(sb.delete(sb.length()-2, sb.length()) +"removed Ids");
			parameterMap.put("bg_text", "");
			parameterMap.put("issued_by", AppJFrame.userLoggedIn);
			
		   parameterMap.put("receivedby", Receivedbyuser);
			
			if(!selecteddispensaryname.equalsIgnoreCase("Select Dispensing Point")){
				parameterMap.put("dispensing_point",
						sb);
				}
	
			AppJFrame.glassPane.activate(null);
			print = JasperFillManager.fillReport("Reports/items_issue.jasper",
					parameterMap, new JRBeanCollectionDataSource(list));
            
			JRViewer jrViewer =  new JRViewer(print);
			showReport(jrViewer);
			
			//SCCReportDialogue dialog = new SCCReportDialogue(new JRViewer(print), null, true);
			//dialog.setVisible(true);
				/*JFrame frame2 = new JFrame("Report");
			frame2.getContentPane().setPreferredSize(
					new Dimension(1020, 650));
			frame2.getContentPane().add(new JRViewer(print));
			frame2.pack();
			frame2.setVisible(true);*/

		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage()
					.toString());
		}

	
	}// Exit check dispensary selected
	   else{
			JOptionPane.showMessageDialog(this, "You must select a dispensing point to finish issuing \n\n " 
				 ,  "Select Dispensing point", JOptionPane.ERROR_MESSAGE);
	   }
			

	}
private static void flushtableModel_barcodefproducts(String[]  itemList){
		
		for (int i = 0; i < itemList.length - 1; i++) {
			 
                if(i == 0) {
				defaultv_barcodefacilityapprovedproducts[0] = "";
						
				defaultv_barcodefacilityapprovedproducts[1] = "";
				defaultv_barcodefacilityapprovedproducts[2] = "";
				defaultv_barcodefacilityapprovedproducts[3] = "";
								
                }
				 // find the position of array ;	
				System.out.println(i);	
				
			}
	}
	
	@SuppressWarnings("unchecked")
	public static void PopulateProgram_ChangeregimenTable(String[] itemList) {
		// TODO Auto-generated method stub
		try {
									
		for (int i = 0; i < itemList.length - 1; i++) {
			 
                if(i == 0) {
				defaultv_barcodefacilityapprovedproducts[0] = itemList[i]
						.toString();
				defaultv_barcodefacilityapprovedproducts[1] = itemList[i + 1]
						.toString();
                 
				//TableColumn column1 = issueJTable	.getColumnModel().getColumn(2);
				//column1.setCellRenderer(new JDateChooserRenderer());
				
				
			    //column1.setCellEditor(new DatePickerCellEditor());
								
				defaultv_barcodefacilityapprovedproducts[2] = itemList[i + 2]
						.toString();
				defaultv_barcodefacilityapprovedproducts[3] = itemList[i + 3]
						.toString();
				

				@SuppressWarnings("rawtypes")
				ArrayList cols = new ArrayList();

				//cols.add(regimenlist);
				for (int j = 0; j < columns_barcodefacilityApprovedproducts.length; j++) {
					cols.add(defaultv_barcodefacilityapprovedproducts[j]);

				}

				tableModel_issueJTable.insertRow(cols);
				//fapprovedproductsIterator.remove();
                }
				 // find the position of array ;	
				System.out.println(i);	
				
			}
		  

		
		} catch (NullPointerException e) {
			//do something here 
			e.getMessage();
		}
		flushtableModel_barcodefproducts(itemList);
	}
	private void cancelJBtnActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		// this.cancelJBtn.getFocusListeners();
		// this.cancelJBtn.transferFocus();
		this.cancelJBtn.requestFocusInWindow();
		AppJFrame.glassPane.deactivate();
		this.dispose();

	}

	private String GenerateGUID() {

		UUID uuid = UUID.randomUUID();
		String Idstring = uuid.toString();
		return Idstring;

	}

	private void jTabbedPane1KeyReleased(java.awt.event.KeyEvent evt) {
		// TODO add your handling code here:

		if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
			// your code is scanned and you can access it using
			// frame.getBarCode()
			// now clean the bar code so the next one can be read
			// frame.setBarCode(new String());
			// javax.swing.JOptionPane.showMessageDialog(null, "Here");

		}
	}

	private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {
		// TODO add your handling code here:

		// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getSelectedIndex());

		if (jTabbedPane1.getSelectedIndex() == 1
				&& jTabbedPane1.getTabCount() == 3) {// &&
			// jTabbedPane1.getSelectedIndex()
			// ==2){

			// javax.swing.JOptionPane.showMessageDialog(null,jTabbedPane1.getTabCount());

			jTabbedPane1.remove(2);
		}

		if (jTabbedPane1.getSelectedIndex() == 0
				&& jTabbedPane1.getTabCount() == 2) {// &&
			//remove the products JTable
			jTabbedPane1.remove(1);
			barcodeJTF.setVisible(false);
			searchJL.setVisible(false);
		}

	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				IssuingJD dialog = new IssuingJD(new javax.swing.JFrame(),
						true, new Dispensing_point());
				dialog.addWindowListener(new java.awt.event.WindowAdapter() {
					public void windowClosing(java.awt.event.WindowEvent e) {
						System.exit(0);
					}
				});
				dialog.setVisible(true);
			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	public static javax.swing.JTextField barcodeJTF;
	private javax.swing.JButton cancelJBtn;
	private javax.swing.JButton deleteItemLineJBtn;
	private javax.swing.JButton finishIssuingJBtn;
	public static javax.swing.JTable issueJTable;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel2;
	private javax.swing.JScrollPane jScrollPane1;
	public static javax.swing.JTabbedPane jTabbedPane1;
	private javax.swing.JButton previewJBtn;
	public static javax.swing.JLabel searchJL;
	@SuppressWarnings("rawtypes")
	private JComboBox  SitedispensingpointcomboBox;
	@SuppressWarnings("rawtypes")
	private JComboBox ReceivedbycomboBox;
	private JDateChooser DateofissuedateChooser;
}